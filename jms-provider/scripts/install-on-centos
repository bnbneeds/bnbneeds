#!/usr/bin/env bash

UNAME=/usr/bin/uname
PRINTF=/usr/bin/printf
MKDIR=/usr/bin/mkdir
RM=/usr/bin/rm
CP=/usr/bin/cp
LS=/usr/bin/ls
SED=/usr/bin/sed
TAR=/usr/bin/tar
IFCONFIG=/usr/sbin/ifconfig
GREP=/usr/bin/grep
CUT=/usr/bin/cut
AWK=/usr/bin/awk
SERVICE=/usr/sbin/service
SYSTEMCTL=/usr/bin/systemctl

SERVICE_CONFIG_FILE=bnbneeds-jms-provider-service.config
SERVICE_UNIT_FILE=bnbneeds-jms-provider.service

INSTALL_DIR="/opt/bnbneeds"
PACKAGE_NAME="@@BUILD_PACKAGE_NAME@@"

echo -e "\n-----------------------------------------------------"
echo "Welcome to BNBneeds JMS Provider (Production) Setup Wizard."
echo -e "-------------------------------------------------------\n"

ask_continue()
{
	echo -n "Do you want to continue with the installation (y/n) ? "
	while read response;
	do
     if [[ $response == "y" ]]; then
          do_install
     elif [[ $response == "n" ]]; then
          $PRINTF "%-40s : %s\n" "Installation of BNBneeds Portal" "[Terminated]"
          exit 0
     fi
     echo -n "Do you want to continue with the installation (y/n) ? "
	done
}

do_install()
{

curdir=$PWD
retval=0

echo -e "\nChecking for JAVA installation...\n"
# Determine the Java command to use to start the JVM.
if [ -n "$JAVA_HOME" ] ; then
    if [ -x "$JAVA_HOME/jre/sh/java" ] ; then
        # IBM's JDK on AIX uses strange locations for the executables
        JAVACMD="$JAVA_HOME/jre/sh/java"
    else
        JAVACMD="$JAVA_HOME/bin/java"
    fi
    if [ ! -x "$JAVACMD" ] ; then
        die "ERROR: JAVA_HOME is set to an invalid directory: $JAVA_HOME

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
    fi
else
    JAVACMD="java"
    which java >/dev/null 2>&1 || die "ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
fi

echo -n  "Java Home Directory (JAVA_HOME): [$JAVA_HOME] "

cd $curdir
if [[ $retval -eq 1 ]]; then
     $PRINTF "%-40s : %s\n" "Installation of BNBneeds JMS Provider" "Failed"
     exit 1
fi 

$PRINTF "\n%-40s : %s\n" "Installation of BNBneeds JMS Provider..." "[In Progress]"


# Create the installation directory if does not exist already
installDir="${INSTALL_DIR}/${PACKAGE_NAME}"
echo -n  "Installation Directory [$installDir] "
if [[ ! -d $installDir ]]; then
     mkdirOutput=`$MKDIR -p $installDir 2>&1`
     if [[ $? -ne 0 ]]; then
          echo "Create installation directory failed : $mkdirOutput"
          $PRINTF "%-40s : %s\n" "Installation of BNBneeds JMS Provider" "[Failed]"
          exit 1
     else
          $PRINTF "%-40s : %s\n" "$installDir" "[Created successfully]"
     fi
else
     $PRINTF "%-40s : %s\n" "$installDir" "[Already Exists]"
fi

# Copy the contents to installation directory
retval=0
curdir=$PWD
copyOutput=`$CP -r $curdir/bin $installDir 2>&1`
if [[ $? -ne 0 ]]; then
     $PRINTF "%-40s : %s\n" "Copy of bin to $installDir" "[Failed]"
     $PRINTF "    $copyOutput\n"
     $PRINTF "%-40s : %s\n" "Installation of BNBneeds JMS Provider" "[Failed]"
     exit 1
fi

copyOutput=`$CP -r $curdir/lib $installDir 2>&1`
if [[ $? -ne 0 ]]; then
     $PRINTF "%-40s : %s\n" "Copy of lib to $installDir" "[Failed]"
     $PRINTF "    $copyOutput\n"
     $PRINTF "%-40s : %s\n" "Installation of BNBneeds JMS Provider" "[Failed]"
     exit 1
fi

copyOutput=`$CP -r $curdir/conf $installDir 2>&1`
if [[ $? -ne 0 ]]; then
     $PRINTF "%-40s : %s\n" "Copy of conf to $installDir" "[Failed]"
     $PRINTF "    $copyOutput\n"
     $PRINTF "%-40s : %s\n" "Installation of BNBneeds JMS Provider" "[Failed]"
     exit 1
fi

mkdirLogOutput=`$MKDIR -p $installDir/logs`
if [[ $? -ne 0 ]]; then
     $PRINTF "%-40s : %s\n" "mkdir $installDir/logs" "[Failed]"
     $PRINTF "    $mkdirLogOutput\n"
     $PRINTF "%-40s : %s\n" "Installation of BNBneeds JMS Provider" "[Failed]"
     exit 1
fi

sedOutput=`$SED -i "s,JAVA_HOME=\"[a-z\-]*\",JAVA_HOME=\"${JAVA_HOME}\",g" $curdir/service-unit/${SERVICE_CONFIG_FILE} 2>&1`
if [[ $? -ne 0 ]]; then
     $PRINTF "%-40s : %s\n" "Replacing value of JAVA_HOME in ${SERVICE_CONFIG_FILE}" "[Failed]"
     $PRINTF "    $sedOutput\n"
     $PRINTF "%-40s : %s\n" "Installation of BNBneeds JMS Provider" "[Failed]"
     exit 1
fi

sedOutput=`$SED -i "s,BNBNEEDS_JMS_PROVIDER_HOME=\"[a-z\-]*\",BNBNEEDS_JMS_PROVIDER_HOME=\"${INSTALL_DIR}/\${PACKAGE_NAME}\",g" $curdir/service-unit/${SERVICE_CONFIG_FILE} 2>&1`
if [[ $? -ne 0 ]]; then
     $PRINTF "%-40s : %s\n" "Replacing value of JAVA_HOME in ${SERVICE_CONFIG_FILE}" "[Failed]"
     $PRINTF "    $sedOutput\n"
     $PRINTF "%-40s : %s\n" "Installation of BNBneeds JMS Provider" "[Failed]"
     exit 1
fi

copyOutput=`$CP -r $curdir/service-unit/${SERVICE_UNIT_FILE} /etc/systemd/system 2>&1`
if [[ $? -ne 0 ]]; then
     $PRINTF "%-40s : %s\n" "Copy of $curdir/service-unit/${SERVICE_UNIT_FILE} to /etc/systemd/system" "[Failed]"
     $PRINTF "    $copyOutput\n"
     $PRINTF "%-40s : %s\n" "Installation of BNBneeds JMS Provider" "[Failed]"
     exit 1
fi

copyOutput=`$CP -r $curdir/service-unit/${SERVICE_CONFIG_FILE} /etc/sysconfig 2>&1`
if [[ $? -ne 0 ]]; then
     $PRINTF "%-40s : %s\n" "Copy of $curdir/${PACKAGE_NAME}/service-unit/${SERVICE_CONFIG_FILE} to /etc/sysconfig" "[Failed]"
     $PRINTF "    $copyOutput\n"
     $PRINTF "%-40s : %s\n" "Installation of BNBneeds JMS Provider" "[Failed]"
     exit 1
fi

reloadDaemonOutput=`$SYSTEMCTL daemon-reload`
if [[ $? -ne 0 ]]; then
     $PRINTF "%-40s : %s\n" "Reload System Control Daemon" "[Failed]"
     $PRINTF "    $reloadDaemonOutput\n"
     exit 0
fi


enableStartUpOutput=`$SYSTEMCTL enable /etc/systemd/system/${SERVICE_UNIT_FILE}`
if [[ $? -ne 0 ]]; then
     $PRINTF "%-40s : %s\n" "Enable BNBNeeds Portal as a start up service" "[Failed]"
     $PRINTF "    $enableStartUpOutput\n"
     exit 0
fi

serviceOutput=`$SYSTEMCTL start ${SERVICE_UNIT_FILE}`
if [[ $? -ne 0 ]]; then
     $PRINTF "%-40s : %s\n" "Start BNBNeeds Portal service" "[Failed]"
     $PRINTF "    $serviceOutput\n"
     exit 0
fi

$PRINTF "%-40s : %s\n" "BNBneeds JMS Provider Service" "[Started]"

$PRINTF "%-40s : %s\n" "Installation of BNBneeds JMS Provider" "[Completed Successfully]"

exit 0
	
}

ask_continue
do_install

