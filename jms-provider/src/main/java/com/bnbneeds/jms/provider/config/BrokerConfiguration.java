package com.bnbneeds.jms.provider.config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jms.Connection;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.TransportConnector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.bnbneeds.portal.common.jms.JMSConstants;

@Configuration
public class BrokerConfiguration {

	@Value("${activemq.broker.url}")
	private String brokerUrl;

	@Value("${spring.application.name}")
	private String clientId;

	@Value("${activemq.broker.username}")
	private String userName;

	@Value("${activemq.broker.password}")
	private String password;

	private BrokerService brokerService;
	private Connection connection;
	private TransportConnector connector;

	@PostConstruct
	public void start() throws Exception {
		brokerService = new BrokerService();
		brokerService.setBrokerName("bnbneeds-activemq-broker");
		connector = brokerService.addConnector(brokerUrl);
		brokerService.setUseJmx(false);
		brokerService.setStartAsync(true);
		brokerService.setUseShutdownHook(true);
		brokerService.start();

		ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(connector.getConnectUri());
		activeMQConnectionFactory.setUserName(userName);
		activeMQConnectionFactory.setPassword(password);
		activeMQConnectionFactory.setClientID(clientId);
		List<String> trustedPackages = new ArrayList<>();
		trustedPackages.add(JMSConstants.JMS_MESSAGE_BEANS_PACKAGE);
		activeMQConnectionFactory.setTrustedPackages(trustedPackages);
		connection = activeMQConnectionFactory.createConnection();

		connection.start();
	}

	@PreDestroy
	public void stop() throws Exception {
		brokerService.stop();
		connection.stop();
		connector.stop();
	}

}
