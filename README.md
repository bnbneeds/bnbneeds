# BNBNeeds #

BNBNeeds is an e-commerce app that connects Buyers and Vendors in a B2B market. Vendors can register their organizations, products and advertize offers.

* Version: 1.0
* [REST API Specification](https://bitbucket.org/bnbneeds/bnbneeds/wiki/BNB%20Needs%20REST%20API%20Specification)
* [Project Structure](https://bitbucket.org/bnbneeds/bnbneeds/wiki/Components%20And%20Their%20Responsibilities)

### How do I get set up? ###


1. Download the source code using git.
2. Execute `./gradlew clean build` from the project root folder. This builds all the sub-projects: `models`, `rest-client`, `apisvc`, `androidapp` and `admin-portal`.
3. `apisvc` uses GAE: Google App Engine. And it uses Google Cloud Datastore for persisting data. It is heart of the app - the main component.
4. `dealer-portal`, `admin-portal` and `androidapp` communicate with `apisvc` using REST APIs.

#### Building the entire project ####

```
#!shell

~/bnbneeds# ./gradlew clean build

```

The ZIP and TAR archives of `dealer-portal` and `admin-portal` are under `build/distributions` directory of the sub-projects respectively. These archives can be taken and inflate on any host.

Steps to use `dealer-portal` ZIP or TAR: 

Inflate `dealer-portal-1.0.zip` or `dealer-portal-1.0.tar`

```
#!shell

#cd dealer-portal-1.0/bin

# Open conf/application.properties to edit apisvc.url based on where apisvc is running in your environment.

# On Linux: 

dealer-portal-1.0/bin# ./dealer-portal

# On Windows: open dealer-potal.bat

```

Follow similar steps for running `admin-portal`.

However, you can build and execute individual sub-projects locally.


### API Service (apisvc) ###

To build and execute `apisvc` on development server (local server), execute the command below. It runs as a REST web service listening on port: 8080.


```
#!shell

~/bnbneeds# ./gradlew appengineRun

```

### Administrator Portal (admin-portal) ###

To build and execute `admin-portal` execute the commands below:

```
#!shell

~/bnbneeds# ./gradlew buildAdminPortal

```

Once `admin-portal` is successfully built, execute:

```
#!shell

~/bnbneeds# cd admin-portal/build/dist/bin

~/bnbneeds/admin-portal/build/dist/bin# 

# Open conf/application.properties to edit apisvc.url based on where apisvc is running in your environment.

~/bnbneeds/admin-portal/build/dist/bin# ./admin-portal

# On Windows, execute the BAT file
# C:\bnbneeds\admin-portal\build\dist\bin> admin-portal.bat

```

`admin-potal` listens on port 8084. Open http://localhost:8084 on your browser to launch the portal.

### Dealer Portal (dealer-portal) ###

To build and execute `dealer-portal` execute the commands below:


```
#!shell

~/bnbneeds# ./gradlew buildDealerPortal

```

Once `dealer-portal` is successfully built, execute:

```
#!shell

~/bnbneeds# cd dealer-portal/build/dist/bin

~/bnbneeds/dealer-portal/build/dist/bin# 

# Open conf/application.properties to edit apisvc.url based on where apisvc is running in your environment.

~/bnbneeds/dealer-portal/build/dist/bin# ./dealer-portal

# On Windows, execute the BAT file

# C:\bnbneeds\dealer-portal\build\dist\bin> dealer-portal.bat


```

`dealer-potal` listens on port 80. Open http://localhost on your browser to launch the portal.

### Contribution guidelines ###

* [Developer Environment Setup](https://bitbucket.org/bnbneeds/bnbneeds/wiki/BNBNeeds%20Developer%20Environment%20Setup)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact