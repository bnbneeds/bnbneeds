package com.bnbneeds.portal.admin.controller;



import static com.bnbneeds.portal.admin.controller.util.RequestResponseUtils.handleBadRequest;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.EntityParam;
import com.bnbneeds.app.model.UpdateEntityParam;
import com.bnbneeds.app.model.EntityApprovalParam.EntityStatus;
import com.bnbneeds.app.model.account.UpdateUserAccountParam;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.dealer.UpdateEnquiryParam;
import com.bnbneeds.app.model.product.CreateProductTypeBulkParam;
import com.bnbneeds.app.model.product.CreateProductTypeParam;
import com.bnbneeds.app.model.product.ProductInfoResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.formbean.EntityBean;
import com.bnbneeds.portal.admin.formbean.EntityListBean;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.portal.admin.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.admin.formbean.UpdateEntityBean;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.rest.client.core.AbstractMasterEntityTypes.EntityType;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@Layout(title = ProductTypeController.LIST_VIEW_TITLE_NAME)
@RequestMapping(path = ProductTypeController.BASE_REQUEST_PATH)
public class ProductTypeController extends MasterEntityController {

	protected static final String LIST_VIEW_TITLE_NAME = "Product Types";
	protected static final String BASE_REQUEST_PATH = "/product-types";
	public static final String LIST_ENTITIES_REQUEST_URI = BASE_REQUEST_PATH
			+ "/list";
	private static final String CREATE_ENTITIES_REQUEST_URI = BASE_REQUEST_PATH
			+ "/create";
	private static final String UPDATE_ENTITY_FORM_ATTRIBURE = "updateEntityParam";;

	@RequestMapping(method = RequestMethod.POST, path = "/create")
	@PreAuthorize("hasAnyAuthority('SUPER_ADMIN','ADMIN')")
	public String create(
			@ModelAttribute(ENTITY_LIST_FORM_ATTRIBUTE) EntityListBean form,
			Model model) {

		setViewTitle(model, LIST_VIEW_TITLE_NAME);
		CreateProductTypeBulkParam param = new CreateProductTypeBulkParam();
		List<EntityBean> entities = form.getEntities();
		for (Iterator<EntityBean> iterator = entities.iterator(); iterator
				.hasNext();) {
			EntityBean entityBean = iterator.next();

			CreateProductTypeParam eachParam = new CreateProductTypeParam();
			eachParam.setName(entityBean.getName());
			String desc = entityBean.getDescription();
			if (desc != null) {
				eachParam.setDescription(desc);
			}
			param.add(eachParam);
		}

		try {
			bnbneedsClient.productTypes().create(param);
		} catch (BNBNeedsServiceException e) {
			setModelForEntityListView(model, null);
			return handleBadRequest(e, model, ENTITY_LIST_VIEW);
		}

		return getRedirectPath(LIST_ENTITIES_REQUEST_URI + "?createSuccess");
	}
	
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST } , path = "/{id}/delete")
	public String delete(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String productTypeId, Model model) {

		try {
			bnbneedsClient.productTypes().delete(productTypeId);
		} catch (BNBNeedsServiceException e) {
			setModelForEntityListView(model, null);
			return handleBadRequest(e, model, ENTITY_LIST_VIEW);
		}

		return getRedirectPath(LIST_ENTITIES_REQUEST_URI + "?deleteSuccess");
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST } , path = "/{id}/disapprove")
	public String disapprove(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String entityid, Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.DISAPPROVED);
			bnbneedsClient.productTypes().updateEntityStatus(entityid,param);
		} catch (BNBNeedsServiceException e) {
			setModelForEntityListView(model, null);
			return handleBadRequest(e, model, ENTITY_LIST_VIEW);
		}

		return getRedirectPath(LIST_ENTITIES_REQUEST_URI + "?disapproveSuccess");
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST } , path = "/{id}/approve")
	public String approve(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String entityid, Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.APPROVED);
			bnbneedsClient.productTypes().updateEntityStatus(entityid,param);
		} catch (BNBNeedsServiceException e) {
			setModelForEntityListView(model, null);
			return handleBadRequest(e, model, ENTITY_LIST_VIEW);
		}

		return getRedirectPath(LIST_ENTITIES_REQUEST_URI + "?approveSuccess");
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST } , path = "/{id}/blacklist")
	public String blacklist(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String entityid, Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.BLACKLISTED);
			bnbneedsClient.productTypes().updateEntityStatus(entityid,param);
		} catch (BNBNeedsServiceException e) {
			setModelForEntityListView(model, null);
			return handleBadRequest(e, model, ENTITY_LIST_VIEW);
		}

		return getRedirectPath(LIST_ENTITIES_REQUEST_URI + "?blacklistSuccess");
	}
	
	
	@Override
	public String getViewTitle() {
		return LIST_VIEW_TITLE_NAME;
	}
	
	@Override
	public String getBaseRequestUrl() {
		return BASE_REQUEST_PATH;
	}

	@Override
	public String getCreateEntityURI() {
		return CREATE_ENTITIES_REQUEST_URI;
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.product_type;
	}

	@Override
	public String getListEntityURI() {
		return LIST_ENTITIES_REQUEST_URI;
	}

}
