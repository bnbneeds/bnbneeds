package com.bnbneeds.portal.admin.formbean;

import org.hibernate.validator.constraints.NotBlank;

public class CancelMemberRequestBean {

	@NotBlank
	private String reason;

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
