package com.bnbneeds.portal.admin.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionInfoListResponse;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionInfoResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.core.BuyerBiddingSubscriptions;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@Layout(title = BuyerBiddingSubscriptionController.LIST_VIEW_TITLE_NAME)
@RequestMapping(path = BuyerBiddingSubscriptionController.BASE_REQUEST_PATH)
public class BuyerBiddingSubscriptionController extends AbstractBaseController {

	protected static final String LIST_VIEW_TITLE_NAME = "Dealer Bidding Subscription Plans";
	static final String BIDDING_SUBSCRIPTION_VIEW = "bidding-subscription-details";
	static final String BIDDING_SUBSCRIPTIONS_LIST_VIEW = "bidding-subscriptions-list";
	private static final String LIST_VIEW_TITLE_ATTRIBUTE = "listViewTitle";
	protected static final String BASE_REQUEST_PATH = "/buyers/{buyerId}/bidding-subscriptions";
	private static final String LIST_RESPONSE_ATTRIBUTE = "listResponse";
	private static final String LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_ATTRIBUTE = "listBiddingSubscriptionsRequestURI";
	private static final String BASE_REQUEST_URL_ATTRIBUTE = "baseRequestURI";
	private static final String LIST_BIDDING_SUBSCRIPTIONS_REQUEST_URI = BASE_REQUEST_PATH + "/list";
	private static final String BIDDING_SUBSCRIPTIONS_INFO_ATTRIBURE = "biddingSubscriptionsInfo";

	public String getViewTitle() {
		return LIST_VIEW_TITLE_NAME;
	}

	public String getListVerificationSubscriptionsURI() {
		return LIST_BIDDING_SUBSCRIPTIONS_REQUEST_URI;
	}

	public String getBaseRequestUrl() {
		return BASE_REQUEST_PATH;
	}

	protected void setViewTitle(Model model, String title) {
		model.addAttribute(LIST_VIEW_TITLE_ATTRIBUTE, title);
	}

	protected void setBaseRequestUrl(Model model, String baseUrl) {
		model.addAttribute(BASE_REQUEST_URL_ATTRIBUTE, baseUrl);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/list")
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String list(@PathVariable("buyerId") String buyerId,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset, Model model) {
		try {
			setModelForSubscriptionsListView(model, buyerId, nextOffset);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BIDDING_SUBSCRIPTIONS_LIST_VIEW);
		}
		return BIDDING_SUBSCRIPTIONS_LIST_VIEW;
	}

	protected void setModelForSubscriptionsListView(Model model, String buyerId, String nextOffset) {

		setViewTitle(model, getViewTitle());
		setBaseRequestUrl(model, getBaseRequestUrl());

		model.addAttribute(LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_ATTRIBUTE,
				request.getContextPath() + getListVerificationSubscriptionsURI());

		try {
			BuyerBiddingSubscriptionInfoListResponse listResponse = null;
			BuyerBiddingSubscriptions buyerBiddingSubscriptionsClient = bnbneedsClient
					.buyerBiddingSubscriptions(buyerId);
			if (StringUtils.hasText(nextOffset)) {
				listResponse = buyerBiddingSubscriptionsClient.list(RESULT_FETCH_SIZE, null, nextOffset);
			} else {
				listResponse = buyerBiddingSubscriptionsClient.list(RESULT_FETCH_SIZE);
			}

			model.addAttribute(LIST_RESPONSE_ATTRIBUTE, listResponse);
		} catch (BNBNeedsServiceException e) {
			throw e;
		}
	}

	@Layout(title = "Subscription Plan Details")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/details")
	public String getSubscriptionDetails(@PathVariable("buyerId") String buyerId,
			@PathVariable("id") String subscriptionId, Model model) {

		BuyerBiddingSubscriptionInfoResponse info = bnbneedsClient.buyerBiddingSubscriptions(buyerId)
				.getSubscription(subscriptionId);
		model.addAttribute(BIDDING_SUBSCRIPTIONS_INFO_ATTRIBURE, info);
		return BIDDING_SUBSCRIPTION_VIEW;
	}

	public static String getBiddingSubscriptionPlansURI() {
		return String.format(LIST_BIDDING_SUBSCRIPTIONS_REQUEST_URI);
	}

}
