package com.bnbneeds.portal.admin.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;

import com.mailgun.rest.client.MailgunClient;

public abstract class DealerController extends AbstractBaseController {
	
	private static final Logger logger = LoggerFactory
			.getLogger(DealerController.class);

	private static final String DATE_RANGE_DELIMITER = "to";
	
	@Autowired
	protected MailgunClient mailgunClient;
	
	@Value("${email.address.all-dealers}")
	protected String dealersMailingListAddress;

	@Value("${email.address.buyers}")
	protected String buyersMailingListAddress;

	@Value("${email.address.vendors}")
	protected String vendorsMailingListAddress;

	protected static final Map<String, String> APPROVED_ENTITY_QUERY_MAP = new HashMap<String, String>();
	static {
		APPROVED_ENTITY_QUERY_MAP.put("status", "approved");
	}

	protected void setSelectedDealer(Model model, String dealer) {
		model.addAttribute("selectedDealer", dealer);
	}

	protected String[] getRequestStartAndEndDate(String dateRange) {
		if (dateRange == null) {
			return null;
		}
		String[] dateRangeArray = dateRange.split(DATE_RANGE_DELIMITER);
		return dateRangeArray;
	}
	
	protected void removeDealerFromMailinglist(String dealerEmailAddress) {
		try{
			mailgunClient.deleteMailingListMember(dealersMailingListAddress, dealerEmailAddress);
			if(this instanceof BuyerOperationsController){
				mailgunClient.deleteMailingListMember(buyersMailingListAddress, dealerEmailAddress);
			}else if (this instanceof VendorOperationsController){
				mailgunClient.deleteMailingListMember(vendorsMailingListAddress, dealerEmailAddress);
			}
		}catch(Exception e){
			logger.error("Deleting dealer email id: {} from mailing list failed with error message: {}",dealerEmailAddress, e.getMessage());
		}
	}

}
