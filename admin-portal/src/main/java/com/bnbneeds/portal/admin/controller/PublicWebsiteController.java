package com.bnbneeds.portal.admin.controller;

import java.net.ConnectException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.ConnectionUtils;
import com.bnbneeds.rest.client.ClientConfig;

@Controller
public class PublicWebsiteController extends AbstractBaseController {

	private static final Logger logger = LoggerFactory
			.getLogger(PublicWebsiteController.class);

	private static final String VIEW = "welcome";
	public static final String PING_BNBNEEDS_SERVICE_URL = "/ping-bnbneeds";

	@Layout(title = "Welcome")
	@RequestMapping(method = RequestMethod.GET, path = { "", "/",
			AuthenticationController.LOGIN_URL })
	public String welcome(@AuthenticationPrincipal BNBNeedsUser user,
			Model model) throws ConnectException {

		ClientConfig config = bnbneedsClient.getConfig();

		ConnectionUtils.checkServerConnection(config.getHost(),
				config.getPort());

		if (user == null) {
			return VIEW;
		}
		return getRedirectPath(HomePageController.HOMEPAGE_URL);

	}

	@RequestMapping(method = RequestMethod.GET, path = { PING_BNBNEEDS_SERVICE_URL })
	public String pingBNBNeedsService() throws ConnectException {

		ClientConfig config = bnbneedsClient.getConfig();

		ConnectionUtils.checkServerConnection(config.getHost(),
				config.getPort());

		return getRedirectPath(HomePageController.HOMEPAGE_URL);
	}

}
