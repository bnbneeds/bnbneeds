package com.bnbneeds.portal.admin.configuration;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.htmlcompressor.compressor.HtmlCompressor;

public class HTMLCompressionFilter implements Filter {
	private HtmlCompressor compressor;

	@Override
	public void init(FilterConfig config) throws ServletException {
		compressor = new HtmlCompressor();
		compressor.setCompressCss(true);
		compressor.setCompressJavaScript(true);
		compressor.setRemoveScriptAttributes(true);
		compressor.setRemoveSurroundingSpaces(HtmlCompressor.BLOCK_TAGS_MAX + ",textarea,label");
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {

		HttpServletResponse response = (HttpServletResponse) resp;
		CharResponseWrapper responseWrapper = new CharResponseWrapper(response);

		chain.doFilter(req, responseWrapper);

		ServletOutputStream writer = response.getOutputStream();
		String output = compressor.compress(responseWrapper.toString());
		writer.write(output.getBytes());
	}

	@Override
	public void destroy() {

	}
}
