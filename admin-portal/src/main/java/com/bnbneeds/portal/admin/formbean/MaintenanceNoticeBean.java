package com.bnbneeds.portal.admin.formbean;

import org.hibernate.validator.constraints.NotBlank;

public class MaintenanceNoticeBean {

	@NotBlank
	private String timeRange;

	@NotBlank(message = "Subject cannot be empty.")
	private String subject;
	@NotBlank(message = "Message cannot be empty.")
	private String message;

	private boolean sendEmailToAllDealers;

	public String getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}

	public String getSubject() {
		return subject;
	}

	public String getMessage() {
		return message;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSendEmailToAllDealers() {
		return sendEmailToAllDealers;
	}

	public void setSendEmailToAllDealers(boolean sendEmailToAllDealers) {
		this.sendEmailToAllDealers = sendEmailToAllDealers;
	}

}
