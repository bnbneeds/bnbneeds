package com.bnbneeds.portal.admin.formbean;

import javax.validation.constraints.NotNull;

public class PartyHallBookingBean {

	@NotNull
	private String dateCommaSeparated;

	private String bookingIdCommaSeparated;

	private Integer menuListId;

	private Integer catererId;

	private Integer memberId;

	private String description;

	public String getDateCommaSeparated() {
		return dateCommaSeparated;
	}

	public void setDateCommaSeparated(String dateCommaSeparated) {
		this.dateCommaSeparated = dateCommaSeparated;
	}

	public Integer getMenuListId() {
		return menuListId;
	}

	public void setMenuListId(Integer menuListId) {
		this.menuListId = menuListId;
	}

	public Integer getCatererId() {
		return catererId;
	}

	public void setCatererId(Integer catererId) {
		this.catererId = catererId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getBookingIdCommaSeparated() {
		return bookingIdCommaSeparated;
	}

	public void setBookingIdCommaSeparated(String bookingIdCommaSeparated) {
		this.bookingIdCommaSeparated = bookingIdCommaSeparated;
	}

	@Override
	public String toString() {
		return dateCommaSeparated;
	}
}
