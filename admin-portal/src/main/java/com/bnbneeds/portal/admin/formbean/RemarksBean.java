package com.bnbneeds.portal.admin.formbean;

import org.hibernate.validator.constraints.NotBlank;

public class RemarksBean {

	@NotBlank
	private String remarks;

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
