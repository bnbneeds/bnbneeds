package com.bnbneeds.portal.admin.formbean;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

public class Dependents {

	private List<Dependents.Dependent> dependentList;

	public List<Dependents.Dependent> getDependentList() {
		return dependentList;
	}

	public void setDependentList(List<Dependents.Dependent> dependentList) {
		this.dependentList = dependentList;
	}

	public void addDependent(Dependent dependent) {
		if (this.dependentList == null) {
			this.dependentList = new ArrayList<Dependents.Dependent>();
		}
		this.dependentList.add(dependent);
	}

	public void removeDependent(Dependent dependent) {
		if (this.dependentList == null) {
			this.dependentList = new ArrayList<Dependents.Dependent>();
		}
		this.dependentList.remove(dependent);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Dependents [");
		if (dependentList != null) {
			builder.append("dependentList=");
			builder.append(dependentList);
		}
		builder.append("]");
		return builder.toString();
	}

	public static class Dependent {

		@NotBlank
		private String name;
		@NotBlank
		private String relationshipWithMember;
		@NotBlank
		private String dateOfBirth;
		@NotBlank
		private String occupation;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getRelationshipWithMember() {
			return relationshipWithMember;
		}

		public void setRelationshipWithMember(String relationshipWithMember) {
			this.relationshipWithMember = relationshipWithMember;
		}

		public String getDateOfBirth() {
			return dateOfBirth;
		}

		public void setDateOfBirth(String dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
		}

		public String getOccupation() {
			return occupation;
		}

		public void setOccupation(String occupation) {
			this.occupation = occupation;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("Dependent [");
			if (name != null) {
				builder.append("name=");
				builder.append(name);
				builder.append(", ");
			}
			if (relationshipWithMember != null) {
				builder.append("relationshipWithMember=");
				builder.append(relationshipWithMember);
				builder.append(", ");
			}
			if (dateOfBirth != null) {
				builder.append("dateOfBirth=");
				builder.append(dateOfBirth);
				builder.append(", ");
			}
			if (occupation != null) {
				builder.append("occupation=");
				builder.append(occupation);
			}
			builder.append("]");
			return builder.toString();
		}

	}

}
