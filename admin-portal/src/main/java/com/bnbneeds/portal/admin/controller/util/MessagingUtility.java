package com.bnbneeds.portal.admin.controller.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.common.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.bnbneeds.app.model.EmailMessageParam;
import com.bnbneeds.portal.admin.formbean.EmailMessageBean;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.model.request.FileAttachment;
import com.bnbneeds.rest.client.model.request.FormElement;

public class MessagingUtility {

	public static final String MAILGUN_TEMPLATE_RECIPIENT_SALUTATION = "Hello %recipient_name%,\n\n";

	public static void emailMessageWitAttachment(MultipartFile myFile, MultipartFile inlineImage,
			EmailMessageBean emailForm, BNBNeedsClient bnbneedsClient) throws IOException {

		List<FormElement> formDataList = new ArrayList<>();

		formDataList.add(new FormElement(EmailMessageBean.FormElementName.TO.getName(), emailForm.getTo()));
		if (!StringUtils.isEmpty(emailForm.getCc()))
			formDataList.add(new FormElement(EmailMessageBean.FormElementName.CC.getName(), emailForm.getCc()));
		if (!StringUtils.isEmpty(emailForm.getBcc()))
			formDataList.add(new FormElement(EmailMessageBean.FormElementName.BCC.getName(), emailForm.getBcc()));
		if (!StringUtils.isEmpty(emailForm.getSubject()))
			formDataList
					.add(new FormElement(EmailMessageBean.FormElementName.SUBJECT.getName(), emailForm.getSubject()));
		String message = emailForm.getMessage();
		if (!StringUtils.isEmpty(message)) {
			message = MAILGUN_TEMPLATE_RECIPIENT_SALUTATION + message;
			formDataList.add(new FormElement(EmailMessageBean.FormElementName.MESSAGE.getName(),
					com.bnbneeds.portal.admin.util.StringUtils.convertLineEndingsToHtml(message)));
		}

		FileAttachment attachment = null;
		FileAttachment inlineImageAttachment = null;

		if (!inlineImage.isEmpty()) {
			inlineImageAttachment = new FileAttachment(EmailMessageBean.FormElementName.INLINEIMAGE.getName(),
					inlineImage.getOriginalFilename(), inlineImage.getContentType(), inlineImage.getInputStream());
		}
		if (!myFile.isEmpty()) {
			attachment = new FileAttachment(EmailMessageBean.FormElementName.ATTACHMENT.getName(),
					myFile.getOriginalFilename(), myFile.getContentType(), myFile.getInputStream());
		}
		bnbneedsClient.messaging().sendEmail(formDataList, inlineImageAttachment, attachment);
	}

	public static void emailMessageWithoutAttachment(EmailMessageBean emailForm, BNBNeedsClient bnbneedsClient) {
		EmailMessageParam emailParam = new EmailMessageParam();
		emailParam.setTo(emailForm.getTo());
		emailParam.setCc(emailForm.getCc());
		emailParam.setBcc(emailForm.getBcc());
		String message = emailForm.getMessage();
		if (!StringUtils.isEmpty(message)) {
			message = MAILGUN_TEMPLATE_RECIPIENT_SALUTATION + message;
			emailParam.setMessage(com.bnbneeds.portal.admin.util.StringUtils.convertLineEndingsToHtml(message));
		}
		emailParam.setSubject(emailForm.getSubject());

		bnbneedsClient.messaging().sendEmail(emailParam);

	}

}
