package com.bnbneeds.portal.admin.formbean;

public class EntityAdminApprovalFormBean {

	private String approvalStatus;

	public String getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public EntityAdminApprovalFormBean() {
		super();
	}

}
