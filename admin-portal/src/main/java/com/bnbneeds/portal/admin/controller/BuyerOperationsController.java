package com.bnbneeds.portal.admin.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.EntityApprovalParam.EntityStatus;
import com.bnbneeds.app.model.EntityInfoListResponse;
import com.bnbneeds.app.model.EntityInfoResponse;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.dealer.BuyerInfoResponse;
import com.bnbneeds.app.model.dealer.UpdateBuyerParam;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.controller.util.ReviewUtility;
import com.bnbneeds.portal.admin.formbean.UpdateBuyerInfoBean;
import com.bnbneeds.portal.admin.formbean.mapper.DealerFormBeanMapper;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = BuyerOperationsController.BASE_REQUEST_PATH)
@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
public class BuyerOperationsController extends DealerController {

	private static final Logger logger = LoggerFactory.getLogger(BuyerOperationsController.class);

	protected static final String BASE_REQUEST_PATH = "/buyers";
	private static final String UPDATE_BUYER_FORM_VIEW = "buyer-update-form";
	private static final String BUYER_PROFILE_VIEW = "buyer-profile";
	private static final String UPDATE_BUYER_FORM_ATTRIBURE = "buyerUpdateFormBean";
	private static final String BUSINESS_TYPE_LIST_ATTRIBUTE = "businessTypes";
	private static final String BUYER_INFO_ATTRIBUTE = "buyerInfo";
	static final String LIST_BUYERS_VIEW = "buyer-list";
	private static final String LIST_URI = "/list";
	private static final String BUYER_LIST_URI = BASE_REQUEST_PATH + LIST_URI;

	static void setBusinessTypes(BNBNeedsClient bnbneedsClient, Model model) {

		List<EntityInfoResponse> businessTypes = new ArrayList<>();
		try {
			EntityInfoListResponse infolistResponse = bnbneedsClient.businessTypes().listBulk(RESULT_FETCH_SIZE,
					APPROVED_ENTITY_QUERY_MAP);
			List<EntityInfoResponse> entityList = infolistResponse.getEntityInfoList();
			if (entityList != null) {
				businessTypes.addAll(entityList);
				String offset = infolistResponse.getNextOffset();
				String nextOffset = null;
				while (StringUtils.hasText(offset)) {
					infolistResponse = bnbneedsClient.businessTypes().listBulk(RESULT_FETCH_SIZE,
							APPROVED_ENTITY_QUERY_MAP, offset);
					if (infolistResponse != null) {
						entityList = infolistResponse.getEntityInfoList();
						if (entityList != null) {
							businessTypes.addAll(entityList);
						}
						nextOffset = infolistResponse.getNextOffset();
						if (nextOffset.equals(offset)) {
							break;
						} else {
							offset = nextOffset;
						}
					}
				}
			}
			model.addAttribute(BUSINESS_TYPE_LIST_ATTRIBUTE, businessTypes);
		} catch (BNBNeedsServiceException e) {
			throw e;
		}
	}

	static void setBuyerInfoAttribute(Model model, BuyerInfoResponse info) {
		String description = info.getDescription();
		if (StringUtils.hasText(description)) {
			description = StringUtils.convertLineEndingsToHtml(description);
			info.setDescription(description);
		}

		String address = info.getAddress();
		if (StringUtils.hasText(address)) {
			address = StringUtils.convertLineEndingsToHtml(address);
			info.setAddress(address);
		}

		model.addAttribute(BUYER_INFO_ATTRIBUTE, info);
	}

	@Layout(title = "Buyer Information")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/profile")
	public String getBuyerProfile(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String buyerId,
			Model model) {

		try {
			BuyerInfoResponse buyerInfoResponse = bnbneedsClient.buyers(buyerId).get();
			setBuyerInfoAttribute(model, buyerInfoResponse);
			ReviewUtility.setAverageRatingResponse(bnbneedsClient, ReviewFor.buyer, buyerId, model);
			ReviewUtility.setReviewListResponse(bnbneedsClient, ReviewFor.buyer, buyerId, null, null, model);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, LIST_BUYERS_VIEW);
		}

		return BUYER_PROFILE_VIEW;
	}

	@Layout(title = "Update Buyer Information")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/update-form")
	public String getUpdateBuyerForm(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String buyerId,
			@ModelAttribute(UPDATE_BUYER_FORM_ATTRIBURE) UpdateBuyerInfoBean form, Model model) {

		try {
			BuyerInfoResponse buyerInfo = bnbneedsClient.buyers(buyerId).get();

			DealerFormBeanMapper.mapBuyerInfoResponseToFormBean(buyerInfo, form);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, UPDATE_BUYER_FORM_VIEW);
		}

		return UPDATE_BUYER_FORM_VIEW;
	}

	@Layout(title = "Update Buyer Information")
	@RequestMapping(method = RequestMethod.POST, path = "/update")
	public String updateBuyer(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(UPDATE_BUYER_FORM_ATTRIBURE) @Valid UpdateBuyerInfoBean form, BindingResult result,
			Model model) {

		String buyerId = form.getBuyerId();

		String mobileNumber = form.getMobileNumber();

		if (!StringUtils.isPhoneNumberValid(mobileNumber)) {
			result.rejectValue("mobileNumber", null, "Mobile number is invalid.");
		}

		if (result.hasFieldErrors()) {
			return UPDATE_BUYER_FORM_VIEW;
		}

		UpdateBuyerParam param = new UpdateBuyerParam();
		DealerFormBeanMapper.mapBuyerUpdateAttributes(form, param);

		try {
			bnbneedsClient.buyers(buyerId).update(param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, UPDATE_BUYER_FORM_VIEW);
		}

		setMessage(model, "Organization has been successfully updated.");
		return getRedirectPath(BUYER_LIST_URI + "?updateSuccess");
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{id}/delete")
	public String deleteBuyer(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String buyerId,
			Model model) {

		try {
			UserAccountInfoResponse userAccount = bnbneedsClient.adminEntities()
					.getUserAccountByRelationshipId(buyerId);
			bnbneedsClient.buyers(buyerId).deactivate(true);
			removeDealerFromMailinglist(userAccount.getUsername());
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_PROFILE_VIEW);
		}
		setMessage(model, "Organization has been successfully deleted.");
		return getRedirectPath(BUYER_LIST_URI + "?deleteSuccess");
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{id}/blacklist")
	public String blacklist(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String entityid,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.BLACKLISTED);
			bnbneedsClient.buyers().updateEntityStatus(entityid, param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_PROFILE_VIEW);
		}
		setMessage(model, "Organization has been successfully blacklisted.");
		return getRedirectPath(BUYER_LIST_URI + "?blacklistSuccess");
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{id}/approve")
	public String approve(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String entityid,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.APPROVED);
			bnbneedsClient.buyers().updateEntityStatus(entityid, param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_PROFILE_VIEW);
		}
		setMessage(model, "Organization has been successfully approved.");
		return getRedirectPath(BUYER_LIST_URI + "?approveSuccess");
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{id}/disapprove")
	public String disapprove(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String entityid,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.DISAPPROVED);
			bnbneedsClient.buyers().updateEntityStatus(entityid, param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_PROFILE_VIEW);
		}
		setMessage(model, "Organization has been successfully disapproved.");
		return getRedirectPath(BUYER_LIST_URI + "?disapproveSuccess");
	}

}
