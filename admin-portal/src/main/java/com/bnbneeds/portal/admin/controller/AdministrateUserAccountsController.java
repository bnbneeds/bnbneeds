package com.bnbneeds.portal.admin.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.model.account.AccountType;
import com.bnbneeds.app.model.account.UpdateAccountStatusParam;
import com.bnbneeds.app.model.account.UpdateAccountStatusParam.AccountStatus;
import com.bnbneeds.app.model.account.CreateUserAccountParam;
import com.bnbneeds.app.model.account.UserAccountInfoListResponse;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.account.UserAccountListResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.formbean.AccountStatusBean;
import com.bnbneeds.portal.admin.formbean.AdminAccountBean;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.core.AdminAccounts;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@Layout(title = AdministrateUserAccountsController.LIST_VIEW_TITLE_NAME)
@RequestMapping(path = AdministrateUserAccountsController.BASE_REQUEST_PATH)
public class AdministrateUserAccountsController extends AbstractBaseController {

	protected static final String LIST_VIEW_TITLE_NAME = "Admin Accounts";
	static final String ACCOUNT_VIEW = "admin-account";
	static final String ADMIN_ACCOUNTS_LIST_VIEW = "admin-accounts-list";
	private static final String LIST_VIEW_TITLE_ATTRIBUTE = "listViewTitle";
	protected static final String BASE_REQUEST_PATH = "/admin-accounts";
	private static final String LIST_RESPONSE_ATTRIBUTE = "listResponse";
	private static final String CREATE_ACCOUNT_REQUEST_URI = "createAccountRequestURI";
	private static final String LIST_ACCOUNT_REQUEST_URI = "listAccountsRequestURI";
	protected static final String CREATE_ADMIN_USER_FORM_ATTRIBUTE = "createAdminUserForm";
	private static final String BASE_REQUEST_URL_ATTRIBUTE = "baseRequestURI";
	private static final String UPDATE_ENTITY_URL_ATTRIBUTE = "updateEntityURI";
	private static final String ACCOUNT_INFO_ATTRIBURE = "accountInfo";
	static final String UPDATE_ACCOUNT_FORM_VIEW = "update-admin-account";
	private static final String LIST_ACCOUNTS_REQUEST_URI = BASE_REQUEST_PATH
			+ "/list";
	private static final String CREATE_ENTITIES_REQUEST_URI = BASE_REQUEST_PATH
			+ "/create";
	private static final String ACCOUNT_INFO_REQUEST_URI = "/info";
	
	public String getViewTitle() {
		return LIST_VIEW_TITLE_NAME;
	}

	public String getCreateAccountURI() {
		return CREATE_ENTITIES_REQUEST_URI;
	}

	public String getListAccountsURI() {
		return LIST_ACCOUNTS_REQUEST_URI;
	}
	
	public String getBaseRequestUrl() {
		return BASE_REQUEST_PATH;
	}


	protected void setViewTitle(Model model, String title) {
		model.addAttribute(LIST_VIEW_TITLE_ATTRIBUTE, title);
	}
	
	protected void setBaseRequestUrl(Model model, String baseUrl) {
		model.addAttribute(BASE_REQUEST_URL_ATTRIBUTE, baseUrl);
	}
	
	protected void setUpdateEntityUrl(Model model, String updateEntityUrl) {
		model.addAttribute(UPDATE_ENTITY_URL_ATTRIBUTE, updateEntityUrl);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/list")
	@PreAuthorize("hasAuthority('SUPER_ADMIN')")
	public String list(
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			@ModelAttribute(CREATE_ADMIN_USER_FORM_ATTRIBUTE) AdminAccountBean createForm,
			Model model) {


		try {
			setModelForAccountsListView(model, nextOffset);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, ADMIN_ACCOUNTS_LIST_VIEW);
		}
		return ADMIN_ACCOUNTS_LIST_VIEW;
	}

	
	protected void setModelForAccountsListView(Model model, String nextOffset) {

		setViewTitle(model, getViewTitle());
		setBaseRequestUrl(model,getBaseRequestUrl());
		model.addAttribute(CREATE_ACCOUNT_REQUEST_URI, request.getContextPath()
				+ getCreateAccountURI());
		model.addAttribute(LIST_ACCOUNT_REQUEST_URI, request.getContextPath()
				+ getListAccountsURI());

		try {
			UserAccountInfoListResponse listResponse = null;
			AdminAccounts adminClient = bnbneedsClient.adminAccounts();
			if (StringUtils.hasText(nextOffset)) {
				listResponse = adminClient.listBulk(RESULT_FETCH_SIZE,
						nextOffset);
			} else {
				listResponse = adminClient.listBulk(RESULT_FETCH_SIZE);
			}
			model.addAttribute(LIST_RESPONSE_ATTRIBUTE, listResponse);
		} catch (BNBNeedsServiceException e) {
			throw e;
		}

	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/create")
	@PreAuthorize("hasAuthority('SUPER_ADMIN')")
	public String create(
			@ModelAttribute(CREATE_ADMIN_USER_FORM_ATTRIBUTE) AdminAccountBean form,
			Model model) {

		setViewTitle(model, LIST_VIEW_TITLE_NAME);

		CreateUserAccountParam accountParam = new CreateUserAccountParam();
		accountParam.setAccountName(form.getAccountName());
			accountParam.setMobileNumber(form.getMobileNumber());
			accountParam.setType(AccountType.ADMIN);
			accountParam.setUsername(form.getUsername());

		try {
			bnbneedsClient.adminAccounts().createAccount(accountParam);
		} catch (BNBNeedsServiceException e) {
			setModelForAccountsListView(model, null);
			return handleBadRequest(e, model, ADMIN_ACCOUNTS_LIST_VIEW);
		}

		return getRedirectPath(LIST_ACCOUNTS_REQUEST_URI + "?createSuccess");
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST } , path = "/{id}/delete")
	public String delete(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String accountId, Model model) {

		try {
			bnbneedsClient.adminAccounts().deleteAccount(accountId);
		} catch (BNBNeedsServiceException e) {
			setModelForAccountsListView(model, null);
			return handleBadRequest(e, model, ADMIN_ACCOUNTS_LIST_VIEW);
		}

		return getRedirectPath(LIST_ACCOUNTS_REQUEST_URI + "?deleteSuccess");
	}
	
	@Layout(title = "Account Details")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/info")
	public String getAccountInfo(@PathVariable("id") String accountId,Model model) {

		UserAccountInfoResponse info = bnbneedsClient.adminAccounts().getAccount(accountId);
		model.addAttribute(ACCOUNT_INFO_ATTRIBURE, info);
		return ACCOUNT_VIEW;
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/{id}/approvals")
	@PreAuthorize("hasAuthority('SUPER_ADMIN')")
	public  String updateAccountStatus(@PathVariable("id") String accountId,
			@AuthenticationPrincipal BNBNeedsUser user,
			AccountStatusBean form, Model model) {
		try {
			UpdateAccountStatusParam param = new UpdateAccountStatusParam();
			param.setStatus(AccountStatus.valueOf(form.getStatus()));
			bnbneedsClient.adminAccounts().updateAccountStatus(accountId, param);
		} catch (BNBNeedsServiceException e) {
			setModelForAccountsListView(model, null);
			return handleBadRequest(e, model, ADMIN_ACCOUNTS_LIST_VIEW);
		}
		
		return getRedirectPath(BASE_REQUEST_PATH + "/"+ accountId + ACCOUNT_INFO_REQUEST_URI + "?statusUpdateSuccess");
	}
	

}
