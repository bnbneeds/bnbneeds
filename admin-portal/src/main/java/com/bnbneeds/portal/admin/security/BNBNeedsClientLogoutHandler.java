package com.bnbneeds.portal.admin.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import com.bnbneeds.rest.client.BNBNeedsClient;

@Component
public class BNBNeedsClientLogoutHandler implements LogoutHandler {

	private static final Logger logger = LoggerFactory
			.getLogger(BNBNeedsClientLogoutHandler.class);

	@Autowired
	private BNBNeedsClient bnbneedsClient;

	public BNBNeedsClientLogoutHandler() {
	}

	@Override
	public void logout(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication) {
		logger.info("Calling bnbneedsclient.logout()");
		bnbneedsClient.logout();
	}

}
