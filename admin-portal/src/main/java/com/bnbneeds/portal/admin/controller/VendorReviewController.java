package com.bnbneeds.portal.admin.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.EntityApprovalParam.EntityStatus;
import com.bnbneeds.portal.admin.controller.util.ReviewUtility;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = "/vendors/{vendorId}/reviews")
public class VendorReviewController extends AbstractBaseController {

	private static final ReviewFor REVIEW_FOR = ReviewFor.vendor;

	private static final String VENDOR_PROFILE_VIEW = "vendor-profile";
	protected static final String BASE_REQUEST_PATH = "/vendors";
	private static final String LIST_URI = "/list";
	private static final String VENDOR_LIST_URI = BASE_REQUEST_PATH + LIST_URI ;

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{reviewId}/delete")
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String deleteReview(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId,
			@PathVariable("reviewId") String reviewId,
			Model model) {
		try {

			ReviewUtility.deleteReview(bnbneedsClient, REVIEW_FOR, vendorId,
					reviewId);
			
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_PROFILE_VIEW);
		}
		setMessage(model, "Review has been successfully deleted.");
		return getRedirectPath(VENDOR_LIST_URI + "?blacklistSuccess");
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{reviewId}/blacklist")
	public String blacklistVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId,
			@PathVariable("reviewId") String reviewId,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.BLACKLISTED);
			ReviewUtility.updateEntityStatus(bnbneedsClient, REVIEW_FOR, vendorId,
					reviewId,param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_PROFILE_VIEW);
		}
		setMessage(model, "Review has been successfully blacklisted.");
		return getRedirectPath(VENDOR_LIST_URI + "?blacklistSuccess");
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{reviewId}/approve")
	public String approveVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId,
			@PathVariable("reviewId") String reviewId,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.APPROVED);
			ReviewUtility.updateEntityStatus(bnbneedsClient, REVIEW_FOR, vendorId,
					reviewId,param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_PROFILE_VIEW);
		}
		setMessage(model, "Organization has been successfully approved.");
		return getRedirectPath(VENDOR_LIST_URI + "?approveSuccess");
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{reviewId}/disapprove")
	public String disapproveVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId,
			@PathVariable("reviewId") String reviewId,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.DISAPPROVED);
			ReviewUtility.updateEntityStatus(bnbneedsClient, REVIEW_FOR, vendorId,
					reviewId,param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_PROFILE_VIEW);
		}
		setMessage(model, "Organization has been successfully disapproved.");
		return getRedirectPath(VENDOR_LIST_URI + "?disapproveSuccess");
	}

}
