package com.bnbneeds.portal.admin.controller;

import static com.bnbneeds.portal.admin.controller.util.RequestResponseUtils.writeResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.dealer.VendorInfoListResponse;
import com.bnbneeds.app.model.dealer.VendorInfoResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.admin.controller.util.ReviewUtility;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = VendorController.BASE_REQUEST_PATH)
@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
public class VendorController extends DealerController {

	private static final Logger logger = LoggerFactory
			.getLogger(VendorController.class);

	protected static final String BASE_REQUEST_PATH = "/vendors";
	private static final String LIST_URI = "/list";
	private static final String DETAIL_URI = "/%s/details";
	private static final String VENDOR_DETAIL_REQUEST_URI = BASE_REQUEST_PATH
			+ DETAIL_URI;

	public static final String LIST_VENDORS_URI = BASE_REQUEST_PATH + LIST_URI;

	private static final String VENDOR_LIST_RESPONSE_ATTRIBUTE = "vendorListResponse";
	private static final String VENDOR_INFO_ATTRIBUTE = "vendorInfo";

	private static final String VENDOR_LIST_VIEW = "vendor-list";
	private static final String VENDOR_PROFILE_VIEW = "vendor-profile";

	private static final int VENDOR_LIST_RESULT_SIZE = 20;

	public static String getVendorDetailsRequestURI(String vendorId) {
		return String.format(VENDOR_DETAIL_REQUEST_URI, vendorId);
	}

	static void setVendorInfoAttribute(Model model, VendorInfoResponse info) {
		String description = info.getDescription();
		if (StringUtils.hasText(description)) {
			description = StringUtils.convertLineEndingsToHtml(description);
			info.setDescription(description);
		}

		String address = info.getAddress();
		if (StringUtils.hasText(address)) {
			address = StringUtils.convertLineEndingsToHtml(address);
			info.setAddress(address);
		}
		model.addAttribute(VENDOR_INFO_ATTRIBUTE, info);
	}

	private String getVendorProfileView(Model model,
			VendorInfoResponse buyerInfo) {
		setVendorInfoAttribute(model, buyerInfo);
		return VENDOR_PROFILE_VIEW;
	}

	static void setVendorListResponse(Model model,
			VendorInfoListResponse listResponse) {
		model.addAttribute(VENDOR_LIST_RESPONSE_ATTRIBUTE, listResponse);
	}

	@Layout(title = "Vendors")
	@RequestMapping(method = RequestMethod.GET, path = LIST_URI)
	public String getVendorList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "nextOffset", required = false) String nextOffset,
			Model model) {

		VendorInfoListResponse listResponse = null;
		Map<String, String> queryParams = new HashMap<String, String>();
		try {

			if (!queryParams.isEmpty()) {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = bnbneedsClient.vendors().listBulk(
							VENDOR_LIST_RESULT_SIZE, queryParams, nextOffset);
				} else {
					listResponse = bnbneedsClient.vendors().listBulk(
							VENDOR_LIST_RESULT_SIZE, queryParams);
				}
			} else {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = bnbneedsClient.vendors().listBulk(
							VENDOR_LIST_RESULT_SIZE, nextOffset);
				} else {
					listResponse = bnbneedsClient.vendors().listBulk(
							VENDOR_LIST_RESULT_SIZE);
				}
			}
			setVendorListResponse(model, listResponse);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_LIST_VIEW);
		}

		return VENDOR_LIST_VIEW;
	}

	@Layout(title = "Vendor Information")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/details")
	public String getVendorInfo(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String vendorId,
			@RequestParam(name = "reviewId", required = false) String reviewId,
			Model model) {

		try {
			VendorInfoResponse vendor = bnbneedsClient.vendors(vendorId).get();
			ReviewUtility.setAverageRatingResponse(bnbneedsClient,
					ReviewFor.vendor, vendorId, model);
			ReviewUtility.setReviewListResponse(bnbneedsClient,
					ReviewFor.vendor, vendorId, reviewId, null, null, model);
			return getVendorProfileView(model, vendor);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_PROFILE_VIEW);
		}
	}

	@Layout(title = "Vendor Information")
	@RequestMapping(method = RequestMethod.GET, path = "/details")
	public String getDetailsForReviewId(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "reviewId") String reviewId, Model model) {

		try {
			String vendorId = ReviewUtility.getEntityIdForReviewId(
					bnbneedsClient, ReviewFor.vendor, reviewId);
			if (!StringUtils.hasText(vendorId)) {
				return getRedirectPath(LIST_VENDORS_URI + "?reviewId="
						+ reviewId);
			}
			return getRedirectPath(getVendorDetailsURI(vendorId)
					+ "?reviews=true&reviewId=" + reviewId);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_PROFILE_VIEW);
		}

	}

	private String getVendorDetailsURI(String buyerId) {
		return String.format(VENDOR_DETAIL_REQUEST_URI, buyerId);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/detail-as-text")
	@ResponseBody
	public synchronized void writeVendorInfo(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String vendorId, HttpServletResponse response)
			throws IOException {

		try {
			String vendor = bnbneedsClient.vendors(vendorId).getAsString();
			writeResponse(200, MediaType.TEXT_PLAIN_VALUE, vendor, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

}
