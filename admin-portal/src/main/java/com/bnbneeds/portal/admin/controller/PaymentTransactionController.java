package com.bnbneeds.portal.admin.controller;


import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.model.DataObjectInfoListResponse;
import com.bnbneeds.app.model.dealer.PaymentTransactionInfoResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.core.PaymentTransactions;
import com.bnbneeds.rest.client.core.PaymentTransactions.PaymentTransactionOf;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

public abstract class PaymentTransactionController extends AbstractBaseController {

	private static final String PAYMENT_TRANSACTION_LIST_VIEW = "payment-transaction-list";
	private static final String LIST_RESPONSE_ATTRIBUTE = "listResponse";
	private static final String LIST_SUBSCRIPTION_URI = "/list";
	protected static final String LIST_VIEW_TITLE = "Payment Transactions";
	protected static final String PAYMENT_TRANSACTION_OF_DEALER_TYPE = "paymentTransactionOfDealerType";

	public abstract PaymentTransactionOf getPaymentTransactionOf();
	

	private PaymentTransactions getClientHandle(String dealerId) {
		PaymentTransactionOf dealerType = getPaymentTransactionOf();
		switch (dealerType) {
		case buyer:
			return bnbneedsClient.buyerPaymentTransactions(dealerId);
		case vendor:
			return bnbneedsClient.vendorPaymentTransactions(dealerId);
		default:
			return null;
		}
	}

	private void setModelForSubscriptionPlansListView(String dealerId, String nextOffset, Model model) {

		PaymentTransactions paymentTransactionsClient = getClientHandle(dealerId);
		model.addAttribute(PAYMENT_TRANSACTION_OF_DEALER_TYPE, getPaymentTransactionOf().name());
		try {
			DataObjectInfoListResponse<PaymentTransactionInfoResponse> listResponse = null;

			if (StringUtils.hasText(nextOffset)) {
				listResponse = paymentTransactionsClient.list(RESULT_FETCH_SIZE, nextOffset);
			} else {
				listResponse = paymentTransactionsClient.list(RESULT_FETCH_SIZE);
			}
			model.addAttribute(LIST_RESPONSE_ATTRIBUTE, listResponse);
			
		} catch (BNBNeedsServiceException e) {
			throw e;
		}
	}

	@Layout(title = LIST_VIEW_TITLE)
	@RequestMapping(method = RequestMethod.GET, path = LIST_SUBSCRIPTION_URI)
	public String listBiddingSubscriptions(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String dealerId,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset, Model model) {
		try {
			setModelForSubscriptionPlansListView(dealerId, nextOffset, model);
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, PAYMENT_TRANSACTION_LIST_VIEW);
		}
		return PAYMENT_TRANSACTION_LIST_VIEW;
	}

}
