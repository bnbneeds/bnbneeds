package com.bnbneeds.portal.admin.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.model.AddRemarksParam;
import com.bnbneeds.app.model.report.AbuseReportInfoListResponse;
import com.bnbneeds.app.model.report.AbuseReportInfoResponse;
import com.bnbneeds.app.model.report.CloseReportParam;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.formbean.RemarksBean;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.core.AbuseReports;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@Layout(title = AbuseReportsController.LIST_VIEW_TITLE_NAME)
@RequestMapping(path = AbuseReportsController.BASE_REQUEST_PATH)
public class AbuseReportsController extends AbstractBaseController {

	protected static final String LIST_VIEW_TITLE_NAME = "Abuse Reports";
	static final String ABUSE_REPORT_VIEW = "abuse-report";
	static final String ABUSE_REPORTS_LIST_VIEW = "abuse-reports-list";
	private static final String LIST_VIEW_TITLE_ATTRIBUTE = "listViewTitle";
	protected static final String BASE_REQUEST_PATH = "/abuse-reports";
	private static final String LIST_RESPONSE_ATTRIBUTE = "listResponse";
	private static final String LIST_ABUSE_REPORTS_REQUEST_ATTRIBUTE = "listAbuseReportsRequestURI";
	private static final String BASE_REQUEST_URL_ATTRIBUTE = "baseRequestURI";
	private static final String LIST_ABUSE_REPORTS_REQUEST_URI = BASE_REQUEST_PATH + "/list";
	private static final String ABUSE_REPORT_INFO_ATTRIBURE = "abuseReportInfo";
	protected static final String ADD_REMARKS_FORM_ATTRIBUTE = "addRemarksForm";

	public String getViewTitle() {
		return LIST_VIEW_TITLE_NAME;
	}

	public String getListAbuseReportsURI() {
		return LIST_ABUSE_REPORTS_REQUEST_URI;
	}

	public String getBaseRequestUrl() {
		return BASE_REQUEST_PATH;
	}

	protected void setViewTitle(Model model, String title) {
		model.addAttribute(LIST_VIEW_TITLE_ATTRIBUTE, title);
	}

	protected void setBaseRequestUrl(Model model, String baseUrl) {
		model.addAttribute(BASE_REQUEST_URL_ATTRIBUTE, baseUrl);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/list")
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String list(@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			@ModelAttribute(ADD_REMARKS_FORM_ATTRIBUTE) RemarksBean form, Model model) {
		try {
			setModelForAbuseReportsListView(model, nextOffset);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, ABUSE_REPORTS_LIST_VIEW);
		}
		return ABUSE_REPORTS_LIST_VIEW;
	}

	protected void setModelForAbuseReportsListView(Model model, String nextOffset) {

		setViewTitle(model, getViewTitle());
		setBaseRequestUrl(model, getBaseRequestUrl());

		model.addAttribute(LIST_ABUSE_REPORTS_REQUEST_ATTRIBUTE, request.getContextPath() + getListAbuseReportsURI());

		try {
			AbuseReportInfoListResponse listResponse = null;
			AbuseReports abuseReportsClient = bnbneedsClient.abuseReports();
			if (StringUtils.hasText(nextOffset)) {
				listResponse = abuseReportsClient.listBulk(RESULT_FETCH_SIZE, null, nextOffset);
			} else {
				listResponse = abuseReportsClient.listBulk(RESULT_FETCH_SIZE);
			}

			if (listResponse != null && !CollectionUtils.isEmpty(listResponse.getAbuseReportList())) {
				for (AbuseReportInfoResponse abuseReport : listResponse.getAbuseReportList()) {
					abuseReport.setEntityType(StringUtils.getTypeName(abuseReport.getEntityId()).toUpperCase());
				}
			}
			model.addAttribute(LIST_RESPONSE_ATTRIBUTE, listResponse);
		} catch (BNBNeedsServiceException e) {
			throw e;
		}
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{id}/close")
	public String close(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String accountId, Model model) {

		try {
			CloseReportParam param = new CloseReportParam();
			bnbneedsClient.abuseReports().close(accountId, param);
		} catch (BNBNeedsServiceException e) {
			setModelForAbuseReportsListView(model, null);
			return handleBadRequest(e, model, ABUSE_REPORTS_LIST_VIEW);
		}

		return getRedirectPath(LIST_ABUSE_REPORTS_REQUEST_URI + "?deleteSuccess");
	}

	@Layout(title = "Abuse Report Details")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/info")
	public String getAccountInfo(@PathVariable("id") String abuseReportId, Model model) {

		AbuseReportInfoResponse info = bnbneedsClient.abuseReports().get(abuseReportId);
		model.addAttribute(ABUSE_REPORT_INFO_ATTRIBURE, info);
		return ABUSE_REPORT_VIEW;
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{id}/assign")
	public String assignToSelf(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String accountId,
			Model model) {

		try {
			AddRemarksParam param = new AddRemarksParam();
			bnbneedsClient.abuseReports().assignToSelf(accountId, param);
		} catch (BNBNeedsServiceException e) {
			setModelForAbuseReportsListView(model, null);
			return handleBadRequest(e, model, ABUSE_REPORTS_LIST_VIEW);
		}

		return getRedirectPath(LIST_ABUSE_REPORTS_REQUEST_URI + "?deleteSuccess");
	}

	@RequestMapping(method = RequestMethod.POST, path = "{id}/add-remarks")
	@PreAuthorize("hasAnyAuthority('SUPER_ADMIN','ADMIN')")
	public String addRemarks(@ModelAttribute(ADD_REMARKS_FORM_ATTRIBUTE) RemarksBean form,
			@PathVariable("id") String abuseReportId, Model model) {

		setViewTitle(model, LIST_VIEW_TITLE_NAME);
		AddRemarksParam param = new AddRemarksParam();
		param.setRemarks(form.getRemarks());
		try {
			bnbneedsClient.abuseReports().addRemarks(abuseReportId, param);
		} catch (BNBNeedsServiceException e) {
			setModelForAbuseReportsListView(model, null);
			return handleBadRequest(e, model, ABUSE_REPORTS_LIST_VIEW);
		}

		return getRedirectPath(LIST_ABUSE_REPORTS_REQUEST_URI + "?addRemarksSuccess");
	}

	public static String getAbuseReportsURI() {
		return String.format(LIST_ABUSE_REPORTS_REQUEST_URI);
	}

}
