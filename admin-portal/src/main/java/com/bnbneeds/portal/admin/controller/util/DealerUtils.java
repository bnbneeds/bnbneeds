package com.bnbneeds.portal.admin.controller.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.portal.admin.exception.HasNoDealershipException;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.security.Role;
import com.bnbneeds.portal.admin.util.StringUtils;

public class DealerUtils {
	private static final Logger logger = LoggerFactory
			.getLogger(DealerUtils.class);

	public static void checkHasDealership(BNBNeedsUser user) {
		String role = user.getRole();
		String dealershipId = user.getRelationshipId();
		if (Role.VENDOR.equals(role) || Role.BUYER.equals(role)) {
			if (!StringUtils.hasText(dealershipId)) {
				logger.error("User has no dealership.");
				throw new HasNoDealershipException(
						"You are not registered as a dealer. Please register you organiztion and try again.");
			}
		}
	}

	public static String checkAndReturnRelationshipId(BNBNeedsUser user) {
		checkHasDealership(user);
		return user.getRelationshipId();
	}
}
