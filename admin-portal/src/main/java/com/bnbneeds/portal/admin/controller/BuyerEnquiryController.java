package com.bnbneeds.portal.admin.controller;

import static com.bnbneeds.portal.admin.controller.util.RequestResponseUtils.writeResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.dealer.EnquiryInfoListResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.admin.formbean.BuyerEnquiryListRequestBean;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = BuyerEnquiryController.BASE_REQUEST_PATH)
public class BuyerEnquiryController extends DealerController {

	protected static final String BASE_REQUEST_PATH = "/buyers/product-enquiries";
	private static final int FETCH_SIZE = 20;
	private static final String BUYER_ENQUIRY_LIST_VIEW = "buyer-enquiry-list";
	private static final String BUYER_ENQUIRY_LIST_RESPONSE_ATTRIBUTE = "buyerEnquiryListResponse";
	private static final String ENQUIRY_LIST_REQUEST_FORM_ATTRIBUTE = "enquiryListRequestForm";
	public static final String DATE_FORMAT = "dd-MMM-yyyy";

	public static String getEnquiryListURI() {
		return BASE_REQUEST_PATH;
	}

	public static String getEnquiryListURI(String enquiryDate) {
		return BASE_REQUEST_PATH
				+ String.format("?enquiryDate=%s", enquiryDate);
	}

	private static void setEnquiryListResponse(Model model,
			EnquiryInfoListResponse listResponse) {
		model.addAttribute(BUYER_ENQUIRY_LIST_RESPONSE_ATTRIBUTE, listResponse);
	}

	@Layout(title = "Product Enquiries")
	@RequestMapping(method = RequestMethod.GET, path = "/list-request-form")
	public String getEnquiryListRequestForm(
			@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(ENQUIRY_LIST_REQUEST_FORM_ATTRIBUTE) BuyerEnquiryListRequestBean form,
			BindingResult result, Model model) {

		return BUYER_ENQUIRY_LIST_VIEW;

	}

	@Layout(title = "Product Enquiries")
	@RequestMapping(method = RequestMethod.GET)
	public String getEnquiries(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			@ModelAttribute(ENQUIRY_LIST_REQUEST_FORM_ATTRIBUTE) BuyerEnquiryListRequestBean form,
			BindingResult result, Model model) {

		Map<String, String> queryMap = new HashMap<String, String>();
		if (StringUtils.hasText(form.getEnquiryDate())) {
			if (!StringUtils.isDateFormatValid(form.getEnquiryDate(),
					DATE_FORMAT)) {
				result.rejectValue("enquiryDate", null,
						"Enquiry date format is invalid.");
			}
			queryMap.put("enquiryDate", form.getEnquiryDate());
		} else {
			if (StringUtils.hasText(form.getEnquiryDateRange())) {
				String[] dateRangeArray = getRequestStartAndEndDate(form
						.getEnquiryDateRange());
				String startDate = dateRangeArray[0].trim();
				String endDate = dateRangeArray[1].trim();

				if (!StringUtils.isDateFormatValid(startDate, DATE_FORMAT)) {
					result.rejectValue("enquiryDateRange", null,
							"Start date format is invalid.");
				}

				if (!StringUtils.isDateFormatValid(endDate, DATE_FORMAT)) {
					result.rejectValue("enquiryDateRange", null,
							"End date format is invalid.");
				}
				queryMap.put("startEnquiryDate", startDate);
				queryMap.put("endEnquiryDate", endDate);
			}
		}

		if (StringUtils.hasText(form.getBuyer())) {
			String buyer = form.getBuyer();
			if (form.getBuyer().contains("|")) {
				String[] nameAndIdArr = buyer.split("\\|");
				queryMap.put("buyerId", nameAndIdArr[1].trim());
				setSelectedDealer(model, buyer);
			}
		}

		try {
			if (!result.hasErrors()) {
				EnquiryInfoListResponse listResponse = null;
				if (!queryMap.isEmpty()) {
					if (StringUtils.hasText(nextOffset)) {
						listResponse = bnbneedsClient.adminEntities()
								.getAllEnquiries(FETCH_SIZE, queryMap, nextOffset);
					} else {
						listResponse = bnbneedsClient.adminEntities()
								.getAllEnquiries(FETCH_SIZE, queryMap);
					}
				} else {
					if (StringUtils.hasText(nextOffset)) {
						listResponse = bnbneedsClient.adminEntities()
								.getAllEnquiries(FETCH_SIZE, nextOffset);
					} else {
						listResponse = bnbneedsClient.adminEntities()
								.getAllEnquiries(FETCH_SIZE);
					}

				}
				setEnquiryListResponse(model, listResponse);
			}
			return BUYER_ENQUIRY_LIST_VIEW;
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_ENQUIRY_LIST_VIEW);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/buyers")
	@ResponseBody
	public synchronized void writeAssociatedBuyerList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "enquiryDate", required = false) String enquiryDate,
			@RequestParam(name = "enquiryDateRange", required = false) String enquiryDateRange,
			HttpServletResponse response) throws IOException,
			InterruptedException {

		String associatedBuyerListResponse = null;

		try {
			if (StringUtils.hasText(enquiryDate)) {
				if (!StringUtils.isDateFormatValid(enquiryDate, DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST,
							MediaType.TEXT_PLAIN_VALUE,
							"Request date format is invalid.", response);
					return;
				}
				associatedBuyerListResponse = bnbneedsClient.adminEntities()
						.getAssociatedBuyerListOfEnquiriesAsString(enquiryDate);
			} else {
				// Date range
				String[] dateRangeArray = getRequestStartAndEndDate(enquiryDateRange);
				String startEnquiryDate = dateRangeArray[0].trim();
				String endEnquiryDate = dateRangeArray[1].trim();

				if (!StringUtils.isDateFormatValid(startEnquiryDate,
						DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST,
							MediaType.TEXT_PLAIN_VALUE,
							"Start date format is invalid.", response);
					return;
				}

				if (!StringUtils.isDateFormatValid(endEnquiryDate, DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST,
							MediaType.TEXT_PLAIN_VALUE,
							"End date format is invalid.", response);
					return;
				}

				associatedBuyerListResponse = bnbneedsClient.adminEntities()
						.getAssociatedBuyerListOfEnquiriesByDateRangeAsString(
								startEnquiryDate, endEnquiryDate);

			}
			writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, associatedBuyerListResponse,
					response);

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}
	
	
}
