package com.bnbneeds.portal.admin.formbean;

public class CreateVendorBiddingSubscriptionPlanBean extends BiddingSubscriptionPlanBean  {

	private Integer numberOfTendersAllowedToAccess;

	public Integer getNumberOfTendersAllowedToAccess() {
		return numberOfTendersAllowedToAccess;
	}

	public void setNumberOfTendersAllowedToAccess(Integer numberOfTendersAllowedToAccess) {
		this.numberOfTendersAllowedToAccess = numberOfTendersAllowedToAccess;
	}


}
