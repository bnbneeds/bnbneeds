package com.bnbneeds.portal.admin.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.portal.admin.util.StringUtils;

public class BNBNeedsUser extends User {

	private static final long serialVersionUID = -4044095031396263507L;
	private String accountName;
	private String relationshipId;
	private String role;

	public BNBNeedsUser(String username, String password,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
	}

	public BNBNeedsUser(UserAccountInfoResponse userAccount) {
		this(userAccount.getUsername(), "non-empty-value",
				getRoles(userAccount));
		this.relationshipId = userAccount.getRelationshipId();
		this.accountName = userAccount.getAccountName();
		this.role = userAccount.getRole();
	}

	private static List<? extends GrantedAuthority> getRoles(
			UserAccountInfoResponse userAccount) {
		String role = userAccount.getRole();
		SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role);
		List<SimpleGrantedAuthority> list = new ArrayList<SimpleGrantedAuthority>();
		list.add(authority);
		return list;
	}

	public String getRelationshipId() {
		return relationshipId;
	}

	public void setRelationshipId(String relationshipId) {
		this.relationshipId = relationshipId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getRole() {
		return role;
	}

	public boolean isAdmin() {
		return Role.ADMIN.equals(getRole());
	}

	public boolean isSuperAdmin() {
		return Role.SUPER_ADMIN.equals(getRole());
	}

	public boolean hasRelationshipId() {
		return StringUtils.hasText(relationshipId);
	}

}
