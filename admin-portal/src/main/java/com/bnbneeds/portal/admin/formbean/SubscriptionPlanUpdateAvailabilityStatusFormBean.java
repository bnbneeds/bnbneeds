package com.bnbneeds.portal.admin.formbean;

public class SubscriptionPlanUpdateAvailabilityStatusFormBean {

	private String availabilityStatus;
	
	public SubscriptionPlanUpdateAvailabilityStatusFormBean() {
		super();
	}

	public String getAvailabilityStatus() {
		return availabilityStatus;
	}

	public void setAvailabilityStatus(String availabilityStatus) {
		this.availabilityStatus = availabilityStatus;
	}

}
