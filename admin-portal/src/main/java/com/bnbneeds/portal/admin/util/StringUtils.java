package com.bnbneeds.portal.admin.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils extends org.springframework.util.StringUtils {

	public static final String REGEX_PASSWORD = "^.*(?=.{8,})(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!]).*$";
	public static final String REGEX_EMAIL_ADDRESS = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
	public static final String REGEX_DATE = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";
	public static final String VALID_IMAGE_FILE_EXTENSION_PATTERN = "(.+(\\.(?i)(jpg|png))$)";
	public static final String REGEX_PAN = "[A-Z]{5}\\d{4}[A-Z]{1}";
	public static final String REGEX_SERVICE_TAX_NUMBER = "[A-Z]{5}\\d{4}[A-Z]{1}S(D|T)\\d{3}";
	public static final String REGEX_VAT_NUMBER = "\\d{11}";
	private static final Pattern TYPE_PATTERN = Pattern
			.compile("urn\\:bnb\\:([^\\:]+)");
	
	private static final String YOUTUBE_VIDEO_EMBED_URL_FORMAT = "https://www.youtube.com/embed/%1$s?rel=0&modestbranding=1&autohide=1&showinfo=0&loop=1&playlist=%2$s";
	private static final String YOUTUBE_VIDEO_EMBED_URL_WITH_AUTOPLAY_FORMAT = "https://www.youtube.com/embed/%1$s?rel=0&autoplay=1&modestbranding=1&autohide=1&showinfo=0&loop=1&playlist=%2$s";
	
	
	public static boolean isPhoneNumberValid(String phoneNo) {

		if (phoneNo == null)
			return false;
		// validate phone numbers of format "1234567890"
		else if (phoneNo.matches("\\d{10}"))
			return true;
		// validating phone number with -, . or spaces
		else if (phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}"))
			return true;
		// validating phone number where area code is in braces ()
		else if (phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}"))
			return true;
		// return false if nothing matches the input
		else
			return false;

	}

	public static String convertLineEndingsToHtml(String text) {
		String regex = "(\r\n|\n)";
		return text.replaceAll(regex, "<br />");
	}
	
	public static String getTypeName(String id) {
		Matcher m = TYPE_PATTERN.matcher(id);
		if (m.find()) {
			return m.group(1);
		}
		return null;
	}
	
	public static String getYoutubeEmbedURL(String videoId) {
		return String.format(YOUTUBE_VIDEO_EMBED_URL_FORMAT, videoId, videoId);
	}

	public static String getYoutubeEmbedURLWithAutoPlay(String videoId) {
		return String.format(YOUTUBE_VIDEO_EMBED_URL_WITH_AUTOPLAY_FORMAT,
				videoId, videoId);
	}

	public static String getYoutubeEmbedURLFromWatchURL(String watchURL) {
		String videoID = getYoutubeVideoId(watchURL);
		if (hasText(videoID)) {
			return getYoutubeEmbedURL(videoID);
		}
		return null;
	}
	
	public static String getYoutubeVideoId(String watchURL) {

		String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

		Pattern compiledPattern = Pattern.compile(pattern);
		Matcher matcher = compiledPattern.matcher(watchURL);

		if (matcher.find()) {
			return matcher.group();
		}
		return null;
	}

	public static String splitCamelCaseWordIntoSpaceSeparatedWords(String word) {
		StringBuffer buffer = new StringBuffer();
		String[] splitArray = word
				.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
		for (int i = 0; i < splitArray.length; i++) {
			if (i > 0) {
				buffer.append(" ");
			}
			buffer.append(splitArray[i]);
		}

		return buffer.toString();
	}
	
	public static boolean isDateFormatValid(String text, String pattern) {

		if (StringUtils.isEmpty(text)) {
			return false;
		}
		boolean valid = false;
		SimpleDateFormat f = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			date = f.parse(text);
		} catch (ParseException e) {
			valid = false;
		}
		if (date != null) {
			valid = f.format(date).equals(text);
		}
		return valid;
	}

	public static String getDateTimestamp(SimpleDateFormat dateFormat, Date date) {
		return dateFormat.format(date);
	}

	public static String getDateTimestamp(String pattern, Date date) {
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		return getDateTimestamp(df, date);
	}

}
