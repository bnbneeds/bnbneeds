package com.bnbneeds.portal.admin.formbean;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

public class CommitteeMemberBean {

	@NotBlank
	private String name;
	@NotBlank
	private String designation;
	@NotNull
	@Min(value = 1)
	private Integer rank;
	private MultipartFile file;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CommitteeMemberBean [");
		if (name != null) {
			builder.append("name=");
			builder.append(name);
			builder.append(", ");
		}
		if (designation != null) {
			builder.append("designation=");
			builder.append(designation);
			builder.append(", ");
		}
		builder.append("rank=");
		builder.append(rank);
		builder.append(", ");
		if (file != null) {
			builder.append("file=");
			builder.append(file);
		}
		builder.append("]");
		return builder.toString();
	}

}
