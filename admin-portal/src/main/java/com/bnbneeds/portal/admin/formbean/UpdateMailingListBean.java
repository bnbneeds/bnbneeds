package com.bnbneeds.portal.admin.formbean;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class UpdateMailingListBean {
	
	public static enum DealerType {
		ALL,
		VENDOR,
		BUYER;
	}
	
	@NotBlank
	private String mailingList;
	
	@NotNull
	private List<DealerMailBean> allDealerMailList;
	
	@NotNull
	private List<String> dealerMailList;
	
	private String nextOffset;
	
	private String dealerType;
	
	private String currentOffset;
	
	public String getCurrentOffset() {
		return currentOffset;
	}

	public void setCurrentOffset(String currentOffset) {
		this.currentOffset = currentOffset;
	}

	public String getDealerType() {
		return dealerType;
	}

	public void setDealerType(String dealerType) {
		this.dealerType = dealerType;
	}

	public List<DealerMailBean> getAllDealerMailList() {
		if(allDealerMailList == null){
			return allDealerMailList = new ArrayList<DealerMailBean>();
		}
		return allDealerMailList;
	}
	
	public List<String> getAllDealerMails() {
		List<String> mailIds = new ArrayList<>();
		for(DealerMailBean mailBean : allDealerMailList)
			mailIds.add(mailBean.getDealerEmailId());
		
		return mailIds;
	}
	
	public void setAllDealerMailList(List<DealerMailBean> allDealerMailList) {
		this.allDealerMailList = allDealerMailList;
	}
	public String getMailingList() {
		return mailingList;
	}
	public void setMailingList(String mailingList) {
		this.mailingList = mailingList;
	}
	
	public List<String> getDealerMailList() {
		if(dealerMailList == null){
			return dealerMailList = new ArrayList<String>();
		}
		return dealerMailList;
	}
	
	public void setDealerMailList(List<String> dealerMailList) {
		this.dealerMailList = dealerMailList;
	}
	
	public String getNextOffset() {
		return nextOffset;
	}
	public void setNextOffset(String nextOffset) {
		this.nextOffset = nextOffset;
	}
	
	

}
