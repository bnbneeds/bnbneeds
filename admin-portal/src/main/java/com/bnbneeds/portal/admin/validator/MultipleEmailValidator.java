package com.bnbneeds.portal.admin.validator;

import java.util.regex.Matcher;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.bnbneeds.portal.admin.util.StringUtils;

public class MultipleEmailValidator implements ConstraintValidator<MultipleEmail, String> {

	 private static java.util.regex.Pattern pattern;
	 
    @Override
    public void initialize(MultipleEmail parameters) {

        pattern = java.util.regex.Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",
                java.util.regex.Pattern.CASE_INSENSITIVE);
    
    }

    @Override
    public boolean isValid(String emailIds, ConstraintValidatorContext constraintContext) {

    	if(StringUtils.isEmpty(emailIds)){
    		return true;
    	}

        for (String email : emailIds.split(",")) {
        	
            Matcher m = pattern.matcher(email.trim());
            if (!m.matches())
                return false;
        }
        return true;
    
    }
}
  