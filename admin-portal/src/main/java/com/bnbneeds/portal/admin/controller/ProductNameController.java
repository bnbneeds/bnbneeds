package com.bnbneeds.portal.admin.controller;

import java.util.Iterator;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.EntityApprovalParam.EntityStatus;
import com.bnbneeds.app.model.product.CreateProductNameBulkParam;
import com.bnbneeds.app.model.product.CreateProductNameParam;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.formbean.EntityBean;
import com.bnbneeds.portal.admin.formbean.EntityListBean;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.core.AbstractMasterEntityTypes.EntityType;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@Layout(title = ProductNameController.LIST_VIEW_TITLE_NAME)
@RequestMapping(path = ProductNameController.BASE_REQUEST_PATH)
public class ProductNameController extends MasterEntityController {

	protected static final String LIST_VIEW_TITLE_NAME = "Product Names";
	protected static final String BASE_REQUEST_PATH = "/product-names";
	public static final String LIST_ENTITIES_REQUEST_URI = BASE_REQUEST_PATH
			+ "/list";
	private static final String CREATE_ENTITIES_REQUEST_URI = BASE_REQUEST_PATH
			+ "/create";
	public static final String LIST_ENTITIES_REQUEST_URI_FOR_AJAX = BASE_REQUEST_PATH
			+ "/listForAjax";

	@RequestMapping(method = RequestMethod.POST, path = "/create")
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String create(
			@ModelAttribute(ENTITY_LIST_FORM_ATTRIBUTE) EntityListBean form,
			Model model) {

		setViewTitle(model, LIST_VIEW_TITLE_NAME);
		CreateProductNameBulkParam param = new CreateProductNameBulkParam();
		List<EntityBean> entities = form.getEntities();
		for (Iterator<EntityBean> iterator = entities.iterator(); iterator
				.hasNext();) {
			EntityBean entityBean = iterator.next();
			CreateProductNameParam eachParam = new CreateProductNameParam();
			eachParam.setName(entityBean.getName());
			String desc = entityBean.getDescription();
			if (StringUtils.hasText(desc)) {
				eachParam.setDescription(desc);
			}
			param.add(eachParam);
		}
		try {
			bnbneedsClient.productNames().create(param);
		} catch (BNBNeedsServiceException e) {
			setModelForEntityListView(model, null);
			return handleBadRequest(e, model, ENTITY_LIST_VIEW);
		}

		return getRedirectPath(LIST_ENTITIES_REQUEST_URI + "?createSuccess");
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{id}/delete")
	public String delete(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String productNameId, Model model) {

		try {
			bnbneedsClient.productNames().delete(productNameId);
		} catch (BNBNeedsServiceException e) {
			setModelForEntityListView(model, null);
			return handleBadRequest(e, model, ENTITY_LIST_VIEW);
		}

		return getRedirectPath(LIST_ENTITIES_REQUEST_URI + "?deleteSuccess");
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{id}/disapprove")
	public String disapprove(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String entityid, Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.DISAPPROVED);
			bnbneedsClient.productTypes().updateEntityStatus(entityid, param);
		} catch (BNBNeedsServiceException e) {
			setModelForEntityListView(model, null);
			return handleBadRequest(e, model, ENTITY_LIST_VIEW);
		}

		return getRedirectPath(LIST_ENTITIES_REQUEST_URI + "?disapproveSuccess");
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{id}/approve")
	public String approve(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String entityid, Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.APPROVED);
			bnbneedsClient.productTypes().updateEntityStatus(entityid, param);
		} catch (BNBNeedsServiceException e) {
			setModelForEntityListView(model, null);
			return handleBadRequest(e, model, ENTITY_LIST_VIEW);
		}

		return getRedirectPath(LIST_ENTITIES_REQUEST_URI + "?approveSuccess");
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{id}/blacklist")
	public String blacklist(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String entityid, Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.BLACKLISTED);
			bnbneedsClient.productTypes().updateEntityStatus(entityid, param);
		} catch (BNBNeedsServiceException e) {
			setModelForEntityListView(model, null);
			return handleBadRequest(e, model, ENTITY_LIST_VIEW);
		}

		return getRedirectPath(LIST_ENTITIES_REQUEST_URI + "?blacklistSuccess");
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.product_name;
	}

	@Override
	public String getViewTitle() {
		return LIST_VIEW_TITLE_NAME;
	}

	@Override
	public String getCreateEntityURI() {
		return CREATE_ENTITIES_REQUEST_URI;
	}

	@Override
	public String getListEntityURI() {
		return LIST_ENTITIES_REQUEST_URI;
	}

	@Override
	public String getBaseRequestUrl() {
		return BASE_REQUEST_PATH;
	}

}
