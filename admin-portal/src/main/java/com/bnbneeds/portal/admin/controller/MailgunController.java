package com.bnbneeds.portal.admin.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.model.account.UserAccountInfoListResponse;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.formbean.DealerMailBean;
import com.bnbneeds.portal.admin.formbean.UpdateMailingListBean;
import com.bnbneeds.portal.admin.formbean.UpdateMailingListBean.DealerType;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.core.DealerAccounts;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.mailgun.rest.client.MailgunClient;
import com.mailgun.rest.client.exceptions.MailgunServiceException;
import com.mailgun.rest.client.model.AddMailingListMemberParam;
import com.mailgun.rest.client.model.MailingListMemberParam;
import com.mailgun.rest.client.model.MailingListMembersResponse;
import com.mailgun.rest.client.model.MailingListsResponse;

@Controller
@RequestMapping(path = "/mailgun")
@PreAuthorize("hasAuthority('SUPER_ADMIN')")
public class MailgunController extends AbstractBaseController {

	private static final Logger logger = LoggerFactory.getLogger(MailgunController.class);

	private static final String MAILING_LIST_RESPONSE = "mailingListsResponse";
	private static final String UPDATE_MAILINGLIST_FORM_ATTRIBURE = "updateMailingListForm";
	private static final String LIST_MAILING_LISTS_FORM_VIEW = "mailingList-list";
	private static final String LIST_MAILING_MEMBERS_ADD_FORM_VIEW = "mailingList-add-members";
	private static final String LIST_MAILING_MEMBERS_REMOVE_FORM_VIEW = "mailingList-remove-members";
	private static final String LIST_MAILING_MEMBERS_URL = "/mailgun/list-mailinglist-form";

	@Autowired
	protected MailgunClient mailgunClient;

	@Layout(title = "Update Mailing lists")
	@RequestMapping(method = RequestMethod.GET, path = "/list-mailinglist-form")
	public String listMailingLists(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			@ModelAttribute(UPDATE_MAILINGLIST_FORM_ATTRIBURE) UpdateMailingListBean mailingListForm, Model model) {

		// get mailinglists
		MailingListsResponse mailingListsResponse = null;
		try {
			mailingListsResponse = mailgunClient.getMailingLists();
		} catch (MailgunServiceException ex) {
			logger.error("MailgunServiceException  occurred", ex);
			setErrorMessage(model, "Error retreiving mailing list: " + ex.getMessage());
			return LIST_MAILING_LISTS_FORM_VIEW;
		}
		model.addAttribute(MAILING_LIST_RESPONSE, mailingListsResponse);

		return LIST_MAILING_LISTS_FORM_VIEW;
	}

	@Layout(title = "Update Mailing lists")
	@RequestMapping(method = RequestMethod.GET, path = "/update-mailinglist-form")
	public String viewMailingListDealers(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			@ModelAttribute(UPDATE_MAILINGLIST_FORM_ATTRIBURE) UpdateMailingListBean mailingListForm, Model model) {

		setFormFields(mailingListForm, nextOffset);

		return LIST_MAILING_MEMBERS_ADD_FORM_VIEW;
	}

	@Layout(title = "Remove Mailing list Members")
	@RequestMapping(method = RequestMethod.GET, path = "/remove-mailinglist-members-form")
	public String viewMailingListMembers(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			@ModelAttribute(UPDATE_MAILINGLIST_FORM_ATTRIBURE) UpdateMailingListBean mailingListForm, Model model) {

		String mailingListAddress = mailingListForm.getMailingList();

		// get mail ids in mailinglist
		MailingListMembersResponse mailingListMembersResp = mailgunClient.getMailingListMembers(mailingListAddress);

		while(!mailingListMembersResp.isEmpty()){
			List<MailingListMemberParam> mailingListMembers = mailingListMembersResp.getMailingListMembers();
			for (MailingListMemberParam mailingListMember : mailingListMembers) {
				DealerMailBean dealerMailBean = new DealerMailBean();
				dealerMailBean.setDealerEmailId(mailingListMember.getAddress());
				dealerMailBean.setDealerName(mailingListMember.getName());
				mailingListForm.getAllDealerMailList().add(dealerMailBean);
			}
			mailingListMembersResp = mailgunClient.getMailingListMembersWithPaginationURL(mailingListMembersResp.getPaging().getNext() );
		}
		
		return LIST_MAILING_MEMBERS_REMOVE_FORM_VIEW;
	}

	private void setFormFields(UpdateMailingListBean mailingListForm, String nextOffset) {

		String mailingListAddress = mailingListForm.getMailingList();
		String dealerType = mailingListForm.getDealerType();
		List<DealerMailBean> dealerMailsToBeDisplayed = new ArrayList<DealerMailBean>();
		// get dealer users
		UserAccountInfoListResponse listResponse = null;

		Map<String, String> queryMap = new HashMap<>();
		DealerType dealerTypeEnum = UpdateMailingListBean.DealerType.valueOf(dealerType);
		switch (dealerTypeEnum) {
		case BUYER:
		case VENDOR:
			queryMap.put("type", dealerTypeEnum.name());
			break;
		default:
			break;
		}
		
		DealerAccounts dealerAccounts = bnbneedsClient.adminDealerAccounts();
		MailingListMembersResponse mailingListMembersResp ;
		// loop here, in case all the dealers are in the mailing list
		while(dealerMailsToBeDisplayed.isEmpty()){
		try {
			if (!queryMap.isEmpty()) {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = dealerAccounts.list(RESULT_FETCH_SIZE, queryMap, nextOffset);
				} else {
					listResponse = dealerAccounts.list(RESULT_FETCH_SIZE, queryMap);
					;
				}
			} else {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = dealerAccounts.list(RESULT_FETCH_SIZE, nextOffset);
				} else {
					listResponse = dealerAccounts.list(RESULT_FETCH_SIZE);
				}
			}
		} catch (BNBNeedsServiceException e) {
			throw e;
		}
		List<UserAccountInfoResponse> userAccountList = (List<UserAccountInfoResponse>) ((listResponse != null
				&& listResponse.getUserAccountList() != null) ? listResponse.getUserAccountList() : new ArrayList<>());

		if(userAccountList.isEmpty()){
			break;
		}
				
		// temporary holder for all the dealer mails to be displayed.
		for (UserAccountInfoResponse accountInfo : userAccountList) {
			// if organization is not registered then dont display
			if(StringUtils.isEmpty(accountInfo.getRelationshipId())){
				continue;
			}
			DealerMailBean dealerMailBean = new DealerMailBean();
			dealerMailBean.setDealerEmailId(accountInfo.getName());
			dealerMailBean.setDealerName(accountInfo.getAccountName());
			dealerMailsToBeDisplayed.add(dealerMailBean);
		}
		
		// get mail ids in mailinglist
		mailingListMembersResp = mailgunClient.getMailingListMembers(mailingListAddress);
		List<MailingListMemberParam> mailingListMembers = null;
		while(!mailingListMembersResp.isEmpty()){
			boolean existsInMailingListflag = false;
			mailingListMembers = mailingListMembersResp.getMailingListMembers();
			for (UserAccountInfoResponse accountInfo : userAccountList) {
				DealerMailBean dealerMailBean = new DealerMailBean();
				dealerMailBean.setDealerEmailId(accountInfo.getName());
				dealerMailBean.setDealerName(accountInfo.getAccountName());
				MailingListMemberParam temp = new MailingListMemberParam();
				temp.setAddress(accountInfo.getName());
				existsInMailingListflag = mailingListMembers.contains(temp);
				// if member exists then remove from temp holder
				if(existsInMailingListflag){
					dealerMailsToBeDisplayed.remove(dealerMailBean);
				}
			}
			mailingListMembersResp = mailgunClient.getMailingListMembersWithPaginationURL(mailingListMembersResp.getPaging().getNext());
		}
		mailingListForm.getAllDealerMailList().addAll(dealerMailsToBeDisplayed);
		nextOffset = listResponse.getNextOffset();
		mailingListForm.setNextOffset(listResponse.getNextOffset());
		}
	}

	@Layout(title = "Update Mailing lists")
	@RequestMapping(method = RequestMethod.POST, path = "/update-mailinglist")
	public String updateMailingListMembers(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			@ModelAttribute(UPDATE_MAILINGLIST_FORM_ATTRIBURE) UpdateMailingListBean mailingListForm, Model model) {

		String mailingListAddress = mailingListForm.getMailingList();
		try {
			List<AddMailingListMemberParam> reqParamList = new ArrayList<>();
			for (String member : mailingListForm.getDealerMailList()) {
				String[] memberArray = member.split("\\|");
				AddMailingListMemberParam reqParam = new AddMailingListMemberParam();
				reqParam.setName(memberArray[0].trim());
				reqParam.setAddress(memberArray[1].trim());
				reqParamList.add(reqParam);
			}
			mailgunClient.addMailingListMembers(reqParamList, mailingListAddress);
		} catch (MailgunServiceException ex) {
			logger.error("MailgunServiceException  occurred", ex);
			setErrorMessage(model, "Error adding members to mailing list: " + ex.getMessage());
			return LIST_MAILING_MEMBERS_ADD_FORM_VIEW;
		}
		return getRedirectPath(LIST_MAILING_MEMBERS_URL + "?sendSuccess");
	}

	@Layout(title = "Remove Mailing list members")
	@RequestMapping(method = RequestMethod.POST, path = "/remove-mailinglist-members")
	public String removeMailingListMembers(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			@ModelAttribute(UPDATE_MAILINGLIST_FORM_ATTRIBURE) UpdateMailingListBean mailingListForm, Model model) {

		String mailingListAddress = mailingListForm.getMailingList();
		try {
			for (String memberAddress : mailingListForm.getDealerMailList()) {
				mailgunClient.deleteMailingListMember(mailingListAddress, memberAddress);
			}
		} catch (MailgunServiceException ex) {
			logger.error("MailgunServiceException  occurred", ex);
			setErrorMessage(model, "Error adding members to mailing list: " + ex.getMessage());
			return LIST_MAILING_MEMBERS_REMOVE_FORM_VIEW;
		}
		return getRedirectPath(LIST_MAILING_MEMBERS_URL + "?sendSuccess");
	}

}
