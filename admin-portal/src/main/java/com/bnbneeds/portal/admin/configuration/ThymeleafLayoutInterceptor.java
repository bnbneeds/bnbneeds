package com.bnbneeds.portal.admin.configuration;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.autoconfigure.web.BasicErrorController;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.bnbneeds.portal.admin.annotation.Layout;

public class ThymeleafLayoutInterceptor extends HandlerInterceptorAdapter {

	private static final String DEFAULT_LAYOUT = "default-template";
	private static final String DEFAULT_TITLE = "BNBneeds";
	private static final String VIEW_ATTRIBUTE_NAME = "view";
	private static final String TEMPLATE_FILE_NAME_SUFFIX = "-template";
	private static final String VIEW_FILE_NAME_SUFFIX = "-view";
	private static final String TITLE_PREFIX = "BNBneeds - ";
	private static final String TITLE_ATTRIBUTE_NAME = "title";

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		if (modelAndView == null || !modelAndView.hasView()) {
			return;
		}
		String originalViewName = modelAndView.getViewName();
		if (isForwardOrRedirect(originalViewName)) {
			return;
		}
		originalViewName += VIEW_FILE_NAME_SUFFIX;
		String layoutName = getLayoutName(handler);
		String title = getViewTitle(handler);
		modelAndView.setViewName(layoutName);
		modelAndView.addObject(VIEW_ATTRIBUTE_NAME, originalViewName);
		modelAndView.addObject(TITLE_ATTRIBUTE_NAME, title);
	}

	private boolean isForwardOrRedirect(String originalViewName)
			throws ServletException, IOException {

		if (originalViewName.startsWith("redirect:")
				|| originalViewName.startsWith("forward:")) {
			return true;
		}
		return false;
	}

	private String getLayoutName(Object handler) {
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		Layout layout = getMethodOrTypeAnnotation(handlerMethod);
		if (layout == null) {
			return DEFAULT_LAYOUT;
		} else {
			String layoutName = layout.value();

			return StringUtils.hasText(layoutName) ? (layoutName + TEMPLATE_FILE_NAME_SUFFIX)
					: DEFAULT_LAYOUT;
		}
	}

	private String getViewTitle(Object handler) {
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		Layout layout = getMethodOrTypeAnnotation(handlerMethod);
		if (layout == null) {
			return DEFAULT_TITLE;
		} else {
			String title = layout.title();

			return StringUtils.hasText(title) ? (TITLE_PREFIX + title)
					: DEFAULT_TITLE;
		}
	}

	private Layout getMethodOrTypeAnnotation(HandlerMethod handlerMethod) {

		Layout layout = handlerMethod.getMethodAnnotation(Layout.class);
		if (layout == null) {
			if (BasicErrorController.class == handlerMethod.getBeanType()) {
				return APIExceptionHandler.class.getAnnotation(Layout.class);
			}
			return handlerMethod.getBeanType().getAnnotation(Layout.class);
		}
		return layout;
	}

}
