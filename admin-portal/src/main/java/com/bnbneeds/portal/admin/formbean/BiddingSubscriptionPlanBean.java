package com.bnbneeds.portal.admin.formbean;

import org.hibernate.validator.constraints.NotBlank;

public class BiddingSubscriptionPlanBean {
	
	public static enum PlanType {
		DURATION, QUOTA
	}

	@NotBlank
	private String title;
	
	@NotBlank
	private String name;
	
	private String description;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@NotBlank
	private String type;
	
	private String durationType;
	
	private Integer durationValue;
	
	@NotBlank
	private Double premiumAmount;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDurationType() {
		return durationType;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public Integer getDurationValue() {
		return durationValue;
	}

	public void setDurationValue(Integer durationValue) {
		this.durationValue = durationValue;
	}

	public Double getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(Double premiumAmount) {
		this.premiumAmount = premiumAmount;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
