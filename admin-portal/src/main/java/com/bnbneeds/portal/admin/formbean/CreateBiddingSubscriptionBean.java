package com.bnbneeds.portal.admin.formbean;

public class CreateBiddingSubscriptionBean   {

	private String subscriptionPlanId;

	public String getSubscriptionPlanId() {
		return subscriptionPlanId;
	}

	public void setSubscriptionPlanId(String subscriptionPlanId) {
		this.subscriptionPlanId = subscriptionPlanId;
	}

}
