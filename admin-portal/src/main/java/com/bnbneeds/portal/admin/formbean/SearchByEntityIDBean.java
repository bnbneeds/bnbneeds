package com.bnbneeds.portal.admin.formbean;

public class SearchByEntityIDBean {

	private String subscriptionId;

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

}
