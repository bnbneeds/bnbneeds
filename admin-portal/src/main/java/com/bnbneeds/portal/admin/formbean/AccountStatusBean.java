package com.bnbneeds.portal.admin.formbean;

public class AccountStatusBean {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
