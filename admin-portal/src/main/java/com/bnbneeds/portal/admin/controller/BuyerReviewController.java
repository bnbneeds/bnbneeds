package com.bnbneeds.portal.admin.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.EntityApprovalParam.EntityStatus;
import com.bnbneeds.portal.admin.controller.util.DealerUtils;
import com.bnbneeds.portal.admin.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.admin.controller.util.ReviewUtility;
import com.bnbneeds.portal.admin.formbean.ReviewBean;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = "/buyers/{buyerId}/reviews")
public class BuyerReviewController extends AbstractBaseController {

	private static final ReviewFor REVIEW_FOR = ReviewFor.buyer;

	private static final String BUYER_PROFILE_VIEW = "buyer-profile";
	protected static final String BASE_REQUEST_PATH = "/buyers";
	private static final String LIST_URI = "/list";
	private static final String BUYER_LIST_URI = BASE_REQUEST_PATH + LIST_URI ;
	
	@RequestMapping(method = RequestMethod.GET, path = "/list-as-text")
	@ResponseBody
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public synchronized void writeReviews(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			@RequestParam(name = "reviewedBy", required = false) String reviewedBy,
			@RequestParam(name = "nextOffset", required = false) String nextOffset,
			HttpServletResponse response) throws IOException {

		try {
			//DealerUtils.checkHasDealership(user);
			String list = ReviewUtility.getReviewsAsString(bnbneedsClient,
					REVIEW_FOR, buyerId, reviewedBy, nextOffset);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, list, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/list-as-text-by-reviewer")
	@ResponseBody
	@PreAuthorize("hasAuthority('VENDOR')")
	public synchronized void writeReviewsByVendor(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			HttpServletResponse response) throws IOException {

		try {
			String vendorId = DealerUtils.checkAndReturnRelationshipId(user);
			String list = ReviewUtility.getReviewsAsString(bnbneedsClient,
					REVIEW_FOR, buyerId, vendorId, null);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, list, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize("hasAuthority('VENDOR')")
	public synchronized void postReview(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, ReviewBean form,
			HttpServletResponse response) throws IOException {
		try {
			DealerUtils.checkHasDealership(user);

			ReviewUtility.postReview(bnbneedsClient, REVIEW_FOR, buyerId, form);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, "OK", response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{reviewId}/delete")
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String deleteReview(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			@PathVariable("reviewId") String reviewId,
			Model model) {
		try {

			ReviewUtility.deleteReview(bnbneedsClient, REVIEW_FOR, buyerId,
					reviewId);
			
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_PROFILE_VIEW);
		}
		setMessage(model, "Review has been successfully deleted.");
		return getRedirectPath(BUYER_LIST_URI + "?deleteSuccess");
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{reviewId}/blacklist")
	public String blacklistVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			@PathVariable("reviewId") String reviewId,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.BLACKLISTED);
			ReviewUtility.updateEntityStatus(bnbneedsClient, REVIEW_FOR, buyerId,
					reviewId,param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_PROFILE_VIEW);
		}
		setMessage(model, "Review has been successfully blacklisted.");
		return getRedirectPath(BUYER_LIST_URI + "?blacklistSuccess");
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{reviewId}/approve")
	public String approveVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			@PathVariable("reviewId") String reviewId,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.APPROVED);
			ReviewUtility.updateEntityStatus(bnbneedsClient, REVIEW_FOR, buyerId,
					reviewId,param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_PROFILE_VIEW);
		}
		setMessage(model, "Organization has been successfully approved.");
		return getRedirectPath(BUYER_LIST_URI + "?approveSuccess");
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{reviewId}/disapprove")
	public String disapproveVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			@PathVariable("reviewId") String reviewId,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.DISAPPROVED);
			ReviewUtility.updateEntityStatus(bnbneedsClient, REVIEW_FOR, buyerId,
					reviewId,param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_PROFILE_VIEW);
		}
		setMessage(model, "Organization has been successfully disapproved.");
		return getRedirectPath(BUYER_LIST_URI + "?disapproveSuccess");
	}
}
