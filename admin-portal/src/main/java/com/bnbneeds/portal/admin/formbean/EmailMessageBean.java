package com.bnbneeds.portal.admin.formbean;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.bnbneeds.portal.admin.validator.MultipleEmail;

public class EmailMessageBean {

	public static enum FormElementName {
		TO("to"), CC("cc"), BCC("bcc"), SUBJECT("subject"), MESSAGE("message"), INLINEIMAGE("inlineImage"), ATTACHMENT(
				"attachment");

		private String name;

		private FormElementName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	@NotBlank(message = "Subject cannot be empty.")
	private String subject;

	@NotBlank(message = "Message cannot be empty.")
	private String message;
	@NotBlank(message = "To address cannot be empty.")
	private String to;
	private String cc;
	private String bcc;

	public String getSubject() {
		return subject;
	}

	public String getMessage() {
		return message;
	}

	@Email(message = "E-mail address is invalid.")
	public String getTo() {
		return to;
	}

	@MultipleEmail(message = "E-mail address is invalid.")
	public String getCc() {
		return cc;
	}

	@MultipleEmail(message = "E-mail address is invalid.")
	public String getBcc() {
		return bcc;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmailMessageParam [");
		if (subject != null)
			builder.append("subject=").append(subject).append(", ");
		if (message != null)
			builder.append("message=").append(message).append(", ");
		if (to != null)
			builder.append("to=").append(to).append(", ");
		if (cc != null)
			builder.append("cc=").append(cc).append(", ");
		if (bcc != null)
			builder.append("bcc=").append(bcc);
		builder.append("]");
		return builder.toString();
	}

}
