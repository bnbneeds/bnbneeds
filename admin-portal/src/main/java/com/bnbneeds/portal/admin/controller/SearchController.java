package com.bnbneeds.portal.admin.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionInfoResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.formbean.SearchByEntityIDBean;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = SearchController.BASE_REQUEST_PATH)
@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
public class SearchController extends AbstractBaseController {

	private static final Logger logger = LoggerFactory
			.getLogger(SearchController.class);

	protected static final String BASE_REQUEST_PATH = "/search";
	private static final String SEARCH_ENTITY_FORM_VIEW = "search-entity-form";
	protected static final String BUYER_DETAILS_URI = "/buyers/";
	protected static final String VENDOR_DETAILS_URI = "/vendors/";
	private static final String SEARCH_BY_ID_FORM_ATTRIBUTE = "searchByIDFormBean";
	

	@Layout(title = "Lookup Dealer Information")
	@RequestMapping(method = RequestMethod.GET, path = "/search-dealer-by-bidding-subscription-id-form")
	public String getSearchDealerByBiddingSubscriptionForm(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(SEARCH_BY_ID_FORM_ATTRIBUTE) SearchByEntityIDBean form,
			Model model) {

			return SEARCH_ENTITY_FORM_VIEW;
	}

	@Layout(title = "Lookup Dealer Information")
	@RequestMapping(method = RequestMethod.POST, path = "/search-dealer-by-bidding-subscription-id-form")
	public String searchDealerByBiddingSubscription(
			@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(SEARCH_BY_ID_FORM_ATTRIBUTE) @Valid SearchByEntityIDBean form,
			BindingResult result, Model model) {
		
		String subscriptionId = form.getSubscriptionId();
		String link = null;
		try {
			String className = StringUtils.getTypeName(subscriptionId);
			switch (className.toLowerCase()) {
			case "buyerbiddingsubscription":
				BuyerBiddingSubscriptionInfoResponse resp = bnbneedsClient.adminEntities().getEntity(BuyerBiddingSubscriptionInfoResponse.class, subscriptionId);
				String buyerId = resp.getBuyer().getId() + "/details";
				link = BUYER_DETAILS_URI + buyerId ;
				break;
			case "vendorbiddingsubscription":
				VendorBiddingSubscriptionInfoResponse vendoResp = bnbneedsClient.adminEntities().getEntity(VendorBiddingSubscriptionInfoResponse.class, subscriptionId);
				String vendorId = vendoResp.getVendor().getId();
				link = VENDOR_DETAILS_URI + vendorId + "/details";
				break;
			default:
				setErrorMessage(model, "Please enter valid subscription id");
				return SEARCH_ENTITY_FORM_VIEW;
			}
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, SEARCH_ENTITY_FORM_VIEW);
		}

		return getRedirectPath(link); 
	}


}
