package com.bnbneeds.portal.admin.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mailgun.rest.client.ClientType;
import com.mailgun.rest.client.MailgunClient;

@Configuration
public class MailgunClientInitializer {

private static final Logger logger = LoggerFactory.getLogger(MailgunClientInitializer.class);

	@Value("${apisvc.url}")
	private String apisvcURL;
	
	@Value("${smtp.mailgun.domain}")
	private String mailgunDomain;

	@Value("${smtp.mailgun.apiKey}")
	private String mailgunApiKey;
	
	@Bean(name = "mailgunClient")
	public MailgunClient init() {
		logger.info("Initializing mailgun client with client type: web ");
		MailgunClient mailgunClient = new MailgunClient(ClientType.web, mailgunDomain, mailgunApiKey);
		return mailgunClient;

	}

}
