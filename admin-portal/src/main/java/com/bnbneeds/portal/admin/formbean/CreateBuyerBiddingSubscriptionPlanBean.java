package com.bnbneeds.portal.admin.formbean;

public class CreateBuyerBiddingSubscriptionPlanBean extends BiddingSubscriptionPlanBean  {

	private Integer numberOfTendersAllowedToCreate;

	public Integer getNumberOfTendersAllowedToCreate() {
		return numberOfTendersAllowedToCreate;
	}

	public void setNumberOfTendersAllowedToCreate(Integer numberOfTendersAllowedToCreate) {
		this.numberOfTendersAllowedToCreate = numberOfTendersAllowedToCreate;
	}

	
}
