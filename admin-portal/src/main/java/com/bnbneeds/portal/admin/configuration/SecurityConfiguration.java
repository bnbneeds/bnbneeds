package com.bnbneeds.portal.admin.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.AuthenticatedVoter;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.expression.WebExpressionVoter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.context.request.RequestContextListener;

import com.bnbneeds.portal.admin.controller.AuthenticationController;
import com.bnbneeds.portal.admin.controller.PublicWebsiteController;
import com.bnbneeds.portal.admin.security.BNBNeedsAuthenticationProvider;
import com.bnbneeds.portal.admin.security.BNBNeedsClientLogoutHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final String[] PUBLIC_URLS = { "/", "/errors/*",
			PublicWebsiteController.PING_BNBNEEDS_SERVICE_URL,
			AuthenticationController.LOGIN_URL, "/authenticate",
			"/session-terminated", "/reset-password-form", "/reset-password",
			"/about", "/contact" };

	private static final String[] STATIC_RESOURCE_URL_PATTERNS = {
			"/bower_components/**", "/external/**" };

	@Bean
	public AccessDecisionManager accessDecisionManager() {
		RoleVoter roleVoter = new RoleVoter();
		roleVoter.setRolePrefix("");
		List<AccessDecisionVoter<? extends Object>> decisionVoters = new ArrayList<AccessDecisionVoter<? extends Object>>();
		decisionVoters.add(roleVoter);
		decisionVoters.add(new AuthenticatedVoter());
		decisionVoters.add(new WebExpressionVoter());
		AccessDecisionManager accessDecisionManager = new AffirmativeBased(
				decisionVoters);
		return accessDecisionManager;
	}

	@Override
	public void configure(WebSecurity webSecurity) throws Exception {
		webSecurity.ignoring().antMatchers(STATIC_RESOURCE_URL_PATTERNS);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.csrf()
				.disable()
				.authenticationProvider(bnbnnedsAuthenticationProvider())
				.authorizeRequests()
				.accessDecisionManager(accessDecisionManager())
				.and()
				.authorizeRequests()
				.antMatchers(PUBLIC_URLS)
				.permitAll()
				.and()
				.formLogin()
				.loginProcessingUrl("/authenticate")
				.loginPage(AuthenticationController.LOGIN_URL)
				.defaultSuccessUrl("/home-page")
				.and()
				.authorizeRequests()
				.antMatchers("/**")
				.authenticated()
				.and()
				.logout()
				.addLogoutHandler(bnbneedsClientLogoutHandler())
				.logoutRequestMatcher(
						new AntPathRequestMatcher(
								AuthenticationController.LOGOUT_URL))
				.logoutSuccessUrl(
						AuthenticationController.LOGIN_URL + "?logout")
				.and()
				.sessionManagement()
				.maximumSessions(2)
				.expiredUrl(
						AuthenticationController.LOGIN_URL + "?sessionExpired");
	}

	@Bean
	public BNBNeedsClientLogoutHandler bnbneedsClientLogoutHandler() {
		return new BNBNeedsClientLogoutHandler();
	}

	@Bean
	public AuthenticationProvider bnbnnedsAuthenticationProvider() {
		return new BNBNeedsAuthenticationProvider();
	}

	/*
	 * @Bean public LoginUrlAuthenticationEntryPoint
	 * bnbneedsAdminPortalAuthEntryPoint() { return new
	 * LoginUrlAuthenticationEntryPoint( AuthenticationController.LOGIN_URL); }
	 */

	@Bean
	public RequestContextListener requestContextListener() {
		return new RequestContextListener();
	}

}
