package com.bnbneeds.portal.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.portal.admin.annotation.Layout;

@Controller
@RequestMapping(path = "/errors")
@Layout(title = "Oops!")
public class ErrorPageController extends AbstractBaseController {

	private static final String ERRORS_URL_PREFIX = "/errors";
	private static final String RECONNECT_URL = "/attempt-reconnect";
	private static final String RECONNECT_VIEW = "reconnect";

	private static final String MODEL_ATTRIBUTE_PING_URL = "pingURL";

	public static final String ATTEMPT_RECONNECT_URL = ERRORS_URL_PREFIX
			+ RECONNECT_URL;

	@RequestMapping(method = RequestMethod.GET, path = { RECONNECT_URL })
	public String attemptReconnect(Model model) {
		String pingURL = request.getContextPath()
				+ PublicWebsiteController.PING_BNBNEEDS_SERVICE_URL;
		model.addAttribute(MODEL_ATTRIBUTE_PING_URL, pingURL);
		return RECONNECT_VIEW;
	}
}
