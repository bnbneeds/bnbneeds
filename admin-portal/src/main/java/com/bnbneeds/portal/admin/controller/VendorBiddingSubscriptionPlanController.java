package com.bnbneeds.portal.admin.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.model.dealer.subscription.BiddingSubscriptionPlanParam.PlanType;
import com.bnbneeds.app.model.dealer.subscription.CreateVendorSubscriptionPlanParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateVendorSubscriptionPlanParam;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionPlanInfoListResponse;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionPlanInfoResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.formbean.CreateVendorBiddingSubscriptionPlanBean;
import com.bnbneeds.portal.admin.formbean.EntityAdminApprovalFormBean;
import com.bnbneeds.portal.admin.formbean.SubscriptionPlanUpdateAvailabilityStatusFormBean;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.core.BiddingSubscriptionPlans.BiddingSubscriptionPlanFor;
import com.bnbneeds.rest.client.core.VendorBiddingSubscriptionPlans;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@Layout(title = VendorBiddingSubscriptionPlanController.LIST_VIEW_TITLE_NAME)
@RequestMapping(path = VendorBiddingSubscriptionPlanController.BASE_REQUEST_PATH)
public class VendorBiddingSubscriptionPlanController extends BiddingSubscriptionPlanController {

	protected static final String LIST_VIEW_TITLE_NAME = "Vendor Bidding Subscription Plans";
	static final String BIDDING_SUBSCRIPTION_PLAN_VIEW = "vendor-bidding-subscription-plan-details";
	static final String BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW = "vendor-bidding-subscription-plans-list";
	protected static final String BASE_REQUEST_PATH = "/vendors/bidding-subscription-plans";
	private static final String LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_ATTRIBUTE = "listBiddingSubscriptionPlansRequestURI";
	private static final String LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_URI = BASE_REQUEST_PATH + "/list";
	private static final String BIDDING_SUBSCRIPTION_PLANS_INFO_ATTRIBURE = "biddingSubscriptionPlansInfo";
	protected static final String NEW_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE = "createSubscriptionPlanForm";
	private static final String UPDATE_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE = "updateSubscriptionPlanForm";
	private static final String BIDDING_SUBSCRIPTION_PLAN_UPDATE_FORM_VIEW = "vendor-bidding-subscription-plan-update-form";

	@Override
	public String getViewTitle() {
		return LIST_VIEW_TITLE_NAME;
	}
	
	@Override
	public String getBiddingSubscriptionPlanListView() {
		return BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW;
	}

	@Override
	public String getListBiddingSubscriptionPlanReqestURI() {
		return LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_URI;
	}

	@Override
	protected BiddingSubscriptionPlanFor getBiddingSubscriptionPlanFor() {
		return BiddingSubscriptionPlanFor.vendor;
	}

	public String getListVerificationSubscriptionPlansURI() {
		return LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_URI;
	}

	@Override
	public String getBaseRequestUrl() {
		return BASE_REQUEST_PATH;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/list")
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String list(@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			@ModelAttribute(NEW_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE) CreateVendorBiddingSubscriptionPlanBean form,
			Model model) {
		try {
			setModelForSubscriptionPlansListView(model, nextOffset);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW);
		}
		return BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/create")
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String createSubscriptionPlan(
			@ModelAttribute(NEW_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE) CreateVendorBiddingSubscriptionPlanBean form,
			Model model) {

		CreateVendorSubscriptionPlanParam param = new CreateVendorSubscriptionPlanParam();
		String planName = form.getName();
		planName = planName.trim().toUpperCase().replaceAll(" ", "_");
		param.setName(planName);
		param.setTitle(form.getTitle());
		param.setDescription(form.getDescription());
		param.setType(form.getType());
		if (form.getType().equals(PlanType.DURATION.name())) {
			param.setDurationValue(form.getDurationValue());
			param.setDurationType(form.getDurationType());
		} else if (form.getType().equals(PlanType.QUOTA.name())) {
			if (form.getNumberOfTendersAllowedToAccess() != null) {
				param.setNumberOfTendersAllowedToAccess(form.getNumberOfTendersAllowedToAccess());
			}
		}

		param.setPremiumAmount(form.getPremiumAmount());

		try {
			bnbneedsClient.vendorBiddingSubscriptionPlans().create(param);
		} catch (BNBNeedsServiceException e) {
			setModelForSubscriptionPlansListView(model, null);
			return handleBadRequest(e, model, BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW);
		}

		return getRedirectPath(LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_URI + "?createSuccess");
	}

	@Layout(title = "Subscription Plan Update Form")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/update-form")
	public String getSubscriptionPlanUpdateForm(@PathVariable("id") String planId, Model model,
			@ModelAttribute(UPDATE_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE) UpdateVendorSubscriptionPlanParam form) {

		VendorBiddingSubscriptionPlanInfoResponse info = new VendorBiddingSubscriptionPlanInfoResponse();
		try {
			info = (VendorBiddingSubscriptionPlanInfoResponse) bnbneedsClient.vendorBiddingSubscriptionPlans().get(planId);
		} catch (BNBNeedsServiceException e) {
			setErrorModelForSubscriptionPlansListView(model, null);
			return handleBadRequest(e, model, BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW);
		}
		mapPlanInfoResponseToUpdateForm(info, form);
		return BIDDING_SUBSCRIPTION_PLAN_UPDATE_FORM_VIEW;
	}

	private void mapPlanInfoResponseToUpdateForm(VendorBiddingSubscriptionPlanInfoResponse info,
			UpdateVendorSubscriptionPlanParam form) {
		form.setName(info.getName());
		form.setTitle(info.getTitle());
		form.setDescription(info.getDescription());
		form.setType(info.getType());
		form.setDurationType(info.getDurationType());
		form.setDurationValue(info.getDurationValue());
		form.setNumberOfTendersAllowedToAccess(info.getNumberOfTendersAllowedToAccess());
		form.setPremiumAmount(info.getPremiumAmount());
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{planId}/update")
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String updateSubscriptionPlan(@PathVariable("planId") String planId,
			@ModelAttribute(UPDATE_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE) UpdateVendorSubscriptionPlanParam form,
			Model model) {

		try {
			bnbneedsClient.vendorBiddingSubscriptionPlans().update(form, planId);
		} catch (BNBNeedsServiceException e) {
			setErrorModelForSubscriptionPlansListView(model, null);
			return handleBadRequest(e, model, BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW);
		}

		return getRedirectPath(LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_URI + "?updateSuccess");
	}

	protected void setModelForSubscriptionPlansListView(Model model, String nextOffset) {

		setViewTitle(model, getViewTitle());
		setBaseRequestUrl(model, getBaseRequestUrl());

		model.addAttribute(LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_ATTRIBUTE,
				request.getContextPath() + getListVerificationSubscriptionPlansURI());

		try {
			VendorBiddingSubscriptionPlanInfoListResponse listResponse = null;
			VendorBiddingSubscriptionPlans vendorBiddingSubscriptionPlansClient = bnbneedsClient
					.vendorBiddingSubscriptionPlans();
			if (StringUtils.hasText(nextOffset)) {
				listResponse = (VendorBiddingSubscriptionPlanInfoListResponse) vendorBiddingSubscriptionPlansClient
						.list(RESULT_FETCH_SIZE, null, nextOffset);
			} else {
				listResponse = (VendorBiddingSubscriptionPlanInfoListResponse) vendorBiddingSubscriptionPlansClient
						.list(RESULT_FETCH_SIZE);
			}

			model.addAttribute(LIST_RESPONSE_ATTRIBUTE, listResponse);
		} catch (BNBNeedsServiceException e) {
			throw e;
		}
	}

	@Override
	protected void setErrorModelForSubscriptionPlansListView(Model model, String nextOffset) {
		model.addAttribute(NEW_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE, new CreateVendorBiddingSubscriptionPlanBean());
		setModelForSubscriptionPlansListView(model, nextOffset);
	}

	@Layout(title = "Subscription Plan Details")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/details")
	public String getSubscriptionPlanDetails(@PathVariable("id") String planId,
			@ModelAttribute(UPDATE_PLAN_AVAILABILITY_FORM_BEAN) SubscriptionPlanUpdateAvailabilityStatusFormBean updatePlanAvailabilityForm,
			@ModelAttribute(ENTITY_ADMIN_FORM_BEAN) EntityAdminApprovalFormBean updateEntityStatusForm,	Model model) {

		VendorBiddingSubscriptionPlanInfoResponse info = new VendorBiddingSubscriptionPlanInfoResponse();
		try {
			info = (VendorBiddingSubscriptionPlanInfoResponse) bnbneedsClient.vendorBiddingSubscriptionPlans().get(planId);
			String availabilityStatus = info.getAvailablityStatus();
			if(!StringUtils.isEmpty(availabilityStatus)){
				info.setAvailablityStatus(availabilityStatuses.get(availabilityStatus));	
			}
		} catch (BNBNeedsServiceException e) {
			setErrorModelForSubscriptionPlansListView(model, null);
			return handleBadRequest(e, model, BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW);
		}
		model.addAttribute(BIDDING_SUBSCRIPTION_PLANS_INFO_ATTRIBURE, info);
		return BIDDING_SUBSCRIPTION_PLAN_VIEW;
	}

	public static String getBiddingSubscriptionPlansURI() {
		return String.format(LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_URI);
	}

}
