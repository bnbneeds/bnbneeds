package com.bnbneeds.portal.admin.util;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionUtils {
	private static final Logger logger = LoggerFactory.getLogger(ConnectionUtils.class);

	public static void checkServerConnection(String hostNameOrIPAddress, int port) throws ConnectException {

		Socket s = null;
		try {
			/*
			 * If socket is not able to initialize, ConnectException is thrown. That's what
			 * we are interested in. If ConnectException occurs, then APIExceptionHandler
			 * kicks and loads the reconnect-view.html.
			 * 
			 * If socket connection is successful, then user will automatically be
			 * redirected to /home-page or /login-page
			 */
			s = new Socket(hostNameOrIPAddress, port);
		} catch (ConnectException e) {
			logger.error("ConnectException occured", e);
			throw e;
		} catch (UnknownHostException e) {
			logger.error("UnknownHostException occured", e);
		} catch (IOException e) {
			logger.error("IOException occured", e);
		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (IOException e) {
					logger.error("IOException occured", e);
				}
			}
		}

	}

	public static void checkJMSBrokerConnection(ActiveMQConnectionFactory activeMQfactory) throws JMSException {
		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(activeMQfactory.getBrokerURL());

		// set transport listener so that active MQ start is notified.
		Connection conn = factory.createConnection(activeMQfactory.getUserName(), activeMQfactory.getPassword());
		Session session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Destination dest = new ActiveMQQueue("test");
		MessageProducer producer = session.createProducer(dest);
		MessageConsumer consumer = session.createConsumer(dest);
		MessageListener messageListener = new MessageListener() {

			@Override
			public void onMessage(Message message) {
				// Do nothing
			}
		};
		consumer.setMessageListener(messageListener); // class that implements MessageListener
		conn.start();
		TextMessage message = new ActiveMQTextMessage();
		message.setText("TestMessage");
		producer.send(message);
	}

}
