package com.bnbneeds.portal.admin.security;

public interface Role {
	String ADMIN = "ADMIN";
	String SUPER_ADMIN = "SUPER_ADMIN";
	String VENDOR = "VENDOR";
	String BUYER = "BUYER";
}
