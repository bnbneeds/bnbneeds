package com.bnbneeds.portal.admin.exception;

public class ForbiddenException extends APIException {

	private static final long serialVersionUID = 4507969646304306503L;

	private static final String USER_NOT_AUTHORIZED = "You are not authorized to access this resource.";

	public ForbiddenException() {
	}

	public ForbiddenException(String message) {
		super(message);
	}

	public ForbiddenException(Throwable cause) {
		super(cause);
	}

	public static ForbiddenException unauthorizedAccess() {
		return new ForbiddenException(USER_NOT_AUTHORIZED);
	}

}
