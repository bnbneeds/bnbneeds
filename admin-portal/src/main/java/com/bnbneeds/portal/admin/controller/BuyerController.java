package com.bnbneeds.portal.admin.controller;

import static com.bnbneeds.portal.admin.controller.util.RequestResponseUtils.writeResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.dealer.BuyerInfoListResponse;
import com.bnbneeds.app.model.dealer.BuyerInfoResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.controller.util.ReviewUtility;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = BuyerController.BASE_REQUEST_PATH)
public class BuyerController extends DealerController {

	private static final Logger logger = LoggerFactory
			.getLogger(BuyerController.class);

	protected static final String BASE_REQUEST_PATH = "/buyers";
	private static final String LIST_URI = "/list";
	private static final String DETAIL_URI = "/%s/details";
	private static final String BUYER_DETAIL_REQUEST_URI = BASE_REQUEST_PATH
			+ DETAIL_URI;

	public static final String LIST_BUYERS_URI = BASE_REQUEST_PATH + LIST_URI;

	private static final String BUYER_LIST_RESPONSE_ATTRIBUTE = "buyerListResponse";
	private static final String BUYER_INFO_ATTRIBUTE = "buyerInfo";

	private static final String BUYER_LIST_VIEW = "buyer-list";

	private static final String BUYER_PROFILE_VIEW = "buyer-profile";

	private static final int BUYER_LIST_RESULT_SIZE = 20;

	public static String getBuyerDetailsRequestURI(String buyerId) {
		return String.format(BUYER_DETAIL_REQUEST_URI, buyerId);
	}

	static void setBuyerInfoAttribute(Model model, BuyerInfoResponse info) {
		String description = info.getDescription();
		if (StringUtils.hasText(description)) {
			description = StringUtils.convertLineEndingsToHtml(description);
			info.setDescription(description);
		}

		String address = info.getAddress();
		if (StringUtils.hasText(address)) {
			address = StringUtils.convertLineEndingsToHtml(address);
			info.setAddress(address);
		}

		model.addAttribute(BUYER_INFO_ATTRIBUTE, info);
	}

	private String getBuyerProfileView(Model model, BuyerInfoResponse buyerInfo) {
		setBuyerInfoAttribute(model, buyerInfo);
		return BUYER_PROFILE_VIEW;
	}

	static void setBuyerListResponse(Model model,
			BuyerInfoListResponse listResponse) {
		model.addAttribute(BUYER_LIST_RESPONSE_ATTRIBUTE, listResponse);
	}

	@Layout(title = "Buyers")
	@RequestMapping(method = RequestMethod.GET, path = LIST_URI)
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String getBuyerList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "businessType", required = false) String businessType,
			@RequestParam(name = "nextOffset", required = false) String nextOffset,
			Model model) {

		BuyerInfoListResponse listResponse = null;
		Map<String, String> queryParams = new HashMap<String, String>();
		try {
			if (StringUtils.hasText(businessType)) {
				if (businessType.contains("|")) {
					String[] nameAndIdArr = businessType.split("\\|");
					queryParams.put("businessTypeId", nameAndIdArr[1].trim());
					model.addAttribute("selectedBusinessType",
							nameAndIdArr[0].trim());
				} else {
					model.addAttribute("selectedBusinessType", businessType);
				}
			}

			if (!queryParams.isEmpty()) {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = bnbneedsClient.buyers().list(
							BUYER_LIST_RESULT_SIZE, queryParams, nextOffset);
				} else {
					listResponse = bnbneedsClient.buyers().list(
							BUYER_LIST_RESULT_SIZE, queryParams);
				}
			} else {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = bnbneedsClient.buyers().list(
							BUYER_LIST_RESULT_SIZE, nextOffset);
				} else {
					BuyerOperationsController.setBusinessTypes(bnbneedsClient,
							model);
					listResponse = bnbneedsClient.buyers().list(
							BUYER_LIST_RESULT_SIZE);
				}
			}
			setBuyerListResponse(model, listResponse);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_LIST_VIEW);
		}

		return BUYER_LIST_VIEW;
	}

	@Layout(title = "Buyer Information")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/details")
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String getBuyerInfo(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String buyerId,
			@RequestParam(name = "reviewId", required = false) String reviewId,
			Model model) {

		try {
			BuyerInfoResponse buyer = bnbneedsClient.buyers(buyerId).get();
			ReviewUtility.setAverageRatingResponse(bnbneedsClient,
					ReviewFor.buyer, buyerId, model);
			ReviewUtility.setReviewListResponse(bnbneedsClient,
					ReviewFor.buyer, buyerId, reviewId, null, null, model);
			return getBuyerProfileView(model, buyer);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_PROFILE_VIEW);
		}
	}

	@Layout(title = "Buyer Information")
	@RequestMapping(method = RequestMethod.GET, path = "/details")
	public String getDetailsForReviewId(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "reviewId") String reviewId, Model model) {

		try {
			String buyerId = ReviewUtility.getEntityIdForReviewId(
					bnbneedsClient, ReviewFor.buyer, reviewId);
			if (!StringUtils.hasText(buyerId)) {
				return getRedirectPath(LIST_BUYERS_URI + "?reviewId="
						+ reviewId);
			}
			return getRedirectPath(getBuyerDetailsURI(buyerId)
					+ "?reviews=true&reviewId=" + reviewId);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_PROFILE_VIEW);
		}

	}

	private String getBuyerDetailsURI(String buyerId) {
		return String.format(BUYER_DETAIL_REQUEST_URI, buyerId);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/detail-as-text")
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	@ResponseBody
	public void writeBuyerInfo(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String buyerId, HttpServletResponse response)
			throws IOException {

		try {
			String buyer = bnbneedsClient.buyers(buyerId).getAsString();
			writeResponse(200, MediaType.TEXT_PLAIN_VALUE, buyer, response);
		} catch (BNBNeedsServiceException e) {
			writeResponse(e.getHttpCode(), MediaType.TEXT_PLAIN_VALUE, e
					.getTask().getMessage(), response);
		}
	}
}
