package com.bnbneeds.portal.admin.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.model.EntityCountResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;

@Controller
@Layout(title = "Home")
public class HomePageController extends AbstractBaseController {

	public static final String HOMEPAGE_URL = "/home-page";

	static final String VIEW = "home";

	@RequestMapping(method = RequestMethod.GET, path = HOMEPAGE_URL)
	public String landingPage(@AuthenticationPrincipal BNBNeedsUser user, Model model) {

		if (user == null) {
			return getRedirectPath(AuthenticationController.LOGIN_URL);
		}

		return VIEW;
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/entity-count")
	public synchronized void getEntityCount(@RequestParam("entityType") String entityType,
			HttpServletResponse httpResponse) throws IOException {
		try {
			EntityCountResponse response = bnbneedsClient.utilities().getEntityCount(entityType);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE,
					String.valueOf(response.getCount()), httpResponse);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, httpResponse);
		}
	}
}
