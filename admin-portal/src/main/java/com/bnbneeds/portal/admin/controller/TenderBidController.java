package com.bnbneeds.portal.admin.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.EntityApprovalParam.EntityStatus;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderProductInfoListResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.admin.formbean.EntityAdminApprovalFormBean;
import com.bnbneeds.portal.admin.formbean.TenderListBean;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.core.TenderBids;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.google.gson.Gson;

@Controller
@RequestMapping(path = TenderBidController.BASE_REQUEST_PATH)
@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
public class TenderBidController extends TenderController {

	static final String TENDER_DETAILS_VIEW = "tender-details";
	private static final String TENDER_LIST_REQUEST_URI = "/list";
	protected static final String BASE_REQUEST_PATH = "/tenders";
	private static final String LIST_TENDER_FORM_ATTRIBUTE = "listTenderFormBean";
	private static final String TENDER_LIST_RESPONSE_ATTRIBUTE = "listResponse";
	private static final String TENDER_LIST_VIEW = "tender-list";
	private static final String TENDER_DETAILS_REQUEST_URI = "/{tenderId}/details";
	private static final String TENDER_BID_PRODUCT_LIST_RESPONSE_ATTRIBUTE = "tenderBidProductList";
	private static final String TENDER_BID_LIST_RESPONSE_ATTRIBUTE = "bidListResponse";
	protected static final String ENTITY_ADMIN_FORM_BEAN = "entityAdminApprovalFormBean";

	public static String getPathForGetTenderDetails(String tenderId) {
		return BASE_REQUEST_PATH + TENDER_DETAILS_REQUEST_URI.replace("{tenderId}", tenderId);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/bids/list-as-text")
	@ResponseBody
	public synchronized void writeBids(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId,
			@RequestParam(name = "nextOffset", required = false) String nextOffset, HttpServletResponse response)
			throws IOException {

		try {
			TenderBidInfoListResponse bidListResponse = null;
			TenderBids tenderBidsClient = bnbneedsClient.tenderBids();
			if (StringUtils.hasText(nextOffset)) {
				bidListResponse = tenderBidsClient.listTenderBids(tenderId, FETCH_SIZE, nextOffset);
			} else {
				bidListResponse = tenderBidsClient.listTenderBids(tenderId, FETCH_SIZE);
			}

			if (bidListResponse != null && !bidListResponse.isEmpty()) {
				for (Iterator<TenderBidInfoResponse> iterator = bidListResponse.getIterator(); iterator.hasNext();) {
					TenderBidInfoResponse info = iterator.next();
					setBidTimestamp(info);
				}
			}

			Gson gson = new Gson();
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE,
					gson.toJson(bidListResponse), response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/products/list-as-text")
	@ResponseBody
	public synchronized void writeTenderProducts(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId,
			@RequestParam(name = "nextOffset", required = false) String nextOffset, HttpServletResponse response)
			throws IOException {
		try {
			String bidProductListResponse;
			TenderBids tenderBidsClient = bnbneedsClient.tenderBids();
			if (StringUtils.hasText(nextOffset)) {
				bidProductListResponse = tenderBidsClient.getTenderProductsAsString(tenderId, nextOffset);
			} else {
				bidProductListResponse = tenderBidsClient.getTenderProductsAsString(tenderId);
			}
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE,
					bidProductListResponse, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@Layout(title = "Accesed Tenders")
	@RequestMapping(method = RequestMethod.GET, path = TENDER_LIST_REQUEST_URI)
	public String getTenderList(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(LIST_TENDER_FORM_ATTRIBUTE) TenderListBean form, Model model) {
		loadTenderQueryStatusMap(model);
		TenderBids tenderBidsClient = bnbneedsClient.tenderBids();

		String tenderStatus = form.getTenderStatus();
		String nextOffset = form.getNextOffset();
		Map<String, String> queryParams = new HashMap<>();
		if (StringUtils.hasText(tenderStatus)) {
			queryParams.put("status", tenderStatus);
		}
		TenderInfoListResponse listResponse = null;
		try {

			if (!queryParams.isEmpty()) {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = tenderBidsClient.listTenders(RESULT_FETCH_SIZE, queryParams, nextOffset);
				} else {
					listResponse = tenderBidsClient.listTenders(RESULT_FETCH_SIZE, queryParams);
				}
			} else {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = tenderBidsClient.listTenders(RESULT_FETCH_SIZE, nextOffset);
				} else {
					listResponse = tenderBidsClient.listTenders(RESULT_FETCH_SIZE);
				}
			}
			if (listResponse != null) {
				nextOffset = listResponse.getNextOffset();
				if (StringUtils.hasText(nextOffset)) {
					form.setNextOffset(nextOffset);
				}
			}
			model.addAttribute(TENDER_LIST_RESPONSE_ATTRIBUTE, listResponse);
			return TENDER_LIST_VIEW;
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, TENDER_LIST_VIEW);
		}

	}

	@Layout(title = "Tender Details")
	@RequestMapping(method = RequestMethod.GET, path = TENDER_DETAILS_REQUEST_URI)
	public String getTenderDetails(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId, Model model) {

		try {
			// set response
			setTenderInfoResponse(user, tenderId, model);
			return TENDER_DETAILS_VIEW;
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, TENDER_DETAILS_VIEW);
		}
	}

	private void setTenderInfoResponse(BNBNeedsUser user, String tenderId, Model model) {
		TenderBids tenderBidsClient = bnbneedsClient.tenderBids();
		TenderInfoResponse response = tenderBidsClient.getTenderDetails(tenderId);
		setTenderInfoResponse(response, model);
		TenderProductInfoListResponse bidProducts = tenderBidsClient.listTenderProducts(tenderId, 3);
		model.addAttribute(TENDER_BID_PRODUCT_LIST_RESPONSE_ATTRIBUTE, bidProducts);
		TenderBidInfoListResponse tenderBids = tenderBidsClient.listTenderBids(tenderId, 5);

		if (tenderBids != null && !tenderBids.isEmpty()) {
			for (Iterator<TenderBidInfoResponse> iterator = tenderBids.getIterator(); iterator.hasNext();) {
				TenderBidInfoResponse info = iterator.next();
				setBidTimestamp(info);
			}
		}
		model.addAttribute(TENDER_BID_LIST_RESPONSE_ATTRIBUTE, tenderBids);

	}

	@RequestMapping(method = RequestMethod.POST, path = "/{id}/update-entity-status")
	public String updateSubscriptionPlanStatus(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(ENTITY_ADMIN_FORM_BEAN) EntityAdminApprovalFormBean form,
			@PathVariable("id") String entityid, Model model) {

		TenderBids tenderBidsClient = bnbneedsClient.tenderBids();
		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.valueOf(form.getApprovalStatus()));
			tenderBidsClient.updateEntityStatus(entityid, param);
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, TENDER_DETAILS_VIEW);
		}

		return TENDER_DETAILS_VIEW;
	}

}
