package com.bnbneeds.portal.admin.controller;

import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.jms.JMSException;
import javax.validation.Valid;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.bnbneeds.app.model.EmailMessageParam;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.controller.util.MessagingUtility;
import com.bnbneeds.portal.admin.formbean.EmailMessageBean;
import com.bnbneeds.portal.admin.formbean.MaintenanceNoticeBean;
import com.bnbneeds.portal.admin.util.ConnectionUtils;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.portal.common.jms.JMSConstants;
import com.bnbneeds.portal.common.jms.beans.MaintenanceNoticeMBean;
import com.bnbneeds.portal.common.jms.beans.MaintenanceNoticeMBean.Operation;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.mailgun.rest.client.MailgunClient;
import com.mailgun.rest.client.model.MailingListsResponse;

@Controller
@RequestMapping(path = "/messaging")
@PreAuthorize("hasAuthority('SUPER_ADMIN')")
public class MessagingController extends AbstractBaseController {

	private static final Logger logger = LoggerFactory.getLogger(MessagingController.class);

	static final String EMAIL_DEALERS_FORM_URI = "/messaging/email-dealers-form";
	private static final String EMAIL_DEALERS_FORM_VIEW = "email-dealers-form";
	private static final String MAINTENANCE_NOTICE_FORM_VIEW = "maintenance-notice-form";
	private static final String MAILING_LIST_RESPONSE = "mailingListsResponse";
	private static final String DEALERS_MAILING_LIST_ADDRESS_ATTRIBUTE = "dealersMailingListAddress";
	private static final String EMAIL_DEALERS_FORM_ATTRIBURE = "emailDealersFormBean";
	private static final String MAINTENANCE_NOTICE_FORM_ATTRIBURE = "maintenanceNoticeFormBean";

	@Autowired
	protected MailgunClient mailgunClient;

	@Autowired
	private JmsTemplate jmsTemplate;

	@Autowired
	private ActiveMQConnectionFactory activeMQConnectionFactory;

	@Value("${email.address.all-dealers}")
	private String emailAddressOfAllDealers;

	@Value("${email.address.application-admin}")
	private String adminEmailAddress;

	@Value("${spring.http.multipart.location}")
	private String tempUploadLocation;

	@PostConstruct
	public void createTemporaryUploadDirectoryIfNotExists() {
		File dir = new File(tempUploadLocation);
		if (dir.exists()) {
			logger.info("Temporary upload directory already exists: {}", dir);
		} else {
			logger.info("Temporary upload directory does not exist. So create it: {}", dir);
			dir.mkdirs();
		}
	}

	private static void setDealersMailingListAddress(String emailAddressOfAllDealers, Model model) {
		model.addAttribute(DEALERS_MAILING_LIST_ADDRESS_ATTRIBUTE, emailAddressOfAllDealers);
	}

	private static void setMailingLists(MailgunClient mailgunClient, Model model) {
		MailingListsResponse mailingListsResponse = mailgunClient.getMailingLists();
		model.addAttribute(MAILING_LIST_RESPONSE, mailingListsResponse);
	}

	@Layout(title = "Email Dealers")
	@RequestMapping(method = RequestMethod.GET, path = "/email-dealers-form")
	public String getEmailDealersForm(@ModelAttribute(EMAIL_DEALERS_FORM_ATTRIBURE) EmailMessageBean emailForm,
			Model model) {
		setMailingLists(mailgunClient, model);
		return EMAIL_DEALERS_FORM_VIEW;
	}

	@Layout(title = "Email Dealers")
	@RequestMapping(method = RequestMethod.POST, path = "/email-dealers")
	public String emailDealers(@RequestParam("myFile") MultipartFile myFile,
			@RequestParam("inlineImage") MultipartFile inlineImage,
			@ModelAttribute(EMAIL_DEALERS_FORM_ATTRIBURE) @Valid EmailMessageBean emailForm, BindingResult result,
			Model model) throws IOException {

		if (result.hasErrors()) {
			setMailingLists(mailgunClient, model);
			return EMAIL_DEALERS_FORM_VIEW;
		}

		try {
			if (myFile.isEmpty() && inlineImage.isEmpty()) {
				MessagingUtility.emailMessageWithoutAttachment(emailForm, bnbneedsClient);
			} else {
				MessagingUtility.emailMessageWitAttachment(myFile, inlineImage, emailForm, bnbneedsClient);
			}
		} catch (BNBNeedsServiceException e) {
			setMailingLists(mailgunClient, model);
			return handleBadRequest(e, model, EMAIL_DEALERS_FORM_VIEW);
		} catch (Exception e) {
			setMailingLists(mailgunClient, model);
			setErrorMessage(model, e.getMessage());
			return EMAIL_DEALERS_FORM_VIEW;
		}
		return getRedirectPath(HomePageController.HOMEPAGE_URL + "?sendEmailSuccess");
	}

	@Layout(title = "Maintenance Notice")
	@RequestMapping(method = RequestMethod.GET, path = "/maintenance-notice-form")
	public String getMaintenanceNoticeForm(
			@ModelAttribute(MAINTENANCE_NOTICE_FORM_ATTRIBURE) MaintenanceNoticeBean form, Model model) {

		setDealersMailingListAddress(emailAddressOfAllDealers, model);

		try {
			ConnectionUtils.checkJMSBrokerConnection(activeMQConnectionFactory);
		} catch (JMSException e) {
			setErrorMessage(model, "JMS Provider is not up or has a problem: " + e.getMessage());
		}
		return MAINTENANCE_NOTICE_FORM_VIEW;
	}

	@Layout(title = "Maintenance Notice")
	@RequestMapping(method = RequestMethod.POST, path = "/maintenance-notice")
	public String postMaintenanceNotice(
			@ModelAttribute(MAINTENANCE_NOTICE_FORM_ATTRIBURE) @Valid MaintenanceNoticeBean form, BindingResult result,
			Model model) {

		setDealersMailingListAddress(emailAddressOfAllDealers, model);

		if (result.hasErrors()) {
			return MAINTENANCE_NOTICE_FORM_VIEW;
		}

		String message = form.getMessage();
		String subject = form.getSubject();
		String timeRange = form.getTimeRange();
		String[] timeRangeArray = timeRange.split("To");
		String startTime = timeRangeArray[0].trim();
		String endTime = timeRangeArray[1].trim();
		boolean sendEmail = form.isSendEmailToAllDealers();

		MaintenanceNoticeMBean jmsBean = new MaintenanceNoticeMBean(Operation.POST);
		jmsBean.setSubject(subject);
		jmsBean.setMessage(message);
		jmsBean.setStartTime(startTime);
		jmsBean.setEndTime(endTime);
		try {
			jmsTemplate.convertAndSend(new ActiveMQTopic(JMSConstants.MAINTENANCE_NOTICE_QUEUE_NAME), jmsBean);
			if (sendEmail) {
				EmailMessageParam emailParam = new EmailMessageParam();
				emailParam.setTo(emailAddressOfAllDealers);
				emailParam.setBcc(adminEmailAddress);
				String maintenanceWindowText = String.format(
						"<p class=\"align-center\"><strong>Maintenance Window: %1$s - %2$s</strong></p>", startTime,
						endTime);
				emailParam.setSubject(subject);
				emailParam.setMessage(maintenanceWindowText + StringUtils
						.convertLineEndingsToHtml(MessagingUtility.MAILGUN_TEMPLATE_RECIPIENT_SALUTATION + message));
				bnbneedsClient.messaging().sendEmail(emailParam);
			}
		} catch (JmsException e) {
			logger.error("JMSException: Unable to send message.", e);
			setErrorMessage(model, "Unable to send message to JMS Provider: " + e.getMessage());
			return MAINTENANCE_NOTICE_FORM_VIEW;
		} catch (BNBNeedsServiceException e) {
			logger.error("BNBNeedsServiceException: Unable to send e-mail message.", e);
			setErrorMessage(model, "Unable to send e-mail: " + e.getMessage());
			return MAINTENANCE_NOTICE_FORM_VIEW;
		}
		return getRedirectPath(HomePageController.HOMEPAGE_URL + "?postNoticeSuccess");
	}

	@Layout(title = "Clear Maintenance Notice")
	@RequestMapping(method = RequestMethod.POST, path = "/maintenance-notice/clear")
	public String clearMaintenanceNotice(Model model) {
		MaintenanceNoticeMBean jmsBean = new MaintenanceNoticeMBean(Operation.DELETE);
		try {
			jmsTemplate.convertAndSend(new ActiveMQTopic(JMSConstants.MAINTENANCE_NOTICE_QUEUE_NAME), jmsBean);
		} catch (JmsException e) {
			logger.error("JMSException: Unable to send message.", e);
			setErrorMessage(model, "Unable to send message: " + e.getMessage());
			return MAINTENANCE_NOTICE_FORM_VIEW;
		}
		return getRedirectPath(HomePageController.HOMEPAGE_URL + "?clearNoticeSuccess");
	}
}
