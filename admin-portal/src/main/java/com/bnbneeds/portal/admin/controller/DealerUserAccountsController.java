package com.bnbneeds.portal.admin.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.AccountType;
import com.bnbneeds.app.model.account.UpdateAccountStatusParam;
import com.bnbneeds.app.model.account.UpdateAccountStatusParam.AccountStatus;
import com.bnbneeds.app.model.account.UpdateUserAccountParam;
import com.bnbneeds.app.model.account.UserAccountInfoListResponse;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.formbean.AccountStatusBean;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.core.DealerAccounts;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.mailgun.rest.client.MailgunClient;
import com.mailgun.rest.client.model.AddMailingListMemberParam;

@Controller
@Layout(title = DealerUserAccountsController.LIST_VIEW_TITLE_NAME)
@RequestMapping(path = DealerUserAccountsController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('SUPER_ADMIN')")
public class DealerUserAccountsController extends AbstractBaseController {

	private static final Logger logger = LoggerFactory.getLogger(DealerUserAccountsController.class);

	protected static final String LIST_VIEW_TITLE_NAME = "Dealer Accounts";
	static final String ACCOUNT_VIEW = "dealer-account";
	static final String DEALER_ACCOUNTS_LIST_VIEW = "dealer-accounts-list";
	private static final String LIST_VIEW_TITLE_ATTRIBUTE = "listViewTitle";
	protected static final String BASE_REQUEST_PATH = "/dealer-accounts";
	private static final String LIST_RESPONSE_ATTRIBUTE = "listResponse";
	private static final String LIST_ACCOUNT_REQUEST_URI = "listAccountsRequestURI";
	private static final String BASE_REQUEST_URL_ATTRIBUTE = "baseRequestURI";
	private static final String UPDATE_ENTITY_URL_ATTRIBUTE = "updateEntityURI";
	private static final String ACCOUNT_INFO_ATTRIBURE = "accountInfo";
	private static final String UPDATE_ACCOUNT_FORM_ATTRIBURE = "updateUserAccountParam";
	private static final String ACCOUNT_ID_ATTRIBURE = "accountId";
	private static final String ORGANIZATION_EXISTS_ATTRIBURE = "organizationExists";
	static final String UPDATE_ACCOUNT_FORM_VIEW = "update-dealer-account";
	private static final String LIST_ACCOUNTS_REQUEST_URI = BASE_REQUEST_PATH + "/list";
	private static final String CREATE_ENTITIES_REQUEST_URI = BASE_REQUEST_PATH + "/create";
	private static final String ACCOUNT_INFO_REQUEST_URI = "/info";

	@Autowired
	protected MailgunClient mailgunClient;

	@Value("${email.address.all-dealers}")
	protected String dealersMailingListAddress;

	@Value("${email.address.buyers}")
	protected String buyersMailingListAddress;

	@Value("${email.address.vendors}")
	protected String vendorsMailingListAddress;

	private static final String DEALER_EMAIL_UPSERT = "yes";

	public String getCreateAccountURI() {
		return CREATE_ENTITIES_REQUEST_URI;
	}

	public String getListAccountsURI() {
		return LIST_ACCOUNTS_REQUEST_URI;
	}

	public String getBaseRequestUrl() {
		return BASE_REQUEST_PATH;
	}

	protected void setViewTitle(String title, Model model) {
		model.addAttribute(LIST_VIEW_TITLE_ATTRIBUTE, title);
	}

	protected void setBaseRequestUrl(Model model, String baseUrl) {
		model.addAttribute(BASE_REQUEST_URL_ATTRIBUTE, baseUrl);
	}

	protected void setUpdateEntityUrl(Model model, String updateEntityUrl) {
		model.addAttribute(UPDATE_ENTITY_URL_ATTRIBUTE, updateEntityUrl);
	}

	@Layout(title = LIST_VIEW_TITLE_NAME)
	@RequestMapping(method = RequestMethod.GET, path = "/list")
	public String list(@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset, Model model) {
		try {
			setModelForAccountsListView(model, nextOffset);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, DEALER_ACCOUNTS_LIST_VIEW);
		}
		return DEALER_ACCOUNTS_LIST_VIEW;
	}

	protected void setModelForAccountsListView(Model model, String nextOffset) {

		setBaseRequestUrl(model, getBaseRequestUrl());
		model.addAttribute(LIST_ACCOUNT_REQUEST_URI, request.getContextPath() + getListAccountsURI());

		try {
			UserAccountInfoListResponse listResponse = null;
			DealerAccounts adminClient = bnbneedsClient.adminDealerAccounts();
			if (StringUtils.hasText(nextOffset)) {
				listResponse = adminClient.list(RESULT_FETCH_SIZE, nextOffset);
			} else {
				listResponse = adminClient.list(RESULT_FETCH_SIZE);
			}
			model.addAttribute(LIST_RESPONSE_ATTRIBUTE, listResponse);
		} catch (BNBNeedsServiceException e) {
			throw e;
		}

	}

	@RequestMapping(method = RequestMethod.POST, path = "/{id}/delete")
	public String delete(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String accountId,
			Model model) {

		try {
			UserAccountInfoResponse info = bnbneedsClient.adminDealerAccounts().getAccount(accountId);
			bnbneedsClient.adminDealerAccounts().deleteAccount(accountId);
			removeDealerFromMailinglist(info.getUsername(), info.getRole());
		} catch (BNBNeedsServiceException e) {
			setModelForAccountsListView(model, null);
			return handleBadRequest(e, model, DEALER_ACCOUNTS_LIST_VIEW);
		}

		return getRedirectPath(LIST_ACCOUNTS_REQUEST_URI + "?deleteSuccess");
	}

	@Layout(title = "Account Details")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/info")
	public String getAccountInfo(@PathVariable("id") String accountId, Model model) {

		UserAccountInfoResponse info = bnbneedsClient.adminDealerAccounts().getAccount(accountId);
		model.addAttribute(ACCOUNT_INFO_ATTRIBURE, info);
		return ACCOUNT_VIEW;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{id}/approvals")
	public String updateAccountStatus(@PathVariable("id") String accountId, @AuthenticationPrincipal BNBNeedsUser user,
			AccountStatusBean form, Model model) {
		try {
			UpdateAccountStatusParam param = new UpdateAccountStatusParam();
			param.setStatus(AccountStatus.valueOf(form.getStatus()));
			bnbneedsClient.adminDealerAccounts().updateAccountStatus(accountId, param);
		} catch (BNBNeedsServiceException e) {
			setModelForAccountsListView(model, null);
			return handleBadRequest(e, model, DEALER_ACCOUNTS_LIST_VIEW);
		}

		return getRedirectPath(BASE_REQUEST_PATH + "/" + accountId + ACCOUNT_INFO_REQUEST_URI + "?statusUpdateSuccess");
	}

	@Layout(title = "Edit Account")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/update-account-form")
	public String getUpdateAccountForm(@PathVariable("id") String accountId,
			@ModelAttribute(UPDATE_ACCOUNT_FORM_ATTRIBURE) UpdateUserAccountParam updateParam, Model model) {

		UserAccountInfoResponse info = bnbneedsClient.adminDealerAccounts().getAccount(accountId);
		updateParam.setUsername(info.getUsername());
		updateParam.setMobileNumber(info.getMobileNumber());
		updateParam.setAccountName(info.getAccountName());
		if (!StringUtils.isEmpty(info.getRelationshipId())) {
			model.addAttribute(ORGANIZATION_EXISTS_ATTRIBURE, Boolean.TRUE);
		}
		updateParam.setType(AccountType.valueOf(info.getRole()));
		model.addAttribute(ACCOUNT_ID_ATTRIBURE, accountId);

		return UPDATE_ACCOUNT_FORM_VIEW;
	}

	@Layout(title = "Edit Account")
	@RequestMapping(method = RequestMethod.POST, path = "/{id}/update")
	public String updateProfile(@PathVariable("id") String accountId,
			@ModelAttribute(UPDATE_ACCOUNT_FORM_ATTRIBURE) UpdateUserAccountParam updateParam, Model model) {

		try {
			UserAccountInfoResponse info = bnbneedsClient.adminDealerAccounts().getAccount(accountId);
			String oldUsername = info.getUsername();

			TaskResponse task = bnbneedsClient.adminDealerAccounts().updateAccount(updateParam, accountId);
			updateMailgunMailingList(oldUsername, updateParam);
			setMessage(model, task.getMessage());
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, UPDATE_ACCOUNT_FORM_VIEW);
		}

		UserAccountInfoResponse info = bnbneedsClient.adminDealerAccounts().getAccount(accountId);
		model.addAttribute(ACCOUNT_INFO_ATTRIBURE, info);
		return ACCOUNT_VIEW;
	}

	private void updateMailgunMailingList(String oldUsername, UpdateUserAccountParam updateParam) {

		// if the email id has changed, update the mailing list with the new member
		// email id
		if (!oldUsername.equalsIgnoreCase(updateParam.getUsername())) {
			updateDealerToMailinglist(updateParam, oldUsername);
		}
	}

	protected void updateDealerToMailinglist(UpdateUserAccountParam param, String oldUsername) {

		AddMailingListMemberParam memberParam = new AddMailingListMemberParam();
		memberParam.setName(param.getAccountName());
		memberParam.setAddress(param.getUsername());

		// adding separate try/catch for every call, as we want to continue to change
		// the email id in other mailing lists even if it fails in one.
		try {
			mailgunClient.updateMailingListMember(memberParam, dealersMailingListAddress, oldUsername);
		} catch (Exception e) {
			logger.error("Updating dealer email id: {} to all dealer mailing list failed with error message: {}",
					param.getUsername(), e);
		}
		try {
			mailgunClient.updateMailingListMember(memberParam, buyersMailingListAddress, oldUsername);
		} catch (Exception e) {
			logger.error("Updating dealer email id: {} to buyer mailing list failed with error message: {}",
					param.getUsername(), e);
		}
		try {
			mailgunClient.updateMailingListMember(memberParam, vendorsMailingListAddress, oldUsername);
		} catch (Exception e) {
			logger.error("Updating dealer email id: {} to vendor mailing list failed with error message: {}",
					param.getUsername(), e);
		}

	}
	
	private void removeDealerFromMailinglist(String dealerEmailAddress, String role) {
		try{
				mailgunClient.deleteMailingListMember(dealersMailingListAddress, dealerEmailAddress);
				if(role.equalsIgnoreCase(AccountType.BUYER.name())){
					mailgunClient.deleteMailingListMember(buyersMailingListAddress, dealerEmailAddress);
				} 
				else if(role.equalsIgnoreCase(AccountType.VENDOR.name())){
					mailgunClient.deleteMailingListMember(vendorsMailingListAddress, dealerEmailAddress);
				}
		}catch(Exception e){
			logger.error("Deleting dealer email id: {} from  mailing list failed with error message: {}",dealerEmailAddress, e.getMessage());
		}
	}

}
