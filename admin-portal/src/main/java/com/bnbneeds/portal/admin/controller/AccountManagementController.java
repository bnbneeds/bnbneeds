package com.bnbneeds.portal.admin.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.ChangePasswordParam;
import com.bnbneeds.app.model.account.UpdateUserAccountParam;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = "/accounts")
@PreAuthorize("isAuthenticated()")
public class AccountManagementController extends AbstractBaseController {

	static final String ACCOUNT_VIEW = "account";
	static final String UPDATE_ACCOUNT_FORM_VIEW = "update-account";
	static final String CHANGE_PASSWORD_FORM_VIEW = "change-password";

	private static final String ACCOUNT_INFO_ATTRIBURE = "accountInfo";
	private static final String UPDATE_ACCOUNT_FORM_ATTRIBURE = "updateAccountParam";
	private static final String CHANGE_PASSWORD_FORM_ATTRIBURE = "changePasswordParam";

	@Layout(title = "Your Account")
	@RequestMapping(method = RequestMethod.GET, path = "/profile")
	public String getProfile(Model model) {

		UserAccountInfoResponse info = bnbneedsClient.users().getAccount();
		model.addAttribute(ACCOUNT_INFO_ATTRIBURE, info);
		return ACCOUNT_VIEW;
	}

	@Layout(title = "Edit Account")
	@RequestMapping(method = RequestMethod.GET, path = "/update-account-form")
	public String getUpdateAccountForm(
			@ModelAttribute(UPDATE_ACCOUNT_FORM_ATTRIBURE) UpdateUserAccountParam updateParam) {

		UserAccountInfoResponse info = bnbneedsClient.users().getAccount();
		updateParam.setUsername(info.getUsername());
		updateParam.setMobileNumber(info.getMobileNumber());
		updateParam.setAccountName(info.getAccountName());
		return UPDATE_ACCOUNT_FORM_VIEW;
	}

	@Layout(title = "Edit Account")
	@RequestMapping(method = RequestMethod.POST, path = "/update-account")
	public String updateProfile(
			@ModelAttribute(UPDATE_ACCOUNT_FORM_ATTRIBURE) UpdateUserAccountParam updateParam,
			Model model) {

		try {
			TaskResponse task = bnbneedsClient.users().updateAccount(
					updateParam);
			setMessage(model, task.getMessage());
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, UPDATE_ACCOUNT_FORM_VIEW);
		}

		UserAccountInfoResponse info = bnbneedsClient.users().getAccount();
		model.addAttribute(ACCOUNT_INFO_ATTRIBURE, info);
		return ACCOUNT_VIEW;
	}

	@Layout(title = "Change Password")
	@RequestMapping(method = RequestMethod.GET, path = "/change-password-form")
	public String getChangePasswordForm(
			@ModelAttribute(CHANGE_PASSWORD_FORM_ATTRIBURE) ChangePasswordParam param) {
		return CHANGE_PASSWORD_FORM_VIEW;
	}

	@Layout(title = "Change Password")
	@RequestMapping(method = RequestMethod.POST, path = "/change-password")
	public String changePassword(
			@ModelAttribute(CHANGE_PASSWORD_FORM_ATTRIBURE) ChangePasswordParam param,
			Model model) {
		try {
			bnbneedsClient.auth().changePassword(param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, CHANGE_PASSWORD_FORM_VIEW);
		}
		return getRedirectPath(AuthenticationController.LOGOUT_URL);
	}

}
