package com.bnbneeds.portal.admin.configuration;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.bnbneeds.portal.admin.security.BNBNeedsUser;

@ControllerAdvice
public class ControllerSetup {

	protected static final String LOGGED_IN_USER_ATTRIBUTE = "loggedInUser";
	protected static final String BRAND_NAME_ATTRIBUTE = "brandName";
	private static final String BRAND_NAME = "BNBneeds";

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		StringTrimmerEditor stringtrimmer = new StringTrimmerEditor(true);
		binder.registerCustomEditor(String.class, stringtrimmer);
	}

	@ModelAttribute(LOGGED_IN_USER_ATTRIBUTE)
	public BNBNeedsUser getLoggedInUser(
			@AuthenticationPrincipal BNBNeedsUser user) {
		return user;

	}

	@ModelAttribute(BRAND_NAME_ATTRIBUTE)
	public String getBrandName() {
		return BRAND_NAME;

	}

}
