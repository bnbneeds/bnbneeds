package com.bnbneeds.portal.admin.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.model.dealer.subscription.BiddingSubscriptionPlanParam.PlanType;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionPlanInfoListResponse;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionPlanInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.CreateBuyerSubscriptionPlanParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateBuyerSubscriptionPlanParam;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.formbean.CreateBuyerBiddingSubscriptionPlanBean;
import com.bnbneeds.portal.admin.formbean.EntityAdminApprovalFormBean;
import com.bnbneeds.portal.admin.formbean.SubscriptionPlanUpdateAvailabilityStatusFormBean;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.core.BiddingSubscriptionPlans.BiddingSubscriptionPlanFor;
import com.bnbneeds.rest.client.core.BuyerBiddingSubscriptionPlans;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@Layout(title = BuyerBiddingSubscriptionPlanController.LIST_VIEW_TITLE_NAME)
@RequestMapping(path = BuyerBiddingSubscriptionPlanController.BASE_REQUEST_PATH)
public class BuyerBiddingSubscriptionPlanController extends BiddingSubscriptionPlanController {

	protected static final String LIST_VIEW_TITLE_NAME = "Dealer Bidding Subscription Plans";
	static final String BIDDING_SUBSCRIPTION_PLAN_VIEW = "buyer-bidding-subscription-plan-details";
	static final String BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW = "buyer-bidding-subscription-plans-list";
	protected static final String BASE_REQUEST_PATH = "/buyers/bidding-subscription-plans";
	private static final String LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_ATTRIBUTE = "listBiddingSubscriptionPlansRequestURI";
	private static final String LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_URI = BASE_REQUEST_PATH + "/list";
	private static final String BIDDING_SUBSCRIPTION_PLANS_INFO_ATTRIBURE = "biddingSubscriptionPlansInfo";
	protected static final String NEW_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE = "createSubscriptionPlanForm";
	private static final String UPDATE_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE = "updateSubscriptionPlanForm";
	private static final String BIDDING_SUBSCRIPTION_PLAN_UPDATE_FORM_VIEW = "buyer-bidding-subscription-plan-update-form";
	
	@Override
	public String getViewTitle() {
		return LIST_VIEW_TITLE_NAME;
	}

	public String getListVerificationSubscriptionPlansURI() {
		return LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_URI;
	}

	@Override
	public String getBaseRequestUrl() {
		return BASE_REQUEST_PATH;
	}

	public static String getBiddingSubscriptionPlansURI() {
		return String.format(LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_URI);
	}

	@Override
	public String getBiddingSubscriptionPlanListView() {
		return BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW;
	}

	@Override
	public String getListBiddingSubscriptionPlanReqestURI() {
		return LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_URI;
	}

	@Override
	protected BiddingSubscriptionPlanFor getBiddingSubscriptionPlanFor() {
		return BiddingSubscriptionPlanFor.buyer;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/list")
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String list(@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			@ModelAttribute(NEW_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE) CreateBuyerBiddingSubscriptionPlanBean form,
			Model model) {
		try {
			setModelForSubscriptionPlansListView(model, nextOffset);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW);
		}
		return BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/create")
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String createSubscriptionPlan(
			@ModelAttribute(NEW_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE) CreateBuyerBiddingSubscriptionPlanBean form,
			Model model) {

		CreateBuyerSubscriptionPlanParam param = new CreateBuyerSubscriptionPlanParam();
		param.setName(form.getName());
		String planName = form.getName();
		planName = planName.trim().toUpperCase().replaceAll(" ", "_");
		param.setName(planName);
		param.setTitle(form.getTitle());
		param.setDescription(form.getDescription());
		param.setType(form.getType());
		if (form.getType().equals(PlanType.DURATION.name())) {
			param.setDurationValue(form.getDurationValue());
			param.setDurationType(form.getDurationType());
		} else if (form.getType().equals(PlanType.QUOTA.name())) {
			if (form.getNumberOfTendersAllowedToCreate() != null) {
				param.setNumberOfTendersAllowedToCreate(form.getNumberOfTendersAllowedToCreate());
			}
		}

		param.setPremiumAmount(form.getPremiumAmount());

		try {
			bnbneedsClient.buyerBiddingSubscriptionPlans().create(param);
		} catch (BNBNeedsServiceException e) {
			setModelForSubscriptionPlansListView(model, null);
			return handleBadRequest(e, model, BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW);
		}

		return getRedirectPath(LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_URI + "?createSuccess");
	}

	protected void setModelForSubscriptionPlansListView(Model model, String nextOffset) {

		setViewTitle(model, getViewTitle());
		setBaseRequestUrl(model, getBaseRequestUrl());

		model.addAttribute(LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_ATTRIBUTE,
				request.getContextPath() + getListVerificationSubscriptionPlansURI());

		try {
			BuyerBiddingSubscriptionPlanInfoListResponse listResponse = null;
			BuyerBiddingSubscriptionPlans buyerBiddingSubscriptionPlansClient = bnbneedsClient
					.buyerBiddingSubscriptionPlans();
			if (StringUtils.hasText(nextOffset)) {
				listResponse = (BuyerBiddingSubscriptionPlanInfoListResponse) buyerBiddingSubscriptionPlansClient
						.list(RESULT_FETCH_SIZE, null, nextOffset);
			} else {
				listResponse = (BuyerBiddingSubscriptionPlanInfoListResponse) buyerBiddingSubscriptionPlansClient
						.list(RESULT_FETCH_SIZE);
			}

			model.addAttribute(LIST_RESPONSE_ATTRIBUTE, listResponse);
		} catch (BNBNeedsServiceException e) {
			throw e;
		}
	}

	@Override
	protected void setErrorModelForSubscriptionPlansListView(Model model, String nextOffset) {
		model.addAttribute(NEW_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE, new CreateBuyerBiddingSubscriptionPlanBean());
		setModelForSubscriptionPlansListView(model, nextOffset);
	}

	@Layout(title = "Subscription Plan Details")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/details")
	public String getSubscriptionPlanDetails(@PathVariable("id") String planId,
			@ModelAttribute(UPDATE_PLAN_AVAILABILITY_FORM_BEAN) SubscriptionPlanUpdateAvailabilityStatusFormBean updatePlanAvailabilityForm,
			@ModelAttribute(ENTITY_ADMIN_FORM_BEAN) EntityAdminApprovalFormBean updateEntityStatusForm,	Model model) {

		BuyerBiddingSubscriptionPlanInfoResponse info = new BuyerBiddingSubscriptionPlanInfoResponse();
		try {
			info = (BuyerBiddingSubscriptionPlanInfoResponse) bnbneedsClient.buyerBiddingSubscriptionPlans().get(planId);
			String availabilityStatus = info.getAvailablityStatus();
			if(!StringUtils.isEmpty(availabilityStatus)){
				info.setAvailablityStatus(availabilityStatuses.get(availabilityStatus));	
			}
			
		} catch (BNBNeedsServiceException e) {
			setErrorModelForSubscriptionPlansListView(model, null);
			return handleBadRequest(e, model, BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW);
		}
		model.addAttribute(BIDDING_SUBSCRIPTION_PLANS_INFO_ATTRIBURE, info);
		return BIDDING_SUBSCRIPTION_PLAN_VIEW;
	}

	@Layout(title = "Subscription Plan Update Form")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/update-form")
	public String getSubscriptionPlanUpdateForm(@PathVariable("id") String planId, Model model,
			@ModelAttribute(UPDATE_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE) UpdateBuyerSubscriptionPlanParam form) {

		BuyerBiddingSubscriptionPlanInfoResponse info = new BuyerBiddingSubscriptionPlanInfoResponse();
		try {
			info = (BuyerBiddingSubscriptionPlanInfoResponse) bnbneedsClient.buyerBiddingSubscriptionPlans().get(planId);
		} catch (BNBNeedsServiceException e) {
			setErrorModelForSubscriptionPlansListView(model, null);
			return handleBadRequest(e, model, BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW);
		}
		mapPlanInfoResponseToUpdateForm(info, form);
		return BIDDING_SUBSCRIPTION_PLAN_UPDATE_FORM_VIEW;
	}

	private void mapPlanInfoResponseToUpdateForm(BuyerBiddingSubscriptionPlanInfoResponse info,
			UpdateBuyerSubscriptionPlanParam form) {
		form.setName(info.getName());
		form.setTitle(info.getTitle());
		form.setDescription(info.getDescription());
		form.setType(info.getType());
		form.setDurationType(info.getDurationType());
		form.setDurationValue(info.getDurationValue());
		form.setNumberOfTendersAllowedToCreate(info.getNumberOfTendersAllowedToCreate());
		form.setPremiumAmount(info.getPremiumAmount());
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{planId}/update")
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String update(@PathVariable("planId") String planId,
			@ModelAttribute(UPDATE_SUBSCRIPTION_PLAN_FORM_ATTRIBUTE) UpdateBuyerSubscriptionPlanParam form,
			Model model) {

		try {
			bnbneedsClient.buyerBiddingSubscriptionPlans().update(form, planId);
		} catch (BNBNeedsServiceException e) {
			setErrorModelForSubscriptionPlansListView(model, null);
			return handleBadRequest(e, model, BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW);
		}

		return getRedirectPath(LIST_BIDDING_SUBSCRIPTION_PLANS_REQUEST_URI + "?updateSuccess");
	}

}
