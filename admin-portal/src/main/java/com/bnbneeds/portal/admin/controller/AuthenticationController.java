package com.bnbneeds.portal.admin.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.formbean.ResetPasswordBean;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
public class AuthenticationController extends AbstractBaseController {

	private static final Logger logger = LoggerFactory
			.getLogger(AuthenticationController.class);

	public static final String LOGIN_URL = "/login-page";
	public static final String LOGOUT_URL = "/logout";
	static final String RESET_PASSWORD_FORM_VIEW = "reset-password";

	private static final String RESET_PASSWORD_FORM_ATTRIBURE = "resetPasswordBean";

	@Layout(title = "Reset Password")
	@RequestMapping(method = RequestMethod.GET, path = "/reset-password-form")
	public String getResetPasswordForm(
			@ModelAttribute(RESET_PASSWORD_FORM_ATTRIBURE) ResetPasswordBean param) {
		return RESET_PASSWORD_FORM_VIEW;
	}

	@Layout(title = "Reset Password")
	@RequestMapping(method = RequestMethod.POST, path = "/reset-password")
	public String resetPassword(
			@ModelAttribute(RESET_PASSWORD_FORM_ATTRIBURE) ResetPasswordBean param,
			Model model) {
		try {
			bnbneedsClient.auth().resetPassword(param.getUsername().trim());
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, RESET_PASSWORD_FORM_VIEW);
		}
		return getRedirectPath(LOGIN_URL);
	}

}
