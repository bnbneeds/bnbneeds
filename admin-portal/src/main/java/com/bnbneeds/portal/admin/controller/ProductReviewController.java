package com.bnbneeds.portal.admin.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.EntityApprovalParam.EntityStatus;
import com.bnbneeds.portal.admin.controller.util.DealerUtils;
import com.bnbneeds.portal.admin.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.admin.controller.util.ReviewUtility;
import com.bnbneeds.portal.admin.formbean.ReviewBean;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = "/products/{productId}/reviews")
public class ProductReviewController extends AbstractBaseController {

	private static final ReviewFor REVIEW_FOR = ReviewFor.product;
	
	protected static final String BASE_PATH = "/products";
	private static final String LIST_REQUEST_URI = "/list";
	public static final String LIST_PRODUCTS_REQUEST_URI = BASE_PATH
			+ LIST_REQUEST_URI;
	static final String PRODUCT_DETAILS_VIEW = "product-details";

	@RequestMapping(method = RequestMethod.GET, path = "/list-as-text")
	@ResponseBody
	@PreAuthorize("hasAnyAuthority('ADMIN', 'SPUER_ADMIN')")
	public synchronized void writeReviews(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId,
			@RequestParam(name = "reviewedBy", required = false) String reviewedBy,
			@RequestParam(name = "nextOffset", required = false) String nextOffset,
			HttpServletResponse response) throws IOException {

		try {
			DealerUtils.checkHasDealership(user);
			String list = ReviewUtility.getReviewsAsString(bnbneedsClient,
					REVIEW_FOR, productId, reviewedBy, nextOffset);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, list, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/list-as-text-by-reviewer")
	@ResponseBody
	@PreAuthorize("hasAuthority('BUYER')")
	public synchronized void writeReviewsByBuyer(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId,
			HttpServletResponse response) throws IOException {

		try {
			String buyerId = DealerUtils.checkAndReturnRelationshipId(user);
			String list = ReviewUtility.getReviewsAsString(bnbneedsClient,
					REVIEW_FOR, productId, buyerId, null);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, list, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize("hasAuthority('BUYER')")
	public synchronized void postReview(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId, ReviewBean form,
			HttpServletResponse response) throws IOException {
		try {
			DealerUtils.checkHasDealership(user);

			ReviewUtility.postReview(bnbneedsClient, REVIEW_FOR, productId,
					form);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, "OK", response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{reviewId}/delete")
	@ResponseBody
	@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
	public String deleteReview(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId,
			@PathVariable("reviewId") String reviewId,
			Model model) throws IOException {
		try {
			ReviewUtility.deleteReview(bnbneedsClient, REVIEW_FOR, productId,
					reviewId);
			
		}  catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PRODUCT_DETAILS_VIEW);
		}
		setMessage(model, "Review has been successfully deleted.");
		return PRODUCT_DETAILS_VIEW;
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{reviewId}/blacklist")
	public String blacklistVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId,
			@PathVariable("reviewId") String reviewId,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.BLACKLISTED);
			ReviewUtility.updateEntityStatus(bnbneedsClient, REVIEW_FOR, productId,
					reviewId,param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PRODUCT_DETAILS_VIEW);
		}
		setMessage(model, "Review has been successfully blacklisted.");
		return PRODUCT_DETAILS_VIEW;
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{reviewId}/approve")
	public String approveVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId,
			@PathVariable("reviewId") String reviewId,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.APPROVED);
			ReviewUtility.updateEntityStatus(bnbneedsClient, REVIEW_FOR, productId,
					reviewId,param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PRODUCT_DETAILS_VIEW);
		}
		setMessage(model, "Organization has been successfully approved.");
		return PRODUCT_DETAILS_VIEW;
	}
	
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{reviewId}/disapprove")
	public String disapproveVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId,
			@PathVariable("reviewId") String reviewId,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.DISAPPROVED);
			ReviewUtility.updateEntityStatus(bnbneedsClient, REVIEW_FOR, productId,
					reviewId,param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PRODUCT_DETAILS_VIEW);
		}
		setMessage(model, "Organization has been successfully disapproved.");
		return PRODUCT_DETAILS_VIEW;
	}

}
