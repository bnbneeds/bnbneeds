package com.bnbneeds.portal.admin.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.bnbneeds.app.model.notification.NotificationInfoResponse;
import com.bnbneeds.app.model.notification.NotificationListResponse;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;

public class NotificationEventListResponse {

	private List<NotificationEvent> events;
	private String nextOffset;

	public List<NotificationEvent> getEvents() {
		return events;
	}

	public String getNextOffset() {
		return nextOffset;
	}

	public void setEvents(List<NotificationEvent> events) {
		this.events = events;
	}

	public void setNextOffset(String nextOffset) {
		this.nextOffset = nextOffset;
	}

	public void add(NotificationEvent event) {
		if (events == null) {
			events = new ArrayList<NotificationEvent>();
		}
		events.add(event);
	}

	public void add(BNBNeedsUser user, NotificationInfoResponse notification) {
		add(new NotificationEvent(user, notification));
	}

	public static NotificationEventListResponse map(BNBNeedsUser user,
			NotificationListResponse response) {
		if (response == null) {
			return null;
		}
		NotificationEventListResponse eventListResponse = new NotificationEventListResponse();
		eventListResponse.setNextOffset(response.getNextOffset());

		List<NotificationInfoResponse> notificationList = response
				.getNotificationList();

		if (notificationList != null && !notificationList.isEmpty()) {
			for (Iterator<NotificationInfoResponse> iterator = notificationList
					.iterator(); iterator.hasNext();) {
				NotificationInfoResponse notificationInfoResponse = iterator
						.next();
				eventListResponse.add(user, notificationInfoResponse);
			}
		}
		return eventListResponse;
	}
}
