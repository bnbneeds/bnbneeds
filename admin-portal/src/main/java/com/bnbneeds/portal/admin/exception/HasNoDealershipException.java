package com.bnbneeds.portal.admin.exception;

public class HasNoDealershipException extends APIException {

	private static final long serialVersionUID = 7844061415116092183L;

	public HasNoDealershipException() {
		super();
	}

	public HasNoDealershipException(String message) {
		super(message);
	}

	public HasNoDealershipException(Throwable cause) {
		super(cause);
	}

}
