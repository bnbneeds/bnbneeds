package com.bnbneeds.portal.admin.formbean;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.bnbneeds.portal.admin.util.StringUtils;

public class MemberRegistrationBean extends UpdateMemberProfileBean {

	@NotBlank
	@Size(min = 8, max = 30)
	protected String username;
	@NotBlank
	@Size(min = 8, max = 15)
	@Pattern(regexp = StringUtils.REGEX_PASSWORD, message = "Password must have atleast 8 characters. Must contain at least one lower case letter, one upper case letter, one digit and one special character.")
	protected String password;
	@NotBlank
	protected String confirmPassword;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MemberRegistrationBean [");
		if (username != null) {
			builder.append("username=");
			builder.append(username);
			builder.append(", ");
		}
		if (password != null) {
			builder.append("password=");
			builder.append(password);
			builder.append(", ");
		}
		if (confirmPassword != null) {
			builder.append("confirmPassword=");
			builder.append(confirmPassword);
			builder.append(", ");
		}
		if (super.toString() != null) {
			builder.append(super.toString());
		}
		builder.append("]");
		return builder.toString();
	}

}
