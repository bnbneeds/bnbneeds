package com.bnbneeds.portal.admin.formbean;

public class BuyerEnquiryListRequestBean extends EnquiryListRequestBean {

	private String buyer;

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

}
