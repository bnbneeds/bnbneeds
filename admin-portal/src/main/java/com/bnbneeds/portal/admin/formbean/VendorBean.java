package com.bnbneeds.portal.admin.formbean;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import com.bnbneeds.portal.admin.util.StringUtils;

public class VendorBean extends DealerRegistrationBean {

	@Pattern(regexp = StringUtils.REGEX_VAT_NUMBER, message = "VAT number is invalid.")
	private String vatNumber;

	@Pattern(regexp = StringUtils.REGEX_SERVICE_TAX_NUMBER, message = "Service tax number is invalid.")
	private String serviceTaxNumber;
	@NotBlank
	private String cities;
	
	private String vendorId;

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public String getServiceTaxNumber() {
		return serviceTaxNumber;
	}

	public String getCities() {
		return cities;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public void setServiceTaxNumber(String serviceTaxNumber) {
		this.serviceTaxNumber = serviceTaxNumber;
	}

	public void setCities(String cities) {
		this.cities = cities;
	}

}
