package com.bnbneeds.portal.admin.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.EntityApprovalParam.EntityStatus;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionPlanAvailabilityStatus;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionPlanDefaultStatus;
import com.bnbneeds.portal.admin.formbean.EntityAdminApprovalFormBean;
import com.bnbneeds.portal.admin.formbean.SubscriptionPlanUpdateAvailabilityStatusFormBean;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.rest.client.core.BiddingSubscriptionPlans;
import com.bnbneeds.rest.client.core.BiddingSubscriptionPlans.BiddingSubscriptionPlanFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

public abstract class BiddingSubscriptionPlanController extends AbstractBaseController {

	protected static final String LIST_VIEW_TITLE_ATTRIBUTE = "listViewTitle";
	protected static final String LIST_RESPONSE_ATTRIBUTE = "listResponse";
	protected static final String BASE_REQUEST_URL_ATTRIBUTE = "baseRequestURI";
	protected static final String AVAILABLE_STATUS = "AVAILABLE";
	protected static final String UNAVAILABLE_STATUS = "NOT_AVAILABLE";
	protected static final String ENTITY_ADMIN_FORM_BEAN = "entityAdminApprovalFormBean" ;
	protected static final String UPDATE_PLAN_AVAILABILITY_FORM_BEAN = "subscriptionPlanUpdateAvailabilityStatusFormBean" ;
	
	protected static final Map<String, String> availabilityStatuses = new LinkedHashMap<>();
	
	static{
		availabilityStatuses.put(AVAILABLE_STATUS, "Available");
		availabilityStatuses.put(UNAVAILABLE_STATUS, "Not Available");
	}

	public abstract String getViewTitle();

	public abstract String getBaseRequestUrl();
	
	public abstract String getBiddingSubscriptionPlanListView();

	public abstract String getListBiddingSubscriptionPlanReqestURI();

	protected void setBaseRequestUrl(Model model, String baseUrl) {
		model.addAttribute(BASE_REQUEST_URL_ATTRIBUTE, baseUrl);
	}
	
	protected void setViewTitle(Model model, String title) {
		model.addAttribute(LIST_VIEW_TITLE_ATTRIBUTE, title);
	}

	protected abstract BiddingSubscriptionPlanFor getBiddingSubscriptionPlanFor();
	
	protected abstract void setErrorModelForSubscriptionPlansListView(Model model, String object) ;
	
	private BiddingSubscriptionPlans getClientHandle() {
		BiddingSubscriptionPlanFor dealerType = getBiddingSubscriptionPlanFor();

		switch (dealerType) {
		case buyer:
			return bnbneedsClient.buyerBiddingSubscriptionPlans();
		case vendor:
			return bnbneedsClient.vendorBiddingSubscriptionPlans();
		default:
			return null;
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/{id}/delete")
	public String deleteSubscriptionPlan(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String planId, Model model) {

		try {
			getClientHandle().delete(planId);
		} catch (BNBNeedsServiceException e) {
			setErrorModelForSubscriptionPlansListView(model, null);
			return handleBadRequest(e, model, getBiddingSubscriptionPlanListView());
		}

		return getRedirectPath(getListBiddingSubscriptionPlanReqestURI() + "?deleteSuccess");
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{id}/update-entity-status")
	public String updateSubscriptionPlanStatus(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(ENTITY_ADMIN_FORM_BEAN) EntityAdminApprovalFormBean form,
			@PathVariable("id") String entityid, Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.valueOf(form.getApprovalStatus()));
			getClientHandle().updateEntityStatus(entityid, param);
		} catch (BNBNeedsServiceException e) {
			setErrorModelForSubscriptionPlansListView(model, null);
			return handleBadRequest(e, model, getBiddingSubscriptionPlanListView());
		}

		return getRedirectPath(getListBiddingSubscriptionPlanReqestURI() + "?statusChangeSuccess");
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{id}/update-default-status")
	public String setDefaultSubscriptionPlan(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String planId, Model model) {

		try {
			UpdateSubscriptionPlanDefaultStatus param = new UpdateSubscriptionPlanDefaultStatus();
			param.setSetAsDefaultPlan(Boolean.TRUE);
			getClientHandle().updateDefaultStatus(planId, param);
		} catch (BNBNeedsServiceException e) {
			setErrorModelForSubscriptionPlansListView(model, null);
			return handleBadRequest(e, model, getBiddingSubscriptionPlanListView());
		}

		return getRedirectPath(getListBiddingSubscriptionPlanReqestURI() + "?updateSuccess");
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/{id}/update-availability-status")
	public String setPlanAsAvailable(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(UPDATE_PLAN_AVAILABILITY_FORM_BEAN) SubscriptionPlanUpdateAvailabilityStatusFormBean form,
			@PathVariable("id") String planId, Model model) {

		try {
			UpdateSubscriptionPlanAvailabilityStatus param = new UpdateSubscriptionPlanAvailabilityStatus();
			param.setStatus(form.getAvailabilityStatus());
			getClientHandle().updateAvailabilityStatus(planId, param);
		} catch (BNBNeedsServiceException e) {
			setErrorModelForSubscriptionPlansListView(model, null);
			return handleBadRequest(e, model, getBiddingSubscriptionPlanListView());
		}

		return getRedirectPath(getListBiddingSubscriptionPlanReqestURI() + "?updateAvailabilitySuccess");
	}
	
}
