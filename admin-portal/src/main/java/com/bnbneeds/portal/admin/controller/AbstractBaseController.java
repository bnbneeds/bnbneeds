package com.bnbneeds.portal.admin.controller;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;

import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

public abstract class AbstractBaseController {

	private static final Logger logger = LoggerFactory.getLogger(AbstractBaseController.class);

	public static final String REDIRECT_LOGOUT_URL = "redirect:/logout";
	protected static final String MODEL_ATTRIBUTE_MESSAGE = "message";
	protected static final String MODEL_ATTRIBUTE_ERROR_MESSAGE = "errorMessage";

	protected static final String MODEL_ATTRIBUTE_IMAGE_URI = "image_uri";
	protected static final String MODEL_ATTRIBUTE_FORM_TITLE = "formTitle";
	protected static final String MODEL_ATTRIBUTE_FORM_ACTION = "formAction";
	protected static final String ADD_PHOTO_FORM_VIEW = "upload-photo";
	protected static final String UPLOAD_FILE_REQUEST_PARAM_NAME = "file";
	protected static final String CLASSPATH_NO_IMAGE_AVAILABLE_FILE = "classpath:static/external/images/no-image-available.png";
	protected static final String CLASSPATH_BROKEN_IMAGE_FILE = "classpath:static/external/images/broken-image.jpg";
	protected static final String REQ_PARAM_NEXT_OFFSET = "nextOffset";
	protected static final int RESULT_FETCH_SIZE = 100;
	protected static final long SLEEP_TIME_IN_MILLIS = 200;

	@Autowired
	protected BNBNeedsClient bnbneedsClient;

	@Autowired
	protected HttpServletRequest request;

	@Autowired
	protected ResourceLoader resourceLoader;

	protected void setMessage(Model model, String message) {
		model.addAttribute(MODEL_ATTRIBUTE_MESSAGE, message);
	}

	protected void setErrorMessage(Model model, String message) {
		model.addAttribute(MODEL_ATTRIBUTE_ERROR_MESSAGE, message);
	}

	protected byte[] getImageNotAvailable() {
		InputStream in = null;
		byte[] image = null;

		try {
			in = resourceLoader.getResource(CLASSPATH_NO_IMAGE_AVAILABLE_FILE).getInputStream();
			image = FileCopyUtils.copyToByteArray(in);
		} catch (IOException e) {
			// logger.error("IOException occured", e);
		}
		return image;
	}

	protected byte[] getImageNotAvailableIfNull(byte[] photo) {
		return (photo == null || photo.length == 0) ? getImageNotAvailable() : photo;
	}

	protected byte[] getBrokenImage() {
		return getFileInBytes(CLASSPATH_BROKEN_IMAGE_FILE);
	}

	protected byte[] getFileInBytes(String imagePath) {
		InputStream in = null;
		byte[] image = null;

		try {
			in = resourceLoader.getResource(imagePath).getInputStream();
			image = FileCopyUtils.copyToByteArray(in);
		} catch (IOException e) {
			logger.error("IOException occured", e);
		}
		return image;
	}

	protected String getRedirectPath(String endpointURI) {
		StringBuffer buf = new StringBuffer("redirect:");
		buf.append(request.getContextPath());
		if (!endpointURI.startsWith("/")) {
			buf.append("/");
		}
		buf.append(endpointURI);
		return buf.toString();
	}

	protected String getForwardPath(String endpointURI) {
		StringBuffer buf = new StringBuffer("forward:");
		buf.append(request.getContextPath());
		if (!endpointURI.startsWith("/")) {
			buf.append("/");
		}
		buf.append(endpointURI);
		return buf.toString();
	}

	protected String handleBadRequest(BNBNeedsServiceException e, Model model, String failureViewName) {

		switch (e.getHttpCode()) {
		case 400:
		case 403:
		case 404:
			TaskResponse task = e.getTask();
			setErrorMessage(model, task.getMessage());
			break;
		case 411:
			setErrorMessage(model, e.getMessage());
			break;
		default:
			throw e;
		}
		return failureViewName;
	}

	/**
	 * Call this method to make the execution sleep for 200ms
	 * 
	 * @throws InterruptedException
	 */
	protected static void sleep() throws InterruptedException {
		sleep(SLEEP_TIME_IN_MILLIS);
	}

	protected static void sleep(long ms) throws InterruptedException {
		Thread.sleep(ms);
	}
}
