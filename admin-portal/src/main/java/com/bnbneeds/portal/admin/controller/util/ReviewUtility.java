package com.bnbneeds.portal.admin.controller.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.ui.Model;

import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.review.AverageRatingResponse;
import com.bnbneeds.app.model.review.BuyerReviewInfoResponse;
import com.bnbneeds.app.model.review.ProductReviewInfoResponse;
import com.bnbneeds.app.model.review.ReviewInfoListResponse;
import com.bnbneeds.app.model.review.ReviewInfoResponse;
import com.bnbneeds.app.model.review.ReviewParam;
import com.bnbneeds.app.model.review.VendorReviewInfoResponse;
import com.bnbneeds.portal.admin.formbean.ReviewBean;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.core.Reviews;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;

public class ReviewUtility {

	private static final String REVIEW_LIST_RESPONSE = "reviewListResponse";
	private static final int REVIEW_LIST_RESULT_SIZE = 10;
	private static final String REVIEW_AVERAGE_RATING_RESPONSE = "averageRating";

	private static Reviews reviews(BNBNeedsClient bnbneedsClient,
			ReviewFor reviewFor, String entityId) {

		switch (reviewFor) {
		case vendor:
			return bnbneedsClient.vendorReviews(entityId);
		case buyer:
			return bnbneedsClient.buyerReviews(entityId);
		case product:
			return bnbneedsClient.productReviews(entityId);
		default:
			break;
		}
		return null;
	}

	public static ReviewInfoListResponse<? extends ReviewInfoResponse> getReviewListResponse(
			BNBNeedsClient bnbneedsClient, ReviewFor reviewFor,
			String entityId, String reviewId, String reviewedBy,
			String nextOffset) {

		Reviews reviews = reviews(bnbneedsClient, reviewFor, entityId);

		Map<String, String> queryMap = new HashMap<String, String>();

		if (StringUtils.hasText(reviewedBy)) {
			queryMap.put("reviewedBy", reviewedBy);
		}

		ReviewInfoListResponse<? extends ReviewInfoResponse> reviewListResponse = null;

		if (StringUtils.hasText(reviewId)) {
			return reviews.getReview(reviewId);
		}

		if (!queryMap.isEmpty()) {
			if (StringUtils.hasText(nextOffset)) {
				reviewListResponse = reviews.getReviews(
						REVIEW_LIST_RESULT_SIZE, queryMap, nextOffset);
			} else {
				reviewListResponse = reviews.getReviews(
						REVIEW_LIST_RESULT_SIZE, queryMap);
			}
		} else {
			if (StringUtils.hasText(nextOffset)) {
				reviewListResponse = reviews.getReviews(
						REVIEW_LIST_RESULT_SIZE, nextOffset);
			} else {
				reviewListResponse = reviews
						.getReviews(REVIEW_LIST_RESULT_SIZE);
			}

		}

		return reviewListResponse;
	}

	public static AverageRatingResponse getAverageRatingResponse(
			BNBNeedsClient bnbneedsClient, ReviewFor reviewFor, String entityId) {

		return reviews(bnbneedsClient, reviewFor, entityId).getAverageRating();

	}

	public static String getReviewsAsString(BNBNeedsClient bnbneedsClient,
			ReviewFor reviewFor, String entityId, String reviewedBy,
			String nextOffset) {

		Reviews reviews = reviews(bnbneedsClient, reviewFor, entityId);

		Map<String, String> queryMap = new HashMap<String, String>();

		if (StringUtils.hasText(reviewedBy)) {
			queryMap.put("reviewedBy", reviewedBy);
		}

		String reviewListResponse = null;

		if (!queryMap.isEmpty()) {
			if (StringUtils.hasText(nextOffset)) {
				reviewListResponse = reviews.getReviewsAsString(
						REVIEW_LIST_RESULT_SIZE, queryMap, nextOffset);
			} else {
				reviewListResponse = reviews.getReviewsAsString(
						REVIEW_LIST_RESULT_SIZE, queryMap);
			}
		} else {
			if (StringUtils.hasText(nextOffset)) {
				reviewListResponse = reviews.getReviewsAsString(
						REVIEW_LIST_RESULT_SIZE, nextOffset);
			} else {
				reviewListResponse = reviews
						.getReviewsAsString(REVIEW_LIST_RESULT_SIZE);
			}
		}
		return reviewListResponse;
	}

	public static void setReviewListResponse(BNBNeedsClient client,
			ReviewFor reviewFor, String entityId, String reviewedBy,
			String nextOffset, Model model) {

		setReviewListResponse(client, reviewFor, entityId, null, reviewedBy,
				nextOffset, model);
	}

	public static void setReviewListResponse(BNBNeedsClient client,
			ReviewFor reviewFor, String entityId, String reviewId,
			String reviewedBy, String nextOffset, Model model) {

		ReviewInfoListResponse<? extends ReviewInfoResponse> reviewListResponse = getReviewListResponse(
				client, reviewFor, entityId, reviewId, reviewedBy, nextOffset);

		model.addAttribute(REVIEW_LIST_RESPONSE, reviewListResponse);
	}

	public static String getEntityIdForReviewId(BNBNeedsClient client,
			ReviewFor reviewFor, String reviewId) {

		String refEntityId = null;
		//Reviews reviews = reviews(client, reviewFor, null);
		ReviewInfoResponse reviewInfoResponse = client.adminEntities().getReviewById(reviewFor,reviewId);

		switch (reviewFor) {
		case vendor:
			VendorReviewInfoResponse vendorReview = (VendorReviewInfoResponse) reviewInfoResponse;
			refEntityId = vendorReview.getVendor().getId();
			break;
		case buyer:
			BuyerReviewInfoResponse buyerReview = (BuyerReviewInfoResponse) reviewInfoResponse;
			refEntityId = buyerReview.getBuyer().getId();
			break;
		case product:
			ProductReviewInfoResponse productReview = (ProductReviewInfoResponse) reviewInfoResponse;
			refEntityId = productReview.getProduct().getId();
			break;
		default:
			break;
		}

		return refEntityId;
	}

	public static void setAverageRatingResponse(BNBNeedsClient client,
			ReviewFor reviewFor, String entityId, Model model) {

		AverageRatingResponse response = getAverageRatingResponse(client,
				reviewFor, entityId);

		model.addAttribute(REVIEW_AVERAGE_RATING_RESPONSE, response);
	}

	public static boolean hasValidRating(ReviewBean form) {
		int rating = form.getRating();
		return (rating > 0 && rating <= 5);
	}

	public static void postReview(BNBNeedsClient client, ReviewFor reviewFor,
			String entityId, ReviewBean form) {

		ReviewParam param = new ReviewParam();
		param.setRating(form.getRating());
		param.setHeadline(form.getHeadline());
		param.setDescription(form.getReviewDescription());
		String reviewId = form.getReviewId();
		boolean reviewIdPresent = StringUtils.hasText(form.getReviewId());

		if (reviewIdPresent) {
			reviews(client, reviewFor, entityId).updateReview(reviewId, param);
		} else {
			reviews(client, reviewFor, entityId).createReview(param);
		}
	}

	public static void deleteReview(BNBNeedsClient client, ReviewFor reviewFor,
			String entityId, String reviewId) {
		reviews(client, reviewFor, entityId).deleteReview(reviewId);
	}

	public static void updateEntityStatus(BNBNeedsClient client,
			ReviewFor reviewFor, String entityId, String reviewId,
			EntityApprovalParam param) {
		reviews(client, reviewFor, entityId)
				.updateReviewStatus(reviewId, param);
	}
}
