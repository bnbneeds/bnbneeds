package com.bnbneeds.portal.admin.formbean;

public class UpdateBuyerInfoBean extends DealerRegistrationBean {

	private String buyerId;

	public String getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}
}
