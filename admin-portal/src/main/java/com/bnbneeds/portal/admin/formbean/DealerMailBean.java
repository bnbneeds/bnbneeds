package com.bnbneeds.portal.admin.formbean;

public class DealerMailBean {
	
	private String dealerName;
	private String dealerEmailId;
	private boolean existsInMailingList;
	
	public boolean isExistsInMailingList() {
		return existsInMailingList;
	}
	public void setExistsInMailingList(boolean existsInMailingList) {
		this.existsInMailingList = existsInMailingList;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getDealerEmailId() {
		return dealerEmailId;
	}
	public void setDealerEmailId(String dealerEmailId) {
		this.dealerEmailId = dealerEmailId;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		boolean retVal = false;

        if (obj instanceof DealerMailBean){
        	DealerMailBean ptr = (DealerMailBean) obj;
            retVal = ptr.getDealerEmailId().equalsIgnoreCase(this.getDealerEmailId());
        }

     return retVal;
	}

}
