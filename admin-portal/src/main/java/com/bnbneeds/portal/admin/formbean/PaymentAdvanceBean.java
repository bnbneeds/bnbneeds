package com.bnbneeds.portal.admin.formbean;

import org.hibernate.validator.constraints.NotBlank;

public class PaymentAdvanceBean {
	
	@NotBlank
	private String memberId;

	@NotBlank
	private String amountPaid;

	@NotBlank
	private String paymentMode;
	
	private String paymentModeIdentifier;
	
	public String getMemberId() {
		return memberId;
	}
	
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(String amountPaid) {
		this.amountPaid = amountPaid;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	
	public String getPaymentModeIdentifier() {
		return paymentModeIdentifier;
	}
	
	public void setPaymentModeIdentifier(String paymentModeIdentifier) {
		this.paymentModeIdentifier = paymentModeIdentifier;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PaymentAdvanceBean [");
		if (amountPaid != null) {
			builder.append("amountPaid=");
			builder.append(amountPaid);
			builder.append(", ");
		}
		if (paymentMode != null) {
			builder.append("paymentMode=");
			builder.append(paymentMode);
		}
		builder.append("]");
		return builder.toString();
	}

}
