package com.bnbneeds.portal.admin.formbean;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.bnbneeds.portal.admin.util.StringUtils;

public class UpdateMemberProfileBean {

	@NotBlank
	protected String fullName;
	@NotBlank
	protected String address;
	@NotBlank
	protected String bloodGroup;
	@NotNull
	@Pattern(regexp = StringUtils.REGEX_DATE, message = "Invalid Date of Birth.")
	protected String dateOfBirth;
	@NotBlank
	protected String contactNumber;
	@NotBlank
	@Email
	protected String emailAddress;
	@NotBlank
	protected String memberType;
	@NotBlank
	protected String memberId;
	@NotBlank
	protected String designation;
	@NotBlank
	@Size(min = 1, max = 1)
	protected String sex;
	@NotBlank
	protected String section;
	@NotBlank
	protected String emergencyContactName;
	@NotBlank
	protected String emergencyContactNumber;
	@NotBlank
	protected String division;
	protected boolean skinDisease;
	protected boolean epilepsy;
	protected String otherDisease;
	protected String physicalDisability;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getEmergencyContactName() {
		return emergencyContactName;
	}

	public void setEmergencyContactName(String emergencyContactName) {
		this.emergencyContactName = emergencyContactName;
	}

	public String getEmergencyContactNumber() {
		return emergencyContactNumber;
	}

	public void setEmergencyContactNumber(String emergencyContactNumber) {
		this.emergencyContactNumber = emergencyContactNumber;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public boolean isSkinDisease() {
		return skinDisease;
	}

	public void setSkinDisease(boolean skinDisease) {
		this.skinDisease = skinDisease;
	}

	public boolean isEpilepsy() {
		return epilepsy;
	}

	public void setEpilepsy(boolean epilepsy) {
		this.epilepsy = epilepsy;
	}

	public String getOtherDisease() {
		return otherDisease;
	}

	public void setOtherDisease(String otherDisease) {
		this.otherDisease = otherDisease;
	}

	public String getPhysicalDisability() {
		return physicalDisability;
	}

	public void setPhysicalDisability(String physicalDisability) {
		this.physicalDisability = physicalDisability;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MemberRegistrationBean [");
		if (fullName != null) {
			builder.append("fullName=");
			builder.append(fullName);
			builder.append(", ");
		}
		if (address != null) {
			builder.append("address=");
			builder.append(address);
			builder.append(", ");
		}
		if (bloodGroup != null) {
			builder.append("bloodGroup=");
			builder.append(bloodGroup);
			builder.append(", ");
		}
		if (dateOfBirth != null) {
			builder.append("dateOfBirth=");
			builder.append(dateOfBirth);
			builder.append(", ");
		}
		if (contactNumber != null) {
			builder.append("contactNumber=");
			builder.append(contactNumber);
			builder.append(", ");
		}
		if (emailAddress != null) {
			builder.append("emailAddress=");
			builder.append(emailAddress);
			builder.append(", ");
		}
		if (memberType != null) {
			builder.append("memberType=");
			builder.append(memberType);
			builder.append(", ");
		}
		if (memberId != null) {
			builder.append("memberId=");
			builder.append(memberId);
			builder.append(", ");
		}
		if (designation != null) {
			builder.append("designation=");
			builder.append(designation);
			builder.append(", ");
		}
		if (sex != null) {
			builder.append("sex=");
			builder.append(sex);
			builder.append(", ");
		}
		if (section != null) {
			builder.append("section=");
			builder.append(section);
			builder.append(", ");
		}
		if (emergencyContactName != null) {
			builder.append("emergencyContactName=");
			builder.append(emergencyContactName);
			builder.append(", ");
		}
		if (emergencyContactNumber != null) {
			builder.append("emergencyContactNumber=");
			builder.append(emergencyContactNumber);
			builder.append(", ");
		}
		if (division != null) {
			builder.append("division=");
			builder.append(division);
			builder.append(", ");
		}
		builder.append("skinDisease=");
		builder.append(skinDisease);
		builder.append(", epilepsy=");
		builder.append(epilepsy);
		builder.append(", ");
		if (otherDisease != null) {
			builder.append("otherDisease=");
			builder.append(otherDisease);
			builder.append(", ");
		}
		if (physicalDisability != null) {
			builder.append("physicalDisability=");
			builder.append(physicalDisability);
		}
		builder.append("]");
		return builder.toString();
	}

}
