package com.bnbneeds.portal.admin.formbean;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.bnbneeds.portal.admin.util.StringUtils;

public class ChangePasswordBean {

	@NotBlank
	private String currentPassword;
	@NotBlank
	@Size(min = 8, max = 15)
	@Pattern(regexp = StringUtils.REGEX_PASSWORD, message = "Password must have atleast 8 characters. Must contain at least one lower case letter, one upper case letter, one digit and one special character.")
	private String newPassword;
	@NotBlank
	private String confirmPassword;

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ChangePasswordBean [");
		if (currentPassword != null) {
			builder.append("currentPassword=****");
			builder.append(", ");
		}
		if (newPassword != null) {
			builder.append("newPassword=****");
			builder.append(", ");
		}
		if (confirmPassword != null) {
			builder.append("confirmPassword=****");
		}
		builder.append("]");
		return builder.toString();
	}

}
