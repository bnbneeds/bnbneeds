package com.bnbneeds.portal.admin.util;

public interface Constants {

	String DATE_FORMAT = "dd-MMM-yyyy";
	String TIMESTAMP_FORMAT = "dd-MMM-yyyy HH:mm:ss z";
	String TIMESTAMP_FORMAT_FOR_JQUERY_FINAL_COUNT_DOWN = "yyyy/MM/dd HH:mm:ss";
	String APISVC_RESPONSE_TIMESTAMP_FORMAT = "dd-MMM-yyyy HH:mm:ss z";
	String TIME_ZONE_IST = "IST";
}