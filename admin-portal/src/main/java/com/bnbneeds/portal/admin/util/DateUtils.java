package com.bnbneeds.portal.admin.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TimeZone;


public class DateUtils {
	
	private static Calendar calendar = Calendar.getInstance();

	public static Date getFirstDayOfMonth(int month, int year) {
	
		calendar.set(Calendar.MONTH, month-1);
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		
		return calendar.getTime();
	}
	
	public static Date getLastDayOfMonth(int month, int year) {
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.YEAR, year);
		calendar.add(Calendar.DAY_OF_MONTH, -3);
		
		return calendar.getTime();
	}

	public static Date getLastDayOfNextMonth() {
		
		calendar.add(Calendar.MONTH, 2);  
		calendar.set(Calendar.DAY_OF_MONTH, 1);  
		calendar.add(Calendar.DATE, -1); 
		
		return calendar.getTime();
		
	}

	public static Date getTodayAtMidnight() {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		return calendar.getTime();
	}
	
	public static int getCurrentMonth() {
		return calendar.get(Calendar.MONTH) + 1;
	}

	public static int getCurrentYear() {
		return calendar.get(Calendar.YEAR);
	}

	public static List<Date> getDateList (String dateCommaSeparated) throws ParseException {
		
		StringTokenizer dateToken = new StringTokenizer(dateCommaSeparated,",");
		String dateInString = null;
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");;
		List<Date> dateList = new ArrayList<Date>();;
		
		while(dateToken.hasMoreTokens()) {
			dateInString = dateToken.nextToken();
			
			Date date = formatter.parse(dateInString);
			dateList.add(date);
		}

		return dateList;
	}
	
	/**
	 * <p>
	 * Checks if a date is today.
	 * </p>
	 * 
	 * @param date
	 *            the date, not altered, not null.
	 * @return true if the date is today.
	 * @throws IllegalArgumentException
	 *             if the date is <code>null</code>
	 */
	public static boolean isToday(Date date) {
		return isSameDay(date, Calendar.getInstance().getTime());
	}

	/**
	 * <p>
	 * Checks if two dates are on the same day ignoring time.
	 * </p>
	 * 
	 * @param date1
	 *            the first date, not altered, not null
	 * @param date2
	 *            the second date, not altered, not null
	 * @return true if they represent the same day
	 * @throws IllegalArgumentException
	 *             if either date is <code>null</code>
	 */
	public static boolean isSameDay(Date date1, Date date2) {
		if (date1 == null || date2 == null) {
			throw new IllegalArgumentException("The dates must not be null");
		}
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		return isSameDay(cal1, cal2);
	}
	
	/**
	 * <p>
	 * Checks if two calendars represent the same day ignoring time.
	 * </p>
	 * 
	 * @param cal1
	 *            the first calendar, not altered, not null
	 * @param cal2
	 *            the second calendar, not altered, not null
	 * @return true if they represent the same day
	 * @throws IllegalArgumentException
	 *             if either calendar is <code>null</code>
	 */
	public static boolean isSameDay(Calendar cal1, Calendar cal2) {
		if (cal1 == null || cal2 == null) {
			throw new IllegalArgumentException("The dates must not be null");
		}
		return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA)
				&& cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1
					.get(Calendar.DAY_OF_YEAR) == cal2
				.get(Calendar.DAY_OF_YEAR));
	}
	
	/** The maximum date possible. */
	// public static Date MAX_DATE = new Date(Long.MAX_VALUE);

	public static boolean isYesterday(Date date) {
		Calendar c1 = Calendar.getInstance(); // today
		c1.add(Calendar.DAY_OF_YEAR, -1); // yesterday

		Calendar c2 = Calendar.getInstance();
		c2.setTime(date); // your date

		return isSameDay(c1, c2);
	}

	public static SimpleDateFormat getDateFormat(String pattern, String timezone) {
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		df.setTimeZone(TimeZone.getTimeZone(timezone));
		return df;
	}

	public static Date getDate(String pattern, String timestamp) {
		try {
			SimpleDateFormat df = new SimpleDateFormat(pattern);
			return df.parse(timestamp);
		} catch (ParseException e) {
			return null;
		}
	}
}
