package com.bnbneeds.portal.admin.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.portal.admin.controller.util.RequestResponseUtils;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Component
public class BNBNeedsAuthenticationProvider extends
		AbstractUserDetailsAuthenticationProvider {

	@Autowired
	private HttpServletRequest httpServletRequest;

	@Autowired
	private BNBNeedsClient bnbneedsClient;

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		/*
		 * Do nothing here as authentication is done in the retrieveUser()
		 */
	}

	@Override
	protected BNBNeedsUser retrieveUser(String username,
			UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		String password = authentication.getCredentials().toString();
		UserAccountInfoResponse res = authenticateByBNBNeeds(username, password);
		BNBNeedsUser user = new BNBNeedsUser(res);
		return user;
	}

	private UserAccountInfoResponse authenticateByBNBNeeds(String username,
			String password) {
		try {
			String clientIPAddress = RequestResponseUtils
					.getClientIPAddress(httpServletRequest);

			return bnbneedsClient.login(username.toLowerCase(), password,
					clientIPAddress);
		} catch (BNBNeedsServiceException e) {
			if (401 == e.getHttpCode()) {
				throw new BadCredentialsException(
						"Username or password is invalid.");
			} else {
				throw new InternalAuthenticationServiceException(
						"Internal authentication service exception occurred.",
						e);
			}
		} catch (Exception e) {
			throw new InternalAuthenticationServiceException(
					"Internal authentication service exception occurred.", e);
		}
	}
}
