package com.bnbneeds.portal.admin.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.EntityApprovalParam.EntityStatus;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.dealer.UpdateVendorParam;
import com.bnbneeds.app.model.dealer.VendorInfoResponse;
import com.bnbneeds.portal.admin.annotation.Layout;
import com.bnbneeds.portal.admin.controller.util.ReviewUtility;
import com.bnbneeds.portal.admin.formbean.UpdateVendorInfoBean;
import com.bnbneeds.portal.admin.formbean.mapper.DealerFormBeanMapper;
import com.bnbneeds.portal.admin.security.BNBNeedsUser;
import com.bnbneeds.portal.admin.util.StringUtils;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = VendorOperationsController.BASE_REQUEST_PATH)
@PreAuthorize("hasAnyAuthority('ADMIN','SUPER_ADMIN')")
public class VendorOperationsController extends DealerController {

	private static final Logger logger = LoggerFactory.getLogger(VendorOperationsController.class);

	protected static final String BASE_REQUEST_PATH = "/vendors";
	private static final String UPDATE_VENDOR_FORM_VIEW = "vendor-update-form";
	private static final String VENDOR_PROFILE_VIEW = "vendor-profile";
	private static final String UPDATE_VENDOR_FORM_ATTRIBURE = "vendorUpdateFormBean";
	private static final String VENDOR_INFO_ATTRIBUTE = "vendorInfo";
	static final String LIST_VENDORS_VIEW = "vendor-list";

	private static final String LIST_URI = "/list";
	private static final String VENDOR_LIST_URI = BASE_REQUEST_PATH + LIST_URI;

	static void setVendorInfoAttribute(Model model, VendorInfoResponse info) {
		String description = info.getDescription();
		if (StringUtils.hasText(description)) {
			description = StringUtils.convertLineEndingsToHtml(description);
			info.setDescription(description);
		}

		String address = info.getAddress();
		if (StringUtils.hasText(address)) {
			address = StringUtils.convertLineEndingsToHtml(address);
			info.setAddress(address);
		}

		model.addAttribute(VENDOR_INFO_ATTRIBUTE, info);
	}

	@Layout(title = "Vendor Information")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/profile")
	public String getVendorProfile(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String vendorId,
			Model model) {

		try {
			VendorInfoResponse vendorInfoResponse = bnbneedsClient.vendors(vendorId).get();
			setVendorInfoAttribute(model, vendorInfoResponse);
			ReviewUtility.setReviewListResponse(bnbneedsClient, ReviewFor.vendor, vendorId, null, null, model);
			ReviewUtility.setAverageRatingResponse(bnbneedsClient, ReviewFor.vendor, vendorId, model);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, LIST_VENDORS_VIEW);
		}

		return VENDOR_PROFILE_VIEW;

	}

	@Layout(title = "Update Vendor Information")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/update-form")
	public String getUpdateVendorForm(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String vendorId,
			@ModelAttribute(UPDATE_VENDOR_FORM_ATTRIBURE) UpdateVendorInfoBean form, Model model) {

		try {
			VendorInfoResponse vendorInfo = bnbneedsClient.vendors(vendorId).get();

			DealerFormBeanMapper.mapVendorInfoResponseToFormBean(vendorInfo, form);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, UPDATE_VENDOR_FORM_VIEW);
		}

		return UPDATE_VENDOR_FORM_VIEW;
	}

	@Layout(title = "Update Vendor Information")
	@RequestMapping(method = RequestMethod.POST, path = "/update")
	public String updateVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(UPDATE_VENDOR_FORM_ATTRIBURE) @Valid UpdateVendorInfoBean form, BindingResult result,
			Model model) {

		String vendorId = form.getVendorId();
		String mobileNumber = form.getMobileNumber();

		if (!StringUtils.isPhoneNumberValid(mobileNumber)) {
			result.rejectValue("mobileNumber", null, "Mobile number is invalid.");
		}

		if (result.hasFieldErrors()) {
			return UPDATE_VENDOR_FORM_VIEW;
		}

		UpdateVendorParam param = new UpdateVendorParam();
		DealerFormBeanMapper.mapVendorUpadateAttributes(form, param);

		try {
			bnbneedsClient.vendors(vendorId).update(param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, UPDATE_VENDOR_FORM_VIEW);
		}

		setMessage(model, "Organization has been successfully updated.");
		return getRedirectPath(VENDOR_LIST_URI + "?updateSuccess");
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{id}/delete")
	public String deleteVendor(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String vendorId,
			Model model) {

		try {
			UserAccountInfoResponse userAccount = bnbneedsClient.adminEntities()
					.getUserAccountByRelationshipId(vendorId);
			bnbneedsClient.vendors(vendorId).deactivate(true);
			removeDealerFromMailinglist(userAccount.getUsername());
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_PROFILE_VIEW);
		}
		setMessage(model, "Organization has been successfully deleted.");
		return getRedirectPath(VENDOR_LIST_URI + "?deleteSuccess");
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{id}/blacklist")
	public String blacklistVendor(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String vendorId,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.BLACKLISTED);
			bnbneedsClient.vendors().updateEntityStatus(vendorId, param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_PROFILE_VIEW);
		}
		setMessage(model, "Organization has been successfully blacklisted.");
		return getRedirectPath(VENDOR_LIST_URI + "?blacklistSuccess");
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{id}/approve")
	public String approveVendor(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String vendorId,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.APPROVED);
			bnbneedsClient.vendors().updateEntityStatus(vendorId, param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_PROFILE_VIEW);
		}
		setMessage(model, "Organization has been successfully approved.");
		return getRedirectPath(VENDOR_LIST_URI + "?approveSuccess");
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = "/{id}/disapprove")
	public String disapproveVendor(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String vendorId,
			Model model) {

		try {
			EntityApprovalParam param = new EntityApprovalParam();
			param.setStatus(EntityStatus.DISAPPROVED);
			bnbneedsClient.vendors().updateEntityStatus(vendorId, param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_PROFILE_VIEW);
		}
		setMessage(model, "Organization has been successfully disapproved.");
		return getRedirectPath(VENDOR_LIST_URI + "?disapproveSuccess");
	}

}
