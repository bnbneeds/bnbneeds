# Enable routing on CentOS
#
# Packet forwarding must be enabled on your server for this method to work. Open the file /etc/sysctl.conf. Make sure the following line is enabled, and set to "1":
#
# net.ipv4.ip_forward = 1
# Save the file, then run:
# $ sysctl -p
#
#
#
# Display existing rules 
$ iptables -t nat -L
#
# Display existing rules with line numbers
$ iptables -t nat -nL --line-numbers
#
# Delete iptable rule using chanin and line-numbers
$ iptables -D INPUT 10
#
# Delete  prerouting rules
# iptables -t nat -D PREROUTING 6

# iptables -A INPUT -i eth0 -p tcp --dport 8084 -j ACCEPT
