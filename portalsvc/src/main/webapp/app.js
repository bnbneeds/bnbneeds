var bnbNeedsApp = angular
    .module(
        'bnbNeedsApp',
        [
            'ui.router',
            'ngResource',
            'ngImgCrop',
            'LocalStorageModule',
            'ngFileUpload',
            'ui.bootstrap',
            'infinite-scroll',
            'basicAuth',
            'bnbNeedsApp.services'
        ]
    );

bnbNeedsApp
    .constant('STATE_LOGIN', 'login')
    .constant('STATE_SIGNUP', 'signup')
    .constant('STATE_VENDOR_REGISTER', 'vendorRegister')
    .constant('STATE_HOME', 'home')
    .constant('STATE_PRODUCTS', 'products')
    .constant('STATE_PRODUCT_REG_1', 'productRegistration1')
    .constant('STATE_PRODUCT_REG_2', 'productRegistration2')
    .constant('STATE_PRODUCT_REG_3', 'productRegistration3')
    .constant('STATE_PRODUCT_IMG_REG', 'productImageReg')
    .constant('STATE_PRODUCT_DETAILS', 'productDetails')
    .constant('STATE_PRODUCT_EDIT_PHYSICAL', 'productEditPhysical')
    .constant('STATE_PRODUCT_EDIT_ELECTRICAL', 'productEditElectrical')
    .constant('STATE_VENDOR_PROFILE', 'vendorProfile')
    .constant('STATE_VENDOR_PROFILE_EDIT', 'vendorProfileEdit')
    .constant('STATE_OFFERS', 'offers')
    .constant('STATE_OFFER_CREATE', 'offerCreate')
    .constant('STATE_OFFER_DETAILS', 'offerDetails')
    .constant('STATE_OFFER_EDIT', 'offerEdit');

/**
* This is where all the routing is managed.
*
* $routeProvider looks at the address on the
* browser's address bar and loads the 
* template file from the given templateUrl 
* and also injects the controller into the 
* view.
*
* Note: For supporting older and legacy browsers
* which doesn't have full support for HTML5, the
* $routeProvider adds a /#/ after each section
* of the url to keep the browser page from 
* reloading.
*
*/
bnbNeedsApp
    .config(
        [
            '$stateProvider',
            '$httpProvider',
            'localStorageServiceProvider',
            'BASIC_AUTH_REALM',
            'STATE_LOGIN',
            'STATE_SIGNUP',
            'STATE_VENDOR_REGISTER',
            'STATE_HOME',
            'STATE_PRODUCTS',
            'STATE_PRODUCT_REG_1',
            'STATE_PRODUCT_REG_2',
            'STATE_PRODUCT_REG_3',
            'STATE_PRODUCT_IMG_REG',
            'STATE_PRODUCT_DETAILS',
            'STATE_PRODUCT_EDIT_PHYSICAL',
            'STATE_PRODUCT_EDIT_ELECTRICAL',
            'STATE_VENDOR_PROFILE',
            'STATE_VENDOR_PROFILE_EDIT',
            'STATE_OFFERS',
            'STATE_OFFER_CREATE',
            'STATE_OFFER_DETAILS',
            'STATE_OFFER_EDIT',

            function(
                $stateProvider,
                $httpProvider,
                localStorageServiceProvider,
                BASIC_AUTH_REALM,
                STATE_LOGIN,
                STATE_SIGNUP,
                STATE_VENDOR_REGISTER,
                STATE_HOME,
                STATE_PRODUCTS,
                STATE_PRODUCT_REG_1,
                STATE_PRODUCT_REG_2,
                STATE_PRODUCT_REG_3,
                STATE_PRODUCT_IMG_REG,
                STATE_PRODUCT_DETAILS,
                STATE_PRODUCT_EDIT_PHYSICAL,
                STATE_PRODUCT_EDIT_ELECTRICAL,
                STATE_VENDOR_PROFILE,
                STATE_VENDOR_PROFILE_EDIT,
                STATE_OFFERS,
                STATE_OFFER_CREATE,
                STATE_OFFER_DETAILS,
                STATE_OFFER_EDIT
            ) {
        
                $stateProvider
    
                    .state(STATE_LOGIN, {
                        url: '/login',
                        templateUrl: '/pages/login/login.tmpl.html',
                        controller: 'LoginController',
                        controllerAs: 'loginCtrl',
                        data: { requireLogin: false }
                    })

                    .state(STATE_SIGNUP, {
                        url: '/signup',
                        templateUrl: '/pages/signup/signup.tmpl.html',
                        controller: 'SignupController',
                        controllerAs: 'signupCtrl',
                        data: { requireLogin: false }
                    })

                    .state(STATE_HOME, {
                        url: '/home',
                        templateUrl: '/pages/home/home.tmpl.html',
                        controller: 'HomeController',
                        controllerAs: 'homeCtrl',
                        data: { requireLogin: true }
                    })

                    .state(STATE_VENDOR_REGISTER, {
                        url: '/register',
                        templateUrl: '/pages/register/register.tmpl.html',
                        controller: 'RegisterController',
                        controllerAs: 'registerCtrl',
                        data: { requireLogin: true }
                    })

                    .state(STATE_PRODUCTS, {
                        url: '/products',
                        templateUrl: '/pages/products/products.tmpl.html',
                        controller: 'ProductsController',
                        controllerAs: 'productsCtrl',
                        data: { requireLogin: true }
                    })

                    .state(STATE_PRODUCT_REG_1, {
                        url: '/products/register/step1',
                        templateUrl: '/pages/product-register/step-1/product-register-1.tmpl.html',
                        controller: 'ProductRegister1Controller',
                        controllerAs: 'productRegCtrl',
                        data: { requireLogin: true }
                    })

                    .state(STATE_PRODUCT_REG_2, {
                        url: '/products/register/step2',
                        templateUrl: '/pages/product-register/step-2/product-register-2.tmpl.html',
                        controller: 'ProductRegister2Controller',
                        controllerAs: 'productRegCtrl',
                        data: { requireLogin: true }
                    })

                    .state(STATE_PRODUCT_REG_3, {
                        url: '/products/register/step3',
                        templateUrl: '/pages/product-register/step-3/product-register-3.tmpl.html',
                        controller: 'ProductRegister3Controller',
                        controllerAs: 'productRegCtrl',
                        data: { requireLogin: true }
                    })

                    .state(STATE_PRODUCT_EDIT_PHYSICAL, {
                        url: '/products/:productId/edit/physical',
                        templateUrl: '/pages/product-edit/physical/product-edit-physical.tmpl.html',
                        controller: 'ProductEditPhysicalController',
                        controllerAs: 'productEditPhysicalCtrl',
                        data: { requireLogin: true }
                    })

                    .state(STATE_PRODUCT_EDIT_ELECTRICAL, {
                        url: '/products/:productId/edit/electrical',
                        templateUrl: '/pages/product-edit/electrical/product-edit-electrical.tmpl.html',
                        controller: 'ProductEditElectricalController',
                        controllerAs: 'productEditElectricalCtrl',
                        data: { requireLogin: true }
                    })

                    .state(STATE_PRODUCT_DETAILS, {
                        url: '/products/:productId',
                        templateUrl: '/pages/product-details/product-details.tmpl.html',
                        controller: 'ProductDetailsController',
                        controllerAs: 'productDetailsCtrl',
                        data: { requireLogin: true }
                    })

                    .state(STATE_VENDOR_PROFILE, {
                        url: '/profile',
                        templateUrl: '/pages/vendor-profile/vendor-profile.tmpl.html',
                        controller: 'VendorProfileController',
                        controllerAs: 'vendorProfileCtrl',
                        data: { requireLogin: true }
                    })

                    .state(STATE_VENDOR_PROFILE_EDIT, {
                        url: '/profile/edit',
                        templateUrl: '/pages/vendor-profile-edit/vendor-profile-edit.tmpl.html',
                        controller: 'VendorProfileEditController',
                        controllerAs: 'vendorProfileEditCtrl',
                        data: { requireLogin: true }
                    })
                
                    .state(STATE_OFFERS, {
                        url: '/offers',
                        templateUrl: '/pages/offers/offers.tmpl.html',
                        controller: 'ProductOfferController',
                        controllerAs: 'productOfferCtrl',
                        data: { requireLogin: true }
                    })
                
                    .state(STATE_OFFER_CREATE, {
                        url: '/offers/create',
                        templateUrl: '/pages/offer-create/offer-create.tmpl.html',
                        controller: 'OfferCreateController',
                        controllerAs: 'offerCreateCtrl',
                        data: { requireLogin: true }
                    })
                
                    .state(STATE_OFFER_DETAILS, {
                        url: '/offers/:offerId',
                        templateUrl: '/pages/offer-details/offer-details.tmpl.html',
                        controller: 'OfferDetailsController',
                        controllerAs: 'offerDetailsCtrl',
                        data: { requireLogin: true }
                    })
                
                    .state(STATE_OFFER_EDIT, {
                        url: '/offers/:offerId/edit',
                        templateUrl: '/pages/offer-edit/offer-edit.tmpl.html',
                        controller: 'OfferEditController',
                        controllerAs: 'offerEditCtrl',
                        data: { requireLogin: true }
                    });
                
                $httpProvider.defaults.withCredentials = true;

                localStorageServiceProvider
                    .setPrefix('bnbneeds-portal')
                    .setStorageType('localStorage')
                    .setNotify(true, true);

                var interceptor = ['$location', '$q', '$injector', function($location, $q, $injector) {

                    return {
                        responseError: function(response) {
                            if(response.status === 401) {
                                // $state cannot be directly injected because it causes a cyclic dependency.
                                // Hence the $injector.get()
                                var $state = $injector.get('$state');
                                
                                // Redirect on 401 only if the state is not SIGNUP
                                // This is because the authentication token request during signup
                                // is responded with a 401
                                if($state.current.name !== STATE_SIGNUP) {
                                    $state.transitionTo('login');
                                }
                                return $q.reject(response);
                            }
                            else {
                                return $q.reject(response);
                            }
                        }
                    };

                }];

                $httpProvider.interceptors.push(interceptor);

            }
        ]
    );

bnbNeedsApp
    .run(
        [
            '$rootScope',
            '$state',
            'UserAccountService',
            '$http',
            'BASIC_AUTH_REALM',
            'STATE_LOGIN',
            'STATE_HOME',
    
            function(
                $rootScope,
                $state,
                UserAccountService,
                $http,
                BASIC_AUTH_REALM,
                STATE_LOGIN,
                STATE_HOME
            ) {
                
                $http.defaults.headers.common['Request-Source'] = 'portal';
    
                if(!$state.current.name) {
                    $state.go(STATE_HOME);
                }

                $rootScope.$on('$stateChangeSuccess',
                    function(event, toState, toParams, fromState, fromParams) {
                    var requireLogin = toState.data.requireLogin;

                    if(toState.name === STATE_LOGIN) {
                        $rootScope.intentedState = fromState.name;
                    }

                    if (requireLogin && !UserAccountService.getUser()) {
                        event.preventDefault();

                        $state.go(STATE_LOGIN);
                    }
                });

            }
        ]
    );