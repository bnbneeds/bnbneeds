var basicAuth = angular.module('basicAuth', []);

basicAuth
    .constant('URL_LOGIN', '/user-accounts/login')
    .constant('BASIC_AUTH_REALM', 'realm="BNBNeeds Authentication"')
    .constant('URL_TOKEN', '/user-accounts/token')
    .constant('URL_LOGOUT', '/user-accounts/logout');


basicAuth
    .factory('UserAccountService', ['localStorageService', function(localStorageService) {
        var service = {};
        service.user = null;

        service.setUser = setUser;
        service.setVendorId = setVendorId;
        service.getUser = getUser;
        service.clearUser = clearUser;

        function setUser(username, id, vendorId) {
            this.user = this.user || {};
            this.user.username = username;
            this.user.id = id;
            this.user.vendorId = vendorId;
            localStorageService.set('userData', this.user);
        }

        function setVendorId(vendorId) {
            this.user.vendorId = vendorId;
            localStorageService.set('userData', this.user);
        }

        function getUser() {
            this.user = localStorageService.get('userData');
            return this.user;
        }

        function clearUser() {
            this.user = null;
            localStorageService.remove('userData');

        }

        return service;
    }]);

basicAuth
    .factory('AuthenticationService',
        [
            'Base64',
            '$http',
            '$q',
            'URL_BASE',
            'URL_LOGIN',
            'URL_LOGOUT',
            'BASIC_AUTH_REALM',
            'URL_TOKEN',
            'UserAccountService',
            function(
                Base64,
                $http,
                $q,
                URL_BASE,
                URL_LOGIN,
                URL_LOGOUT,
                BASIC_AUTH_REALM,
                URL_TOKEN,
                UserAccountService
            ) {
                var service = {};

                service.BASIC_AUTH_REALM = BASIC_AUTH_REALM;

                service.getToken = getToken;
                service.login = login;
                service.logout = logout;
                service.clearCredentials = clearCredentials;

                function getToken(username, password) {
                    return $http
                        .get(
                            URL_BASE + URL_TOKEN, {
                                headers: {
                                    'Authorization': 'Basic '
                                        + Base64.encode(username + ":" + password)
                                        + " " + service.BASIC_AUTH_REALM
                                }
                            }
                        );
                }

                function login() {
                    return $http.post(URL_BASE + URL_LOGIN, {})
                    .then(function(response) {
                        if(response.data.role === "VENDOR")
                            return response;
                        else
                            return $q.reject("You are registered as a buyer." +
                                " Use the android app if you want to login");
                    })
                    .catch(function(response) {
                        return $q.reject(response.statusText);
                    });
                }

                function logout() {
                  return $http.post(URL_BASE + URL_LOGOUT, "");
                }

                function clearCredentials() {
                    UserAccountService.clearUser();
                }

                return service;
            }
        ]
    );

basicAuth.factory('Base64', function() {
    /* jshint ignore:start */
  
    var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  
    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
  
            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
  
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
  
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
  
                output = output +
                    keyStr.charAt(enc1) +
                    keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) +
                    keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
  
            return output;
        },
  
        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
  
            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
  
            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));
  
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
  
                output = output + String.fromCharCode(chr1);
  
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
  
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
  
            } while (i < input.length);
  
            return output;
        }
    };
  
    /* jshint ignore:end */
});