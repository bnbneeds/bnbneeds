bnbNeedsApp.factory('ProductType', ['$q', 'ProductTypeService', function($q, ProductTypeService) {
    
    var service = {};

    service.getProductTypes = getProductTypes;
    service.getProductTypeById = getProductTypeById;
    service.saveProductType = saveProductType;
    
    function getProductTypes() {
        return new ProductTypeService().$get()
        .then(function(response) {
            if(typeof response.product_type === 'object') {
                return response.product_type;
            } else {
                return $q.reject(response);
            }
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }
    
    function getProductTypeById(id) {
        return new ProductTypeService().$get({id: id})
        .then(function(response) {
            return response.name;
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }
    
    function saveProductType(productType) {
        return new ProductTypeService(productType).$save()
        .then(function(response) {
            if(response.status === 'success') {
                return response.resource;
            } else {
                return $q.reject(response);
            }
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }
    
    return service;
    
}]);