bnbNeedsApp.factory('Product', ['$q', 'ProductService', 'ProductImageService', function($q, ProductService, ProductImageService) {
    
    var service = {};

    service.getAllProducts = getAllProducts;
    service.getProductById = getProductById;
    service.getProductImages = getProductImages;
    
    function getAllProducts(offset) {
        return new ProductService().$get({offset: offset})
        .then(function(response) {
            return response;
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }
    
    function getProductById(productId) {
        return new ProductService().$get({ id: productId })
        .then(function(response) {
            return response;
        })
        .catch(function(response) {
            return $q.reject(response.messsage);
        });
    }
    
    function getProductImages(productId) {
        return new ProductImageService().$get({ id: productId })
        .then(function(response) {
            return response.link;
        })
        .catch(function(response) {
            return $q.reject(response.message);
        });
    }
    
    return service;
    
}]);