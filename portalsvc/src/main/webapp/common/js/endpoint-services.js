/**
* The service layer of the BNB Needs application.
* 
* This layer provides the REST client for all the entities defined on the API service.
*
* The various HTTP methods are exposed hereby through the following functions:
*
* - <ServiceName>.get()
* - <ServiceName>.query()
* - <ServiceName>.save()
* - <ServiceName>.update()
* - <ServiceName>.remove()
* - <ServiceName>.delete()
*
*/
var services = angular.module('bnbNeedsApp.services', []);

services
    .constant('URL_BASE', urlConfig.BASE_IP)
    .constant("PORT", "8080")
    .constant('URL_REL_ACCOUNTS', '/user-accounts')
    .constant('URL_REL_VENDORS', '/vendors/:id')
    .constant('URL_REL_PRODUCT_CATEGORIES', '/product-categories/:id')
    .constant('URL_REL_PRODUCT_TYPES', '/product-types/:id')
    .constant('URL_REL_PRODUCT_NAMES', '/product-names/:id')
    .constant('URL_REL_PRODUCTS', '/products/:id')
    .constant('URL_REL_PRODUCT_IMAGES', '/products/:id/images')
    .constant('URL_REL_VENDOR_PRODUCTS', '/vendors/:vendorId/products/:id')
    .constant('URL_REL_VENDOR_PRODUCT_IMAGES', '/vendors/:vendorId/products/:productId/upload-image-form')
    .constant('URL_REL_PRODUCT_OFFERS', '/product-offers/:id')
    .constant('URL_REL_BUYERS', '/buyers/:id');

services.factory('AccountService', function($resource, URL_BASE, URL_REL_ACCOUNTS) {
    return $resource(URL_BASE + URL_REL_ACCOUNTS);
});

services.factory('VendorService', function($resource, URL_BASE, URL_REL_VENDORS) {
    return  $resource(URL_BASE + URL_REL_VENDORS, {
        id: '@id'
    }, {
        update: { method : 'PUT' }
    });
});

services.factory('ProductCategoryService', function($resource, URL_BASE, URL_REL_PRODUCT_CATEGORIES) {
    return $resource(URL_BASE + URL_REL_PRODUCT_CATEGORIES, {
        id : '@id'
    }, {
        update : { method : 'PUT' }
    });
});

services.factory('ProductTypeService', function($resource, URL_BASE, URL_REL_PRODUCT_TYPES) {
    return $resource(URL_BASE + URL_REL_PRODUCT_TYPES, {
        id : '@id'
    }, {
        update : { method : 'PUT' }
    });
});

services.factory('ProductNameService', function($resource, URL_BASE, URL_REL_PRODUCT_NAMES) {
    return $resource(URL_BASE + URL_REL_PRODUCT_NAMES, {
        id : '@id'
    }, {
        update : { method : 'PUT' }
    });
});

services.factory('ProductService', function($resource, URL_BASE, URL_REL_PRODUCTS) {
    return $resource(URL_BASE + URL_REL_PRODUCTS, {
        id: '@id'
    }, {
        update: { method: 'PUT' }  
    });
});

services.factory('ProductImageService', function($resource, URL_BASE, URL_REL_PRODUCT_IMAGES) {
    return $resource(URL_BASE + URL_REL_PRODUCT_IMAGES, {
        id: '@id'
    });
});

services.factory('VendorProductService', function($resource, UserAccountService, URL_BASE, URL_REL_VENDOR_PRODUCTS) {
    return $resource(URL_BASE + URL_REL_VENDOR_PRODUCTS, {
        vendorId: function() {
            return UserAccountService.getUser().vendorId;
        },
        id: '@id'
    }, {
        update: { method: 'PUT' }
    });
});

services.factory('VendorProductImageService', function($resource, UserAccountService, URL_BASE, URL_REL_VENDOR_PRODUCT_IMAGES) {
    return $resource(URL_BASE + URL_REL_VENDOR_PRODUCT_IMAGES, {
        vendorId: function() {
            return UserAccountService.getUser().vendorId;
        },
        productId: '@id'
    });
});

services.factory('ProductOfferService', function($resource, URL_BASE, URL_REL_PRODUCT_OFFERS) {
    return $resource(URL_BASE + URL_REL_PRODUCT_OFFERS, {
        id: '@id'
    });
})

services.factory('BuyerService', function($resource, URL_BASE, URL_REL_BUYERS) {
    return $resource(URL_BASE + URL_REL_BUYERS, {
        id: '@id'
    });
});
