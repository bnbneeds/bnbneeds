bnbNeedsApp.factory('Offer', ['$q', 'ProductOfferService', function($q, ProductOfferService) {

    var service = {};

    service.createProductOffer = createProductOffer;
    service.getProductOffers = getProductOffers;
    service.getProductOfferById = getProductOfferById;
    service.updateOffer = updateOffer;
    service.deleteOffer = deleteOffer;

    function createProductOffer(productOffer) {
        return new ProductOfferService(productOffer)
            .$save()
            .then(function(response) {
                return response.resource;
            })
            .catch(function(response) {
                return $q.reject(response);
            });
    }

    function getProductOffers() {
        return new ProductOfferService().$get()
            .then(function(response) {
                return response.product_offer;
            })
            .catch(function(response) {
                return $q.reject(response);
            });
    }

    function getProductOfferById(offerId) {
        return new ProductOfferService().$get({ id: offerId })
            .then(function(response) {
                return response.product_offer;
            })
            .catch(function(response) {
                return $q.reject(response);
            });
    }
    
    function updateOffer(updatedOffer) {
        return new ProductOfferService(updatedOffer)
            .$update()
            .then(function(response) {
                if(response.status === 'success') {
                    return response.resource;
                } else {
                    return $q.reject(response);
                }
            });
    }
    
    function deleteOffer(offerId) {
        return new ProductOfferService({id: offerId})
            .$delete()
            .then(function(response) {
                return response;
            })
    }

    return service;

}]);