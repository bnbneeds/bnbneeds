bnbNeedsApp.factory('Buyer', ['$q', 'BuyerService', function($q, BuyerService) {
    service = {};

    service.getBuyers = getBuyers;
    
    function getBuyers() {
        return new BuyerService().$get()
        .then(function(response) {
            return response.data;
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }
    
    return service;
}]);