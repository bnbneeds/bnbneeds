bnbNeedsApp.factory('ProductCategory', ['$q', 'ProductCategoryService', function($q, ProductCategoryService) {
    
    var service = {};

    service.getProductCategories = getProductCategories;
    service.getProductCategoryById = getProductCategoryById;
    service.saveProductCategory = saveProductCategory;
    
    function getProductCategories() {
        return new ProductCategoryService().$get()
        .then(function(response) {
            if(typeof response.product_category === 'object') {
                return response.product_category;
            } else {
                return $q.reject(response);
            }
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }
    
    function getProductCategoryById(id) {
        return new ProductCategoryService().$get({id: id})
        .then(function(response) {
            return response.name;
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }
    
    function saveProductCategory(productCategory) {
        return new ProductCategoryService(productCategory).$save()
        .then(function(response) {
            if(response.status === 'success') {
                return response.resource;
            } else {
                return $q.reject(response);
            }
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }
    
    return service;
    
}]);