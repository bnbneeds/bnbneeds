bnbNeedsApp.factory('Vendor', ['$q', 'VendorService', function($q, VendorService) {
    
    var service = {};
    
    service.getVendorById = getVendorById;
    service.updateVendorProfile = updateVendorProfile;

    function getVendorById(vendorId) {
        return new VendorService().$get({ id: vendorId })
        .then(function(response) {
            return response;
        })
        .catch(function(response) {
            return $q.reject(response.statusText);
        });
    }
    
    function updateVendorProfile(vendorProfile) {
        return new VendorService(vendorProfile)
            .$update()
            .then(function(response) {
                return response;
            })
            .catch(function(response) {
                return $q.reject(response.statusText);
            });
    }
    
    return service;
    
}]);