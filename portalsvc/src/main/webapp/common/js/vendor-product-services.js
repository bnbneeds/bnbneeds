bnbNeedsApp.factory('VendorProduct', ['$q', 'VendorProductService', function($q, VendorProductService) {
    
    var service = {};

    service.getVendorProducts = getVendorProducts;
    service.getVendorProductById = getVendorProductById;
    service.saveVendorProduct = saveVendorProduct;
    service.updateVendorProduct = updateVendorProduct;
    service.deleteVendorProduct = deleteVendorProduct;
    
    function getVendorProducts(offset) {
        return new VendorProductService().$get({ offset: offset })
        .then(function(response) {
            return $q.resolve(response);
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }
    
    function getVendorProductById(id) {
        return new VendorProductService().$get({id: id})
        .then(function(response) {
            if(response.status === 'success') {
                return response.data;
            } else {
                return $q.reject(response);
            }
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }
    
    function saveVendorProduct(vendorProduct) {
        return new VendorProductService(vendorProduct).$save()
        .then(function(response) {
            if(response.status === 'success') {
                return response.resource;
            } else {
                return $q.reject(response);
            }
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }

    function updateVendorProduct(productId, productUpdate) {
        productUpdate.id = productId;
        return new VendorProductService(productUpdate)
            .$update()
            .then(function(response) {
                if(response.status === 'success') {
                    return response.resource;
                } else {
                    return $q.reject(response);
                }
            });
    }
    
    function deleteVendorProduct(productId) {
        return new VendorProductService({ id: productId })
            .$delete()
            .then(function(response) {
                return response;
            });
    }

    return service;
    
}]);