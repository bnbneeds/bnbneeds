bnbNeedsApp.factory('ProductName', ['$q', 'ProductNameService', function($q, ProductNameService) {
    
    var service = {};

    service.getProductNames = getProductNames;
    service.getProductNameById = getProductNameById;
    service.saveProductName = saveProductName;
    
    function getProductNames() {
        return new ProductNameService().$get()
        .then(function(response) {
            if(typeof response.product_name === 'object') {
                return response.product_name;
            } else {
                return $q.reject(response);
            }
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }
    
    function getProductNameById(id) {
        return new ProductNameService().$get({id: id})
        .then(function(response) {
            if(response.status === 'success') {
                return response.name;
            } else {
                return $q.reject(response);
            }
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }
    
    function saveProductName(productName) {
        return new ProductNameService(productName).$save()
        .then(function(response) {
            return response.resource;
        })
        .catch(function(response) {
            return $q.reject(response);
        });
    }
    
    return service;
    
}]);