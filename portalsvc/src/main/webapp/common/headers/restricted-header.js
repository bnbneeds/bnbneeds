bnbNeedsApp
    .controller('HeaderCtrl',
        [
            '$state',
            '$scope',
            'AuthenticationService',
            'STATE_LOGIN',
            'STATE_HOME',
            'STATE_VENDOR_PROFILE',
            'STATE_PRODUCTS',
            'STATE_OFFERS',

            function(
                $state,
                $scope,
                AuthenticationService,
                STATE_LOGIN,
                STATE_HOME,
                STATE_VENDOR_PROFILE,
                STATE_PRODUCTS,
                STATE_OFFERS
            ) {

                $scope.STATE_VENDOR_PROFILE = STATE_VENDOR_PROFILE;
                $scope.STATE_HOME = STATE_HOME;
                $scope.STATE_PRODUCTS = STATE_PRODUCTS;
                $scope.STATE_OFFERS = STATE_OFFERS;

                $scope.logout = function() {
                    AuthenticationService.logout();
                    $state.go(STATE_LOGIN);
                };

            }
        ]
    );