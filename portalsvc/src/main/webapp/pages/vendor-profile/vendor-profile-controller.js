bnbNeedsApp
    .controller('VendorProfileController',
        [
            '$scope',
            '$state',
            'Vendor',
            'UserAccountService',
            'STATE_VENDOR_PROFILE_EDIT',

            function(
                $scope,
                $state,
                Vendor,
                UserAccountService,
                STATE_VENDOR_PROFILE_EDIT
            ) {
    
                var controller = this;

                controller.getVendorProfile = getVendorProfile;

                $scope.vendor = {};
                $scope.STATE_VENDOR_PROFILE_EDIT = STATE_VENDOR_PROFILE_EDIT;

                function getVendorProfile() {
                    Vendor.getVendorById(UserAccountService.getUser().vendorId)
                    .then(function(data) {
                        $scope.vendor = data;
                    });
                }

                controller.getVendorProfile();

            }
        ]
    );