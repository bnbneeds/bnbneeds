bnbNeedsApp.service('productNameModal', ['$uibModal', function($uibModal) {
    
    return function() {
        var instance = $uibModal.open({
            templateUrl: '/pages/product-register/step-1/modals/product-name-modal/product-name-modal.tmpl.html',
            controller: 'ProductNameModalController',
            controllerAs: 'productNameCtrl'
        });
        
        return instance.result;
    };
    
}]);

bnbNeedsApp.controller('ProductNameModalController', ['$q', '$scope', 'ProductName', function($q, $scope, ProductName) {
    
    this.saveProductName = function(productName) {
        ProductName.saveProductName(productName)
        .then(function(data) {
            $scope.$close($q.resolve(data));
        })
        .catch(function(data) {
            return $q.reject(data);
        });
    };
    
    this.cancel = $scope.$dismiss;
    
}]);