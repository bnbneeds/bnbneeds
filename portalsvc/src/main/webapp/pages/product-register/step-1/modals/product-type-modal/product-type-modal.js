bnbNeedsApp.service('productTypeModal', ['$uibModal', function($uibModal) {
    
    return function() {
        var instance = $uibModal.open({
            templateUrl: '/pages/product-register/step-1/modals/product-type-modal/product-type-modal.tmpl.html',
            controller: 'ProductTypeModalController',
            controllerAs: 'productTypeCtrl'
        });
        
        return instance.result;
    };
    
}]);

bnbNeedsApp.controller('ProductTypeModalController', ['$q', '$scope', 'ProductType', function($q, $scope, ProductType) {
    
    this.saveProductType = function(productType) {
        ProductType.saveProductType(productType)
        .then(function(data) {
            $scope.$close($q.resolve(data));
        })
        .catch(function(data) {
            return $q.reject(data);
        });
    };
    
    this.cancel = $scope.$dismiss;
    
}]);