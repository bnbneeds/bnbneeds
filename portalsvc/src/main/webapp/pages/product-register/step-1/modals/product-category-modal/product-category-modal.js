bnbNeedsApp.service('productCategoryModal', ['$uibModal', function($uibModal) {
    
    return function() {
        var instance = $uibModal.open({
            templateUrl: '/pages/product-register/step-1/modals/product-category-modal/product-category-modal.tmpl.html',
            controller: 'ProductCategoryModalController',
            controllerAs: 'productCategoryCtrl'
        });
        
        return instance.result;
    };
    
}]);

bnbNeedsApp.controller('ProductCategoryModalController', ['$q', '$scope', 'ProductCategory', function($q, $scope, ProductCategory) {
    
    this.saveProductCategory = function(productCategory) {
        ProductCategory.saveProductCategory(productCategory)
        .then(function(data) {
            $scope.$close($q.resolve(data));
        })
        .catch(function(data) {
            return $q.reject(data);
        });
    };
    
    this.cancel = $scope.$dismiss;
    
}]);