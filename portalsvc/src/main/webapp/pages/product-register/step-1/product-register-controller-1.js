bnbNeedsApp
    .controller(
        'ProductRegister1Controller',
        [
            '$q',
            '$scope',
            '$state',
            'ProductName',
            'ProductType',
            'ProductCategory',
            'VendorProduct',
            'productNameModal',
            'productTypeModal',
            'productCategoryModal',
            'STATE_PRODUCT_REG_2',

            function
                (
                    $q,
                    $scope,
                    $state,
                    ProductName,
                    ProductType,
                    ProductCategory,
                    VendorProduct,
                    productNameModal,
                    productTypeModal,
                    productCategoryModal,
                    STATE_PRODUCT_REG_2
                )
            {
    
                var controller = this;

                $scope.productNames = [];
                $scope.productTypes = [];
                $scope.productCategories = [];

                $scope.vendorProductData = {};

                controller.productName = null;
                controller.productType = null;
                controller.productCateogry = null;

                controller.initProductNames = initProductNames;
                controller.initProductTypes = initProductTypes;
                controller.initProductCategories = initProductCategories;

                controller.setProductName = setProductName;
                controller.setProductType = setProductType;
                controller.setProductCategory = setProductCategory;

                controller.newProductName = newProductName;
                controller.newProductType = newProductType;
                controller.newProductCategory = newProductCategory;

                controller.saveVendorProduct = saveVendorProduct;

                // Initialize $scope.productNames by fetching from API Service
                function initProductNames() {
                    return ProductName.getProductNames()
                    .then(function(response) {
                        $scope.productNames = response;
                    });
                }

                // Initialize $scope.productTypes by fetching from API Service
                function initProductTypes() {
                    ProductType.getProductTypes()
                    .then(function(response) {
                        $scope.productTypes = response;
                    });
                }

                // Initialize $scope.productCategories by fetching from API Service
                function initProductCategories() {
                    ProductCategory.getProductCategories()
                    .then(function(response) {
                        $scope.productCategories = response;
                    });
                }

                function setProductName(productName) {
                    $scope.vendorProductData.product_name_id = productName.id;
                }

                function setProductType(productType) {
                    $scope.vendorProductData.product_type_id = productType.id;
                }

                function setProductCategory(productCategory) {
                    $scope.vendorProductData.product_category_id = productCategory.id;
                }

                function newProductName () {
                    productNameModal().then(function(data) {
                        controller.initProductNames();
                        return data;
                    });
                }

                function newProductType() {
                    productTypeModal().then(function(data) {
                        controller.initProductTypes();
                    });
                }

                function newProductCategory() {
                    productCategoryModal().then(function(data) {
                        controller.initProductCategories();
                    });
                }

                // save product for vendor using ids of productName,
                // productType and productCategory
                function saveVendorProduct(isValid) {
                    if(isValid) {
                        if(!$scope.vendorProductData.product_name_id) {
                            $scope.message = 'Product name not selected. ' +
                                'Select a product name from the list or create a new product name.';
                            return;
                        }
                        if(!$scope.vendorProductData.product_type_id) {
                            $scope.message = 'Product type not selected. ' +
                                'Select a product type from the list or create a new product type.';
                            return;
                        }
                        if(!$scope.vendorProductData.product_category_id) {
                            $scope.message = 'Product category not selected. ' +
                                'Select a product category from the list or create a new product category.';
                            return;
                        }

                        VendorProduct.saveVendorProduct($scope.vendorProductData)
                        .then(function(response) {
                            $state.get(STATE_PRODUCT_REG_2).data.productId = response.id;
                            $state.go(STATE_PRODUCT_REG_2);
                        });
                    }
                }

                // First the productNames alone are initialized because
                // if all the variables are initialized simultaneously,
                // multiple authentication popups may come up if the
                // authentication token is not present.
                controller.initProductNames().then(function(data) {
                    controller.initProductCategories();
                    controller.initProductTypes();
                });

            }
        ]
    )
;