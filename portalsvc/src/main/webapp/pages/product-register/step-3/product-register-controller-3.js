bnbNeedsApp
    .controller("ProductRegister3Controller",
        [
            '$state',
            '$scope',
            '$timeout',
            'Upload',
            'VendorProduct',
            'VendorProductImageService',
            'STATE_PRODUCTS',

            function(
                $state,
                $scope,
                $timeout,
                Upload,
                VendorProduct,
                VendorProductImageService,
                STATE_PRODUCTS
            ) {

                var controller = this;

                controller.getImageUploadUrl = getImageUploadUrl;
                controller.upload = uploadImage;
                controller.updateServiceAreas = updateServiceAreas;

                function getImageUploadUrl(productId) {
                    new VendorProductImageService()
                        .$get({ productId: productId })
                        .then(
                            function(response) {
                                controller.imgActionUrl = response.action_url;
                                controller.imgName = response.input_file_element_name;
                            }
                        );
                }

                function uploadImage(dataUrl) {
                    var data = {};
                    data[controller.imgName] = Upload.dataUrltoBlob(dataUrl);

                    Upload
                        .upload({
                            url: controller.imgActionUrl,
                            data: data
                        })
                        .then(function(response) {
                            $timeout(function() {
                                $scope.result = response.data;
                            });
                        }, function(response) {
                            if(response.status > 0 ) $scope.errorMsg = response.status;
                        }, function(evt) {
                            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                        });
                }
                    
                function updateServiceAreas(isValid) {
                    if(isValid) {
                        VendorProduct
                            .updateVendorProduct($state.current.data.productId, { service_locations: $scope.serviceAreas });
                        $state.go(STATE_PRODUCTS);
                    } else {
                        $state.go(STATE_PRODUCTS);
                    }
                }

                controller.getImageUploadUrl($state.current.data.productId);

            }
        ]
    );