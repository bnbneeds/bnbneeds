bnbNeedsApp
    .controller("ProductRegister2Controller",
        [
            '$state',
            '$scope',
            'VendorProduct',
            'STATE_PRODUCT_REG_3',

            function ($state,
                      $scope,
                      VendorProduct,
                      STATE_PRODUCT_REG_3) {

                var controller = this;

                $scope.STATE_PRODUCT_REG_3 = STATE_PRODUCT_REG_3;

                controller.updateVendorProduct = updateVendorProduct;

                controller.submitForm = submitForm;
                
                controller.skip = skip;

                function submitForm(isValid) {

                    var productUpdate = { attribute: {} };
                    
                    var hasValue = false;

                    if (isValid) {
                        if ($scope.length) {
                            productUpdate.attribute.length = $scope.length + $scope.lengthUnit;
                            hasValue = true;
                        }
                        if ($scope.breadth) {
                            productUpdate.attribute.height = $scope.breadth + $scope.breadthUnit;
                            hasValue = true;
                        }
                        if ($scope.height) {
                            productUpdate.attribute.height = $scope.height + $scope.heightUnit;
                            hasValue = true;
                        }
                        if ($scope.volume) {
                            productUpdate.attribute.volume = $scope.volume + $scope.volumeUnit;
                            hasValue = true;
                        }
                        if ($scope.weight) {
                            productUpdate.attribute.weight = $scope.weight + $scope.weightUnit;
                            hasValue = true;
                        }
                        if ($scope.electricRating) {
                            productUpdate.attribute.electricRating = $scope.electricRating + $scope.electricRatingUnit;
                            hasValue = true;
                        }
                        if ($scope.powerRating) {
                            productUpdate.attribute.powerRating = $scope.powerRating + $scope.powerRatingUnit;
                            hasValue = true;
                        }
                        if ($scope.minTemp) {
                            productUpdate.attribute.minTemp = $scope.minTemp + $scope.minTempUnit;
                            hasValue = true;
                        }
                        if ($scope.maxTemp) {
                            productUpdate.attribute.maxTemp = $scope.maxTemp + $scope.maxTempUnit;
                            hasValue = true;
                        }

                        if(hasValue) {
                            controller.updateVendorProduct(productUpdate);
                        } else {
                            $state.get(STATE_PRODUCT_REG_3).data.productId = $state.current.data.productId;
                            $state.go(STATE_PRODUCT_REG_3);
                        }
                    }
                }

                function updateVendorProduct(updateProduct) {
                    VendorProduct
                        .updateVendorProduct($state.current.data.productId, updateProduct)
                        .then(function () {
                            $state.get(STATE_PRODUCT_REG_3).data.productId = $state.current.data.productId;
                            $state.go(STATE_PRODUCT_REG_3);
                        })
                        .catch(function () {

                        });
                }
                
                function skip() {
                    $state.get(STATE_PRODUCT_REG_3).data.productId = $state.current.data.productId;
                    $state.go(STATE_PRODUCT_REG_3);
                }

            }
        ]
    );
