bnbNeedsApp
    .controller('ProductEditPhysicalController', 
        [
            '$scope',
            '$state',
            '$stateParams',
            'VendorProduct',
            'STATE_PRODUCT_DETAILS',
            
            function(
                $scope, 
                $state, 
                $stateParams,
                VendorProduct,
                STATE_PRODUCT_DETAILS) {
                    
                    var controller = this;
                    
                    controller.updateProduct = updateProduct;
                    controller.submitForm = submitForm;
                    controller.goBack = goBack;
                  
                    $scope.STATE_PRODUCT_DETAILS = STATE_PRODUCT_DETAILS;
                    
                    function submitForm(isValid) {
                            
                        var productUpdate = { attribute: {} };
                        var hasValue = false;
                        
                        if(isValid) {                            
                            if ($scope.length) {
                                productUpdate.attribute.length = $scope.length + $scope.lengthUnit;
                                hasValue = true;
                            }
                            if ($scope.breadth) {
                                productUpdate.attribute.height = $scope.breadth + $scope.breadthUnit;
                                hasValue = true;
                            }
                            if ($scope.height) {
                                productUpdate.attribute.height = $scope.height + $scope.heightUnit;
                                hasValue = true;
                            }
                            if ($scope.volume) {
                                productUpdate.attribute.volume = $scope.volume + $scope.volumeUnit;
                                hasValue = true;
                            }
                            if ($scope.weight) {
                                productUpdate.attribute.weight = $scope.weight + $scope.weightUnit;
                                hasValue = true;
                            }
                            
                            if(hasValue) {
                                controller.updateProduct(productUpdate);
                            } else {
                                $scope.message = "You haven't made any changes. There is nothing to update.";
                            }
                        }
                    }
                    
                    function updateProduct(productUpdate) {
                        VendorProduct
                            .updateVendorProduct($stateParams.productId, productUpdate)
                            .then(function(response) {
                                $state.go(STATE_PRODUCT_DETAILS, { productId: $stateParams.productId });
                            })
                            .catch(function(response) {
                                $scope.message = response;
                            });
                    }
                    
                    function goBack() {
                        $state.go(STATE_PRODUCT_DETAILS, {productId: $stateParams.productId})
                    }
        
            }
        ]
    );