bnbNeedsApp
    .controller('ProductEditElectricalController', 
        [
            '$scope',
            '$state',
            '$stateParams',
            'VendorProduct',
            'STATE_PRODUCT_DETAILS',
            
            function(
                $scope, 
                $state, 
                $stateParams,
                VendorProduct,
                STATE_PRODUCT_DETAILS) {
                    
                    var controller = this;
                    
                    controller.updateProduct = updateProduct;
                    controller.submitForm = submitForm;
                    controller.goBack = goBack;
                  
                    $scope.STATE_PRODUCT_DETAILS = STATE_PRODUCT_DETAILS;
                    
                    function submitForm(isValid) {
                            
                        var productUpdate = { attribute: {} };
                        var hasValue = false;
                        
                        if(isValid) {
                            if ($scope.electricRating) {
                                productUpdate.attribute.electricRating = $scope.electricRating + $scope.electricRatingUnit;
                                hasValue = true;
                            }
                            if ($scope.powerRating) {
                                productUpdate.attribute.powerRating = $scope.powerRating + $scope.powerRatingUnit;
                                hasValue = true;
                            }
                            if ($scope.minTemp) {
                                productUpdate.attribute.minTemp = $scope.minTemp + $scope.minTempUnit;
                                hasValue = true;
                            }
                            if ($scope.maxTemp) {
                                productUpdate.attribute.maxTemp = $scope.maxTemp + $scope.maxTempUnit;
                                hasValue = true;
                            }
                            
                            if(hasValue) {
                                controller.updateProduct(productUpdate);
                            } else {
                                $scope.message = "You haven't made any changes. There is nothing to update.";
                            }
                        }
                    }
                    
                    function updateProduct(productUpdate) {
                        VendorProduct
                            .updateVendorProduct($stateParams.productId, productUpdate)
                            .then(function(response) {
                                $state.go(STATE_PRODUCT_DETAILS, { productId: $stateParams.productId });
                            })
                            .catch(function(response) {
                                $scope.message = response;
                            });
                    }
                    
                    function goBack() {
                        $state.go(STATE_PRODUCT_DETAILS, {productId: $stateParams.productId})
                    }
        
            }
        ]
    );