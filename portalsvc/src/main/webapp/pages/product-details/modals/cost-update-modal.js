bnbNeedsApp
    .service('CostUpdateModal', ['$uibModal', function($uibModal) {
        
        return function() {
            var instance = $uibModal.open({
                templateUrl: '/pages/product-details/modals/cost-update-modal.tmpl.html',
                controller: 'CostUpdateModalController',
                controllerAs: 'costUpdateModalCtrl'
            });
            
            return instance.result;
        };
        
    }])

    .controller('CostUpdateModalController', ['$q', '$scope', '$stateParams', 'VendorProduct', function($q, $scope, $stateParams, VendorProduct) {
    
        this.update = function() {
            var updatedProduct = $scope.updatedProduct,
                productId = $stateParams.productId;
            VendorProduct
                .updateVendorProduct(productId, updatedProduct)
                .then(function(data) {
                    $scope.$close($q.resolve(data));
                })
                .catch(function(data) {
                    return $q.reject(data);
                });
        };

        this.cancel = $scope.$dismiss;
    
    }]);