bnbNeedsApp
    .controller('ProductDetailsController',
        [
            '$state',
            '$stateParams',
            '$scope',
            'Product',
            'VendorProduct',
            'ProductName',
            'ProductType',
            'ProductCategory',
            'CostUpdateModal',
            'STATE_PRODUCT_EDIT_PHYSICAL',
            'STATE_PRODUCT_EDIT_ELECTRICAL',
            'STATE_PRODUCTS',

            function(
                $state,
                $stateParams,
                $scope,
                Product,
                VendorProduct,
                ProductName,
                ProductType,
                ProductCategory,
                CostUpdateModal,
                STATE_PRODUCT_EDIT_PHYSICAL,
                STATE_PRODUCT_EDIT_ELECTRICAL,
                STATE_PRODUCTS
            ) {
    
                var controller = this;

                controller.getDetails = getDetails;
                controller.getProductType = getProductType;
                controller.getProductCategory = getProductCategory;
                controller.getImage = getImage;
                controller.deleteProduct = deleteProduct;
                controller.updatePrice = updatePrice;
                controller.editProductPhysical = editProductPhysical;
                controller.editProductElectrical = editProductElectrical;
                
                function getDetails() {
                    var productId = $stateParams.productId;
                    return Product
                        .getProductById(productId)
                        .then(function(data) {
                            $scope.product = data;
                            controller.productId = $scope.product.id;
                            controller.getProductCategory($scope.product.product_category.id);
                            controller.getProductType($scope.product.product_type.id);
                        })
                        .catch(function(message) {
                            $scope.message = message;
                        });
                }

                function getProductType(productTypeId) {
                    ProductType
                        .getProductTypeById(productTypeId)
                        .then(function(data) {
                            $scope.productType = data;
                        });
                }

                function getProductCategory(productCategoryId) {
                    ProductCategory
                        .getProductCategoryById(productCategoryId)
                        .then(function(data) {
                            $scope.productCategory = data;
                        });
                }

                function getImage(productId) {
                    return Product
                        .getProductImages(productId)
                        .then(function(data) {
                            $scope.productImageUrls = data;
                        });
                }
                  
                function deleteProduct() {
                    var productId = $stateParams.productId;
                    VendorProduct
                        .deleteVendorProduct(productId)
                        .then(function() {
                            $state.go(STATE_PRODUCTS);
                        });
                }
                    
                function updatePrice() {
                    CostUpdateModal()
                        .then(function() {
                            controller.getDetails();
                        });
                }
                    
                function editProductPhysical() {
                    $state.go(STATE_PRODUCT_EDIT_PHYSICAL, {productId: $stateParams.productId});
                }
                    
                function editProductElectrical() {
                    $state.go(STATE_PRODUCT_EDIT_ELECTRICAL, {productId: $stateParams.productId});
                }

                controller
                    .getDetails()
                    .then(
                        controller.getImage($stateParams.productId)
                    );

            }
        ]
    );