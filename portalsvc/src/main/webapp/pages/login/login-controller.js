bnbNeedsApp
    .controller('LoginController',
        [
            '$scope',
            '$state',
            '$rootScope',
            'AuthenticationService',
            'UserAccountService',
            'STATE_HOME',
            'STATE_VENDOR_REGISTER',
            'STATE_SIGNUP',

            function (
                $scope,
                $state,
                $rootScope,
                AuthenticationService,
                UserAccountService,
                STATE_HOME,
                STATE_VENDOR_REGISTER,
                STATE_SIGNUP
            ) {

                $scope.signup = signup;
                $scope.login = login;
    
                AuthenticationService.clearCredentials();

                function login(isValid) {

                    if(isValid) {
                        $scope.dataLoading = true;

                        if($scope.username && $scope.password) {
                            AuthenticationService.getToken($scope.username, $scope.password)
                            .then(
                                function(response) {
                                    AuthenticationService
                                        .login()
                                        .then(
                                            function(response) {
                                                UserAccountService
                                                    .setUser(
                                                        response.data.name,
                                                        response.data.id,
                                                        response.data.relationship_id
                                                    );

                                                if(UserAccountService.getUser().vendorId) {
                                                    $state.go(STATE_HOME);
                                                } else {
                                                    $state.go(STATE_VENDOR_REGISTER);
                                                }
                                            }
                                        )
                                        .catch(
                                            function(message) {
                                                AuthenticationService.clearCredentials();
                                                $scope.error = message;
                                                $scope.dataLoading = false;
                                            }
                                        );
                                }
                            )
                            .catch(
                                function(response) {
                                    AuthenticationService.clearCredentials();
                                    $scope.error = response.statusText;
                                    $scope.dataLoading = false;
                                }
                            );
                        } else {
                            $scope.error = "All fields are mandatory";
                        }
                    }
                }

                function signup() {
                    $state.go(STATE_SIGNUP);
                }
            }
        ]
    );