bnbNeedsApp
    .controller(
        'OfferCreateController', [
        '$state',
        '$scope',
        'Offer',
        'VendorProduct',
        'STATE_OFFERS',
        
        function (
            $state,
            $scope,
            Offer,
            VendorProduct,
            STATE_OFFERS) {

                var controller = this;

                controller.listProducts = listProducts;
                controller.saveOffer = saveOffer;
                
                $scope.startDateOpen = false;
                $scope.endDateOpen = false;
                
                $scope.openStartDatePicker = openStartDatePicker;
                $scope.openEndDatePicker = openEndDatePicker;

                function listProducts() {
                    VendorProduct
                        .getVendorProducts()
                        .then(function(data) {
                            $scope.productList = data;
                        });
                }

                function saveOffer(isValid) {
                    if(isValid) {
                        $scope.offer.product_id = $scope.product.id;
                        $scope.offer.start_date = getFormattedDate($scope.startDate);
                        $scope.offer.end_date = getFormattedDate($scope.endDate);
                        
                        Offer
                            .createProductOffer($scope.offer)
                            .then(function() {
                                $state.go(STATE_OFFERS);
                            })
                            .catch(function(response) {
                                $scope.message = response.message;
                            });
                    } else {
                        $scope.message = 'Please fill out the mandatory fields';
                    }
                }
                
                function openStartDatePicker() {
                    $scope.startDateOpen = true;
                }
                
                function openEndDatePicker() {
                    $scope.endDateOpen = true;
                }
                
                function getFormattedDate(convertDate) {
                    var date = convertDate.getDate(),
                        month = convertDate.getMonth() + 1,
                        year = convertDate.getFullYear();
                    
                    if (month < 10) month = '0' + month;
                    if (date < 10) date = '0' + date;

                    
                    return [date, month, year].join('-');
                }
                
                controller.listProducts();
            }
        ]
    );