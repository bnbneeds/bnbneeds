bnbNeedsApp
    .controller('SignupController',
        [
            'AccountService',
            'AuthenticationService',
            '$state',
            '$scope',
            'STATE_LOGIN',

            function(
                Account,
                AuthenticationService,
                $state,
                $scope,
                STATE_LOGIN
            ) {

                $scope.addAccount = addAccount;
                $scope.STATE_LOGIN = STATE_LOGIN;

                function addAccount(isValid) {
                    if(isValid)
                    {
                        if($scope.accountData.password === $scope.confirmPassword) {
                            $scope.accountData.account_type = 'VENDOR';
                            var account = new Account($scope.accountData);

                            account
                                .$save()
                                .then(function(response) {
                                    $state.transitionTo(STATE_LOGIN);
                                })
                                .catch(function(response) {
                                    $scope.message = response.data.message;
                                });
                        } else {
                            $scope.message = 'The passwords entered do not match.';
                        }
                    }
                }

                AuthenticationService.getToken('', '');

            }
        ]
    );