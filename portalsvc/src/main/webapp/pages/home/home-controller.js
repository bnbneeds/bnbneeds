bnbNeedsApp
    .controller('HomeController',
        [
            'Buyer',
            '$state',
            '$scope',

            function (
                Buyer,
                $state,
                $scope
            ) {

                Buyer
                    .getBuyers()
                    .then(function(response) {
                        $scope.buyersList = response;
                    })
                    .catch(function(response) {
                        $scope.message = response.statusText;
                    });
            }
        ]
    );