bnbNeedsApp
    .controller(
        'VendorProfileEditController',
        [
            '$state', 
            '$scope', 
            'Vendor',
            'UserAccountService',
            'STATE_VENDOR_PROFILE',

            function ($state, $scope, Vendor, UserAccountService, STATE_VENDOR_PROFILE) {

                var controller = this;

                controller.updateVendorProfile = updateVendorProfile;
                controller.submitForm = submitForm;
                
                $scope.STATE_VENDOR_PROFILE = STATE_VENDOR_PROFILE;

                function submitForm(isValid) {
                    if(isValid) {
                        updateVendorProfile($scope.vendorUpdate);
                    }
                }
                
                function updateVendorProfile(newVendorProfile) {
                    newVendorProfile.id = UserAccountService.getUser().vendorId;
                    Vendor
                        .updateVendorProfile(newVendorProfile)
                        .then(function (response) {
                            $state.go(STATE_VENDOR_PROFILE);
                        })
                        .catch(function (response) {
                            $scope.message = response;
                        });
                }

            }
        ]
    );