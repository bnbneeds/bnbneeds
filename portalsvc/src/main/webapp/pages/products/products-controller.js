bnbNeedsApp
    .controller('ProductsController',
        [
            '$scope',
            '$state',
            'Product',
            'ProductCategoryService',
            'STATE_PRODUCT_DETAILS',
            'STATE_PRODUCT_REG_1',

            function(
                $scope,
                $state,
                Product,
                ProductCategory,
                STATE_PRODUCT_DETAILS,
                STATE_PRODUCT_REG_1
            ) {
    
                var controller = this;
                
                controller.next_offset = null;
                controller.busy = false;
                
                $scope.productList = [];
                $scope.STATE_PRODUCT_REG_1 = STATE_PRODUCT_REG_1;

                $scope.productDetails = productDetails;
                $scope.updateProductList = updateProductList;

                function updateProductList() {
                    if(controller.next_offset !== '' && !controller.busy) {
                        controller.busy = true;
                        Product
                            .getAllProducts(controller.next_offset)
                            .then(function(data) {
                                var products = data.product_info;
                                if(typeof products !== 'undefined') {
                                    $scope.productList = $scope.productList.concat(products);
                                    if(controller.next_offset !== data.next_result_offset) {
                                        controller.next_offset = data.next_result_offset;
                                    } else {
                                        controller.next_offset = '';
                                    }
                                    controller.busy = false;
                                }
                            });
                    }
                }

                function productDetails(productId) {
                    $state.go(STATE_PRODUCT_DETAILS, { productId: productId });
                }

            }
        ]
    );