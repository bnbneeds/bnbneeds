bnbNeedsApp.controller(
    'ProductOfferController',
    [
        '$q',
        '$scope',
        '$state',
        'Offer',
        'STATE_OFFER_CREATE',
        'STATE_OFFER_DETAILS',
        function(
            $q,
            $scope,
            $state, 
            Offer,
            STATE_OFFER_CREATE,
            STATE_OFFER_DETAILS
        ) {

            var controller = this;
                
            $scope.STATE_OFFER_CREATE = STATE_OFFER_CREATE;
                
            $scope.offerDetails = offerDetails;
            
            controller.getProductOffersList = getProductOffersList;
            controller.deleteOffer = deleteOffer;
                
            function getProductOffersList() {
                Offer
                    .getProductOffers()
                    .then(function(response) {
                        $scope.offers = response;
                    })
                    .catch(function(response) {
                        $q.reject(response);
                    });
            }
                
            function offerDetails(offerId) {
                $state.go(STATE_OFFER_DETAILS, { offerId: offerId });
            }
            
            function deleteOffer(offerId) {
                Offer
                    .deleteOffer(offerId)
                    .then(function() {
                        controller.getProductOffersList();
                    });
            }
                
            controller.getProductOffersList();

        }
    ]
);