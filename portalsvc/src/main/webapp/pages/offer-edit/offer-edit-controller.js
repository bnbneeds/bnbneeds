bnbNeedsApp
    .controller('OfferEditController', ['$state', '$scope', 'Offer', 'STATE_OFFERS', function($state, $scope, Offer, STATE_OFFERS) {
        var controller = this;
        
        controller.updateOffer = updateOffer;
        
        function updateOffer(updatedOffer) {
            Offer
                .updateOffer(updatedOffer)
                .then(function(response) {
                    $state.go(STATE_OFFERS);
                })
                .catch(function(response) {
                    $scope.message = response;
                });
        }
    }]);