bnbNeedsApp
    .controller('RegisterController',
        [
            'VendorService',
            'UserAccountService',
            '$state',
            '$scope',
            'STATE_HOME',
            'STATE_LOGIN',

            function(
                Vendor,
                UserAccountService,
                $state,
                $scope,
                STATE_HOME,
                STATE_LOGIN
            ) {

                var controller = this;

                controller.registerVendor = registerVendor;
                  
                $scope.STATE_LOGIN = STATE_LOGIN;

                function registerVendor(isValid) {

                    if(isValid) {

                        var vendor = new  Vendor($scope.vendor);

                        vendor
                            .$save()
                            .then(function(response) {
                                UserAccountService.setVendorId(response.resource.id);
                                $state.go(STATE_HOME);
                            })
                            .catch(function(response) {
                                $scope.message = response.data.message;
                            });
                    }

                }

            }
        ]
    );