# BNB Needs Portal Service: README
***

## Overview
***

Single page web application built using AngularJS 1.4.

The main application code can be found in `app.js` and the base page is `index.html`.

Apart from these, the code is basically structured into two folders

 - `common`
 - `pages`
 
### Common Folder
This folder contains all components which are commonly used by any of the pages on the application. This includes scripts and templates for common components like headers.

### Pages Folder
This folder contains all the different pages grouped further into subfolders containing the page templates and their controllers.

***

## Tools
***
All webapp dependecies are managed by bower. Dependencies can be viewed by opening `bower.json` in the `webapp` folder.

***

## Running the application
***
First of all, do a clean build of the Maven project by running

`mvn clean install`

on the root directory of the webapp.

Then execute the `run-webapp` file from any UNIX terminal using:

`./run-webapp`

This will start the web application on Jetty 9.2 server on port `8081`.

