var IDLE_TIMEOUT_IN_MS = 45000;
var ON_LOAD_NOTIFICATION_POLL_INTERVAL_IN_MS = 5000;
var NOTIFICATION_POLL_INTERVAL_IN_MS = 50000;
var IDLE_TIME_NOTIFICATION_POLL_INTERVAL_IN_MS = 120000;

var ratingOptions = {
	displayOnly: true,
	step: 1,
	size:'xs',
	showCaption: false,
	showClear: false,
	emptyStar: '<i class="fa fa-star-o" aria-hidden="true"></i>',
	filledStar: '<i class="fa fa-star" aria-hidden="true"></i>'
};

$(document).ready(function () {
	
	$('.btn-delete').addClass('btn btn-danger').prepend('<i class="fa fa-trash-o" aria-hidden="true"></i> ');
    $('.btn-add').addClass('btn btn-primary').prepend('<i class="fa fa-plus" aria-hidden="true"></i> ');
    $('.btn-update').addClass('btn btn-primary').prepend('<i class="fa fa-pencil" aria-hidden="true"></i> ');
    $('.btn-submit').addClass('btn btn-primary').prepend('<i class="fa fa-check-square-o" aria-hidden="true"></i> ');
    $('.btn-next').addClass('btn btn-primary').append(' <i class="fa fa-caret-right" aria-hidden="true"></i>');
    $('.btn-previous').addClass('btn btn-default').prepend('<i class="fa fa-caret-left" aria-hidden="true"></i> ');
    $('.btn-close').addClass('btn btn-default').prepend('<i class="fa fa-times" aria-hidden="true"></i> ');
    $('.btn-reset').addClass('btn btn-default').prepend('<i class="fa fa-repeat" aria-hidden="true"></i> ');
    $('.btn-filter').addClass('btn btn-primary').prepend('<i class="fa fa-filter" aria-hidden="true"></i> ');
    $('.btn-upload').addClass('btn btn-primary').prepend('<i class="fa fa-upload" aria-hidden="true"></i> ');
    $('.btn-list').addClass('btn').prepend('<i class="fa fa-bars" aria-hidden="true"></i> ');
    $('.btn-expand').addClass('btn').prepend('<i class="fa fa-plus-square" aria-hidden="true"></i> ');
    $('.error').addClass('text-danger').prepend('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ');
    $('.btn-more-info').addClass('btn btn-primary').prepend('<i class="fa fa-info-circle" aria-hidden="true"></i> ');
    $('.btn-image').addClass('btn btn-primary').prepend('<i class="fa fa-file-image-o" aria-hidden="true"></i> ');
	$('a.map').prepend('<i class="fa fa-map-marker" title="Address"></i> ').addClass('show-hand no-underline').attr('title', 'Locate this on Google Maps').attr('target', '_blank').click(function() {
		var src = "https://maps.google.com/?q=" + $(this).text();
		$(this).attr('href', src)
	});
	
	$('.display-rating').rating(ratingOptions);
	
	
	$('.focus').focus();
	
    //Initialize tooltips
	var tootipOptions = {
		trigger: "hover",
		show : {
			effect: "blind",
			duration: 800
		},
		hide : {
			effect: "explode",
			duration: 1000
		}
	}
	
	$('.tool-tip').tooltip(tootipOptions);
    $('a[title]').tooltip(tootipOptions);
    $('button[title]').tooltip(tootipOptions);
    $('.btn-pop').popover();
    
    // Customize select dropdown
    $('.selectpicker').selectpicker({
    	liveSearch: true,
    	showTick: true,
    	width: '100%'
    });
    
    new WOW().init();
    
    $("input.datepicker").singleDatePicker();
    
    $('input.date-range-selector').daterangepicker({
	      autoUpdateInput: false,
	      showDropdowns: true,
	      autoApply: true,
	      locale: {
	    	  format: 'DD-MMM-YYYY',
	          cancelLabel: 'Clear'
	      }
	});
    
    $.idleTimer({
        timeout: IDLE_TIMEOUT_IN_MS,
        idle: true
    });
    
	$('input.date-range-selector').on('apply.daterangepicker', function(ev, picker) {
	    $(this).val(picker.startDate.format('DD-MMM-YYYY') + ' to ' + picker.endDate.format('DD-MMM-YYYY'));
	    $(this).trigger('change');
	});

	$('input.date-range-selector').on('cancel.daterangepicker', function(ev, picker) {
	    $(this).val('');
	    $(this).trigger('change');
	});
    
    // Add fade effect to modals
    $('.modal').on('shown.bs.modal', function() {
      $(this).find('[autofocus]').focus();
    });
    
    $("img.lazy").unveil();
    
    $("button.go-back").click(function() {
		parent.history.back();
		return false;
	});
    
    $('button.print-page-btn').click(function() {
		window.print();
	});
    
    $(".list-item-hover").hover(function() {
        $(this).toggleClass('list-group-item-info');
    });
    
    $(".dropdown-hover").hover(
    		function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
            }
    );
    
    $('li.dropdown.open-dropdown a').on('click', function(event) {
        $(this).parent().toggleClass("open");
    });

    $('body').on('click', function(e) {
        if (!$('li.dropdown.open-dropdown').is(e.target) && $('li.dropdown.open-dropdown').has(e.target).length === 0 && $('.open').has(e.target).length === 0) {
            $('li.dropdown.open-dropdown').removeClass('open');
        }
    });
    
	jQuery.validator.addMethod("validateYoutubeURL", function(value, element) {
	    return this.optional(element) || /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);
	}, "Invalid YouTube video link.");
	jQuery.validator.addClassRules('youtube-url', {validateYoutubeURL: true});
	// Show spinner on action buttons on click and disable them
    $('.btn-action').click(function(e) {
    	var $btn = $(this);
    	
    	if($btn.attr('type') == 'submit') {
    		// var form = $btn.parents('form:first');
    		var form = $btn.closest('form');
	    	if($(form).length) {
	    		var options = {
	    			errorElement: 'div',
	    			errorClass: 'text-danger small',
	    			highlight: function(element) {
		    			$(element).parent("div").addClass('has-error');
		    	    },
		    		unhighlight: function(element) {
		    			$(element).parent('div').removeClass('has-error');
		    	    }
		    	    
	    		}
	    		
	    		var validator = $(form).validate(options);
	    		
	    		$(form).find('input:text, textarea').each(function() {
	    			var inputVal = $(this).val();
	    			$(this).val($.trim(inputVal));
	    	    });
	    		if(validator.form()) {
	    			$btn.off('click');
	    			showSpinnerOnButton($btn);
	    			freezePage();
	    			form.submit(); 
	    		}
	    	}
    	} else {
    		e.preventDefault();
    		$btn.off('click');
    		showSpinnerOnButton($btn);
    		freezePage();
    		if($btn.is('a')) {
    			var url = $btn.attr('href');
    			$btn.attr('href', 'javascript:void(0);');
    			window.location = url;
    		}
    	}
    });
});


function showSpinnerOnButton(button) {
	var textNode = button.text();
	button.val = '';
	button.empty();
	button.append(
		$('<i/>', {'class': 'fa fa-refresh fa-spin'})
	);
	button.append(' ' + textNode + ' ');
	button.attr('disabled', 'disabled');
}

function validateYoutubeURL(url) {
	
    if (url.length > 0) {
    	//var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        //var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
    	var regExp = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        var match = url.match(regExp);
        if (match) {
        	return true;
        } else { 
        	return false; 
        }
    }
    return true;
}

function notifySuccess(text) {
	$.notify({
		icon: 'fa fa-check-square',
		message: text
	},{
		type: 'success',
		offset: {
			y: 80,
			x: 20
		}
	});
}

function freezePage() {
	var div= document.createElement("div");
    div.className += "disable-page";
    document.body.appendChild(div);
}

function reloadPage(queryParam) {
	var url = window.location + '';
	if(queryParam != null) {
		url = url.split(/[?#]/)[0];
		url += "?" + queryParam;
	}
	window.location = url;
}

function urlContainsQueryParam(queryParam) {
	return (window.location.href.indexOf(queryParam) > -1);
}

$.fn.singleDatePicker = function() {
	$(this).on("apply.daterangepicker", function(e, picker) {
	    picker.element.val(picker.startDate.format(picker.locale.format));
	    $(this).trigger('change');
	});
	return $(this).daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		autoUpdateInput: false,
		autoApply: true,
		locale: {
			format: 'DD-MMM-YYYY'
		}
	});
};


$(window).scroll(function() {
	  if ($(window).scrollTop() > 300) {
	    $('button.back-to-top').addClass('show');
	  } else {
	    $('button.back-to-top').removeClass('show');
	  }
	});

	$(document).on('click', 'button.back-to-top', function() {
	  $('html, body').animate({
	    scrollTop: 0
	  }, 800);
	  return false;
	});
