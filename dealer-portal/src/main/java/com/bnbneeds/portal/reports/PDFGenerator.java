package com.bnbneeds.portal.reports;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.model.dealer.order.AggregatedPurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemInfoResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseListResponse;
import com.bnbneeds.portal.dealer.util.Constants;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.color.DeviceCmyk;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.VerticalAlignment;

public class PDFGenerator {

	private static final Logger logger = LoggerFactory.getLogger(PDFGenerator.class);

	private static final String CLASSPATH_LOGO_FILE = "classpath:static/external/images/bnbneeds-logo.png";
	private static final float BODY_HEADER_FONT_SIZE = 10;
	private static final Color TABLE_TITLE_FONT_COLOR = Color.WHITE;
	private static final float TABLE_TITLE_FONT_SIZE = 9.0f;
	private static final Color TABLE_BORDER_COLOR = Color.GRAY;
	private static final float BODY_FONT_SIZE = 9;
	private static final float TOP_MARGIN = 130;
	private static final float RIGHT_MARGIN = 60;
	private static final float BOTTOM_MARGIN = 80;
	private static final float LEFT_MARGIN = 60;
	private static final String FOOTER_INFO_LEFT = "www.bnbneeds.com";
	private static final String FOOTER_INFO_RIGHT = "support@bnbneeds.com";
	private static final PageSize PAGE_SIZE = new PageSize(PageSize.A4);
	private static final DeviceCmyk TABLE_HEADER_BACKGROUND_COLOR = new DeviceCmyk(83, 41, 0, 27);
	private static final int PURCHASE_LIST_RESULT_SIZE = 50;
	public static final String PURCHASE_LIST_REPORT_NAME = "Purchase List";
	public static final String AGGREGATED_PURCHASE_LIST_REPORT_NAME = "Aggregated Purchase List";

	private BNBNeedsClient client;

	public PDFGenerator(BNBNeedsClient client) {
		super();
		this.client = client;
	}

	private static Image getLogo() throws MalformedURLException {
		ImageData image = ImageDataFactory.create(CLASSPATH_LOGO_FILE);
		Image img = new Image(image).scaleToFit(95, 95).setFixedPosition(50, 725);
		return img;
	}

	private static void setMargin(Document document) {
		document.setMargins(TOP_MARGIN, RIGHT_MARGIN, BOTTOM_MARGIN, LEFT_MARGIN);
	}

	private static void closeDocument(Document document, PdfDocument pdfDocument) {
		if (document != null) {
			document.close();
		}

		if (pdfDocument != null) {
			pdfDocument.close();
		}
	}

	private void setHeaderFooter(PdfDocument pdfDocument, String currentDate, String reportName) {

		VariableHeaderEventHandler handler = new VariableHeaderEventHandler();
		pdfDocument.addEventHandler(PdfDocumentEvent.END_PAGE, handler);

		handler.setDate(currentDate);
		handler.setReportName(reportName);
		handler.setFooterInfoLeft(FOOTER_INFO_LEFT);
		handler.setFooterInfoRight(FOOTER_INFO_RIGHT);
	}

	private void emptyPurchaseList(Document document) throws IOException {
		Paragraph purchaseListEmpty = new Paragraph("No purchase list to display.")
				.setFont(PdfFontFactory.createFont(FontConstants.HELVETICA)).setFontSize(BODY_HEADER_FONT_SIZE);
		purchaseListEmpty.setItalic();
		document.add(purchaseListEmpty);
	}

	public void generateBuyerPurchaseListReport(String buyerId, Map<String, String> queryParams,
			String targetFilePath) {

		Document document = null;
		PdfDocument pdfDocument = null;

		String currentDate = StringUtils.getDateTimestamp(Constants.DATE_FORMAT, new Date().getTime());

		PurchaseListResponse listResponse = null;
		List<PurchaseItemInfoResponse> purchaseInfoResponse = null;
		String nextOffset = null;

		if (!queryParams.isEmpty()) {

			listResponse = client.buyers(buyerId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE, queryParams);

		} else {
			listResponse = client.buyers(buyerId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE);

		}

		purchaseInfoResponse = new ArrayList<>();
		List<PurchaseItemInfoResponse> purchaseItemInfoResponse = listResponse.getPurchaseItems();
		purchaseInfoResponse.addAll(purchaseItemInfoResponse);
		if (purchaseItemInfoResponse != null) {
			String offset = listResponse.getNextOffset();

			while (StringUtils.hasText(offset)) {
				if (!queryParams.isEmpty()) {
					if (StringUtils.hasText(offset)) {
						listResponse = client.buyers(buyerId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE, queryParams,
								offset);
					}
				} else {
					if (StringUtils.hasText(offset)) {
						listResponse = client.buyers(buyerId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE, offset);
					}

				}
				if (listResponse != null) {
					purchaseItemInfoResponse = listResponse.getPurchaseItems();
					if (purchaseItemInfoResponse != null) {
						purchaseInfoResponse.addAll(purchaseItemInfoResponse);
					}
					nextOffset = listResponse.getNextOffset();
					if (nextOffset.equals(offset)) {
						break;
					} else {
						offset = nextOffset;
					}
				}
			}

		}

		try {

			pdfDocument = new PdfDocument(new PdfWriter(targetFilePath));

			pdfDocument.setDefaultPageSize(PAGE_SIZE);

			document = new Document(pdfDocument);
			setMargin(document);

			setHeaderFooter(pdfDocument, currentDate, PURCHASE_LIST_REPORT_NAME);

			float[] columnWidth = new float[] { 10, 53, 33, 2, 2 };
			Table table = new Table(columnWidth, true);

			// Table Header
			Cell publishedDate = new Cell(1, 1).add("Request Date");
			publishedDate.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(publishedDate);
			Cell productName = new Cell(1, 1).add("Product Name");
			productName.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(productName);
			Cell target = new Cell(1, 1).add("Vendor");
			target.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(target);

			Cell requiredQuantity = new Cell(1, 1).add("Required Quantity");
			requiredQuantity.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(requiredQuantity);
			Cell deliveredQuantity = new Cell(1, 1).add("Delivered Quantity");
			deliveredQuantity.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(deliveredQuantity);

			document.add(table);
			List<List<String>> dataset = getPurchaseListData("BUYER", purchaseInfoResponse);
			for (List<String> record : dataset) {
				for (String field : record) {
					table.addCell(new Cell().add(new Paragraph(field).setFontSize(BODY_FONT_SIZE))
							.setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(TABLE_BORDER_COLOR, 0)));
				}
			}
			document.add(table);

		} catch (Exception e) {
			logger.error("There was a problem occurred while generating Buyer purchase list report.", e);
			throw new ReportGenerationException("A problem occurred while generating report.", e);
		} finally {
			closeDocument(document, pdfDocument);
		}

	}

	public void generateVendorPurchaseListReport(String vendorId, Map<String, String> queryParams,
			String targetFilePath) {

		Document document = null;
		PdfDocument pdfDocument = null;

		String currentDate = StringUtils.getDateTimestamp(Constants.DATE_FORMAT, new Date().getTime());

		PurchaseListResponse listResponse = null;
		List<PurchaseItemInfoResponse> purchaseInfoResponse = null;
		String nextOffset = null;

		if (!queryParams.isEmpty()) {
			listResponse = client.vendors(vendorId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE, queryParams);
		} else {
			listResponse = client.vendors(vendorId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE);
		}

		purchaseInfoResponse = new ArrayList<>();
		List<PurchaseItemInfoResponse> purchaseItemInfoResponse = listResponse.getPurchaseItems();
		purchaseInfoResponse.addAll(purchaseItemInfoResponse);
		if (purchaseItemInfoResponse != null) {
			String offset = listResponse.getNextOffset();

			while (StringUtils.hasText(offset)) {
				if (!queryParams.isEmpty()) {
					if (StringUtils.hasText(offset)) {
						listResponse = client.vendors(vendorId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE, queryParams,
								offset);
					}
				} else {
					if (StringUtils.hasText(offset)) {
						listResponse = client.vendors(vendorId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE, offset);
					}

				}
				if (listResponse != null) {
					purchaseItemInfoResponse = listResponse.getPurchaseItems();
					if (purchaseItemInfoResponse != null) {
						purchaseInfoResponse.addAll(purchaseItemInfoResponse);
					}
					nextOffset = listResponse.getNextOffset();
					if (nextOffset.equals(offset)) {
						break;
					} else {
						offset = nextOffset;
					}
				}
			}

		}

		try {
			pdfDocument = new PdfDocument(new PdfWriter(targetFilePath));

			pdfDocument.setDefaultPageSize(PAGE_SIZE);

			document = new Document(pdfDocument);

			setMargin(document);
			setHeaderFooter(pdfDocument, currentDate, PURCHASE_LIST_REPORT_NAME);

			float[] columnWidth = new float[] { 10, 53, 33, 2, 2 };
			Table table = new Table(columnWidth, true);

			// Table Header

			Cell publishedDate = new Cell(1, 1).add("Request Date");
			publishedDate.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(publishedDate);

			Cell productName = new Cell(1, 1).add("Product Name");
			productName.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(productName);

			Cell target = new Cell(1, 1).add("Buyer");
			target.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(target);

			Cell requiredQuantity = new Cell(1, 1).add("Required Quantity");
			requiredQuantity.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(requiredQuantity);

			Cell deliveredQuantity = new Cell(1, 1).add("Delivered Quantity");
			deliveredQuantity.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(deliveredQuantity);

			document.add(table);
			List<List<String>> dataset = getPurchaseListData("VENDOR", purchaseInfoResponse);
			for (List<String> record : dataset) {
				for (String field : record) {
					table.addCell(new Cell().add(new Paragraph(field).setFontSize(BODY_FONT_SIZE))
							.setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(TABLE_BORDER_COLOR, 0)));
				}
			}

			document.add(table);

		} catch (Exception e) {
			logger.error("There was a problem occurred while generating Vendor purchase list report. ", e);
			throw new ReportGenerationException("A problem occurred while generating report.", e);
		} finally {
			closeDocument(document, pdfDocument);
		}

	}

	private static List<List<String>> getPurchaseListData(String userRole,
			List<PurchaseItemInfoResponse> purchaseListResponse) {

		String productName, name, requestDate, requiredQuantity, deliveredQuantity;
		List<List<String>> data = new ArrayList<>();
		Iterator<PurchaseItemInfoResponse> iterator = purchaseListResponse.iterator();
		for (int i = 0; i < purchaseListResponse.size();) {
			List<String> dataLine = new ArrayList<>();

			PurchaseItemInfoResponse purchaseItemInfoResponse = iterator.next();
			productName = purchaseItemInfoResponse.getProductName().getName();

			if ("BUYER".equals(userRole)) {
				name = purchaseItemInfoResponse.getVendor().getName();
			} else {
				name = purchaseItemInfoResponse.getBuyer().getName();
			}

			requestDate = purchaseItemInfoResponse.getRequestDate();
			requiredQuantity = purchaseItemInfoResponse.getQuantity().getRequired().toString() + " "
					+ purchaseItemInfoResponse.getQuantity().getUnit();
			deliveredQuantity = purchaseItemInfoResponse.getQuantity().getDelivered().toString() + " "
					+ purchaseItemInfoResponse.getQuantity().getUnit();

			String[] tableTitleList = { requestDate, productName, name, requiredQuantity, deliveredQuantity };
			data.add(Arrays.asList(tableTitleList));

			i++;
			for (int j = 0; j < tableTitleList.length; j++) {
				dataLine.add(tableTitleList[j]);
			}

		}
		return data;
	}

	public void generateVendorAggregatedPurchaseListReport(String vendorId, String buyerId, String startRequestDate,
			String endRequestDate, String requestDate, String productNameId, String targetFilePath) {

		Document document = null;
		PdfDocument pdfDocument = null;
		AggregatedPurchaseListResponse listResponse = null;

		String currentDate = StringUtils.getDateTimestamp(Constants.DATE_FORMAT, new Date().getTime());

		if (requestDate != null) {

			listResponse = client.vendors(vendorId).getAggregatedPurchaseList(requestDate, productNameId, buyerId);

		} else {
			listResponse = client.vendors(vendorId).getAggregatedPurchaseListByDateRange(startRequestDate,
					endRequestDate, productNameId, buyerId);
		}

		try {
			pdfDocument = new PdfDocument(new PdfWriter(targetFilePath));

			pdfDocument.setDefaultPageSize(PAGE_SIZE);

			document = new Document(pdfDocument);

			setMargin(document);
			setHeaderFooter(pdfDocument, currentDate, AGGREGATED_PURCHASE_LIST_REPORT_NAME);

			float[] columnWidth = new float[] { 60, 20, 20 };
			Paragraph requestedDate = new Paragraph("Requested Date: " + listResponse.getRequestDate())
					.setFont(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD))
					.setFontSize(BODY_HEADER_FONT_SIZE);
			document.add(requestedDate);
			if (buyerId != null) {
				Paragraph buyerName = new Paragraph(listResponse.getItemList().iterator().next().getBuyerList()
						.iterator().next().getDealer().getName())
								.setFont(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD))
								.setFontSize(BODY_HEADER_FONT_SIZE);
				document.add(buyerName);
			}
			Table table = new Table(columnWidth, true);
			// Table Header
			Cell productName = new Cell(1, 1).add("Product Name");
			productName.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(productName);
			Cell totalRequiredQuantity = new Cell(1, 1).add("Total Required Quantity");
			totalRequiredQuantity.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(totalRequiredQuantity);
			Cell totalDeliveredQuantity = new Cell(1, 1).add("Total Delivered Quantity");
			totalDeliveredQuantity.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR)
					.setFontColor(TABLE_TITLE_FONT_COLOR).setFontSize(TABLE_TITLE_FONT_SIZE)
					.setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(totalDeliveredQuantity);

			List<List<String>> dataset = getAggregatePurchaseListData(listResponse);
			if (dataset != null) {
				for (List<String> record : dataset) {
					for (String field : record) {
						table.addCell(new Cell().add(new Paragraph(field).setFontSize(BODY_FONT_SIZE))
								.setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(TABLE_BORDER_COLOR, 0)));
					}
				}
			} else {
				emptyPurchaseList(document);
			}

			document.add(table);

		} catch (Exception e) {
			logger.error("There was a problem occurred while generating Vendor aggregate purchase list report. ", e);

			throw new ReportGenerationException("A problem occurred while generating report.", e);

		} finally {
			closeDocument(document, pdfDocument);
		}

	}

	public void generateBuyerAggregatedPurchaseListReport(String buyerId, String vendorId, String startRequestDate,
			String endRequestDate, String requestDate, String productNameId, String targetFilePath) {

		Document document = null;
		PdfDocument pdfDocument = null;

		AggregatedPurchaseListResponse listResponse = null;

		String currentDate = StringUtils.getDateTimestamp(Constants.DATE_FORMAT, new Date().getTime());

		try {
			if (requestDate != null) {

				listResponse = client.buyers(buyerId).getAggregatedPurchaseList(requestDate, productNameId, vendorId);

			} else {
				listResponse = client.buyers(buyerId).getAggregatedPurchaseListByDateRange(startRequestDate,
						endRequestDate, productNameId, vendorId);
			}

			pdfDocument = new PdfDocument(new PdfWriter(targetFilePath));

			pdfDocument.setDefaultPageSize(PAGE_SIZE);

			document = new Document(pdfDocument);

			setMargin(document);
			setHeaderFooter(pdfDocument, currentDate, AGGREGATED_PURCHASE_LIST_REPORT_NAME);

			float[] columnWidth = new float[] { 60, 20, 20 };
			Paragraph requestedDate = new Paragraph("Requested Date: " + listResponse.getRequestDate())
					.setFont(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD))
					.setFontSize(BODY_HEADER_FONT_SIZE);
			document.add(requestedDate);

			if (vendorId != null) {
				Paragraph vendorName = new Paragraph(listResponse.getItemList().iterator().next().getVendorList()
						.iterator().next().getDealer().getName())
								.setFont(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD))
								.setFontSize(BODY_HEADER_FONT_SIZE);
				document.add(vendorName);
			}
			Table table = new Table(columnWidth, true);
			// Table Header
			Cell productName = new Cell(1, 1).add("Product Name");
			productName.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(productName);
			Cell totalRequiredQuantity = new Cell(1, 1).add("Total Required Quantity");
			totalRequiredQuantity.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR).setFontColor(TABLE_TITLE_FONT_COLOR)
					.setFontSize(TABLE_TITLE_FONT_SIZE).setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(totalRequiredQuantity);
			Cell totalDeliveredQuantity = new Cell(1, 1).add("Total Delivered Quantity");
			totalDeliveredQuantity.setBackgroundColor(TABLE_HEADER_BACKGROUND_COLOR)
					.setFontColor(TABLE_TITLE_FONT_COLOR).setFontSize(TABLE_TITLE_FONT_SIZE)
					.setBorder(new SolidBorder(TABLE_BORDER_COLOR, 0));
			table.addHeaderCell(totalDeliveredQuantity);

			List<List<String>> dataset = getAggregatePurchaseListData(listResponse);
			if (dataset != null) {
				for (List<String> record : dataset) {
					for (String field : record) {
						table.addCell(new Cell().add(new Paragraph(field).setFontSize(BODY_FONT_SIZE))
								.setBorder(Border.NO_BORDER).setBorderBottom(new SolidBorder(TABLE_BORDER_COLOR, 0)));
					}
				}
			} else {
				emptyPurchaseList(document);
			}

			document.add(table);

		} catch (Exception e) {
			logger.error("There was a problem occurred while generating Buyer aggregate purchase list report. ", e);

			throw new ReportGenerationException("A problem occurred while generating report.", e);

		} finally {
			closeDocument(document, pdfDocument);

		}

	}

	private static List<List<String>> getAggregatePurchaseListData(AggregatedPurchaseListResponse listResponse) {
		String productName = null;
		String totalRequiredQuantity = null;
		String totalDeliveredQuantity = null;

		List<List<String>> data = new ArrayList<>();

		if (listResponse.size() != 0) {
			Iterator<AggregatedPurchaseListResponse.AggregatedPuchaseItem> iterator = listResponse.getItemList()
					.iterator();

			for (int i = 0; i < listResponse.size();) {
				List<String> dataLine = new ArrayList<>();

				AggregatedPurchaseListResponse.AggregatedPuchaseItem purchaseItemInfoResponse = iterator.next();
				productName = purchaseItemInfoResponse.getProductName().getName();

				totalRequiredQuantity = purchaseItemInfoResponse.getQuantity().getRequired().toString() + " "
						+ purchaseItemInfoResponse.getQuantity().getUnit();
				totalDeliveredQuantity = purchaseItemInfoResponse.getQuantity().getDelivered().toString() + " "
						+ purchaseItemInfoResponse.getQuantity().getUnit();

				String[] tableTitleList = { productName, totalRequiredQuantity, totalDeliveredQuantity };
				data.add(Arrays.asList(tableTitleList));

				i++;
				for (int j = 0; j < tableTitleList.length; j++) {
					dataLine.add(tableTitleList[j]);
				}

			}
			return data;
		}
		return null;

	}

	protected class VariableHeaderEventHandler implements IEventHandler {
		protected String reportName;
		protected String date;
		protected String footerInfoRight;
		protected String footerInfoLeft;

		public void setReportName(String reportName) {
			this.reportName = reportName;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public void setFooterInfoRight(String footerInfoRight) {
			this.footerInfoRight = footerInfoRight;
		}

		public void setFooterInfoLeft(String footerInfoLeft) {
			this.footerInfoLeft = footerInfoLeft;
		}

		@SuppressWarnings("resource")
		public void handleEvent(Event event) {
			PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
			PdfPage page = docEvent.getPage();
			Rectangle pageSize = page.getPageSize();
			float pageWidth = pageSize.getWidth();
			PdfDocument pdfDoc = docEvent.getDocument();
			PdfCanvas pdfCanvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc);
			PdfFont font = null;

			try {
				font = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
			} catch (IOException e) {
				logger.error("Problem occured while creating font.");
			}
			Image image = null;
			try {
				image = getLogo();
			} catch (MalformedURLException e) {
				logger.error("Problem occured while loading image into PDF file.");
			}
			// watermark
			new Canvas(pdfCanvas, pdfDoc, pageSize).setFontColor(Color.LIGHT_GRAY).setFontSize(50).setFont(font)
					.showTextAligned(new Paragraph(FOOTER_INFO_LEFT), 298, 421, pdfDoc.getPageNumber(page),
							TextAlignment.CENTER, VerticalAlignment.MIDDLE, 45);

			// Report Title
			new Canvas(pdfCanvas, pdfDoc, pageSize).setFontSize(16).showTextAligned(reportName, pageWidth / 2,
					pageSize.getTop() - 60, TextAlignment.CENTER, VerticalAlignment.MIDDLE, 0);
			new Canvas(pdfCanvas, pdfDoc, pageSize)
					// header
					.add(image)
					.showTextAligned("Report generated on: " + date, pageWidth - 153, pageSize.getTop() - 110,
							TextAlignment.CENTER, VerticalAlignment.MIDDLE, 0)
					.setFontSize(BODY_FONT_SIZE)
					// footer
					.showTextAligned(footerInfoLeft, pageSize.getLeft() + 90, 30, TextAlignment.CENTER,
							VerticalAlignment.MIDDLE, 0)
					.showTextAligned(Integer.toString(pdfDoc.getPageNumber(page)), pageWidth / 2, 30,
							TextAlignment.CENTER, VerticalAlignment.MIDDLE, 0)
					.showTextAligned(footerInfoRight, pageWidth - 100, 30, TextAlignment.CENTER,
							VerticalAlignment.MIDDLE, 0);
		}
	}

}
