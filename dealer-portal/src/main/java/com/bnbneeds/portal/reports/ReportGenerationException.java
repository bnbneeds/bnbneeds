package com.bnbneeds.portal.reports;

public class ReportGenerationException extends RuntimeException {

	private static final long serialVersionUID = 7666518325284865179L;

	public ReportGenerationException() {
		super();
	}

	public ReportGenerationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ReportGenerationException(String arg0) {
		super(arg0);
	}

	public ReportGenerationException(Throwable arg0) {
		super(arg0);
	}

}
