package com.bnbneeds.portal.dealer.formbean;

public class BuyerPurchaseItemUpdateBean extends PurchaseItemUpdateBean {

	private Double requiredQuantity;
	private String unit;
	private String vendor;

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public Double getRequiredQuantity() {
		return requiredQuantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setRequiredQuantity(Double requiredQuantity) {
		this.requiredQuantity = requiredQuantity;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

}
