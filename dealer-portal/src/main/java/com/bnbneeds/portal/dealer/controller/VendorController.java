package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.handleBadRequest;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.writeResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.dealer.VendorInfoListResponse;
import com.bnbneeds.app.model.dealer.VendorInfoResponse;
import com.bnbneeds.app.model.dealer.VendorListResponse;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.DealerUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.controller.util.ReviewUtility;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.google.gson.Gson;

@Controller
@RequestMapping(path = VendorController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('BUYER')")
public class VendorController extends DealerController {

	private static final Logger logger = LoggerFactory
			.getLogger(VendorController.class);

	protected static final String BASE_REQUEST_PATH = "/vendors";
	private static final String LIST_URI = "/list";
	private static final String DETAIL_URI = "/%s/details";
	private static final String VENDOR_DETAIL_REQUEST_URI = BASE_REQUEST_PATH
			+ DETAIL_URI;

	public static final String LIST_VENDORS_URI = BASE_REQUEST_PATH + LIST_URI;

	private static final String VENDOR_LIST_RESPONSE_ATTRIBUTE = "vendorListResponse";

	private static final String VENDOR_LIST_VIEW = "vendor-list";
	private static final String VENDOR_PROFILE_VIEW = "vendor-profile";

	private static final int VENDOR_LIST_RESULT_SIZE = 20;

	public static String getVendorDetailsRequestURI(String vendorId) {
		return String.format(VENDOR_DETAIL_REQUEST_URI, vendorId);
	}

	private String getVendorProfileView(Model model,
			VendorInfoResponse vendorInfo) {
		VendorOperationsController.setVendorInfoAttribute(vendorInfo, model);
		ReviewUtility.setReviewListResponse(bnbneedsClient, ReviewFor.vendor,
				vendorInfo.getId(), null, null, model);
		ReviewUtility.setAverageRatingResponse(bnbneedsClient,
				ReviewFor.vendor, vendorInfo.getId(), model);
		return VENDOR_PROFILE_VIEW;
	}

	static void setVendorListResponse(Model model,
			VendorInfoListResponse listResponse) {
		model.addAttribute(VENDOR_LIST_RESPONSE_ATTRIBUTE, listResponse);
	}

	@Layout(title = "Vendors")
	@RequestMapping(method = RequestMethod.GET, path = LIST_URI)
	public String getVendorList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			Model model) {

		DealerUtils.checkHasDealership(user);

		VendorInfoListResponse listResponse = null;
		Map<String, String> queryParams = new HashMap<String, String>();
		try {

			if (!queryParams.isEmpty()) {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = bnbneedsClient.vendors().listBulk(
							VENDOR_LIST_RESULT_SIZE, queryParams, nextOffset);
				} else {
					listResponse = bnbneedsClient.vendors().listBulk(
							VENDOR_LIST_RESULT_SIZE, queryParams);
				}
			} else {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = bnbneedsClient.vendors().listBulk(
							VENDOR_LIST_RESULT_SIZE, nextOffset);
				} else {
					listResponse = bnbneedsClient.vendors().listBulk(
							VENDOR_LIST_RESULT_SIZE);
				}
			}
			setVendorListResponse(model, listResponse);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_LIST_VIEW);
		}

		return VENDOR_LIST_VIEW;
	}

	@Layout(title = "Vendor Information")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/details")
	public String getVendorInfo(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String vendorId, Model model) {
		DealerUtils.checkHasDealership(user);

		try {
			VendorInfoResponse vendor = bnbneedsClient.vendors(vendorId).get();
			return getVendorProfileView(model, vendor);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_PROFILE_VIEW);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{id}/detail-as-text")
	@ResponseBody
	public synchronized void writeVendorInfo(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String vendorId, HttpServletResponse response)
			throws IOException {

		try {
			DealerUtils.checkHasDealership(user);
			String vendor = bnbneedsClient.vendors(vendorId).getAsString();
			writeResponse(200, MediaType.TEXT_PLAIN_VALUE, vendor, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/list-as-text")
	@ResponseBody
	public synchronized void writeAllVendors(
			@AuthenticationPrincipal BNBNeedsUser user,
			HttpServletResponse response) throws IOException {
		List<NamedRelatedResourceRep> vendorList = new ArrayList<NamedRelatedResourceRep>();
		try {
			DealerUtils.checkHasDealership(user);
			String nextOffset = null;
			VendorListResponse vendorListResp = null;
			String currentOffset = null;

			vendorListResp = bnbneedsClient.vendors().list(RESULT_FETCH_SIZE);
			if (vendorListResp != null) {
				currentOffset = nextOffset;
				nextOffset = vendorListResp.getNextOffset();
				if (vendorListResp.getVendorList() != null) {
					vendorList.addAll(vendorListResp.getVendorList());
				}
			}

			do {
				vendorListResp = bnbneedsClient.vendors().list(
						RESULT_FETCH_SIZE, nextOffset);
				if (vendorListResp != null) {
					currentOffset = nextOffset;
					nextOffset = vendorListResp.getNextOffset();
					if (vendorListResp.getVendorList() != null) {
						vendorList.addAll(vendorListResp.getVendorList());
					}
				}

			} while (!nextOffset.equals(currentOffset));
			Gson gson = new Gson();
			String textResponse = gson.toJson(vendorList);
			writeResponse(200, MediaType.TEXT_PLAIN_VALUE, textResponse,
					response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}
}
