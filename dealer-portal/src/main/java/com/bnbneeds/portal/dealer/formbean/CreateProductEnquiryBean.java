package com.bnbneeds.portal.dealer.formbean;

public class CreateProductEnquiryBean extends ProductEnquiryBean {

	private String productId;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

}
