package com.bnbneeds.portal.dealer.formbean;

public class UpdateTenderBean extends TenderBean {

	private String tenderId;

	public String getTenderId() {
		return tenderId;
	}

	public void setTenderId(String tenderId) {
		this.tenderId = tenderId;
	}

}
