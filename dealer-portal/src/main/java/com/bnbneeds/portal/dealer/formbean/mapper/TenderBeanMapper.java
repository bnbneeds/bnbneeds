package com.bnbneeds.portal.dealer.formbean.mapper;

import java.util.Date;

import com.bnbneeds.app.model.dealer.tender.AddTenderProductsParam;
import com.bnbneeds.app.model.dealer.tender.TenderProductInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderProductParam;
import com.bnbneeds.app.model.dealer.tender.CreateTenderParam;
import com.bnbneeds.app.model.dealer.tender.TenderInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderParam;
import com.bnbneeds.portal.dealer.formbean.BidProductBean;
import com.bnbneeds.portal.dealer.formbean.CreateBidProductBean;
import com.bnbneeds.portal.dealer.formbean.CreateTenderBean;
import com.bnbneeds.portal.dealer.formbean.TenderBean;
import com.bnbneeds.portal.dealer.formbean.UpdateBidProductBean;
import com.bnbneeds.portal.dealer.formbean.UpdateTenderBean;
import com.bnbneeds.portal.dealer.util.Constants;
import com.bnbneeds.portal.dealer.util.DateUtils;
import com.bnbneeds.portal.dealer.util.StringUtils;

public class TenderBeanMapper {

	public static void mapTenderBean(TenderBean from, TenderParam to) {
		to.setName(from.getName());
		to.setCreditTerms(from.getCreditTerms());
		to.setDescription(from.getDescription());
		to.setDeliveryFrequency(from.getDeliveryFrequency());
		to.setExpectedPrice(from.getExpectedPrice());
		to.setPaymentMode(from.getPaymentMode());
		to.setBidValueType(from.getBidValueType());
		to.setDeliveryLocation(from.getDeliveryLocation());
		to.setCloseDate(from.getCloseDate());
		String deliveryTerms = from.getDeliveryTerms();
		if (StringUtils.hasText(deliveryTerms)) {
			to.setDeliveryTerms(deliveryTerms);
		}
	}

	public static void mapCreateTenderBean(CreateTenderBean from, CreateTenderParam to) {
		mapTenderBean(from, to);
	}

	public static CreateTenderParam mapCreateTenderBean(CreateTenderBean from) {
		CreateTenderParam to = new CreateTenderParam();
		mapTenderBean(from, to);
		return to;
	}

	public static void mapTender(TenderInfoResponse from, UpdateTenderBean to) {
		to.setTenderId(from.getId());
		to.setName(from.getName());
		to.setDescription(from.getDescription());
		to.setCreditTerms(from.getCreditTerms());
		to.setBidValueType(from.getBidValueType());
		to.setDescription(from.getDescription());
		to.setDeliveryFrequency(from.getDeliveryFrequency());
		to.setExpectedPrice(from.getExpectedPrice());
		to.setPaymentMode(from.getPaymentMode());
		to.setDeliveryLocation(from.getDeliveryLocation());
		Date closeDate = DateUtils.getDate(Constants.APISVC_RESPONSE_TIMESTAMP_FORMAT, from.getCloseTimestamp());
		String closeDateText = StringUtils.getDateTimestamp(Constants.DATE_FORMAT, closeDate);
		to.setCloseDate(closeDateText);
		String deliveryTerms = from.getDeliveryTerms();
		if (StringUtils.hasText(deliveryTerms)) {
			to.setDeliveryTerms(deliveryTerms);
		}
	}

	public static void mapBidProduct(TenderProductInfoResponse from, UpdateBidProductBean to) {
		to.setBidProductId(from.getId());
		to.setTenderId(from.getTender().getId());
		to.setProductNameId(from.getProductName().getId());
		to.setQualitySpecification(from.getQualitySpecification());
		to.setRequiredQuantity(from.getQuantity().getRequired());
		to.setUnit(from.getQuantity().getUnit());
		to.setSpecialRequirements(from.getSpecialRequirements());
	}

	public static void mapBidProductBean(BidProductBean from, TenderProductParam to) {
		to.setProductNameId(from.getProductNameId());
		to.setQualitySpecification(from.getQualitySpecification());
		to.setQuantity(from.getRequiredQuantity());
		to.setSpecialRequirements(from.getSpecialRequirements());
		to.setUnit(from.getUnit());
	}

	public static AddTenderProductsParam mapCreateBidProductBean(CreateBidProductBean from) {
		AddTenderProductsParam param = new AddTenderProductsParam();
		TenderProductParam bidProduct = new TenderProductParam();
		mapBidProductBean(from, bidProduct);
		param.add(bidProduct);
		return param;
	}

}
