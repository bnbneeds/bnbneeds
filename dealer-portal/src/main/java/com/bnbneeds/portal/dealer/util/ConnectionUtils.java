package com.bnbneeds.portal.dealer.util;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionUtils {
	private static final Logger logger = LoggerFactory
			.getLogger(ConnectionUtils.class);

	public static void checkServerConnection(String hostNameOrIPAddress,
			int port) throws ConnectException {

		Socket s = null;
		try {
			/*
			 * If socket is not able to initialize, ConnectException is thrown.
			 * That's what we are interested in. If ConnectException occurs,
			 * then APIExceptionHandler kicks in and loads the
			 * reconnect-view.html.
			 * 
			 * If socket connection is successful, then user will automatically
			 * be redirected to /home-page or /login-page
			 */
			s = new Socket(hostNameOrIPAddress, port);
		} catch (ConnectException e) {
			logger.error("ConnectException occured", e);
			throw e;
		} catch (UnknownHostException e) {
			logger.error("UnknownHostException occured", e);
		} catch (IOException e) {
			logger.error("IOException occured", e);
		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (IOException e) {
					logger.error("IOException occured", e);
				}
			}
		}

	}

}
