package com.bnbneeds.portal.dealer.formbean;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.bnbneeds.portal.dealer.util.StringUtils;

public class UserRegistrationBean extends UserAccountProfileBean {

	@NotBlank
	private String dealerType;

	@NotBlank
	@Size(min = 6, max = 30, message = "Invalid password.")
	@Pattern(regexp = StringUtils.REGEX_PASSWORD, message = "Invalid password.")
	private String password;
	@NotBlank
	private String confirmPassword;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getDealerType() {
		return dealerType;
	}

	public void setDealerType(String dealerType) {
		this.dealerType = dealerType;
	}

}
