package com.bnbneeds.portal.dealer.formbean;

import org.hibernate.validator.constraints.NotBlank;

public class EntityBean {

	@NotBlank
	private String name;
	private String description;

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
