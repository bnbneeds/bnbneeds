package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.DealerUtils.checkHasDealership;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.writeResponse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.DataObjectInfoListResponse;
import com.bnbneeds.app.model.dealer.CreatePaymentTransactionParam;
import com.bnbneeds.app.model.dealer.subscription.CreateSubscriptionParam;
import com.bnbneeds.app.model.dealer.subscription.DealerBiddingSubscriptionInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.SubscriptionPlanInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionStatusParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionStatusParam.SubscriptionStatus;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.bean.RazorpayAuthBean;
import com.bnbneeds.portal.dealer.controller.util.RazorpayUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.formbean.RazorpayCheckoutOrderBean;
import com.bnbneeds.portal.dealer.formbean.RazorpayPaymentBean;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.Constants;
import com.bnbneeds.portal.dealer.util.DateUtils;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.core.BiddingSubscriptionPlans;
import com.bnbneeds.rest.client.core.BiddingSubscriptions;
import com.bnbneeds.rest.client.core.BiddingSubscriptions.BiddingSubscriptionFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.google.gson.Gson;
import com.razorpay.Order;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

public abstract class BiddingSubscriptionController extends DealerController {

	private static final Logger logger = LoggerFactory.getLogger(BiddingSubscriptionController.class);

	private static final String BIDDING_SUBSCRIPTION_DETAILS_VIEW = "bidding-subscription-details";
	private static final String BIDDING_SUBSCRIPTION_LIST_VIEW = "bidding-subscription-list";
	private static final String LIST_RESPONSE_ATTRIBUTE = "listResponse";
	private static final String LIST_SUBSCRIPTION_URI = "/list";
	private static final String LIST_SUBSCRIPTION_REQUEST_URL_ATTRIBUTE = "listSubscriptionsRequestURL";
	private static final String BIDDING_SUBSCRIPTION_INFO_ATTRIBURE = "biddingSubscriptionInfo";
	private static final String SUBSCRIPTION_STATE_QUERY_FIELD = "state";
	private static final String SUBSCRIPTION_PAYMENT_FORM_URI = "/{subscriptionId}/payment-form";
	private static final String AUTHORIZE_SUBSCRIPTION_PAYMENT_URI = "/{subscriptionId}/authorize";
	private static final String SUBSCRIPTION_ACTIVATION_IN_PROGRESS_STATUS = "ACTIVATION_IN_PROGRESS";
	protected static final String LIST_VIEW_TITLE = "Bidding Subscriptions";

	@Autowired
	private RazorpayClient razorpayClient;

	@Autowired
	private RazorpayAuthBean razorpayAuthBean;

	protected abstract String getBaseRequestURI();

	protected abstract BiddingSubscriptionFor getBiddingSubscriptionFor();

	private static class CheckExistingSubscriptionResponse {

		private String result;
		private String existingSubscriptionId;

		@SuppressWarnings("unused")
		public String getResult() {
			return result;
		}

		@SuppressWarnings("unused")
		public String getExistingSubscriptionId() {
			return existingSubscriptionId;
		}

		public void setResult(String result) {
			this.result = result;
		}

		public void setExistingSubscriptionId(String existingSubscriptionId) {
			this.existingSubscriptionId = existingSubscriptionId;
		}

	}

	private String getListSubscriptionsUrl() {
		return String.format("%s%s", getBaseRequestURI(), LIST_SUBSCRIPTION_URI);
	}

	private String getAuthorizePaymentUrl(String subscriptionId) {
		return String.format("%s%s", getBaseRequestURI(),
				AUTHORIZE_SUBSCRIPTION_PAYMENT_URI.replace("{subscriptionId}", subscriptionId));
	}

	private void setListSubscriptionsUrl(Model model) {
		model.addAttribute(LIST_SUBSCRIPTION_REQUEST_URL_ATTRIBUTE, getListSubscriptionsUrl());
	}

	private static void setDealerBiddingSubscription(DealerBiddingSubscriptionInfoResponse info, Model model) {
		if (info != null) {
			Date date = null;
			SimpleDateFormat dateFormat = DateUtils.getDateFormat(Constants.TIMESTAMP_FORMAT, Constants.TIME_ZONE_IST);
			String createdTimestamp = info.getCreatedTimestamp();
			if (StringUtils.hasText(createdTimestamp)) {
				date = DateUtils.getDate(Constants.APISVC_RESPONSE_TIMESTAMP_FORMAT, createdTimestamp);
				info.setCreatedTimestamp(StringUtils.getDateTimestamp(dateFormat, date));
			}

			String activatedTimestamp = info.getActivationTimestamp();
			if (StringUtils.hasText(activatedTimestamp)) {
				date = DateUtils.getDate(Constants.APISVC_RESPONSE_TIMESTAMP_FORMAT, activatedTimestamp);
				info.setActivationTimestamp(StringUtils.getDateTimestamp(dateFormat, date));
			}

			String expiryTimestamp = info.getExpiryTimestamp();
			if (StringUtils.hasText(expiryTimestamp)) {
				date = DateUtils.getDate(Constants.APISVC_RESPONSE_TIMESTAMP_FORMAT, expiryTimestamp);
				info.setExpiryTimestamp(StringUtils.getDateTimestamp(dateFormat, date));
			}
		}

		model.addAttribute(BIDDING_SUBSCRIPTION_INFO_ATTRIBURE, info);
	}

	private BiddingSubscriptions getClientHandle(BNBNeedsUser user) {
		String dealerId = user.getRelationshipId();
		BiddingSubscriptionFor dealerType = getBiddingSubscriptionFor();
		switch (dealerType) {
		case buyer:
			return bnbneedsClient.buyerBiddingSubscriptions(dealerId);
		case vendor:
			return bnbneedsClient.vendorBiddingSubscriptions(dealerId);
		default:
			return null;
		}
	}

	private BiddingSubscriptionPlans getBiddingSubscriptionPlanClientHandle() {
		BiddingSubscriptionFor dealerType = getBiddingSubscriptionFor();
		switch (dealerType) {
		case buyer:
			return bnbneedsClient.buyerBiddingSubscriptionPlans();
		case vendor:
			return bnbneedsClient.vendorBiddingSubscriptionPlans();
		default:
			return null;
		}
	}

	private void setModelForSubscriptionPlansListView(BNBNeedsUser user, String nextOffset, Model model) {

		BiddingSubscriptions biddingSubscriptionsClient = getClientHandle(user);

		try {
			DataObjectInfoListResponse<? extends DealerBiddingSubscriptionInfoResponse> listResponse = null;

			if (StringUtils.hasText(nextOffset)) {
				listResponse = biddingSubscriptionsClient.list(RESULT_FETCH_SIZE, nextOffset);
			} else {
				listResponse = biddingSubscriptionsClient.list(RESULT_FETCH_SIZE);
			}
			model.addAttribute(LIST_RESPONSE_ATTRIBUTE, listResponse);
		} catch (BNBNeedsServiceException e) {
			throw e;
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/check-for-subscription-history")
	public void writeSubscriptionExists(@AuthenticationPrincipal BNBNeedsUser user, HttpServletResponse response)
			throws IOException {

		try {
			checkHasDealership(user);
			CheckExistingSubscriptionResponse checkExistingSubscriptionResponse = new CheckExistingSubscriptionResponse();
			String resultText = "NO_SUBSCRIPTION_EXISTS";

			DataObjectInfoListResponse<? extends DealerBiddingSubscriptionInfoResponse> listResponse = getClientHandle(
					user).list(1);
			if (listResponse != null && !listResponse.isEmpty()) {
				resultText = "SUBSCRIPTION_EXISTS";
				String existingSubscriptionId = listResponse.getIterator().next().getId();
				checkExistingSubscriptionResponse.setExistingSubscriptionId(existingSubscriptionId);
			}
			checkExistingSubscriptionResponse.setResult(resultText);
			Gson gson = new Gson();
			writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE,
					gson.toJson(checkExistingSubscriptionResponse), response);

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/check-for-existing-subscription")
	public void writeStatusOfExistingSubscription(@AuthenticationPrincipal BNBNeedsUser user,
			HttpServletResponse response) throws IOException {

		try {
			checkHasDealership(user);
			CheckExistingSubscriptionResponse checkExistingSubscriptionResponse = new CheckExistingSubscriptionResponse();
			String resultText = "NO_SUBSCRIPTION_EXISTS";
			final String ACTIVATION_PENDING_STATUS = "ACTIVATION_PENDING";
			Map<String, String> queryMap = new HashMap<>();
			queryMap.put(SUBSCRIPTION_STATE_QUERY_FIELD, ACTIVATION_PENDING_STATUS);

			DataObjectInfoListResponse<? extends DealerBiddingSubscriptionInfoResponse> listResponse = getClientHandle(
					user).list(1, queryMap);
			if (listResponse != null && !listResponse.isEmpty()) {
				resultText = ACTIVATION_PENDING_STATUS;
				String existingSubscriptionId = listResponse.getIterator().next().getId();
				checkExistingSubscriptionResponse.setExistingSubscriptionId(existingSubscriptionId);
			} else {
				final String[] subscriptionStatusesToCheck = { "ACTIVATION_IN_PROGRESS", "ACTIVE" };
				for (String statusToCheck : subscriptionStatusesToCheck) {
					queryMap = new HashMap<>();
					queryMap.put(SUBSCRIPTION_STATE_QUERY_FIELD, statusToCheck);

					listResponse = getClientHandle(user).list(1, queryMap);
					if (listResponse != null && !listResponse.isEmpty()) {
						resultText = statusToCheck;
						break;
					}
				}
			}

			checkExistingSubscriptionResponse.setResult(resultText);
			Gson gson = new Gson();
			writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE,
					gson.toJson(checkExistingSubscriptionResponse), response);

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@Layout(title = LIST_VIEW_TITLE)
	@RequestMapping(method = RequestMethod.GET, path = LIST_SUBSCRIPTION_URI)
	public String listBiddingSubscriptions(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset, Model model) {
		checkHasDealership(user);
		setListSubscriptionsUrl(model);
		try {
			setModelForSubscriptionPlansListView(user, nextOffset, model);
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, BIDDING_SUBSCRIPTION_LIST_VIEW);
		}
		return BIDDING_SUBSCRIPTION_LIST_VIEW;
	}

	@Layout(title = "Bidding Subscription Details")
	@RequestMapping(method = RequestMethod.GET, path = "/{subscriptionId}/details")
	public String getBiddingSubscriptionDetails(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("subscriptionId") String subscriptionId, Model model) {
		checkHasDealership(user);

		try {
			DealerBiddingSubscriptionInfoResponse info = getClientHandle(user).getSubscription(subscriptionId);
			setDealerBiddingSubscription(info, model);
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, BIDDING_SUBSCRIPTION_DETAILS_VIEW);
		}
		return BIDDING_SUBSCRIPTION_DETAILS_VIEW;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/create-subscription-with-default-plan")
	public String createBiddingSubscriptionWithDefaultPlan(@AuthenticationPrincipal BNBNeedsUser user, Model model) {
		checkHasDealership(user);

		try {
			CreateResourceResponse taskResponse = getClientHandle(user).createSubscriptionWithDefaultPlan();
			String redirectURL = getRequestPathForCreateDefaultSubscriptionSuccess(taskResponse);
			return getRedirectPath(redirectURL);
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, BIDDING_SUBSCRIPTION_DETAILS_VIEW);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/create")
	public String createBiddingSubscription(@AuthenticationPrincipal BNBNeedsUser user,
			String biddingSubscriptionPlanId, Model model) {
		checkHasDealership(user);

		try {
			CreateSubscriptionParam param = new CreateSubscriptionParam();
			param.setSubscriptionPlanId(biddingSubscriptionPlanId);
			CreateResourceResponse taskResponse = getClientHandle(user).createSubscription(param);
			String redirectURL = getRequestPathForCreateSubscriptionSuccess(taskResponse);
			return getRedirectPath(redirectURL);
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, BIDDING_SUBSCRIPTION_DETAILS_VIEW);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{subscriptionId}/update")
	public String updateBiddingSubscription(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("subscriptionId") String subscriptionId, String newSubscriptionPlanId, Model model) {
		checkHasDealership(user);

		try {
			UpdateSubscriptionParam param = new UpdateSubscriptionParam();
			param.setSubscriptionPlanId(newSubscriptionPlanId);
			getClientHandle(user).updateSubscription(subscriptionId, param);
			String redirectURL = getRequestPathForUpdateSubscriptionSuccess(subscriptionId);
			return getRedirectPath(redirectURL);
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, BIDDING_SUBSCRIPTION_DETAILS_VIEW);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{subscriptionId}/delete")
	public String deleteBiddingSubscription(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("subscriptionId") String subscriptionId, Model model) {
		checkHasDealership(user);

		try {
			getClientHandle(user).deleteSubscription(subscriptionId);
			String redirectURL = getListSubscriptionsUrl() + "?deleteSuccess";
			return getRedirectPath(redirectURL);
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, BIDDING_SUBSCRIPTION_DETAILS_VIEW);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{subscriptionId}/initiate-activation")
	public String initiateActivation(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("subscriptionId") String subscriptionId, String newSubscriptionPlanId, Model model) {
		checkHasDealership(user);
		String requestURL = null;
		try {
			UpdateSubscriptionStatusParam param = new UpdateSubscriptionStatusParam();
			param.setStatus(UpdateSubscriptionStatusParam.SubscriptionStatus.INITIATE_ACTIVATION);
			try {
				getClientHandle(user).updateStatus(subscriptionId, param);
				requestURL = getRequestPathForSubscriptionPaymentForm(subscriptionId);
			} catch (BNBNeedsServiceException e) {
				requestURL = getSubscriptionDetailsURI(subscriptionId) + "?failedToInitiateActivation="
						+ e.getMessage();
			}
			return getRedirectPath(requestURL);
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, BIDDING_SUBSCRIPTION_DETAILS_VIEW);
		}
	}

	@Layout(title = "Bidding Subscription Premium Payment")
	@RequestMapping(method = RequestMethod.GET, path = SUBSCRIPTION_PAYMENT_FORM_URI)
	public String getSubscriptionPaymentForm(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("subscriptionId") String subscriptionId, Model model) {

		checkHasDealership(user);
		try {
			DealerBiddingSubscriptionInfoResponse info = getClientHandle(user).getSubscription(subscriptionId);
			if (SUBSCRIPTION_ACTIVATION_IN_PROGRESS_STATUS.equals(info.getSubscriptionState())) {
				String subscriptionPlanId = info.getSubscriptionPlan().getId();
				SubscriptionPlanInfoResponse plan = getBiddingSubscriptionPlanClientHandle().get(subscriptionPlanId);
				int amount = plan.getPremiumAmount().intValue();
				String paymentDescription = String.format("Premium payment for the bidding subscription plan: %s",
						plan.getName());
				String callbackActionURL = getAuthorizePaymentUrl(subscriptionId);
				RazorpayPaymentBean bean = RazorpayUtils.getRazorpayPaymentFormBean(razorpayAuthBean.getKeyId(), user,
						amount, paymentDescription, subscriptionId, callbackActionURL);

				Order order = razorpayClient.Orders.create(bean.getJSONObjectToCreateOrder());
				bean.setOrderId(order.get("id"));
				return RazorpayUtils.getRazorpayPaymentFormView(bean, model);
			}
			return getRedirectPath(getRequestPathForSubscriptionToForbidPaymentTransaction(subscriptionId));
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, BIDDING_SUBSCRIPTION_DETAILS_VIEW);
		} catch (RazorpayException e) {
			RazorpayUtils.setErrorMessage(e, model);
			return BIDDING_SUBSCRIPTION_DETAILS_VIEW;
		}
	}

	/**
	 * 
	 * Authorize the payment. If the payment is authorized then, to capture the
	 * payment:
	 * <ol>
	 * <li>Verify the payment signature. If verified:</li>
	 * <li>Get the plan linked with the subscription.</li>
	 * <li>Get the premium amount of the plan.</li>
	 * <li>Capture the payment passing the premium amount.</li>
	 * <li>Create a record of of the payment transaction.</li>
	 * <li>Mark the subscription as ACTIVE.</li>
	 * </ol>
	 * 
	 * If the payment is already captured:
	 * <ul>
	 * <li>If the subscription status is ACTIVATION_IN_PROGRESS, then record the
	 * payment and activate the subscription.</li>
	 * <li>Otherwise,then initiate refund</li>
	 * </ul>
	 * 
	 * @param user
	 * @param subscriptionId
	 * @param form
	 * @param model
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, path = AUTHORIZE_SUBSCRIPTION_PAYMENT_URI)
	public String authorizePayment(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("subscriptionId") String subscriptionId, RazorpayCheckoutOrderBean form, Model model) {
		checkHasDealership(user);

		try {

			String redirectPath = null;
			DealerBiddingSubscriptionInfoResponse info = getClientHandle(user).getSubscription(subscriptionId);
			Payment payment = razorpayClient.Payments.fetch(form.getRazorpay_payment_id());
			String subscriptionStatus = info.getSubscriptionState();
			logger.info("Subscription status of subscription ID: {} is {}", subscriptionId, subscriptionStatus);
			if (SUBSCRIPTION_ACTIVATION_IN_PROGRESS_STATUS.equals(subscriptionStatus)) {
				// Capture the payment only if the subscription state is ACTIVATION_IN_PROGRESS
				if (RazorpayUtils.isPaymentFailed(payment)) {
					// If the payment as has failed, then mark the subscription as ACTIVATION_FAILED
					redirectPath = getRequestPathForSubscriptionPaymentFailed(subscriptionId);
				} else if (RazorpayUtils.isPaymentAuthorized(payment)) {

					// 1. Verify the payment signature
					boolean okay = RazorpayUtils.isPaymentSignatureValid(form, razorpayAuthBean);
					if (okay) {
						// 2. Get the plan linked with the subscription
						String subscriptionPlanId = info.getSubscriptionPlan().getId();
						SubscriptionPlanInfoResponse plan = getBiddingSubscriptionPlanClientHandle()
								.get(subscriptionPlanId);
						// 3. Get the premium amount of the plan
						int amount = plan.getPremiumAmount().intValue();
						// 4. Capture the payment passing the premium amount
						payment = RazorpayUtils.capturePayment(razorpayClient, form.getRazorpay_payment_id(), amount);
						// Steps 5 and 6
						redirectPath = recordPaymentAndActivateSubscription(user, subscriptionId, payment);
					} else {
						redirectPath = getRequestPathForSubscriptionPaymentFailed(subscriptionId);
					}
				} else if (RazorpayUtils.isPaymentCaptured(payment)) {
					redirectPath = recordPaymentAndActivateSubscription(user, subscriptionId, payment);
				} else {
					redirectPath = getRequestPathForSubscriptionPaymentFailed(subscriptionId);
				}
			} else {
				if (RazorpayUtils.isPaymentCaptured(payment)) {
					logger.info("Payment is captured. Initiating refund because the subsctiption status is {}.",
							subscriptionStatus);
					RazorpayUtils.refundCapturedPayment(razorpayClient, payment);
				}
				redirectPath = getRequestPathForSubscriptionToForbidPaymentTransaction(subscriptionId);
			}
			return getRedirectPath(redirectPath);

		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, BIDDING_SUBSCRIPTION_DETAILS_VIEW);
		} catch (RazorpayException e) {
			RazorpayUtils.setErrorMessage(e, model);
			return BIDDING_SUBSCRIPTION_DETAILS_VIEW;
		}
	}

	/**
	 * Records the payment and activates the subscription. If any of these two step
	 * fail due to {@code BNBNeedsServiceException}, then refund API is called on
	 * razorpay client.
	 * 
	 * @param user
	 *            the authenticated user
	 * @param subscriptionId
	 *            the subscription ID
	 * @param payment
	 *            the razorpay payment object. Make sure the payment status is
	 *            "captured". Otherwise refund fails.
	 * @return the redirect path
	 * @throws RazorpayException
	 */
	private String recordPaymentAndActivateSubscription(BNBNeedsUser user, String subscriptionId, Payment payment)
			throws RazorpayException {
		try {
			// 6. Create a record of of the payment transaction
			createPaymentTransactionForBiddingSubscriptionInDatastore(user, subscriptionId, payment);
			// 7. Mark the subscription as ACTIVE
			UpdateSubscriptionStatusParam updateSubscriptionStatus = new UpdateSubscriptionStatusParam();
			updateSubscriptionStatus.setStatus(SubscriptionStatus.ACTIVATE);
			getClientHandle(user).updateStatus(subscriptionId, updateSubscriptionStatus);

			return getRequestPathForSubscriptionActivated(subscriptionId);
		} catch (BNBNeedsServiceException e) {
			logger.info(
					"Payment was captured but exception BNBNeedsServiceException occured. Initiating refund for bidding subscription ID: {}",
					subscriptionId);
			RazorpayUtils.refundCapturedPayment(razorpayClient, payment);
			switch (e.getHttpCode()) {
			case 401:
			case 403:
				return AuthenticationController.LOGOUT_URL;
			default:
				return getRequestPathForSubscriptionPaymentRefund(subscriptionId);
			}
		}
	}

	private String getRequestPathForSubscriptionPaymentRefund(String subscriptionId) {
		String redirectURL = getSubscriptionDetailsURI(subscriptionId) + "?refundPayment";
		return redirectURL;
	}

	private String createPaymentTransactionForBiddingSubscriptionInDatastore(BNBNeedsUser user, String subscriptionId,
			Payment payment) throws RazorpayException {

		CreatePaymentTransactionParam createPaymentTransaction = new CreatePaymentTransactionParam();
		RazorpayUtils.mapRazorpayPaymentCheckout(payment, createPaymentTransaction);
		CreateResourceResponse taskResponse = getClientHandle(user).createPaymentTransaction(subscriptionId,
				createPaymentTransaction);
		return taskResponse.getResourceRep().getId();
	}

	private String getRequestPathForSubscriptionPaymentForm(String subscriptionId) {
		String uri = getBaseRequestURI() + SUBSCRIPTION_PAYMENT_FORM_URI.replace("{subscriptionId}", subscriptionId);
		return uri;
	}

	private String getSubscriptionDetailsURI(String subscriptionId) {
		String detailURI = String.format("/%s/details", subscriptionId);
		StringBuffer buffer = new StringBuffer();
		buffer.append(getBaseRequestURI()).append(detailURI);
		return buffer.toString();
	}

	private String getRequestPathForCreateSubscriptionSuccess(CreateResourceResponse resource) {
		String redirectURL = getSubscriptionDetailsURI(resource.getResourceRep().getId()) + "?createSuccess";
		return redirectURL;
	}

	private String getRequestPathForCreateDefaultSubscriptionSuccess(CreateResourceResponse resource) {
		String redirectURL = getSubscriptionDetailsURI(resource.getResourceRep().getId())
				+ "?createDefaultSubscriptionSuccess";
		return redirectURL;
	}

	private String getRequestPathForUpdateSubscriptionSuccess(String subscriptionId) {
		String redirectURL = getSubscriptionDetailsURI(subscriptionId) + "?updateSuccess";
		return redirectURL;
	}

	private String getRequestPathForSubscriptionPaymentFailed(String subscriptionId) {
		String redirectURL = getSubscriptionDetailsURI(subscriptionId) + "?paymentFailed";
		return redirectURL;
	}

	private String getRequestPathForSubscriptionPaymentCancelled(String subscriptionId) {
		String redirectURL = getSubscriptionDetailsURI(subscriptionId) + "?paymentCancelled";
		return redirectURL;
	}

	private String getRequestPathForSubscriptionActivated(String subscriptionId) {
		String redirectURL = getSubscriptionDetailsURI(subscriptionId) + "?activated";
		return redirectURL;
	}

	private String getRequestPathForSubscriptionToForbidPaymentTransaction(String subscriptionId) {
		String redirectURL = getSubscriptionDetailsURI(subscriptionId) + "?forbidPaymentTransaction";
		return redirectURL;
	}
}
