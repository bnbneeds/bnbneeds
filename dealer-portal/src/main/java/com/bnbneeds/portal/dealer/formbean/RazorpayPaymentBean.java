package com.bnbneeds.portal.dealer.formbean;

import org.json.JSONArray;
import org.json.JSONObject;

public class RazorpayPaymentBean {
	private String keyId;
	private int amount;
	private String description;
	private String dealerName;
	private String dealerEmailAddress;
	private String dealerMobileNumber;
	private String callbackActionURL;
	private String orderId;
	private String entityId;

	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public void setDealerEmailAddress(String dealerEmailAddress) {
		this.dealerEmailAddress = dealerEmailAddress;
	}

	public void setDealerMobileNumber(String dealerMobileNumber) {
		this.dealerMobileNumber = dealerMobileNumber;
	}

	public String getCallbackActionURL() {
		return callbackActionURL;
	}

	public void setCallbackActionURL(String callbackActionURL) {
		this.callbackActionURL = callbackActionURL;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getKeyId() {
		return keyId;
	}

	public int getAmount() {
		return amount;
	}

	public int getAmountInPaise() {
		return amount * 100;
	}

	public String getDescription() {
		return description;
	}

	public String getDealerName() {
		return dealerName;
	}

	public String getDealerEmailAddress() {
		return dealerEmailAddress;
	}

	public String getDealerMobileNumber() {
		return dealerMobileNumber;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public JSONObject getJSONObjectToCreateOrder() {
		JSONObject options = new JSONObject();
		options.put("amount", getAmountInPaise());
		options.put("currency", "INR");
		options.put("payment_capture", false);
		return options;
	}

	public JSONObject getJSONObjectToCreateInvoice(String itemTitle) {

		JSONObject lineItem = new JSONObject();
		lineItem.put("name", itemTitle);
		lineItem.put("amount", getAmountInPaise());
		lineItem.put("description", description);
		lineItem.put("currency", "INR");
		lineItem.put("quantity", 1);

		JSONArray lineItems = new JSONArray();
		lineItems.put(lineItem);

		JSONObject customer = new JSONObject();
		customer.put("name", dealerName);
		customer.put("email", dealerEmailAddress);
		customer.put("contact", dealerMobileNumber);

		JSONObject request = new JSONObject();
		request.put("type", "invoice");
		request.put("currency", "INR");
		request.put("customer", customer);
		request.put("line_items", lineItems);
		request.put("sms_notify", "0");
		request.put("email_notify", "0");
		return request;
	}
}