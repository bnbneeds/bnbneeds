package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.handleBadRequest;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.writeResponse;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.dealer.BuyerListResponse;
import com.bnbneeds.app.model.dealer.order.AggregatedPurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemInfoResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.PurchaseItemParam.PurchaseStatus;
import com.bnbneeds.app.model.dealer.order.PurchaseListResponse;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.DealerUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.formbean.BuyerAggregatedPurchaseListBean;
import com.bnbneeds.portal.dealer.formbean.BuyerPurchaseItemUpdateBean;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.Constants;
import com.bnbneeds.portal.dealer.util.ReportFileUtils;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.portal.reports.PDFGenerator;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = BuyerPurchaseListController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('BUYER')")
public class BuyerPurchaseListController extends PurchaseListController {

	private static final Logger logger = LoggerFactory.getLogger(BuyerPurchaseListController.class);

	protected static final String BASE_REQUEST_PATH = "/buyers/purchase-items";
	private static final String PURCHASE_ITEM_UPDATE_FORM_URI = BASE_REQUEST_PATH + "/%s/update-form";

	private static final String VENDOR_NAME_LIST_RESPONSE = "vendorListResponse";

	private static final String AGGREGATED_PURCHASE_LIST_VIEW = "buyer-aggregated-purchase-list";
	private static final String PURCHASE_LIST_VIEW = "buyer-purchase-list";
	private static final String PURCHASE_ITEM_STATUS_UPDATE_FORM_VIEW = "buyer-purchase-item-update-form";
	private static final int PURCHASE_LIST_RESULT_SIZE = 20;
	private static final Map<String, String> STATUS_MAP = new HashMap<>();

	static {
		// CANCELLED, FULFILLED, ITEM_RETURNED, REQUIRED_QUANTITY_MODIFIED,
		// VENDOR_CHANGED, ITEM_DESCRIPTION_CHANGED
		STATUS_MAP.put("Cancel Item", "CANCELLED");
		STATUS_MAP.put("Fulfilled", "FULFILLED");
		STATUS_MAP.put("Return Item", "ITEM_RETURNED");
		STATUS_MAP.put("Required Quantity Modified", "REQUIRED_QUANTITY_MODIFIED");
		// STATUS_MAP.put("Chnage Vendor", "VENDOR_CHANGED");
		STATUS_MAP.put("Item Description Changed", "ITEM_DESCRIPTION_CHANGED");
	}

	public static String getPurchaseListURI() {
		return BASE_REQUEST_PATH;
	}

	public static String getPurchaseListURI(String requestDate) {
		return BASE_REQUEST_PATH + String.format("?requestDate=%s", requestDate);
	}

	public static String getPurchaseUpdateFormView(String ItemId) {
		return String.format(PURCHASE_ITEM_UPDATE_FORM_URI, ItemId);
	}

	static void setPurchaseItemStatusMap(Model model) {
		model.addAttribute(PURCHASE_ITEM_STATUS_MAP_ATTRIBUTE, STATUS_MAP);
	}

	@Layout(title = "Purchase List")
	@RequestMapping(method = RequestMethod.GET)
	public String getPurchaseList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "vendor", required = false) String vendor,
			@RequestParam(name = REQ_PARAM_REQUEST_DATE, required = false) String requestDate,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset, Model model) {

		String buyerId = getDealershipId(user);

		Map<String, String> queryParams = new HashMap<>();
		PurchaseListResponse listResponse = null;

		try {
			if (StringUtils.hasText(vendor)) {
				if (vendor.contains("|")) {
					String[] nameAndIdArr = vendor.split("\\|");
					queryParams.put("vendorId", nameAndIdArr[1].trim());
					setSelectedDealer(model, vendor);
				}
			}

			if (StringUtils.hasText(requestDate)) {
				if (StringUtils.isDateFormatValid(requestDate, Constants.DATE_FORMAT)) {
					queryParams.put("requestDate", requestDate);
					model.addAttribute(REQ_PARAM_REQUEST_DATE, requestDate);
				}
			}

			if (!queryParams.isEmpty()) {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = bnbneedsClient.buyers(buyerId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE,
							queryParams, nextOffset);
				} else {
					listResponse = bnbneedsClient.buyers(buyerId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE,
							queryParams);
				}
			} else {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = bnbneedsClient.buyers(buyerId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE,
							nextOffset);
				} else {
					listResponse = bnbneedsClient.buyers(buyerId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE);
				}
			}
			setPurchaseListResponse(model, listResponse);

		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PURCHASE_LIST_VIEW);
		}

		return PURCHASE_LIST_VIEW;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/pdf")
	public void writePurchaseListToPdf(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "vendor", required = false) String vendor,
			@RequestParam(name = REQ_PARAM_REQUEST_DATE, required = false) String requestDate, Model model,
			HttpServletResponse response) throws Exception {

		String buyerId = user.getRelationshipId();

		String date = StringUtils.getDateTimestamp(Constants.DATE_FORMAT, new Date().getTime());

		String destinationFilePath = ReportFileUtils.generatePDFReportFilePath(reportsDirectoryPath, user,
				PDFGenerator.PURCHASE_LIST_REPORT_NAME, date);

		Map<String, String> queryParams = new HashMap<>();

		try {
			if (StringUtils.hasText(vendor)) {
				if (vendor.contains("|")) {
					String[] nameAndIdArr = vendor.split("\\|");
					queryParams.put("vendorId", nameAndIdArr[1].trim());
					setSelectedDealer(model, vendor);
				}
			}

			if (StringUtils.hasText(requestDate)) {
				if (StringUtils.isDateFormatValid(requestDate, Constants.DATE_FORMAT)) {
					queryParams.put("requestDate", requestDate);
				}
			}

			pdfGenerator.generateBuyerPurchaseListReport(buyerId, queryParams, destinationFilePath);
			RequestResponseUtils.writeFileToResponse(destinationFilePath, RequestResponseUtils.MEDIA_TYPE_PDF,
					response);

		} catch (BNBNeedsServiceException e) {
			logger.error("There was a problem occurred while generating Buyer purchase list report.", e);
		} finally {
			FileUtils.deleteQuietly(new File(destinationFilePath));
		}

	}

	@Layout(title = "Update Purchase Item")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/update-form")
	public String getPurchaseItemStatusUpdateForm(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String itemId,
			@ModelAttribute(PURCHASE_ITEM_UPDATE_FORM_ATTRIBUTE) BuyerPurchaseItemUpdateBean form, Model model) {

		String buyerId = getDealershipId(user);

		setPurchaseItemStatusMap(model);

		try {
			PurchaseItemInfoResponse infoResponse = bnbneedsClient.buyers(buyerId).getPurchaseItem(itemId);
			setPurchaseInfoResponse(model, infoResponse);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PURCHASE_ITEM_STATUS_UPDATE_FORM_VIEW);
		}

		return PURCHASE_ITEM_STATUS_UPDATE_FORM_VIEW;

	}

	@Layout(title = "Update Purchase Item")
	@RequestMapping(method = RequestMethod.POST, path = "/{id}/update")
	public String updatePurchaseItem(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String itemId,
			@ModelAttribute(PURCHASE_ITEM_UPDATE_FORM_ATTRIBUTE) BuyerPurchaseItemUpdateBean form, BindingResult result,
			Model model) {

		String buyerId = getDealershipId(user);
		setPurchaseItemStatusMap(model);
		try {
			PurchaseItemInfoResponse infoResponse = bnbneedsClient.buyers(buyerId).getPurchaseItem(itemId);
			setPurchaseInfoResponse(model, infoResponse);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PURCHASE_ITEM_STATUS_UPDATE_FORM_VIEW);
		}

		Double requiredQuantity = form.getRequiredQuantity();
		String unit = form.getUnit();

		if (!result.hasErrors()) {
			PurchaseItemParam param = new PurchaseItemParam();
			if (requiredQuantity != null) {
				param.setRequiredQuantity(requiredQuantity);
			}
			if (StringUtils.hasText(unit)) {
				param.setUnit(unit);
			}
			PurchaseStatus statusUpdate = new PurchaseStatus();
			statusUpdate.setStatus(form.getStatus());
			statusUpdate.setComments(form.getComment());
			param.setStatusUpdate(statusUpdate);
			try {
				bnbneedsClient.buyers(buyerId).updatePurchaseItem(itemId, param);
			} catch (BNBNeedsServiceException e) {
				return handleBadRequest(e, model, PURCHASE_ITEM_STATUS_UPDATE_FORM_VIEW);
			}
		}
		return getRedirectPath(getPurchaseUpdateFormView(itemId) + "?updateSuccess");
	}

	static void setBuyerListResponse(Model model, BuyerListResponse response) {
		model.addAttribute(VENDOR_NAME_LIST_RESPONSE, response);
	}

	@Layout(title = "Aggregated List")
	@RequestMapping(method = RequestMethod.GET, path = "/aggregated-list-form")
	public String getAggregatedListForm(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(AGGREGATED_PURCHASE_LIST_FORM_ATTRIBUTE) BuyerAggregatedPurchaseListBean form) {

		DealerUtils.checkHasDealership(user);

		return AGGREGATED_PURCHASE_LIST_VIEW;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/product-names")
	@ResponseBody
	public synchronized void writeAssociatedProductNameList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_REQUEST_DATE, required = false) String requestDate,
			@RequestParam(name = REQ_PARAM_REQUEST_DATE_RANGE, required = false) String requestDateRange,
			HttpServletResponse response) throws IOException, InterruptedException {

		String productNameListResponse = null;
		try {
			String buyerId = getDealershipId(user);

			if (StringUtils.hasText(requestDate)) {
				if (!StringUtils.isDateFormatValid(requestDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"Request date format is invalid.", response);
					return;
				}
				productNameListResponse = bnbneedsClient.buyers(buyerId)
						.getAssociatedProductNameListOfPurchaseItemsAsString(requestDate);
			} else {
				// Date range
				String[] dateRangeArray = getRequestStartAndEndDate(requestDateRange);
				String startRequestDate = dateRangeArray[0].trim();
				String endRequestDate = dateRangeArray[1].trim();

				if (!StringUtils.isDateFormatValid(startRequestDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"Start date format is invalid.", response);
					return;
				}

				if (!StringUtils.isDateFormatValid(endRequestDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"End date format is invalid.", response);
					return;
				}

				productNameListResponse = bnbneedsClient.buyers(buyerId)
						.getAssociatedProductNameListOfPurchaseItemsByDateRangeAsString(startRequestDate,
								endRequestDate);

			}
			writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE, productNameListResponse, response);

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/vendors")
	@ResponseBody
	public synchronized void writeAssociatedVendorList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_REQUEST_DATE, required = false) String requestDate,
			@RequestParam(name = REQ_PARAM_REQUEST_DATE_RANGE, required = false) String requestDateRange,
			HttpServletResponse response) throws IOException, InterruptedException {

		try {
			String buyerListResponse = null;
			String buyerId = getDealershipId(user);
			if (StringUtils.hasText(requestDate)) {
				if (!StringUtils.isDateFormatValid(requestDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"Request date format is invalid.", response);
					return;
				}
				buyerListResponse = bnbneedsClient.buyers(buyerId)
						.getAssociatedVendorListOfPurchaseItemsAsString(requestDate);
			} else {
				// Date range
				String[] dateRangeArray = getRequestStartAndEndDate(requestDateRange);
				String startRequestDate = dateRangeArray[0].trim();
				String endRequestDate = dateRangeArray[1].trim();

				if (!StringUtils.isDateFormatValid(startRequestDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"Start date format is invalid.", response);
					return;
				}

				if (!StringUtils.isDateFormatValid(endRequestDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"End date format is invalid.", response);
					return;
				}

				buyerListResponse = bnbneedsClient.buyers(buyerId)
						.getAssociatedVendorListOfPurchaseItemsByDateRangeAsString(startRequestDate, endRequestDate);
			}

			writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE, buyerListResponse, response);

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@Layout(title = "Aggregated List")
	@RequestMapping(method = RequestMethod.GET, path = "/aggregated-list")
	public String getAggregatedList(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(AGGREGATED_PURCHASE_LIST_FORM_ATTRIBUTE) BuyerAggregatedPurchaseListBean form,
			BindingResult result, Model model, HttpServletResponse response) throws Exception {

		String buyerId = getDealershipId(user);
		boolean dateRangeGiven = false;
		String startRequestDate = null;
		String endRequestDate = null;
		String vendorId = form.getVendorId();
		String productNameId = form.getProductNameId();
		String requestDate = form.getRequestDate();

		String date = StringUtils.getDateTimestamp(Constants.DATE_FORMAT, new Date().getTime());

		String destinationFilePath = ReportFileUtils.generatePDFReportFilePath(reportsDirectoryPath, user,
				PDFGenerator.AGGREGATED_PURCHASE_LIST_REPORT_NAME, date);

		if (StringUtils.hasText(form.getRequestDate())) {
			if (!StringUtils.isDateFormatValid(form.getRequestDate(), Constants.DATE_FORMAT)) {
				result.rejectValue(REQ_PARAM_REQUEST_DATE, null, "Request date is invalid.");
			}
		} else {
			dateRangeGiven = true;
			String[] dateRangeArray = getRequestStartAndEndDate(form.getRequestDateRange());
			startRequestDate = dateRangeArray[0].trim();
			endRequestDate = dateRangeArray[1].trim();

			if (!StringUtils.isDateFormatValid(startRequestDate, Constants.DATE_FORMAT)) {
				result.rejectValue(REQ_PARAM_REQUEST_DATE_RANGE, null, "Start date format is invalid.");
			}

			if (!StringUtils.isDateFormatValid(endRequestDate, Constants.DATE_FORMAT)) {
				result.rejectValue(REQ_PARAM_REQUEST_DATE_RANGE, null, "End date format is invalid.");
			}
		}

		AggregatedPurchaseListResponse reponse = null;
		if (!result.hasErrors()) {

			try {
				if (form.isPdf()) {

					pdfGenerator.generateBuyerAggregatedPurchaseListReport(buyerId, vendorId, startRequestDate,
							endRequestDate, requestDate, productNameId, destinationFilePath);
					RequestResponseUtils.writeFileToResponse(destinationFilePath, RequestResponseUtils.MEDIA_TYPE_PDF,
							response);
					return null;

				}

				else {
					if (dateRangeGiven) {
						reponse = bnbneedsClient.buyers(buyerId).getAggregatedPurchaseListByDateRange(startRequestDate,
								endRequestDate, form.getProductNameId(), form.getVendorId());
					} else {
						reponse = bnbneedsClient.buyers(buyerId).getAggregatedPurchaseList(form.getRequestDate(),
								form.getProductNameId(), form.getVendorId());
					}
				}

				setAggregatedPurchaseListResponse(model, reponse);
			} catch (BNBNeedsServiceException e) {
				return handleBadRequest(e, model, AGGREGATED_PURCHASE_LIST_VIEW);
			} finally {
				FileUtils.deleteQuietly(new File(destinationFilePath));
			}
		}

		return AGGREGATED_PURCHASE_LIST_VIEW;
	}
}
