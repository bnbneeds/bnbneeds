package com.bnbneeds.portal.dealer.formbean;

public class TenderListBean {

	private String nextOffset;
	private String tenderStatus;

	public TenderListBean() {
		super();
	}

	public String getNextOffset() {
		return nextOffset;
	}

	public String getTenderStatus() {
		return tenderStatus;
	}

	public void setNextOffset(String nextOffset) {
		this.nextOffset = nextOffset;
	}

	public void setTenderStatus(String tenderStatus) {
		this.tenderStatus = tenderStatus;
	}

}
