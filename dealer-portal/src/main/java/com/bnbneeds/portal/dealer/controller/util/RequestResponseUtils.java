package com.bnbneeds.portal.dealer.controller.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ConnectException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;

import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.portal.dealer.exception.HasNoDealershipException;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

public class RequestResponseUtils {

	private static final Logger logger = LoggerFactory.getLogger(RequestResponseUtils.class);

	private static final String REQUEST_CLIENT_IP_ADDRESS_HEADER = "X-Real-IP";

	protected static final String MODEL_ATTRIBUTE_MESSAGE = "message";
	protected static final String MODEL_ATTRIBUTE_ERROR_MESSAGE = "errorMessage";
	public static final String MEDIA_TYPE_PDF = "application/pdf";

	/**
	 * Invalidates the current user session. When the session gets invalidated,
	 * {@code HTTPSessionDestroyedEvent} is triggered that will be caught by the
	 * {@code HTTPSessionEventPublisher}. HTTPSessionEventPublisher is registered in
	 * {@code SecurityConfiguration}.
	 * 
	 * @param request
	 */
	public static void invalidateCurrentSession(HttpServletRequest request) {
		logger.info("Called invalidateCurrentSession()");
		HttpSession session = request.getSession(false);
		if (session != null) {

			logger.info("Session to invalidate: {}", session.getId());
			session.invalidate();
		}
		logger.info("Exit invalidateCurrentSession()");
	}

	public static void setMessage(String message, Model model) {
		model.addAttribute(MODEL_ATTRIBUTE_MESSAGE, message);
	}

	public static void setErrorMessage(String message, Model model) {
		model.addAttribute(MODEL_ATTRIBUTE_ERROR_MESSAGE, message);
	}

	public static String handleBadRequest(BNBNeedsServiceException e, Model model, String failureViewName) {

		setErrorMessageForBadRequest(e, model);
		return failureViewName;
	}

	public static String handleBadRequest(BNBNeedsServiceException e, Model model, String errorMessageAttributeName,
			String failureViewName) {

		setErrorMessageForBadRequest(e, model, errorMessageAttributeName);
		return failureViewName;
	}

	public static void setErrorMessageForBadRequest(BNBNeedsServiceException e, Model model) {
		setErrorMessageForBadRequest(e, model, MODEL_ATTRIBUTE_ERROR_MESSAGE);
	}

	public static void setErrorMessageForBadRequest(BNBNeedsServiceException e, Model model,
			String errorMessageAttributeName) {

		switch (e.getHttpCode()) {
		case 400:
		case 403:
		case 404:
			TaskResponse task = e.getTask();
			if (task != null) {
				model.addAttribute(errorMessageAttributeName, task.getMessage());
			}
			break;
		case 411:
			model.addAttribute(errorMessageAttributeName, e.getMessage());
			break;
		default:
			throw e;
		}
	}

	public static void writeImageToResponse(Response clientResponse, String mediaType, HttpServletResponse httpResponse)
			throws IOException {

		if (HttpServletResponse.SC_OK != clientResponse.getStatus()) {
			throw new IllegalStateException(
					"Cannot write image. Client response status: " + clientResponse.getStatusInfo().getReasonPhrase());
		}

		Object entity = clientResponse.getEntity();
		if (entity != null && entity instanceof InputStream) {
			InputStream in = (InputStream) entity;
			httpResponse.setContentType(mediaType);

			OutputStream out = null;
			try {
				out = httpResponse.getOutputStream();
				byte[] buf = new byte[1024];
				int count = 0;
				while ((count = in.read(buf)) >= 0) {
					out.write(buf, 0, count);
				}
			} finally {
				in.close();
				if (out != null) {
					out.flush();
					out.close();
				}
			}
		}
	}

	public static void writeResponse(int status, String contentType, String responseText,
			HttpServletResponse httpResponse) throws IOException {

		if (responseText == null) {
			return;
		}

		httpResponse.setStatus(status);
		httpResponse.setContentType(contentType);
		PrintWriter writer = null;
		try {
			writer = httpResponse.getWriter();
			if (writer != null) {
				writer.write(responseText);
			}
		} finally {
			if (writer != null) {
				writer.flush();
				writer.close();
			}
		}
	}

	public static void writeImageToResponse(byte[] byteArray, String mediaType, HttpServletResponse httpResponse)
			throws IOException {

		httpResponse.setContentType(mediaType);

		OutputStream out = null;
		try {
			out = httpResponse.getOutputStream();
			if (out != null) {
				out.write(byteArray);
			}
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

	public static void writeFileToResponse(String filePath, String contentType, HttpServletResponse response)
			throws IOException {

		FileInputStream fileInputStream = null;
		OutputStream responseOutputStream = null;
		try {
			File file = new File(filePath);
			fileInputStream = new FileInputStream(file);
			response.setContentType(contentType);
			response.addHeader("Content-Disposition",
					"inline; filename=" + filePath.substring(filePath.indexOf("reports") + 8));
			response.setContentLength((int) file.length());
			responseOutputStream = response.getOutputStream();

			int length = 0;
			byte[] buffer = new byte[4096];
			while ((length = fileInputStream.read(buffer)) > 0) {
				responseOutputStream.write(buffer, 0, length);
			}
		} finally {
			if (fileInputStream != null) {
				fileInputStream.close();
			}
			if (responseOutputStream != null) {
				responseOutputStream.flush();
				responseOutputStream.close();
			}
		}
	}

	public static void writeExceptionToResponse(Exception e, HttpServletResponse response) throws IOException {
		if (e instanceof BNBNeedsServiceException) {
			BNBNeedsServiceException ex = (BNBNeedsServiceException) e;
			String message = null;
			if (ex.getTask() != null) {
				message = ex.getTask().getMessage();
			} else {
				message = ex.getMessage();
			}
			logger.error(message);
			writeResponse(ex.getHttpCode(), MediaType.TEXT_PLAIN_VALUE, message, response);
		} else if (e instanceof HasNoDealershipException) {
			writeResponse(HttpServletResponse.SC_FORBIDDEN, MediaType.TEXT_PLAIN_VALUE, e.getMessage(), response);
		} else if (e instanceof ConnectException) {
			writeResponse(HttpServletResponse.SC_SERVICE_UNAVAILABLE, MediaType.TEXT_PLAIN_VALUE,
					"Unable to reach BNBneeds. Please try again later.", response);
		} else if (e instanceof ProcessingException) {
			if (e.getCause() instanceof ConnectException) {
				writeResponse(HttpServletResponse.SC_SERVICE_UNAVAILABLE, MediaType.TEXT_PLAIN_VALUE,
						"Unable to reach BNBneeds. Please try again later.", response);
			} else {
				writeResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, MediaType.TEXT_PLAIN_VALUE, e.getMessage(),
						response);
			}
		} else {
			writeResponse(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, MediaType.TEXT_PLAIN_VALUE, e.getMessage(),
					response);
		}
	}

	/**
	 * Returns the client IP address. First it checks the value in the request
	 * header: {@code X-Real-IP}. If it is null, then it return the value using
	 * {@code httpRequest.getRemoteAddr()}
	 * 
	 * @param httpRequest
	 *            - the request
	 * @return the IP address of the requested client
	 */
	public static String getClientIPAddress(HttpServletRequest httpRequest) {

		String clientAddress = httpRequest.getHeader(REQUEST_CLIENT_IP_ADDRESS_HEADER);

		if (clientAddress != null) {
			logger.info("Requested client IP address specified in header {}: {}", REQUEST_CLIENT_IP_ADDRESS_HEADER,
					clientAddress);
		} else {
			clientAddress = httpRequest.getRemoteAddr();
			logger.info("Requested client IP adrress: {}", clientAddress);
		}

		return clientAddress;
	}

}
