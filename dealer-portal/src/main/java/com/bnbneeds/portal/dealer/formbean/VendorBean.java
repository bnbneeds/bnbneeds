package com.bnbneeds.portal.dealer.formbean;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import com.bnbneeds.portal.dealer.util.StringUtils;

public class VendorBean extends DealerRegistrationBean {

	@Pattern(regexp = StringUtils.REGEX_VAT_NUMBER, message = "VAT number is invalid.")
	private String vatNumber;

	@Pattern(regexp = StringUtils.REGEX_SERVICE_TAX_NUMBER, message = "Service tax number is invalid.")
	private String serviceTaxNumber;
	@NotBlank
	private String cities;

	public String getVatNumber() {
		return vatNumber;
	}

	public String getServiceTaxNumber() {
		return serviceTaxNumber;
	}

	public String getCities() {
		return cities;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public void setServiceTaxNumber(String serviceTaxNumber) {
		this.serviceTaxNumber = serviceTaxNumber;
	}

	public void setCities(String cities) {
		this.cities = cities;
	}

}
