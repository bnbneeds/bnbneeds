package com.bnbneeds.portal.dealer.formbean;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class UserAccountProfileBean {

	@NotBlank
	@Size(min = 4, message = "Name is too short.")
	private String accountName;

	@NotBlank
	private String mobileNumber;

	@NotBlank
	@Email(message = "E-mail address is invalid.")
	private String username;

	public String getAccountName() {
		return accountName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateUserAccountParam [");
		if (accountName != null)
			builder.append("accountName=").append(accountName).append(", ");
		if (mobileNumber != null)
			builder.append("mobileNumber=").append(mobileNumber);
		builder.append("]");
		return builder.toString();
	}

}
