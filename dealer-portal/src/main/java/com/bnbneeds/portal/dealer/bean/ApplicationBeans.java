package com.bnbneeds.portal.dealer.bean;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

@ApplicationScope
@Component
public class ApplicationBeans {

	private MaintenanceNoticeBean maintenanceNotice;

	public MaintenanceNoticeBean getMaintenanceNotice() {
		return maintenanceNotice;
	}

	public void setMaintenanceNotice(MaintenanceNoticeBean maintenanceNotice) {
		this.maintenanceNotice = maintenanceNotice;
	}

}
