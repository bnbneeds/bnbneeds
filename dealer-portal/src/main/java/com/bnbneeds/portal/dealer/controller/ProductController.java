package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.DealerUtils.checkHasDealership;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.handleBadRequest;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.writeImageToResponse;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.writeResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.BlobResourceRep;
import com.bnbneeds.app.model.dealer.VendorInfoResponse;
import com.bnbneeds.app.model.product.ProductImageListResponse;
import com.bnbneeds.app.model.product.ProductInfoListResponse;
import com.bnbneeds.app.model.product.ProductInfoResponse;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.controller.util.ReviewUtility;
import com.bnbneeds.portal.dealer.formbean.QueryProductsBean;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = ProductController.BASE_PATH)
@PreAuthorize("hasAnyAuthority('VENDOR','BUYER')")
public class ProductController extends DealerController {

	protected static final String BASE_PATH = "/products";
	private static final String LIST_REQUEST_URI = "/list";
	public static final String LIST_PRODUCTS_REQUEST_URI = BASE_PATH
			+ LIST_REQUEST_URI;
	private static final String PRODUCT_DETAILS_URI = BASE_PATH + "/%s/details";
	private static final String PRODUCT_IMAGE_URI = BASE_PATH
			+ "/%1$s/images/%2$s?mediaType=%3$s";

	private static final String PRODUCT_LIST_RESPONSE_ATTRIBUTE = "listResponse";
	private static final String PRODUCT_INFO_RESPONSE_ATTRIBUTE = "productInfo";
	private static final String PRODUCT_INFO_URL_ATTRIBUTE = "productInfoURL";
	private static final String PRODUCT_IMAGE_KEYS_ATTRIBUTE = "productImageKeys";

	private static final String PRODUCT_SEARCH_VIEW = "product-search";
	private static final String QUERY_PRODUCTS_FORM_BEAN = "queryProductsBean";
	private static final String PRODUCT_SEARCH_FILTER_TEXT_ATTRIBUTE = "productSearchFilterText";
	private static final int PRODUCT_SEARCH_RESULT_SIZE = 30;

	static final String PRODUCT_LIST_VIEW = "product-list";
	static final String PRODUCT_DETAILS_VIEW = "product-details";

	@Layout(title = "Products")
	@RequestMapping(method = RequestMethod.GET, path = LIST_REQUEST_URI)
	public String list(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "vendor", required = false) String vendor,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			Model model) {

		checkHasDealership(user);

		ProductInfoListResponse response = null;
		Map<String, String> queryMap = new HashMap<String, String>();
		if (StringUtils.hasText(vendor) && vendor.contains("|")) {
			setSelectedDealer(model, vendor);
			String vendorId = vendor.split("\\|")[1];
			queryMap.put("vendorId", vendorId);
		}
		try {

			if (!queryMap.isEmpty()) {
				if (StringUtils.hasText(nextOffset)) {
					response = bnbneedsClient.products().list(
							RESULT_FETCH_SIZE, queryMap, nextOffset);
				} else {
					response = bnbneedsClient.products().list(
							RESULT_FETCH_SIZE, queryMap);
				}
			} else {
				if (StringUtils.hasText(nextOffset)) {
					response = bnbneedsClient.products().list(
							RESULT_FETCH_SIZE, nextOffset);
				} else {
					response = bnbneedsClient.products()
							.list(RESULT_FETCH_SIZE);
				}
			}
			model.addAttribute(PRODUCT_LIST_RESPONSE_ATTRIBUTE, response);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PRODUCT_LIST_VIEW);
		}

		return PRODUCT_LIST_VIEW;
	}

	@Layout(title = "Product Information")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/details")
	public String getDetails(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String productId, Model model) {

		checkHasDealership(user);

		try {
			ProductInfoResponse productInfo = bnbneedsClient
					.products(productId).get();

			VendorInfoResponse vendorInfo = bnbneedsClient.vendors(
					productInfo.getVendor().getId()).get();

			ProductImageListResponse imageListResponse = productInfo
					.getImageList();

			setProductImageLinks(imageListResponse, model);

			ReviewUtility.setReviewListResponse(bnbneedsClient,
					ReviewFor.product, productId, null, null, model);
			ReviewUtility.setAverageRatingResponse(bnbneedsClient,
					ReviewFor.product, productId, model);

			return setProductInfoModelAttributeAndReturnProductDetailsView(
					vendorInfo, productInfo, model);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PRODUCT_DETAILS_VIEW);
		}

	}

	@RequestMapping(method = RequestMethod.GET, path = "/{id}/detail-as-text")
	public synchronized void writeProductDetails(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String productId, HttpServletResponse response)
			throws IOException {

		try {
			checkHasDealership(user);
			String productInfo = bnbneedsClient.products(productId)
					.getAsString();

			writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, productInfo, response);

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}

	}

	@RequestMapping(method = RequestMethod.GET, path = "/{id}/images/{imageKey}")
	@ResponseBody
	public synchronized void writeProductImage(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String productId,
			@PathVariable("imageKey") String imageKey,
			@RequestParam("mediaType") String mediaType,
			HttpServletResponse httpResponse) throws IOException,
			InterruptedException {

		byte[] image = null;
		try {
			checkHasDealership(user);
			image = bnbneedsClient.products(productId).getImage(imageKey,
					mediaType);

			writeImageToResponse(image, mediaType, httpResponse);
		} catch (Exception e) {
			image = getBrokenImage();
			RequestResponseUtils.writeImageToResponse(image,
					MediaType.IMAGE_PNG_VALUE, httpResponse);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{id}/image")
	@ResponseBody
	public synchronized void writeAnyOneProductImage(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String productId,
			HttpServletResponse httpResponse) throws IOException,
			InterruptedException {

		checkHasDealership(user);
		byte[] image = null;
		String mediaType = null;
		boolean imageNotAvailable = false;
		try {
			ProductImageListResponse imageListResponse = bnbneedsClient
					.products(productId).getImageLinks();
			if (imageListResponse != null) {
				List<BlobResourceRep> imageListResp = imageListResponse
						.getImageLinks();
				if (imageListResp != null && !imageListResp.isEmpty()) {
					BlobResourceRep firstImage = imageListResp.get(0);
					image = bnbneedsClient.products(productId).getImage(
							firstImage.getId(), firstImage.getMediaType());
					mediaType = firstImage.getMediaType();
				} else {
					imageNotAvailable = true;
				}
			} else {
				imageNotAvailable = true;
			}
			if (imageNotAvailable) {
				mediaType = MediaType.IMAGE_JPEG_VALUE;
				image = getImageNotAvailable();
			}
			writeImageToResponse(image, mediaType, httpResponse);
		} catch (Exception e) {
			image = getBrokenImage();
			RequestResponseUtils.writeImageToResponse(image,
					MediaType.IMAGE_PNG_VALUE, httpResponse);
		}
	}

	public static String getProductDetailsURI(String productId) {
		return String.format(PRODUCT_DETAILS_URI, productId);
	}

	public static String getProductDetailsURI(String productId,
			String queryParam) {
		return String.format("%1$s?%2$s", getProductDetailsURI(productId),
				queryParam);
	}

	static void setProductDetailsURL(String productId, Model model) {
		model.addAttribute(PRODUCT_INFO_URL_ATTRIBUTE,
				getProductDetailsURI(productId));
	}

	static void setProductInfoModelAttribute(ProductInfoResponse productInfo,
			Model model) {
		String description = productInfo.getDescription();
		if (StringUtils.hasText(description)) {
			description = StringUtils.convertLineEndingsToHtml(description);
			productInfo.setDescription(description);
		}

		String videoURL = productInfo.getVideoURL();
		if (StringUtils.hasText(videoURL)) {
			String youtubeEmbedVideoURL = StringUtils
					.getYoutubeEmbedURLFromWatchURL(videoURL);
			productInfo.setVideoURL(youtubeEmbedVideoURL);
		}

		String tutorialVideoURL = productInfo.getTutorialVideoURL();
		if (StringUtils.hasText(tutorialVideoURL)) {
			String youtubeEmbedVideoURL = StringUtils
					.getYoutubeEmbedURLFromWatchURL(tutorialVideoURL);
			productInfo.setTutorialVideoURL(youtubeEmbedVideoURL);
		}

		setProductDetailsURL(productInfo.getId(), model);
		model.addAttribute(PRODUCT_INFO_RESPONSE_ATTRIBUTE, productInfo);
	}

	static String setProductInfoModelAttributeAndReturnProductDetailsView(
			VendorInfoResponse vendorInfo, ProductInfoResponse productInfo,
			Model model) {

		setProductInfoModelAttribute(productInfo, model);

		VendorOperationsController.setVendorInfoAttribute(vendorInfo, model);

		return PRODUCT_DETAILS_VIEW;
	}

	static void setProductImageLinks(
			ProductImageListResponse imageListResponse, Model model) {

		if (imageListResponse != null
				&& imageListResponse.getImageLinks() != null) {
			String productId = imageListResponse.getProductId();

			Map<String, String> imageKeyToURLMap = new HashMap<String, String>();
			for (BlobResourceRep imageLink : imageListResponse.getImageLinks()) {
				String imageURL = getProductImageLink(productId,
						imageLink.getId(), imageLink.getMediaType());
				imageKeyToURLMap.put(imageLink.getId(), imageURL);
			}
			model.addAttribute(PRODUCT_IMAGE_KEYS_ATTRIBUTE, imageKeyToURLMap);
		}
	}

	static String getProductImageLink(String productId, String imageKey,
			String mediaType) {
		String imageURL = String.format(PRODUCT_IMAGE_URI, productId, imageKey,
				mediaType);
		return imageURL;
	}

	@Layout(title = "Browse Products")
	@RequestMapping(method = RequestMethod.GET, path = "/search")
	public String getSearchProductsForm(
			@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(QUERY_PRODUCTS_FORM_BEAN) QueryProductsBean form,
			Model model) {

		try {

			checkHasDealership(user);
			Map<String, String> queryMap = constructProductQueryMap(form);
			ProductInfoListResponse listResponse = getProductInfoListResponse(
					queryMap, form.getNextOffset());

			form.setNextOffset(listResponse.getNextOffset());
			String filterText = form.getQueryFilterText();
			if (StringUtils.hasText(filterText)) {
				model.addAttribute(PRODUCT_SEARCH_FILTER_TEXT_ATTRIBUTE,
						filterText);
			}
			model.addAttribute(PRODUCT_LIST_RESPONSE_ATTRIBUTE, listResponse);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PRODUCT_SEARCH_VIEW);
		}
		return PRODUCT_SEARCH_VIEW;
	}

	private ProductInfoListResponse getProductInfoListResponse(
			Map<String, String> queryMap, String nextOffset) {

		ProductInfoListResponse listResponse = null;

		boolean queryPresent = (queryMap != null && !queryMap.isEmpty());

		if (StringUtils.hasText(nextOffset)) {
			if (queryPresent) {
				listResponse = bnbneedsClient.products().list(
						PRODUCT_SEARCH_RESULT_SIZE, queryMap, nextOffset);
			} else {
				listResponse = bnbneedsClient.products().list(
						PRODUCT_SEARCH_RESULT_SIZE, nextOffset);
			}
		} else {
			if (queryPresent) {
				listResponse = bnbneedsClient.products().list(
						PRODUCT_SEARCH_RESULT_SIZE, queryMap);
			} else {
				listResponse = bnbneedsClient.products().list(
						PRODUCT_SEARCH_RESULT_SIZE);
			}
		}

		return listResponse;
	}

	private static Map<String, String> constructProductQueryMap(
			QueryProductsBean form) {

		if (form == null) {
			return null;
		}

		Map<String, String> queryMap = new HashMap<String, String>();

		String productType = form.getProductType();
		if (StringUtils.hasText(productType) && productType.contains("|")) {
			String productTypeId = productType.split("\\|")[1].trim();
			queryMap.put("productTypeId", productTypeId);
		}

		String productCategory = form.getProductCategory();
		if (StringUtils.hasText(productCategory)
				&& productCategory.contains("|")) {
			String productCategoryId = productCategory.split("\\|")[1].trim();
			queryMap.put("productCategoryId", productCategoryId);
		}

		String productName = form.getProductName();
		if (StringUtils.hasText(productName) && productName.contains("|")) {
			String productNameId = productName.split("\\|")[1].trim();
			queryMap.put("productNameId", productNameId);
		}

		String vendor = form.getVendor();
		if (StringUtils.hasText(vendor) && vendor.contains("|")) {
			String vendorId = vendor.split("\\|")[1].trim();
			queryMap.put("vendorId", vendorId);
		}

		int minCostPrice = form.getMinCostPrice();
		if (minCostPrice > 0) {
			queryMap.put("minCostPrice", String.valueOf(minCostPrice));
		}
		int maxCostPrice = form.getMaxCostPrice();
		if (maxCostPrice > 0) {
			queryMap.put("maxCostPrice", String.valueOf(maxCostPrice));
		}

		return queryMap;

	}
}
