package com.bnbneeds.portal.dealer.formbean;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.bnbneeds.portal.dealer.util.StringUtils;

public class ChangePasswordBean {

	@NotBlank
	private String currentPassword;
	@NotBlank
	@Size(min = 6, max = 30, message = "Invalid Password.")
	@Pattern(regexp = StringUtils.REGEX_PASSWORD, message = "Invalid Password.")
	private String newPassword;
	@NotBlank
	private String confirmPassword;

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ChangePasswordBean [");
		if (currentPassword != null) {
			builder.append("currentPassword=****");
			builder.append(", ");
		}
		if (newPassword != null) {
			builder.append("newPassword=****");
			builder.append(", ");
		}
		if (confirmPassword != null) {
			builder.append("confirmPassword=****");
		}
		builder.append("]");
		return builder.toString();
	}

}
