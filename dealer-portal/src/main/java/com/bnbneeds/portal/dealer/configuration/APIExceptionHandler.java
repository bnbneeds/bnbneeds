package com.bnbneeds.portal.dealer.configuration;

import java.io.IOException;
import java.net.ConnectException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;
import org.springframework.web.servlet.view.RedirectView;

import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.ErrorPageController;
import com.bnbneeds.portal.dealer.controller.HomePageController;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.exception.APIException;
import com.bnbneeds.portal.dealer.exception.BadRequestException;
import com.bnbneeds.portal.dealer.exception.ForbiddenException;
import com.bnbneeds.portal.dealer.exception.HasNoDealershipException;
import com.bnbneeds.portal.dealer.exception.InternalException;
import com.bnbneeds.portal.dealer.exception.ResourceNotFoundException;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@SuppressWarnings("deprecation")
@ControllerAdvice
@Layout(title = "Oops!", view = ErrorPageController.VIEW_TEMPLATE)
public class APIExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(APIExceptionHandler.class);

	private static final String MODEL_ATTRIBUTE_MESSAGE = "message";
	private static final String ERROR_VIEW = "error";

	private ModelAndView getErrorView(String message) {
		ModelAndView mav = new ModelAndView();
		mav.addObject(MODEL_ATTRIBUTE_MESSAGE, message);
		mav.setViewName(ERROR_VIEW);
		return mav;
	}

	private ModelAndView getErrorView(Exception ex) {
		return getErrorView(ex.getMessage());
	}

	/*
	 * 400 - BadRequest
	 */
	@ExceptionHandler({ MissingServletRequestPartException.class, MissingServletRequestParameterException.class,
			MethodArgumentNotValidException.class, HttpMessageNotReadableException.class, BindException.class,
			TypeMismatchException.class, BadRequestException.class, NumberFormatException.class })
	public ModelAndView handleBadRequestException(Exception ex) {

		return getErrorView(ex);

	}

	/*
	 * 404 - NotFound
	 */
	@SuppressWarnings("deprecation")
	@ExceptionHandler({ NoSuchRequestHandlingMethodException.class, NoHandlerFoundException.class,
			ResourceNotFoundException.class })
	public ModelAndView handleResourceNotFoundException(Exception ex) {

		return getErrorView(ex);

	}

	/*
	 * 500 Internal Server Error
	 */

	@ExceptionHandler({ ConversionNotSupportedException.class, HttpMessageNotWritableException.class,
			ServletException.class, APIException.class, InternalException.class })
	public ModelAndView handleInternalServerError(Exception ex, Model model) {
		logger.error("Exception {} occured.");
		return getErrorView(ex);
	}

	@ExceptionHandler({ ConnectException.class })
	public RedirectView handleConnectionRefusedError(Exception ex) {

		logger.error("Unable to connect to BNBNeeds service.", ex);

		RedirectView redirect = new RedirectView(ErrorPageController.ATTEMPT_RECONNECT_URL, true);

		return redirect;
	}

	/*
	 * 415 Unsupported Media Type.
	 */
	@ExceptionHandler({ HttpMediaTypeNotSupportedException.class })
	public ModelAndView handleMediaTypeNotSupportedException(Exception ex, Model model) {

		return getErrorView(ex);
	}

	/*
	 * 406 - Not Acceptable
	 */
	@ExceptionHandler({ HttpMediaTypeNotAcceptableException.class })
	public ModelAndView handleMediaTypeNotAcceptedException(Exception ex, Model model) {

		return getErrorView(ex);

	}

	/*
	 * 403 - Forbidden
	 */
	@ExceptionHandler({ ForbiddenException.class })
	public ModelAndView handleForbiddenException(Exception ex, Model model) {

		return getErrorView(ex);

	}

	@ExceptionHandler({ BNBNeedsServiceException.class })
	public RedirectView handleBNBNeedsServiceException(BNBNeedsServiceException ex, HttpServletRequest request)
			throws IOException {

		RedirectView view = null;

		switch (ex.getHttpCode()) {
		case 401:
			RequestResponseUtils.invalidateCurrentSession(request);
			// view = new RedirectView(AuthenticationController.LOGOUT_URL,
			// true);
			break;

		default:
			view = new RedirectView(HomePageController.HOMEPAGE_URL, true);
			break;
		}
		return view;
	}

	@ExceptionHandler({ HasNoDealershipException.class })
	public RedirectView handleHasNoDealershipException(Model model) throws IOException {

		RedirectView view = new RedirectView(HomePageController.HOMEPAGE_URL, true);
		return view;
	}

}
