package com.bnbneeds.portal.dealer.formbean;

import org.hibernate.validator.constraints.NotBlank;

public class ResetPasswordBean {

	@NotBlank
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResetPasswordBean [");
		if (username != null) {
			builder.append("username=");
			builder.append(username);
		}
		builder.append("]");
		return builder.toString();
	}

}
