package com.bnbneeds.portal.dealer.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.portal.dealer.annotation.Layout;

@Controller
@RequestMapping(path = ErrorPageController.ERRORS_URL)
@Layout(title = "Oops!", view = ErrorPageController.VIEW_TEMPLATE)
public class ErrorPageController extends AbstractBaseController {

	public static final String VIEW_TEMPLATE = "error";

	protected static final String ERRORS_URL = "/errors";
	private static final String RECONNECT_URL = "/attempt-reconnect";
	private static final String RECONNECT_VIEW = "reconnect";
	private static final String MAINTENANCE_VIEW = "maintenance";
	private static final String MODEL_ATTRIBUTE_PING_URL = "pingURL";
	private static final String MODEL_ATTRIBUTE_MAINTENANCE_WINDOW = "maintenanceWindow";

	public static final String ATTEMPT_RECONNECT_URL = ERRORS_URL + RECONNECT_URL;

	@Value("${maintenance.mode}")
	private boolean maintenanceMode;

	@Value("${maintenance.window}")
	private String maintenanceWindow;

	@RequestMapping(method = RequestMethod.GET, path = { RECONNECT_URL })
	public String attemptReconnect(Model model) {

		if (maintenanceMode) {
			model.addAttribute(MODEL_ATTRIBUTE_MAINTENANCE_WINDOW, maintenanceWindow);
			return MAINTENANCE_VIEW;

		}

		String pingURL = request.getContextPath() + PublicWebsiteController.getPingBNBneedsURL();
		model.addAttribute(MODEL_ATTRIBUTE_PING_URL, pingURL);
		return RECONNECT_VIEW;
	}
}
