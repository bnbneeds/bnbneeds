package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.DealerUtils.checkHasDealership;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.handleBadRequest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.dealer.tender.CreateTenderBidParam;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderPreviewListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderProductInfoListResponse;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.DealerUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.formbean.PostBidRequestBean;
import com.bnbneeds.portal.dealer.formbean.TenderListBean;
import com.bnbneeds.portal.dealer.formbean.TenderPreviewListRequestBean;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.core.TenderBids;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.google.gson.Gson;

@Controller
@RequestMapping(path = TenderBidController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('VENDOR')")
public class TenderBidController extends TenderController {

	static final String TENDER_DETAILS_VIEW = "vendor-tender-details";
	static final String TENDER_PREVIEW_LIST_VIEW = "vendor-tender-preview-list";
	private static final String TENDER_LIST_REQUEST_URI = "/list";
	protected static final String BASE_REQUEST_PATH = "/tenders";
	private static final String PREVIEW_RESPONSE_ATTRIBUTE = "listResponse";
	private static final String TENDER_PREVIEW_LIST_REQUEST_FORM_ATTRIBUTE = "tenderPreviewListRequestForm";
	private static final String LOWEST_BID_ATTRIBUTE = "lowestBidInfo";
	private static final String CURRENT_BID_ATTRIBUTE = "currentBidInfo";
	private static final String POST_BID_REQUEST_FORM_ATTRIBUTE = "postBidForm";
	private static final String LIST_TENDER_FORM_ATTRIBUTE = "listTenderFormBean";
	private static final String TENDER_LIST_RESPONSE_ATTRIBUTE = "listResponse";
	private static final String TENDER_LIST_VIEW = "vendor-tender-list";
	private static final String TENDER_DETAILS_REQUEST_URI = "/{tenderId}/details";
	private static final String TENDER_BID_PRODUCT_LIST_RESPONSE_ATTRIBUTE = "tenderBidProductList";
	private static final String TENDER_BID_LIST_RESPONSE_ATTRIBUTE = "bidListResponse";
	private static final String LIST_TENDER_PREVIEWS_URI = "/preview-list";

	public static String getTenderPreviewsUrl() {
		return BASE_REQUEST_PATH + LIST_TENDER_PREVIEWS_URI;
	}

	public static String getPathForGetTenderDetails(String tenderId) {
		return BASE_REQUEST_PATH + TENDER_DETAILS_REQUEST_URI.replace("{tenderId}", tenderId);
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{tenderId}/post-bid")
	public String postBid(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(POST_BID_REQUEST_FORM_ATTRIBUTE) PostBidRequestBean form,
			@PathVariable("tenderId") String tenderId, Model model) {
		checkHasDealership(user);
		try {

			Double value = form.getValue();
			String comments = form.getComments();

			CreateTenderBidParam createBidParam = new CreateTenderBidParam();

			createBidParam.setValue(value);
			createBidParam.setComments(comments);

			TenderBids tenderBidsClient = bnbneedsClient.tenderBids();
			tenderBidsClient.postBid(tenderId, createBidParam);

		} catch (BNBNeedsServiceException e) {
			return getRedirectPath(getPathForGetTenderDetails(tenderId) + "?postBidFailed=" + e.getMessage());
		}
		return getRedirectPath(getPathForGetTenderDetails(tenderId) + "?postBidSuccess");
	}

	@Layout(title = "Tender Previews")
	@RequestMapping(method = RequestMethod.GET, path = LIST_TENDER_PREVIEWS_URI)
	public String getTenderPreviews(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(TENDER_PREVIEW_LIST_REQUEST_FORM_ATTRIBUTE) TenderPreviewListRequestBean form,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset, Model model) {
		checkHasDealership(user);
		try {
			setModelForTenderPreviewListView(model, nextOffset);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, TENDER_LIST_VIEW);
		}
		return TENDER_PREVIEW_LIST_VIEW;
	}

	protected void setModelForTenderPreviewListView(Model model, String nextOffset) {

		try {
			TenderPreviewListResponse listResponse = null;
			TenderBids tenderBidsClient = bnbneedsClient.tenderBids();
			if (StringUtils.hasText(nextOffset)) {
				listResponse = tenderBidsClient.listTenderPreviews(RESULT_FETCH_SIZE, nextOffset);
			} else {
				listResponse = tenderBidsClient.listTenderPreviews(RESULT_FETCH_SIZE);
			}

			model.addAttribute(PREVIEW_RESPONSE_ATTRIBUTE, listResponse);
		} catch (BNBNeedsServiceException e) {
			throw e;
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/bids/list-as-text")
	@ResponseBody
	public synchronized void writeBids(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			HttpServletResponse response) throws IOException {

		try {
			DealerUtils.checkHasDealership(user);
			TenderBidInfoListResponse bidListResponse = null;
			TenderBids tenderBidsClient = bnbneedsClient.tenderBids();
			if (StringUtils.hasText(nextOffset)) {
				bidListResponse = tenderBidsClient.listTenderBids(tenderId, FETCH_SIZE, nextOffset);
			} else {
				bidListResponse = tenderBidsClient.listTenderBids(tenderId, FETCH_SIZE);
			}

			if (bidListResponse != null && !bidListResponse.isEmpty()) {
				for (Iterator<TenderBidInfoResponse> iterator = bidListResponse.getIterator(); iterator.hasNext();) {
					TenderBidInfoResponse info = iterator.next();
					setBidTimestamp(info);
					maskBidDetails(user, info);
				}
			}

			Gson gson = new Gson();
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE,
					gson.toJson(bidListResponse), response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/products/list-as-text")
	@ResponseBody
	public synchronized void writeTenderProducts(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			HttpServletResponse response) throws IOException {
		try {
			DealerUtils.checkHasDealership(user);
			String bidProductListResponse;
			TenderBids tenderBidsClient = bnbneedsClient.tenderBids();
			if (StringUtils.hasText(nextOffset)) {
				bidProductListResponse = tenderBidsClient.getTenderProductsAsString(tenderId, nextOffset);
			} else {
				bidProductListResponse = tenderBidsClient.getTenderProductsAsString(tenderId);
			}
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE,
					bidProductListResponse, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/preview-products/list-as-text")
	@ResponseBody
	public synchronized void writeTenderProductsPreview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			HttpServletResponse response) throws IOException {

		final int FETCH_SIZE = 10;

		try {
			DealerUtils.checkHasDealership(user);
			String bidProductListResponse = null;
			TenderBids tenderBidsClient = bnbneedsClient.tenderBids();

			if (StringUtils.hasText(nextOffset)) {
				bidProductListResponse = tenderBidsClient.previewTenderProductsAsString(tenderId, FETCH_SIZE,
						nextOffset);
			} else {
				bidProductListResponse = tenderBidsClient.previewTenderProductsAsString(tenderId, FETCH_SIZE);
			}
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE,
					bidProductListResponse, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@Layout(title = "Accesed Tenders")
	@RequestMapping(method = RequestMethod.GET, path = TENDER_LIST_REQUEST_URI)
	public String getTenderList(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(LIST_TENDER_FORM_ATTRIBUTE) TenderListBean form, Model model) {
		DealerUtils.checkHasDealership(user);
		loadTenderQueryStatusMap(model);
		TenderBids tenderBidsClient = bnbneedsClient.tenderBids();

		String tenderStatus = form.getTenderStatus();
		String nextOffset = form.getNextOffset();
		Map<String, String> queryParams = new HashMap<>();
		if (StringUtils.hasText(tenderStatus)) {
			queryParams.put("status", tenderStatus);
		}
		TenderInfoListResponse listResponse = null;
		try {

			if (!queryParams.isEmpty()) {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = tenderBidsClient.listTenders(RESULT_FETCH_SIZE, queryParams, nextOffset);
				} else {
					listResponse = tenderBidsClient.listTenders(RESULT_FETCH_SIZE, queryParams);
				}
			} else {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = tenderBidsClient.listTenders(RESULT_FETCH_SIZE, nextOffset);
				} else {
					listResponse = tenderBidsClient.listTenders(RESULT_FETCH_SIZE);
				}
			}
			if (listResponse != null) {
				nextOffset = listResponse.getNextOffset();
				if (StringUtils.hasText(nextOffset)) {
					form.setNextOffset(nextOffset);
				}
			}
			model.addAttribute(TENDER_LIST_RESPONSE_ATTRIBUTE, listResponse);
			return TENDER_LIST_VIEW;
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, TENDER_LIST_VIEW);
		}

	}

	@Layout(title = "Tender Details")
	@RequestMapping(method = RequestMethod.GET, path = TENDER_DETAILS_REQUEST_URI)
	public String getTenderDetails(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId, Model model) {

		DealerUtils.checkHasDealership(user);
		try {
			// access tender
			TenderBids tenderBidsClient = bnbneedsClient.tenderBids();
			try {
				tenderBidsClient.accessTender(tenderId);
			} catch (BNBNeedsServiceException e) {
				return getRedirectPath(getTenderPreviewsUrl() + "?failedToAccessTender=" + e.getMessage());
			}
			// set response
			setTenderInfoResponse(user, tenderId, model);
			return TENDER_DETAILS_VIEW;
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, TENDER_DETAILS_VIEW);
		}
	}

	private void setTenderInfoResponse(BNBNeedsUser user, String tenderId, Model model) {
		TenderBids tenderBidsClient = bnbneedsClient.tenderBids();
		TenderInfoResponse response = tenderBidsClient.getTenderDetails(tenderId);
		setTenderInfoResponse(response, model);
		TenderProductInfoListResponse bidProducts = tenderBidsClient.listTenderProducts(tenderId, 3);
		model.addAttribute(TENDER_BID_PRODUCT_LIST_RESPONSE_ATTRIBUTE, bidProducts);
		TenderBidInfoListResponse tenderBids = tenderBidsClient.listTenderBids(tenderId, 5);

		if (tenderBids != null && !tenderBids.isEmpty()) {
			for (Iterator<TenderBidInfoResponse> iterator = tenderBids.getIterator(); iterator.hasNext();) {
				TenderBidInfoResponse info = iterator.next();
				// mask all the details
				maskBidDetails(user, info);
				setBidTimestamp(info);
			}
		}
		model.addAttribute(TENDER_BID_LIST_RESPONSE_ATTRIBUTE, tenderBids);

		// get lowest bid
		TenderBidInfoResponse lowestBid = tenderBidsClient.getLowestBid(tenderId);
		model.addAttribute(LOWEST_BID_ATTRIBUTE, lowestBid);

		// get my bid, if any
		TenderBidInfoResponse myBid = tenderBidsClient.getCurrentBid(tenderId);
		model.addAttribute(CURRENT_BID_ATTRIBUTE, myBid);

	}

	private void maskBidDetails(BNBNeedsUser user, TenderBidInfoResponse info) {
		NamedRelatedResourceRep bidder = info.getVendor();
		String vendorId = user.getRelationshipId();
		String bidderId = bidder.getId();
		if (bidderId.equals(vendorId)) {
			info.getVendor().setName("You");
		} else {
			info.setComments("********");
			String vendorName = bidder.getName();
			int nameSize = vendorName.length();
			String maskedVendorName = vendorName.charAt(0) + "****" + vendorName.substring(nameSize - 1);
			bidder.setName(maskedVendorName);
		}
	}
}
