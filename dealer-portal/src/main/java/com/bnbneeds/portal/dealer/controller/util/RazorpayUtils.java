package com.bnbneeds.portal.dealer.controller.util;

import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.dealer.CreatePaymentTransactionParam;
import com.bnbneeds.portal.dealer.bean.RazorpayAuthBean;
import com.bnbneeds.portal.dealer.formbean.RazorpayCheckoutOrderBean;
import com.bnbneeds.portal.dealer.formbean.RazorpayPaymentBean;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.razorpay.Invoice;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.razorpay.Utils;

public class RazorpayUtils {

	private static final Logger logger = LoggerFactory.getLogger(RazorpayUtils.class);

	private static final String RAZORPAY_PAYMENT_MODEL_ATTRIBUTE = "razorpayPayment";
	private static final String RAZORPAY_PAYMENT_FORM_VIEW = "razorpay-payment-form";
	private static final String PAYMENT_CAPTURED_STATUS = "captured";
	private static final String PAYMENT_AUTHORIZED_STATUS = "authorized";
	private static final String PAYMENT_FAILED_STATUS = "failed";

	private static void map(UserAccountInfoResponse from, RazorpayPaymentBean to) {
		to.setDealerName(from.getAccountName());
		to.setDealerEmailAddress(from.getUsername());
		to.setDealerMobileNumber(from.getMobileNumber());
	}

	public static void mapRazorpayPaymentCheckout(Payment from, CreatePaymentTransactionParam to) {
		int intValue = from.get("amount");
		Integer amount = new Integer(intValue);
		to.setAmount(amount.doubleValue() / 100);
		to.setPaymentGateway("Razorpay");
		to.setPaymentMethod(from.get("method"));
		to.setTransactionDescription(from.get("description"));
		to.setTransactionId(from.get("id"));
		Date createdAt = from.get("created_at");
		to.setTimestamp(createdAt.getTime());

		String transactionStatus = getPaymentStatus(from);
		switch (transactionStatus) {
		case PAYMENT_CAPTURED_STATUS:
			to.setTransactionStatus("SUCCESS");
			break;
		default:
			to.setTransactionStatus(transactionStatus.toUpperCase());
			break;
		}
	}

	public static String getPaymentStatus(Payment from) {
		return from.get("status");
	}

	public static boolean isPaymentCaptured(Payment from) {
		return PAYMENT_CAPTURED_STATUS.equals(getPaymentStatus(from));
	}

	public static boolean isPaymentAuthorized(Payment from) {
		return PAYMENT_AUTHORIZED_STATUS.equals(getPaymentStatus(from));
	}

	public static boolean isPaymentFailed(Payment from) {
		return PAYMENT_FAILED_STATUS.equals(getPaymentStatus(from));
	}

	public static RazorpayPaymentBean getRazorpayPaymentFormBean(String keyId, BNBNeedsUser user, int amount,
			String description, String entityId, String callBackActionURL) {
		RazorpayPaymentBean to = new RazorpayPaymentBean();
		map(user.getUserAccountInfo(), to);
		to.setKeyId(keyId);
		to.setAmount(amount);
		to.setDescription(description);
		to.setCallbackActionURL(callBackActionURL);
		to.setEntityId(entityId);
		return to;
	}

	public static String getRazorpayPaymentFormView(RazorpayPaymentBean from, Model to) {
		to.addAttribute(RAZORPAY_PAYMENT_MODEL_ATTRIBUTE, from);
		return RAZORPAY_PAYMENT_FORM_VIEW;
	}

	public static int getAmountInPaise(int amountInINR) {
		return (amountInINR * 100);
	}

	public static void setErrorMessage(RazorpayException e, Model model) {
		RequestResponseUtils.setErrorMessage("Payment Gateway: " + e.getMessage(), model);
	}

	public static boolean isPaymentSignatureValid(RazorpayCheckoutOrderBean form, RazorpayAuthBean auth)
			throws RazorpayException {
		return Utils.verifyPaymentSignature(form.getJSONObject(), auth.getKeySecret());
	}

	public static void refundCapturedPayment(RazorpayClient razorpayClient, Payment payment) throws RazorpayException {
		String paymentId = payment.get("id");
		if (isPaymentCaptured(payment)) {
			logger.info("Initiating refund for payment with ID: {}", payment);
			razorpayClient.Payments.refund(paymentId);
		} else {
			logger.error("Refund cannot be processed because payment is not captured: {}", paymentId);
			throw new RazorpayException("Cannot process refund. Payment is not captured: " + paymentId);
		}
	}

	public static Payment capturePayment(RazorpayClient razorpayClient, String paymentId, int amountInINR)
			throws RazorpayException {
		JSONObject jsonRequest = new JSONObject();
		jsonRequest.put("amount", getAmountInPaise(amountInINR));
		return razorpayClient.Payments.capture(paymentId, jsonRequest);
	}

	public static Invoice getInvoiceByPaymentId(RazorpayClient razorpayClient, String paymentId)
			throws RazorpayException {
		Invoice invoice = null;
		JSONObject invoiceRequest = new JSONObject();
		invoiceRequest.put("payment_id", paymentId);
		List<Invoice> invoices = razorpayClient.Invoices.fetchAll(invoiceRequest);
		if (!CollectionUtils.isEmpty(invoices)) {
			invoice = invoices.get(0);
		}
		return invoice;
	}

	public static Invoice getInvoiceInPayment(RazorpayClient razorpayClient, Payment payment) throws RazorpayException {
		Invoice invoice = null;
		String invoiceId = payment.get("invoice_id");
		invoice = razorpayClient.Invoices.fetch(invoiceId);
		return invoice;
	}
}
