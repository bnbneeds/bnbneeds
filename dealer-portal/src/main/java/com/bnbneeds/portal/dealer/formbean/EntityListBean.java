package com.bnbneeds.portal.dealer.formbean;

import java.util.List;

public class EntityListBean {

	private List<EntityBean> entities;

	public List<EntityBean> getEntities() {
		return entities;
	}

	public void setEntities(List<EntityBean> entities) {
		this.entities = entities;
	}

}
