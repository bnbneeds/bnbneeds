package com.bnbneeds.portal.dealer.formbean;

import org.hibernate.validator.constraints.NotBlank;

public class PurchaseItemUpdateBean {

	@NotBlank
	private String status;
	@NotBlank
	private String comment;

	public String getStatus() {
		return status;
	}

	public String getComment() {
		return comment;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}