package com.bnbneeds.portal.dealer.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.report.CreateReportParam;
import com.bnbneeds.portal.dealer.controller.util.DealerUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;

@Controller
@RequestMapping(path = "/abuse-reports")
@PreAuthorize("hasAnyAuthority('VENDOR', 'BUYER')")
public class AbuseReportController extends AbstractBaseController {

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public synchronized void createAbuseReport(
			@AuthenticationPrincipal BNBNeedsUser user, String entityId,
			String title, String description, Model model,
			HttpServletResponse response) throws IOException {
		try {
			DealerUtils.checkHasDealership(user);
			CreateReportParam param = new CreateReportParam();
			param.setEntityId(entityId);
			param.setTitle(title);
			param.setDescription(description);
			bnbneedsClient.abuseReports().create(param);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, "OK", response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}
}
