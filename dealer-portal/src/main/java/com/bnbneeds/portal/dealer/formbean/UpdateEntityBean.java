package com.bnbneeds.portal.dealer.formbean;

public class UpdateEntityBean extends EntityBean {

	private String entityId;

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

}
