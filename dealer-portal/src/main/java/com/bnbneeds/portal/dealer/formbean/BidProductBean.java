package com.bnbneeds.portal.dealer.formbean;

public class BidProductBean {
	private String tenderId;
	private String productNameId;
	private Double requiredQuantity;
	private String unit;
	private String qualitySpecification;
	private String specialRequirements;

	public String getProductNameId() {
		return productNameId;
	}

	public String getUnit() {
		return unit;
	}

	public String getSpecialRequirements() {
		return specialRequirements;
	}

	public void setProductNameId(String productNameId) {
		this.productNameId = productNameId;
	}

	public Double getRequiredQuantity() {
		return requiredQuantity;
	}

	public void setRequiredQuantity(Double requiredQuantity) {
		this.requiredQuantity = requiredQuantity;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setSpecialRequirements(String specialRequirements) {
		this.specialRequirements = specialRequirements;
	}

	public String getQualitySpecification() {
		return qualitySpecification;
	}

	public void setQualitySpecification(String qualitySpecification) {
		this.qualitySpecification = qualitySpecification;
	}

	public String getTenderId() {
		return tenderId;
	}

	public void setTenderId(String tenderId) {
		this.tenderId = tenderId;
	}

}
