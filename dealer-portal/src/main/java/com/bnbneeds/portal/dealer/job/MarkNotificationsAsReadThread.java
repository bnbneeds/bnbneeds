package com.bnbneeds.portal.dealer.job;

import java.util.Iterator;
import java.util.List;

import org.springframework.ui.Model;

import com.bnbneeds.portal.dealer.model.NotificationEvent;
import com.bnbneeds.portal.dealer.util.StringUtils;

public class MarkNotificationsAsReadThread implements Runnable {

	private List<NotificationEvent> notificationList;
	private Model model;
	private String eventId;
	private String notificationListAttributeName;
	private String newNotificationCountAttributeName;

	public MarkNotificationsAsReadThread(
			List<NotificationEvent> notificationList, String eventId,
			Model model, String notificationListAttributeName,
			String newNotificationCountAttributeName) {
		super();
		this.notificationList = notificationList;
		this.model = model;
		this.eventId = eventId;
		this.notificationListAttributeName = notificationListAttributeName;
		this.newNotificationCountAttributeName = newNotificationCountAttributeName;
	}

	@Override
	public synchronized void run() {
		int unreadNotificationCount = notificationList.size();
		for (Iterator<NotificationEvent> iterator = notificationList.iterator(); iterator
				.hasNext();) {
			NotificationEvent notificationEvent = iterator.next();
			if (StringUtils.hasText(eventId)) {
				if (eventId.equals(notificationEvent.getEventId())) {
					if (notificationEvent.isUnread()) {
						notificationEvent.markAsRead();
						unreadNotificationCount--;
					}
				}
			} else {
				if (notificationEvent.isUnread()) {
					notificationEvent.markAsRead();
					unreadNotificationCount--;
				}
			}
		}

		if (StringUtils.hasText(eventId)
				&& unreadNotificationCount != notificationList.size()) {
			model.addAttribute(newNotificationCountAttributeName,
					unreadNotificationCount);
		} else {
			model.addAttribute(newNotificationCountAttributeName, 0);
		}
		model.addAttribute(notificationListAttributeName, notificationList);

	}
}
