package com.bnbneeds.portal.dealer.formbean;

public class EnquiryListRequestBean {

	private String enquiryDate;
	private String enquiryDateRange;

	public String getEnquiryDate() {
		return enquiryDate;
	}

	public String getEnquiryDateRange() {
		return enquiryDateRange;
	}

	public void setEnquiryDate(String enquiryDate) {
		this.enquiryDate = enquiryDate;
	}

	public void setEnquiryDateRange(String enquiryDateRange) {
		this.enquiryDateRange = enquiryDateRange;
	}

}
