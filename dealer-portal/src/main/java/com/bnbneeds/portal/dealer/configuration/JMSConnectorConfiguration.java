package com.bnbneeds.portal.dealer.configuration;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

import com.bnbneeds.portal.common.jms.JMSConstants;

@Configuration
@EnableJms
public class JMSConnectorConfiguration {

	@Value("${jms.provider.url}")
	private String jmsProviderURL;
	@Value("${spring.application.name}")
	private String clientId;
	@Value("${jms.provider.username}")
	private String userName;
	@Value("${jms.provider.password}")
	private String password;

	@Bean
	public ActiveMQConnectionFactory activeMQConnectionFactory() {
		ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
		activeMQConnectionFactory.setBrokerURL(jmsProviderURL);
		List<String> trustedPackages = new ArrayList<>();
		trustedPackages.add(JMSConstants.JMS_MESSAGE_BEANS_PACKAGE);
		activeMQConnectionFactory.setTrustedPackages(trustedPackages);
		activeMQConnectionFactory.setClientID(clientId);
		activeMQConnectionFactory.setUserName(userName);
		activeMQConnectionFactory.setPassword(password);
		return activeMQConnectionFactory;
	}

	@Bean(name = JMSConstants.CONTAINER_FACTORY_BEAN_NAME)
	public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		factory.setPubSubDomain(true);
		factory.setConnectionFactory(activeMQConnectionFactory());
		factory.setPubSubDomain(true);

		return factory;
	}

	@Bean(name = JMSConstants.JMS_TEMPLATE_BEAN_NAME)
	public JmsTemplate jmsTemplate() throws JMSException {
		return new JmsTemplate(activeMQConnectionFactory());
	}

}