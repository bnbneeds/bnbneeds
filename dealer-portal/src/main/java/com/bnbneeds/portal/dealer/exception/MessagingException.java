package com.bnbneeds.portal.dealer.exception;

public class MessagingException extends RuntimeException {

	private static final long serialVersionUID = -1626598996018986839L;

	public MessagingException() {
		super();
	}

	public MessagingException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public MessagingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public MessagingException(String arg0) {
		super(arg0);
	}

	public MessagingException(Throwable arg0) {
		super(arg0);
	}

}
