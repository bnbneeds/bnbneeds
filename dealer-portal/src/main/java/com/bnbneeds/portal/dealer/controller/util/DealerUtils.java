package com.bnbneeds.portal.dealer.controller.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.portal.dealer.exception.HasNoDealershipException;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.StringUtils;

public class DealerUtils {
	private static final Logger logger = LoggerFactory
			.getLogger(DealerUtils.class);

	public static void checkHasDealership(BNBNeedsUser user) {
		String dealershipId = user.getRelationshipId();

		if (user.isDealer()) {
			if (!StringUtils.hasText(dealershipId)) {
				logger.error("User has no dealership.");
				throw new HasNoDealershipException(
						"You are not registered as a dealer. Please register your organization and try again.");
			}
		}
	}

	public static String checkAndReturnRelationshipId(BNBNeedsUser user) {
		checkHasDealership(user);
		return user.getRelationshipId();
	}
}
