package com.bnbneeds.portal.dealer.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bnbneeds.rest.client.core.PaymentTransactions.PaymentTransactionOf;

@Controller
@RequestMapping(path = BuyerPaymentTransactionController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('BUYER')")
public class BuyerPaymentTransactionController extends PaymentTransactionController {

	protected static final String BASE_REQUEST_PATH = "/buyers/payment-transactions";

	@Override
	public PaymentTransactionOf getPaymentTransactionOf() {
		return PaymentTransactionOf.buyer;
	}

}
