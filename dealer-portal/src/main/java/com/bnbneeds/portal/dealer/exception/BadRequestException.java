package com.bnbneeds.portal.dealer.exception;

public class BadRequestException extends APIException {

	private static final long serialVersionUID = 2417486850678036862L;

	private static final String PARAMETER_INVALID1 = "Parameter provided is invalid: %s";

	public BadRequestException() {
		super();
	}

	public BadRequestException(String message) {
		super(message);
	}

	public static BadRequestException create(String message) {
		return new BadRequestException(message);
	}

	public static BadRequestException parameterInvalid(String fieldName) {
		return new BadRequestException(
				getMessage(PARAMETER_INVALID1, fieldName));
	}

}
