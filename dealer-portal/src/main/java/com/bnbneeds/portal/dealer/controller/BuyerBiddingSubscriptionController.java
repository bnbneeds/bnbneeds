package com.bnbneeds.portal.dealer.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bnbneeds.rest.client.core.BiddingSubscriptions.BiddingSubscriptionFor;

@Controller
@RequestMapping(path = BuyerBiddingSubscriptionController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('BUYER')")
public class BuyerBiddingSubscriptionController extends BiddingSubscriptionController {

	protected static final String BASE_REQUEST_PATH = "/buyers/bidding-subscriptions";

	@Override
	protected String getBaseRequestURI() {
		return BASE_REQUEST_PATH;
	}

	@Override
	protected BiddingSubscriptionFor getBiddingSubscriptionFor() {
		return BiddingSubscriptionFor.buyer;
	}

	public static String getBiddingSubscriptionDetailsUrl(String subscriptionId) {
		return BASE_REQUEST_PATH + String.format("/%s/details", subscriptionId);
	}

}
