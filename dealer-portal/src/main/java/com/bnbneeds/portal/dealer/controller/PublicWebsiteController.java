package com.bnbneeds.portal.dealer.controller;

import java.io.IOException;
import java.net.ConnectException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.EntityCountResponse;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.util.ConnectionUtils;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.ClientConfig;

@Controller
@RequestMapping(path = PublicWebsiteController.BASE_REQUEST_PATH)
public class PublicWebsiteController extends AbstractBaseController {

	private static final Logger logger = LoggerFactory.getLogger(PublicWebsiteController.class);

	private static final String WHAT_IS_VIEW = "what-is";
	private static final String WHAT_IS_VIDEO_URL_ATTRIBUTE = "whatIsVideoURL";
	private static final String BUYER_VIDEO_URL_ATTRIBUTE = "buyerVideoURL";
	private static final String VENDOR_VIDEO_URL_ATTRIBUTE = "vendorVideoURL";
	private static final String CLASSPATH_LOADING_ANIMATION_GIF_FILE = "classpath:static/external/images/loading-circle2.gif";

	private static final String WHAT_IS_VIDEO_URL = StringUtils.getYoutubeEmbedURLWithAutoPlay("mEIw6l-LuF8");
	private static final String BUYER_VIDEO_URL = StringUtils.getYoutubeEmbedURL("nWG8WRrUmx0");
	private static final String VENDOR_VIDEO_URL = StringUtils.getYoutubeEmbedURL("UlKUlVEj3oo");
	private static final String PING_BNBNEEDS_SERVICE_URL = "/ping-bnbneeds";

	protected static final String BASE_REQUEST_PATH = "/public";

	public static String getPingBNBneedsURL() {
		return String.format("%1$s%2$s", BASE_REQUEST_PATH, PING_BNBNEEDS_SERVICE_URL);
	}

	@RequestMapping(method = RequestMethod.GET, path = { PING_BNBNEEDS_SERVICE_URL })
	public String pingBNBNeedsService() throws ConnectException {

		ClientConfig config = bnbneedsClient.getConfig();

		ConnectionUtils.checkServerConnection(config.getHost(), config.getPort());

		return getRedirectPath(HomePageController.HOMEPAGE_URL);
	}

	@Layout(title = "About Us")
	@RequestMapping(method = RequestMethod.GET, path = "/about")
	public String getAboutView() throws ConnectException {
		return "about";
	}

	@Layout(title = "Contact")
	@RequestMapping(method = RequestMethod.GET, path = "/contact")
	public String getContactView() throws ConnectException {
		return "contact";
	}

	@Layout(title = "Terms Of Service")
	@RequestMapping(method = RequestMethod.GET, path = "/terms-of-service")
	public String getTermsOfServiceView() throws ConnectException {
		return "terms-of-service";
	}

	@RequestMapping(method = RequestMethod.GET, path = "/loading-gif")
	public @ResponseBody byte[] getLoadingGIFImage() {
		return getFileInBytes(CLASSPATH_LOADING_ANIMATION_GIF_FILE);
	}

	@Layout(title = "What is BNBneeds")
	@RequestMapping(method = RequestMethod.GET, path = "/what-is")
	public String getWhatIsView(Model model) {

		model.addAttribute(WHAT_IS_VIDEO_URL_ATTRIBUTE, WHAT_IS_VIDEO_URL);
		model.addAttribute(BUYER_VIDEO_URL_ATTRIBUTE, BUYER_VIDEO_URL);
		model.addAttribute(VENDOR_VIDEO_URL_ATTRIBUTE, VENDOR_VIDEO_URL);

		return WHAT_IS_VIEW;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/entity-count")
	public synchronized void writeEntityCount(@RequestParam("entityType") String entityType,
			HttpServletResponse httpResponse) throws IOException {
		try {
			EntityCountResponse response = bnbneedsClient.utilities().getEntityCount(entityType);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE,
					String.valueOf(response.getCount()), httpResponse);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, httpResponse);
		}
	}

}
