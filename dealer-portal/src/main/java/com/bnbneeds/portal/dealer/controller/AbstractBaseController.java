package com.bnbneeds.portal.dealer.controller;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;

import com.bnbneeds.portal.dealer.job.Job;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.portal.reports.PDFGenerator;
import com.bnbneeds.rest.client.BNBNeedsClient;

public abstract class AbstractBaseController {

	private static final Logger logger = LoggerFactory.getLogger(AbstractBaseController.class);

	protected static final String REQ_PARAM_NEXT_OFFSET = "nextOffset";
	protected static final String REQ_PARAM_STATUS = "status";
	protected static final int RESULT_FETCH_SIZE = 100;

	protected static final String MODEL_ATTRIBUTE_IMAGE_URI = "image_uri";
	protected static final String MODEL_ATTRIBUTE_FORM_TITLE = "formTitle";
	protected static final String MODEL_ATTRIBUTE_FORM_ACTION = "formAction";

	protected static final String UPLOAD_FILE_REQUEST_PARAM_NAME = "imageFile";
	protected static final String CLASSPATH_NO_IMAGE_AVAILABLE_FILE = "classpath:static/external/images/image-not-available.jpg";
	protected static final String CLASSPATH_BROKEN_IMAGE_FILE = "classpath:static/external/images/broken-image.jpg";
	protected static final long SLEEP_TIME_IN_MILLIS = 200;

	@Autowired
	protected BNBNeedsClient bnbneedsClient;

	@Autowired
	protected HttpServletRequest request;

	@Autowired
	protected ResourceLoader resourceLoader;

	@Autowired
	protected Job job;

	@Autowired
	protected PDFGenerator pdfGenerator;

	@Value("${reports.directory}")
	protected String reportsDirectoryPath;

	@Value("${apisvc.url}")
	protected String apiSvcBaseURL;

	@Value("${apisvc.devserver}")
	protected boolean devserver;

	protected byte[] getFileInBytes(String imagePath) {
		InputStream in = null;
		byte[] image = null;

		try {
			in = resourceLoader.getResource(imagePath).getInputStream();
			image = FileCopyUtils.copyToByteArray(in);
		} catch (IOException e) {
			logger.error("IOException occured", e);
		}
		return image;
	}

	protected byte[] getImageNotAvailable() {
		return getFileInBytes(CLASSPATH_NO_IMAGE_AVAILABLE_FILE);
	}

	protected byte[] getBrokenImage() {
		return getFileInBytes(CLASSPATH_BROKEN_IMAGE_FILE);
	}

	protected byte[] getImageNotAvailableIfNull(byte[] photo) {
		return (photo == null || photo.length == 0) ? getImageNotAvailable() : photo;
	}

	protected String getRedirectPath(String endpointURI) {
		StringBuffer buf = new StringBuffer("redirect:");
		buf.append(request.getContextPath());
		if (!endpointURI.startsWith("/")) {
			buf.append("/");
		}
		buf.append(endpointURI);
		return buf.toString();
	}

	protected String getForwardPath(String endpointURI) {
		StringBuffer buf = new StringBuffer("forward:");
		buf.append(request.getContextPath());
		if (!endpointURI.startsWith("/")) {
			buf.append("/");
		}
		buf.append(endpointURI);
		return buf.toString();
	}

	protected boolean hasDealership(BNBNeedsUser user) {
		if (user == null) {
			return false;
		}
		return StringUtils.hasText(user.getRelationshipId());
	}

	protected void execute(Runnable runner) {
		job.execute(runner);
	}

	/**
	 * Call this method to make the execution sleep for 200ms
	 * 
	 * @throws InterruptedException
	 */
	protected static void sleep() throws InterruptedException {
		sleep(SLEEP_TIME_IN_MILLIS);
	}

	protected static void sleep(long ms) throws InterruptedException {
		Thread.sleep(ms);
	}

}
