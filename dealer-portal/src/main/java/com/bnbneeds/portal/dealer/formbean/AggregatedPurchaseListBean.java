package com.bnbneeds.portal.dealer.formbean;

public class AggregatedPurchaseListBean {

	private String requestDate;
	private String requestDateRange;
	private String productNameId;
	private boolean pdf;

	public String getProductNameId() {
		return productNameId;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public String getRequestDateRange() {
		return requestDateRange;
	}

	public void setRequestDateRange(String requestDateRange) {
		this.requestDateRange = requestDateRange;
	}

	public void setProductNameId(String productNameId) {
		this.productNameId = productNameId;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public boolean isPdf() {
		return pdf;
	}

	public void setPdf(boolean pdf) {
		this.pdf = pdf;
	}

}
