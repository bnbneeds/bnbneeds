package com.bnbneeds.portal.dealer.formbean;

public class VendorPurchaseItemUpdateBean extends PurchaseItemUpdateBean {

	private String deliveredQuantity;

	public String getDeliveredQuantity() {
		return deliveredQuantity;
	}

	public void setDeliveredQuantity(String deliveredQuantity) {
		this.deliveredQuantity = deliveredQuantity;
	}

}
