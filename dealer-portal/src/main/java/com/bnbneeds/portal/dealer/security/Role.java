package com.bnbneeds.portal.dealer.security;

public interface Role {
	String ADMIN = "ADMIN";
	String VENDOR = "VENDOR";
	String BUYER = "BUYER";
}
