package com.bnbneeds.portal.dealer.formbean;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.bnbneeds.portal.dealer.util.StringUtils;

public class DealerRegistrationBean {

	@NotBlank
	private String organizationName;
	private String description;
	@NotBlank
	private String address;
	@NotBlank
	private String contactName;
	@NotBlank
	private String mobileNumber;
	@NotBlank
	@Email(message = "E-mail address is invalid.")
	private String emailAddress;
	private String website;
	@NotBlank
	@Pattern(regexp = StringUtils.REGEX_PAN, message = "PAN is invalid.")
	private String pan;

	public String getOrganizationName() {
		return organizationName;
	}

	public String getDescription() {
		return description;
	}

	public String getAddress() {
		return address;
	}

	public String getContactName() {
		return contactName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getWebsite() {
		return website;
	}

	public String getPan() {
		return pan;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

}
