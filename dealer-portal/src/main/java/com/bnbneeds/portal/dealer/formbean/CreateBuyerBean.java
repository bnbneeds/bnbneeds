package com.bnbneeds.portal.dealer.formbean;

import org.hibernate.validator.constraints.NotBlank;

public class CreateBuyerBean extends DealerRegistrationBean {

	@NotBlank
	private String businessTypeId;

	public String getBusinessTypeId() {
		return businessTypeId;
	}

	public void setBusinessTypeId(String businessTypeId) {
		this.businessTypeId = businessTypeId;
	}

}
