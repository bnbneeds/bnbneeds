package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.writeResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.bnbneeds.app.model.notification.NotificationInfoResponse;
import com.bnbneeds.app.model.notification.NotificationListResponse;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.DealerUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.model.NotificationEvent;
import com.bnbneeds.portal.dealer.model.NotificationEventListResponse;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.google.gson.Gson;

@Controller
@RequestMapping(path = "/notifications")
@PreAuthorize("hasAnyAuthority('VENDOR', 'BUYER')")
@SessionAttributes(names = {
		NotificationController.NOTIFICATION_LIST_ATTRIBUTE,
		NotificationController.NEW_NOTIFICATIONS_ATTRIBUTE })
public class NotificationController extends DealerController {

	private static final String CLASSPATH_AUDIO_BELL_MP3_FILE = "classpath:static/external/sounds/DingSmallBell.mp3";

	protected static final String NEW_NOTIFICATIONS_ATTRIBUTE = "newNotifications";
	protected static final String NOTIFICATION_LIST_ATTRIBUTE = "notificationList";

	protected static final String NOTIFICATION_EVENT_LIST_RESONSE_ATTRIBUTE = "eventListResponse";
	private static final String NOTIFICATION_LIST_VIEW = "notification-list";

	private static final int NOTIFICATION_LIST_RESULT_SIZE = 10;
	private static final int NOTIFICATION_LIST_RESULT_SIZE_FOR_VIEW = 20;

	private static final Comparator<NotificationEvent> NOTIFICATION_LIST_COMPARATOR;

	static {
		NOTIFICATION_LIST_COMPARATOR = new Comparator<NotificationEvent>() {

			@Override
			public int compare(NotificationEvent event1,
					NotificationEvent event2) {
				long time1 = event1.getTime();
				long time2 = event2.getTime();
				if (time1 == time2) {
					return 0;
				} else if (time1 < time2) {
					// Descending order
					return 1;
				} else {
					return -1;
				}
			}
		};
	}

	@RequestMapping(method = RequestMethod.GET, path = "/bell")
	public @ResponseBody byte[] writeMP3Bell(HttpServletResponse response) {
		response.setContentType("audio/mpeg");
		return getFileInBytes(CLASSPATH_AUDIO_BELL_MP3_FILE);
	}

	@Layout(title = "Notifications")
	@RequestMapping(method = RequestMethod.GET, path = "/list")
	public String getNotificationList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			Model model) {

		DealerUtils.checkHasDealership(user);
		NotificationEventListResponse eventListResponse = null;

		try {
			NotificationListResponse response = null;
			if (StringUtils.hasText(nextOffset)) {
				response = bnbneedsClient.notifications().list(
						NOTIFICATION_LIST_RESULT_SIZE_FOR_VIEW, nextOffset);
			} else {
				response = bnbneedsClient.notifications().list(
						NOTIFICATION_LIST_RESULT_SIZE_FOR_VIEW);
			}
			eventListResponse = NotificationEventListResponse.map(user,
					response);

			model.addAttribute(NOTIFICATION_EVENT_LIST_RESONSE_ATTRIBUTE,
					eventListResponse);
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model,
					NOTIFICATION_LIST_VIEW);
		}

		return NOTIFICATION_LIST_VIEW;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public synchronized void writeNotifications(
			@AuthenticationPrincipal BNBNeedsUser user, ModelMap modelMap,
			HttpServletResponse httpResponse) throws IOException {

		List<NotificationEvent> notificationList = null;

		String contentType = MediaType.TEXT_PLAIN_VALUE;

		try {
			notificationList = getNotificationEventsFromSession(modelMap);
			modelMap.addAttribute(NOTIFICATION_LIST_ATTRIBUTE, notificationList);
			Gson gson = new Gson();
			String jsonResponse = gson.toJson(notificationList);

			writeResponse(HttpServletResponse.SC_OK, contentType, jsonResponse,
					httpResponse);

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, httpResponse);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/mark-as-read")
	@ResponseBody
	public synchronized void markNotificationsAsRead(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "eventId", required = false) String eventId,
			ModelMap modelMap, HttpServletResponse httpResponse)
			throws IOException {

		List<NotificationEvent> notificationList = getNotificationEventsFromSession(modelMap);
		List<String> readNotificationList = new ArrayList<String>();

		int unreadNotificationCount = notificationList.size();
		for (Iterator<NotificationEvent> iterator = notificationList.iterator(); iterator
				.hasNext();) {
			NotificationEvent notificationEvent = iterator.next();
			if (StringUtils.hasText(eventId)) {
				if (eventId.equals(notificationEvent.getEventId())) {
					if (notificationEvent.isUnread()) {
						notificationEvent.markAsRead();
						readNotificationList.add(eventId);
						unreadNotificationCount--;
					}
				}
			} else {
				if (notificationEvent.isUnread()) {
					notificationEvent.markAsRead();
					readNotificationList.add(notificationEvent.getEventId());
					unreadNotificationCount--;
				}
			}
		}
		try {
			if (readNotificationList != null && !readNotificationList.isEmpty()) {
				bnbneedsClient.notifications().markAsRead(readNotificationList);
			}

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, httpResponse);
			return;
		}

		if (StringUtils.hasText(eventId)
				&& unreadNotificationCount != notificationList.size()) {
			modelMap.addAttribute(NEW_NOTIFICATIONS_ATTRIBUTE,
					unreadNotificationCount);
		} else {
			modelMap.addAttribute(NEW_NOTIFICATIONS_ATTRIBUTE, 0);
		}
		writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE,
				"OK", httpResponse);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/anything-new")
	@ResponseBody
	public synchronized void writeNewNotifications(
			@AuthenticationPrincipal BNBNeedsUser user, ModelMap modelMap,
			HttpServletResponse httpResponse) throws IOException {

		String contentType = MediaType.TEXT_PLAIN_VALUE;

		try {

			setNotificationEventsInSession(user, modelMap);

			int newNotificationCount = 0;

			Object value = modelMap.get(NEW_NOTIFICATIONS_ATTRIBUTE);
			if (value != null) {
				newNotificationCount = (int) value;
			}

			NewNotification newNotificationCountResponse = new NewNotification(
					newNotificationCount);
			Gson gson = new Gson();
			String jsonResponse = gson.toJson(newNotificationCountResponse);

			writeResponse(HttpServletResponse.SC_OK, contentType, jsonResponse,
					httpResponse);

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, httpResponse);
		}
	}

	private void setNotificationEventsInSession(BNBNeedsUser user,
			ModelMap modelMap) {

		DealerUtils.checkHasDealership(user);

		List<NotificationEvent> eventList = getNotificationEventsFromSession(modelMap);

		NotificationListResponse listResponse = bnbneedsClient.notifications()
				.list(NOTIFICATION_LIST_RESULT_SIZE);
		Iterator<NotificationInfoResponse> iterator = listResponse.iterator();

		int unreadCount = 0;

		if (eventList.isEmpty()) {
			if (iterator != null) {
				while (iterator.hasNext()) {
					NotificationInfoResponse notification = iterator.next();
					NotificationEvent event = new NotificationEvent(user,
							notification);
					eventList.add(event);
					if (event.isUnread()) {
						unreadCount++;
					}
				}
			}

			if (!eventList.isEmpty()) {
				modelMap.addAttribute(NEW_NOTIFICATIONS_ATTRIBUTE, unreadCount);
			}

		} else {
			unreadCount = 0;
			int index = -1;
			if (listResponse != null
					&& listResponse.getNotificationList() != null) {
				for (Iterator<NotificationInfoResponse> iterator1 = listResponse
						.getNotificationList().iterator(); iterator1.hasNext();) {

					NotificationInfoResponse notification = iterator1.next();
					NotificationEvent event = new NotificationEvent(user,
							notification);

					if (!eventList.contains(event)) {
						eventList.add(event);
						unreadCount++;
					} else {
						// Add the unread notification count
						index = eventList.indexOf(event);
						if (index > -1) {
							event = eventList.get(index);
							if (event.isUnread()) {
								unreadCount++;
							}
						}
					}

				}
				if (unreadCount > 0) {
					modelMap.addAttribute(NEW_NOTIFICATIONS_ATTRIBUTE,
							unreadCount);
				}
			}
		}
		Collections.sort(eventList, NOTIFICATION_LIST_COMPARATOR);
		modelMap.addAttribute(NOTIFICATION_LIST_ATTRIBUTE, eventList);
	}

	@SuppressWarnings("unchecked")
	private List<NotificationEvent> getNotificationEventsFromSession(
			ModelMap modelMap) {
		List<NotificationEvent> notificationList = null;

		Object notificationListObj = modelMap.get(NOTIFICATION_LIST_ATTRIBUTE);
		if (notificationListObj != null) {
			notificationList = (List<NotificationEvent>) notificationListObj;
		} else {
			notificationList = new ArrayList<NotificationEvent>();
		}
		return notificationList;
	}

	private class NewNotification {
		@SuppressWarnings("unused")
		private int newNotifications;

		@SuppressWarnings("unused")
		public NewNotification() {
			super();
		}

		public NewNotification(int newNotifications) {
			super();
			this.newNotifications = newNotifications;
		}

	}
}
