package com.bnbneeds.portal.dealer.configuration;

import java.io.File;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bnbneeds.portal.dealer.bean.ApplicationBeans;
import com.bnbneeds.portal.dealer.bean.RazorpayAuthBean;
import com.bnbneeds.portal.reports.PDFGenerator;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

@Configuration
public class ApplicationConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationConfiguration.class);

	@Value("${reports.directory}")
	protected String reportsDirectoryPath;

	@Value("${razorpay.key.id}")
	private String razorPayKeyId;
	@Value("${razorpay.key.secret}")
	private String razorPayKeySecret;

	@Bean
	public RazorpayAuthBean razorpaybean() {
		return new RazorpayAuthBean(razorPayKeyId, razorPayKeySecret);
	}

	@Bean
	@Autowired
	public RazorpayClient razorpayClient(RazorpayAuthBean authBean) throws RazorpayException {
		return new RazorpayClient(authBean.getKeyId(), authBean.getKeySecret());
	}

	@Bean
	@Autowired
	public PDFGenerator getPdfGenerator(BNBNeedsClient client) {
		return new PDFGenerator(client);
	}

	@Bean
	public ApplicationBeans appilcationBeans() {
		return new ApplicationBeans();
	}

	@PostConstruct
	public void createReportsDirectoryIfNotPresent() {

		File reportsDirectory = new File(reportsDirectoryPath);
		if (reportsDirectory.exists()) {
			logger.info("Reports directory already exists: {}", reportsDirectory);
		} else {
			logger.info("Reports directory does not exist. So create it: {}", reportsDirectory);
			reportsDirectory.mkdirs();
		}

	}

}
