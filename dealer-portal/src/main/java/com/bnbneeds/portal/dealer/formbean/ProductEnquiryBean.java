package com.bnbneeds.portal.dealer.formbean;

public class ProductEnquiryBean {

	private String requiredQuantity;
	private int likelyToBuyIn;
	private String timeUnit;
	private String description;

	public String getRequiredQuantity() {
		return requiredQuantity;
	}

	public int getLikelyToBuyIn() {
		return likelyToBuyIn;
	}

	public String getTimeUnit() {
		return timeUnit;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setRequiredQuantity(String requiredQuantity) {
		this.requiredQuantity = requiredQuantity;
	}

	public void setLikelyToBuyIn(int likelyToBuyIn) {
		this.likelyToBuyIn = likelyToBuyIn;
	}

	public void setTimeUnit(String timeUnit) {
		this.timeUnit = timeUnit;
	}

}
