package com.bnbneeds.portal.dealer.formbean;

public class VendorEnquiryListRequestBean extends EnquiryListRequestBean {

	private String buyer;

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

}
