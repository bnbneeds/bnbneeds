package com.bnbneeds.portal.dealer.controller.util;

import java.util.concurrent.TimeUnit;

import com.bnbneeds.app.model.PipelineJobInfoResponse;
import com.bnbneeds.rest.client.BNBNeedsClient;

public class PipelineJobUtils {

	public static void pollTillJobCompletes(BNBNeedsClient bnbNeedsClient, String jobId, long sleepTimeInSeconds)
			throws InterruptedException {
		boolean breakLoop = false;
		while (true) {
			PipelineJobInfoResponse jobInfo = bnbNeedsClient.pipelineJobs().getJobInfo(jobId);
			switch (jobInfo.getStatus()) {
			case "RUNNING":
				breakLoop = false;
				TimeUnit.SECONDS.sleep(sleepTimeInSeconds);
				continue;
			case "WAITING_TO_RETRY":
			case "COMPLETED_SUCCESSFULLY":
			case "STOPPED_BY_REQUEST":
			case "CANCELED_BY_REQUEST":
			case "STOPPED_BY_ERROR":
				breakLoop = true;
				break;
			default:
				breakLoop = true;
				break;
			}
			if (breakLoop) {
				break;
			}
		}
	}
}
