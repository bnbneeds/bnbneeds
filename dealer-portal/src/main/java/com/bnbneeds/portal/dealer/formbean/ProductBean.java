package com.bnbneeds.portal.dealer.formbean;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

public class ProductBean {

	private String description;
	@NotBlank
	private String costPrice;
	private String modelId;
	private String serviceLocations;
	private List<Attribute> attributes;
	private String videoURL;
	private String tutorialVideoURL;

	public String getDescription() {
		return description;
	}

	public String getCostPrice() {
		return costPrice;
	}

	public String getModelId() {
		return modelId;
	}

	public String getServiceLocations() {
		return serviceLocations;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	public String getVideoURL() {
		return videoURL;
	}

	public String getTutorialVideoURL() {
		return tutorialVideoURL;
	}

	public void setVideoURL(String videoURL) {
		this.videoURL = videoURL;
	}

	public void setTutorialVideoURL(String tutorialVideoURL) {
		this.tutorialVideoURL = tutorialVideoURL;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public void setServiceLocations(String serviceLocations) {
		this.serviceLocations = serviceLocations;
	}

	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}

	public static class Attribute {

		private String name;
		private String value;

		public Attribute() {
			super();
		}

		public Attribute(String name, String value) {
			super();
			this.name = name;
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public String getValue() {
			return value;
		}

		public void setName(String name) {
			this.name = name;
		}

		public void setValue(String value) {
			this.value = value;
		}

	}

}
