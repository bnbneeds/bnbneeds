package com.bnbneeds.portal.dealer.formbean;

public class UpdateBidProductBean extends BidProductBean {

	private String productName;
	private String bidProductId;

	public String getBidProductId() {
		return bidProductId;
	}

	public void setBidProductId(String bidProductId) {
		this.bidProductId = bidProductId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

}
