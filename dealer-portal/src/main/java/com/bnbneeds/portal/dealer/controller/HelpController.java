package com.bnbneeds.portal.dealer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.util.StringUtils;

@Controller
@RequestMapping(path = "/help")
public class HelpController extends AbstractBaseController {

	private static final String MODEL_ATTRIBUTE_YOUTUBE_VIDEO_URL = "youtubeVideoURL";
	private static final String YOUTUBE_VIDEO_VIEW = "youtube-video";
	private static final String MODEL_ATTRIBUTE_VIDEO_VIEW_TITLE = "videoTitle";

	private static final String ADD_PRODUCTS_TUTORIAL_VIDEO_VIEW_TITLE = "Tutorial: How to add your products?";
	private static final String ANDROID_APP_VIDEO_TUTORIAL_VIEW_TITLE = "Tutorial: Android App";
	private static final String VENDOR_ANDROID_APP_TUTORIAL_VIDEO_YOUTUBE_ID = "uBUYK8-jgOQ";
	private static final String BUYER_ANDROID_APP_TUTORIAL_VIDEO_YOUTUBE_ID = "cHJgoAR1I5Y";
	private static final String ADD_PRODUCTS_YOUTUBE_VIDEO_ID = "UZcczrR7RCQ";

	private String getYoutubeVideoView(String videoId, String viewTitle, Model model) {
		String youtubeVideoURL = StringUtils.getYoutubeEmbedURLWithAutoPlay(videoId);
		model.addAttribute(MODEL_ATTRIBUTE_VIDEO_VIEW_TITLE, viewTitle);
		model.addAttribute(MODEL_ATTRIBUTE_YOUTUBE_VIDEO_URL, youtubeVideoURL);
		return YOUTUBE_VIDEO_VIEW;
	}

	@Layout(title = ADD_PRODUCTS_TUTORIAL_VIDEO_VIEW_TITLE)
	@RequestMapping(method = RequestMethod.GET, path = "/tutorials/add-product-video")
	public String getAddProductsTutorialVideoView(Model model) {
		return getYoutubeVideoView(ADD_PRODUCTS_YOUTUBE_VIDEO_ID, ADD_PRODUCTS_TUTORIAL_VIDEO_VIEW_TITLE, model);
	}

	@Layout(title = ANDROID_APP_VIDEO_TUTORIAL_VIEW_TITLE)
	@RequestMapping(method = RequestMethod.GET, path = "/tutorials/android-app-video")
	public String getAndroidTutorialVideo(@RequestParam("appFor") String appFor, Model model) {
		String videoId = null;
		switch (appFor) {
		case "vendor":
			videoId = VENDOR_ANDROID_APP_TUTORIAL_VIDEO_YOUTUBE_ID;
			break;
		case "buyer":
			videoId = BUYER_ANDROID_APP_TUTORIAL_VIDEO_YOUTUBE_ID;
		default:
			break;
		}
		return getYoutubeVideoView(videoId, ANDROID_APP_VIDEO_TUTORIAL_VIEW_TITLE, model);

	}

}
