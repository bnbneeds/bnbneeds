package com.bnbneeds.portal.dealer.formbean;

public class BuyerAggregatedPurchaseListBean extends AggregatedPurchaseListBean {

	private String vendorId;

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

}
