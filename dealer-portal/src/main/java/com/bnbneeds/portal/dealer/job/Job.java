package com.bnbneeds.portal.dealer.job;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class Job {

	private static final Logger logger = LoggerFactory.getLogger(Job.class);
	protected ExecutorService execService;

	public Job() {
		execService = Executors.newCachedThreadPool();
	}

	public void execute(Runnable runner) {
		logger.info("Executing runner: {}", runner.getClass().getSimpleName());
		execService.execute(runner);
	}

}
