package com.bnbneeds.portal.dealer.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = "/pipeline-jobs")
public class PipelineJobController extends AbstractBaseController {

	@RequestMapping(method = RequestMethod.GET, path = "/{jobId}")
	public void writeJobInfo(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("jobId") String jobId,
			HttpServletResponse httpResponse) throws IOException {

		try {
			String responseText = bnbneedsClient.pipelineJobs().getJobInfoAsString(jobId);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE, responseText,
					httpResponse);
		} catch (BNBNeedsServiceException e) {
			RequestResponseUtils.writeExceptionToResponse(e, httpResponse);
		}

	}
}
