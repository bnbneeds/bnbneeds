package com.bnbneeds.portal.dealer.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.EntityInfoListResponse;
import com.bnbneeds.app.model.EntityInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.AddTenderProductsParam;
import com.bnbneeds.app.model.dealer.tender.CreateTenderParam;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderProductInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderProductInfoResponse;
import com.bnbneeds.app.model.dealer.tender.UpdateTenderProductParam;
import com.bnbneeds.app.model.dealer.tender.UpdateTenderParam;
import com.bnbneeds.app.model.dealer.tender.UpdateTenderStatusParam;
import com.bnbneeds.app.model.dealer.tender.UpdateTenderStatusParam.TenderStatus;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.DealerUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.formbean.CreateBidProductBean;
import com.bnbneeds.portal.dealer.formbean.CreateTenderBean;
import com.bnbneeds.portal.dealer.formbean.TenderListBean;
import com.bnbneeds.portal.dealer.formbean.UpdateBidProductBean;
import com.bnbneeds.portal.dealer.formbean.UpdateTenderBean;
import com.bnbneeds.portal.dealer.formbean.mapper.TenderBeanMapper;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.core.Tenders;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.google.gson.Gson;

@Controller
@RequestMapping(path = TenderManagementController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('BUYER')")
public class TenderManagementController extends TenderController {

	private static final Logger logger = LoggerFactory.getLogger(TenderManagementController.class);

	protected static final String BASE_REQUEST_PATH = "/buyers/tenders";
	private static final String CREATE_TENDER_FORM_ATTRIBURE = "createTenderFormBean";
	private static final String ADD_BID_PRODUCT_FORM_ATTRIBURE = "addBidProductFormBean";
	private static final String UPDATE_BID_PRODUCT_FORM_ATTRIBURE = "updateBidProductFormBean";
	private static final String LIST_TENDER_FORM_ATTRIBURE = "listTenderFormBean";
	private static final String UPDATE_TENDER_FORM_ATTRIBURE = "updateTenderFormBean";
	private static final String CREATE_TENDER_FORM_VIEW = "tender-create-form";
	private static final String CREATE_TENDER_PRODUCT_FORM_VIEW = "tender-product-create-form";
	private static final String UPDATE_TENDER_PRODUCT_FORM_VIEW = "tender-product-update-form";
	private static final String TENDER_DETAILS_VIEW = "buyer-tender-details";
	private static final String TENDER_LIST_RESPONSE_ATTRIBUTE = "listResponse";
	private static final String TENDER_BID_PRODUCT_LIST_RESPONSE_ATTRIBUTE = "tenderBidProductList";
	private static final String TENDER_BID_LIST_RESPONSE_ATTRIBUTE = "bidListResponse";
	private static final String TENDER_DETAILS_REQUEST_URI = "/{tenderId}/details";
	private static final String ADD_BID_PRODUCT_FORM_REQUEST_URI = "/{tenderId}/add-product-form";
	private static final String TENDER_LIST_REQUEST_URI = "/list";
	private static final String TENDER_LIST_VIEW = "buyer-tender-list";
	private static final String UPDATE_TENDER_FORM_VIEW = "tender-update-form";
	private static final String CREDIT_TERMS_ATTRIBUTE = "creditTerms";
	private static final String PAYMENT_MODES_ATTRIBUTE = "paymentModes";
	private static final String BID_VALUE_TYPES_ATTRIBUTE = "bidValueTypes";
	private static final String DELIVERY_FREQUENCY_TYPES_ATTRIBUTE = "deliveryFrequencyTypes";
	private static final String PRODUCT_NAMES_ATTRIBUTE = "productNames";

	private static Map<String, List<EntityInfoResponse>> productNamesUserCache = new ConcurrentHashMap<>();

	private void loadTenderFormAttributes(Model model) {
		model.addAttribute(CREDIT_TERMS_ATTRIBUTE, creditTermsTypes);
		model.addAttribute(PAYMENT_MODES_ATTRIBUTE, paymentModes);
		model.addAttribute(BID_VALUE_TYPES_ATTRIBUTE, bidValueTypes);
		model.addAttribute(DELIVERY_FREQUENCY_TYPES_ATTRIBUTE, deliveryFrequencyTypes);
	}

	private synchronized static List<EntityInfoResponse> getProductNamesFromCache(BNBNeedsUser user,
			BNBNeedsClient bnbneedsClient) {

		String username = user.getUsername();
		List<EntityInfoResponse> productNames = productNamesUserCache.get(username);

		if (productNames == null) {
			productNames = new ArrayList<>();

			try {
				EntityInfoListResponse infolistResponse = bnbneedsClient.productNames().listBulk(RESULT_FETCH_SIZE,
						APPROVED_ENTITY_QUERY_MAP);
				List<EntityInfoResponse> entytyList = infolistResponse.getEntityInfoList();
				if (entytyList != null) {
					productNames.addAll(entytyList);
					String offset = infolistResponse.getNextOffset();
					String nextOffset = null;
					while (StringUtils.hasText(offset)) {
						infolistResponse = bnbneedsClient.productNames().listBulk(RESULT_FETCH_SIZE,
								APPROVED_ENTITY_QUERY_MAP, offset);
						if (infolistResponse != null) {
							entytyList = infolistResponse.getEntityInfoList();
							if (entytyList != null) {
								productNames.addAll(entytyList);
							}
							nextOffset = infolistResponse.getNextOffset();
							if (nextOffset.equals(offset)) {
								break;
							} else {
								offset = nextOffset;
							}
						}
					}
					productNamesUserCache.put(username, productNames);
				}
			} catch (BNBNeedsServiceException e) {
				clearProductNamesFromCache(user);
			}
		}
		return productNames;
	}

	private void setModelAttributesForBidProductForm(BNBNeedsUser user, Model model) {
		List<EntityInfoResponse> productNameList = getProductNamesFromCache(user, bnbneedsClient);
		model.addAttribute(PRODUCT_NAMES_ATTRIBUTE, productNameList);
	}

	private synchronized static void clearProductNamesFromCache(BNBNeedsUser user) {

		String username = user.getUsername();
		productNamesUserCache.remove(username);
	}

	private Tenders getClientHandle(BNBNeedsUser user) {
		String buyerId = user.getRelationshipId();
		return bnbneedsClient.tenders(buyerId);
	}

	public static String getPathForGetTenderDetails(String tenderId) {
		return BASE_REQUEST_PATH + TENDER_DETAILS_REQUEST_URI.replace("{tenderId}", tenderId);
	}

	private String getPathForGetTenderList() {
		return BASE_REQUEST_PATH + TENDER_LIST_REQUEST_URI;
	}

	private String getPathForGetAddBidProductForm(String tenderId) {
		return BASE_REQUEST_PATH + ADD_BID_PRODUCT_FORM_REQUEST_URI.replace("{tenderId}", tenderId);
	}

	private void setTenderInfoResponse(BNBNeedsUser user, String tenderId, Model model) {
		TenderInfoResponse response = getClientHandle(user).get(tenderId);
		setTenderInfoResponse(response, model);
		TenderProductInfoListResponse bidProducts = getClientHandle(user).listBidProducts(tenderId, 3);
		model.addAttribute(TENDER_BID_PRODUCT_LIST_RESPONSE_ATTRIBUTE, bidProducts);
		TenderBidInfoListResponse tenderBids = getClientHandle(user).listTenderBids(tenderId, 5);

		setBidTimestampForListResponse(tenderBids);
		model.addAttribute(TENDER_BID_LIST_RESPONSE_ATTRIBUTE, tenderBids);
	}

	private boolean isBiddingSubscriptionActive(BNBNeedsUser user) {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("state", "ACTIVE");
		BuyerBiddingSubscriptionInfoListResponse response = bnbneedsClient
				.buyerBiddingSubscriptions(user.getRelationshipId()).list(1, queryMap);
		return response.isEmpty() == false;

	}

	/**
	 * Checks if the tender can be updated
	 * 
	 * @param user
	 *            the authenticated user
	 * @param tenderId
	 *            the tender ID
	 * @return the redirect path on exception; {@code null} otherwise.
	 */
	private String isTenderAllowedToBeUpdated(BNBNeedsUser user, String tenderId) {

		TenderInfoResponse tender = getClientHandle(user).get(tenderId);
		switch (tender.getStatus()) {
		case TENDER_STATUS_OPEN:
			try {
				UpdateTenderStatusParam param = new UpdateTenderStatusParam(TenderStatus.EDIT);
				getClientHandle(user).updateTenderStatus(tenderId, param);
			} catch (BNBNeedsServiceException e) {
				return getRedirectPath(
						getPathForGetTenderDetails(tenderId) + "?cannotUpdateTenderReason=" + e.getMessage());
			}
			break;
		case "CREATED":
		case "EDIT_IN_PROGRESS":
			return null;
		default:
			return null;
		}
		return null;
	}

	public static String getOpenTenderListUrl() {
		return String.format("%1$s%2$s?tenderStatus=%3$s", BASE_REQUEST_PATH, TENDER_LIST_REQUEST_URI,
				TENDER_STATUS_OPEN);
	}

	public static String getPathForGetTenderBids(String tenderId) {
		return getPathForGetTenderDetails(tenderId) + "?bids";
	}

	@Layout(title = "Create Tender")
	@RequestMapping(method = RequestMethod.GET, path = "/create-form")
	public String getCreateTenderForm(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(CREATE_TENDER_FORM_ATTRIBURE) CreateTenderBean form, Model model) {

		DealerUtils.checkHasDealership(user);
		if (!isBiddingSubscriptionActive(user)) {
			return getRedirectPath(getPathForGetTenderList() + "?noActiveBiddingSubscription");
		}
		loadTenderFormAttributes(model);
		return CREATE_TENDER_FORM_VIEW;
	}

	@Layout(title = "Add Products to the Tender")
	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/add-product-form")
	public String getCreateTenderProductForm(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId,
			@ModelAttribute(ADD_BID_PRODUCT_FORM_ATTRIBURE) CreateBidProductBean form, Model model) {

		DealerUtils.checkHasDealership(user);
		try {
			String redirectPath = isTenderAllowedToBeUpdated(user, tenderId);
			if (redirectPath != null) {
				return redirectPath;
			}
			clearProductNamesFromCache(user);
			setModelAttributesForBidProductForm(user, model);
			form.setTenderId(tenderId);
			return CREATE_TENDER_PRODUCT_FORM_VIEW;
		} catch (BNBNeedsServiceException e) {
			setModelAttributesForBidProductForm(user, model);
			return RequestResponseUtils.handleBadRequest(e, model, CREATE_TENDER_PRODUCT_FORM_VIEW);
		}
	}

	@Layout(title = "Add Products to the Tender")
	@RequestMapping(method = RequestMethod.POST, path = "/{tenderId}/add-product")
	public String createTenderProduct(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId,
			@ModelAttribute(ADD_BID_PRODUCT_FORM_ATTRIBURE) CreateBidProductBean form, Model model) {

		DealerUtils.checkHasDealership(user);
		try {

			String redirectPath = isTenderAllowedToBeUpdated(user, tenderId);
			if (redirectPath != null) {
				return redirectPath;
			}

			AddTenderProductsParam param = TenderBeanMapper.mapCreateBidProductBean(form);
			getClientHandle(user).addBidProducts(tenderId, param);

			if (form.isCreateAnotherBidProduct()) {
				return getRedirectPath(getPathForGetAddBidProductForm(tenderId) + "?createBidProductSuccess");
			} else {
				clearProductNamesFromCache(user);
			}
			return getRedirectPath(getPathForGetTenderDetails(tenderId) + "?addBidProductsSuccess");
		} catch (BNBNeedsServiceException e) {
			setModelAttributesForBidProductForm(user, model);
			return RequestResponseUtils.handleBadRequest(e, model, CREATE_TENDER_PRODUCT_FORM_VIEW);
		}
	}

	@Layout(title = "Create Tender")
	@RequestMapping(method = RequestMethod.POST, path = "/create")
	public String createTender(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(CREATE_TENDER_FORM_ATTRIBURE) CreateTenderBean form, Model model) {

		DealerUtils.checkHasDealership(user);

		try {
			if (!isBiddingSubscriptionActive(user)) {
				return getRedirectPath(getPathForGetTenderList() + "?noActiveBiddingSubscription");
			}
			CreateTenderParam param = TenderBeanMapper.mapCreateTenderBean(form);
			// mapFormAttributes(param);
			CreateResourceResponse response = getClientHandle(user).create(param);
			String tenderId = response.getResourceRep().getId();
			return getRedirectPath(getPathForGetTenderDetails(tenderId) + "?createSuccess");
		} catch (BNBNeedsServiceException e) {
			loadTenderFormAttributes(model);
			return RequestResponseUtils.handleBadRequest(e, model, CREATE_TENDER_FORM_VIEW);
		}
	}

	@Layout(title = "Tenders")
	@RequestMapping(method = RequestMethod.GET, path = TENDER_LIST_REQUEST_URI)
	public String getTenderList(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(LIST_TENDER_FORM_ATTRIBURE) TenderListBean form, Model model) {

		DealerUtils.checkHasDealership(user);

		loadTenderQueryStatusMap(model);
		String tenderStatus = form.getTenderStatus();
		String nextOffset = form.getNextOffset();
		Map<String, String> queryParams = new HashMap<>();
		if (StringUtils.hasText(tenderStatus)) {
			queryParams.put("status", tenderStatus);
		}
		TenderInfoListResponse listResponse = null;
		try {

			if (!queryParams.isEmpty()) {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = getClientHandle(user).list(RESULT_FETCH_SIZE, queryParams, nextOffset);
				} else {
					listResponse = getClientHandle(user).list(RESULT_FETCH_SIZE, queryParams);
				}
			} else {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = getClientHandle(user).list(RESULT_FETCH_SIZE, nextOffset);
				} else {
					listResponse = getClientHandle(user).list(RESULT_FETCH_SIZE);
				}
			}
			if (listResponse != null) {
				nextOffset = listResponse.getNextOffset();
				if (StringUtils.hasText(nextOffset)) {
					form.setNextOffset(nextOffset);
				}
			}
			model.addAttribute(TENDER_LIST_RESPONSE_ATTRIBUTE, listResponse);
			return TENDER_LIST_VIEW;
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, TENDER_LIST_VIEW);
		}
	}

	@Layout(title = "Tender Details")
	@RequestMapping(method = RequestMethod.GET, path = TENDER_DETAILS_REQUEST_URI)
	public String getTenderDetails(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId, Model model) {

		DealerUtils.checkHasDealership(user);

		try {
			setTenderInfoResponse(user, tenderId, model);
			return TENDER_DETAILS_VIEW;
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, TENDER_DETAILS_VIEW);
		}
	}

	@Layout(title = "Update Tender")
	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/update-form")
	public String getUpdateTenderForm(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId,
			@ModelAttribute(UPDATE_TENDER_FORM_ATTRIBURE) UpdateTenderBean form, Model model) {

		DealerUtils.checkHasDealership(user);

		loadTenderFormAttributes(model);

		try {
			String redirectPath = isTenderAllowedToBeUpdated(user, tenderId);
			if (redirectPath != null) {
				return redirectPath;
			}

			TenderInfoResponse tender = getClientHandle(user).get(tenderId);
			TenderBeanMapper.mapTender(tender, form);
			form.setTenderId(tenderId);
			return UPDATE_TENDER_FORM_VIEW;
		} catch (BNBNeedsServiceException e) {
			return getRedirectPath(
					getPathForGetTenderDetails(tenderId) + "?cannotUpdateTenderReason=" + e.getMessage());
		}
	}

	@Layout(title = "Update Tender")
	@RequestMapping(method = RequestMethod.POST, path = "/{tenderId}/update")
	public String updateTender(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("tenderId") String tenderId,
			@ModelAttribute(UPDATE_TENDER_FORM_ATTRIBURE) UpdateTenderBean form, Model model) {

		DealerUtils.checkHasDealership(user);

		try {
			form.setTenderId(tenderId);
			String redirectPath = isTenderAllowedToBeUpdated(user, tenderId);
			if (redirectPath != null) {
				return redirectPath;
			}
			UpdateTenderParam param = new UpdateTenderParam();
			TenderBeanMapper.mapTenderBean(form, param);
			getClientHandle(user).updateTender(tenderId, param);
			return getRedirectPath(getPathForGetTenderDetails(tenderId) + "?updateSuccess");
		} catch (BNBNeedsServiceException e) {
			loadTenderFormAttributes(model);
			return RequestResponseUtils.handleBadRequest(e, model, UPDATE_TENDER_FORM_VIEW);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{tenderId}/update-status")
	public String updateTenderStatus(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId, String status, String reasonForCancellation, Model model) {

		DealerUtils.checkHasDealership(user);

		try {
			TenderStatus tenderStatus = TenderStatus.valueOf(status);
			UpdateTenderStatusParam param = new UpdateTenderStatusParam(tenderStatus);
			if (tenderStatus == TenderStatus.CANCEL) {
				param.setReasonForCancellation(reasonForCancellation);
			}
			getClientHandle(user).updateTenderStatus(tenderId, param);
			return getRedirectPath(getPathForGetTenderDetails(tenderId) + "?updateStatusSuccess");
		} catch (BNBNeedsServiceException e) {
			return getRedirectPath(getPathForGetTenderDetails(tenderId) + "?updateStatusFailed=" + e.getMessage());
		}
	}

	@Layout(title = "Update Product Requirement")
	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/products/{productId}/update-form")
	public String getUpdateTenderProductForm(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId, @PathVariable("productId") String productId,
			@ModelAttribute(UPDATE_BID_PRODUCT_FORM_ATTRIBURE) UpdateBidProductBean form, Model model) {

		DealerUtils.checkHasDealership(user);

		try {

			String redirectPath = isTenderAllowedToBeUpdated(user, tenderId);
			if (redirectPath != null) {
				return redirectPath;
			}
			TenderProductInfoResponse bidProduct = getClientHandle(user).getBidProduct(tenderId, productId);
			TenderBeanMapper.mapBidProduct(bidProduct, form);
			form.setProductName(bidProduct.getProductName().getName());
			return UPDATE_TENDER_PRODUCT_FORM_VIEW;
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, UPDATE_TENDER_PRODUCT_FORM_VIEW);
		}
	}

	@Layout(title = "Update Product Requirement")
	@RequestMapping(method = RequestMethod.POST, path = "/{tenderId}/products/{productId}/update")
	public String updateTenderProduct(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId, @PathVariable("productId") String productId,
			@ModelAttribute(UPDATE_BID_PRODUCT_FORM_ATTRIBURE) UpdateBidProductBean form, Model model) {

		DealerUtils.checkHasDealership(user);

		try {
			String redirectPath = isTenderAllowedToBeUpdated(user, tenderId);
			if (redirectPath != null) {
				return redirectPath;
			}
			UpdateTenderProductParam param = new UpdateTenderProductParam();
			TenderBeanMapper.mapBidProductBean(form, param);
			param.setProductNameId(null);
			getClientHandle(user).updateBidProduct(tenderId, productId, param);
			return getRedirectPath(getPathForGetTenderDetails(tenderId) + "?updateBidProductSuccess");
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, UPDATE_TENDER_PRODUCT_FORM_VIEW);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{tenderId}/products/{productId}/delete")
	public String deleteBidProduct(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId, @PathVariable("productId") String productId, Model model) {

		DealerUtils.checkHasDealership(user);

		try {

			String redirectPath = isTenderAllowedToBeUpdated(user, tenderId);
			if (redirectPath != null) {
				return redirectPath;
			}
			getClientHandle(user).deleteBidProduct(tenderId, productId);
			return getRedirectPath(getPathForGetTenderDetails(tenderId) + "?deleteBidProductSuccess");
		} catch (BNBNeedsServiceException e) {
			return getRedirectPath(getPathForGetTenderDetails(tenderId) + "?deleteBidProductFailed=" + e.getMessage());
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/products/list-as-text")
	@ResponseBody
	public synchronized void writeTenderProducts(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			HttpServletResponse response) throws IOException {

		final int FETCH_SIZE = 10;

		try {
			DealerUtils.checkHasDealership(user);
			String bidProductListResponse = null;

			if (StringUtils.hasText(nextOffset)) {
				bidProductListResponse = getClientHandle(user).listTenderProductsAsString(tenderId, FETCH_SIZE,
						nextOffset);
			} else {
				bidProductListResponse = getClientHandle(user).listTenderProductsAsString(tenderId, FETCH_SIZE);
			}
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE,
					bidProductListResponse, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/bids/list-as-text")
	@ResponseBody
	public synchronized void writeBids(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			HttpServletResponse response) throws IOException {

		try {
			DealerUtils.checkHasDealership(user);
			TenderBidInfoListResponse bidListResponse = null;

			if (StringUtils.hasText(nextOffset)) {
				bidListResponse = getClientHandle(user).listTenderBids(tenderId, FETCH_SIZE, nextOffset);
			} else {
				bidListResponse = getClientHandle(user).listTenderBids(tenderId, FETCH_SIZE);
			}

			setBidTimestampForListResponse(bidListResponse);

			Gson gson = new Gson();
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE,
					gson.toJson(bidListResponse), response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

}
