package com.bnbneeds.portal.dealer.exception;

public class ResourceNotFoundException extends APIException {

	private static final long serialVersionUID = 2417486850678036862L;

	private static final String ID_NOT_FOUND_OF_TYPE = "%s not found with id: %s.";
	private static final String NOT_FOUND_OF_TYPE = "%s not found.";

	public ResourceNotFoundException() {
		super();
	}

	public ResourceNotFoundException(String message) {
		super(message);
	}

	public static ResourceNotFoundException idNotFound(String id,
			Class<? extends Object> clazz) {
		return new ResourceNotFoundException(getMessage(ID_NOT_FOUND_OF_TYPE,
				clazz.getSimpleName(), id));
	}

	public static ResourceNotFoundException idNotFound(String id, String type) {
		return new ResourceNotFoundException(getMessage(ID_NOT_FOUND_OF_TYPE,
				type, id));
	}

	public static ResourceNotFoundException idNotFound(Integer id,
			Class<? extends Object> clazz) {
		return new ResourceNotFoundException(getMessage(ID_NOT_FOUND_OF_TYPE,
				clazz.getSimpleName(), String.valueOf(id)));
	}

	public static ResourceNotFoundException idNotFound(Integer id, String type) {
		return new ResourceNotFoundException(getMessage(ID_NOT_FOUND_OF_TYPE,
				type, String.valueOf(id)));
	}

	public static ResourceNotFoundException notFound(
			Class<? extends Object> clazz) {
		return new ResourceNotFoundException(getMessage(NOT_FOUND_OF_TYPE,
				clazz.getSimpleName()));
	}

	public static ResourceNotFoundException notFound(String type) {
		return new ResourceNotFoundException(
				getMessage(NOT_FOUND_OF_TYPE, type));
	}

}
