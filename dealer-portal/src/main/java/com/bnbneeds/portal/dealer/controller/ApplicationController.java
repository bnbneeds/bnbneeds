package com.bnbneeds.portal.dealer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.bnbneeds.portal.dealer.bean.ApplicationBeans;

@Controller
@RequestMapping(path = "/application")
public class ApplicationController extends AbstractBaseController {

	@Autowired
	private ApplicationBeans applicationBeans;

	@Autowired
	private JmsTemplate jmsTemplate;

	@RequestMapping(method = RequestMethod.GET, path = "/maintenance-notice")
	@ResponseStatus(code = HttpStatus.OK)
	@ResponseBody
	public String postMaintenanceNotice() {
		// Message message =
		// jmsTemplate.receive(JMSConstants.MAINTENANCE_NOTICE_QUEUE_NAME);
		// System.out.println(message);

		return applicationBeans.getMaintenanceNotice().toString();
	}

	@RequestMapping(method = RequestMethod.POST, path = "/maintenance-notice/clear")
	@ResponseStatus(code = HttpStatus.OK)
	@ResponseBody
	public String clearMaintenanceNotice() {
		applicationBeans.setMaintenanceNotice(null);
		return "OK";
	}

}
