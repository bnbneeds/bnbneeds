package com.bnbneeds.portal.dealer.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.portal.dealer.controller.util.DealerUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.controller.util.ReviewUtility;
import com.bnbneeds.portal.dealer.formbean.ReviewBean;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;

@Controller
@RequestMapping(path = "/products/{productId}/reviews")
public class ProductReviewController extends AbstractBaseController {

	private static final ReviewFor REVIEW_FOR = ReviewFor.product;

	@RequestMapping(method = RequestMethod.GET, path = "/list-as-text")
	@ResponseBody
	@PreAuthorize("hasAnyAuthority('BUYER', 'VENDOR')")
	public synchronized void writeReviews(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId,
			@RequestParam(name = "reviewedBy", required = false) String reviewedBy,
			@RequestParam(name = "nextOffset", required = false) String nextOffset,
			HttpServletResponse response) throws IOException {

		try {
			DealerUtils.checkHasDealership(user);
			String list = ReviewUtility.getReviewsAsString(bnbneedsClient,
					REVIEW_FOR, productId, reviewedBy, nextOffset);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, list, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/list-as-text-by-reviewer")
	@ResponseBody
	@PreAuthorize("hasAuthority('BUYER')")
	public synchronized void writeReviewsByBuyer(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId,
			HttpServletResponse response) throws IOException {

		try {
			String buyerId = DealerUtils.checkAndReturnRelationshipId(user);
			String list = ReviewUtility.getReviewsAsString(bnbneedsClient,
					REVIEW_FOR, productId, buyerId, null);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, list, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize("hasAuthority('BUYER')")
	public synchronized void postReview(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId, ReviewBean form,
			HttpServletResponse response) throws IOException {
		try {
			DealerUtils.checkHasDealership(user);

			ReviewUtility.postReview(bnbneedsClient, REVIEW_FOR, productId,
					form);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, "OK", response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{reviewId}/delete")
	@ResponseBody
	@PreAuthorize("hasAuthority('BUYER')")
	public synchronized void deleteReview(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId,
			@PathVariable("reviewId") String reviewId,
			HttpServletResponse response) throws IOException {
		try {
			DealerUtils.checkHasDealership(user);

			ReviewUtility.deleteReview(bnbneedsClient, REVIEW_FOR, productId,
					reviewId);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, "OK", response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

}
