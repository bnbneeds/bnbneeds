package com.bnbneeds.portal.dealer.bean;

public class MaintenanceNoticeBean {

	private String startTime;
	private String endTime;
	private String subject;
	private String message;

	public String getSubject() {
		return subject;
	}

	public String getMessage() {
		return message;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStartTime() {
		return startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MaintenanceNoticeBean [");
		if (startTime != null)
			builder.append("startTime=").append(startTime).append(", ");
		if (endTime != null)
			builder.append("endTime=").append(endTime).append(", ");
		if (subject != null)
			builder.append("subject=").append(subject).append(", ");
		if (message != null)
			builder.append("message=").append(message);
		builder.append("]");
		return builder.toString();
	}

}
