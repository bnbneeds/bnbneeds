package com.bnbneeds.portal.dealer.controller;

import org.springframework.ui.Model;

import com.bnbneeds.app.model.dealer.order.AggregatedPurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemInfoResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseListResponse;
import com.bnbneeds.app.model.product.ProductNameListResponse;

public abstract class PurchaseListController extends DealerController {

	protected static final String PURCHASE_LIST_RESPONSE_ATTRIBUTE = "purchaseListResponse";
	protected static final String PURCHASE_ITEM_INFO_RESPONSE_ATTRIBUTE = "purchaseItemInfoResponse";
	protected static final String PURCHASE_ITEM_STATUS_MAP_ATTRIBUTE = "purchaseItemStatusMap";
	protected static final String PURCHASE_ITEM_UPDATE_FORM_ATTRIBUTE = "purchaseItemUpdateBean";
	protected static final String AGGREGATED_PURCHASE_LIST_FORM_ATTRIBUTE = "aggregatedPurchaseListFormBean";
	protected static final String PRODUCT_NAME_LIST_RESPONSE = "productNameListResponse";
	protected static final String AGGREGATED_PURCHASE_LIST_RESPONSE = "aggregatedResponse";

	protected static final String REQ_PARAM_REQUEST_DATE = "requestDate";
	protected static final String REQ_PARAM_REQUEST_DATE_RANGE = "requestDateRange";

	static void setPurchaseListResponse(Model model,
			PurchaseListResponse listResponse) {
		model.addAttribute(PURCHASE_LIST_RESPONSE_ATTRIBUTE, listResponse);
	}

	static void setPurchaseInfoResponse(Model model,
			PurchaseItemInfoResponse infoResponse) {
		model.addAttribute(PURCHASE_ITEM_INFO_RESPONSE_ATTRIBUTE, infoResponse);
	}

	static void setAggregatedPurchaseListResponse(Model model,
			AggregatedPurchaseListResponse reponse) {
		model.addAttribute(AGGREGATED_PURCHASE_LIST_RESPONSE, reponse);
	}

	static void setProductNameListResponse(Model model,
			ProductNameListResponse response) {
		model.addAttribute(PRODUCT_NAME_LIST_RESPONSE, response);
	}

}
