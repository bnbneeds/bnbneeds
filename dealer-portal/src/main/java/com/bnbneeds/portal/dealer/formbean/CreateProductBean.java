package com.bnbneeds.portal.dealer.formbean;

import org.hibernate.validator.constraints.NotBlank;

public class CreateProductBean extends ProductBean {

	@NotBlank
	private String productCategoryId;
	@NotBlank
	private String productTypeId;
	@NotBlank
	private String productNameId;

	public String getProductCategoryId() {
		return productCategoryId;
	}

	public String getProductTypeId() {
		return productTypeId;
	}

	public String getProductNameId() {
		return productNameId;
	}

	public void setProductCategoryId(String productCategoryId) {
		this.productCategoryId = productCategoryId;
	}

	public void setProductTypeId(String productTypeId) {
		this.productTypeId = productTypeId;
	}

	public void setProductNameId(String productNameId) {
		this.productNameId = productNameId;
	}
}
