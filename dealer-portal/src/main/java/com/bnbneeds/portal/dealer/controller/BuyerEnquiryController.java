package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.handleBadRequest;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.writeResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.dealer.CreateEnquiryParam;
import com.bnbneeds.app.model.dealer.EnquiryInfoListResponse;
import com.bnbneeds.app.model.dealer.UpdateEnquiryParam;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.DealerUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.formbean.BuyerEnquiryListRequestBean;
import com.bnbneeds.portal.dealer.formbean.CreateProductEnquiryBean;
import com.bnbneeds.portal.dealer.formbean.UpdateProductEnquiryBean;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.Constants;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = BuyerEnquiryController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('BUYER')")
public class BuyerEnquiryController extends DealerController {

	protected static final String BASE_REQUEST_PATH = "/buyers/product-enquiries";
	private static final int FETCH_SIZE = 20;
	private static final String BUYER_ENQUIRY_LIST_VIEW = "buyer-enquiry-list";
	private static final String BUYER_ENQUIRY_LIST_RESPONSE_ATTRIBUTE = "buyerEnquiryListResponse";
	private static final String ENQUIRY_LIST_REQUEST_FORM_ATTRIBUTE = "enquiryListRequestForm";

	private static void setEnquiryListResponse(Model model, EnquiryInfoListResponse listResponse) {

		model.addAttribute(BUYER_ENQUIRY_LIST_RESPONSE_ATTRIBUTE, listResponse);
	}

	public static String getEnquiryListURI() {
		return BASE_REQUEST_PATH;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/create")
	@ResponseBody
	public synchronized void createProductEnquiry(@AuthenticationPrincipal BNBNeedsUser user,
			CreateProductEnquiryBean form, HttpServletResponse response) throws IOException {

		try {
			String buyerId = DealerUtils.checkAndReturnRelationshipId(user);
			CreateEnquiryParam param = new CreateEnquiryParam();
			param.setProductId(form.getProductId());
			param.setLikelyToBuyIn(form.getLikelyToBuyIn(), form.getTimeUnit());
			param.setRequiredQuantity(form.getRequiredQuantity());
			param.setDescription(form.getDescription());
			bnbneedsClient.buyers(buyerId).createProductEnquiry(param);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE, "OK", response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/delete")
	@ResponseBody
	public synchronized void deleteProductEnquiry(@AuthenticationPrincipal BNBNeedsUser user, String enquiryId,
			HttpServletResponse response) throws IOException {
		try {
			String buyerId = DealerUtils.checkAndReturnRelationshipId(user);

			bnbneedsClient.buyers(buyerId).deleteProductEnquiry(enquiryId);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE, "OK", response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/update")
	@ResponseBody
	public synchronized void updateProductEnquiry(@AuthenticationPrincipal BNBNeedsUser user,
			UpdateProductEnquiryBean form, HttpServletResponse response) throws IOException {

		try {
			String buyerId = DealerUtils.checkAndReturnRelationshipId(user);
			UpdateEnquiryParam param = new UpdateEnquiryParam();
			param.setLikelyToBuyIn(form.getLikelyToBuyIn(), form.getTimeUnit());
			param.setRequiredQuantity(form.getRequiredQuantity());
			param.setDescription(form.getDescription());
			bnbneedsClient.buyers(buyerId).updateProductEnquiry(form.getEnquiryId(), param);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE, "OK", response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@Layout(title = "Product Enquiries")
	@RequestMapping(method = RequestMethod.GET, path = "/list-request-form")
	public String getEnquiryListRequestForm(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(ENQUIRY_LIST_REQUEST_FORM_ATTRIBUTE) BuyerEnquiryListRequestBean form, BindingResult result,
			Model model) {

		return BUYER_ENQUIRY_LIST_VIEW;

	}

	@Layout(title = "Product Enquiries")
	@RequestMapping(method = RequestMethod.GET)
	public String getEnquiries(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			@ModelAttribute(ENQUIRY_LIST_REQUEST_FORM_ATTRIBUTE) BuyerEnquiryListRequestBean form, BindingResult result,
			Model model) {

		String buyerId = getDealershipId(user);

		Map<String, String> queryMap = new HashMap<>();
		if (StringUtils.hasText(form.getEnquiryDate())) {
			if (!StringUtils.isDateFormatValid(form.getEnquiryDate(), Constants.DATE_FORMAT)) {
				result.rejectValue("enquiryDate", null, "Enquiry date format is invalid.");
			}
			queryMap.put("enquiryDate", form.getEnquiryDate());
		} else {
			if (StringUtils.hasText(form.getEnquiryDateRange())) {
				String[] dateRangeArray = getRequestStartAndEndDate(form.getEnquiryDateRange());
				String startDate = dateRangeArray[0].trim();
				String endDate = dateRangeArray[1].trim();

				if (!StringUtils.isDateFormatValid(startDate, Constants.DATE_FORMAT)) {
					result.rejectValue("enquiryDateRange", null, "Start date format is invalid.");
				}

				if (!StringUtils.isDateFormatValid(endDate, Constants.DATE_FORMAT)) {
					result.rejectValue("enquiryDateRange", null, "End date format is invalid.");
				}
				queryMap.put("startEnquiryDate", startDate);
				queryMap.put("endEnquiryDate", endDate);
			}
		}

		if (StringUtils.hasText(form.getVendor())) {
			String vendor = form.getVendor();
			if (form.getVendor().contains("|")) {
				String[] nameAndIdArr = vendor.split("\\|");
				queryMap.put("vendorId", nameAndIdArr[1].trim());
				setSelectedDealer(model, vendor);
			}
		}

		try {
			if (!result.hasErrors()) {
				EnquiryInfoListResponse listResponse = null;
				if (!queryMap.isEmpty()) {
					if (StringUtils.hasText(nextOffset)) {
						listResponse = bnbneedsClient.buyers(buyerId).getEnquiries(FETCH_SIZE, queryMap, nextOffset);
					} else {

						listResponse = bnbneedsClient.buyers(buyerId).getEnquiries(FETCH_SIZE, queryMap);
					}
				} else {
					if (StringUtils.hasText(nextOffset)) {
						listResponse = bnbneedsClient.buyers(buyerId).getEnquiries(FETCH_SIZE, nextOffset);
					} else {
						listResponse = bnbneedsClient.buyers(buyerId).getEnquiries(FETCH_SIZE);
					}

				}
				setEnquiryListResponse(model, listResponse);
			}
			return BUYER_ENQUIRY_LIST_VIEW;
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_ENQUIRY_LIST_VIEW);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/vendors")
	@ResponseBody
	public synchronized void writeAssociatedVendorList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "enquiryDate", required = false) String enquiryDate,
			@RequestParam(name = "enquiryDateRange", required = false) String enquiryDateRange,
			HttpServletResponse response) throws IOException, InterruptedException {

		String associatedBuyerListResponse = null;

		try {

			String buyerId = getDealershipId(user);

			if (StringUtils.hasText(enquiryDate)) {
				if (!StringUtils.isDateFormatValid(enquiryDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"Request date format is invalid.", response);
					return;
				}
				associatedBuyerListResponse = bnbneedsClient.buyers(buyerId)
						.getAssociatedVendorListOfEnquiriesAsString(enquiryDate);
			} else {
				// Date range

				String[] dateRangeArray = getRequestStartAndEndDate(enquiryDateRange);
				String startEnquiryDate = dateRangeArray[0].trim();
				String endEnquiryDate = dateRangeArray[1].trim();

				if (!StringUtils.isDateFormatValid(startEnquiryDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"Start date format is invalid.", response);
					return;
				}

				if (!StringUtils.isDateFormatValid(endEnquiryDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"End date format is invalid.", response);
					return;
				}

				associatedBuyerListResponse = bnbneedsClient.buyers(buyerId)
						.getAssociatedVendorListOfEnquiriesByDateRangeAsString(startEnquiryDate, endEnquiryDate);

			}

			writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE, associatedBuyerListResponse, response);

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}
}
