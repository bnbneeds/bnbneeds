package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.handleBadRequest;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.writeResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.model.EntityInfoListResponse;
import com.bnbneeds.app.model.EntityInfoResponse;
import com.bnbneeds.portal.dealer.controller.util.DealerUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.formbean.EntityBean;
import com.bnbneeds.portal.dealer.formbean.EntityListBean;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.core.AbstractMasterEntityTypes;
import com.bnbneeds.rest.client.core.AbstractMasterEntityTypes.EntityType;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.google.gson.Gson;

public abstract class MasterEntityController extends AbstractBaseController {

	static final String ENTITY_LIST_VIEW = "entity-list";
	private static final String LIST_VIEW_TITLE_ATTRIBUTE = "listViewTitle";
	private static final String LIST_RESPONSE_ATTRIBUTE = "listResponse";
	private static final String CREATE_ENTITY_REQUEST_URI = "createEntityRequestURI";
	private static final String LIST_ENTITY_REQUEST_URI = "listEntityRequestURI";
	protected static final String ENTITY_LIST_FORM_ATTRIBUTE = "entityListForm";

	public abstract EntityType getEntityType();

	public abstract String getViewTitle();

	public abstract String getCreateEntityURI();

	public abstract String getListEntityURI();

	private AbstractMasterEntityTypes getClientByEntityType() {
		switch (getEntityType()) {
		case product_type:
			return bnbneedsClient.productTypes();
		case product_category:
			return bnbneedsClient.productCategories();
		case product_name:
			return bnbneedsClient.productNames();
		case business_type:
			return bnbneedsClient.businessTypes();
		default:
			return null;
		}
	}

	protected void setViewTitle(Model model, String title) {
		model.addAttribute(LIST_VIEW_TITLE_ATTRIBUTE, title);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/list")
	@PreAuthorize("hasAnyAuthority('VENDOR', 'BUYER')")
	public String list(
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			@ModelAttribute(ENTITY_LIST_FORM_ATTRIBUTE) EntityListBean form,
			Model model) {

		List<EntityBean> entities = new ArrayList<EntityBean>();
		EntityBean entityForm = new EntityBean();
		entities.add(entityForm);
		form.setEntities(entities);

		try {
			setModelForEntityListView(model, nextOffset);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, ENTITY_LIST_VIEW);
		}
		return ENTITY_LIST_VIEW;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/list-as-text")
	@PreAuthorize("hasAnyAuthority('VENDOR', 'BUYER')")
	public synchronized void writeAllEntities(
			@AuthenticationPrincipal BNBNeedsUser user,
			HttpServletResponse response) throws IOException {
		List<EntityInfoResponse> entityList = new ArrayList<EntityInfoResponse>();
		try {
			DealerUtils.checkHasDealership(user);
			String nextOffset = null;
			EntityInfoListResponse entityListResp = null;
			String currentOffset = null;

			entityListResp = getClientByEntityType()
					.listBulk(RESULT_FETCH_SIZE);
			if (entityListResp != null) {
				currentOffset = nextOffset;
				nextOffset = entityListResp.getNextOffset();
				if (entityListResp.getEntityInfoList() != null) {
					entityList.addAll(entityListResp.getEntityInfoList());
				}
			}

			do {
				entityListResp = getClientByEntityType().listBulk(
						RESULT_FETCH_SIZE, nextOffset);
				if (entityListResp != null) {
					currentOffset = nextOffset;
					nextOffset = entityListResp.getNextOffset();
					if (entityListResp.getEntityInfoList() != null) {
						entityList.addAll(entityListResp.getEntityInfoList());
					}
				}

			} while (!nextOffset.equals(currentOffset));
			Gson gson = new Gson();
			String textResponse = gson.toJson(entityList);
			writeResponse(200, MediaType.TEXT_PLAIN_VALUE, textResponse,
					response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	protected void setModelForEntityListView(Model model, String nextOffset) {

		setViewTitle(model, getViewTitle());
		model.addAttribute(CREATE_ENTITY_REQUEST_URI, request.getContextPath()
				+ getCreateEntityURI());
		model.addAttribute(LIST_ENTITY_REQUEST_URI, request.getContextPath()
				+ getListEntityURI());

		try {
			EntityInfoListResponse listResponse = null;
			AbstractMasterEntityTypes entityClient = getClientByEntityType();
			if (StringUtils.hasText(nextOffset)) {
				listResponse = entityClient.listBulk(RESULT_FETCH_SIZE,
						nextOffset);
			} else {
				listResponse = entityClient.listBulk(RESULT_FETCH_SIZE);
			}
			model.addAttribute(LIST_RESPONSE_ATTRIBUTE, listResponse);
		} catch (BNBNeedsServiceException e) {
			throw e;
		}

	}

}
