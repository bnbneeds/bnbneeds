package com.bnbneeds.portal.dealer.formbean;

public class CreateBidProductBean extends BidProductBean {

	private boolean createAnotherBidProduct;

	public boolean isCreateAnotherBidProduct() {
		return createAnotherBidProduct;
	}

	public void setCreateAnotherBidProduct(boolean createAnotherBidProduct) {
		this.createAnotherBidProduct = createAnotherBidProduct;
	}

}
