package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.handleBadRequest;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.setMessage;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.dealer.CreateVendorParam;
import com.bnbneeds.app.model.dealer.UpdateVendorParam;
import com.bnbneeds.app.model.dealer.VendorInfoResponse;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.PipelineJobUtils;
import com.bnbneeds.portal.dealer.controller.util.ReviewUtility;
import com.bnbneeds.portal.dealer.formbean.UpdateVendorInfoBean;
import com.bnbneeds.portal.dealer.formbean.VendorRegistrationBean;
import com.bnbneeds.portal.dealer.formbean.mapper.DealerFormBeanMapper;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = VendorOperationsController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('VENDOR')")
public class VendorOperationsController extends DealerController {

	private static final Logger logger = LoggerFactory.getLogger(VendorOperationsController.class);

	protected static final String BASE_REQUEST_PATH = "/vendors";

	private static final String REGISTER_VENDOR_FORM_VIEW = "vendor-registration-form";
	private static final String UPDATE_VENDOR_FORM_VIEW = "vendor-update-form";
	private static final String VENDOR_PROFILE_VIEW = "vendor-profile";

	private static final String CREATE_VENDOR_FORM_ATTRIBURE = "vendorRegistrationFormBean";
	private static final String UPDATE_VENDOR_FORM_ATTRIBURE = "vendorUpdateFormBean";

	private static final String VENDOR_INFO_ATTRIBUTE = "vendorInfo";

	static final String VENDOR_REGISTRATION_REQUEST_FORM_URI = BASE_REQUEST_PATH + DEALER_REGISTRATION_FORM_URI;

	public static String getProfileURI() {
		return String.format("%1$s%2$s", BASE_REQUEST_PATH, DEALER_PROFILE_URI);
	}

	public static String getProfileURI(String queryParam) {
		return String.format("%1$s?%2$s", getProfileURI(), queryParam);
	}

	static void setVendorInfoAttribute(VendorInfoResponse info, Model model) {
		String description = info.getDescription();
		if (StringUtils.hasText(description)) {
			description = StringUtils.convertLineEndingsToHtml(description);
			info.setDescription(description);
		}

		String address = info.getAddress();
		if (StringUtils.hasText(address)) {
			address = StringUtils.convertLineEndingsToHtml(address);
			info.setAddress(address);
		}

		model.addAttribute(VENDOR_INFO_ATTRIBUTE, info);
	}

	@Layout(title = "Vendor Registration")
	@RequestMapping(method = RequestMethod.GET, path = DEALER_REGISTRATION_FORM_URI)
	public String getRegistrationForm(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(CREATE_VENDOR_FORM_ATTRIBURE) VendorRegistrationBean form, Model model) {

		if (StringUtils.hasText(user.getRelationshipId())) {
			return getRedirectPath(HomePageController.HOMEPAGE_URL);
		}

		return REGISTER_VENDOR_FORM_VIEW;
	}

	@Layout(title = "Vendor Registration")
	@RequestMapping(method = RequestMethod.POST, path = "/register")
	public String registerVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(CREATE_VENDOR_FORM_ATTRIBURE) @Valid VendorRegistrationBean form, BindingResult result,
			Model model) {

		if (StringUtils.hasText(user.getRelationshipId())) {
			return getRedirectPath(HomePageController.HOMEPAGE_URL);
		}

		String mobileNumber = form.getMobileNumber();

		if (!StringUtils.isPhoneNumberValid(mobileNumber)) {
			result.rejectValue("mobileNumber", null, "Mobile number is invalid.");
		}

		if (result.hasFieldErrors()) {
			return REGISTER_VENDOR_FORM_VIEW;
		}

		CreateVendorParam param = new CreateVendorParam();
		DealerFormBeanMapper.mapVendorRegistrationAttributes(form, param);

		try {
			CreateResourceResponse response = bnbneedsClient.vendors().create(param);
			user.setRelationshipId(response.getResourceRep().getId());
			addDealerToMailinglist(user);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, REGISTER_VENDOR_FORM_VIEW);
		}

		return getRedirectPath(HomePageController.HOMEPAGE_URL + "?registerSuccess");
	}

	@Layout(title = "Vendor Information")
	@RequestMapping(method = RequestMethod.GET, path = DEALER_PROFILE_URI)
	public String getVendorProfile(@AuthenticationPrincipal BNBNeedsUser user, Model model) {

		String vendorId = getDealershipId(user);

		try {
			VendorInfoResponse vendorInfoResponse = bnbneedsClient.vendors(vendorId).get();
			setVendorInfoAttribute(vendorInfoResponse, model);
			ReviewUtility.setReviewListResponse(bnbneedsClient, ReviewFor.vendor, vendorId, null, null, model);
			ReviewUtility.setAverageRatingResponse(bnbneedsClient, ReviewFor.vendor, vendorId, model);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, HomePageController.VENDOR_HOME_VIEW);
		}

		return VENDOR_PROFILE_VIEW;

	}

	@Layout(title = "Update Vendor Information")
	@RequestMapping(method = RequestMethod.GET, path = "/update-form")
	public String getUpdateVendorForm(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(UPDATE_VENDOR_FORM_ATTRIBURE) UpdateVendorInfoBean form, Model model) {

		String vendorId = getDealershipId(user);

		try {
			VendorInfoResponse vendorInfo = bnbneedsClient.vendors(vendorId).get();

			DealerFormBeanMapper.mapVendorInfoResponseToFormBean(vendorInfo, form);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, UPDATE_VENDOR_FORM_VIEW);
		}

		return UPDATE_VENDOR_FORM_VIEW;
	}

	@Layout(title = "Update Vendor Information")
	@RequestMapping(method = RequestMethod.POST, path = "/update")
	public String updateVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(UPDATE_VENDOR_FORM_ATTRIBURE) @Valid UpdateVendorInfoBean form, BindingResult result,
			Model model) {

		String vendorId = getDealershipId(user);

		String mobileNumber = form.getMobileNumber();

		if (!StringUtils.isPhoneNumberValid(mobileNumber)) {
			result.rejectValue("mobileNumber", null, "Mobile number is invalid.");
		}

		if (result.hasFieldErrors()) {
			return UPDATE_VENDOR_FORM_VIEW;
		}

		UpdateVendorParam param = new UpdateVendorParam();
		DealerFormBeanMapper.mapVendorUpadateAttributes(form, param);

		try {
			bnbneedsClient.vendors(vendorId).update(param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, UPDATE_VENDOR_FORM_VIEW);
		}

		setMessage("Your Organization has been successfully updated.", model);
		return HomePageController.VENDOR_HOME_VIEW;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/delete")
	public String deleteVendor(@AuthenticationPrincipal BNBNeedsUser user, Model model) throws InterruptedException {

		String vendorId = getDealershipId(user);

		try {
			VendorInfoResponse vendorInfo = bnbneedsClient.vendors(vendorId).get();
			setVendorInfoAttribute(vendorInfo, model);
			CreateResourceResponse jobInfo = bnbneedsClient.vendors(vendorId).deactivate(true);
			String jobId = jobInfo.getResourceRep().getId();
			PipelineJobUtils.pollTillJobCompletes(bnbneedsClient, jobId, 2);
			user.setRelationshipId(null);
			removeDealerFromMailinglist(user.getUsername());
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_PROFILE_VIEW);
		}
		return getRedirectPath(HomePageController.HOMEPAGE_URL);
	}

}
