package com.bnbneeds.portal.dealer.controller;

import java.net.ConnectException;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.formbean.UserRegistrationBean;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.ConnectionUtils;
import com.bnbneeds.rest.client.ClientConfig;

@Controller
public class WelcomePageController extends AbstractBaseController {

	public static final String WELCOME_PAGE_VIEW = "welcome";

	@Layout(title = "Welcome")
	@RequestMapping(method = RequestMethod.GET, path = { "", "/", AuthenticationController.LOGIN_URL })
	public String welcome(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(UserAccountManagementController.CREATE_ACCOUNT_FORM_ATTRIBURE) UserRegistrationBean form,
			Model model) throws ConnectException {

		ClientConfig config = bnbneedsClient.getConfig();

		ConnectionUtils.checkServerConnection(config.getHost(), config.getPort());

		if (user == null) {
			UserAccountManagementController.loadDealerTypes(model);
			return WELCOME_PAGE_VIEW;
		}
		return getRedirectPath(HomePageController.HOMEPAGE_URL);

	}
}
