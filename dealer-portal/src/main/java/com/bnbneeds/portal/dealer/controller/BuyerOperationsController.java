package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.handleBadRequest;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.setMessage;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.EntityInfoListResponse;
import com.bnbneeds.app.model.EntityInfoResponse;
import com.bnbneeds.app.model.dealer.BuyerInfoResponse;
import com.bnbneeds.app.model.dealer.CreateBuyerParam;
import com.bnbneeds.app.model.dealer.UpdateBuyerParam;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.PipelineJobUtils;
import com.bnbneeds.portal.dealer.controller.util.ReviewUtility;
import com.bnbneeds.portal.dealer.formbean.CreateBuyerBean;
import com.bnbneeds.portal.dealer.formbean.UpdateBuyerInfoBean;
import com.bnbneeds.portal.dealer.formbean.mapper.DealerFormBeanMapper;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = BuyerOperationsController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('BUYER')")
public class BuyerOperationsController extends DealerController {

	private static final Logger logger = LoggerFactory.getLogger(BuyerOperationsController.class);

	protected static final String BASE_REQUEST_PATH = "/buyers";

	private static final String REGISTER_BUYER_FORM_VIEW = "buyer-registration-form";
	private static final String UPDATE_BUYER_FORM_VIEW = "buyer-update-form";
	private static final String BUYER_PROFILE_VIEW = "buyer-profile";

	private static final String CREATE_BUYER_FORM_ATTRIBURE = "buyerRegistrationFormBean";
	private static final String UPDATE_BUYER_FORM_ATTRIBURE = "buyerUpdateFormBean";

	private static final String BUSINESS_TYPE_LIST_ATTRIBUTE = "businessTypes";
	private static final String BUYER_INFO_ATTRIBUTE = "buyerInfo";

	static final String BUYER_REGISTRATION_REQUEST_FORM_URI = BASE_REQUEST_PATH + DEALER_REGISTRATION_FORM_URI;

	public static String getProfileURI() {
		return String.format("%1$s%2$s", BASE_REQUEST_PATH, DEALER_PROFILE_URI);
	}

	public static String getProfileURI(String queryParam) {
		return String.format("%1$s?%2$s", getProfileURI(), queryParam);
	}

	static void setBusinessTypes(BNBNeedsClient bnbneedsClient, Model model) {

		List<EntityInfoResponse> businessTypes = new ArrayList<>();
		try {
			EntityInfoListResponse infolistResponse = bnbneedsClient.businessTypes().listBulk(RESULT_FETCH_SIZE,
					APPROVED_ENTITY_QUERY_MAP);
			List<EntityInfoResponse> entityList = infolistResponse.getEntityInfoList();
			if (entityList != null) {
				businessTypes.addAll(entityList);
				String offset = infolistResponse.getNextOffset();
				String nextOffset = null;
				while (StringUtils.hasText(offset)) {
					infolistResponse = bnbneedsClient.businessTypes().listBulk(RESULT_FETCH_SIZE,
							APPROVED_ENTITY_QUERY_MAP, offset);
					if (infolistResponse != null) {
						entityList = infolistResponse.getEntityInfoList();
						if (entityList != null) {
							businessTypes.addAll(entityList);
						}
						nextOffset = infolistResponse.getNextOffset();
						if (nextOffset.equals(offset)) {
							break;
						} else {
							offset = nextOffset;
						}
					}
				}
			}
			model.addAttribute(BUSINESS_TYPE_LIST_ATTRIBUTE, businessTypes);
		} catch (BNBNeedsServiceException e) {
			throw e;
		}
	}

	static void setBuyerInfoAttribute(BuyerInfoResponse info, Model model) {
		String description = info.getDescription();
		if (StringUtils.hasText(description)) {
			description = StringUtils.convertLineEndingsToHtml(description);
			info.setDescription(description);
		}

		String address = info.getAddress();
		if (StringUtils.hasText(address)) {
			address = StringUtils.convertLineEndingsToHtml(address);
			info.setAddress(address);
		}

		model.addAttribute(BUYER_INFO_ATTRIBUTE, info);
	}

	@Layout(title = "Buyer Registration")
	@RequestMapping(method = RequestMethod.GET, path = DEALER_REGISTRATION_FORM_URI)
	public String getRegistrationForm(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(CREATE_BUYER_FORM_ATTRIBURE) CreateBuyerBean form, Model model) {

		if (user.hasRelationshipId()) {
			return getRedirectPath(HomePageController.HOMEPAGE_URL);
		}

		try {
			BuyerOperationsController.setBusinessTypes(bnbneedsClient, model);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, REGISTER_BUYER_FORM_VIEW);
		}

		return REGISTER_BUYER_FORM_VIEW;
	}

	@Layout(title = "Buyer Registration")
	@RequestMapping(method = RequestMethod.POST, path = "/register")
	public String registerBuyer(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(CREATE_BUYER_FORM_ATTRIBURE) @Valid CreateBuyerBean form, BindingResult result,
			Model model) {

		if (StringUtils.hasText(user.getRelationshipId())) {
			return getRedirectPath(HomePageController.HOMEPAGE_URL);
		}

		String mobileNumber = form.getMobileNumber();

		if (!StringUtils.isPhoneNumberValid(mobileNumber)) {
			result.rejectValue("mobileNumber", null, "Mobile number is invalid.");
		}

		try {
			BuyerOperationsController.setBusinessTypes(bnbneedsClient, model);
			if (result.hasFieldErrors()) {
				return REGISTER_BUYER_FORM_VIEW;
			}

			CreateBuyerParam param = new CreateBuyerParam();
			DealerFormBeanMapper.mapBuyerRegistrationAttributes(form, param);

			CreateResourceResponse response = bnbneedsClient.buyers().create(param);
			user.setRelationshipId(response.getResourceRep().getId());
			addDealerToMailinglist(user);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, REGISTER_BUYER_FORM_VIEW);
		}

		return getRedirectPath(HomePageController.HOMEPAGE_URL + "?registerSuccess");
	}

	@Layout(title = "Buyer Information")
	@RequestMapping(method = RequestMethod.GET, path = DEALER_PROFILE_URI)
	public String getBuyerProfile(@AuthenticationPrincipal BNBNeedsUser user, Model model) {

		String buyerId = getDealershipId(user);

		try {
			BuyerInfoResponse buyerInfoResponse = bnbneedsClient.buyers(buyerId).get();
			setBuyerInfoAttribute(buyerInfoResponse, model);
			ReviewUtility.setAverageRatingResponse(bnbneedsClient, ReviewFor.buyer, buyerId, model);
			ReviewUtility.setReviewListResponse(bnbneedsClient, ReviewFor.buyer, buyerId, null, null, model);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, HomePageController.BUYER_HOME_VIEW);
		}

		return BUYER_PROFILE_VIEW;

	}

	@Layout(title = "Update Buyer Information")
	@RequestMapping(method = RequestMethod.GET, path = "/update-form")
	public String getUpdateBuyerForm(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(UPDATE_BUYER_FORM_ATTRIBURE) UpdateBuyerInfoBean form, Model model) {

		String buyerId = getDealershipId(user);

		try {
			BuyerInfoResponse buyerInfo = bnbneedsClient.buyers(buyerId).get();

			DealerFormBeanMapper.mapBuyerInfoResponseToFormBean(buyerInfo, form);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, UPDATE_BUYER_FORM_VIEW);
		}

		return UPDATE_BUYER_FORM_VIEW;
	}

	@Layout(title = "Update Buyer Information")
	@RequestMapping(method = RequestMethod.POST, path = "/update")
	public String updateBuyer(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(UPDATE_BUYER_FORM_ATTRIBURE) @Valid UpdateBuyerInfoBean form, BindingResult result,
			Model model) {

		String buyerId = getDealershipId(user);

		String mobileNumber = form.getMobileNumber();

		if (!StringUtils.isPhoneNumberValid(mobileNumber)) {
			result.rejectValue("mobileNumber", null, "Mobile number is invalid.");
		}

		if (result.hasFieldErrors()) {
			return UPDATE_BUYER_FORM_VIEW;
		}

		UpdateBuyerParam param = new UpdateBuyerParam();
		DealerFormBeanMapper.mapBuyerUpdateAttributes(form, param);

		try {
			bnbneedsClient.buyers(buyerId).update(param);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, UPDATE_BUYER_FORM_VIEW);
		}

		setMessage("Your Organization has been successfully updated.", model);
		return HomePageController.BUYER_HOME_VIEW;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/delete")
	public String deleteBuyer(@AuthenticationPrincipal BNBNeedsUser user, Model model) throws InterruptedException {
		String buyerId = getDealershipId(user);
		try {
			BuyerInfoResponse buyerInfo = bnbneedsClient.buyers(buyerId).get();
			setBuyerInfoAttribute(buyerInfo, model);
			CreateResourceResponse jobInfo = bnbneedsClient.buyers(buyerId).deactivate(true);
			String jobId = jobInfo.getResourceRep().getId();
			PipelineJobUtils.pollTillJobCompletes(bnbneedsClient, jobId, 2);
			user.setRelationshipId(null);
			removeDealerFromMailinglist(user.getUsername());
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, BUYER_PROFILE_VIEW);
		}
		return getRedirectPath(HomePageController.HOMEPAGE_URL);
	}

}
