package com.bnbneeds.portal.dealer.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.bnbneeds.portal.dealer.bean.ApplicationBeans;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.Constants;

@ControllerAdvice
public class ControllerSetup {

	private static final String LOGGED_IN_USER_ATTRIBUTE = "loggedInUser";
	private static final String BRAND_NAME_ATTRIBUTE = "brandName";
	private static final String VERSION_ATTRIBUTE = "version";
	private static final String APPLICATION_BEANS_ATTRIBUTE = "applicationBeans";

	@Value("${version}")
	private String version;

	@Autowired
	private ApplicationBeans applicationBeans;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		StringTrimmerEditor stringtrimmer = new StringTrimmerEditor(true);
		binder.registerCustomEditor(String.class, stringtrimmer);
	}

	@ModelAttribute(LOGGED_IN_USER_ATTRIBUTE)
	public BNBNeedsUser getLoggedInUser(@AuthenticationPrincipal BNBNeedsUser user) {
		return user;

	}

	@ModelAttribute(BRAND_NAME_ATTRIBUTE)
	public String getBrandName() {
		return Constants.PRODUCT_BRAND_NAME;
	}

	@ModelAttribute(VERSION_ATTRIBUTE)
	public String getVersion() {
		return version;
	}

	@ModelAttribute(APPLICATION_BEANS_ATTRIBUTE)
	public ApplicationBeans getApplicationBeans() {
		return applicationBeans;
	}
}
