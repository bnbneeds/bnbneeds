package com.bnbneeds.portal.dealer.util;

import com.bnbneeds.portal.dealer.security.BNBNeedsUser;

public class ReportFileUtils {

	public static String generateReportFilePath(String reportsDirectoryPath,
			BNBNeedsUser user, String reportName, String date, String extention) {
		String path = String.format("%1$s/%2$s_%3$s_%4$s.%5$s",
				reportsDirectoryPath, user.getUsername(), reportName, date,
				extention);
		return path.replaceAll("\\s+", "_").toLowerCase();
	}

	public static String generatePDFReportFilePath(String reportsDirectoryPath,
			BNBNeedsUser user, String reportName, String date) {

		return generateReportFilePath(reportsDirectoryPath, user, reportName,
				date, "pdf");
	}

}
