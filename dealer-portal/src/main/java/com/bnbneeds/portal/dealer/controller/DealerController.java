package com.bnbneeds.portal.dealer.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;

import com.bnbneeds.portal.dealer.controller.util.DealerUtils;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.mailgun.rest.client.MailgunClient;
import com.mailgun.rest.client.model.AddMailingListMemberParam;

public abstract class DealerController extends AbstractBaseController {

	private static final Logger logger = LoggerFactory.getLogger(DealerController.class);

	@Autowired
	protected MailgunClient mailgunClient;

	@Value("${email.address.all-dealers}")
	protected String dealersMailingListAddress;

	@Value("${email.address.buyers}")
	protected String buyersMailingListAddress;

	@Value("${email.address.vendors}")
	protected String vendorsMailingListAddress;

	private static final String DATE_RANGE_DELIMITER = "to";
	private static final String DEALER_EMAIL_UPSERT = "yes";
	protected static final String DEALER_REGISTRATION_FORM_URI = "/register-form";
	protected static final String DEALER_PROFILE_URI = "/profile";

	protected static final Map<String, String> APPROVED_ENTITY_QUERY_MAP = new HashMap<>();
	static {
		APPROVED_ENTITY_QUERY_MAP.put("status", "approved");
	}

	protected String getDealershipId(BNBNeedsUser user) {
		return DealerUtils.checkAndReturnRelationshipId(user);
	}

	protected void setSelectedDealer(Model model, String dealer) {
		model.addAttribute("selectedDealer", dealer);
	}

	protected String[] getRequestStartAndEndDate(String dateRange) {
		if (dateRange == null) {
			return null;
		}
		String[] dateRangeArray = dateRange.split(DATE_RANGE_DELIMITER);
		return dateRangeArray;
	}

	protected void addDealerToMailinglist(BNBNeedsUser param) {
		try {
			AddMailingListMemberParam memberParam = new AddMailingListMemberParam();
			memberParam.setName(param.getAccountName());
			memberParam.setAddress(param.getUsername());
			memberParam.setUpsert(DEALER_EMAIL_UPSERT);
			mailgunClient.addMailingListMember(memberParam, dealersMailingListAddress);
			if (this instanceof BuyerOperationsController) {
				mailgunClient.addMailingListMember(memberParam, buyersMailingListAddress);
			} else if (this instanceof VendorOperationsController) {
				mailgunClient.addMailingListMember(memberParam, vendorsMailingListAddress);
			}
		} catch (Exception e) {
			logger.error("Adding dealer email id: {} to mailing list failed with error message: {}",
					param.getUsername(), e.getMessage());
		}
	}

	protected void removeDealerFromMailinglist(String dealerEmailAddress) {
		try {
			mailgunClient.deleteMailingListMember(dealersMailingListAddress, dealerEmailAddress);
			if (this instanceof BuyerOperationsController) {
				mailgunClient.deleteMailingListMember(buyersMailingListAddress, dealerEmailAddress);
			} else if (this instanceof VendorOperationsController) {
				mailgunClient.deleteMailingListMember(vendorsMailingListAddress, dealerEmailAddress);
			}
		} catch (Exception e) {
			logger.error("Deleting dealer email id: {} from mailing list failed with error message: {}",
					dealerEmailAddress, e.getMessage());
		}
	}

}
