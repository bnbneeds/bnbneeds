package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.handleBadRequest;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.writeResponse;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.dealer.BuyerListResponse;
import com.bnbneeds.app.model.dealer.order.AggregatedPurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemInfoResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.PurchaseItemParam.PurchaseStatus;
import com.bnbneeds.app.model.dealer.order.PurchaseListResponse;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.DealerUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.formbean.VendorAggregatedPurchaseListBean;
import com.bnbneeds.portal.dealer.formbean.VendorPurchaseItemUpdateBean;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.Constants;
import com.bnbneeds.portal.dealer.util.ReportFileUtils;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.portal.reports.PDFGenerator;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = VendorPurchaseListController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('VENDOR')")
public class VendorPurchaseListController extends PurchaseListController {

	private static final Logger logger = LoggerFactory.getLogger(VendorPurchaseListController.class);

	protected static final String BASE_REQUEST_PATH = "/vendors/purchase-items";
	private static final String PURCHASE_ITEM_UPDATE_FORM_URI = BASE_REQUEST_PATH + "/%s/update-form";

	private static final String BUYER_NAME_LIST_RESPONSE = "buyerListResponse";

	private static final String AGGREGATED_PURCHASE_LIST_VIEW = "vendor-aggregated-purchase-list";

	private static final String PURCHASE_LIST_VIEW = "vendor-purchase-list";
	private static final String PURCHASE_ITEM_STATUS_UPDATE_FORM_VIEW = "vendor-purchase-item-update-form";
	private static final int PURCHASE_LIST_RESULT_SIZE = 20;

	private static final Map<String, String> STATUS_MAP = new HashMap<>();

	static {
		STATUS_MAP.put("Cannot Deliver", "CANNOT_BE_DELIVERED");
		STATUS_MAP.put("Delivered", "DELIVERED");
		STATUS_MAP.put("Partially Delivered", "PARTIALLY_DELIVERED");
	}

	public static String getPurchaseListURI() {
		return BASE_REQUEST_PATH;
	}

	public static String getPurchaseListURI(String requestDate) {
		return BASE_REQUEST_PATH + String.format("?requestDate=%s", requestDate);
	}

	public static String getPurchaseUpdateFormView(String ItemId) {
		return String.format(PURCHASE_ITEM_UPDATE_FORM_URI, ItemId);
	}

	static void setPurchaseItemStatusMap(Model model) {
		model.addAttribute(PURCHASE_ITEM_STATUS_MAP_ATTRIBUTE, STATUS_MAP);
	}

	@Layout(title = "Purchase List")
	@RequestMapping(method = RequestMethod.GET)
	public String getPurchaseList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "buyer", required = false) String buyer,
			@RequestParam(name = REQ_PARAM_REQUEST_DATE, required = false) String requestDate,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset, Model model) {

		String vendorId = getDealershipId(user);

		Map<String, String> queryParams = new HashMap<>();
		PurchaseListResponse listResponse = null;

		try {
			if (StringUtils.hasText(buyer)) {
				if (buyer.contains("|")) {
					String[] nameAndIdArr = buyer.split("\\|");
					queryParams.put("buyerId", nameAndIdArr[1].trim());
					setSelectedDealer(model, buyer);
				}
			}

			if (StringUtils.hasText(requestDate)) {
				if (StringUtils.isDateFormatValid(requestDate, Constants.DATE_FORMAT)) {
					queryParams.put("requestDate", requestDate);
					model.addAttribute(REQ_PARAM_REQUEST_DATE, requestDate);
				}
			}

			if (!queryParams.isEmpty()) {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = bnbneedsClient.vendors(vendorId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE,
							queryParams, nextOffset);
				} else {
					listResponse = bnbneedsClient.vendors(vendorId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE,
							queryParams);
				}
			} else {
				if (StringUtils.hasText(nextOffset)) {
					listResponse = bnbneedsClient.vendors(vendorId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE,
							nextOffset);
				} else {
					listResponse = bnbneedsClient.vendors(vendorId).getPurchaseList(PURCHASE_LIST_RESULT_SIZE);
				}
			}
			setPurchaseListResponse(model, listResponse);

		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PURCHASE_LIST_VIEW);
		}

		return PURCHASE_LIST_VIEW;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/pdf")
	public void writePurchaseListToPdf(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "buyer", required = false) String buyer,
			@RequestParam(name = REQ_PARAM_REQUEST_DATE, required = false) String requestDate, Model model,
			HttpServletResponse response) throws Exception {

		String vendorId = user.getRelationshipId();
		String date = StringUtils.getDateTimestamp(Constants.DATE_FORMAT, new Date().getTime());

		String destinationFilePath = ReportFileUtils.generatePDFReportFilePath(reportsDirectoryPath, user,
				PDFGenerator.PURCHASE_LIST_REPORT_NAME, date);

		Map<String, String> queryParams = new HashMap<>();

		try {
			if (StringUtils.hasText(buyer)) {
				if (buyer.contains("|")) {
					String[] nameAndIdArr = buyer.split("\\|");
					queryParams.put("buyerId", nameAndIdArr[1].trim());
					setSelectedDealer(model, buyer);
				}
			}

			if (StringUtils.hasText(requestDate)) {
				if (StringUtils.isDateFormatValid(requestDate, Constants.DATE_FORMAT)) {
					queryParams.put("requestDate", requestDate);
				}
			}

			pdfGenerator.generateVendorPurchaseListReport(vendorId, queryParams, destinationFilePath);
			RequestResponseUtils.writeFileToResponse(destinationFilePath, RequestResponseUtils.MEDIA_TYPE_PDF,
					response);

		} catch (BNBNeedsServiceException e) {
			logger.error("There was a problem occurred while generating Buyer purchase list report.", e);
		} finally {
			FileUtils.deleteQuietly(new File(destinationFilePath));
		}

	}

	@Layout(title = "Update Purchase Item")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/update-form")
	public String getPurchaseItemStatusUpdateForm(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String itemId,
			@ModelAttribute(PURCHASE_ITEM_UPDATE_FORM_ATTRIBUTE) VendorPurchaseItemUpdateBean form, Model model) {

		String vendorId = getDealershipId(user);

		setPurchaseItemStatusMap(model);

		try {
			PurchaseItemInfoResponse infoResponse = bnbneedsClient.vendors(vendorId).getPurchaseItem(itemId);
			setPurchaseInfoResponse(model, infoResponse);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PURCHASE_ITEM_STATUS_UPDATE_FORM_VIEW);
		}

		return PURCHASE_ITEM_STATUS_UPDATE_FORM_VIEW;

	}

	@Layout(title = "Update Purchase Item")
	@RequestMapping(method = RequestMethod.POST, path = "/{id}/update")
	public String updatePurchaseItem(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String itemId,
			@ModelAttribute(PURCHASE_ITEM_UPDATE_FORM_ATTRIBUTE) VendorPurchaseItemUpdateBean form,
			BindingResult result, Model model) {

		String vendorId = getDealershipId(user);
		setPurchaseItemStatusMap(model);
		try {
			PurchaseItemInfoResponse infoResponse = bnbneedsClient.vendors(vendorId).getPurchaseItem(itemId);
			setPurchaseInfoResponse(model, infoResponse);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PURCHASE_ITEM_STATUS_UPDATE_FORM_VIEW);
		}

		String deliveredQuantity = form.getDeliveredQuantity();

		if (StringUtils.hasText(deliveredQuantity) && !StringUtils.isNumber(deliveredQuantity)) {
			result.rejectValue("deliveredQuantity", null, "Quantity is invalid.");
		}

		if (!result.hasErrors()) {
			PurchaseItemParam param = new PurchaseItemParam();
			if (StringUtils.hasText(deliveredQuantity)) {
				param.setDeliveredQuantity(Double.parseDouble(deliveredQuantity));
			}
			PurchaseStatus statusUpdate = new PurchaseStatus();
			statusUpdate.setStatus(form.getStatus());
			statusUpdate.setComments(form.getComment());
			param.setStatusUpdate(statusUpdate);
			try {
				bnbneedsClient.vendors(vendorId).updatePurchaseItem(itemId, param);
			} catch (BNBNeedsServiceException e) {
				return handleBadRequest(e, model, PURCHASE_ITEM_STATUS_UPDATE_FORM_VIEW);
			}
		}
		return getRedirectPath(getPurchaseUpdateFormView(itemId) + "?updateSuccess");
	}

	static void setBuyerListResponse(Model model, BuyerListResponse response) {
		model.addAttribute(BUYER_NAME_LIST_RESPONSE, response);
	}

	@Layout(title = "Aggregated List")
	@RequestMapping(method = RequestMethod.GET, path = "/aggregated-list-form")
	public String getAggregatedListForm(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(AGGREGATED_PURCHASE_LIST_FORM_ATTRIBUTE) VendorAggregatedPurchaseListBean form) {

		DealerUtils.checkHasDealership(user);

		return AGGREGATED_PURCHASE_LIST_VIEW;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/product-names")
	@ResponseBody
	public synchronized void writeAssociatedProductNameList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_REQUEST_DATE, required = false) String requestDate,
			@RequestParam(name = REQ_PARAM_REQUEST_DATE_RANGE, required = false) String requestDateRange,
			HttpServletResponse response) throws IOException, InterruptedException {

		String productNameListResponse = null;
		try {
			String vendorId = getDealershipId(user);

			if (StringUtils.hasText(requestDate)) {
				if (!StringUtils.isDateFormatValid(requestDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"Request date format is invalid.", response);
					return;
				}

				productNameListResponse = bnbneedsClient.vendors(vendorId)
						.getAssociatedProductNameListOfPurchaseItemsAsString(requestDate);
			} else {
				// Date range

				String[] dateRangeArray = getRequestStartAndEndDate(requestDateRange);
				String startRequestDate = dateRangeArray[0].trim();
				String endRequestDate = dateRangeArray[1].trim();

				if (!StringUtils.isDateFormatValid(startRequestDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"Start date format is invalid.", response);
					return;
				}

				if (!StringUtils.isDateFormatValid(endRequestDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"End date format is invalid.", response);
					return;
				}

				productNameListResponse = bnbneedsClient.vendors(vendorId)
						.getAssociatedProductNameListOfPurchaseItemsByDateRangeAsString(startRequestDate,
								endRequestDate);

			}

			writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE, productNameListResponse, response);

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/buyers")
	@ResponseBody
	public synchronized void writeAssociatedBuyerList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_REQUEST_DATE, required = false) String requestDate,
			@RequestParam(name = REQ_PARAM_REQUEST_DATE_RANGE, required = false) String requestDateRange,
			HttpServletResponse response) throws IOException, InterruptedException {

		try {
			String buyerListResponse = null;
			String vendorId = getDealershipId(user);
			if (StringUtils.hasText(requestDate)) {
				if (!StringUtils.isDateFormatValid(requestDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"Request date format is invalid.", response);
					return;
				}
				buyerListResponse = bnbneedsClient.vendors(vendorId)
						.getAssociatedBuyerListOfPurchaseItemsAsString(requestDate);
			} else {
				// Date range
				String[] dateRangeArray = getRequestStartAndEndDate(requestDateRange);
				String startRequestDate = dateRangeArray[0].trim();
				String endRequestDate = dateRangeArray[1].trim();

				if (!StringUtils.isDateFormatValid(startRequestDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"Start date format is invalid.", response);
					return;
				}

				if (!StringUtils.isDateFormatValid(endRequestDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"End date format is invalid.", response);
					return;
				}

				buyerListResponse = bnbneedsClient.vendors(vendorId)
						.getAssociatedBuyerListOfPurchaseItemsByDateRangeAsString(startRequestDate, endRequestDate);
			}

			writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE, buyerListResponse, response);

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@Layout(title = "Aggregated List")
	@RequestMapping(method = RequestMethod.GET, path = "/aggregated-list")
	public String getAggregatedList(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(AGGREGATED_PURCHASE_LIST_FORM_ATTRIBUTE) VendorAggregatedPurchaseListBean form,
			BindingResult result, Model model, HttpServletResponse response) throws Exception {

		String vendorId = getDealershipId(user);
		boolean dateRangeGiven = false;
		String startRequestDate = null;
		String endRequestDate = null;
		String buyerId = form.getBuyerId();
		String productNameId = form.getProductNameId();
		String requestDate = form.getRequestDate();

		String date = StringUtils.getDateTimestamp(Constants.DATE_FORMAT, new Date().getTime());

		String destinationFilePath = ReportFileUtils.generatePDFReportFilePath(reportsDirectoryPath, user,
				PDFGenerator.AGGREGATED_PURCHASE_LIST_REPORT_NAME, date);

		if (StringUtils.hasText(form.getRequestDate())) {
			if (!StringUtils.isDateFormatValid(form.getRequestDate(), Constants.DATE_FORMAT)) {
				result.rejectValue(REQ_PARAM_REQUEST_DATE, null, "Request date is invalid.");
			}
		} else {
			dateRangeGiven = true;
			String[] dateRangeArray = getRequestStartAndEndDate(form.getRequestDateRange());
			startRequestDate = dateRangeArray[0].trim();
			endRequestDate = dateRangeArray[1].trim();

			if (!StringUtils.isDateFormatValid(startRequestDate, Constants.DATE_FORMAT)) {
				result.rejectValue(REQ_PARAM_REQUEST_DATE_RANGE, null, "Start date format is invalid.");
			}

			if (!StringUtils.isDateFormatValid(endRequestDate, Constants.DATE_FORMAT)) {
				result.rejectValue(REQ_PARAM_REQUEST_DATE_RANGE, null, "End date format is invalid.");
			}
		}
		AggregatedPurchaseListResponse reponse = null;
		if (!result.hasErrors()) {

			try {
				if (form.isPdf()) {

					pdfGenerator.generateVendorAggregatedPurchaseListReport(vendorId, buyerId, startRequestDate,
							endRequestDate, requestDate, productNameId, destinationFilePath);
					RequestResponseUtils.writeFileToResponse(destinationFilePath, RequestResponseUtils.MEDIA_TYPE_PDF,
							response);
					return null;

				}

				else {
					if (dateRangeGiven) {
						reponse = bnbneedsClient.vendors(vendorId).getAggregatedPurchaseListByDateRange(
								startRequestDate, endRequestDate, form.getProductNameId(), form.getBuyerId());

					} else {
						reponse = bnbneedsClient.vendors(vendorId).getAggregatedPurchaseList(form.getRequestDate(),
								form.getProductNameId(), form.getBuyerId());

					}
				}

				setAggregatedPurchaseListResponse(model, reponse);

			} catch (BNBNeedsServiceException e) {
				return handleBadRequest(e, model, AGGREGATED_PURCHASE_LIST_VIEW);
			} finally {
				FileUtils.deleteQuietly(new File(destinationFilePath));
			}
		}

		return AGGREGATED_PURCHASE_LIST_VIEW;
	}
}
