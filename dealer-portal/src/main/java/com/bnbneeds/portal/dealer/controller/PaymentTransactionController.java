package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.DealerUtils.checkHasDealership;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.model.DataObjectInfoListResponse;
import com.bnbneeds.app.model.dealer.PaymentTransactionInfoResponse;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.RazorpayUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.core.PaymentTransactions;
import com.bnbneeds.rest.client.core.PaymentTransactions.PaymentTransactionOf;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

public abstract class PaymentTransactionController extends AbstractBaseController {

	private static final String PAYMENT_TRANSACTION_LIST_VIEW = "payment-transaction-list";
	private static final String PAYMENT_RECEIPT_VIEW = "payment-receipt";
	private static final String LIST_RESPONSE_ATTRIBUTE = "listResponse";
	private static final String PAYMENT_INFO_ATTRIBUTE = "paymentInfo";
	private static final String LIST_PAYMENT_TRANSACTIONS_URI = "/list";
	protected static final String LIST_VIEW_TITLE = "Payment Transactions";

	@Autowired
	private RazorpayClient razorpayClient;

	public abstract PaymentTransactionOf getPaymentTransactionOf();

	private PaymentTransactions getClientHandle(BNBNeedsUser user) {
		String dealerId = user.getRelationshipId();
		PaymentTransactionOf dealerType = getPaymentTransactionOf();
		switch (dealerType) {
		case buyer:
			return bnbneedsClient.buyerPaymentTransactions(dealerId);
		case vendor:
			return bnbneedsClient.vendorPaymentTransactions(dealerId);
		default:
			return null;
		}
	}

	private void setModelForPaymentTransactionListView(BNBNeedsUser user, String nextOffset, Model model) {

		PaymentTransactions paymentTransactionsClient = getClientHandle(user);

		try {
			DataObjectInfoListResponse<PaymentTransactionInfoResponse> listResponse = null;

			if (StringUtils.hasText(nextOffset)) {
				listResponse = paymentTransactionsClient.list(RESULT_FETCH_SIZE, nextOffset);
			} else {
				listResponse = paymentTransactionsClient.list(RESULT_FETCH_SIZE);
			}
			model.addAttribute(LIST_RESPONSE_ATTRIBUTE, listResponse);
		} catch (BNBNeedsServiceException e) {
			throw e;
		}
	}

	@Layout(title = LIST_VIEW_TITLE)
	@RequestMapping(method = RequestMethod.GET, path = LIST_PAYMENT_TRANSACTIONS_URI)
	public String listBiddingSubscriptions(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset, Model model) {
		checkHasDealership(user);
		try {
			setModelForPaymentTransactionListView(user, nextOffset, model);
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, PAYMENT_TRANSACTION_LIST_VIEW);
		}
		return PAYMENT_TRANSACTION_LIST_VIEW;
	}

	@Layout(title = "Payment Receipt")
	@RequestMapping(method = RequestMethod.GET, path = "/{transactionId}/receipt")
	public String getPaymentReceipt(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("transactionId") String transactionId, Model model) {
		checkHasDealership(user);
		try {
			PaymentTransactionInfoResponse paymentInfo = getClientHandle(user).get(transactionId);
			String paymentId = paymentInfo.getTransactionId();
			Payment razorpayPaymentInfo = razorpayClient.Payments.fetch(paymentId);
			model.addAttribute(PAYMENT_INFO_ATTRIBUTE, razorpayPaymentInfo);
			return PAYMENT_RECEIPT_VIEW;
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, PAYMENT_RECEIPT_VIEW);
		} catch (RazorpayException e) {
			RazorpayUtils.setErrorMessage(e, model);
			return PAYMENT_RECEIPT_VIEW;
		}
	}
}
