package com.bnbneeds.portal.dealer.formbean;

public class ReviewBean {

	private String reviewId;
	private int rating;
	private String headline;
	private String reviewDescription;

	public int getRating() {
		return rating;
	}

	public String getHeadline() {
		return headline;
	}

	public String getReviewDescription() {
		return reviewDescription;
	}

	public void setReviewDescription(String reviewDescription) {
		this.reviewDescription = reviewDescription;
	}

	public String getReviewId() {
		return reviewId;
	}

	public void setReviewId(String reviewId) {
		this.reviewId = reviewId;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

}
