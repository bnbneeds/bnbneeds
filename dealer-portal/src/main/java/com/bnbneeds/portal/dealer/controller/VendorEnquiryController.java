package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.handleBadRequest;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.writeResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.dealer.EnquiryInfoListResponse;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.formbean.VendorEnquiryListRequestBean;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.Constants;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = VendorEnquiryController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('VENDOR')")
public class VendorEnquiryController extends DealerController {

	protected static final String BASE_REQUEST_PATH = "/vendors/product-enquiries";
	private static final int FETCH_SIZE = 20;
	private static final String VENDOR_ENQUIRY_LIST_VIEW = "vendor-enquiry-list";
	private static final String VENDOR_ENQUIRY_LIST_RESPONSE_ATTRIBUTE = "vendorEnquiryListResponse";
	private static final String ENQUIRY_LIST_REQUEST_FORM_ATTRIBUTE = "enquiryListRequestForm";

	public static String getEnquiryListURI() {
		return BASE_REQUEST_PATH;
	}

	public static String getEnquiryListURI(String enquiryDate) {
		return BASE_REQUEST_PATH + String.format("?enquiryDate=%s", enquiryDate);
	}

	private static void setEnquiryListResponse(Model model, EnquiryInfoListResponse listResponse) {
		model.addAttribute(VENDOR_ENQUIRY_LIST_RESPONSE_ATTRIBUTE, listResponse);
	}

	@Layout(title = "Product Enquiries")
	@RequestMapping(method = RequestMethod.GET, path = "/list-request-form")
	public String getEnquiryListRequestForm(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(ENQUIRY_LIST_REQUEST_FORM_ATTRIBUTE) VendorEnquiryListRequestBean form,
			BindingResult result, Model model) {

		return VENDOR_ENQUIRY_LIST_VIEW;

	}

	@Layout(title = "Product Enquiries")
	@RequestMapping(method = RequestMethod.GET)
	public String getEnquiries(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset,
			@ModelAttribute(ENQUIRY_LIST_REQUEST_FORM_ATTRIBUTE) VendorEnquiryListRequestBean form,
			BindingResult result, Model model) {

		String vendorId = getDealershipId(user);

		Map<String, String> queryMap = new HashMap<>();
		if (StringUtils.hasText(form.getEnquiryDate())) {
			if (!StringUtils.isDateFormatValid(form.getEnquiryDate(), Constants.DATE_FORMAT)) {
				result.rejectValue("enquiryDate", null, "Enquiry date format is invalid.");
			}
			queryMap.put("enquiryDate", form.getEnquiryDate());
		} else {
			if (StringUtils.hasText(form.getEnquiryDateRange())) {
				String[] dateRangeArray = getRequestStartAndEndDate(form.getEnquiryDateRange());
				String startDate = dateRangeArray[0].trim();
				String endDate = dateRangeArray[1].trim();

				if (!StringUtils.isDateFormatValid(startDate, Constants.DATE_FORMAT)) {
					result.rejectValue("enquiryDateRange", null, "Start date format is invalid.");
				}

				if (!StringUtils.isDateFormatValid(endDate, Constants.DATE_FORMAT)) {
					result.rejectValue("enquiryDateRange", null, "End date format is invalid.");
				}
				queryMap.put("startEnquiryDate", startDate);
				queryMap.put("endEnquiryDate", endDate);
			}
		}

		if (StringUtils.hasText(form.getBuyer())) {
			String buyer = form.getBuyer();
			if (form.getBuyer().contains("|")) {
				String[] nameAndIdArr = buyer.split("\\|");
				queryMap.put("buyerId", nameAndIdArr[1].trim());
				setSelectedDealer(model, buyer);
			}
		}

		try {
			if (!result.hasErrors()) {
				EnquiryInfoListResponse listResponse = null;
				if (!queryMap.isEmpty()) {
					if (StringUtils.hasText(nextOffset)) {
						listResponse = bnbneedsClient.vendors(vendorId).getEnquiries(FETCH_SIZE, queryMap, nextOffset);
					} else {

						listResponse = bnbneedsClient.vendors(vendorId).getEnquiries(FETCH_SIZE, queryMap);
					}
				} else {
					if (StringUtils.hasText(nextOffset)) {
						listResponse = bnbneedsClient.vendors(vendorId).getEnquiries(FETCH_SIZE, nextOffset);
					} else {
						listResponse = bnbneedsClient.vendors(vendorId).getEnquiries(FETCH_SIZE);
					}

				}
				setEnquiryListResponse(model, listResponse);
			}
			return VENDOR_ENQUIRY_LIST_VIEW;
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, VENDOR_ENQUIRY_LIST_VIEW);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/buyers")
	@ResponseBody
	public synchronized void writeAssociatedBuyerList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "enquiryDate", required = false) String enquiryDate,
			@RequestParam(name = "enquiryDateRange", required = false) String enquiryDateRange,
			HttpServletResponse response) throws IOException, InterruptedException {

		String associatedBuyerListResponse = null;

		try {

			String vendorId = getDealershipId(user);

			if (StringUtils.hasText(enquiryDate)) {
				if (!StringUtils.isDateFormatValid(enquiryDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"Request date format is invalid.", response);
					return;
				}
				associatedBuyerListResponse = bnbneedsClient.vendors(vendorId)
						.getAssociatedBuyerListOfEnquiriesAsString(enquiryDate);
			} else {
				// Date range
				String[] dateRangeArray = getRequestStartAndEndDate(enquiryDateRange);
				String startEnquiryDate = dateRangeArray[0].trim();
				String endEnquiryDate = dateRangeArray[1].trim();

				if (!StringUtils.isDateFormatValid(startEnquiryDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"Start date format is invalid.", response);
					return;
				}

				if (!StringUtils.isDateFormatValid(endEnquiryDate, Constants.DATE_FORMAT)) {
					writeResponse(HttpServletResponse.SC_BAD_REQUEST, MediaType.TEXT_PLAIN_VALUE,
							"End date format is invalid.", response);
					return;
				}

				associatedBuyerListResponse = bnbneedsClient.vendors(vendorId)
						.getAssociatedBuyerListOfEnquiriesByDateRangeAsString(startEnquiryDate, endEnquiryDate);

			}
			writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_PLAIN_VALUE, associatedBuyerListResponse, response);

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}
}
