package com.bnbneeds.portal.dealer.formbean;

import javax.validation.constraints.DecimalMin;

public class PostBidRequestBean {

	@DecimalMin("0.0")
	private Double value;
	private String comments;
	
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

}
