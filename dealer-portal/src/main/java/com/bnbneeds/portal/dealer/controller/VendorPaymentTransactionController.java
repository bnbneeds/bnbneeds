package com.bnbneeds.portal.dealer.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bnbneeds.rest.client.core.PaymentTransactions.PaymentTransactionOf;

@Controller
@RequestMapping(path = VendorPaymentTransactionController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('VENDOR')")
public class VendorPaymentTransactionController extends PaymentTransactionController {

	protected static final String BASE_REQUEST_PATH = "/vendors/payment-transactions";

	@Override
	public PaymentTransactionOf getPaymentTransactionOf() {
		return PaymentTransactionOf.vendor;
	}

}
