package com.bnbneeds.portal.dealer.model;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.bnbneeds.app.model.notification.NotificationInfoResponse;
import com.bnbneeds.portal.dealer.controller.BusinessTypeController;
import com.bnbneeds.portal.dealer.controller.BuyerBiddingSubscriptionController;
import com.bnbneeds.portal.dealer.controller.BuyerController;
import com.bnbneeds.portal.dealer.controller.BuyerEnquiryController;
import com.bnbneeds.portal.dealer.controller.BuyerOperationsController;
import com.bnbneeds.portal.dealer.controller.BuyerPurchaseListController;
import com.bnbneeds.portal.dealer.controller.ProductCategoryController;
import com.bnbneeds.portal.dealer.controller.ProductController;
import com.bnbneeds.portal.dealer.controller.ProductNameController;
import com.bnbneeds.portal.dealer.controller.ProductTypeController;
import com.bnbneeds.portal.dealer.controller.TenderBidController;
import com.bnbneeds.portal.dealer.controller.TenderManagementController;
import com.bnbneeds.portal.dealer.controller.VendorBiddingSubscriptionController;
import com.bnbneeds.portal.dealer.controller.VendorController;
import com.bnbneeds.portal.dealer.controller.VendorEnquiryController;
import com.bnbneeds.portal.dealer.controller.VendorOperationsController;
import com.bnbneeds.portal.dealer.controller.VendorPurchaseListController;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.Constants;
import com.bnbneeds.portal.dealer.util.DateUtils;
import com.bnbneeds.portal.dealer.util.StringUtils;

public class NotificationEvent {

	private static final SimpleDateFormat EVENT_TIMESTAMP_FORMAT = new SimpleDateFormat(
			"dd MMM yyyy 'at' HH:mm");

	private static final SimpleDateFormat EVENT_ISO_TIMESTAMP_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm'Z'");
	private static final TimeZone TIME_ZONE = TimeZone.getTimeZone("UTC");

	private static final SimpleDateFormat EVENT_TIMESTAMP_FORMAT_FOR_TODAY = new SimpleDateFormat(
			"'Today at' HH:mm");

	private static final SimpleDateFormat EVENT_TIMESTAMP_FORMAT_FOR_YESTERDAY = new SimpleDateFormat(
			"'Yesterday at' HH:mm");

	static {
		EVENT_ISO_TIMESTAMP_FORMAT.setTimeZone(TIME_ZONE);
	}

	private static String getTimestamp(Long timestamp) {
		String result = null;

		Date date = new Date(timestamp);
		if (DateUtils.isToday(date)) {
			result = EVENT_TIMESTAMP_FORMAT_FOR_TODAY.format(date);
		} else if (DateUtils.isYesterday(date)) {
			result = EVENT_TIMESTAMP_FORMAT_FOR_YESTERDAY.format(date);
		} else {
			result = EVENT_TIMESTAMP_FORMAT.format(date);
		}
		return result;
	}

	private static String getISOTimestamp(Long timestamp) {
		Date date = new Date(timestamp);
		return EVENT_ISO_TIMESTAMP_FORMAT.format(date);
	}

	public static enum Type {

		NEW_VENDOR_REGISTERED(
				"<a href=\"{1}\" class='no-underline'><strong class='text-info'>{0}</strong></a> has registered as a new vendor with us"),
		NEW_BUYER_REGISTERED(
				"<a href=\"{1}\" class='no-underline'><strong class='text-info'>{0}</strong></a> has registered as a new buyer with us"),
		NEW_PRODUCT_ADDED(
				"<a href=\"{2}\" class='no-underline'><strong class='text-info'>{0}</strong></a> A new product has been added by <span class='text-info'>{1}</span>"),
		PRODUCT_UPDATED(
				"<a href=\"{2}\" class='no-underline'><strong class='text-info'>{0}</strong></a> This product has been updated by <span class='text-info'>{1}</span>"),
		PRODUCT_DELETED(
				"<strong class='text-danger'>{0}:</strong> This product has been deleted"),
		NEW_PURCHASE_ITEM_ORDERED(
				"<a href=\"{1}\" class='no-underline'><strong class='text-info'>A new purchase list</strong></a> has been placed by <span class='text-info'>{0}</span>"),
		PURCHASE_ITEM_STATUS_UPDATED(
				"<a href=\"{2}\" class='no-underline'><strong class='text-info'>{0}</strong></a> The status of purchase item has been updated by <span class='text-info'>{1}</span>"),
		PURCHASE_ITEMS_UPDATED(
						"<a href=\"{1}\" class='no-underline'><strong class='text-info'>Purchase items</strong></a> have been updated by <span class='text-info'>{0}</span>"),
		PURCHASE_ITEM_DELETED(
				"<strong class='text-danger'>{0}:</strong> This purchase item has been deleted"),
		NEW_PRODUCT_OFFER_POSTED(
				"<a href=\"{2}\" class='no-underline'><strong class='text-info'>{0}</strong></a> New product offer has been posted by <span class='text-info'>{1}</span>"),
		PRODUCT_OFFER_UPDATED(
				"<a href=\"{2}\" class='no-underline'><strong class='text-info'>{0}</strong></a> Product offer has been updated"),
		PRODUCT_OFFER_DELETED(
				"<strong class='text-danger'>{0}</strong> Product offer has been deleted"),
		NEW_PRODUCT_ENQUIRY_POSTED(
				"<a href=\"{2}\" class='no-underline'><strong class='text-info'>{0}</strong></a> A new enquiry has been posted for the product by <span class='text-info'>{1}</span>"),
		PRODUCT_ENQUIRY_UPDATED(
				"<a href=\"{2}\" class='no-underline'><strong class='text-info'>{0}</strong></a> Product enquiry has been updated by <span class='text-info'>{1}</span>"),
		PRODUCT_ENQUIRY_DELETED(
				"<strong class='text-danger'>{0}</strong> Product enquiry has been deleted by <span class='text-info'>{1}</span>"),
		ENTITY_APPROVED(
				"<a href=\"{3}\" class='no-underline'><strong class='text-info'>{1}</strong></a> The status of this {0} has been approved by <span class='text-info'>{2}</span>"),
		ENTITY_DISAPPROVED(
				"<a href=\"{3}\" class='no-underline'><strong class='text-info'>{1}</strong></a> The status of this {0} has been disapproved by <span class='text-info'>{2}</span>"),
		ENTITY_STATUS_UPDATED(
				"<a href=\"{3}\" class='no-underline'><strong class='text-info'>{1}</strong></a> The status of this {0} has been updated by <span class='text-info'>{2}</span>"),
		ABUSE_REPORT_POSTED("Abuse report has been posted"),
		
		BUYER_REVIEW_POSTED("<a href=\"{1}\" class='no-underline'><strong class='text-info'>A new review</strong></a> has been posted about your firm by <span class='text-info'>{0}</span>"),
		VENDOR_REVIEW_POSTED("<a href=\"{1}\" class='no-underline'><strong class='text-info'>A new review</strong></a> has been posted about your firm by <span class='text-info'>{0}</span>"),
		PRODUCT_REVIEW_POSTED("<a href=\"{2}\" class='no-underline'><strong class='text-info'>{0}</strong></a> A new review has been posted about this product by <span class='text-info'>{1}</span>"),
		TENDER_OPEN("<a href=\"{0}\" class='no-underline'><strong class='text-info'>A new tender</strong></a> has been opened by a buyer. Place your bid now."),
		FIRST_TENDER_BID_POSTED("<strong class='text-info'>The first bid</strong> has been posted against your tender: <a href=\"{2}\" class='no-underline'><strong class='text-info'>{0}</strong></a> by <span class='text-info'>{1}</span>."),
		TENDER_CLOSED("<a href=\"{1}\" class='no-underline'><strong class='text-info'>{0}</strong></a> This tender has been closed."),
		BUYER_BIDDING_SUBSCRIPTION_EXPIRED("Your <a href=\"{0}\" class='no-underline'><strong class='text-info'>current bidding subscription</strong></a> has expired. Please subscribe to one of of our subscription plans to continue posting new tenders."),
		VENDOR_BIDDING_SUBSCRIPTION_EXPIRED("Your <a href=\"{0}\" class='no-underline'><strong class='text-info'>current bidding subscription</strong></a> has expired. Please subscribe to one of of our subscription plans to continue accessing new tenders.");
		

		private String message;

		private Type(String message) {
			this.message = message;
		}

		public String getMessage(String... args) {
			return MessageFormat.format(message, args);
		}

		public void setMessage(String message) {
			this.message = message;
		}
	}

	private String eventId;
	private long time;
	private String timestamp;
	private String notificationText;
	private String recipientType;
	private String recipientId;
	private String objectId;
	private String objectName;
	private String triggeredBy;
	private Type eventType;
	private String displayText;
	private String link;
	private boolean read;

	public NotificationEvent() {
		super();
	}

	public NotificationEvent(BNBNeedsUser user,
			NotificationInfoResponse apiNotification) {

		this.eventId = apiNotification.getId();
		this.timestamp = getISOTimestamp(apiNotification.getTimestamp());
		this.time = apiNotification.getTimestamp();
		this.eventType = Type.valueOf(apiNotification.getNotificationType());
		this.recipientType = apiNotification.getRecipientType();
		this.recipientId = apiNotification.getRecipientId();
		this.objectId = apiNotification.getEntityId();
		this.objectName = apiNotification.getEntityName();
		this.triggeredBy = apiNotification.getTriggeredBy();
		this.notificationText = apiNotification.getNotificationText();
		this.read = apiNotification.isRead();

		switch (eventType) {
		case NEW_BUYER_REGISTERED:
			link = BuyerController.getBuyerDetailsRequestURI(objectId);
			displayText = eventType.getMessage(objectName, link);
			break;

		case NEW_VENDOR_REGISTERED:
			link = VendorController.getVendorDetailsRequestURI(objectId);
			displayText = eventType.getMessage(objectName, link);
			break;

		case NEW_PRODUCT_ADDED:
		case PRODUCT_UPDATED:
			link = ProductController.getProductDetailsURI(objectId);
			displayText = eventType.getMessage(objectName, triggeredBy, link);
			break;

		case NEW_PRODUCT_ENQUIRY_POSTED:
			String enquiryDate = StringUtils.getDateTimestamp(Constants.DATE_FORMAT, getTime());
			link = VendorEnquiryController.getEnquiryListURI(enquiryDate);
			displayText = eventType.getMessage(objectName, triggeredBy, link);
			break;
		case PRODUCT_ENQUIRY_UPDATED:
			link = VendorEnquiryController.getEnquiryListURI();
			displayText = eventType.getMessage(objectName, triggeredBy, link);
			break;
		case PRODUCT_ENQUIRY_DELETED:
			displayText = eventType.getMessage(objectName, triggeredBy);
			break;

		case PRODUCT_DELETED:
			link = ProductController.getProductDetailsURI(objectId);
			displayText = eventType.getMessage(objectName);
			break;
		case NEW_PURCHASE_ITEM_ORDERED:
			String requestDate = StringUtils.getDateTimestamp(Constants.DATE_FORMAT, getTime());
			link = VendorPurchaseListController.getPurchaseListURI(requestDate);
			displayText = eventType.getMessage(triggeredBy, link);
			break;
		case PURCHASE_ITEM_STATUS_UPDATED:
			if (user.isVendor()) {
				link = VendorPurchaseListController
						.getPurchaseUpdateFormView(objectId);
				displayText = eventType.getMessage(objectName, triggeredBy,
						link);
			}
			if (user.isBuyer()) {
				link = BuyerPurchaseListController.getPurchaseListURI();
				displayText = eventType.getMessage(objectName, triggeredBy,
						link);
			}
			break;
		case PURCHASE_ITEMS_UPDATED:
			if (user.isVendor()) {
				link = VendorPurchaseListController.getPurchaseListURI();
				displayText = eventType.getMessage(triggeredBy,
						link);
			}
			if (user.isBuyer()) {
				link = BuyerPurchaseListController.getPurchaseListURI();
				displayText = eventType.getMessage(triggeredBy,
						link);
			}
			break;
		case ENTITY_APPROVED:
		case ENTITY_DISAPPROVED:
		case ENTITY_STATUS_UPDATED:
			if(objectId == null) {
				break;
			}
			String entityType = StringUtils.getTypeName(objectId);
			switch (entityType.toLowerCase()) {
			case "producttype":
				link = ProductTypeController.LIST_ENTITIES_REQUEST_URI;
				break;
			case "productcategory":
				link = ProductCategoryController.LIST_ENTITIES_REQUEST_URI;
				break;
			case "productname":
				link = ProductNameController.LIST_ENTITIES_REQUEST_URI;
				break;
			case "businesstype":
				link = BusinessTypeController.LIST_ENTITIES_REQUEST_URI;
				break;
			case "vendor":
					link = VendorController.LIST_VENDORS_URI;
					break;
			case "buyer":
					link = BuyerController.LIST_BUYERS_URI;
					break;
			case "product":
				link = ProductController.LIST_PRODUCTS_REQUEST_URI;
				break;
			case "enquiry":
				if(user.isBuyer()) {
					link = BuyerEnquiryController.getEnquiryListURI();
				}
				if(user.isVendor()) {
					link = VendorEnquiryController.getEnquiryListURI();
				}
				break;
			case "purchaseitem":
				if(user.isBuyer()) {
					link = BuyerPurchaseListController.getPurchaseListURI();
				}
				if(user.isVendor()) {
					link = VendorPurchaseListController.getPurchaseListURI();
				}
				break;
			
			default:
				break;
			}
			displayText = eventType.getMessage(StringUtils
					.splitCamelCaseWordIntoSpaceSeparatedWords(entityType),
					objectName, triggeredBy, link);
			break;
		case BUYER_REVIEW_POSTED:
			link = BuyerOperationsController.getProfileURI("reviews");
			displayText = eventType.getMessage(triggeredBy, link);
			break;
		case VENDOR_REVIEW_POSTED:
			link = VendorOperationsController.getProfileURI("reviews");
			displayText = eventType.getMessage(triggeredBy, link);
			break;
		case PRODUCT_REVIEW_POSTED:
			String [] splitArray = apiNotification.getEntityLink().split("/");
			String productId = splitArray[3];
			link = ProductController.getProductDetailsURI(productId, "reviews");
			displayText = eventType.getMessage(objectName, triggeredBy, link);
			break;
		case TENDER_OPEN:
			link = TenderBidController.getTenderPreviewsUrl();
			displayText = eventType.getMessage(link);
			break;
		case FIRST_TENDER_BID_POSTED:
			link = TenderManagementController.getPathForGetTenderBids(objectId);
			displayText = eventType.getMessage(objectName, triggeredBy, link);
			break;
		case TENDER_CLOSED:
			link = TenderManagementController.getPathForGetTenderDetails(objectId);
			displayText = eventType.getMessage(objectName, link);
			break;
		case BUYER_BIDDING_SUBSCRIPTION_EXPIRED:
			link = BuyerBiddingSubscriptionController.getBiddingSubscriptionDetailsUrl(objectId);
			displayText = eventType.getMessage(link);
			break;
		case VENDOR_BIDDING_SUBSCRIPTION_EXPIRED:
			link = VendorBiddingSubscriptionController.getBiddingSubscriptionDetailsUrl(objectId);
			displayText = eventType.getMessage(link);
			break;
		default:
			break;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		NotificationEvent event = (NotificationEvent) obj;
		return isSameNotification(event);
	}

	public boolean isSameNotification(NotificationInfoResponse infoResponse) {
		return this.eventId.equals(infoResponse.getId());
	}

	public boolean isSameNotification(NotificationEvent event) {
		return this.eventId.equals(event.getEventId());
	}

	public boolean isRead() {
		return read;
	}

	public boolean isUnread() {
		return (isRead() == false);
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public void markAsRead() {
		setRead(true);
	}

	public void markAsUnread() {
		setRead(false);
	}

	public String getEventId() {
		return eventId;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public String getNotificationText() {
		return notificationText;
	}

	public String getRecipientType() {
		return recipientType;
	}

	public String getRecipientId() {
		return recipientId;
	}

	public String getObjectId() {
		return objectId;
	}

	public String getObjectName() {
		return objectName;
	}

	public String getTriggeredBy() {
		return triggeredBy;
	}

	public Type getEventType() {
		return eventType;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public void setNotificationText(String notificationText) {
		this.notificationText = notificationText;
	}

	public void setRecipientType(String recipientType) {
		this.recipientType = recipientType;
	}

	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public void setTriggeredBy(String triggeredBy) {
		this.triggeredBy = triggeredBy;
	}

	public void setEventType(Type eventType) {
		this.eventType = eventType;
	}

	public String getDisplayText() {
		return displayText;
	}

	public String getLink() {
		return link;
	}

	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

}
