package com.bnbneeds.portal.dealer.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.ui.Model;

import com.bnbneeds.app.model.dealer.tender.TenderBidInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoResponse;
import com.bnbneeds.portal.dealer.util.Constants;
import com.bnbneeds.portal.dealer.util.DateUtils;
import com.bnbneeds.portal.dealer.util.StringUtils;

public abstract class TenderController extends DealerController {

	protected static final String TENDER_STATUSES_ATTRIBUTE = "tenderStatuses";
	protected static final String TENDER_INFO_RESPONSE_ATTRIBUTE = "tenderInfo";
	protected static final SimpleDateFormat DATE_FORMAT_IST = DateUtils.getDateFormat(Constants.TIMESTAMP_FORMAT,
			Constants.TIME_ZONE_IST);
	protected static final String TENDER_STATUS_OPEN = "OPEN";
	protected static final int FETCH_SIZE = 20;

	protected static final Map<String, String> creditTermsTypes = new LinkedHashMap<>();
	protected static final Map<String, String> paymentModes = new LinkedHashMap<>();
	protected static final Map<String, String> bidValueTypes = new LinkedHashMap<>();
	protected static final Map<String, String> deliveryFrequencyTypes = new LinkedHashMap<>();

	static {
		// DAYS_15, DAYS_30, DAYS_45, DAYS_60, DAYS_90
		creditTermsTypes.put("IMMEDIATE_PAYMENT", "Immediate Payment");
		creditTermsTypes.put("DAYS_15", "15 Days");
		creditTermsTypes.put("DAYS_30", "30 Days");
		creditTermsTypes.put("DAYS_45", "45 Days");
		creditTermsTypes.put("DAYS_60", "60 Days");
		creditTermsTypes.put("DAYS_90", "90 Days");
		creditTermsTypes.put("DAYS_120", "120 Days");
		creditTermsTypes.put("MORE_THAN_120_DAYS", "More Than 120 Days");

		// CASH, CHEQUE, ONLINE, CREDIT
		paymentModes.put("CASH", "Cash");
		paymentModes.put("CHEQUE", "Cheque");
		paymentModes.put("ONLINE", "Online");
		paymentModes.put("CREDIT", "Credit");

		// UNIT_PRICE, TOTAL_PRICE
		bidValueTypes.put("UNIT_PRICE", "Unit Price");
		bidValueTypes.put("TOTAL_PRICE", "Total Price");

		// ONETIME, DAILY, WEEKLY, FORTNIGHTLY, MONTHLY, QUATERLY
		deliveryFrequencyTypes.put("ONETIME", "One Time");
		deliveryFrequencyTypes.put("DAILY", "Daily");
		deliveryFrequencyTypes.put("WEEKLY", "Weekly");
		deliveryFrequencyTypes.put("FORTNIGHTLY", "Fortnightly");
		deliveryFrequencyTypes.put("MONTHLY", "Monthy");
		deliveryFrequencyTypes.put("QUARTERLY", "Quarterly");
	}

	protected static void mapDisplayAttributes(TenderInfoResponse response) {

		String bidValueType = response.getBidValueType();
		response.setBidValueType(bidValueTypes.get(bidValueType));

		String creditTerms = response.getCreditTerms();
		response.setCreditTerms(creditTermsTypes.get(creditTerms));

		String paymentMode = response.getPaymentMode();
		response.setPaymentMode(paymentModes.get(paymentMode));

		String deliveryFrequency = response.getDeliveryFrequency();
		response.setDeliveryFrequency(deliveryFrequencyTypes.get(deliveryFrequency));
	}

	protected void loadTenderQueryStatusMap(Model model) {
		Map<String, String> map = new LinkedHashMap<>();
		map.put("Created", "CREATED");
		map.put("Open", "OPEN");
		map.put("Closed", "CLOSED");
		map.put("Cancelled", "CANCELLED");
		map.put("Edit In Progress", "EDIT_IN_PROGRESS");
		model.addAttribute(TENDER_STATUSES_ATTRIBUTE, map);
	}

	protected void setBidTimestamp(TenderBidInfoResponse info) {
		String bidTimestamp = info.getUpdateTimestamp();
		if (StringUtils.isEmpty(bidTimestamp)) {
			bidTimestamp = info.getCreateTimestamp();
		}
		Date date = DateUtils.getDate(Constants.APISVC_RESPONSE_TIMESTAMP_FORMAT, bidTimestamp);
		info.setCreateTimestamp(StringUtils.getDateTimestamp(DATE_FORMAT_IST, date));
	}

	protected void setBidTimestampForListResponse(TenderBidInfoListResponse tenderBidInfoListResponse) {

		if (tenderBidInfoListResponse != null && !tenderBidInfoListResponse.isEmpty()) {
			for (Iterator<TenderBidInfoResponse> iterator = tenderBidInfoListResponse.getIterator(); iterator
					.hasNext();) {
				TenderBidInfoResponse info = iterator.next();
				setBidTimestamp(info);
			}
		}

	}

	protected void setTenderInfoResponse(TenderInfoResponse response, Model model) {

		if (TENDER_STATUS_OPEN.equals(response.getStatus())) {
			Date closeDate = DateUtils.getDate(Constants.APISVC_RESPONSE_TIMESTAMP_FORMAT,
					response.getCloseTimestamp());
			response.setCloseTimestamp(
					StringUtils.getDateTimestamp(Constants.TIMESTAMP_FORMAT_FOR_JQUERY_FINAL_COUNT_DOWN, closeDate));
		}

		mapDisplayAttributes(response);

		model.addAttribute(TENDER_INFO_RESPONSE_ATTRIBUTE, response);
	}

}
