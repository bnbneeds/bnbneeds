package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.DealerUtils.checkAndReturnRelationshipId;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.handleBadRequest;
import static com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils.writeResponse;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.EntityInfoListResponse;
import com.bnbneeds.app.model.EntityInfoResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.UploadFormResponse;
import com.bnbneeds.app.model.product.CreateProductParam;
import com.bnbneeds.app.model.product.ProductImageListResponse;
import com.bnbneeds.app.model.product.ProductInfoResponse;
import com.bnbneeds.app.model.product.UpdateProductParam;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.DealerUtils;
import com.bnbneeds.portal.dealer.controller.util.PipelineJobUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.formbean.CreateProductBean;
import com.bnbneeds.portal.dealer.formbean.ProductBean.Attribute;
import com.bnbneeds.portal.dealer.formbean.UpdateProductBean;
import com.bnbneeds.portal.dealer.formbean.mapper.ProductBeanMapper;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

@Controller
@RequestMapping(path = "/vendors/products")
@PreAuthorize("hasAuthority('VENDOR')")
public class VendorProductsController extends DealerController {

	private static final Logger logger = LoggerFactory.getLogger(VendorProductsController.class);

	private static final String BASE_REQUEST_PATH = "/vendors/products";
	private static final String UPLOAD_PRODUCT_IMAGE_FORM_URI = BASE_REQUEST_PATH + "/%s/add-image-form";

	private static final String CREATE_PRODUCT_FORM_VIEW = "product-create-form";
	private static final String UPDATE_PRODUCT_FORM_VIEW = "product-update-form";
	private static final String PRODUCT_IMAGE_MANAGEMENT_FORM_VIEW = "product-image-management";

	private static final String CREATE_PRODUCT_FORM_ATTRIBUTE = "createProductFormBean";
	private static final String UPDATE_PRODUCT_FORM_ATTRIBUTE = "updateProductFormBean";
	private static final String PRODUCT_NAMES_ATTRIBUTE = "productNames";
	private static final String PRODUCT_TYPES_ATTRIBUTE = "productTypes";
	private static final String PRODUCT_CATEGORIES_ATTRIBUTE = "productCategories";

	private static Map<String, List<EntityInfoResponse>> productNamesUserCache = new ConcurrentHashMap<>();
	private static Map<String, List<EntityInfoResponse>> productTypesUserCache = new ConcurrentHashMap<>();
	private static Map<String, List<EntityInfoResponse>> productCategoriesUserCache = new ConcurrentHashMap<>();

	private static final int NUMBER_OF_PRODUCT_ATTRIBUTES = 7;

	static String getProductImageUploadFormURI(String productId) {
		return String.format(UPLOAD_PRODUCT_IMAGE_FORM_URI, productId);
	}

	private synchronized static List<EntityInfoResponse> getProductNamesFromCache(BNBNeedsUser user,
			BNBNeedsClient bnbneedsClient) {

		String username = user.getUsername();
		List<EntityInfoResponse> productNames = productNamesUserCache.get(username);

		if (productNames == null) {
			productNames = new ArrayList<>();

			try {
				EntityInfoListResponse infolistResponse = bnbneedsClient.productNames().listBulk(RESULT_FETCH_SIZE,
						APPROVED_ENTITY_QUERY_MAP);
				List<EntityInfoResponse> entytyList = infolistResponse.getEntityInfoList();
				if (entytyList != null) {
					productNames.addAll(entytyList);
					String offset = infolistResponse.getNextOffset();
					String nextOffset = null;
					while (StringUtils.hasText(offset)) {
						infolistResponse = bnbneedsClient.productNames().listBulk(RESULT_FETCH_SIZE,
								APPROVED_ENTITY_QUERY_MAP, offset);
						if (infolistResponse != null) {
							entytyList = infolistResponse.getEntityInfoList();
							if (entytyList != null) {
								productNames.addAll(entytyList);
							}
							nextOffset = infolistResponse.getNextOffset();
							if (nextOffset.equals(offset)) {
								break;
							} else {
								offset = nextOffset;
							}
						}
					}
					productNamesUserCache.put(username, productNames);
				}
			} catch (BNBNeedsServiceException e) {
				clearProductNamesFromCache(user);
			}
		}
		return productNames;
	}

	private synchronized static void clearProductNamesFromCache(BNBNeedsUser user) {

		String username = user.getUsername();
		productNamesUserCache.remove(username);
	}

	private synchronized static List<EntityInfoResponse> getProductTypesFromCache(BNBNeedsUser user,
			BNBNeedsClient bnbneedsClient) {

		String username = user.getUsername();
		List<EntityInfoResponse> productTypes = productTypesUserCache.get(username);

		if (productTypes == null) {
			productTypes = new ArrayList<>();

			try {
				EntityInfoListResponse infolistResponse = bnbneedsClient.productTypes().listBulk(RESULT_FETCH_SIZE,
						APPROVED_ENTITY_QUERY_MAP);
				List<EntityInfoResponse> entityList = infolistResponse.getEntityInfoList();
				if (entityList != null) {
					productTypes.addAll(entityList);
					String offset = infolistResponse.getNextOffset();
					String nextOffset = null;
					while (StringUtils.hasText(offset)) {
						infolistResponse = bnbneedsClient.productTypes().listBulk(RESULT_FETCH_SIZE,
								APPROVED_ENTITY_QUERY_MAP, offset);
						if (infolistResponse != null) {
							entityList = infolistResponse.getEntityInfoList();
							if (entityList != null) {
								productTypes.addAll(entityList);
							}
							nextOffset = infolistResponse.getNextOffset();
							if (nextOffset.equals(offset)) {
								break;
							} else {
								offset = nextOffset;
							}
						}
					}
					productTypesUserCache.put(username, productTypes);
				}
			} catch (BNBNeedsServiceException e) {
				clearProductTypesFromCache(user);
			}
		}
		return productTypes;
	}

	private static void clearProductTypesFromCache(BNBNeedsUser user) {

		String username = user.getUsername();
		productTypesUserCache.remove(username);
	}

	private synchronized static List<EntityInfoResponse> getProductCategoriesFromCache(BNBNeedsUser user,
			BNBNeedsClient bnbneedsClient) {

		String username = user.getUsername();
		List<EntityInfoResponse> productCategories = productCategoriesUserCache.get(username);

		if (productCategories == null) {
			productCategories = new ArrayList<>();

			try {
				EntityInfoListResponse infolistResponse = bnbneedsClient.productCategories().listBulk(RESULT_FETCH_SIZE,
						APPROVED_ENTITY_QUERY_MAP);
				List<EntityInfoResponse> entityList = infolistResponse.getEntityInfoList();
				if (entityList != null) {
					productCategories.addAll(entityList);
					String offset = infolistResponse.getNextOffset();
					String nextOffset = null;
					while (StringUtils.hasText(offset)) {
						infolistResponse = bnbneedsClient.productCategories().listBulk(RESULT_FETCH_SIZE,
								APPROVED_ENTITY_QUERY_MAP, offset);
						if (infolistResponse != null) {
							entityList = infolistResponse.getEntityInfoList();
							if (entityList != null) {
								productCategories.addAll(entityList);
							}
							nextOffset = infolistResponse.getNextOffset();
							if (nextOffset.equals(offset)) {
								break;
							} else {
								offset = nextOffset;
							}
						}
					}
					productCategoriesUserCache.put(username, productCategories);
				}
			} catch (BNBNeedsServiceException e) {
				clearProductCategoriesFromCache(user);
			}
		}
		return productCategories;
	}

	private static void clearProductCategoriesFromCache(BNBNeedsUser user) {

		String username = user.getUsername();
		productCategoriesUserCache.remove(username);
	}

	private static void clearAllCache(BNBNeedsUser user) {
		clearProductNamesFromCache(user);
		clearProductTypesFromCache(user);
		clearProductCategoriesFromCache(user);
	}

	private void setModelAttributesForCreateProductForm(BNBNeedsUser user, Model model) {

		List<EntityInfoResponse> productNameList = getProductNamesFromCache(user, bnbneedsClient);
		List<EntityInfoResponse> productTypeList = getProductTypesFromCache(user, bnbneedsClient);
		List<EntityInfoResponse> productCategoryList = getProductCategoriesFromCache(user, bnbneedsClient);

		model.addAttribute(PRODUCT_NAMES_ATTRIBUTE, productNameList);
		model.addAttribute(PRODUCT_TYPES_ATTRIBUTE, productTypeList);
		model.addAttribute(PRODUCT_CATEGORIES_ATTRIBUTE, productCategoryList);

	}

	@Layout(title = "Add New Product")
	@RequestMapping(method = RequestMethod.GET, path = "/add-product-form")
	public String getCreateProductForm(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(CREATE_PRODUCT_FORM_ATTRIBUTE) CreateProductBean form, Model model) {

		DealerUtils.checkHasDealership(user);

		clearAllCache(user);

		setModelAttributesForCreateProductForm(user, model);

		ArrayList<Attribute> attrs = new ArrayList<>();

		for (int i = 0; i < NUMBER_OF_PRODUCT_ATTRIBUTES; i++) {
			attrs.add(new Attribute());
		}
		form.setAttributes(attrs);

		return CREATE_PRODUCT_FORM_VIEW;

	}

	@Layout(title = "Add New Product")
	@RequestMapping(method = RequestMethod.POST, path = "/add-product")
	public String createProduct(@AuthenticationPrincipal BNBNeedsUser user,
			@ModelAttribute(CREATE_PRODUCT_FORM_ATTRIBUTE) @Valid CreateProductBean form, BindingResult result,
			Model model) {

		String vendorId = checkAndReturnRelationshipId(user);
		setModelAttributesForCreateProductForm(user, model);
		if (result.hasErrors()) {
			return CREATE_PRODUCT_FORM_VIEW;
		}

		CreateProductParam param = new CreateProductParam();
		ProductBeanMapper.mapCreateProductBean(form, param);
		try {
			CreateResourceResponse response = bnbneedsClient.vendors(vendorId).createProduct(param);
			String newProductId = response.getResourceRep().getId();
			clearAllCache(user);
			return getRedirectPath(ProductController.LIST_PRODUCTS_REQUEST_URI + "?createSuccess=" + newProductId);
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, CREATE_PRODUCT_FORM_VIEW);
		}
	}

	@Layout(title = "Manage images of the product")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/manage-images")
	public String getManageProductImagesView(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String productId, Model model) {

		DealerUtils.checkHasDealership(user);

		try {

			ProductInfoResponse productInfo = bnbneedsClient.products(productId).get();

			ProductController.setProductInfoModelAttribute(productInfo, model);

			ProductImageListResponse imageListResponse = productInfo.getImageList();

			ProductController.setProductImageLinks(imageListResponse, model);

			return PRODUCT_IMAGE_MANAGEMENT_FORM_VIEW;
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, PRODUCT_IMAGE_MANAGEMENT_FORM_VIEW);
		}

	}

	@RequestMapping(method = RequestMethod.GET, path = "/{id}/add-image-url")
	@ResponseBody
	public synchronized void writeProductImageUploadURL(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String productId, HttpServletResponse response) throws IOException {

		String contentType = MediaType.TEXT_PLAIN_VALUE;

		try {

			String vendorId = getDealershipId(user);
			UploadFormResponse formResponse = bnbneedsClient.vendors(vendorId).getProductImageUploadForm(productId);
			String uploadURL = formResponse.getActionURL();
			if (devserver) {
				URI localURL = URI.create(uploadURL);
				uploadURL = apiSvcBaseURL + localURL.getPath();
			}
			writeResponse(HttpServletResponse.SC_OK, contentType, uploadURL, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{id}/delete")
	public String deleteProduct(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String productId,
			Model model) throws InterruptedException {

		try {
			String vendorId = getDealershipId(user);
			CreateResourceResponse jobInfo = bnbneedsClient.vendors(vendorId).deactivateProduct(productId, true);
			String jobId = jobInfo.getResourceRep().getId();
			PipelineJobUtils.pollTillJobCompletes(bnbneedsClient, jobId, 2);
			return getRedirectPath(ProductController.LIST_PRODUCTS_REQUEST_URI + "?deleteSuccess");
		} catch (BNBNeedsServiceException e) {
			ProductInfoResponse productInfo = bnbneedsClient.products(productId).get();
			ProductController.setProductInfoModelAttribute(productInfo, model);
			return handleBadRequest(e, model, ProductController.PRODUCT_DETAILS_VIEW);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{id}/images/{imageKey}/delete")
	@ResponseBody
	public synchronized void deleteProductImage(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String productId, @PathVariable("imageKey") String imageKey,
			HttpServletResponse response) throws IOException, InterruptedException {

		String contentType = MediaType.TEXT_PLAIN_VALUE;

		String vendorId = getDealershipId(user);
		sleep();

		try {

			TaskResponse task = bnbneedsClient.vendors(vendorId).deleteProductImage(productId, imageKey);
			writeResponse(task.getHttpCode(), contentType, task.getMessage(), response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@Layout(title = "Update Product Information")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/update-form")
	public String getUpdateProductForm(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String productId,
			@ModelAttribute(UPDATE_PRODUCT_FORM_ATTRIBUTE) UpdateProductBean form, Model model) {

		DealerUtils.checkHasDealership(user);

		try {
			ProductInfoResponse productInfo = bnbneedsClient.products(productId).get();

			ProductBeanMapper.mapProductInfoToProductBean(productInfo, form);
			ProductController.setProductInfoModelAttribute(productInfo, model);

			List<Attribute> attributes = form.getAttributes();
			int attributeCount = NUMBER_OF_PRODUCT_ATTRIBUTES - attributes.size();

			for (int i = 0; i < attributeCount; i++) {
				attributes.add(new Attribute());
			}
			form.setAttributes(attributes);
			return UPDATE_PRODUCT_FORM_VIEW;
		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, UPDATE_PRODUCT_FORM_VIEW);
		}
	}

	@Layout(title = "Update Product Information")
	@RequestMapping(method = RequestMethod.POST, path = "/{id}/update")
	public String updateProduct(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String productId,
			@ModelAttribute(UPDATE_PRODUCT_FORM_ATTRIBUTE) @Valid UpdateProductBean form, BindingResult result,
			Model model) {

		String vendorId = getDealershipId(user);

		try {
			ProductInfoResponse productInfo = bnbneedsClient.products(productId).get();

			ProductController.setProductInfoModelAttribute(productInfo, model);

			try {
				Double.parseDouble(form.getCostPrice());
			} catch (NumberFormatException e) {
				result.rejectValue("costPrice", null, "Value is invalid.");
			}

			if (result.hasErrors()) {
				return UPDATE_PRODUCT_FORM_VIEW;
			}

			UpdateProductParam updateProductParam = new UpdateProductParam();
			ProductBeanMapper.mapUpdateProductBean(form, updateProductParam);
			bnbneedsClient.vendors(vendorId).updateProduct(productId, updateProductParam);

			return getRedirectPath(ProductController.getProductDetailsURI(productId) + "?updateSuccess");

		} catch (BNBNeedsServiceException e) {
			return handleBadRequest(e, model, UPDATE_PRODUCT_FORM_VIEW);
		}
	}

}
