package com.bnbneeds.portal.dealer.formbean.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.bnbneeds.app.model.product.CreateProductParam;
import com.bnbneeds.app.model.product.ProductInfoResponse;
import com.bnbneeds.app.model.product.ProductParam;
import com.bnbneeds.app.model.product.UpdateProductParam;
import com.bnbneeds.portal.dealer.formbean.CreateProductBean;
import com.bnbneeds.portal.dealer.formbean.ProductBean;
import com.bnbneeds.portal.dealer.formbean.ProductBean.Attribute;
import com.bnbneeds.portal.dealer.formbean.UpdateProductBean;
import com.bnbneeds.portal.dealer.util.StringUtils;

public class ProductBeanMapper {

	public static void mapProductBean(ProductBean from, ProductParam to) {

		to.setCostPrice(from.getCostPrice());
		String description = from.getDescription();
		if (StringUtils.hasText(description)) {
			to.setDescription(description);
		}
		String modelId = from.getModelId();
		if (StringUtils.hasText(modelId)) {
			to.setModelId(modelId);
		}

		String serviceLocations = from.getServiceLocations();
		if (StringUtils.hasText(serviceLocations)) {
			to.setServiceLocations(serviceLocations);
		}

		String videoURL = from.getVideoURL();
		if (StringUtils.hasText(videoURL)) {
			to.setVideoURL(videoURL);
		}

		String tutorialVideoURL = from.getTutorialVideoURL();
		if (StringUtils.hasText(tutorialVideoURL)) {
			to.setTutorialVideoURL(tutorialVideoURL);
		}

		List<Attribute> attributes = from.getAttributes();

		if (attributes != null && !attributes.isEmpty()) {
			Map<String, String> attributeMap = new HashMap<String, String>();
			for (Iterator<Attribute> iterator = attributes.iterator(); iterator
					.hasNext();) {
				Attribute attribute = iterator.next();
				if (attribute != null) {
					String attibuteName = attribute.getName();
					String attibuteValue = attribute.getValue();
					if (StringUtils.hasText(attibuteName)
							&& StringUtils.hasText(attibuteValue)) {
						attributeMap.put(attibuteName.toUpperCase(),
								attibuteValue);
					}
				}
			}
			to.setAttributes(attributeMap);
		}
	}

	public static void mapCreateProductBean(CreateProductBean from,
			CreateProductParam to) {
		mapProductBean(from, to);
		to.setProductTypeId(from.getProductTypeId());
		to.setProductCategoryId(from.getProductCategoryId());
		to.setProductNameId(from.getProductNameId());

	}

	public static void mapUpdateProductBean(UpdateProductBean from,
			UpdateProductParam to) {
		mapProductBean(from, to);
	}

	public static void mapProductInfoToProductBean(ProductInfoResponse from,
			ProductBean to) {

		to.setCostPrice(String.valueOf(from.getCostPrice()));

		String modelId = from.getModelId();
		to.setModelId(modelId);

		String description = from.getDescription();
		to.setDescription(description);

		String serviceLocations = from.getServiceLocations();
		to.setServiceLocations(serviceLocations);

		String videoURL = from.getVideoURL();
		to.setVideoURL(videoURL);

		String tutorialVideoURL = from.getTutorialVideoURL();
		to.setTutorialVideoURL(tutorialVideoURL);

		Map<String, String> attributeMap = from.getAttributes();
		List<Attribute> attributes = new ArrayList<Attribute>();
		if (attributeMap != null && !attributeMap.isEmpty()) {
			for (String key : attributeMap.keySet()) {
				attributes.add(new Attribute(key, attributeMap.get(key)));
			}
		}
		to.setAttributes(attributes);
	}
}
