package com.bnbneeds.portal.dealer.formbean;

public class TenderBean extends EntityBean {

	private Double expectedPrice;
	private String closeDate;
	private String deliveryFrequency;
	private String paymentMode;
	private String bidValueType;
	private String creditTerms;
	private String deliveryTerms;
	private String deliveryLocation;

	public Double getExpectedPrice() {
		return expectedPrice;
	}

	public String getDeliveryFrequency() {
		return deliveryFrequency;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public String getBidValueType() {
		return bidValueType;
	}

	public String getCreditTerms() {
		return creditTerms;
	}

	public String getDeliveryTerms() {
		return deliveryTerms;
	}

	public String getDeliveryLocation() {
		return deliveryLocation;
	}

	public void setExpectedPrice(Double expectedPrice) {
		this.expectedPrice = expectedPrice;
	}

	public void setDeliveryFrequency(String deliveryFrequency) {
		this.deliveryFrequency = deliveryFrequency;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public void setBidValueType(String bidValueType) {
		this.bidValueType = bidValueType;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}

	public void setDeliveryLocation(String deliveryLocation) {
		this.deliveryLocation = deliveryLocation;
	}

	public String getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}

}
