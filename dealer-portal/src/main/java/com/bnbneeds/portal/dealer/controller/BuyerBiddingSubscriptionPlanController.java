package com.bnbneeds.portal.dealer.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.rest.client.core.BiddingSubscriptionPlans.BiddingSubscriptionPlanFor;

@Controller
@Layout(title = BuyerBiddingSubscriptionPlanController.LIST_VIEW_TITLE_NAME)
@RequestMapping(path = BuyerBiddingSubscriptionPlanController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('BUYER')")
public class BuyerBiddingSubscriptionPlanController extends BiddingSubscriptionPlanController {

	protected static final String LIST_VIEW_TITLE_NAME = "Bidding Subscription Plans";
	static final String BIDDING_SUBSCRIPTION_PLAN_VIEW = "bidding-subscription-plan-details";
	static final String BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW = "bidding-subscription-plans-list";
	protected static final String BASE_REQUEST_PATH = "/buyers/bidding-subscription-plans";

	@Override
	protected String getBaseRequestURI() {
		return BASE_REQUEST_PATH;
	}

	@Override
	protected BiddingSubscriptionPlanFor getBiddingSubscriptionPlanFor() {
		return BiddingSubscriptionPlanFor.buyer;
	}

}
