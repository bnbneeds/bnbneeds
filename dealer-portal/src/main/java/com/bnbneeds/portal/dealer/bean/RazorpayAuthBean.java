package com.bnbneeds.portal.dealer.bean;

public class RazorpayAuthBean {

	private String keyId;
	private String keySecret;

	public RazorpayAuthBean(String keyId, String keySecret) {
		super();
		this.keyId = keyId;
		this.keySecret = keySecret;
	}

	public String getKeyId() {
		return keyId;
	}

	public String getKeySecret() {
		return keySecret;
	}

}
