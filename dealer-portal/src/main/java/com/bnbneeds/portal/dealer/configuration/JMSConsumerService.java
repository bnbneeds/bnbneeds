package com.bnbneeds.portal.dealer.configuration;

import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.bnbneeds.portal.common.jms.JMSConstants;
import com.bnbneeds.portal.common.jms.beans.MaintenanceNoticeMBean;
import com.bnbneeds.portal.dealer.bean.ApplicationBeans;
import com.bnbneeds.portal.dealer.bean.MaintenanceNoticeBean;
import com.bnbneeds.portal.dealer.util.StringUtils;

@Component
public class JMSConsumerService {

	private static final Logger logger = LoggerFactory.getLogger(JMSConsumerService.class);

	@Autowired
	private ApplicationBeans applicationBeans;

	@JmsListener(destination = JMSConstants.MAINTENANCE_NOTICE_QUEUE_NAME, containerFactory = JMSConstants.CONTAINER_FACTORY_BEAN_NAME)
	public void processMaintenanceNoticeMessage(MaintenanceNoticeMBean maintenanceNoticeMBean) throws JMSException {
		logger.info("JMS message recieved in maintenanceNoticeMBean: {}", maintenanceNoticeMBean);

		switch (maintenanceNoticeMBean.getOperation()) {
		case POST:
			MaintenanceNoticeBean maintenanceNotice = new MaintenanceNoticeBean();
			maintenanceNotice.setStartTime(maintenanceNoticeMBean.getStartTime());
			maintenanceNotice.setEndTime(maintenanceNoticeMBean.getEndTime());
			maintenanceNotice.setSubject(maintenanceNoticeMBean.getSubject());
			maintenanceNotice.setMessage(StringUtils.convertLineEndingsToHtml(maintenanceNoticeMBean.getMessage()));
			applicationBeans.setMaintenanceNotice(maintenanceNotice);
			break;
		case DELETE:
			applicationBeans.setMaintenanceNotice(null);
		default:
			break;
		}
	}
}
