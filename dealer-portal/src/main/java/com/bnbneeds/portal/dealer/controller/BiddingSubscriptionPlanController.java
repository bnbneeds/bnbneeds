package com.bnbneeds.portal.dealer.controller;

import static com.bnbneeds.portal.dealer.controller.util.DealerUtils.checkHasDealership;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.model.DataObjectInfoListResponse;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionPlanInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.SubscriptionPlanInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionPlanInfoResponse;
import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.portal.dealer.util.StringUtils;
import com.bnbneeds.rest.client.core.BiddingSubscriptionPlans;
import com.bnbneeds.rest.client.core.BiddingSubscriptionPlans.BiddingSubscriptionPlanFor;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.google.gson.Gson;

public abstract class BiddingSubscriptionPlanController extends DealerController {

	static final String BIDDING_SUBSCRIPTION_PLAN_VIEW = "bidding-subscription-plan-details";
	static final String BIDDING_SUBSCRIPTION_PLAN_LIST_VIEW = "bidding-subscription-plan-list";
	private static final String LIST_RESPONSE_ATTRIBUTE = "listResponse";
	// private static final String BASE_REQUEST_PATH_URL_ATTRIBUTE =
	// "baseRequestPathURL";
	private static final String LIST_SUBSCRIPTION_PLANS_URI = "/list";
	private static final String LIST_SUBSCRIPTION_PLANS_REQUEST_URL_ATTRIBUTE = "listSubscriptionPlansRequestURL";
	private static final String BIDDING_SUBSCRIPTION_PLAN_INFO_ATTRIBURE = "biddingSubscriptionPlanInfo";
	private static final String BIDDING_SUBSCRIPTION_PLAN_TYPE_QUOTA = "QUOTA";

	protected abstract String getBaseRequestURI();

	protected abstract BiddingSubscriptionPlanFor getBiddingSubscriptionPlanFor();

	private void setListSubscriptionPlansUrl(Model model) {
		String url = String.format("%s%s", getBaseRequestURI(), LIST_SUBSCRIPTION_PLANS_URI);
		model.addAttribute(LIST_SUBSCRIPTION_PLANS_REQUEST_URL_ATTRIBUTE, url);
	}

	private BiddingSubscriptionPlans getClientHandle() {
		BiddingSubscriptionPlanFor dealerType = getBiddingSubscriptionPlanFor();

		switch (dealerType) {
		case buyer:
			return bnbneedsClient.buyerBiddingSubscriptionPlans();
		case vendor:
			return bnbneedsClient.vendorBiddingSubscriptionPlans();
		default:
			return null;
		}
	}

	private static class CheckForDefaultPlan {
		private String result;
		private SubscriptionPlanInfoResponse planInfo;
		private String quotaText;

		@SuppressWarnings("unused")
		public String getResult() {
			return result;
		}

		public void setResult(String result) {
			this.result = result;
		}

		@SuppressWarnings("unused")
		public SubscriptionPlanInfoResponse getPlanInfo() {
			return planInfo;
		}

		public void setPlanInfo(SubscriptionPlanInfoResponse planInfo) {
			this.planInfo = planInfo;
		}

		@SuppressWarnings("unused")
		public String getQuotaText() {
			return quotaText;
		}

		public void setQuotaText(String quotaText) {
			this.quotaText = quotaText;
		}

	}

	private void setModelForSubscriptionPlansListView(String nextOffset, Model model) {

		BiddingSubscriptionPlans biddingSubscriptionPlansClient = getClientHandle();

		try {
			DataObjectInfoListResponse<? extends SubscriptionPlanInfoResponse> listResponse = null;

			if (StringUtils.hasText(nextOffset)) {
				listResponse = biddingSubscriptionPlansClient.list(RESULT_FETCH_SIZE, nextOffset);
			} else {
				listResponse = biddingSubscriptionPlansClient.list(RESULT_FETCH_SIZE);
			}
			model.addAttribute(LIST_RESPONSE_ATTRIBUTE, listResponse);
		} catch (BNBNeedsServiceException e) {
			throw e;
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/is-default-plan-available")
	public synchronized void writeStatusOfDefaultPlan(@AuthenticationPrincipal BNBNeedsUser user,
			HttpServletResponse httpResponse) throws IOException {
		String result = null;
		try {
			checkHasDealership(user);
			Map<String, String> queryFilterMap = new HashMap<>();
			queryFilterMap.put("defaultPlan", "true");
			DataObjectInfoListResponse<? extends SubscriptionPlanInfoResponse> listResponse = getClientHandle().list(1,
					queryFilterMap);
			CheckForDefaultPlan checkForDefaultPlan = new CheckForDefaultPlan();
			SubscriptionPlanInfoResponse planInfo = null;
			String quotaText = null;

			if (listResponse != null) {
				if (listResponse.isEmpty()) {
					result = "DEFAULT_PLAN_NOT_AVAILABLE";
				} else {
					result = "DEFAULT_PLAN_AVAILABLE";
					planInfo = listResponse.getIterator().next();

					if (BIDDING_SUBSCRIPTION_PLAN_TYPE_QUOTA.equals(planInfo.getType())) {
						if (user.isBuyer()) {
							BuyerBiddingSubscriptionPlanInfoResponse buyerPlanInfo = (BuyerBiddingSubscriptionPlanInfoResponse) planInfo;
							quotaText = String.format("Post %d tenders",
									buyerPlanInfo.getNumberOfTendersAllowedToCreate());

						}
						if (user.isVendor()) {
							VendorBiddingSubscriptionPlanInfoResponse vendorPlanInfo = (VendorBiddingSubscriptionPlanInfoResponse) planInfo;
							quotaText = String.format("Access %d tenders",
									vendorPlanInfo.getNumberOfTendersAllowedToAccess());

						}
					} else {
						// Duration
						if (user.isBuyer()) {
							quotaText = String.format("Post any number of tenders for %1$d %2$s",
									planInfo.getDurationValue(), planInfo.getDurationType());
						}
						if (user.isVendor()) {
							quotaText = String.format("Access any number of tenders for %1$d %2$s",
									planInfo.getDurationValue(), planInfo.getDurationType());
						}
					}
				}
			} else {
				result = "DEFAULT_PLAN_NOT_AVAILABLE";
			}
			checkForDefaultPlan.setResult(result);
			checkForDefaultPlan.setPlanInfo(planInfo);
			checkForDefaultPlan.setQuotaText(quotaText);
			Gson gson = new Gson();
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK, MediaType.TEXT_HTML_VALUE,
					gson.toJson(checkForDefaultPlan), httpResponse);

		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, httpResponse);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = LIST_SUBSCRIPTION_PLANS_URI)
	public String listBiddingSubscriptionPlans(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String nextOffset, Model model) {
		checkHasDealership(user);
		setListSubscriptionPlansUrl(model);
		try {
			setModelForSubscriptionPlansListView(nextOffset, model);
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, BIDDING_SUBSCRIPTION_PLAN_LIST_VIEW);
		}
		return BIDDING_SUBSCRIPTION_PLAN_LIST_VIEW;
	}

	@Layout(title = "Bidding Subscription Plan Details")
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/details")
	public String getBiddingSubscriptionDetails(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String planId, Model model) {
		checkHasDealership(user);
		try {
			SubscriptionPlanInfoResponse info = getClientHandle().get(planId);
			model.addAttribute(BIDDING_SUBSCRIPTION_PLAN_INFO_ATTRIBURE, info);
		} catch (BNBNeedsServiceException e) {
			return RequestResponseUtils.handleBadRequest(e, model, BIDDING_SUBSCRIPTION_PLAN_VIEW);
		}
		return BIDDING_SUBSCRIPTION_PLAN_VIEW;
	}
}
