package com.bnbneeds.portal.dealer.formbean;

public class VendorAggregatedPurchaseListBean extends
		AggregatedPurchaseListBean {

	private String buyerId;

	public String getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

}
