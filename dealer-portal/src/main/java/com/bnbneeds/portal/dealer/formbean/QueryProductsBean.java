package com.bnbneeds.portal.dealer.formbean;

import com.bnbneeds.portal.dealer.util.StringUtils;

public class QueryProductsBean {

	private String productType;
	private String productCategory;
	private String productName;
	private String vendor;
	private int minCostPrice;
	private int maxCostPrice;
	private String nextOffset;

	public String getProductType() {
		return productType;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public String getProductName() {
		return productName;
	}

	public int getMinCostPrice() {
		return minCostPrice;
	}

	public int getMaxCostPrice() {
		return maxCostPrice;
	}

	public String getNextOffset() {
		return nextOffset;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setMinCostPrice(int minCostPrice) {
		this.minCostPrice = minCostPrice;
	}

	public void setMaxCostPrice(int maxCostPrice) {
		this.maxCostPrice = maxCostPrice;
	}

	public void setNextOffset(String nextOffset) {
		this.nextOffset = nextOffset;
	}

	public String getQueryFilterText() {
		StringBuilder builder = new StringBuilder();
		if (productType != null) {
			if (productType.contains("|")) {
				String name = productType.split("\\|")[0];
				builder.append("<li>").append("<strong>").append("Type: ")
						.append("</strong>").append(name).append("</li>")
						.append(" ");
			}

		}

		if (StringUtils.hasText(productCategory)) {
			if (productCategory.contains("|")) {
				String name = productCategory.split("\\|")[0];
				builder.append("<li>").append("<strong>").append("Category: ")
						.append("</strong>").append(name).append("</li>")
						.append(" ");
			}
		}

		if (StringUtils.hasText(productName)) {
			if (productName.contains("|")) {
				String name = productName.split("\\|")[0];
				builder.append("<li>").append("<strong>").append("Product: ")
						.append("</strong>").append(name).append("</li>")
						.append(" ");
			}
		}
		if (StringUtils.hasText(vendor)) {
			if (vendor.contains("|")) {
				String name = vendor.split("\\|")[0];
				builder.append("<li>").append("<strong>").append("Vendor: ")
						.append("</strong>").append(name).append("</li>")
						.append(" ");
			}
		}
		if (minCostPrice > 0) {
			builder.append("<li>").append("<strong>")
					.append("Minimum Cost Price: ").append("</strong>")
					.append("&#8377;").append(minCostPrice).append("</li>")
					.append(" ");
		}
		if (maxCostPrice > 0) {
			builder.append("<li>").append("<strong>")
					.append("Maximum Cost Price: ").append("</strong>")
					.append("&#8377;").append(maxCostPrice).append("</li>")
					.append(" ");
		}

		if (builder.length() > 0) {
			builder.append("</ul>");
			builder.insert(0, "<ul>");

		}

		return builder.toString();
	}

}
