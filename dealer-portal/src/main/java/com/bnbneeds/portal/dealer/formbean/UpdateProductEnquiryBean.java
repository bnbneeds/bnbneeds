package com.bnbneeds.portal.dealer.formbean;

public class UpdateProductEnquiryBean extends ProductEnquiryBean {

	private String enquiryId;

	public String getEnquiryId() {
		return enquiryId;
	}

	public void setEnquiryId(String enquiryId) {
		this.enquiryId = enquiryId;
	}

}
