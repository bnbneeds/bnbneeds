package com.bnbneeds.portal.dealer.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bnbneeds.portal.dealer.annotation.Layout;
import com.bnbneeds.rest.client.core.BiddingSubscriptionPlans.BiddingSubscriptionPlanFor;

@Controller
@Layout(title = VendorBiddingSubscriptionPlanController.LIST_VIEW_TITLE_NAME)
@RequestMapping(path = VendorBiddingSubscriptionPlanController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('VENDOR')")
public class VendorBiddingSubscriptionPlanController extends BiddingSubscriptionPlanController {

	protected static final String LIST_VIEW_TITLE_NAME = "Bidding Subscription Plans";
	static final String BIDDING_SUBSCRIPTION_PLAN_VIEW = "bidding-subscription-plan-details";
	static final String BIDDING_SUBSCRIPTION_PLANS_LIST_VIEW = "bidding-subscription-plans-list";
	protected static final String BASE_REQUEST_PATH = "/vendors/bidding-subscription-plans";

	@Override
	protected String getBaseRequestURI() {
		return BASE_REQUEST_PATH;
	}

	@Override
	protected BiddingSubscriptionPlanFor getBiddingSubscriptionPlanFor() {
		return BiddingSubscriptionPlanFor.vendor;
	}

}
