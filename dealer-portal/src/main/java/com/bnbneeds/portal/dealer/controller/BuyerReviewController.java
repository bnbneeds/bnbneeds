package com.bnbneeds.portal.dealer.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bnbneeds.portal.dealer.controller.util.DealerUtils;
import com.bnbneeds.portal.dealer.controller.util.RequestResponseUtils;
import com.bnbneeds.portal.dealer.controller.util.ReviewUtility;
import com.bnbneeds.portal.dealer.formbean.ReviewBean;
import com.bnbneeds.portal.dealer.security.BNBNeedsUser;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;

@Controller
@RequestMapping(path = "/buyers/{buyerId}/reviews")
public class BuyerReviewController extends AbstractBaseController {

	private static final ReviewFor REVIEW_FOR = ReviewFor.buyer;

	@RequestMapping(method = RequestMethod.GET, path = "/list-as-text")
	@ResponseBody
	@PreAuthorize("hasAnyAuthority('BUYER', 'VENDOR')")
	public synchronized void writeReviews(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			@RequestParam(name = "reviewedBy", required = false) String reviewedBy,
			@RequestParam(name = "nextOffset", required = false) String nextOffset,
			HttpServletResponse response) throws IOException {

		try {
			DealerUtils.checkHasDealership(user);
			String list = ReviewUtility.getReviewsAsString(bnbneedsClient,
					REVIEW_FOR, buyerId, reviewedBy, nextOffset);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, list, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/list-as-text-by-reviewer")
	@ResponseBody
	@PreAuthorize("hasAuthority('VENDOR')")
	public synchronized void writeReviewsByVendor(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			HttpServletResponse response) throws IOException {

		try {
			String vendorId = DealerUtils.checkAndReturnRelationshipId(user);
			String list = ReviewUtility.getReviewsAsString(bnbneedsClient,
					REVIEW_FOR, buyerId, vendorId, null);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, list, response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize("hasAuthority('VENDOR')")
	public synchronized void postReview(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, ReviewBean form,
			HttpServletResponse response) throws IOException {
		try {
			DealerUtils.checkHasDealership(user);

			ReviewUtility.postReview(bnbneedsClient, REVIEW_FOR, buyerId, form);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, "OK", response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{reviewId}/delete")
	@ResponseBody
	@PreAuthorize("hasAuthority('VENDOR')")
	public synchronized void deleteReview(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			@PathVariable("reviewId") String reviewId,
			HttpServletResponse response) throws IOException {
		try {
			DealerUtils.checkHasDealership(user);

			ReviewUtility.deleteReview(bnbneedsClient, REVIEW_FOR, buyerId,
					reviewId);
			RequestResponseUtils.writeResponse(HttpServletResponse.SC_OK,
					MediaType.TEXT_PLAIN_VALUE, "OK", response);
		} catch (Exception e) {
			RequestResponseUtils.writeExceptionToResponse(e, response);
		}
	}
}
