package com.bnbneeds.portal.dealer.configuration;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;
import org.springframework.security.web.csrf.MissingCsrfTokenException;
import org.springframework.stereotype.Component;

import com.bnbneeds.portal.dealer.controller.AuthenticationController;

@Component
public class InvalidCsrfTokenExceptionHandler extends AccessDeniedHandlerImpl {

	@Override
	public void handle(HttpServletRequest request,
			HttpServletResponse response,

			AccessDeniedException accessDeniedException) throws IOException,
			ServletException {
		if (accessDeniedException instanceof MissingCsrfTokenException
				|| accessDeniedException instanceof InvalidCsrfTokenException) {

			response.sendRedirect(request.getContextPath()
					+ AuthenticationController.LOGIN_URL
					+ "?invalidCSRFTokenFound");
		}
		super.handle(request, response, accessDeniedException);
	}

}
