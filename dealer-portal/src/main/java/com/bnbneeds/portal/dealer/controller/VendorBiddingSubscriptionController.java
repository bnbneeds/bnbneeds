package com.bnbneeds.portal.dealer.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bnbneeds.rest.client.core.BiddingSubscriptions.BiddingSubscriptionFor;

@Controller
@RequestMapping(path = VendorBiddingSubscriptionController.BASE_REQUEST_PATH)
@PreAuthorize("hasAuthority('VENDOR')")
public class VendorBiddingSubscriptionController extends BiddingSubscriptionController {

	protected static final String BASE_REQUEST_PATH = "/vendors/bidding-subscriptions";

	@Override
	protected String getBaseRequestURI() {
		return BASE_REQUEST_PATH;
	}

	@Override
	protected BiddingSubscriptionFor getBiddingSubscriptionFor() {
		return BiddingSubscriptionFor.vendor;
	}

	public static String getBiddingSubscriptionDetailsUrl(String subscriptionId) {
		return BASE_REQUEST_PATH + String.format("/%s/details", subscriptionId);
	}
}
