package com.bnbneeds.portal.dealer.exception;

public class APIException extends RuntimeException {

	private static final long serialVersionUID = -2651397900659969583L;

	protected static String getMessage(String messageFormat, String... args) {
		return String.format(messageFormat, args);
	}

	public APIException() {
		super();
	}

	public APIException(String message) {
		super(message);
	}

	public APIException(Throwable cause) {
		super(cause);
	}
	

}
