package com.bnbneeds.portal.dealer.formbean;

public class BuyerEnquiryListRequestBean extends EnquiryListRequestBean {

	private String vendor;

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

}
