package com.bnbneeds.portal.dealer.formbean.mapper;

import com.bnbneeds.app.model.dealer.BuyerInfoResponse;
import com.bnbneeds.app.model.dealer.CreateBuyerParam;
import com.bnbneeds.app.model.dealer.CreateVendorParam;
import com.bnbneeds.app.model.dealer.DealerInfoResponse;
import com.bnbneeds.app.model.dealer.DealerParam;
import com.bnbneeds.app.model.dealer.UpdateBuyerParam;
import com.bnbneeds.app.model.dealer.UpdateVendorParam;
import com.bnbneeds.app.model.dealer.VendorInfoResponse;
import com.bnbneeds.portal.dealer.formbean.CreateBuyerBean;
import com.bnbneeds.portal.dealer.formbean.DealerRegistrationBean;
import com.bnbneeds.portal.dealer.formbean.UpdateBuyerInfoBean;
import com.bnbneeds.portal.dealer.formbean.VendorBean;
import com.bnbneeds.portal.dealer.util.StringUtils;

public class DealerFormBeanMapper {

	public static void mapDealerRegistrationAttributes(
			DealerRegistrationBean bean, DealerParam param) {
		param.setAddress(bean.getAddress());
		param.setContactName(bean.getContactName());
		String description = bean.getDescription();
		if (StringUtils.hasText(description)) {
			param.setDescription(description);
		}
		param.setEmailAddress(bean.getEmailAddress());
		param.setMobileNumber(bean.getMobileNumber());
		param.setOrganizationName(bean.getOrganizationName());
		param.setPan(bean.getPan());
		String website = bean.getWebsite();
		if (StringUtils.hasText(website)) {
			param.setWebsite(website);
		}

	}

	public static void mapVendorRegistrationAttributes(VendorBean bean,
			CreateVendorParam param) {
		mapDealerRegistrationAttributes(bean, param);
		String serviceTaxNumber = bean.getServiceTaxNumber();
		param.setServiceTaxNumber(serviceTaxNumber);
		String vatNumber = bean.getVatNumber();
		param.setVatNumber(vatNumber);
		param.setCities(bean.getCities());
	}

	public static void mapBuyerRegistrationAttributes(CreateBuyerBean bean,
			CreateBuyerParam param) {
		mapDealerRegistrationAttributes(bean, param);
		param.setBusinessTypeId(bean.getBusinessTypeId());
	}

	public static void mapBuyerUpdateAttributes(UpdateBuyerInfoBean bean,
			UpdateBuyerParam param) {
		mapDealerRegistrationAttributes(bean, param);
	}

	public static void mapVendorUpadateAttributes(VendorBean bean,
			UpdateVendorParam param) {
		mapVendorRegistrationAttributes(bean, param);
	}

	public static void mapDealerInfoResponseToFormBean(
			DealerInfoResponse param, DealerRegistrationBean bean) {
		bean.setAddress(param.getAddress());
		bean.setContactName(param.getContactName());
		String description = param.getDescription();
		if (StringUtils.hasText(description)) {
			bean.setDescription(description);
		}
		bean.setEmailAddress(param.getEmailAddress());
		bean.setMobileNumber(param.getMobileNumber());
		bean.setOrganizationName(param.getName());
		bean.setPan(param.getPan());
		String website = param.getWebsite();
		if (StringUtils.hasText(website)) {
			bean.setWebsite(website);
		}
	}

	public static void mapVendorInfoResponseToFormBean(
			VendorInfoResponse param, VendorBean bean) {
		mapDealerInfoResponseToFormBean(param, bean);
		bean.setServiceTaxNumber(param.getServiceTaxNumber());
		bean.setVatNumber(param.getVatNumber());
		bean.setCities(param.getCities());
	}

	public static void mapBuyerInfoResponseToFormBean(BuyerInfoResponse param,
			UpdateBuyerInfoBean bean) {
		mapDealerInfoResponseToFormBean(param, bean);
	}

}
