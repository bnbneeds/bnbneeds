package com.bnbneeds.portal.dealer.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;

import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.ClientType;

@Configuration
public class BNBNeedsClientInitializer {

	private static final Logger logger = LoggerFactory
			.getLogger(BNBNeedsClientInitializer.class);

	@Value("${apisvc.url}")
	private String apisvcURL;

	@Value("${request.logging.enabled}")
	private boolean requestLoggingEnabled;

	@Autowired
	private Environment env;

	@Scope(scopeName = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
	@Bean(name = "bnbneedsClient")
	public BNBNeedsClient init() {
		logger.info(
				"Initializing BNBNeeds client with client type: web and baseURL: {}",
				apisvcURL);
		BNBNeedsClient bnbneedsClient = new BNBNeedsClient(ClientType.web,
				apisvcURL);
		bnbneedsClient.getConfig().setRequestLoggingEnabled(
				requestLoggingEnabled);
		return bnbneedsClient;

	}

}
