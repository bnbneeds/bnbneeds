package com.bnbneeds.portal.dealer.formbean;

import org.json.JSONObject;

public class RazorpayCheckoutOrderBean {

	private String razorpay_payment_id;
	private String razorpay_order_id;
	private String razorpay_signature;

	public String getRazorpay_payment_id() {
		return razorpay_payment_id;
	}

	public String getRazorpay_order_id() {
		return razorpay_order_id;
	}

	public String getRazorpay_signature() {
		return razorpay_signature;
	}

	public void setRazorpay_signature(String razorpay_signature) {
		this.razorpay_signature = razorpay_signature;
	}

	public void setRazorpay_payment_id(String razorpay_payment_id) {
		this.razorpay_payment_id = razorpay_payment_id;
	}

	public void setRazorpay_order_id(String razorpay_order_id) {
		this.razorpay_order_id = razorpay_order_id;
	}

	public JSONObject getJSONObject() {
		JSONObject object = new JSONObject();
		object.put("razorpay_payment_id", razorpay_payment_id);
		object.put("razorpay_order_id", razorpay_order_id);
		object.put("razorpay_signature", razorpay_signature);
		return object;
	}

}
