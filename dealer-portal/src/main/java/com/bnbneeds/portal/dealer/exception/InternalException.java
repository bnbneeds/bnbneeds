package com.bnbneeds.portal.dealer.exception;

public class InternalException extends APIException {

	private static final long serialVersionUID = -8341119604658287297L;

	public InternalException() {
	}

	public InternalException(String message) {
		super(message);
	}

	public InternalException(Throwable cause) {
		super(cause);
	}

}
