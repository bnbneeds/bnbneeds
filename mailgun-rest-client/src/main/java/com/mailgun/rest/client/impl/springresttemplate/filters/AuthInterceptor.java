package com.mailgun.rest.client.impl.springresttemplate.filters;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class AuthInterceptor implements ClientHttpRequestInterceptor {

	private static final Logger log = LoggerFactory.getLogger(AuthInterceptor.class);

	private static final String MAILGUN_API_USERNAME = "api";
	private String apiKey;

	public AuthInterceptor(String apiKey) {
		this.apiKey = apiKey;
	}

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {

		HttpHeaders headers = request.getHeaders();

		HttpAuthentication authHeader = new HttpBasicAuthentication(MAILGUN_API_USERNAME, apiKey);
		headers.setAuthorization(authHeader);

		return execution.execute(request, body);
	}
}
