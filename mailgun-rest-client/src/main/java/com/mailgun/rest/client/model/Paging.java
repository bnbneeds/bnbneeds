package com.mailgun.rest.client.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class Paging  {

	private String first;
	private String last;
	private String next;
	private String previous;

@XmlElement(name = "first")
	public String getFirst() {
	return first;
}

public void setFirst(String first) {
	this.first = first;
}

@XmlElement(name = "last")
public String getLast() {
	return last;
}

public void setLast(String last) {
	this.last = last;
}

@XmlElement(name = "next")
public String getNext() {
	return next;
}

public void setNext(String next) {
	this.next = next;
}

@XmlElement(name = "previous")
public String getPrevious() {
	return previous;
}

public void setPrevious(String previous) {
	this.previous = previous;
}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Paging [");
		builder.append(super.toString());
		if (first != null)
			builder.append("first=").append(first);
		if (last != null)
			builder.append("last=").append(last);
		if (next != null)
			builder.append("next=").append(next);
		if (previous != null)
			builder.append("previous=").append(previous);
		builder.append("]");
		return builder.toString();
	}

}
