package com.mailgun.rest.client;

import java.net.URI;
import java.text.MessageFormat;
import java.util.List;

import javax.ws.rs.core.Form;

import org.apache.cxf.common.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mailgun.rest.client.exceptions.MailgunClientException;
import com.mailgun.rest.client.model.AddMailingListMemberParam;
import com.mailgun.rest.client.model.AddMailingListMembersResponse;
import com.mailgun.rest.client.model.CreateMailingListParam;
import com.mailgun.rest.client.model.MailingListMemberResponse;
import com.mailgun.rest.client.model.MailingListMembersResponse;
import com.mailgun.rest.client.model.MailingListResponse;
import com.mailgun.rest.client.model.MailingListsResponse;
import com.mailgun.rest.client.model.UpdateMailingListParam;

public class MailgunClient {
	protected RestClient client;
	
	private static final String ADDRESS_PARAM_NAME = "address";
	private static final  String NAME_PARAM_NAME = "name";
	private static final  String DESCRIPTION_PARAM_NAME = "description";
	private static final  String ACCESS_LEVEL_PARAM_NAME = "access_level";
	private static final  String SUBSCRIBED_PARAM_NAME = "subscribed";
	private static final  String UPSERT_PARAM_NAME = "upsert";
	private static final  String MEMBERS_PARAM_NAME = "members";
	
	private static enum Endpoint {

		MAILGUN_BASE_URI("https://api.mailgun.net/v3"),
		MAILGUN_MAILING_LISTS("/lists/pages"),
		MAILGUN_MAILING_LIST("/lists/{0}"),
		MAILGUN_CREATE_MAILING_LIST("/lists"),
		MAILGUN_UPDATE_MAILING_LIST("/lists/{0}"),
		MAILGUN_DELETE_MAILING_LIST("/lists/{0}"),
		MAILGUN_MAILING_LIST_MEMBERS("/lists/{0}/members/pages"),
		MAILGUN_MAILING_LIST_MEMBER("/lists/{0}/members/{1}"),
		MAILGUN_ADD_MAILING_LIST_MEMBER("/lists/{0}/members"),
		MAILGUN_ADD_MAILING_LIST_MEMBERS("/lists/{0}/members.json"),
		MAILGUN_UPDATE_MAILING_LIST_MEMBER("/lists/{0}/members/{1}"),
		MAILGUN_DELETE_MAILING_LIST_MEMBER("/lists/{0}/members/{1}"),
		;
		private String endpoint;

		private Endpoint(String endpoint) {
			this.endpoint = endpoint;

		}
		
		public URI get() {
			return URI.create( endpoint);
		}
		
		public URI get(String ...args) {
			return URI.create( MessageFormat.format(endpoint, (Object[])args));
		}
	}
	public MailgunClient(RestClient client) {
		this.client = client;
	}

	public MailgunClient(String host) {
		this(new ClientConfig().withHost(host));
	}

	public MailgunClient(ClientConfig config) {
		this(config.newClient());
	}
	
	public MailgunClient(ClientType clientType, String mailgunDomain, String mailgunApiKey) {
		ClientConfig clientConfig = new ClientConfig()
				.withClientType(clientType).withBaseURI(Endpoint.MAILGUN_BASE_URI.get().toString()).withApiKey(mailgunApiKey).withDomain(mailgunDomain);
		this.client = clientConfig.newClient();
		
	}

	public RestClient getClient() {
		return this.client;
	}
	
	public ClientConfig getConfig() {
		return client.getConfig();
	}
	
	public static synchronized MailgunClient newClient(ClientType clientType,
			String baseURL) {

		URI uri = URI.create(baseURL);
		return newClient(clientType);
	}
	
	public static synchronized MailgunClient newClient(ClientType clientType) {

		ClientConfig clientConfig = new ClientConfig()
				.withClientType(clientType);
		return new MailgunClient(clientConfig);
	}
	
	public static synchronized MailgunClient newClient(ClientType clientType, String domain, String apiKey) {

		ClientConfig clientConfig = new ClientConfig()
				.withClientType(clientType).withApiKey(apiKey);
		return new MailgunClient(clientType, domain, apiKey);
	}

	public MailingListsResponse  getMailingLists() {
		return client.get(MailingListsResponse.class,
				Endpoint.MAILGUN_MAILING_LISTS.get());
	}
	
	public MailingListResponse  getMailingList(String address) {
		return client.get(MailingListResponse.class,
				Endpoint.MAILGUN_MAILING_LIST.get(address));
	}
	
	public MailingListsResponse createMailingList(CreateMailingListParam reqParam) {
		
		Form form = new Form();
		form.param(ADDRESS_PARAM_NAME, reqParam.getAddress());
		form.param(DESCRIPTION_PARAM_NAME, reqParam.getDescription());
		form.param(NAME_PARAM_NAME, reqParam.getName());
		form.param(ACCESS_LEVEL_PARAM_NAME, reqParam.getAccessLevel());
		return client.post(MailingListsResponse.class, form,
				Endpoint.MAILGUN_CREATE_MAILING_LIST.get());
	}
	
public MailingListResponse updateMailingList(UpdateMailingListParam reqParam, String currentAddress) {
		
		Form form = new Form();
		if(!StringUtils.isEmpty(reqParam.getAddress()))
			form.param(ADDRESS_PARAM_NAME, reqParam.getAddress());
		if(!StringUtils.isEmpty(reqParam.getDescription()))
			form.param(DESCRIPTION_PARAM_NAME, reqParam.getDescription());
		if(!StringUtils.isEmpty(reqParam.getName()))
			form.param(NAME_PARAM_NAME, reqParam.getName());
		if(!StringUtils.isEmpty(reqParam.getAccessLevel()))
			form.param(ACCESS_LEVEL_PARAM_NAME, reqParam.getAccessLevel());
		return client.put(MailingListResponse.class, form,
				Endpoint.MAILGUN_UPDATE_MAILING_LIST.get(currentAddress));
	}
	
	
	public MailingListResponse  deleteMailingList(String address) {
		return client.delete(MailingListResponse.class,Endpoint.MAILGUN_DELETE_MAILING_LIST.get(address));
	}
	
	public MailingListMembersResponse  getMailingListMembers(String address) {
		return client.get(MailingListMembersResponse.class, Endpoint.MAILGUN_MAILING_LIST_MEMBERS.get(address));
	}
	
	public MailingListMembersResponse  getMailingListMembersWithPaginationURL(String paginationUrl) {
		return client.get(MailingListMembersResponse.class, paginationUrl);
	}
	
	public MailingListMemberResponse  getMailingListMember(String mailingListAddress, String memberAddress) {
		return client.get(MailingListMemberResponse.class,
				Endpoint.MAILGUN_MAILING_LIST_MEMBER.get(mailingListAddress,memberAddress));
	}
	
	public MailingListMemberResponse addMailingListMember(AddMailingListMemberParam reqParam,
			String mailingListAddress ) {
		
		Form form = new Form();
		form.param(ADDRESS_PARAM_NAME, reqParam.getAddress());
		form.param(DESCRIPTION_PARAM_NAME, reqParam.getDescription());
		form.param(NAME_PARAM_NAME, reqParam.getName());
		if(!StringUtils.isEmpty(reqParam.getSubscribed()))
			form.param(SUBSCRIBED_PARAM_NAME, reqParam.getSubscribed());
		if(!StringUtils.isEmpty(reqParam.getUpsert()))
			form.param(UPSERT_PARAM_NAME, reqParam.getUpsert());
		
		return client.post(MailingListMemberResponse.class, form,
				Endpoint.MAILGUN_ADD_MAILING_LIST_MEMBER.get(mailingListAddress));
	}
	
	public AddMailingListMembersResponse addMailingListMembers(List<AddMailingListMemberParam> reqParam,
			String mailingListAddress ) {
		
		Form form = new Form();
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		try {
			String membersList = mapper.writeValueAsString(reqParam);
			form.param(MEMBERS_PARAM_NAME, membersList);
		} catch (JsonProcessingException e) {
			throw new MailgunClientException("Failed while parsing members list: " + e.getMessage());
		}
		return client.post(AddMailingListMembersResponse.class, form,
				Endpoint.MAILGUN_ADD_MAILING_LIST_MEMBERS.get(mailingListAddress));
	}
	
	public MailingListMemberResponse updateMailingListMember(AddMailingListMemberParam reqParam,
		String mailingListAddress,String memberAddress ) {
	
		Form form = new Form();
		if(!StringUtils.isEmpty(reqParam.getAddress()))
		form.param(ADDRESS_PARAM_NAME, reqParam.getAddress());
		if(!StringUtils.isEmpty(reqParam.getDescription()))
		form.param(DESCRIPTION_PARAM_NAME, reqParam.getDescription());
		if(!StringUtils.isEmpty(reqParam.getName()))
		form.param(NAME_PARAM_NAME, reqParam.getName());
		if(!StringUtils.isEmpty(reqParam.getSubscribed()))
		form.param(SUBSCRIBED_PARAM_NAME, reqParam.getSubscribed());
		return client.put(MailingListMemberResponse.class, form,
				Endpoint.MAILGUN_UPDATE_MAILING_LIST_MEMBER.get(mailingListAddress,memberAddress));
	}

	public MailingListMemberResponse  deleteMailingListMember(String mailingListAddress,String memberAddress) {
		return client.delete(MailingListMemberResponse.class,Endpoint.MAILGUN_DELETE_MAILING_LIST_MEMBER.get(mailingListAddress, memberAddress));
	}


}
