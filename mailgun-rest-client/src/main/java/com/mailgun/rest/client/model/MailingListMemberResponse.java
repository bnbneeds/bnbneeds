package com.mailgun.rest.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "list")
public class MailingListMemberResponse {
	
	private MailingListMemberParam mailingListMember ;
	private String message;

	@XmlElement(name = "message")
	@JsonProperty("message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	@XmlElement(name = "member")
	@JsonProperty("member")	
	public MailingListMemberParam getMailingListMember() {
		return mailingListMember;
	}

	public void setMailingListMember(MailingListMemberParam mailingListMember) {
		this.mailingListMember = mailingListMember;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MailingListMemberResponse [");
		builder.append(super.toString());
		mailingListMember.toString();
		builder.append("]");
		return builder.toString();
	}

}