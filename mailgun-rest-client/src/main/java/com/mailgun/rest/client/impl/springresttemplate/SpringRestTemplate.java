package com.mailgun.rest.client.impl.springresttemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mailgun.rest.client.ClientConfig;
import com.mailgun.rest.client.RestClient;
import com.mailgun.rest.client.impl.springresttemplate.filters.AuthInterceptor;
import com.mailgun.rest.client.impl.springresttemplate.filters.MailgunReponseErrorHandler;
import com.mailgun.rest.client.impl.springresttemplate.filters.MediaTypeInterceptor;

public class SpringRestTemplate implements RestClient {

	private static final Logger log = LoggerFactory
			.getLogger(SpringRestTemplate.class.getSimpleName());

	private RestTemplate client;
	protected String apiBaseUri;
	protected ClientConfig config;

	private ClientHttpRequestFactory clientHttpRequestFactory(
			ClientConfig config) {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setReadTimeout(config.getReadTimeout());
		factory.setConnectTimeout(config.getConnectionTimeout());
		return factory;
	}

	public SpringRestTemplate( ClientConfig config, String apiBaseUri) {
		super();
		this.apiBaseUri =  apiBaseUri;
		this.config = config;

		log.info("Base URL: {}", apiBaseUri);
		if (config.getSocketFactory() != null) {
			HttpsURLConnection.setDefaultSSLSocketFactory(config
					.getSocketFactory());
		}
		if (config.getHostnameVerifier() != null) {
			HttpsURLConnection.setDefaultHostnameVerifier(config
					.getHostnameVerifier());
		}
	}

	@Override
	public ClientConfig getConfig() {
		return config;
	}

	public RestTemplate getClient() {
		if (client == null) {
			client = new RestTemplate(clientHttpRequestFactory(config));
			client.setMessageConverters(getMessageConverters());

			client.setErrorHandler(new MailgunReponseErrorHandler(config
					.getMediaType()));
			List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
			interceptors.add(new MediaTypeInterceptor(config.getMediaType()));
			interceptors.add(new AuthInterceptor(config.getApiKey()));
			/*
			 * uncomment this line to log the http request and response to the api.
			 */
			//interceptors.add(new LogInOutInterceptor());
			client.setInterceptors(interceptors);
			
			
		}
		return client;
	}

	protected List<HttpMessageConverter<?>> getMessageConverters() {
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		messageConverters.add(new ByteArrayHttpMessageConverter());
		messageConverters.add(new StringHttpMessageConverter());
		messageConverters.add(new ResourceHttpMessageConverter());
		messageConverters.add(new FormHttpMessageConverter());
		/*
		 * MappingJackson2HttpMessageConverter is initialized with mediaType as
		 * application/json by default. So no need to set mediaType explicitly.
		 */
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		ObjectMapper mapper = getObjectMapper();
		converter.setObjectMapper(mapper);
		messageConverters.add(converter);
		return messageConverters;
	}
	
	protected ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();

		mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		return mapper;
	}
	
	private UriBuilder uriBuilder() {
		return UriBuilder.fromUri(apiBaseUri);
	}

	private URI uriBuilder(String relativePath) {
		return uriBuilder().path(relativePath).build();
	}

	private URI uriBuilder(String relativePath, Properties queryParams) {
		UriBuilder builder = uriBuilder().path(relativePath);
		for (String prop : queryParams.stringPropertyNames()) {
			builder = builder.queryParam(prop, queryParams.getProperty(prop));
		}
		return builder.build();
	}

	private <T> T getEntity(ResponseEntity<T> response) {
		return response.getBody();
	}

	@Override
	public <T> T get(Class<T> responseType, String path) {
		ResponseEntity<T> response = getClient().getForEntity(uriBuilder(path),
				responseType);
		return getEntity(response);
	}

	@Override
	public <T> T get(Class<T> responseType, String path, Object... args) {
		ResponseEntity<T> response = getClient().getForEntity(
				uriBuilder(path).toString(), responseType, args);
		return getEntity(response);

	}

	@Override
	public <T> T get(Class<T> responseType, URI path) {
		return get(responseType, path.toString());
	}

	@Override
	public <T> T get(Class<T> responseType, URI path, Object... args) {
		return get(responseType, path.toString(), args);
	}

	@Override
	public <T> T post(Class<T> responseType, Object request, URI path) {
		
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED);
		
		MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();
		
		if(request instanceof Form){
			Form reqForm = (Form) request;
			MultivaluedMap<String, String> parameters = reqForm.asMap();
			
			for(Map.Entry<String, List<String>> entry : parameters.entrySet()){
				if(entry.getValue() != null && StringUtils.hasText(entry.getValue().get(0))){
					String value =  entry.getValue().get(0);
					String key = entry.getKey();
					requestBody.add(key, value);
				}
			}
		}
		
		HttpEntity<?> requestEntity = new HttpEntity<>(requestBody,requestHeaders);
		
		ResponseEntity<T> response = getClient().postForEntity(
				uriBuilder(path.toString()), requestEntity, responseType);
		return getEntity(response);
	}
	
	
	public <T> T post(Class<T> responseType, Object request, String path) {
		return post(responseType, request, uriBuilder(path));
	}

	@Override
	public <T> T post(Class<T> responseType, Object request, String path,
			Object... args) {
		ResponseEntity<T> response = getClient().postForEntity(
				uriBuilder(path).toString(), request, responseType, args);
		return getEntity(response);
	}

	@Override
	public <T> T delete(Class<T> responseType, URI path) {
		ResponseEntity<T> response = getClient().exchange(
				path, HttpMethod.DELETE, null, responseType);
		return getEntity(response);
	}
	

	public <T> T delete(Class<T> responseType, String path) {
		return delete(responseType, uriBuilder(path));
	}

	@Override
	public <T> T put(Class<T> responseType, Object request, URI path) {
		HttpEntity<?> requestEntity = new HttpEntity<Object>(request);
		ResponseEntity<T> response = getClient().exchange(path,
				HttpMethod.PUT, requestEntity, responseType);
		return getEntity(response);
	}
	
	
	public <T> T put(Class<T> responseType, Object request, String path) {
		return put(responseType, request, uriBuilder(path));
	}



}
