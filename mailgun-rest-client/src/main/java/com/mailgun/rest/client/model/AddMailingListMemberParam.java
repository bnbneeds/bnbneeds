package com.mailgun.rest.client.model;

import java.util.Map;

public class AddMailingListMemberParam {

	private String address;
	private String subscribed;
	private String name;
	private String description;
	private String upsert;
	private Map<String, String> vars;
	
	public String getUpsert() {
		return upsert;
	}
	public void setUpsert(String upsert) {
		this.upsert = upsert;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getSubscribed() {
		return subscribed;
	}
	public void setSubscribed(String subscribed) {
		this.subscribed = subscribed;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Map<String, String> getVars() {
		return vars;
	}
	public void setVars(Map<String, String> vars) {
		this.vars = vars;
	}
	
	
}