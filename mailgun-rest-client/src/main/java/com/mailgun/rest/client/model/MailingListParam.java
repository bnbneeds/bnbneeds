package com.mailgun.rest.client.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class MailingListParam  {

	private String accessLevel;
	private String address;
	private String description;
	private String name;
	private String membersCount;
	private String createdAt;


@XmlElement(name = "created_at")
public String getCreatedAt() {
	return createdAt;
}

public void setCreatedAt(String createdAt) {
	this.createdAt = createdAt;
}

@XmlElement(name = "access_level")
public String getAccessLevel() {
	return accessLevel;
}

public void setAccessLevel(String accessLevel) {
	this.accessLevel = accessLevel;
}

@XmlElement(name = "address")
public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

@XmlElement(name = "description")
public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

@XmlElement(name = "name")
public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

@XmlElement(name = "members_count")
public String getMembersCount() {
	return membersCount;
}

public void setMembersCount(String membersCount) {
	this.membersCount = membersCount;
}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MailingListResponse [");
		builder.append(super.toString());
		if (accessLevel != null)
			builder.append("accessLevel=").append(accessLevel);
		if (address != null)
			builder.append("address=").append(address);
		if (name != null)
			builder.append("name=").append(name);
		if (membersCount != null)
			builder.append("membersCount=").append(membersCount);
		builder.append("]");
		return builder.toString();
	}

}
