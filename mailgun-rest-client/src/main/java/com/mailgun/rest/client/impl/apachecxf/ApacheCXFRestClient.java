package com.mailgun.rest.client.impl.apachecxf;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status.Family;

import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.jaxrs.client.ClientConfiguration;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.mailgun.rest.client.ClientConfig;
import com.mailgun.rest.client.RestClient;
import com.mailgun.rest.client.exceptions.MailgunClientException;
import com.mailgun.rest.client.exceptions.MailgunServiceException;
import com.mailgun.rest.client.model.TaskResponse;
import com.mailgun.rest.client.util.ClientUtils;;

public class ApacheCXFRestClient<T>  implements RestClient {

	private static final Logger log = LoggerFactory
			.getLogger(ApacheCXFRestClient.class);

	private WebClient client;
	
	protected String apiBaseUri;
	protected ClientConfig config;
	private static final String MAILGUN_API_USERNAME = "api";
	
	public String getApiBaseUri() {
		return apiBaseUri;
	}

	public void setApiBaseUri(String apiBaseUri) {
		this.apiBaseUri = apiBaseUri;
	}

	public ApacheCXFRestClient(ClientConfig config, String apiBaseUri) {
		super();
		this.apiBaseUri = apiBaseUri;
		this.config = config;

		log.info("Base URL: {}", apiBaseUri);
		if (config.getSocketFactory() != null) {
			HttpsURLConnection.setDefaultSSLSocketFactory(config
					.getSocketFactory());
		}
		if (config.getHostnameVerifier() != null) {
			HttpsURLConnection.setDefaultHostnameVerifier(config
					.getHostnameVerifier());
		}
	}

	@Override
	public ClientConfig getConfig() {
		return config;
	}

	public WebClient getClient() {
		if (client == null) {
			List<Object> providers = new ArrayList<Object>();

			providers.add(getJacksonJaxbJsonProvider());

			client = WebClient.create(apiBaseUri, providers);

			ClientConfiguration apacheCXFClientconfig = WebClient
					.getConfig(client);

			HTTPConduit conduit = apacheCXFClientconfig.getHttpConduit();
			
			// set basic authorization for mailgun
			AuthorizationPolicy authorization = new AuthorizationPolicy();
			authorization.setUserName(MAILGUN_API_USERNAME);
			authorization.setPassword(config.getApiKey());
			conduit.setAuthorization(authorization);
			
			HTTPClientPolicy httpClientPolicy = conduit.getClient();
			httpClientPolicy
					.setConnectionTimeout(config.getConnectionTimeout());
			httpClientPolicy.setReceiveTimeout(config.getReadTimeout());
			httpClientPolicy.setAutoRedirect(true);
		}
		return client;
	}
	
	protected JacksonJaxbJsonProvider getJacksonJaxbJsonProvider() {
		JacksonJaxbJsonProvider jaxbJsonProvider = new JacksonJaxbJsonProvider(
				getObjectMapper(), JacksonJaxbJsonProvider.DEFAULT_ANNOTATIONS);
		return jaxbJsonProvider;

	}

	protected ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();

		mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

		return mapper;
	}
	
	protected List<HttpMessageConverter<?>> getMessageConverters() {
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		messageConverters.add(new ByteArrayHttpMessageConverter());
		messageConverters.add(new StringHttpMessageConverter());
		messageConverters.add(new ResourceHttpMessageConverter());
		/*
		 * MappingJackson2HttpMessageConverter is initialized with mediaType as
		 * application/json by default. So no need to set mediaType explicitly.
		 */
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		ObjectMapper mapper = getObjectMapper();
		converter.setObjectMapper(mapper);
		messageConverters.add(converter);
		return messageConverters;
	}
	
	private void throwError(WebApplicationException ex) {
		// If its an exception thrown, then it will always be a response of type
		// TaskResponse.
		Response r = ex.getResponse();
		int responseCode = r.getStatus();
		TaskResponse task = null;

		task = parseResponse(r, TaskResponse.class);
		MailgunServiceException e = null;
		if (task != null) {
			e = new MailgunServiceException(responseCode, task);
		} else {
			Family responseFamily = r.getStatusInfo().getFamily();
			switch (responseFamily) {
			case CLIENT_ERROR:
			case SERVER_ERROR:
			case OTHER:
				e = new MailgunServiceException(responseCode,
						"Request failed. Response: HTTP code " + responseCode
								+ " - " + r.getStatusInfo().getReasonPhrase());
				break;
			default:
				return;
			}

		}
		throw e;
	}

	private void throwError(TaskResponse task) {
		if (task != null) {
			int httpCode = task.getHttpCode();

			Family responseFamily = Family.familyOf(httpCode);
			MailgunServiceException e = null;

			switch (responseFamily) {
			case CLIENT_ERROR:
			case SERVER_ERROR:
			case OTHER:
				e = new MailgunServiceException(httpCode, task);
				break;
			default:
				return;
			}
			throw e;
		}
	}

	private <T> T parseResponse(Response r, Class<T> responseType) {

		T task = null;
		try {
			String type = (String) r.getMetadata().getFirst(
					HttpHeaders.CONTENT_TYPE);
			Object entity = r.getEntity();
			task = ClientUtils.parseObjectToPOJO(entity, responseType, type);
		} catch (IOException e) {
			throw new MailgunClientException("IOException occured", e);
		}

		return task;
	}

	public WebClient resource(URI uri) {
		return getClient().back(true).reset().resetQuery().path(uri.toString())
				.accept(config.getMediaType()).type(config.getMediaType());
	}
	
	public WebClient resource(URI uri, Object... args) {
		return getClient().back(true).reset().resetQuery()
				.path(uri.toString(), args).accept(config.getMediaType())
				.type(config.getMediaType());
	}

	public WebClient resourceMultiPart(URI uri) {
		return getClient().back(true).reset().resetQuery().path(uri.toString())
				.accept(config.getMediaType())
				.type(MediaType.MULTIPART_FORM_DATA);
	}

	public WebClient resourceMultiPart(URI uri, Object... args) {
		return getClient().back(true).reset().resetQuery()
				.path(uri.toString(), args).accept(config.getMediaType())
				.type(MediaType.MULTIPART_FORM_DATA);
	}

	public WebClient resource(String uri) {
		return getClient().back(true).reset().resetQuery().path(uri)
				.accept(config.getMediaType()).type(config.getMediaType());
	}
	
	public WebClient resource(String uri, Object... args) {
		return getClient().back(true).reset().resetQuery().path(uri, args)
				.accept(config.getMediaType()).type(config.getMediaType());
	}

	public WebClient resource(String uri, Properties queryParams) {
		WebClient client = getClient().reset().resetQuery().back(true)
				.path(uri).accept(config.getMediaType())
				.type(config.getMediaType());
		for (String prop : queryParams.stringPropertyNames()) {
			client.query(prop, queryParams.getProperty(prop));
		}
		return client;
	}

	public WebClient resource(URI uri, Properties queryParams) {
		return resource(uri.toString(), queryParams);
	}

	public WebClient resource(String uri, Properties queryParams,
			Object... args) {
		WebClient client = getClient().back(true).path(uri, args).reset()
				.resetQuery().accept(config.getMediaType())
				.type(config.getMediaType());
		for (String prop : queryParams.stringPropertyNames()) {
			client.query(prop, queryParams.getProperty(prop));
		}
		return client;
	}

	public WebClient resource(URI uri, Properties queryParams, Object... args) {
		return resource(uri.toString(), queryParams, args);
	}

	public WebClient resourceMultiPart(String uri) {
		return getClient().back(true).path(uri).accept(config.getMediaType())
				.type(MediaType.MULTIPART_FORM_DATA);
	}

	public WebClient resourceMultiPart(String uri, Object... args) {
		return getClient().back(true).path(uri, args)
				.accept(config.getMediaType())
				.type(MediaType.MULTIPART_FORM_DATA);
	}
	
	@Override
	public <T> T get(Class<T> responseType, URI uri) {
		return get(responseType, uri.toString());
	}
	
	@Override
	public <T> T get(Class<T> responseType, URI uri, Object... args) {
		return get(responseType, uri.toString(),args);
	}


	@Override
	public <T> T get(Class<T> responseType, String path) {
		T response = null;
		try {
			response = resource(path).get(responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}catch (Exception e) {
			new MailgunServiceException(404, e.getMessage());
		}
		return response;
	}

	@Override
	public <T> T get(Class<T> responseType, String path, Object... args) {
		T response = null;
		try {
			response = resource(path, args).get(responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}
	
	@Override
	public <T> T post(Class<T> responseType, Object request, URI path) {
		T response = null;
		try {
			response = resource(path).post(request, responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T post(Class<T> responseType, Object request, String path,
			Object... args) {
		T response = null;
		try {
			response = resource(path, args).post(request, responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}
	
	@Override
	public <T> T delete(Class<T> responseType, URI path) {
		T response = null;
		try {
			Response r = resource(path).delete();
			response = parseResponse(r, responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	
	@Override
	public <T> T put(Class<T> responseType, Object request, URI path) {
		T response = null;
		try {
			response = resource(path).put(request, responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

}
