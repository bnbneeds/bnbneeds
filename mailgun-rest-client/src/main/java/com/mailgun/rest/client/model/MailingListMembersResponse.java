package com.mailgun.rest.client.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "items_list")
public class MailingListMembersResponse {
	
	private List<MailingListMemberParam> mailingListMembers ;
	private Paging paging;

	@XmlElement(name = "paging")
	@JsonProperty("paging")
	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

	@XmlElement(name = "items")
	@JsonProperty("items")
	public List<MailingListMemberParam> getMailingListMembers() {
		return mailingListMembers;
	}

	public void setMailingLists(List<MailingListMemberParam> dataObjectList) {
		this.mailingListMembers = dataObjectList;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MailingListsResponse [");
		builder.append(super.toString());
		for(MailingListMemberParam mailingListMember : mailingListMembers){
			mailingListMember.toString();
		}
		paging.toString();
		builder.append("]");
		return builder.toString();
	}
	
	public boolean isEmpty(){
		return size() == 0;
	}
	
	public int size() {
		return mailingListMembers == null ? 0 : mailingListMembers.size();
	}

}