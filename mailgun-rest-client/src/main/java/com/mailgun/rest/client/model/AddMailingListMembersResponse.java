package com.mailgun.rest.client.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "response")
public class AddMailingListMembersResponse {
	
	private AddMailingListMembersResponseParam listParam ;
	private String message;
	private String taskId;

	@XmlElement(name = "task_id")
	@JsonProperty("task-id")
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@XmlElement(name = "message")
	@JsonProperty("message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	@XmlElement(name = "list")
	@JsonProperty("list")	
	public AddMailingListMembersResponseParam getListParam() {
		return listParam;
	}

	public void setListParam(AddMailingListMembersResponseParam listParam) {
		this.listParam = listParam;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AddMailingListMembersResponse [");
		builder.append(super.toString());
		listParam.toString();
		builder.append("]");
		return builder.toString();
	}

}