package com.mailgun.rest.client.impl.springresttemplate.filters;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatus.Series;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;

import com.mailgun.rest.client.model.TaskResponse;
import com.mailgun.rest.client.exceptions.MailgunClientException;
import com.mailgun.rest.client.exceptions.MailgunServiceException;
import com.mailgun.rest.client.util.ClientUtils;

public class MailgunReponseErrorHandler implements ResponseErrorHandler {

	private DefaultResponseErrorHandler defaultHandler;
	private String acceptedMediaType;

	public MailgunReponseErrorHandler(String acceptedMediaType) {
		defaultHandler = new DefaultResponseErrorHandler();
		this.acceptedMediaType = acceptedMediaType;
	}

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		return defaultHandler.hasError(response);
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		TaskResponse task = null;
		try {
			task = ClientUtils.parseObjectToPOJO(response.getBody(),
					TaskResponse.class, acceptedMediaType);
			HttpStatus status = response.getStatusCode();

			MailgunServiceException e = null;
			if (task != null) {
				e = new MailgunServiceException(response.getRawStatusCode(),
						task);
			} else {
				e = new MailgunServiceException(response.getRawStatusCode(),
						"Request failed. Response message: "
								+ status.getReasonPhrase());
			}
			Series series = status.series();
			switch (series) {
			case CLIENT_ERROR:
			case SERVER_ERROR:
				throw e;
			default:
				break;

			}
		} catch (IOException e) {
			throw new MailgunClientException("Error parsing response.", e);
		}

		try {
			defaultHandler.handleError(response);
		} catch (RestClientException e) {
			throw new MailgunClientException(e);
		}
	}
}
