package com.mailgun.rest.client.model;

import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class MailingListMemberParam  {

	private String address;
	private String subscribed;
	private String name;
	private String description;
	private Map<String, String> vars;

@XmlElement(name = "vars")
public Map<String, String> getVars() {
	return vars;
}

public void setVars(Map<String, String> vars) {
	this.vars = vars;
}

@XmlElement(name = "description")
public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

@XmlElement(name = "address")
public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

@XmlElement(name = "subscribed")
public String getSubscribed() {
	return subscribed;
}

public void setSubscribed(String subscribed) {
	this.subscribed = subscribed;
}

@XmlElement(name = "name")
public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MailingListMemberParam [");
		builder.append(super.toString());
		if (address != null)
			builder.append("address=").append(address);
		if (name != null)
			builder.append("name=").append(name);
		if (subscribed != null)
			builder.append("subscribed=").append(subscribed);
		builder.append("]");
		return builder.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		boolean retVal = false;

        if (obj instanceof MailingListMemberParam){
        	MailingListMemberParam ptr = (MailingListMemberParam) obj;
            retVal = ptr.getAddress().equalsIgnoreCase(this.getAddress());
        }
        
        return retVal;
	}

}
