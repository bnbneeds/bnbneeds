package com.mailgun.rest.client.model;

	import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

	@XmlRootElement(name = "create_mailinglist_response")
	public class CreateMailingListResponseParam {
		
		private MailingListParam mailingList ;
		private String message;

		@XmlElement(name = "message")
		@JsonProperty("message")
		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		@XmlElement(name = "list")
		@JsonProperty("list")
		public MailingListParam getMailingList() {
			return mailingList;
		}

		public void setMailingList(MailingListParam dataObjectList) {
			this.mailingList = dataObjectList;
		}
		
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("MailingListsResponse [");
			builder.append(super.toString());
			builder.append(mailingList.toString());
			builder.append(message);
			builder.append("]");
			return builder.toString();
		}

	}