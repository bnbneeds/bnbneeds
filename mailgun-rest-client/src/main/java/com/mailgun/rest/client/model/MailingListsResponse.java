package com.mailgun.rest.client.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "items_list")
public class MailingListsResponse {
	
	private List<MailingListParam> mailingLists ;
	private Paging paging;

	@XmlElement(name = "paging")
	@JsonProperty("paging")
	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

	@XmlElement(name = "items")
	@JsonProperty("items")
	public List<MailingListParam> getMailingLists() {
		return mailingLists;
	}

	public void setMailingLists(List<MailingListParam> dataObjectList) {
		this.mailingLists = dataObjectList;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MailingListsResponse [");
		builder.append(super.toString());
		for(MailingListParam mailingList : mailingLists){
			mailingLists.toString();
		}
		paging.toString();
		builder.append("]");
		return builder.toString();
	}

}