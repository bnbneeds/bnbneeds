package com.mailgun.rest.client.exceptions;

import com.mailgun.rest.client.model.TaskResponse;

public class MailgunServiceException extends RuntimeException {

	private static final long serialVersionUID = -242549080887793797L;

	private int httpCode;
	private TaskResponse task;

	public MailgunServiceException(int httpCode) {
		super();
		this.httpCode = httpCode;
	}

	public MailgunServiceException(int httpCode, TaskResponse task) {
		super(task.toString());
		this.httpCode = httpCode;
		this.task = task;
	}

	public MailgunServiceException(int httpCode, String message) {
		super(message);
		this.httpCode = httpCode;
	}

	public int getHttpCode() {
		return httpCode;
	}

	public TaskResponse getTask() {
		return task;
	}

}
