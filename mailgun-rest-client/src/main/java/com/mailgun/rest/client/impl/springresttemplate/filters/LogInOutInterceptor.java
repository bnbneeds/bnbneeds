package com.mailgun.rest.client.impl.springresttemplate.filters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class LogInOutInterceptor implements ClientHttpRequestInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(LogInOutInterceptor.class);

	private void traceRequest(HttpRequest request, byte[] body) throws IOException {
		logger.debug("===========================Request Begin================================================");
		logger.debug("URI         : {}", request.getURI());
		logger.debug("Method      : {}", request.getMethod());
		logger.debug("Headers     : {}", request.getHeaders());
		logger.debug("Request body: {}", new String(body, "UTF-8"));
		logger.debug("==========================Request End================================================");
	}

	private void traceResponse(ClientHttpResponse response) throws IOException {
		StringBuffer inputStringBuilder = new StringBuffer();
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(response.getBody(), "UTF-8"));
			String line = bufferedReader.readLine();
			while (line != null) {
				inputStringBuilder.append(line);
				inputStringBuilder.append('\n');
				line = bufferedReader.readLine();
			}
			logger.debug("============================Response Begin==========================================");
			logger.debug("Status code  : {}", response.getStatusCode());
			logger.debug("Status text  : {}", response.getStatusText());
			logger.debug("Headers      : {}", response.getHeaders());
			logger.debug("Response body: {}", inputStringBuilder.toString());
			logger.debug("===========================Response End==============================================");
		} finally {
			if (bufferedReader != null) {
				bufferedReader.close();
			}
		}
	}

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
		traceRequest(request, body);
		ClientHttpResponse response = execution.execute(request, body);
		traceResponse(response);
		return response;
	}
}
