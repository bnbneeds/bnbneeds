package com.mailgun.rest.client.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.validator.routines.UrlValidator;

import com.mailgun.rest.client.exceptions.MailgunClientException;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ClientUtils {
	/**
	 * Utility method that adds a query param to the builder. Only adds the
	 * query param if the value is not null.
	 * 
	 * @param builder
	 *            The builder reference to add the query param
	 * @param name
	 *            The name of the query param
	 * @param value
	 *            The value of the query param
	 */
	public static void addQueryParam(UriBuilder builder, String name,
			Object value) {
		if (value != null) {
			if (value instanceof List) {
				for (Object item : (List) value) {
					builder.queryParam(name, item);
				}
			} else {
				builder.queryParam(name, value);
			}
		}
	}

	public static ClientResponseContext makeRequest(
			ClientRequestContext request, ClientResponseContext response) {

		Client client = ClientBuilder.newClient(request.getConfiguration());
		String method = request.getMethod();
		MediaType mediaType = request.getMediaType();
		URI lUri = request.getUri();

		WebTarget resourceTarget = client.target(lUri);

		Invocation.Builder builder = resourceTarget.request(mediaType);

		MultivaluedMap<String, Object> newHeaders = new MultivaluedHashMap<String, Object>();

		for (Map.Entry<String, List<Object>> entry : request.getHeaders()
				.entrySet()) {
			if (HttpHeaders.AUTHORIZATION.equals(entry.getKey())) {
				continue;
			}
			newHeaders.put(entry.getKey(), entry.getValue());
		}

		Invocation invocation;
		if (request.getEntity() == null) {
			invocation = builder.build(method);
		} else {
			invocation = builder.build(method,
					Entity.entity(request.getEntity(), request.getMediaType()));
		}
		Response nextResponse = invocation.invoke();

		if (nextResponse.hasEntity()) {
			response.setEntityStream(nextResponse.readEntity(InputStream.class));
		}
		MultivaluedMap<String, String> headers = response.getHeaders();
		headers.clear();
		headers.putAll(nextResponse.getStringHeaders());
		response.setStatus(nextResponse.getStatus());

		return response;
	}

	public static <T> T parseResponseContextToPOJO(ClientResponseContext res,
			Class<T> responseType, String mediaType) throws JsonParseException,
			JsonMappingException, IOException {
		ObjectMapper mapper = null;
		T object = null;
		if (MediaType.APPLICATION_JSON.equals(mediaType)) {
			mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Include.NON_NULL);
		}

		/*
		 * Currently, we do not support parsing XML responses because: 1. It
		 * conflicts with libraries with android 2. The xml parsing libraries as
		 * a heavy footprint.
		 * 
		 * If you want to parse XML response, add this dependency: compile
		 * group: 'com.fasterxml.jackson.dataformat', name:
		 * 'jackson-dataformat-xml', version: '2.8.2'
		 * 
		 * and uncomment the code below.
		 * 
		 * if (MediaType.APPLICATION_XML.equals(mediaType)) { mapper = new
		 * XmlMapper();
		 * 
		 * }
		 */
		if (mapper != null) {
			object = mapper.readValue(res.getEntityStream(), responseType);
		}
		return object;
	}

	public static <T> T parseObjectToPOJO(Object source, Class<T> responseType,
			String mediaType) throws JsonParseException, JsonMappingException,
			IOException {

		if (source == null) {
			return null;
		}

		ObjectMapper mapper = null;
		T object = null;
		if (mediaType.contains(MediaType.APPLICATION_JSON)) {
			mapper = new ObjectMapper();
			mapper.configure(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME,
					true);
			mapper.setSerializationInclusion(Include.NON_NULL);
		}
		/*
		 * Currently, we do not support parsing XML responses because: 1. It
		 * conflicts with libraries with android 2. The xml parsing libraries as
		 * a heavy footprint.
		 * 
		 * If you want to parse XML response, add this dependency: compile
		 * group: 'com.fasterxml.jackson.dataformat', name:
		 * 'jackson-dataformat-xml', version: '2.8.2'
		 * 
		 * and uncomment the code below.
		 * 
		 * if (MediaType.APPLICATION_XML.equals(mediaType)) { mapper = new
		 * XmlMapper();
		 * 
		 * }
		 */
		if (mapper != null) {
			if (source instanceof InputStream) {
				InputStream in = (InputStream) source;
				object = mapper.readValue(in, responseType);
			} else if (source instanceof String) {
				String in = (String) source;
				object = mapper.readValue(in, responseType);
			} else {
				object = mapper.convertValue(source, responseType);
			}
		}
		return object;
	}

	public static void checkURLisValid(String url) {
		UrlValidator urlValidator = new UrlValidator(new String[] { "http",
				"https" }, UrlValidator.ALLOW_LOCAL_URLS);
		if (!urlValidator.isValid(url)) {
			throw new MailgunClientException("Base URL is invalid: " + url);
		}
	}

}
