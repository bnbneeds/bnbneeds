package com.mailgun.rest.client;

import java.net.URI;

public interface RestClient {

	public ClientConfig getConfig();

	public <T> T get(Class<T> responseType, URI path);

	public <T> T get(Class<T> responseType, URI path, Object... args);
	
	public <T> T get(Class<T> responseType, String path);

	public <T> T get(Class<T> responseType, String path, Object... args);

	public <T> T post(Class<T> responseType, Object request, URI uri);

	public <T> T post(Class<T> responseType, Object request, String path, Object[] args);

	public <T> T put(Class<T> responseType, Object request, URI path);

	public <T> T delete(Class<T> responseType, URI path);



}
