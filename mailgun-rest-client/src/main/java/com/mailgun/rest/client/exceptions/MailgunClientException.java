package com.mailgun.rest.client.exceptions;

import java.text.MessageFormat;

public class MailgunClientException extends RuntimeException {

	private static final long serialVersionUID = -3112107660206426291L;

	private static final String REQUEST_BODY_NULL = "Request body is null.";
	private static final String MEHTOD_SUPPORTED = "This method is not supported for client: [{0}]";

	public MailgunClientException() {
		super();
	}

	public MailgunClientException(String s) {
		super(s);
	}

	public MailgunClientException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public MailgunClientException(Throwable arg0) {
		super(arg0);
	}

	public static MailgunClientException requestBodyIsNull() {
		return new MailgunClientException(REQUEST_BODY_NULL);
	}

	public static MailgunClientException methodNotSupported(String clientType) {
		String message = MessageFormat.format(MEHTOD_SUPPORTED, clientType);
		return new MailgunClientException(message);
	}

}
