package com.bnbneeds.portal.common.jms;

public interface JMSConstants {

	String CONTAINER_FACTORY_BEAN_NAME = "bnbneedsJMSContainerFactory";
	String JMS_TEMPLATE_BEAN_NAME = "bnbneedsJMSTemplate";
	String JMS_MESSAGE_BEANS_PACKAGE = "com.bnbneeds.portal.common.jms.beans";

	/*******************************
	 * User defined queues
	 *******************************/
	String MAINTENANCE_NOTICE_QUEUE_NAME = "maintenanceNoticeQueue";

}
