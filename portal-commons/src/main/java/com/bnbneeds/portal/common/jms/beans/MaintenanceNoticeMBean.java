package com.bnbneeds.portal.common.jms.beans;

import java.io.Serializable;

public class MaintenanceNoticeMBean implements Serializable {

	private static final long serialVersionUID = 6923194460418426283L;

	public static enum Operation {
		POST, DELETE
	}

	private String startTime;
	private String endTime;
	private String subject;
	private String message;
	private Operation operation;

	public MaintenanceNoticeMBean(Operation operation) {
		super();
		this.operation = operation;
	}

	public Operation getOperation() {
		return operation;
	}

	public String getStartTime() {
		return startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public String getSubject() {
		return subject;
	}

	public String getMessage() {
		return message;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MaintenanceNoticeMBean [");
		if (startTime != null)
			builder.append("startTime=").append(startTime).append(", ");
		if (endTime != null)
			builder.append("endTime=").append(endTime).append(", ");
		if (subject != null)
			builder.append("subject=").append(subject).append(", ");
		if (message != null)
			builder.append("message=").append(message).append(", ");
		if (operation != null)
			builder.append("operation=").append(operation.name());
		builder.append("]");
		return builder.toString();
	}

}
