package com.bnbneeds.rest.client.impl.springresttemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.rest.client.ClientConfig;
import com.bnbneeds.rest.client.exceptions.BNBNeedsClientException;
import com.bnbneeds.rest.client.impl.AbstractRestClient;
import com.bnbneeds.rest.client.impl.Constants;
import com.bnbneeds.rest.client.impl.TokenAccess;
import com.bnbneeds.rest.client.impl.springresttemplate.filters.AuthTokenInterceptor;
import com.bnbneeds.rest.client.impl.springresttemplate.filters.BNBNeedsReponseErrorHandler;
import com.bnbneeds.rest.client.impl.springresttemplate.filters.CSRFTokenInterceptor;
import com.bnbneeds.rest.client.impl.springresttemplate.filters.MediaTypeInterceptor;
import com.bnbneeds.rest.client.model.request.FileAttachment;
import com.bnbneeds.rest.client.model.request.FormElement;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SpringRestTemplate extends AbstractRestClient {

	private static final Logger log = LoggerFactory.getLogger(SpringRestTemplate.class.getSimpleName());

	private List<HttpMessageConverter<?>> getMessageConverters() {
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();

		messageConverters.add(new ByteArrayHttpMessageConverter());
		messageConverters.add(new StringHttpMessageConverter());
		messageConverters.add(new ResourceHttpMessageConverter());
		messageConverters.add(new FormHttpMessageConverter());
		/*
		 * MappingJackson2HttpMessageConverter is initialized with mediaType as
		 * application/json by default. So no need to set mediaType explicitly.
		 */
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		ObjectMapper mapper = getObjectMapper();
		converter.setObjectMapper(mapper);
		messageConverters.add(converter);
		return messageConverters;
	}

	private RestTemplate client;

	private ClientHttpRequestFactory clientHttpRequestFactory(ClientConfig config) {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setReadTimeout(config.getReadTimeout());
		factory.setConnectTimeout(config.getConnectionTimeout());
		return factory;
	}

	public SpringRestTemplate(String baseUri, ClientConfig config) {
		super(baseUri, config);
	}

	@Override
	public ClientConfig getConfig() {
		return config;
	}

	public RestTemplate getClient() {
		if (client == null) {
			client = new RestTemplate(clientHttpRequestFactory(config));
			client.setMessageConverters(getMessageConverters());

			client.setErrorHandler(new BNBNeedsReponseErrorHandler(config.getMediaType()));
			List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
			interceptors.add(new MediaTypeInterceptor(config.getMediaType()));
			interceptors.add(new CSRFTokenInterceptor(Constants.REQUESTED_BY_HEADER));
			interceptors.add(new AuthTokenInterceptor(new TokenAccess() {

				@Override
				public void setToken(String token) {
					authToken = token;
				}

				@Override
				public String getToken() {
					return authToken;
				}
			}));
			client.setInterceptors(interceptors);
		}
		return client;
	}

	private UriBuilder uriBuilder() {
		return UriBuilder.fromUri(apiBaseUri);
	}

	private URI uriBuilder(String relativePath) {
		return uriBuilder().path(relativePath).build();
	}

	private URI uriBuilder(String relativePath, Properties queryParams) {
		UriBuilder builder = uriBuilder().path(relativePath);
		for (String prop : queryParams.stringPropertyNames()) {
			builder = builder.queryParam(prop, queryParams.getProperty(prop));
		}
		return builder.build();
	}

	private <T> T getEntity(ResponseEntity<T> response) {
		return response.getBody();
	}

	@Override
	public <T> T get(Class<T> responseType, String path) {
		ResponseEntity<T> response = getClient().getForEntity(uriBuilder(path), responseType);
		return getEntity(response);
	}

	@Override
	public <T> T get(Class<T> responseType, String path, Object... args) {
		ResponseEntity<T> response = getClient().getForEntity(uriBuilder(path).toString(), responseType, args);
		return getEntity(response);

	}

	@Override
	public <T> T get(Class<T> responseType, String path, Properties queryParams) {
		ResponseEntity<T> response = getClient().getForEntity(uriBuilder(path, queryParams), responseType);
		return getEntity(response);
	}

	@Override
	public <T> T get(Class<T> responseType, String path, Properties queryParams, Object... args) {
		ResponseEntity<T> response = getClient().getForEntity(uriBuilder(path, queryParams).toString(), responseType,
				args);
		return getEntity(response);
	}

	@Override
	public <T> T get(GenericType<T> responseType, URI uri) {
		throw BNBNeedsClientException.methodNotSupported(SpringRestTemplate.class.getSimpleName());
	}

	@Override
	public <T> T get(GenericType<T> responseType, URI uri, Object... args) {
		throw BNBNeedsClientException.methodNotSupported(SpringRestTemplate.class.getSimpleName());
	}

	@Override
	public <T> T put(Class<T> responseType, Object request, String path) {
		HttpEntity<?> requestEntity = new HttpEntity<>(request);
		ResponseEntity<T> response = getClient().exchange(uriBuilder(path), HttpMethod.PUT, requestEntity,
				responseType);
		return getEntity(response);
	}

	@Override
	public <T> T get(GenericType<T> responseType, String path) {
		throw BNBNeedsClientException.methodNotSupported(SpringRestTemplate.class.getSimpleName());
	}

	@Override
	public <T> T get(GenericType<T> responseType, String path, Object... args) {
		throw BNBNeedsClientException.methodNotSupported(SpringRestTemplate.class.getSimpleName());
	}

	@Override
	public <T> T put(Class<T> responseType, Object request, String path, Object... args) {
		HttpEntity<?> requestEntity = new HttpEntity<>(request);
		ResponseEntity<T> response = getClient().exchange(uriBuilder(path).toString(), HttpMethod.PUT, requestEntity,
				responseType, args);
		return getEntity(response);
	}

	@Override
	public <T> T post(Class<T> responseType, Object request, String path) {
		ResponseEntity<T> response = getClient().postForEntity(uriBuilder(path), request, responseType);
		return getEntity(response);
	}

	@Override
	public <T> T post(Class<T> responseType, Object request, String path, Object... args) {
		ResponseEntity<T> response = getClient().postForEntity(uriBuilder(path).toString(), request, responseType,
				args);
		return getEntity(response);
	}

	@Override
	public <T> T post(GenericType<T> responseType, Object request, String path) {
		throw BNBNeedsClientException.methodNotSupported(SpringRestTemplate.class.getSimpleName());
	}

	@Override
	public <T> T post(GenericType<T> responseType, Object request, String path, Object... args) {
		throw BNBNeedsClientException.methodNotSupported(SpringRestTemplate.class.getSimpleName());
	}

	private <T> HttpEntity<?> buildMultipartRequestEntity(T request) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<?> requestEntity = new HttpEntity<>(request, headers);
		return requestEntity;
	}

	@Override
	public <T> T postMultiPart(Class<T> responseType, Object request, String path) {
		HttpEntity<?> requestEntity = buildMultipartRequestEntity(request);
		ResponseEntity<T> response = getClient().exchange(uriBuilder(path), HttpMethod.POST, requestEntity,
				responseType);

		return getEntity(response);
	}

	@Override
	public <T> T postMultiPart(Class<T> responseType, Object request, String path, Object... args) {
		HttpEntity<?> requestEntity = buildMultipartRequestEntity(request);
		ResponseEntity<T> response = getClient().exchange(uriBuilder(path).toString(), HttpMethod.POST, requestEntity,
				responseType, args);

		return getEntity(response);
	}

	@Override
	public CreateResourceResponse post(Object request, String path) {
		return post(CreateResourceResponse.class, request, path);
	}

	@Override
	public CreateResourceResponse post(Object request, String path, Object... args) {
		return post(CreateResourceResponse.class, request, path, args);
	}

	@Override
	public TaskResponse delete(String path) {
		ResponseEntity<TaskResponse> response = getClient().exchange(uriBuilder(path), HttpMethod.DELETE, null,
				TaskResponse.class);
		return getEntity(response);
	}

	@Override
	public TaskResponse delete(String path, Object... args) {
		ResponseEntity<TaskResponse> response = getClient().exchange(uriBuilder(path).toString(), HttpMethod.DELETE,
				null, TaskResponse.class, args);
		return getEntity(response);
	}

	@Override
	public TaskResponse delete(String path, Properties queryParams) {
		ResponseEntity<TaskResponse> response = getClient().exchange(uriBuilder(path, queryParams), HttpMethod.DELETE,
				null, TaskResponse.class);
		return getEntity(response);
	}

	@Override
	public <T> T login(Class<T> responseType, String username, String password, String path) {

		HttpAuthentication authHeader = new HttpBasicAuthentication(username, password);
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setAuthorization(authHeader);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		Properties queryParams = new Properties();
		queryParams.put(Constants.REQUEST_PARAM_USE_COOKIE, "true");
		ResponseEntity<T> response = getClient().exchange(uriBuilder(path, queryParams), HttpMethod.POST, requestEntity,
				responseType);
		return getEntity(response);
	}

	@Override
	public <T> T login(Class<T> responseType, String username, String password, String clientIPAddress, String path) {

		HttpAuthentication authHeader = new HttpBasicAuthentication(username, password);
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setAuthorization(authHeader);
		requestHeaders.add(Constants.REQUEST_CLIENT_IP_ADDRESS_HEADER, clientIPAddress);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		Properties queryParams = new Properties();
		queryParams.put(Constants.REQUEST_PARAM_USE_COOKIE, "true");
		ResponseEntity<T> response = getClient().exchange(uriBuilder(path, queryParams), HttpMethod.POST, requestEntity,
				responseType);
		return getEntity(response);
	}

	@Override
	public <T> T put(Class<T> responseType, Object request, String path, Properties queryParams) {

		HttpEntity<?> requestEntity = new HttpEntity<>(request);

		ResponseEntity<T> response = getClient().exchange(uriBuilder(path, queryParams), HttpMethod.PUT, requestEntity,
				responseType);
		return getEntity(response);
	}

	@Override
	public <T> T post(Class<T> responseType, Object request, String path, Properties queryParams) {
		HttpEntity<?> requestEntity = new HttpEntity<>(request);

		ResponseEntity<T> response = getClient().exchange(uriBuilder(path, queryParams), HttpMethod.POST, requestEntity,
				responseType);
		return getEntity(response);
	}

	@Override
	public byte[] getImage(String path, String mediaType, Properties queryParams) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.valueOf(mediaType)));
		HttpEntity<String> entity = new HttpEntity<>(headers);

		ResponseEntity<byte[]> response = getClient().exchange(uriBuilder(path, queryParams), HttpMethod.GET, entity,
				byte[].class);

		return getEntity(response);
	}

	@Override
	public byte[] getImage(String path, String mediaType) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.valueOf(mediaType)));
		HttpEntity<String> entity = new HttpEntity<>(headers);

		ResponseEntity<byte[]> response = getClient().exchange(uriBuilder(path), HttpMethod.GET, entity, byte[].class);

		return getEntity(response);
	}

	@Override
	public <T> T postMutiPart(Class<T> responseType, List<FormElement> formDataList,
			List<FileAttachment> fileAttachments, String path) {
		MultiValueMap<String, Object> multiPartBody = new LinkedMultiValueMap<>();

		HttpHeaders header = null;
		if (formDataList != null && !formDataList.isEmpty()) {
			for (Iterator<FormElement> iterator = formDataList.iterator(); iterator.hasNext();) {
				FormElement formElement = iterator.next();
				header = new HttpHeaders();
				header.setContentType(MediaType.TEXT_PLAIN);
				HttpEntity<String> part = new HttpEntity<>(formElement.getElementValue(), header);
				multiPartBody.add(formElement.getElementName(), part);
			}
		}

		if (fileAttachments != null && !fileAttachments.isEmpty()) {
			for (Iterator<FileAttachment> iterator = fileAttachments.iterator(); iterator.hasNext();) {
				FileAttachment fileAttachment = iterator.next();
				header = new HttpHeaders();
				header.setContentType(MediaType.valueOf(fileAttachment.getMediaType()));
				InputStreamResource inputStreamBody = new InputStreamResource(fileAttachment.getInputStream());
				HttpEntity<InputStreamResource> part = new HttpEntity<>(inputStreamBody, header);
				multiPartBody.add(fileAttachment.getElementName(), part);
			}
		}
		HttpHeaders multipartHeader = new HttpHeaders();
		multipartHeader.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(multiPartBody, multipartHeader);
		ResponseEntity<T> response = getClient().postForEntity(uriBuilder(path), requestEntity, responseType);
		return getEntity(response);
	}
}
