package com.bnbneeds.rest.client.core;

import java.util.HashMap;
import java.util.Map;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.BuyerInfoListResponse;
import com.bnbneeds.app.model.dealer.BuyerInfoResponse;
import com.bnbneeds.app.model.dealer.CreateBuyerParam;
import com.bnbneeds.app.model.dealer.CreateEnquiryParam;
import com.bnbneeds.app.model.dealer.EnquiryInfoListResponse;
import com.bnbneeds.app.model.dealer.UpdateBuyerParam;
import com.bnbneeds.app.model.dealer.UpdateEnquiryParam;
import com.bnbneeds.app.model.dealer.VendorListResponse;
import com.bnbneeds.app.model.dealer.order.AggregatedPurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.CreatePurchaseListParam;
import com.bnbneeds.app.model.dealer.order.PurchaseItemInfoResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.PurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.UpdatePurchaseListParam;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.ProductNameListResponse;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class Buyers extends SearchableResource {

	private String buyerId;

	public Buyers(RestClient restClient, BNBNeedsClient parent) {
		super(restClient, parent);
	}

	public Buyers(RestClient restClient, BNBNeedsClient parent, String buyerId) {
		super(restClient, parent);
		this.buyerId = buyerId;
	}

	public CreateResourceResponse create(CreateBuyerParam param) {
		return create(param, Endpoint.BUYERS.get());
	}

	public TaskResponse update(UpdateBuyerParam param, String buyerId) {
		return update(param, Endpoint.BUYER_INFO.get(buyerId));
	}

	public TaskResponse update(UpdateBuyerParam param) {
		return update(param, buyerId);
	}

	public TaskResponse delete(String buyerId) {
		return delete(Endpoint.BUYER_INFO.get(buyerId));
	}

	public TaskResponse delete() {
		return delete(buyerId);
	}

	public CreateResourceResponse deactivate(String buyerId, boolean forceDelete) {
		TrueOrFalse trueOrFalse = forceDelete ? TrueOrFalse.TRUE : TrueOrFalse.FALSE;
		return deactivate(buyerId, trueOrFalse);
	}

	public CreateResourceResponse deactivate(boolean forceDelete) {
		return deactivate(buyerId, forceDelete);
	}

	public CreateResourceResponse deactivate(String buyerId, TrueOrFalse value) {
		return create(Endpoint.BUYER_DEACTIVATE.get(buyerId), value);
	}

	public CreateResourceResponse deactivate(TrueOrFalse value) {
		return deactivate(buyerId, value);
	}

	public BuyerInfoResponse get(String buyerId) {
		return restClient.get(BuyerInfoResponse.class, Endpoint.BUYER_INFO.get(buyerId));
	}

	public String getAsString(String buyerId) {
		return restClient.get(String.class, Endpoint.BUYER_INFO.get(buyerId));
	}

	public BuyerInfoResponse get() {
		return get(buyerId);
	}

	public String getAsString() {
		return getAsString(buyerId);
	}

	public BuyerInfoListResponse list() {
		return list(BuyerInfoListResponse.class, Endpoint.BUYERS_BULK.get());
	}

	public BuyerInfoListResponse list(int fetchSize, Map<String, String> queryFilterMap, String nextOffset) {
		return list(BuyerInfoListResponse.class, Endpoint.BUYERS_BULK.get(), fetchSize, queryFilterMap, nextOffset);
	}

	public BuyerInfoListResponse list(Map<String, String> queryFilterMap, String nextOffset) {
		return list(BuyerInfoListResponse.class, Endpoint.BUYERS_BULK.get(), queryFilterMap, nextOffset);
	}

	public BuyerInfoListResponse list(Map<String, String> queryFilterMap) {
		return list(BuyerInfoListResponse.class, Endpoint.BUYERS_BULK.get(), queryFilterMap);
	}

	public BuyerInfoListResponse list(int fetchSize, String nextOffset) {
		return list(BuyerInfoListResponse.class, Endpoint.BUYERS_BULK.get(), fetchSize, nextOffset);
	}

	public BuyerInfoListResponse list(String nextOffset) {
		return list(BuyerInfoListResponse.class, Endpoint.BUYERS_BULK.get(), nextOffset);
	}

	public BuyerInfoListResponse list(String nextOffset, int fetchSize) {
		return list(BuyerInfoListResponse.class, Endpoint.BUYERS_BULK.get(), fetchSize, nextOffset);
	}

	public BuyerInfoListResponse list(int fetchSize) {
		return list(BuyerInfoListResponse.class, Endpoint.BUYERS_BULK.get(), fetchSize);
	}

	public BuyerInfoListResponse list(int fetchSize, Map<String, String> queryParams) {
		return list(BuyerInfoListResponse.class, Endpoint.BUYERS_BULK.get(), fetchSize, queryParams);
	}

	private <T> T getAssociatedProductNameListOfPurchaseItems(Class<T> responseType, String buyerId,
			String requestDate) {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("requestDate", requestDate);
		return list(responseType, Endpoint.BUYER_PURCHASE_ITEMS_ASSOCIATED_PRODUCT_NAMES.get(buyerId), queryMap);
	}

	public ProductNameListResponse getAssociatedProductNameListOfPurchaseItems(String buyerId, String requestDate) {
		return getAssociatedProductNameListOfPurchaseItems(ProductNameListResponse.class, buyerId, requestDate);

	}

	public ProductNameListResponse getAssociatedProductNameListOfPurchaseItems(String requestDate) {
		return getAssociatedProductNameListOfPurchaseItems(buyerId, requestDate);
	}

	public String getAssociatedProductNameListOfPurchaseItemsAsString(String buyerId, String requestDate) {
		return getAssociatedProductNameListOfPurchaseItems(String.class, buyerId, requestDate);

	}

	public String getAssociatedProductNameListOfPurchaseItemsAsString(String requestDate) {
		return getAssociatedProductNameListOfPurchaseItemsAsString(buyerId, requestDate);
	}

	private <T> T getAssociatedProductNameListOfPurchaseItemsByDateRange(Class<T> responseType, String buyerId,
			String startRequestDate, String endRequestDate) {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("startRequestDate", startRequestDate);
		queryMap.put("endRequestDate", endRequestDate);
		return list(responseType, Endpoint.BUYER_PURCHASE_ITEMS_ASSOCIATED_PRODUCT_NAMES.get(buyerId), queryMap);
	}

	public ProductNameListResponse getAssociatedProductNameListOfPurchaseItemsByDateRange(String buyerId,
			String startRequestDate, String endRequestDate) {
		return getAssociatedProductNameListOfPurchaseItemsByDateRange(ProductNameListResponse.class, buyerId,
				startRequestDate, endRequestDate);
	}

	public ProductNameListResponse getAssociatedProductNameListOfPurchaseItemsByDateRange(String startRequestDate,
			String endRequestDate) {
		return getAssociatedProductNameListOfPurchaseItemsByDateRange(buyerId, startRequestDate, endRequestDate);

	}

	public String getAssociatedProductNameListOfPurchaseItemsByDateRangeAsString(String buyerId,
			String startRequestDate, String endRequestDate) {
		return getAssociatedProductNameListOfPurchaseItemsByDateRange(String.class, buyerId, startRequestDate,
				endRequestDate);
	}

	public String getAssociatedProductNameListOfPurchaseItemsByDateRangeAsString(String startRequestDate,
			String endRequestDate) {
		return getAssociatedProductNameListOfPurchaseItemsByDateRange(String.class, buyerId, startRequestDate,
				endRequestDate);
	}

	private <T> T getAssociatedVendorListOfPurchaseItems(Class<T> responseType, String buyerId, String requestDate) {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("requestDate", requestDate);
		return list(responseType, Endpoint.BUYER_PURCHASE_ITEMS_ASSOCIATED_VENDORS.get(buyerId), queryMap);
	}

	public VendorListResponse getAssociatedVendorListOfPurchaseItems(String buyerId, String requestDate) {
		return getAssociatedVendorListOfPurchaseItems(VendorListResponse.class, buyerId, requestDate);
	}

	public VendorListResponse getAssociatedVendorListOfPurchaseItems(String requestDate) {
		return getAssociatedVendorListOfPurchaseItems(buyerId, requestDate);
	}

	public String getAssociatedVendorListOfPurchaseItemsAsString(String buyerId, String requestDate) {
		return getAssociatedVendorListOfPurchaseItems(String.class, buyerId, requestDate);
	}

	public String getAssociatedVendorListOfPurchaseItemsAsString(String requestDate) {
		return getAssociatedVendorListOfPurchaseItemsAsString(buyerId, requestDate);
	}

	private <T> T getAssociatedVendorListOfPurchaseItemsByDateRange(Class<T> responseType, String buyerId,
			String startRequestDate, String endRequestDate) {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("startRequestDate", startRequestDate);
		queryMap.put("endRequestDate", endRequestDate);
		return list(responseType, Endpoint.BUYER_PURCHASE_ITEMS_ASSOCIATED_VENDORS.get(buyerId), queryMap);
	}

	public VendorListResponse getAssociatedVendorListOfPurchaseItemsByDateRange(String buyerId, String startRequestDate,
			String endRequestDate) {
		return getAssociatedVendorListOfPurchaseItemsByDateRange(VendorListResponse.class, buyerId, startRequestDate,
				endRequestDate);
	}

	public VendorListResponse getAssociatedVendorListOfPurchaseItemsByDateRange(String startRequestDate,
			String endRequestDate) {
		return getAssociatedVendorListOfPurchaseItemsByDateRange(buyerId, startRequestDate, endRequestDate);
	}

	public String getAssociatedVendorListOfPurchaseItemsByDateRangeAsString(String buyerId, String startRequestDate,
			String endRequestDate) {
		return getAssociatedVendorListOfPurchaseItemsByDateRange(String.class, buyerId, startRequestDate,
				endRequestDate);
	}

	public String getAssociatedVendorListOfPurchaseItemsByDateRangeAsString(String startRequestDate,
			String endRequestDate) {
		return getAssociatedVendorListOfPurchaseItemsByDateRangeAsString(buyerId, startRequestDate, endRequestDate);
	}

	public TaskResponse createPurchaseList(String buyerId, CreatePurchaseListParam purchaseListParam) {
		return restClient.post(TaskResponse.class, purchaseListParam, Endpoint.BUYER_PURCHASE_ITEMS.get(buyerId));
	}

	public TaskResponse createPurchaseList(CreatePurchaseListParam purchaseListParam) {
		return createPurchaseList(buyerId, purchaseListParam);
	}

	public TaskResponse deletePurchaseItem(String buyerId, String itemId) {
		return restClient.delete(Endpoint.BUYER_PURCHASE_ITEM_INFO.get(buyerId, itemId));
	}

	public TaskResponse deletePurchaseItem(String itemId) {
		return deletePurchaseItem(buyerId, itemId);
	}

	// --------------------------------------

	public PurchaseListResponse getPurchaseList(String buyerId, int fetchSize) {
		return list(PurchaseListResponse.class, Endpoint.BUYER_PURCHASE_ITEMS.get(buyerId), fetchSize);
	}

	public PurchaseListResponse getPurchaseList(int fetchSize) {
		return getPurchaseList(buyerId, fetchSize);
	}

	public PurchaseListResponse getPurchaseList(String buyerId, int fetchSize, String nextOffset) {

		return list(PurchaseListResponse.class, Endpoint.BUYER_PURCHASE_ITEMS.get(buyerId), fetchSize, nextOffset);
	}

	public PurchaseListResponse getPurchaseList(int fetchSize, String nextOffset) {
		return getPurchaseList(buyerId, fetchSize, nextOffset);
	}

	public PurchaseListResponse getPurchaseList(String vendorId, int fetchSize, Map<String, String> queryFilterMap,
			String nextOffset) {

		return list(PurchaseListResponse.class, Endpoint.BUYER_PURCHASE_ITEMS.get(vendorId), fetchSize, queryFilterMap,
				nextOffset);
	}

	public PurchaseListResponse getPurchaseList(int fetchSize, Map<String, String> queryFilterMap, String nextOffset) {
		return getPurchaseList(buyerId, fetchSize, queryFilterMap, nextOffset);
	}

	public PurchaseListResponse getPurchaseList(String vendorId, int fetchSize, Map<String, String> queryFilterMap) {
		return list(PurchaseListResponse.class, Endpoint.BUYER_PURCHASE_ITEMS.get(vendorId), fetchSize, queryFilterMap);
	}

	public PurchaseListResponse getPurchaseList(int fetchSize, Map<String, String> queryFilterMap) {
		return getPurchaseList(buyerId, fetchSize, queryFilterMap);
	}

	public PurchaseItemInfoResponse getPurchaseItem(String buyerId, String itemId) {
		return restClient.get(PurchaseItemInfoResponse.class, Endpoint.BUYER_PURCHASE_ITEM_INFO.get(buyerId, itemId));
	}

	public PurchaseItemInfoResponse getPurchaseItem(String itemId) {
		return getPurchaseItem(buyerId, itemId);
	}

	public TaskResponse updatePurchaseItem(String buyerId, String itemId, PurchaseItemParam purchaseItemParam) {
		return update(purchaseItemParam, Endpoint.BUYER_PURCHASE_ITEM_INFO.get(buyerId, itemId));
	}

	public TaskResponse updatePurchaseItem(String itemId, PurchaseItemParam purchaseItemParam) {
		return updatePurchaseItem(buyerId, itemId, purchaseItemParam);
	}

	public TaskResponse updatePurchaseList(String buyerId, UpdatePurchaseListParam purchaseListParam) {
		return update(purchaseListParam, Endpoint.BUYER_PURCHASE_ITEMS.get(buyerId));
	}

	public TaskResponse updatePurchaseList(UpdatePurchaseListParam purchaseListParam) {
		return updatePurchaseList(buyerId, purchaseListParam);
	}

	public AggregatedPurchaseListResponse getAggregatedPurchaseList(String buyerId, String requestDate,
			String productNameId, String vendorId) {
		Map<String, String> queryFilterMap = new HashMap<>();
		queryFilterMap.put("requestDate", requestDate);
		if (productNameId != null && !productNameId.trim().isEmpty()) {
			queryFilterMap.put("productNameId", productNameId);
		}
		if (vendorId != null && !vendorId.trim().isEmpty()) {
			queryFilterMap.put("vendorId", vendorId);
		}

		return list(AggregatedPurchaseListResponse.class, Endpoint.BUYER_AGGREGATED_PURCHASE_LIST.get(buyerId),
				queryFilterMap);
	}

	public AggregatedPurchaseListResponse getAggregatedPurchaseListByDateRange(String buyerId, String startRequestDate,
			String endRequestDate, String productNameId, String vendorId) {
		Map<String, String> queryFilterMap = new HashMap<>();
		queryFilterMap.put("startRequestDate", startRequestDate);
		queryFilterMap.put("endRequestDate", endRequestDate);
		if (productNameId != null && !productNameId.trim().isEmpty()) {
			queryFilterMap.put("productNameId", productNameId);
		}
		if (vendorId != null && !vendorId.trim().isEmpty()) {
			queryFilterMap.put("vendorId", vendorId);
		}

		return list(AggregatedPurchaseListResponse.class, Endpoint.BUYER_AGGREGATED_PURCHASE_LIST.get(buyerId),
				queryFilterMap);
	}

	public AggregatedPurchaseListResponse getAggregatedPurchaseListByDateRange(String startRequestDate,
			String endRequestDate, String productNameId, String vendorId) {

		return getAggregatedPurchaseListByDateRange(buyerId, startRequestDate, endRequestDate, productNameId, vendorId);
	}

	public AggregatedPurchaseListResponse getAggregatedPurchaseList(String requestDate, String productNameId,
			String vendorId) {
		return getAggregatedPurchaseList(buyerId, requestDate, productNameId, vendorId);
	}

	public EnquiryInfoListResponse getEnquiries(String buyerId, int fetchSize) {
		return list(EnquiryInfoListResponse.class, Endpoint.BUYER_PRODUCT_ENQUIRIES.get(buyerId), fetchSize);
	}

	public EnquiryInfoListResponse getEnquiries(int fetchSize) {
		return getEnquiries(buyerId, fetchSize);
	}

	public EnquiryInfoListResponse getEnquiries(String buyerId, int fetchSize, Map<String, String> queryMap) {
		return list(EnquiryInfoListResponse.class, Endpoint.BUYER_PRODUCT_ENQUIRIES.get(buyerId), fetchSize, queryMap);
	}

	public EnquiryInfoListResponse getEnquiries(int fetchSize, Map<String, String> queryMap) {
		return getEnquiries(buyerId, fetchSize, queryMap);
	}

	public EnquiryInfoListResponse getEnquiries(String buyerId, int fetchSize, Map<String, String> queryFilterMap,
			String nextOffset) {

		return list(EnquiryInfoListResponse.class, Endpoint.BUYER_PRODUCT_ENQUIRIES.get(buyerId), fetchSize,
				queryFilterMap, nextOffset);
	}

	public EnquiryInfoListResponse getEnquiries(String buyerId, int fetchSize, String nextOffset) {
		return list(EnquiryInfoListResponse.class, Endpoint.BUYER_PRODUCT_ENQUIRIES.get(buyerId), fetchSize,
				nextOffset);
	}

	public EnquiryInfoListResponse getEnquiries(int fetchSize, String nextOffset) {
		return getEnquiries(buyerId, fetchSize, nextOffset);
	}

	public EnquiryInfoListResponse getEnquiries(int fetchSize, Map<String, String> queryFilterMap, String nextOffset) {
		return getEnquiries(buyerId, fetchSize, queryFilterMap, nextOffset);
	}

	public CreateResourceResponse createProductEnquiry(String buyerId, CreateEnquiryParam param) {
		return create(param, Endpoint.BUYER_PRODUCT_ENQUIRIES.get(buyerId));
	}

	public CreateResourceResponse createProductEnquiry(CreateEnquiryParam param) {
		return createProductEnquiry(buyerId, param);
	}

	public TaskResponse updateProductEnquiry(String buyerId, String enquiryId, UpdateEnquiryParam param) {
		return update(param, Endpoint.BUYER_PRODUCT_ENQUIRY_INFO.get(buyerId, enquiryId));
	}

	public TaskResponse updateProductEnquiry(String enquiryId, UpdateEnquiryParam param) {
		return updateProductEnquiry(buyerId, enquiryId, param);
	}

	public TaskResponse deleteProductEnquiry(String buyerId, String enquiryId) {
		return delete(Endpoint.BUYER_PRODUCT_ENQUIRY_INFO.get(buyerId, enquiryId));
	}

	public TaskResponse deleteProductEnquiry(String enquiryId) {
		return deleteProductEnquiry(buyerId, enquiryId);
	}

	private <T> T getAssociatedVendorListOfEnquiries(Class<T> responseType, String vendorId, String requestDate) {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("enquiryDate", requestDate);
		return list(responseType, Endpoint.BUYER_PRODUCT_ENQUIRIES_ASSOCIATED_VENDORS.get(vendorId), queryMap);
	}

	private <T> T getAssociatedVendorListOfEnquiriesByDateRange(Class<T> responseType, String vendorId,
			String startEnquiryDate, String endEnquiryDate) {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("startEnquiryDate", startEnquiryDate);
		queryMap.put("endEnquiryDate", endEnquiryDate);
		return list(responseType, Endpoint.BUYER_PRODUCT_ENQUIRIES_ASSOCIATED_VENDORS.get(vendorId), queryMap);
	}

	public VendorListResponse getAssociatedVendorListOfEnquiries(String buyerId, String requestDate) {
		return getAssociatedVendorListOfEnquiries(VendorListResponse.class, buyerId, requestDate);
	}

	public VendorListResponse getAssociatedVendorListOfEnquiries(String requestDate) {
		return getAssociatedVendorListOfEnquiries(VendorListResponse.class, buyerId, requestDate);
	}

	public String getAssociatedVendorListOfEnquiriesAsString(String buyerId, String requestDate) {
		return getAssociatedVendorListOfEnquiries(String.class, buyerId, requestDate);
	}

	public String getAssociatedVendorListOfEnquiriesAsString(String requestDate) {
		return getAssociatedVendorListOfEnquiries(String.class, buyerId, requestDate);
	}

	public VendorListResponse getAssociatedVendorListOfEnquiriesByDateRange(String buyerId, String startEnquiryDate,
			String endEnquiryDate) {
		return getAssociatedVendorListOfEnquiriesByDateRange(VendorListResponse.class, buyerId, startEnquiryDate,
				endEnquiryDate);
	}

	public VendorListResponse getAssociatedVendorListOfEnquiriesByDateRange(String startEnquiryDate,
			String endEnquiryDate) {
		return getAssociatedVendorListOfEnquiriesByDateRange(buyerId, startEnquiryDate, endEnquiryDate);
	}

	public String getAssociatedVendorListOfEnquiriesByDateRangeAsString(String buyerId, String startEnquiryDate,
			String endEnquiryDate) {
		return getAssociatedVendorListOfEnquiriesByDateRange(String.class, buyerId, startEnquiryDate, endEnquiryDate);
	}

	public String getAssociatedVendorListOfEnquiriesByDateRangeAsString(String startEnquiryDate,
			String endEnquiryDate) {
		return getAssociatedVendorListOfEnquiriesByDateRangeAsString(buyerId, startEnquiryDate, endEnquiryDate);
	}

	public TaskResponse updateEntityStatus(String entityId, EntityApprovalParam param) {
		return restClient.put(TaskResponse.class, param, Endpoint.ADMIN_ENTITY_BY_ID_APPROVALS.get(entityId));
	}

}
