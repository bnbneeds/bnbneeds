package com.bnbneeds.rest.client.core;

import java.net.URI;
import java.util.Map;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.review.AverageRatingResponse;
import com.bnbneeds.app.model.review.BuyerReviewInfoListResponse;
import com.bnbneeds.app.model.review.BuyerReviewInfoResponse;
import com.bnbneeds.app.model.review.ProductReviewInfoListResponse;
import com.bnbneeds.app.model.review.ProductReviewInfoResponse;
import com.bnbneeds.app.model.review.ReviewInfoListResponse;
import com.bnbneeds.app.model.review.ReviewInfoResponse;
import com.bnbneeds.app.model.review.ReviewParam;
import com.bnbneeds.app.model.review.VendorReviewInfoListResponse;
import com.bnbneeds.app.model.review.VendorReviewInfoResponse;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public abstract class Reviews extends SearchableResource {

	private String entityId;

	public static enum ReviewFor {
		vendor, buyer, product
	}

	private URI getEndpointForReviews() {

		ReviewFor reviewFor = getReviewFor();

		switch (reviewFor) {
		case vendor:
			return Endpoint.VENDOR_REVIEWS.get(entityId);
		case buyer:
			return Endpoint.BUYER_REVIEWS.get(entityId);
		case product:
			return Endpoint.PRODUCT_REVIEWS.get(entityId);
		default:
			break;
		}
		return null;
	}

	private URI getEndpointForReviewInfo(String reviewId) {

		ReviewFor reviewFor = getReviewFor();

		switch (reviewFor) {
		case vendor:
			return Endpoint.VENDOR_REVIEW_INFO.get(entityId, reviewId);
		case buyer:
			return Endpoint.BUYER_REVIEW_INFO.get(entityId, reviewId);
		case product:
			return Endpoint.PRODUCT_REVIEW_INFO.get(entityId, reviewId);
		default:
			break;
		}
		return null;
	}

	private URI getEndpointForAverageRating() {

		ReviewFor reviewFor = getReviewFor();

		switch (reviewFor) {
		case vendor:
			return Endpoint.VENDOR_REVIEWS_AVERAGE_RATING.get(entityId);
		case buyer:
			return Endpoint.BUYER_REVIEWS_AVERAGE_RATING.get(entityId);
		case product:
			return Endpoint.PRODUCT_REVIEWS_AVERAGE_RATING.get(entityId);
		default:
			break;
		}
		return null;
	}

	private Class<? extends ReviewInfoListResponse<? extends ReviewInfoResponse>> getReviewInfoListResponseClass() {
		ReviewFor reviewFor = getReviewFor();

		switch (reviewFor) {
		case vendor:
			return VendorReviewInfoListResponse.class;
		case buyer:
			return BuyerReviewInfoListResponse.class;
		case product:
			return ProductReviewInfoListResponse.class;
		default:
			break;
		}
		return null;
	}

	private Class<? extends ReviewInfoResponse> getReviewInfoResponseClass() {
		ReviewFor reviewFor = getReviewFor();

		switch (reviewFor) {
		case vendor:
			return VendorReviewInfoResponse.class;
		case buyer:
			return BuyerReviewInfoResponse.class;
		case product:
			return ProductReviewInfoResponse.class;
		default:
			break;
		}
		return null;
	}

	protected abstract ReviewFor getReviewFor();

	public Reviews(RestClient restClient, BNBNeedsClient coreClient,
			String entityId) {
		super(restClient, coreClient);
		this.entityId = entityId;
	}

	public Reviews(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public CreateResourceResponse createReview(ReviewParam param) {
		return create(param, getEndpointForReviews());
	}

	public TaskResponse updateReview(String reviewId, ReviewParam param) {
		return update(param, getEndpointForReviewInfo(reviewId));
	}

	public TaskResponse deleteReview(String reviewId) {
		return delete(getEndpointForReviewInfo(reviewId));
	}

	public AverageRatingResponse getAverageRating() {
		return restClient.get(AverageRatingResponse.class,
				getEndpointForAverageRating());
	}

	public ReviewInfoListResponse<? extends ReviewInfoResponse> getReviews() {
		return list(getReviewInfoListResponseClass(), getEndpointForReviews());
	}

	public ReviewInfoListResponse<? extends ReviewInfoResponse> getReviews(
			int fetchSize) {
		return list(getReviewInfoListResponseClass(), getEndpointForReviews(),
				fetchSize);
	}

	public ReviewInfoListResponse<? extends ReviewInfoResponse> getReviews(
			int fetchSize, Map<String, String> queryMap) {
		return list(getReviewInfoListResponseClass(), getEndpointForReviews(),
				fetchSize, queryMap);
	}

	public ReviewInfoListResponse<? extends ReviewInfoResponse> getReviews(
			int fetchSize, Map<String, String> queryMap, String nextOffset) {
		return list(getReviewInfoListResponseClass(), getEndpointForReviews(),
				fetchSize, queryMap, nextOffset);
	}

	public ReviewInfoListResponse<? extends ReviewInfoResponse> getReview(
			String reviewId) {
		return list(getReviewInfoListResponseClass(),
				getEndpointForReviewInfo(reviewId));
	}

	public ReviewInfoListResponse<? extends ReviewInfoResponse> getReviews(
			int fetchSize, String nextOffset) {
		return list(getReviewInfoListResponseClass(), getEndpointForReviews(),
				fetchSize, nextOffset);
	}

	public String getReviewsAsString() {
		return list(String.class, getEndpointForReviews());
	}

	public String getReviewsAsString(int fetchSize) {
		return list(String.class, getEndpointForReviews(), fetchSize);
	}

	public String getReviewsAsString(int fetchSize, Map<String, String> queryMap) {
		return list(String.class, getEndpointForReviews(), fetchSize, queryMap);
	}

	public String getReviewsAsString(int fetchSize,
			Map<String, String> queryMap, String nextOffset) {
		return list(String.class, getEndpointForReviews(), fetchSize, queryMap,
				nextOffset);
	}

	public String getReviewsAsString(int fetchSize, String nextOffset) {
		return list(String.class, getEndpointForReviews(), fetchSize,
				nextOffset);
	}

	public TaskResponse updateReviewStatus(String entityId,
			EntityApprovalParam param) {
		return restClient.put(TaskResponse.class, param,
				Endpoint.ADMIN_ENTITY_BY_ID_APPROVALS.get(entityId));
	}
}
