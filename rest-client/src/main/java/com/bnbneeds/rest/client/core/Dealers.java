package com.bnbneeds.rest.client.core;

import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class Dealers extends SearchableResource {

	Dealers(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}
	
	public static enum DealerType {
		vendors, buyers
	}
	public TaskResponse updateEntityStatus(String entityId, EntityApprovalParam param) {
		return restClient.put(TaskResponse.class, param,
				Endpoint.ADMIN_ENTITY_BY_ID_APPROVALS.get(entityId));
	}
	
}
