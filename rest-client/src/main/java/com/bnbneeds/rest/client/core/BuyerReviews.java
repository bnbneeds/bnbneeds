package com.bnbneeds.rest.client.core;

import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class BuyerReviews extends Reviews {

	public BuyerReviews(RestClient restClient, BNBNeedsClient coreClient,
			String buyerId) {
		super(restClient, coreClient, buyerId);
	}

	@Override
	protected ReviewFor getReviewFor() {
		return ReviewFor.buyer;
	}

}
