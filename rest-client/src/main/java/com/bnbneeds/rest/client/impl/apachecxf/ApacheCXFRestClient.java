package com.bnbneeds.rest.client.impl.apachecxf;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status.Family;

import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxrs.client.ClientConfiguration;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.AttachmentBuilder;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.apache.cxf.jaxrs.ext.multipart.InputStreamDataSource;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.rest.client.ClientConfig;
import com.bnbneeds.rest.client.exceptions.BNBNeedsClientException;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.bnbneeds.rest.client.impl.AbstractRestClient;
import com.bnbneeds.rest.client.impl.Constants;
import com.bnbneeds.rest.client.impl.TokenAccess;
import com.bnbneeds.rest.client.impl.apachecxf.filters.AuthTokenFilter;
import com.bnbneeds.rest.client.impl.apachecxf.filters.CSRFRequestFilter;
import com.bnbneeds.rest.client.model.request.FileAttachment;
import com.bnbneeds.rest.client.model.request.FormElement;
import com.bnbneeds.rest.client.util.ClientUtils;
import com.bnbneeds.rest.client.util.GeneralUtils;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

public class ApacheCXFRestClient extends AbstractRestClient {

	private static final Logger log = LoggerFactory
			.getLogger(ApacheCXFRestClient.class);

	private JacksonJaxbJsonProvider getJacksonJaxbJsonProvider() {
		JacksonJaxbJsonProvider jaxbJsonProvider = new JacksonJaxbJsonProvider(
				getObjectMapper(), JacksonJaxbJsonProvider.DEFAULT_ANNOTATIONS);
		return jaxbJsonProvider;

	}

	private WebClient client;

	public ApacheCXFRestClient(String baseUri, ClientConfig config) {
		super(baseUri, config);
	}

	@Override
	public ClientConfig getConfig() {
		return config;
	}

	public WebClient getClient() {
		if (client == null) {
			List<Object> providers = new ArrayList<Object>();

			providers.add(getJacksonJaxbJsonProvider());
			providers.add(new CSRFRequestFilter(Constants.REQUESTED_BY_HEADER));
			providers.add(new AuthTokenFilter(new TokenAccess() {

				@Override
				public void setToken(String token) {
					authToken = token;
				}

				@Override
				public String getToken() {
					return authToken;
				}
			}));

			client = WebClient.create(apiBaseUri, providers);

			ClientConfiguration apacheCXFClientconfig = WebClient
					.getConfig(client);
			// Log response messages
			apacheCXFClientconfig.getInInterceptors().add(
					new LoggingInInterceptor());
			if (config.isRequestLoggingEnabled()) {
				// Log request messages
				apacheCXFClientconfig.getOutInterceptors().add(
						new LoggingOutInterceptor());
			}
			HTTPConduit conduit = apacheCXFClientconfig.getHttpConduit();
			HTTPClientPolicy httpClientPolicy = conduit.getClient();
			httpClientPolicy
					.setConnectionTimeout(config.getConnectionTimeout());
			httpClientPolicy.setReceiveTimeout(config.getReadTimeout());
			httpClientPolicy.setAutoRedirect(true);
		}
		return client;
	}

	private void setAuthCookie() {
		if (authToken != null) {
			Cookie authCookie = new Cookie(Constants.AUTH_TOKEN_KEY, authToken);
			client.cookie(authCookie);
		}
	}

	private void throwError(WebApplicationException ex) {
		// If its an exception thrown, then it will always be a response of type
		// TaskResponse.
		Response r = ex.getResponse();
		int responseCode = r.getStatus();
		TaskResponse task = null;

		task = parseResponse(r, TaskResponse.class);
		BNBNeedsServiceException e = null;
		if (task != null) {
			e = new BNBNeedsServiceException(responseCode, task);
		} else {
			Family responseFamily = r.getStatusInfo().getFamily();
			switch (responseFamily) {
			case CLIENT_ERROR:
			case SERVER_ERROR:
			case OTHER:
				e = new BNBNeedsServiceException(responseCode,
						"Request failed. Response: HTTP code " + responseCode
								+ " - " + r.getStatusInfo().getReasonPhrase());
				break;
			default:
				return;
			}

		}
		throw e;
	}

	private void throwError(TaskResponse task) {
		if (task != null) {
			int httpCode = task.getHttpCode();

			Family responseFamily = Family.familyOf(httpCode);
			BNBNeedsServiceException e = null;

			switch (responseFamily) {
			case CLIENT_ERROR:
			case SERVER_ERROR:
			case OTHER:
				e = new BNBNeedsServiceException(httpCode, task);
				break;
			default:
				return;
			}
			throw e;
		}
	}

	private <T> T parseResponse(Response r, Class<T> responseType) {

		T task = null;
		try {
			String type = (String) r.getMetadata().getFirst(
					HttpHeaders.CONTENT_TYPE);
			Object entity = r.getEntity();
			task = ClientUtils.parseObjectToPOJO(entity, responseType, type);
		} catch (IOException e) {
			throw new BNBNeedsClientException("IOException occured", e);
		}

		return task;
	}

	public WebClient resource(URI uri) {
		return getClient().back(true).reset().resetQuery().path(uri.toString())
				.accept(config.getMediaType()).type(config.getMediaType());
	}

	public WebClient resource(URI uri, Object... args) {
		return getClient().back(true).reset().resetQuery()
				.path(uri.toString(), args).accept(config.getMediaType())
				.type(config.getMediaType());
	}

	public WebClient resourceMultiPart(URI uri) {
		return getClient().back(true).reset().resetQuery().path(uri.toString())
				.accept(config.getMediaType())
				.type(MediaType.MULTIPART_FORM_DATA);
	}

	public WebClient resourceMultiPart(URI uri, Object... args) {
		return getClient().back(true).reset().resetQuery()
				.path(uri.toString(), args).accept(config.getMediaType())
				.type(MediaType.MULTIPART_FORM_DATA);
	}

	public WebClient resource(String uri) {
		return getClient().back(true).reset().resetQuery().path(uri)
				.accept(config.getMediaType()).type(config.getMediaType());
	}

	public WebClient resource(String uri, Object... args) {
		return getClient().back(true).reset().resetQuery().path(uri, args)
				.accept(config.getMediaType()).type(config.getMediaType());
	}

	public WebClient resource(String uri, Properties queryParams) {
		WebClient client = getClient().reset().resetQuery().back(true)
				.path(uri).accept(config.getMediaType())
				.type(config.getMediaType());
		for (String prop : queryParams.stringPropertyNames()) {
			client.query(prop, queryParams.getProperty(prop));
		}
		return client;
	}

	public WebClient resource(URI uri, Properties queryParams) {
		return resource(uri.toString(), queryParams);
	}

	public WebClient resource(String uri, Properties queryParams,
			Object... args) {
		WebClient client = getClient().back(true).path(uri, args).reset()
				.resetQuery().accept(config.getMediaType())
				.type(config.getMediaType());
		for (String prop : queryParams.stringPropertyNames()) {
			client.query(prop, queryParams.getProperty(prop));
		}
		return client;
	}

	public WebClient resource(URI uri, Properties queryParams, Object... args) {
		return resource(uri.toString(), queryParams, args);
	}

	public WebClient resourceMultiPart(String uri) {
		return getClient().back(true).path(uri).accept(config.getMediaType())
				.type(MediaType.MULTIPART_FORM_DATA);
	}

	public WebClient resourceMultiPart(String uri, Object... args) {
		return getClient().back(true).path(uri, args)
				.accept(config.getMediaType())
				.type(MediaType.MULTIPART_FORM_DATA);
	}

	@Override
	public <T> T put(Class<T> responseType, Object request, String path) {
		T response = null;
		try {
			response = resource(path).put(request, responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T put(Class<T> responseType, Object request, String path,
			Object... args) {
		T response = null;
		try {
			response = resource(path, args).put(request, responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T post(Class<T> responseType, Object request, String path) {
		T response = null;
		try {
			response = resource(path).post(request, responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T post(Class<T> responseType, Object request, String path,
			Object... args) {
		T response = null;
		try {
			response = resource(path, args).post(request, responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T postMultiPart(Class<T> responseType, String filename,
			InputStream request, String path) {

		T response = null;
		try {

			ContentDisposition cd = new ContentDisposition(
					"attachment;filename=" + filename);
			Attachment att = new Attachment("file", request, cd);

			response = resourceMultiPart(path).post(new MultipartBody(att),
					responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T postMultiPart(Class<T> responseType, String filename,
			InputStream request, String path, Object... args) {

		T response = null;
		try {

			ContentDisposition cd = new ContentDisposition(
					"form-data;filename=" + filename);
			Attachment att = new Attachment("root", request, cd);

			response = resourceMultiPart(path, args).post(
					new MultipartBody(att), responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T postMultiPart(Class<T> responseType, String filename,
			String contentType, long fileSize, InputStream request, String path) {

		T response = null;
		try {

			ContentDisposition cd1 = new ContentDisposition(
					"form-data;name=\"imageFile\";filename=\"" + filename
							+ "\";size=\"" + fileSize + "\"");
			System.out.println(cd1.getParameters());
			Attachment att = new Attachment("file", request, cd1);

			List<Attachment> attachments = new ArrayList<Attachment>();
			attachments.add(att);
			MultipartBody mp = new MultipartBody(attachments, true);

			response = resourceMultiPart(path).post(mp, responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T postMultiPart(Class<T> responseType, String filename,
			String contentType, long fileSize, InputStream request,
			String path, Object... args) {

		T response = null;
		try {

			ContentDisposition cd = new ContentDisposition(
					"form-data;name=\"imageFile\";filename=\"" + filename
							+ "\";size=\"" + fileSize + "\"");

			Attachment att = new Attachment("file", request, cd);

			response = resourceMultiPart(path, args).post(
					new MultipartBody(att), responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T postMutiPart(Class<T> responseType,
			List<FormElement> formElementList,
			List<FileAttachment> fileAttachments, String path) {

		List<Attachment> atts = new ArrayList<>();
		int numberOfAttachments = 0;

		if (fileAttachments != null && !fileAttachments.isEmpty()) {

			for (Iterator<FileAttachment> iterator = fileAttachments.iterator(); iterator
					.hasNext();) {
				FileAttachment fileAttachment = iterator.next();
				numberOfAttachments++;
				ContentDisposition cd = new ContentDisposition(
						"form-data; name=\"" + fileAttachment.getElementName()
								+ "\"; filename=\""
								+ fileAttachment.getFileName() + "\"");

				String contentId = fileAttachment.getContentId();
				if (GeneralUtils.isEmpty(contentId)) {
					contentId = String.format("attachment-%d",
							numberOfAttachments);
				}
				DataHandler dataHandler = new DataHandler(
						new InputStreamDataSource(
								fileAttachment.getInputStream(),
								fileAttachment.getMediaType()));
				AttachmentBuilder attachmentBuilder = new AttachmentBuilder();
				Attachment att = attachmentBuilder.contentDisposition(cd)
						.dataHandler(dataHandler).build();

				atts.add(att);
			}
		}

		if (formElementList != null && !formElementList.isEmpty()) {
			for (Iterator<FormElement> iterator = formElementList.iterator(); iterator
					.hasNext();) {
				FormElement formElement = iterator.next();
				ContentDisposition cd = new ContentDisposition(
						"form-data;name=\"" + formElement.getElementName()
								+ "\"");

				Attachment att = new Attachment(formElement.getElementName(),
						new ByteArrayInputStream(formElement.getElementValue()
								.getBytes()), cd);
				atts.add(att);

			}
		}

		T response = null;
		try {
			response = resourceMultiPart(path).post(new MultipartBody(atts),
					responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public CreateResourceResponse post(Object request, String path) {
		CreateResourceResponse response = null;
		try {
			Response r = resource(path).post(request);
			response = parseResponse(r, CreateResourceResponse.class);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public CreateResourceResponse post(Object request, String path,
			Object... args) {
		CreateResourceResponse response = null;
		try {
			Response r = resource(path, args).post(request);
			response = parseResponse(r, CreateResourceResponse.class);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public TaskResponse delete(String path) {
		TaskResponse response = null;
		try {
			Response r = resource(path).delete();
			response = parseResponse(r, TaskResponse.class);
			throwError(response);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public TaskResponse delete(String path, Object... args) {
		TaskResponse response = null;
		try {
			Response r = resource(path, args).delete();
			response = parseResponse(r, TaskResponse.class);
			throwError(response);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public TaskResponse delete(String uri, Properties queryParams) {
		TaskResponse response = null;
		try {
			Response r = resource(uri, queryParams).delete();
			response = parseResponse(r, TaskResponse.class);
			throwError(response);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T get(Class<T> responseType, String path) {
		T response = null;
		try {
			response = resource(path).get(responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T get(Class<T> responseType, String path, Object... args) {
		T response = null;
		try {
			response = resource(path, args).get(responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T get(Class<T> responseType, String path, Properties queryParams) {
		T response = null;
		try {
			response = resource(path, queryParams).get(responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T get(Class<T> responseType, String path,
			Properties queryParams, Object... args) {
		T response = null;
		try {
			response = resource(path, queryParams, args).get(responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T get(GenericType<T> responseType, String path) {
		T response = null;
		try {
			response = resource(path).get(responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T get(GenericType<T> responseType, String path, Object... args) {
		T response = null;
		try {
			response = resource(path, args).get(responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T post(GenericType<T> responseType, Object request, String path) {
		T response = null;
		try {
			response = resource(path).post(request, responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T post(GenericType<T> responseType, Object request, String path,
			Object... args) {
		T response = null;
		try {
			response = resource(path, args).post(request, responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T login(Class<T> responseType, String username, String password,
			String path) {
		HTTPConduit conduit = WebClient.getConfig(getClient()).getHttpConduit();
		AuthorizationPolicy authorization = new AuthorizationPolicy();
		authorization.setUserName(username);
		authorization.setPassword(password);
		conduit.setAuthorization(authorization);
		try {
			Properties queryParams = new Properties();
			queryParams.put(Constants.REQUEST_PARAM_USE_COOKIE, "true");
			return this.post(responseType, null, path, queryParams);
		} finally {
			/*
			 * Set authorization policy to null as we don't need Basic
			 * Authentication for subsequent requests. All subsequent requests
			 * are authenticated using token based approach.
			 */
			conduit.setAuthorization(null);
		}
	}

	@Override
	public <T> T login(Class<T> responseType, String username, String password,
			String clientIPAddress, String path) {

		T response = null;
		WebClient client = getClient();

		ClientConfiguration apacheCXFClientconfig = WebClient.getConfig(client);
		HTTPConduit conduit = apacheCXFClientconfig.getHttpConduit();
		AuthorizationPolicy authorization = new AuthorizationPolicy();
		authorization.setUserName(username);
		authorization.setPassword(password);
		conduit.setAuthorization(authorization);

		try {
			Properties queryParams = new Properties();
			queryParams.put(Constants.REQUEST_PARAM_USE_COOKIE, "true");
			response = resource(path)
					.header(Constants.REQUEST_CLIENT_IP_ADDRESS_HEADER,
							clientIPAddress)
					.query(Constants.REQUEST_PARAM_USE_COOKIE, "true")
					.post(null, responseType);

		} catch (WebApplicationException e) {
			throwError(e);
		} finally {
			/*
			 * Set authorization policy to null as we don't need Basic
			 * Authentication for subsequent requests. All subsequent requests
			 * are authenticated using token based approach.
			 */
			conduit.setAuthorization(null);
		}
		return response;
	}

	@Override
	public <T> T put(Class<T> responseType, Object request, String path,
			Properties queryParams) {
		T response = null;
		try {
			response = resource(path, queryParams).put(request, responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public <T> T post(Class<T> responseType, Object request, String path,
			Properties queryParams) {
		T response = null;
		try {
			response = resource(path, queryParams).post(request, responseType);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public Response get(String path, String mediaType, Properties queryParams) {
		Response response = null;

		try {
			response = resource(path, queryParams).type(mediaType).get();
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public Response get(String path, String mediaType) {
		Response response = null;

		try {
			response = resource(path).accept(mediaType).get();
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public byte[] getImage(String path, String mediaType) {
		byte[] response = null;
		try {
			response = resource(path).accept(mediaType).get(byte[].class);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

	@Override
	public byte[] getImage(String path, String mediaType, Properties queryParams) {
		byte[] response = null;
		try {
			response = resource(path, queryParams).accept(mediaType).get(
					byte[].class);
		} catch (WebApplicationException e) {
			throwError(e);
		}
		return response;
	}

}
