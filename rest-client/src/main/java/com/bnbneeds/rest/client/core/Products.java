package com.bnbneeds.rest.client.core;

import java.util.Map;

import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.ProductImageListResponse;
import com.bnbneeds.app.model.product.ProductInfoListResponse;
import com.bnbneeds.app.model.product.ProductInfoResponse;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class Products extends SearchableResource {

	private String productId;

	public Products(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public Products(RestClient restClient, BNBNeedsClient coreClient,
			String productId) {
		this(restClient, coreClient);
		this.productId = productId;
	}

	public ProductInfoListResponse list(int fetchSize) {
		return list(ProductInfoListResponse.class, Endpoint.PRODUCTS.get(),
				fetchSize);
	}

	public ProductInfoListResponse list(int fetchSize, String nextOffset) {
		return list(ProductInfoListResponse.class, Endpoint.PRODUCTS.get(),
				fetchSize, nextOffset);
	}

	public ProductInfoListResponse list(int fetchSize,
			Map<String, String> queryMap) {
		return list(ProductInfoListResponse.class, Endpoint.PRODUCTS.get(),
				fetchSize, queryMap);
	}

	public ProductInfoListResponse list(int fetchSize,
			Map<String, String> queryMap, String nextOffset) {
		return list(ProductInfoListResponse.class, Endpoint.PRODUCTS.get(),
				fetchSize, queryMap, nextOffset);
	}

	public String listAsString(int fetchSize) {
		return list(String.class, Endpoint.PRODUCTS.get(), fetchSize);
	}

	public String listAsString(int fetchSize, String nextOffset) {
		return list(String.class, Endpoint.PRODUCTS.get(), fetchSize,
				nextOffset);
	}

	public String getAsString(String productId) {
		return restClient.get(String.class,
				Endpoint.PRODUCT_INFO.get(productId));
	}

	public String getAsString() {
		return getAsString(productId);
	}

	public ProductInfoResponse get(String productId) {
		return restClient.get(ProductInfoResponse.class,
				Endpoint.PRODUCT_INFO.get(productId));
	}

	public ProductInfoResponse get() {
		return get(productId);
	}

	public ProductImageListResponse getImageLinks(String productId) {
		return restClient.get(ProductImageListResponse.class,
				Endpoint.PRODUCT_IMAGES.get(productId));
	}

	public ProductImageListResponse getImageLinks() {
		return getImageLinks(productId);
	}

	public byte[] getImage(String productId, String imageId, String mediaType) {
		return restClient.getImage(
				Endpoint.PRODUCT_IMAGE.get(productId, imageId), mediaType);
	}

	public byte[] getImage(String imageId, String mediaType) {
		return restClient.getImage(
				Endpoint.PRODUCT_IMAGE.get(productId, imageId), mediaType);
	}
}
