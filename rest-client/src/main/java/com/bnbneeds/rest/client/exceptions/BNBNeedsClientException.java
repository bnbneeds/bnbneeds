package com.bnbneeds.rest.client.exceptions;

import java.text.MessageFormat;

public class BNBNeedsClientException extends RuntimeException {

	private static final long serialVersionUID = -3112107660206426291L;

	private static final String REQUEST_BODY_NULL = "Request body is null.";
	private static final String MEHTOD_SUPPORTED = "This method is not supported for client: [{0}]";

	public BNBNeedsClientException() {
		super();
	}

	public BNBNeedsClientException(String s) {
		super(s);
	}

	public BNBNeedsClientException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public BNBNeedsClientException(Throwable arg0) {
		super(arg0);
	}

	public static BNBNeedsClientException requestBodyIsNull() {
		return new BNBNeedsClientException(REQUEST_BODY_NULL);
	}

	public static BNBNeedsClientException methodNotSupported(String clientType) {
		String message = MessageFormat.format(MEHTOD_SUPPORTED, clientType);
		return new BNBNeedsClientException(message);
	}

}
