package com.bnbneeds.rest.client.core;

import java.util.Map;

import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionInfoListResponse;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionInfoResponse;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class VendorBiddingSubscriptions extends BiddingSubscriptions {

	public VendorBiddingSubscriptions(RestClient restClient, BNBNeedsClient coreClient, String vendorId) {
		super(restClient, coreClient, vendorId);
	}

	@Override
	protected BiddingSubscriptionFor getSubscriptionType() {
		return BiddingSubscriptionFor.vendor;
	}

	@Override
	public VendorBiddingSubscriptionInfoResponse getSubscription(String subscriptionId) {
		return (VendorBiddingSubscriptionInfoResponse) super.getSubscription(subscriptionId);
	}

	@Override
	public VendorBiddingSubscriptionInfoListResponse list() {
		return (VendorBiddingSubscriptionInfoListResponse) super.list();
	}

	@Override
	public VendorBiddingSubscriptionInfoListResponse list(int fetchSize) {
		return (VendorBiddingSubscriptionInfoListResponse) super.list(fetchSize);
	}

	@Override
	public VendorBiddingSubscriptionInfoListResponse list(int fetchSize, Map<String, String> queryMap) {
		return (VendorBiddingSubscriptionInfoListResponse) super.list(fetchSize, queryMap);
	}

	@Override
	public VendorBiddingSubscriptionInfoListResponse list(int fetchSize, Map<String, String> queryMap,
			String nextOffset) {
		return (VendorBiddingSubscriptionInfoListResponse) super.list(fetchSize, queryMap, nextOffset);
	}

}
