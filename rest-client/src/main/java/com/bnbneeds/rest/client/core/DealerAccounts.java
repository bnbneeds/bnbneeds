package com.bnbneeds.rest.client.core;

import java.util.Map;

import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.UpdateAccountStatusParam;
import com.bnbneeds.app.model.account.UpdateUserAccountParam;
import com.bnbneeds.app.model.account.UserAccountInfoListResponse;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class DealerAccounts extends SearchableResource {

	
	public UserAccountInfoListResponse list() {
		return list(UserAccountInfoListResponse.class, Endpoint.DEALER_USER_ACCOUNTS
				.get());
	}

	public UserAccountInfoListResponse list(int fetchSize) {
		return list(UserAccountInfoListResponse.class, Endpoint.DEALER_USER_ACCOUNTS
				.get(), fetchSize);
	}

	public UserAccountInfoListResponse list(int fetchSize, String nextOffset) {
		return list(UserAccountInfoListResponse.class, Endpoint.DEALER_USER_ACCOUNTS
				.get(), fetchSize, nextOffset);
	}

	public UserAccountInfoListResponse list(int fetchSize,
			Map<String, String> queryMap, String nextOffset) {
		return list(UserAccountInfoListResponse.class, Endpoint.DEALER_USER_ACCOUNTS
				.get(), fetchSize, queryMap, nextOffset);
	}

	public UserAccountInfoListResponse list(int fetchSize,
			Map<String, String> queryMap) {
		return list(UserAccountInfoListResponse.class, Endpoint.DEALER_USER_ACCOUNTS
				.get(), fetchSize, queryMap);
	}


	public DealerAccounts(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public TaskResponse updateAccount(UpdateUserAccountParam user, String accountId) {
		return update(user, Endpoint.DEALER_USER_ACCOUNT_INFO.get(accountId));
	}
	

	public UserAccountInfoResponse getAccount() {
		return restClient.get(UserAccountInfoResponse.class,
				Endpoint.DEALER_USER_ACCOUNT_INFO.get());

	}
	
	public UserAccountInfoResponse getAccount(String accountId) {
		return restClient.get(UserAccountInfoResponse.class,
				Endpoint.DEALER_USER_ACCOUNT_INFO.get(accountId));

	}
	
	public TaskResponse deleteAccount(String accountId) {
		return delete(Endpoint.DEALER_USER_ACCOUNT_INFO.get(accountId));

	}
	
	public TaskResponse updateAccountStatus(String id, UpdateAccountStatusParam param) {
		return restClient.put(TaskResponse.class, param,
				Endpoint.DEALER_USER_ACCOUNTS_APPROVALS.get(id));
	}
	


}
