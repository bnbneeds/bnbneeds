package com.bnbneeds.rest.client.model.request;

import com.bnbneeds.rest.client.util.GeneralUtils;

public class FormElement {

	private String elementName;
	private String elementValue;

	protected FormElement() {
		super();
	}

	public FormElement(String elementName, String elementValue) {
		super();
		GeneralUtils.assertNotEmpty(elementName,
				"Element name cannot be null or empty");
		GeneralUtils.assertNotEmpty(elementValue,
				"Element value cannot be null or empty");
		this.elementName = elementName;
		this.elementValue = elementValue;
	}

	public String getElementName() {
		return elementName;
	}

	protected void setElementName(String elementName) {
		this.elementName = elementName;
	}

	public String getElementValue() {
		return elementValue;
	}

	protected void setElementValue(String elementValue) {
		this.elementValue = elementValue;
	}

}
