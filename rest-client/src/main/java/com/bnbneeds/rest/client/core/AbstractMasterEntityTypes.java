package com.bnbneeds.rest.client.core;

import java.util.Map;

import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.EntityInfoListResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.UpdateEntityParam;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public abstract class AbstractMasterEntityTypes extends SearchableResource {

	public static enum EntityType {
		product_type, product_name, product_category, business_type
	}

	public AbstractMasterEntityTypes(RestClient restClient,
			BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	protected abstract EntityType getEntityType();

	private Endpoint getEndpointForEntityInfo() {

		EntityType type = getEntityType();

		switch (type) {
		case product_type:
			return Endpoint.PRODUCT_TYPE_INFO;
		case product_category:
			return Endpoint.PRODUCT_CATEGORY_INFO;
		case product_name:
			return Endpoint.PRODUCT_NAME_INFO;
		case business_type:
			return Endpoint.BUSINESS_TYPE_INFO;
		default:
			return null;
		}
	}

	private Endpoint getEndpointForBulkEntities() {

		EntityType type = getEntityType();

		switch (type) {
		case product_type:
			return Endpoint.PRODUCT_TYPES_BULK;
		case product_category:
			return Endpoint.PRODUCT_CATEGORIES_BULK;
		case product_name:
			return Endpoint.PRODUCT_NAMES_BULK;
		case business_type:
			return Endpoint.BUSINESS_TYPES_BULK;
		default:
			return null;
		}
	}

	public TaskResponse update(String id, UpdateEntityParam param) {
		return update(param, getEndpointForEntityInfo().get(id));
	}

	public TaskResponse delete(String id) {
		return delete(getEndpointForEntityInfo().get(id));
	}

	public EntityInfoListResponse listBulk() {
		return list(EntityInfoListResponse.class, getEndpointForBulkEntities()
				.get());
	}

	public EntityInfoListResponse listBulk(int fetchSize) {
		return list(EntityInfoListResponse.class, getEndpointForBulkEntities()
				.get(), fetchSize);
	}

	public EntityInfoListResponse listBulk(int fetchSize, String nextOffset) {
		return list(EntityInfoListResponse.class, getEndpointForBulkEntities()
				.get(), fetchSize, nextOffset);
	}

	public EntityInfoListResponse listBulk(int fetchSize,
			Map<String, String> queryMap, String nextOffset) {
		return list(EntityInfoListResponse.class, getEndpointForBulkEntities()
				.get(), fetchSize, queryMap, nextOffset);
	}

	public EntityInfoListResponse listBulk(int fetchSize,
			Map<String, String> queryMap) {
		return list(EntityInfoListResponse.class, getEndpointForBulkEntities()
				.get(), fetchSize, queryMap);
	}

	public String listBulkAsString(int fetchSize) {
		return list(String.class, getEndpointForBulkEntities().get(), fetchSize);
	}

	public String listBulkAsString(int fetchSize,
			Map<String, String> queryFilterMap) {
		return list(String.class, getEndpointForBulkEntities().get(),
				fetchSize, queryFilterMap);
	}

	public String listBulkAsString(int fetchSize, String nextOffset) {
		return list(String.class, getEndpointForBulkEntities().get(),
				fetchSize, nextOffset);
	}

	public String listBulkAsString(int fetchSize,
			Map<String, String> queryFilterMap, String nextOffset) {
		return list(String.class, getEndpointForBulkEntities().get(),
				fetchSize, queryFilterMap, nextOffset);
	}
	
	public TaskResponse updateEntityStatus(String entityId, EntityApprovalParam param) {
		return restClient.put(TaskResponse.class, param,
				Endpoint.ADMIN_ENTITY_BY_ID_APPROVALS.get(entityId));
	}

}
