package com.bnbneeds.rest.client.core;

import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class VendorReviews extends Reviews {

	public VendorReviews(RestClient restClient, BNBNeedsClient coreClient,
			String vendorId) {
		super(restClient, coreClient, vendorId);
	}

	@Override
	protected ReviewFor getReviewFor() {
		return ReviewFor.vendor;
	}

}
