package com.bnbneeds.rest.client.core;

import java.util.Properties;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.CreateUserAccountParam;
import com.bnbneeds.app.model.account.UpdateUserAccountParam;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.account.VerifyAccountParam;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class Users extends Resource {

	public static enum SendVerificationCodeBy {
		email, sms, email_and_sms
	}

	public Users(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public CreateResourceResponse createAccount(CreateUserAccountParam user) {
		return create(user, Endpoint.USER_ACCOUNTS.get());

	}

	public TaskResponse updateAccount(UpdateUserAccountParam user) {
		return update(user, Endpoint.USER_ACCOUNTS.get());
	}

	public UserAccountInfoResponse getAccount() {
		return restClient.get(UserAccountInfoResponse.class,
				Endpoint.USER_ACCOUNTS.get());

	}

	public TaskResponse submitVerficationRequest(String username,
			String emailVerificationCode, String smsVerificationCode) {

		VerifyAccountParam verifyAccountParam = new VerifyAccountParam();
		verifyAccountParam.setEmailVerificationCode(emailVerificationCode);
		verifyAccountParam.setSmsVerificationCode(smsVerificationCode);

		return restClient.post(TaskResponse.class, verifyAccountParam,
				Endpoint.USER_ACCOUNT_VERIFICATIONS.get(username.trim()));

	}

	public TaskResponse resendVerficationCode(String username,
			SendVerificationCodeBy by) {

		Properties queryParam = new Properties();
		queryParam.put("type", by.name());

		return restClient.put(TaskResponse.class, null,
				Endpoint.USER_ACCOUNT_VERIFICATIONS.get(username.trim()),
				queryParam);

	}
}
