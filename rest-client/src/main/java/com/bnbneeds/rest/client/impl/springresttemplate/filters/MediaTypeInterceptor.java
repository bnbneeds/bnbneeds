package com.bnbneeds.rest.client.impl.springresttemplate.filters;

import java.io.IOException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class MediaTypeInterceptor implements ClientHttpRequestInterceptor {

	private static final String CONTENT_TYPE_HEADER = "Content-Type";
	private static final String ACCEPT_HEADER = "Accept";
	private String mediaType;

	public MediaTypeInterceptor(String mediaType) {
		this.mediaType = mediaType;
	}

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body,
			ClientHttpRequestExecution execution) throws IOException {
		HttpHeaders headers = request.getHeaders();

		if (!headers.containsKey(CONTENT_TYPE_HEADER)) {
			headers.add(CONTENT_TYPE_HEADER, mediaType);
		}

		if (!headers.containsKey(ACCEPT_HEADER)) {
			headers.add(ACCEPT_HEADER, mediaType);
		}

		return execution.execute(request, body);
	}

}
