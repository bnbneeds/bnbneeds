package com.bnbneeds.rest.client.impl;

public class Constants {

	public static final String REQUESTED_BY_HEADER = "bnbneeds-web-client";
	public static final String AUTH_TOKEN_KEY = "X-BNBNEEDS-AUTH-TOKEN";
	public static final String REQUEST_XSRF_TOKEN_KEY = "X-XSRF-TOKEN";
	public static final String RESPONSE_XSRF_TOKEN_KEY = "XSRF-TOKEN";
	public static final String REQUEST_CLIENT_IP_ADDRESS_HEADER = "X-Client-IP-Address";
	public static final String REQUEST_PARAM_USE_COOKIE = "use-cookie";

}
