package com.bnbneeds.rest.client.core;

import java.net.URI;
import java.util.Properties;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;
import com.bnbneeds.rest.client.exceptions.BNBNeedsClientException;

class Resource {

	protected static final String FORCE_DELETE_QUERY_PARAM_NAME = "forceDelete";

	protected enum TrueOrFalse {
		TRUE("true"), FALSE("false");

		private String value;

		private TrueOrFalse(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

	}

	protected RestClient restClient;
	protected BNBNeedsClient coreClient;

	Resource(RestClient restClient, BNBNeedsClient coreClient) {
		super();
		this.restClient = restClient;
		this.coreClient = coreClient;
	}

	protected static void checkRequestBodyNull(Object request) {
		if (request == null) {
			throw BNBNeedsClientException.requestBodyIsNull();
		}
	}

	protected CreateResourceResponse create(Object request, URI path) {
		checkRequestBodyNull(request);
		return restClient.post(CreateResourceResponse.class, request, path);
	}

	protected CreateResourceResponse create(URI path) {
		return restClient.post(CreateResourceResponse.class, null, path);
	}

	protected CreateResourceResponse create(URI path, TrueOrFalse forceDelete) {
		Properties q = new Properties();
		q.put(FORCE_DELETE_QUERY_PARAM_NAME, forceDelete.getValue());
		return restClient.post(CreateResourceResponse.class, null, path, q);
	}

	protected TaskResponse update(Object request, URI path) {
		checkRequestBodyNull(request);
		return restClient.put(TaskResponse.class, request, path);
	}

	protected TaskResponse delete(URI path) {
		return restClient.delete(path);
	}

	protected TaskResponse delete(URI path, TrueOrFalse forceDelete) {
		Properties q = new Properties();
		q.put(FORCE_DELETE_QUERY_PARAM_NAME, forceDelete.getValue());
		return restClient.delete(path, q);
	}
}
