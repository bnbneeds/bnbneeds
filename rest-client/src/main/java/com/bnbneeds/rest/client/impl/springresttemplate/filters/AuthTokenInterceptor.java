package com.bnbneeds.rest.client.impl.springresttemplate.filters;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.Cookie;

import org.apache.cxf.jaxrs.impl.CookieHeaderProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import com.bnbneeds.rest.client.impl.Constants;
import com.bnbneeds.rest.client.impl.TokenAccess;

public class AuthTokenInterceptor implements ClientHttpRequestInterceptor {

	private static final Logger log = LoggerFactory
			.getLogger(AuthTokenInterceptor.class);

	private TokenAccess sessionAccess;

	public AuthTokenInterceptor(TokenAccess access) {
		this.sessionAccess = access;
	}

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body,
			ClientHttpRequestExecution execution) throws IOException {
		String token = sessionAccess.getToken();
		if (token != null) {
			request.getHeaders().add("Cookie",
					new Cookie(Constants.AUTH_TOKEN_KEY, token).toString());
		}

		ClientHttpResponse response = execution.execute(request, body);
		HttpStatus responseStatus = response.getStatusCode();
		if (HttpStatus.OK == responseStatus) {
			if (token == null) {
				HttpHeaders headers = response.getHeaders();
				if (headers.containsKey("Set Cookie")) {
					List<String> values = headers.get("Set Cookie");
					for (String value : values) {
						if (value.contains(Constants.AUTH_TOKEN_KEY)) {
							CookieHeaderProvider provider = new CookieHeaderProvider();
							Cookie cookie = provider.fromString(value);
							sessionAccess.setToken(cookie.getValue());
							break;
						}
					}
				}
			}
		} else if (HttpStatus.UNAUTHORIZED == responseStatus) {
			log.info(
					"Setting token to null as it was an unauthorized request. Response HTTP code: {}.",
					responseStatus.value());
			sessionAccess.setToken(null);

		}

		return response;
	}
}
