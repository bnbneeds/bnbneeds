package com.bnbneeds.rest.client.core;

import java.net.URI;
import java.util.List;
import java.util.Map;

import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.notification.NotificationListParam;
import com.bnbneeds.app.model.notification.NotificationListResponse;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class Notifications extends SearchableResource {

	private static final Class<NotificationListResponse> NOTIFICATION_LIST_RESPONSE_TYPE = NotificationListResponse.class;
	private static final URI NOTIFICATION_API_REQUEST_URI = Endpoint.NOTIFICATIONS
			.get();

	public Notifications(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public NotificationListResponse list() {
		return list(NOTIFICATION_LIST_RESPONSE_TYPE,
				NOTIFICATION_API_REQUEST_URI);
	}

	public NotificationListResponse list(int fetchSize,
			Map<String, String> queryFilterMap, String nextOffset) {
		return list(NOTIFICATION_LIST_RESPONSE_TYPE,
				NOTIFICATION_API_REQUEST_URI, fetchSize, queryFilterMap,
				nextOffset);
	}

	public NotificationListResponse list(int fetchSize,
			Map<String, String> queryFilterMap) {
		return list(NOTIFICATION_LIST_RESPONSE_TYPE,
				NOTIFICATION_API_REQUEST_URI, fetchSize, queryFilterMap);
	}

	public NotificationListResponse list(int fetchSize) {
		return list(NOTIFICATION_LIST_RESPONSE_TYPE,
				NOTIFICATION_API_REQUEST_URI, fetchSize);
	}

	public NotificationListResponse list(int fetchSize, String nextOffset) {
		return super.list(NOTIFICATION_LIST_RESPONSE_TYPE,
				NOTIFICATION_API_REQUEST_URI, fetchSize, nextOffset);
	}

	public NotificationListResponse list(String nextOffset) {
		return list(NOTIFICATION_LIST_RESPONSE_TYPE,
				NOTIFICATION_API_REQUEST_URI, nextOffset);
	}

	public NotificationListResponse list(Map<String, String> queryFilterMap) {
		return list(NOTIFICATION_LIST_RESPONSE_TYPE,
				NOTIFICATION_API_REQUEST_URI, queryFilterMap);
	}

	public NotificationListResponse list(Map<String, String> queryFilterMap,
			String nextOffset) {
		return super.list(NOTIFICATION_LIST_RESPONSE_TYPE,
				NOTIFICATION_API_REQUEST_URI, queryFilterMap, nextOffset);
	}

	public String listAsString() {
		return list(String.class, NOTIFICATION_API_REQUEST_URI);
	}

	public String listAsString(int fetchSize,
			Map<String, String> queryFilterMap, String nextOffset) {
		return list(String.class, NOTIFICATION_API_REQUEST_URI, fetchSize,
				queryFilterMap, nextOffset);
	}

	public String listAsString(int fetchSize, Map<String, String> queryFilterMap) {
		return super.list(String.class, NOTIFICATION_API_REQUEST_URI,
				fetchSize, queryFilterMap);
	}

	public String listAsString(int fetchSize) {
		return list(String.class, NOTIFICATION_API_REQUEST_URI, fetchSize);
	}

	public String listAsString(int fetchSize, String nextOffset) {
		return list(String.class, NOTIFICATION_API_REQUEST_URI, fetchSize,
				nextOffset);
	}

	public String listAsString(String nextOffset) {
		return list(String.class, NOTIFICATION_API_REQUEST_URI, nextOffset);
	}

	public String listAsString(Map<String, String> queryFilterMap) {
		return list(String.class, NOTIFICATION_API_REQUEST_URI, queryFilterMap);
	}

	public String listAsString(Map<String, String> queryFilterMap,
			String nextOffset) {
		return super.list(String.class, NOTIFICATION_API_REQUEST_URI,
				queryFilterMap, nextOffset);
	}

	public TaskResponse markAsRead(List<String> notificationIdList) {
		NotificationListParam param = new NotificationListParam();
		param.setNotificationIdList(notificationIdList);
		return update(param, Endpoint.MARK_NOTIFICATIONS_AS_READ.get());
	}

	public TaskResponse markAsUnread(List<String> notificationIdList) {
		NotificationListParam param = new NotificationListParam();
		param.setNotificationIdList(notificationIdList);
		return update(param, Endpoint.MARK_NOTIFICATIONS_AS_UNREAD.get());
	}
}
