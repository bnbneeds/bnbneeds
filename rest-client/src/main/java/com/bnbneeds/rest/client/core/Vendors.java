package com.bnbneeds.rest.client.core;

import java.util.HashMap;
import java.util.Map;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.UploadFormResponse;
import com.bnbneeds.app.model.dealer.BuyerListResponse;
import com.bnbneeds.app.model.dealer.CreateVendorParam;
import com.bnbneeds.app.model.dealer.EnquiryInfoListResponse;
import com.bnbneeds.app.model.dealer.UpdateVendorParam;
import com.bnbneeds.app.model.dealer.VendorInfoListResponse;
import com.bnbneeds.app.model.dealer.VendorInfoResponse;
import com.bnbneeds.app.model.dealer.VendorListResponse;
import com.bnbneeds.app.model.dealer.order.AggregatedPurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemInfoResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.PurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.UpdatePurchaseListParam;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.CreateProductParam;
import com.bnbneeds.app.model.product.ProductNameListResponse;
import com.bnbneeds.app.model.product.UpdateProductParam;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class Vendors extends SearchableResource {

	private String vendorId;

	public Vendors(RestClient restClient, BNBNeedsClient parent) {
		super(restClient, parent);
	}

	public Vendors(RestClient client, BNBNeedsClient bnbNeedsClient, String vendorId) {
		this(client, bnbNeedsClient);
		this.vendorId = vendorId;
	}

	public CreateResourceResponse create(CreateVendorParam param) {
		return create(param, Endpoint.VENDORS.get());
	}

	public TaskResponse update(String vendorId, UpdateVendorParam param) {
		return update(param, Endpoint.VENDOR_INFO.get(vendorId));
	}

	public TaskResponse update(UpdateVendorParam param) {
		return update(vendorId, param);
	}

	public TaskResponse delete(String vendorId) {
		return delete(Endpoint.VENDOR_INFO.get(vendorId), TrueOrFalse.FALSE);
	}

	public TaskResponse delete() {
		return delete(vendorId);
	}

	public CreateResourceResponse deactivate(String vendorId, boolean forceDelete) {
		TrueOrFalse trueOrFalse = forceDelete ? TrueOrFalse.TRUE : TrueOrFalse.FALSE;
		return create(Endpoint.VENDOR_DEACTIVATE.get(vendorId), trueOrFalse);
	}

	public CreateResourceResponse deactivate(boolean forceDelete) {
		return deactivate(vendorId, forceDelete);
	}

	public VendorInfoResponse get(String vendorId) {
		return restClient.get(VendorInfoResponse.class, Endpoint.VENDOR_INFO.get(vendorId));
	}

	public String getAsString() {
		return getAsString(vendorId);
	}

	public String getAsString(String vendorId) {
		return restClient.get(String.class, Endpoint.VENDOR_INFO.get(vendorId));
	}

	public VendorInfoResponse get() {
		return get(vendorId);
	}

	public VendorListResponse list() {
		return list(VendorListResponse.class, Endpoint.VENDORS.get());
	}

	public VendorListResponse list(int fetchSize, Map<String, String> queryFilterMap, String nextOffset) {
		return list(VendorListResponse.class, Endpoint.VENDORS.get(), fetchSize, queryFilterMap, nextOffset);
	}

	public VendorListResponse list(int fetchSize, String nextOffset) {
		return list(VendorListResponse.class, Endpoint.VENDORS.get(), fetchSize, nextOffset);
	}

	public VendorListResponse list(Map<String, String> queryFilterMap, String nextOffset) {
		return list(VendorListResponse.class, Endpoint.VENDORS.get(), queryFilterMap, nextOffset);
	}

	public VendorListResponse list(Map<String, String> queryFilterMap) {
		return list(VendorListResponse.class, Endpoint.VENDORS.get(), queryFilterMap);
	}

	public VendorListResponse list(String nextOffset) {
		return list(VendorListResponse.class, Endpoint.VENDORS.get(), nextOffset);
	}

	public VendorListResponse list(int fetchSize) {
		return list(VendorListResponse.class, Endpoint.VENDORS.get(), fetchSize);
	}

	public VendorInfoListResponse listBulk() {
		return list(VendorInfoListResponse.class, Endpoint.VENDORS_BULK.get());
	}

	public VendorInfoListResponse listBulk(int fetchSize, Map<String, String> queryFilterMap, String nextOffset) {
		return list(VendorInfoListResponse.class, Endpoint.VENDORS_BULK.get(), fetchSize, queryFilterMap, nextOffset);
	}

	public VendorInfoListResponse listBulk(Map<String, String> queryFilterMap, String nextOffset) {
		return list(VendorInfoListResponse.class, Endpoint.VENDORS_BULK.get(), queryFilterMap, nextOffset);
	}

	public VendorInfoListResponse listBulk(int fetchSize, String nextOffset) {
		return list(VendorInfoListResponse.class, Endpoint.VENDORS_BULK.get(), fetchSize, nextOffset);
	}

	public VendorInfoListResponse listBulk(Map<String, String> queryFilterMap) {
		return list(VendorInfoListResponse.class, Endpoint.VENDORS_BULK.get(), queryFilterMap);
	}

	public VendorInfoListResponse listBulk(String nextOffset) {
		return list(VendorInfoListResponse.class, Endpoint.VENDORS_BULK.get(), nextOffset);
	}

	public VendorInfoListResponse listBulk(int fetchSize) {
		return list(VendorInfoListResponse.class, Endpoint.VENDORS_BULK.get(), fetchSize);
	}

	public VendorInfoListResponse listBulk(int fetchSize, Map<String, String> queryFilterMap) {
		return list(VendorInfoListResponse.class, Endpoint.VENDORS_BULK.get(), fetchSize, queryFilterMap);
	}

	public CreateResourceResponse createProduct(String vendorId, CreateProductParam param) {
		return create(param, Endpoint.VENDOR_PRODUCTS.get(vendorId));
	}

	public CreateResourceResponse createProduct(CreateProductParam param) {
		return createProduct(vendorId, param);
	}

	public TaskResponse updateProduct(String vendorId, String productId, UpdateProductParam param) {
		return update(param, Endpoint.VENDOR_PRODUCT_INFO.get(vendorId, productId));
	}

	public TaskResponse updateProduct(String productId, UpdateProductParam param) {
		return updateProduct(vendorId, productId, param);
	}

	public CreateResourceResponse deactivateProduct(String vendorId, String productId) {
		return create(Endpoint.VENDOR_PRODUCT_DEACTIVATE.get(vendorId, productId));
	}

	public CreateResourceResponse deactivateProduct(String productId) {
		return deactivateProduct(vendorId, productId);
	}

	public CreateResourceResponse deactivateProduct(String vendorId, String productId, boolean forceDelete) {
		if (forceDelete) {
			return create(Endpoint.VENDOR_PRODUCT_DEACTIVATE.get(vendorId, productId), TrueOrFalse.TRUE);
		}
		return deactivateProduct(vendorId, productId);
	}

	public CreateResourceResponse deactivateProduct(String productId, boolean forceDelete) {
		return deactivateProduct(vendorId, productId, forceDelete);
	}

	public UploadFormResponse getProductImageUploadForm(String vendorId, String productId) {
		return restClient.get(UploadFormResponse.class,
				Endpoint.VENDOR_PRODUCT_IMAGE_UPLOAD_FORM.get(vendorId, productId));
	}

	public UploadFormResponse getProductImageUploadForm(String productId) {
		return getProductImageUploadForm(vendorId, productId);
	}

	public TaskResponse deleteProductImage(String vendorId, String productId, String imageId) {
		return restClient.delete(Endpoint.VENDOR_PRODUCT_IMAGE.get(vendorId, productId, imageId));
	}

	public TaskResponse deleteProductImage(String productId, String imageId) {
		return deleteProductImage(vendorId, productId, imageId);
	}

	public PurchaseListResponse getPurchaseList(String vendorId, int fetchSize) {
		return list(PurchaseListResponse.class, Endpoint.VENDOR_PURCHASE_ITEMS.get(vendorId), fetchSize);
	}

	public PurchaseListResponse getPurchaseList(int fetchSize) {
		return getPurchaseList(vendorId, fetchSize);
	}

	public PurchaseListResponse getPurchaseList(String vendorId, int fetchSize, String nextOffset) {

		return list(PurchaseListResponse.class, Endpoint.VENDOR_PURCHASE_ITEMS.get(vendorId), fetchSize, nextOffset);

	}

	public PurchaseListResponse getPurchaseList(int fetchSize, String nextOffset) {
		return getPurchaseList(vendorId, fetchSize, nextOffset);
	}

	public PurchaseListResponse getPurchaseList(String vendorId, int fetchSize, Map<String, String> queryFilterMap,
			String nextOffset) {

		return list(PurchaseListResponse.class, Endpoint.VENDOR_PURCHASE_ITEMS.get(vendorId), fetchSize, queryFilterMap,
				nextOffset);
	}

	public PurchaseListResponse getPurchaseList(int fetchSize, Map<String, String> queryFilterMap, String nextOffset) {
		return getPurchaseList(vendorId, fetchSize, queryFilterMap, nextOffset);
	}

	public PurchaseListResponse getPurchaseList(String vendorId, int fetchSize, Map<String, String> queryFilterMap) {
		return list(PurchaseListResponse.class, Endpoint.VENDOR_PURCHASE_ITEMS.get(vendorId), fetchSize,
				queryFilterMap);
	}

	public PurchaseListResponse getPurchaseList(int fetchSize, Map<String, String> queryFilterMap) {
		return getPurchaseList(vendorId, fetchSize, queryFilterMap);
	}

	public PurchaseItemInfoResponse getPurchaseItem(String vendorId, String itemId) {
		return restClient.get(PurchaseItemInfoResponse.class, Endpoint.VENDOR_PURCHASE_ITEM_INFO.get(vendorId, itemId));
	}

	public PurchaseItemInfoResponse getPurchaseItem(String itemId) {
		return getPurchaseItem(vendorId, itemId);
	}

	public TaskResponse updatePurchaseItem(String vendorId, String itemId, PurchaseItemParam purchaseItemParam) {
		return update(purchaseItemParam, Endpoint.VENDOR_PURCHASE_ITEM_INFO.get(vendorId, itemId));
	}

	public TaskResponse updatePurchaseItem(String itemId, PurchaseItemParam purchaseItemParam) {
		return updatePurchaseItem(vendorId, itemId, purchaseItemParam);
	}

	public TaskResponse updatePurchaseList(String vendorId, UpdatePurchaseListParam purchaseListParam) {
		return update(purchaseListParam, Endpoint.VENDOR_PURCHASE_ITEMS.get(vendorId));
	}

	public TaskResponse updatePurchaseList(UpdatePurchaseListParam purchaseListParam) {
		return updatePurchaseList(vendorId, purchaseListParam);
	}

	private <T> T getAssociatedProductNameListOfPurchaseItems(Class<T> responseType, String vendorId,
			String requestDate) {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("requestDate", requestDate);

		return list(responseType, Endpoint.VENDOR_PURCHASE_ITEMS_ASSOCIATED_PRODUCT_NAMES.get(vendorId), queryMap);
	}

	private <T> T getAssociatedProductNameListOfPurchaseItemsByDateRange(Class<T> responseType, String vendorId,
			String startRequestDate, String endRequestDate) {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("startRequestDate", startRequestDate);
		queryMap.put("endRequestDate", endRequestDate);

		return list(responseType, Endpoint.VENDOR_PURCHASE_ITEMS_ASSOCIATED_PRODUCT_NAMES.get(vendorId), queryMap);
	}

	public ProductNameListResponse getAssociatedProductNameListOfPurchaseItems(String vendorId, String requestDate) {
		return getAssociatedProductNameListOfPurchaseItems(ProductNameListResponse.class, vendorId, requestDate);
	}

	public String getAssociatedProductNameListOfPurchaseItemsAsString(String vendorId, String requestDate) {
		return getAssociatedProductNameListOfPurchaseItems(String.class, vendorId, requestDate);
	}

	public String getAssociatedProductNameListOfPurchaseItemsAsString(String requestDate) {
		return getAssociatedProductNameListOfPurchaseItems(String.class, vendorId, requestDate);
	}

	public ProductNameListResponse getAssociatedProductNameListOfPurchaseItems(String requestDate) {
		return getAssociatedProductNameListOfPurchaseItems(vendorId, requestDate);
	}

	public ProductNameListResponse getAssociatedProductNameListOfPurchaseItemsByDateRange(String vendorId,
			String startRequestDate, String endRequestDate) {
		return getAssociatedProductNameListOfPurchaseItemsByDateRange(ProductNameListResponse.class, vendorId,
				startRequestDate, endRequestDate);
	}

	public ProductNameListResponse getAssociatedProductNameListOfPurchaseItemsByDateRange(String startRequestDate,
			String endRequestDate) {
		return getAssociatedProductNameListOfPurchaseItemsByDateRange(vendorId, startRequestDate, endRequestDate);
	}

	public String getAssociatedProductNameListOfPurchaseItemsByDateRangeAsString(String vendorId,
			String startRequestDate, String endRequestDate) {
		return getAssociatedProductNameListOfPurchaseItemsByDateRange(String.class, vendorId, startRequestDate,
				endRequestDate);
	}

	public String getAssociatedProductNameListOfPurchaseItemsByDateRangeAsString(String startRequestDate,
			String endRequestDate) {
		return getAssociatedProductNameListOfPurchaseItemsByDateRangeAsString(vendorId, startRequestDate,
				endRequestDate);
	}

	private <T> T getAssociatedBuyerListOfPurchaseItems(Class<T> responseType, String vendorId, String requestDate) {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("requestDate", requestDate);
		return list(responseType, Endpoint.VENDOR_PURCHASE_ITEMS_ASSOCIATED_BUYERS.get(vendorId), queryMap);
	}

	private <T> T getAssociatedBuyerListOfPurchaseItemsByDateRange(Class<T> responseType, String vendorId,
			String startRequestDate, String endRequestDate) {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("startRequestDate", startRequestDate);
		queryMap.put("endRequestDate", endRequestDate);
		return list(responseType, Endpoint.VENDOR_PURCHASE_ITEMS_ASSOCIATED_BUYERS.get(vendorId), queryMap);
	}

	public BuyerListResponse getAssociatedBuyerListOfPurchaseItemsByDateRange(String vendorId, String startRequestDate,
			String endRequestDate) {
		return getAssociatedBuyerListOfPurchaseItemsByDateRange(BuyerListResponse.class, vendorId, startRequestDate,
				endRequestDate);
	}

	public BuyerListResponse getAssociatedBuyerListOfPurchaseItemsByDateRange(String startRequestDate,
			String endRequestDate) {
		return getAssociatedBuyerListOfPurchaseItemsByDateRange(vendorId, startRequestDate, endRequestDate);
	}

	public String getAssociatedBuyerListOfPurchaseItemsByDateRangeAsString(String vendorId, String startRequestDate,
			String endRequestDate) {
		return getAssociatedBuyerListOfPurchaseItemsByDateRange(String.class, vendorId, startRequestDate,
				endRequestDate);
	}

	public String getAssociatedBuyerListOfPurchaseItemsByDateRangeAsString(String startRequestDate,
			String endRequestDate) {
		return getAssociatedBuyerListOfPurchaseItemsByDateRangeAsString(vendorId, startRequestDate, endRequestDate);
	}

	public BuyerListResponse getAssociatedBuyerListOfPurchaseItems(String vendorId, String requestDate) {
		return getAssociatedBuyerListOfPurchaseItems(BuyerListResponse.class, vendorId, requestDate);
	}

	public BuyerListResponse getAssociatedBuyerListOfPurchaseItems(String requestDate) {
		return getAssociatedBuyerListOfPurchaseItems(vendorId, requestDate);
	}

	public String getAssociatedBuyerListOfPurchaseItemsAsString(String vendorId, String requestDate) {
		return getAssociatedBuyerListOfPurchaseItems(String.class, vendorId, requestDate);
	}

	public String getAssociatedBuyerListOfPurchaseItemsAsString(String requestDate) {
		return getAssociatedBuyerListOfPurchaseItems(String.class, vendorId, requestDate);
	}

	public AggregatedPurchaseListResponse getAggregatedPurchaseList(String vendorId, String requestDate,
			String productNameId, String buyerId) {
		Map<String, String> queryFilterMap = new HashMap<>();
		queryFilterMap.put("requestDate", requestDate);
		if (productNameId != null && !productNameId.trim().isEmpty()) {
			queryFilterMap.put("productNameId", productNameId);
		}
		if (buyerId != null && !buyerId.trim().isEmpty()) {
			queryFilterMap.put("buyerId", buyerId);
		}

		return list(AggregatedPurchaseListResponse.class, Endpoint.VENDOR_AGGREGATED_PURCHASE_LIST.get(vendorId),
				queryFilterMap);
	}

	public AggregatedPurchaseListResponse getAggregatedPurchaseList(String requestDate, String productNameId,
			String buyerId) {
		return getAggregatedPurchaseList(vendorId, requestDate, productNameId, buyerId);
	}

	public AggregatedPurchaseListResponse getAggregatedPurchaseListByDateRange(String vendorId, String startRequestDate,
			String endRequestDate, String productNameId, String buyerId) {
		Map<String, String> queryFilterMap = new HashMap<>();
		queryFilterMap.put("startRequestDate", startRequestDate);
		queryFilterMap.put("endRequestDate", endRequestDate);
		if (productNameId != null && !productNameId.trim().isEmpty()) {
			queryFilterMap.put("productNameId", productNameId);
		}
		if (buyerId != null && !buyerId.trim().isEmpty()) {
			queryFilterMap.put("buyerId", buyerId);
		}

		return list(AggregatedPurchaseListResponse.class, Endpoint.VENDOR_AGGREGATED_PURCHASE_LIST.get(vendorId),
				queryFilterMap);
	}

	public AggregatedPurchaseListResponse getAggregatedPurchaseListByDateRange(String startRequestDate,
			String endRequestDate, String productNameId, String buyerId) {
		return getAggregatedPurchaseListByDateRange(vendorId, startRequestDate, endRequestDate, productNameId, buyerId);
	}

	public EnquiryInfoListResponse getEnquiries(String vendorId, int fetchSize) {
		return list(EnquiryInfoListResponse.class, Endpoint.VENDOR_PRODUCT_ENQUIRIES.get(vendorId), fetchSize);
	}

	public EnquiryInfoListResponse getEnquiries(int fetchSize) {
		return getEnquiries(vendorId, fetchSize);
	}

	public EnquiryInfoListResponse getEnquiries(String vendorId, int fetchSize, String nextOffset) {
		return list(EnquiryInfoListResponse.class, Endpoint.VENDOR_PRODUCT_ENQUIRIES.get(vendorId), fetchSize,
				nextOffset);
	}

	public EnquiryInfoListResponse getEnquiries(int fetchSize, String nextOffset) {
		return list(EnquiryInfoListResponse.class, Endpoint.VENDOR_PRODUCT_ENQUIRIES.get(vendorId), fetchSize,
				nextOffset);
	}

	public EnquiryInfoListResponse getEnquiries(String vendorId, int fetchSize, Map<String, String> queryMap) {
		return list(EnquiryInfoListResponse.class, Endpoint.VENDOR_PRODUCT_ENQUIRIES.get(vendorId), fetchSize,
				queryMap);
	}

	public EnquiryInfoListResponse getEnquiries(int fetchSize, Map<String, String> queryMap) {
		return getEnquiries(vendorId, fetchSize, queryMap);
	}

	public EnquiryInfoListResponse getEnquiries(String vendorId, int fetchSize, Map<String, String> queryFilterMap,
			String nextOffset) {

		return list(EnquiryInfoListResponse.class, Endpoint.VENDOR_PRODUCT_ENQUIRIES.get(vendorId), fetchSize,
				queryFilterMap, nextOffset);
	}

	public EnquiryInfoListResponse getEnquiries(int fetchSize, Map<String, String> queryFilterMap, String nextOffset) {

		return getEnquiries(vendorId, fetchSize, queryFilterMap, nextOffset);
	}

	private <T> T getAssociatedBuyerListOfEnquiries(Class<T> responseType, String vendorId, String requestDate) {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("enquiryDate", requestDate);
		return list(responseType, Endpoint.VENDOR_PRODUCT_ENQUIRIES_ASSOCIATED_BUYERS.get(vendorId), queryMap);
	}

	private <T> T getAssociatedBuyerListOfEnquiriesByDateRange(Class<T> responseType, String vendorId,
			String startEnquiryDate, String endEnquiryDate) {
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("startEnquiryDate", startEnquiryDate);
		queryMap.put("endEnquiryDate", endEnquiryDate);
		return list(responseType, Endpoint.VENDOR_PRODUCT_ENQUIRIES_ASSOCIATED_BUYERS.get(vendorId), queryMap);
	}

	public BuyerListResponse getAssociatedBuyerListOfEnquiries(String vendorId, String requestDate) {
		return getAssociatedBuyerListOfEnquiries(BuyerListResponse.class, vendorId, requestDate);
	}

	public BuyerListResponse getAssociatedBuyerListOfEnquiries(String requestDate) {
		return getAssociatedBuyerListOfEnquiries(BuyerListResponse.class, vendorId, requestDate);
	}

	public String getAssociatedBuyerListOfEnquiriesAsString(String vendorId, String requestDate) {
		return getAssociatedBuyerListOfEnquiries(String.class, vendorId, requestDate);
	}

	public String getAssociatedBuyerListOfEnquiriesAsString(String requestDate) {
		return getAssociatedBuyerListOfEnquiries(String.class, vendorId, requestDate);
	}

	public BuyerListResponse getAssociatedBuyerListOfEnquiriesByDateRange(String vendorId, String startEnquiryDate,
			String endEnquiryDate) {
		return getAssociatedBuyerListOfEnquiriesByDateRange(BuyerListResponse.class, vendorId, startEnquiryDate,
				endEnquiryDate);
	}

	public BuyerListResponse getAssociatedBuyerListOfEnquiriesByDateRange(String startEnquiryDate,
			String endEnquiryDate) {
		return getAssociatedBuyerListOfEnquiriesByDateRange(vendorId, startEnquiryDate, endEnquiryDate);
	}

	public String getAssociatedBuyerListOfEnquiriesByDateRangeAsString(String vendorId, String startEnquiryDate,
			String endEnquiryDate) {
		return getAssociatedBuyerListOfEnquiriesByDateRange(String.class, vendorId, startEnquiryDate, endEnquiryDate);
	}

	public String getAssociatedBuyerListOfEnquiriesByDateRangeAsString(String startEnquiryDate, String endEnquiryDate) {
		return getAssociatedBuyerListOfEnquiriesByDateRangeAsString(vendorId, startEnquiryDate, endEnquiryDate);
	}

	public TaskResponse updateEntityStatus(String entityId, EntityApprovalParam param) {
		return restClient.put(TaskResponse.class, param, Endpoint.ADMIN_ENTITY_BY_ID_APPROVALS.get(entityId));
	}
}
