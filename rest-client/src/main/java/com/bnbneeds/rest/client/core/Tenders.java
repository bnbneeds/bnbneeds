package com.bnbneeds.rest.client.core;

import java.util.Map;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.tender.AddTenderProductsParam;
import com.bnbneeds.app.model.dealer.tender.CreateTenderParam;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderProductInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderProductInfoResponse;
import com.bnbneeds.app.model.dealer.tender.UpdateTenderProductParam;
import com.bnbneeds.app.model.dealer.tender.UpdateTenderParam;
import com.bnbneeds.app.model.dealer.tender.UpdateTenderStatusParam;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class Tenders extends SearchableResource {

	private String buyerId;

	public Tenders(RestClient client, BNBNeedsClient bnbNeedsClient, String buyerId) {
		super(client, bnbNeedsClient);
		this.buyerId = buyerId;
	}

	public CreateResourceResponse create(CreateTenderParam param) {
		return create(param, Endpoint.BUYER_TENDERS.get(buyerId));
	}

	public TaskResponse delete(String tenderId) {
		return delete(Endpoint.BUYER_TENDER_INFO.get(buyerId, tenderId));
	}

	public TaskResponse deleteBidProduct(String tenderId, String bidProductId) {
		return delete(Endpoint.BUYER_TENDER_PRODUCT_INFO.get(buyerId, tenderId, bidProductId));
	}

	public TenderInfoResponse get(String tenderId) {
		return restClient.get(TenderInfoResponse.class, Endpoint.BUYER_TENDER_INFO.get(buyerId, tenderId));
	}

	public TenderProductInfoResponse getBidProduct(String tenderId, String bidProductId) {
		return restClient.get(TenderProductInfoResponse.class,
				Endpoint.BUYER_TENDER_PRODUCT_INFO.get(buyerId, tenderId, bidProductId));
	}

	public TenderInfoListResponse list() {
		return list(TenderInfoListResponse.class, Endpoint.BUYER_TENDERS.get(buyerId));
	}

	public TenderInfoListResponse list(int fetchSize, Map<String, String> queryFilterMap, String nextOffset) {
		return list(TenderInfoListResponse.class, Endpoint.BUYER_TENDERS.get(buyerId), fetchSize, queryFilterMap,
				nextOffset);
	}

	public TenderInfoListResponse list(Map<String, String> queryFilterMap, String nextOffset) {
		return list(TenderInfoListResponse.class, Endpoint.BUYER_TENDERS.get(buyerId), queryFilterMap, nextOffset);
	}

	public TenderInfoListResponse list(int fetchSize, String nextOffset) {
		return list(TenderInfoListResponse.class, Endpoint.BUYER_TENDERS.get(buyerId), fetchSize, nextOffset);
	}

	public TenderInfoListResponse list(Map<String, String> queryFilterMap) {
		return list(TenderInfoListResponse.class, Endpoint.BUYER_TENDERS.get(buyerId), queryFilterMap);
	}

	public TenderInfoListResponse list(int fetchSize, Map<String, String> queryFilterMap) {
		return list(TenderInfoListResponse.class, Endpoint.BUYER_TENDERS.get(buyerId), fetchSize, queryFilterMap);
	}

	public TenderInfoListResponse list(String nextOffset) {
		return list(TenderInfoListResponse.class, Endpoint.BUYER_TENDERS.get(buyerId), nextOffset);
	}

	public TenderInfoListResponse list(int fetchSize) {
		return list(TenderInfoListResponse.class, Endpoint.BUYER_TENDERS.get(buyerId), fetchSize);
	}

	public TaskResponse updateTender(String tenderId, UpdateTenderParam param) {
		return update(param, Endpoint.BUYER_TENDER_INFO.get(buyerId, tenderId));
	}

	public TaskResponse updateTenderStatus(String tenderId, UpdateTenderStatusParam param) {
		return update(param, Endpoint.BUYER_TENDER_STATUS.get(buyerId, tenderId));
	}

	public TaskResponse addBidProducts(String tenderId, AddTenderProductsParam param) {
		return create(param, Endpoint.BUYER_TENDER_PRODUCTS.get(buyerId, tenderId));
	}

	public TaskResponse updateBidProduct(String tenderId, String bidProductId, UpdateTenderProductParam param) {
		return update(param, Endpoint.BUYER_TENDER_PRODUCT_INFO.get(buyerId, tenderId, bidProductId));
	}

	public TenderBidInfoListResponse listTenderBids(String tenderId, int fetchSize, Map<String, String> queryFilterMap,
			String nextOffset) {
		return list(TenderBidInfoListResponse.class, Endpoint.BUYER_TENDER_BIDS.get(buyerId, tenderId), fetchSize,
				queryFilterMap, nextOffset);
	}

	public TenderBidInfoListResponse listTenderBids(String tenderId, int fetchSize, String nextOffset) {
		return list(TenderBidInfoListResponse.class, Endpoint.BUYER_TENDER_BIDS.get(buyerId, tenderId), fetchSize,
				nextOffset);
	}

	public TenderBidInfoListResponse listTenderBids(String tenderId, int fetchSize) {
		return list(TenderBidInfoListResponse.class, Endpoint.BUYER_TENDER_BIDS.get(buyerId, tenderId), fetchSize);
	}

	public TenderBidInfoListResponse listTenderBids(String tenderId, Map<String, String> queryFilterMap,
			String nextOffset) {
		return list(TenderBidInfoListResponse.class, Endpoint.BUYER_TENDER_BIDS.get(buyerId, tenderId), queryFilterMap,
				nextOffset);
	}

	public TenderBidInfoListResponse listTenderBids(String tenderId, Map<String, String> queryFilterMap) {
		return list(TenderBidInfoListResponse.class, Endpoint.BUYER_TENDER_BIDS.get(buyerId, tenderId), queryFilterMap);
	}

	public TenderBidInfoListResponse listTenderBids(String tenderId, String nextOffset) {
		return list(TenderBidInfoListResponse.class, Endpoint.BUYER_TENDER_BIDS.get(buyerId, tenderId), nextOffset);
	}

	public TenderBidInfoListResponse listTenderBids(int fetchSize) {
		return list(TenderBidInfoListResponse.class, Endpoint.TENDER_BIDS.get(), fetchSize);
	}

	public TenderProductInfoListResponse listTenderProducts(String tenderId, int fetchSize, String nextOffset) {
		return list(TenderProductInfoListResponse.class, Endpoint.BUYER_TENDER_PRODUCTS.get(buyerId, tenderId),
				fetchSize, nextOffset);
	}

	public String listTenderProductsAsString(String tenderId, int fetchSize, String nextOffset) {
		return list(String.class, Endpoint.BUYER_TENDER_PRODUCTS.get(buyerId, tenderId), fetchSize, nextOffset);
	}

	public String listTenderProductsAsString(String tenderId, int fetchSize) {
		return list(String.class, Endpoint.BUYER_TENDER_PRODUCTS.get(buyerId, tenderId), fetchSize);
	}

	public String listTenderBidsAsString(String tenderId, int fetchSize, String nextOffset) {
		return list(String.class, Endpoint.BUYER_TENDER_BIDS.get(buyerId, tenderId), fetchSize, nextOffset);
	}

	public String listTenderBidsAsString(String tenderId, int fetchSize) {
		return list(String.class, Endpoint.BUYER_TENDER_BIDS.get(buyerId, tenderId), fetchSize);
	}

	public TenderProductInfoListResponse listBidProducts(String tenderId, int fetchSize) {
		return list(TenderProductInfoListResponse.class, Endpoint.BUYER_TENDER_PRODUCTS.get(buyerId, tenderId),
				fetchSize);
	}

	public TenderInfoResponse getLowestBid(String tenderId) {
		return restClient.get(TenderInfoResponse.class, Endpoint.BUYER_TENDER_LOWEST_BID.get(buyerId, tenderId));
	}

}
