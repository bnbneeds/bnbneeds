package com.bnbneeds.rest.client.core;

import java.util.HashMap;
import java.util.Map;

import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.dealer.BuyerListResponse;
import com.bnbneeds.app.model.dealer.EnquiryInfoListResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.review.BuyerReviewInfoResponse;
import com.bnbneeds.app.model.review.ProductReviewInfoResponse;
import com.bnbneeds.app.model.review.ReviewInfoResponse;
import com.bnbneeds.app.model.review.VendorReviewInfoResponse;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;
import com.bnbneeds.rest.client.core.Reviews.ReviewFor;

public class AdminEntities extends SearchableResource {


	public AdminEntities(RestClient restClient, BNBNeedsClient parent) {
		super(restClient, parent);
	}

	public TaskResponse updateEntityStatus(String entityId, EntityApprovalParam param) {
		return restClient.put(TaskResponse.class, param,
				Endpoint.ADMIN_ENTITY_BY_ID_APPROVALS.get(entityId));
	}
	
	public EnquiryInfoListResponse getAllEnquiries( int fetchSize) {
		return list(EnquiryInfoListResponse.class,
				Endpoint.ADMIN_PRODUCT_ENQUIRIES.get(), fetchSize);
	}
	
	public EnquiryInfoListResponse getAllEnquiries(int fetchSize, String nextOffset) {
		return list(EnquiryInfoListResponse.class,
				Endpoint.ADMIN_PRODUCT_ENQUIRIES.get(), fetchSize,
				nextOffset);
	}
	
	public EnquiryInfoListResponse getAllEnquiries( int fetchSize,
			Map<String, String> queryMap) {
		return list(EnquiryInfoListResponse.class,
				Endpoint.ADMIN_PRODUCT_ENQUIRIES.get(), fetchSize,
				queryMap);
	}
	
	public EnquiryInfoListResponse getAllEnquiries( int fetchSize,
			Map<String, String> queryFilterMap, String nextOffset) {

		return list(EnquiryInfoListResponse.class,
				Endpoint.ADMIN_PRODUCT_ENQUIRIES.get(), fetchSize,
				queryFilterMap, nextOffset);
	}

	private <T> T getAssociatedBuyerListOfEnquiries(Class<T> responseType,
			String requestDate) {
		Map<String, String> queryMap = new HashMap<String, String>();
		queryMap.put("enquiryDate", requestDate);
		return list(responseType,
				Endpoint.ADMIN_PRODUCT_ENQUIRIES_ASSOCIATED_BUYERS
						.get(), queryMap);
	}

	public BuyerListResponse getAssociatedBuyerListOfEnquiries(String requestDate) {
		return getAssociatedBuyerListOfEnquiries(BuyerListResponse.class,
				 requestDate);
	}

	public String getAssociatedBuyerListOfEnquiriesAsString(String requestDate) {
		return getAssociatedBuyerListOfEnquiries(String.class, requestDate);
	}

	private <T> T getAssociatedBuyerListOfEnquiriesByDateRange(
			Class<T> responseType,  String startEnquiryDate,
			String endEnquiryDate) {
		Map<String, String> queryMap = new HashMap<String, String>();
		queryMap.put("startEnquiryDate", startEnquiryDate);
		queryMap.put("endEnquiryDate", endEnquiryDate);
		return list(responseType,
				Endpoint.ADMIN_PRODUCT_ENQUIRIES_ASSOCIATED_BUYERS
						.get(), queryMap);
	}

	public BuyerListResponse getAssociatedBuyerListOfEnquiriesByDateRange(
			 String startEnquiryDate, String endEnquiryDate) {
		return getAssociatedBuyerListOfEnquiriesByDateRange(
				BuyerListResponse.class, startEnquiryDate,
				endEnquiryDate);
	}

	public String getAssociatedBuyerListOfEnquiriesByDateRangeAsString(
			String startEnquiryDate, String endEnquiryDate) {
		return getAssociatedBuyerListOfEnquiriesByDateRange(String.class,
				 startEnquiryDate, endEnquiryDate);
	}
	
	public ReviewInfoResponse getReviewById(ReviewFor reviewFor,String reviewId) {
		return restClient.get(getReviewInfoResponseClass(reviewFor),
				Endpoint.ADMIN_REVIEW.get(reviewId));
	}
	
	private Class<? extends ReviewInfoResponse> getReviewInfoResponseClass(ReviewFor reviewFor) {
		switch (reviewFor) {
		case vendor:
			return VendorReviewInfoResponse.class;
		case buyer:
			return BuyerReviewInfoResponse.class;
		case product:
			return ProductReviewInfoResponse.class;
		default:
			break;
		}
		return null;
	}
	
	public UserAccountInfoResponse getUserAccountByRelationshipId(
			 String relationshipId) {
		return restClient.get(UserAccountInfoResponse.class,
				Endpoint.ADMIN_USER_ACCOUNT_BY_RELATIONSHIP.get(relationshipId));
	}
	
	public <T> T getEntity(Class<T> responseType, String entityId) {
		return restClient.get(responseType,
				Endpoint.ADMIN_ENTITY_BY_ID.get(entityId));
	}

}
