package com.bnbneeds.rest.client.core;

import java.util.Map;

import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionInfoListResponse;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionInfoResponse;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class BuyerBiddingSubscriptions extends BiddingSubscriptions {

	public BuyerBiddingSubscriptions(RestClient restClient, BNBNeedsClient coreClient, String buyerId) {
		super(restClient, coreClient, buyerId);
	}

	@Override
	protected BiddingSubscriptionFor getSubscriptionType() {
		return BiddingSubscriptionFor.buyer;
	}

	@Override
	public BuyerBiddingSubscriptionInfoResponse getSubscription(String subscriptionId) {
		return (BuyerBiddingSubscriptionInfoResponse) super.getSubscription(subscriptionId);
	}

	@Override
	public BuyerBiddingSubscriptionInfoListResponse list() {
		return (BuyerBiddingSubscriptionInfoListResponse) super.list();
	}

	@Override
	public BuyerBiddingSubscriptionInfoListResponse list(int fetchSize) {
		return (BuyerBiddingSubscriptionInfoListResponse) super.list(fetchSize);
	}

	@Override
	public BuyerBiddingSubscriptionInfoListResponse list(int fetchSize, Map<String, String> queryMap) {
		return (BuyerBiddingSubscriptionInfoListResponse) super.list(fetchSize, queryMap);
	}

	@Override
	public BuyerBiddingSubscriptionInfoListResponse list(int fetchSize, Map<String, String> queryMap,
			String nextOffset) {
		return (BuyerBiddingSubscriptionInfoListResponse) super.list(fetchSize, queryMap, nextOffset);
	}

}
