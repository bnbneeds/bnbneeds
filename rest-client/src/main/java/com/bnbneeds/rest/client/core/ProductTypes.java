package com.bnbneeds.rest.client.core;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.CreateProductTypeBulkParam;
import com.bnbneeds.app.model.product.CreateProductTypeParam;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class ProductTypes extends AbstractMasterEntityTypes {

	public ProductTypes(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public CreateResourceResponse create(CreateProductTypeParam param) {
		return create(param, Endpoint.PRODUCT_TYPES.get());
	}

	public TaskResponse create(CreateProductTypeBulkParam param) {
		return restClient.post(TaskResponse.class, param,
				Endpoint.PRODUCT_TYPES_BULK.get());
	}

	@Override
	protected EntityType getEntityType() {
		return EntityType.product_type;
	}

}
