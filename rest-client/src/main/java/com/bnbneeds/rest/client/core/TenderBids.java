package com.bnbneeds.rest.client.core;

import java.util.Map;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.tender.CreateTenderBidParam;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderPreviewListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderPreviewResponse;
import com.bnbneeds.app.model.dealer.tender.TenderProductInfoListResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.ProductNameListResponse;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class TenderBids extends SearchableResource {

	public TenderBids(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public TenderPreviewListResponse listTenderPreviews(int fetchSize, Map<String, String> queryFilterMap,
			String nextOffset) {
		return list(TenderPreviewListResponse.class, Endpoint.TENDERS_PREVIEWS.get(), fetchSize, queryFilterMap,
				nextOffset);
	}

	public TenderPreviewListResponse listTenderPreviews(Map<String, String> queryFilterMap, String nextOffset) {
		return list(TenderPreviewListResponse.class, Endpoint.TENDERS_PREVIEWS.get(), queryFilterMap, nextOffset);
	}

	public TenderPreviewListResponse listTenderPreviews(Map<String, String> queryFilterMap) {
		return list(TenderPreviewListResponse.class, Endpoint.TENDERS_PREVIEWS.get(), queryFilterMap);
	}

	public TenderPreviewListResponse listTenderPreviews(String nextOffset) {
		return list(TenderPreviewListResponse.class, Endpoint.TENDERS_PREVIEWS.get(), nextOffset);
	}

	public TenderPreviewListResponse listTenderPreviews(int fetchSize) {
		return list(TenderPreviewListResponse.class, Endpoint.TENDERS_PREVIEWS.get(), fetchSize);
	}

	public TenderPreviewListResponse listTenderPreviews(int fetchSize, String nextOffset) {
		return list(TenderPreviewListResponse.class, Endpoint.TENDERS_PREVIEWS.get(), fetchSize, nextOffset);
	}

	public ProductNameListResponse previewTenderProducts(String tenderId, Map<String, String> queryFilterMap,
			String nextOffset) {
		return list(ProductNameListResponse.class, Endpoint.TENDER_PREVIEW_PRODUCTS.get(tenderId), queryFilterMap,
				nextOffset);
	}

	public ProductNameListResponse previewTenderProducts(String tenderId, Map<String, String> queryFilterMap) {
		return list(ProductNameListResponse.class, Endpoint.TENDER_PREVIEW_PRODUCTS.get(tenderId), queryFilterMap);
	}

	public ProductNameListResponse previewTenderProducts(String tenderId, String nextOffset) {
		return list(ProductNameListResponse.class, Endpoint.TENDER_PREVIEW_PRODUCTS.get(tenderId), nextOffset);
	}

	public ProductNameListResponse previewTenderProducts(String tenderId, int fetchSize) {
		return list(ProductNameListResponse.class, Endpoint.TENDER_PREVIEW_PRODUCTS.get(tenderId), fetchSize);
	}

	public ProductNameListResponse previewTenderProducts(String tenderId, int fetchSize, String nextOffset) {
		return list(ProductNameListResponse.class, Endpoint.TENDER_PREVIEW_PRODUCTS.get(tenderId), fetchSize,
				nextOffset);
	}

	public String previewTenderProductsAsString(String tenderId, int fetchSize) {
		return list(String.class, Endpoint.TENDER_PREVIEW_PRODUCTS.get(tenderId), fetchSize);
	}

	public String previewTenderProductsAsString(String tenderId, int fetchSize, String nextOffset) {
		return list(String.class, Endpoint.TENDER_PREVIEW_PRODUCTS.get(tenderId), fetchSize, nextOffset);
	}

	public TenderPreviewResponse getTenderPreview(String tenderId) {
		return restClient.get(TenderPreviewResponse.class, Endpoint.TENDER_PREVIEW.get(tenderId));
	}

	public TenderInfoResponse getTenderDetails(String tenderId) {
		return restClient.get(TenderInfoResponse.class, Endpoint.TENDER_INFO.get(tenderId));
	}

	public CreateResourceResponse postBid(String tenderId, CreateTenderBidParam param) {
		return create(param, Endpoint.TENDER_BIDS.get(tenderId));
	}

	public TaskResponse deleteBid(String tenderId, String bidId) {
		return delete(Endpoint.TENDER_BID_INFO.get(tenderId, bidId));
	}

	public TaskResponse accessTender(String tenderId) {
		return create(Endpoint.ACCESS_TENDER.get(tenderId));
	}

	public TenderBidInfoListResponse listTenderBids(String tenderId, int fetchSize, Map<String, String> queryFilterMap,
			String nextOffset) {
		return list(TenderBidInfoListResponse.class, Endpoint.TENDER_BIDS.get(tenderId), fetchSize, queryFilterMap,
				nextOffset);
	}

	public TenderBidInfoListResponse listTenderBids(String tenderId, int fetchSize, String nextOffset) {
		return list(TenderBidInfoListResponse.class, Endpoint.TENDER_BIDS.get(tenderId), fetchSize, nextOffset);
	}

	public TenderBidInfoListResponse listTenderBids(String tenderId, Map<String, String> queryFilterMap,
			String nextOffset) {
		return list(TenderBidInfoListResponse.class, Endpoint.TENDER_BIDS.get(tenderId), queryFilterMap, nextOffset);
	}

	public TenderBidInfoListResponse listTenderBids(String tenderId, Map<String, String> queryFilterMap) {
		return list(TenderBidInfoListResponse.class, Endpoint.TENDER_BIDS.get(tenderId), queryFilterMap);
	}

	public TenderBidInfoListResponse listTenderBids(String tenderId, String nextOffset) {
		return list(TenderBidInfoListResponse.class, Endpoint.TENDER_BIDS.get(tenderId), nextOffset);
	}

	public TenderBidInfoListResponse listTenderBids(String tenderId, int fetchSize) {
		return list(TenderBidInfoListResponse.class, Endpoint.TENDER_BIDS.get(tenderId), fetchSize);
	}

	public TenderProductInfoListResponse listTenderProducts(String tenderId, int fetchSize,
			Map<String, String> queryFilterMap, String nextOffset) {
		return list(TenderProductInfoListResponse.class, Endpoint.TENDER_PRODUCTS.get(tenderId), fetchSize,
				queryFilterMap, nextOffset);
	}

	public TenderProductInfoListResponse listTenderProducts(String tenderId, Map<String, String> queryFilterMap,
			String nextOffset) {
		return list(TenderProductInfoListResponse.class, Endpoint.TENDER_PRODUCTS.get(tenderId), queryFilterMap,
				nextOffset);
	}

	public TenderProductInfoListResponse listTenderProducts(String tenderId, Map<String, String> queryFilterMap) {
		return list(TenderProductInfoListResponse.class, Endpoint.TENDER_PRODUCTS.get(tenderId), queryFilterMap);
	}

	public TenderProductInfoListResponse listTenderProducts(String tenderId, String nextOffset) {
		return list(TenderProductInfoListResponse.class, Endpoint.TENDER_PRODUCTS.get(tenderId), nextOffset);
	}

	public TenderProductInfoListResponse listTenderProducts(String tenderId, int fetchSize) {
		return list(TenderProductInfoListResponse.class, Endpoint.TENDER_PRODUCTS.get(tenderId), fetchSize);
	}

	public String getTenderBidsAsString(String tenderId, int fetchSize, String nextOffset) {
		return list(String.class, Endpoint.TENDER_BIDS.get(tenderId), fetchSize, nextOffset);
	}

	public String getTenderBidsAsString(String tenderId, String nextOffset) {
		return list(String.class, Endpoint.TENDER_BIDS.get(tenderId), nextOffset);
	}

	public String getTenderBidsAsString(String tenderId) {
		return list(String.class, Endpoint.TENDER_BIDS.get(tenderId));
	}

	public String getTenderProductsAsString(String tenderId, int fetchSize, String nextOffset) {
		return list(String.class, Endpoint.TENDER_PRODUCTS.get(tenderId), fetchSize, nextOffset);
	}

	public String getTenderProductsAsString(String tenderId, String nextOffset) {
		return list(String.class, Endpoint.TENDER_PRODUCTS.get(tenderId), nextOffset);
	}

	public String getTenderProductsAsString(String tenderId) {
		return list(String.class, Endpoint.TENDER_PRODUCTS.get(tenderId));
	}

	public TenderBidInfoResponse getLowestBid(String tenderId) {
		return restClient.get(TenderBidInfoResponse.class, Endpoint.TENDER_LOWEST_BID.get(tenderId));
	}

	public TenderBidInfoResponse getCurrentBid(String tenderId) {
		return restClient.get(TenderBidInfoResponse.class, Endpoint.TENDER_BID_CURRENT_BID.get(tenderId));
	}

	public TenderInfoListResponse listTenders(int fetchSize, Map<String, String> queryFilterMap, String nextOffset) {
		return list(TenderInfoListResponse.class, Endpoint.TENDERS.get(), fetchSize, queryFilterMap, nextOffset);
	}

	public TenderInfoListResponse listTenders(int fetchSize, Map<String, String> queryFilterMap) {
		return list(TenderInfoListResponse.class, Endpoint.TENDERS.get(), fetchSize, queryFilterMap);
	}

	public TenderInfoListResponse listTenders(Map<String, String> queryFilterMap, String nextOffset) {
		return list(TenderInfoListResponse.class, Endpoint.TENDERS.get(), queryFilterMap, nextOffset);
	}

	public TenderInfoListResponse listTenders(Map<String, String> queryFilterMap) {
		return list(TenderInfoListResponse.class, Endpoint.TENDERS.get(), queryFilterMap);
	}

	public TenderInfoListResponse listTenders(String nextOffset) {
		return list(TenderInfoListResponse.class, Endpoint.TENDERS.get(), nextOffset);
	}

	public TenderInfoListResponse listTenders(int fetchSize) {
		return list(TenderInfoListResponse.class, Endpoint.TENDERS.get(), fetchSize);
	}

	public TenderInfoListResponse listTenders(int fetchSize, String nextOffset) {
		return list(TenderInfoListResponse.class, Endpoint.TENDERS.get(), fetchSize, nextOffset);
	}

	public TaskResponse updateEntityStatus(String entityId, EntityApprovalParam param) {
		return restClient.put(TaskResponse.class, param, Endpoint.ADMIN_ENTITY_BY_ID_APPROVALS.get(entityId));
	}

}
