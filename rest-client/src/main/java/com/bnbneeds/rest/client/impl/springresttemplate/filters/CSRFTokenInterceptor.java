package com.bnbneeds.rest.client.impl.springresttemplate.filters;

import java.io.IOException;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class CSRFTokenInterceptor implements ClientHttpRequestInterceptor {

	private static final String REQUESTED_BY_HEADER = "X-Requested-By";

	private String requestedBy;

	public CSRFTokenInterceptor(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body,
			ClientHttpRequestExecution execution) throws IOException {
		request.getHeaders().add(REQUESTED_BY_HEADER, requestedBy);

		return execution.execute(request, body);
	}

}
