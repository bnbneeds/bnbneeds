package com.bnbneeds.rest.client.core;

import java.net.URI;
import java.util.Map;

import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.PaymentTransactionInfoListResponse;
import com.bnbneeds.app.model.dealer.PaymentTransactionInfoResponse;
import com.bnbneeds.app.model.dealer.UpdatePaymentTransactionParam;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public abstract class PaymentTransactions extends SearchableResource {

	private String dealerId;

	PaymentTransactions(RestClient restClient, BNBNeedsClient coreClient, String dealerId) {
		super(restClient, coreClient);
		this.dealerId = dealerId;
	}

	public static enum PaymentTransactionOf {
		buyer, vendor;
	}

	protected abstract PaymentTransactionOf getPaymentTransactionOf();

	private URI getListPaymentTransactionListURI() {
		switch (getPaymentTransactionOf()) {
		case buyer:
			return Endpoint.BUYER_PAYMENT_TRANSACTIONS.get(dealerId);
		case vendor:
			return Endpoint.VENDOR_PAYMENT_TRANSACTIONS.get(dealerId);
		default:
			return null;
		}
	}

	private URI getPaymentTransactionInfoURI(String paymentTransactionId) {
		switch (getPaymentTransactionOf()) {
		case buyer:
			return Endpoint.BUYER_PAYMENT_TRANSACTION_INFO.get(dealerId, paymentTransactionId);
		case vendor:
			return Endpoint.VENDOR_PAYMENT_TRANSACTION_INFO.get(dealerId, paymentTransactionId);
		default:
			return null;
		}
	}

	public PaymentTransactionInfoResponse get(String paymentTransactionId) {
		return restClient.get(PaymentTransactionInfoResponse.class, getPaymentTransactionInfoURI(paymentTransactionId));
	}

	public TaskResponse update(String paymentTransactionId, UpdatePaymentTransactionParam param) {
		return update(param, getPaymentTransactionInfoURI(paymentTransactionId));
	}

	public PaymentTransactionInfoListResponse list() {
		return list(PaymentTransactionInfoListResponse.class, getListPaymentTransactionListURI());
	}

	public PaymentTransactionInfoListResponse list(int fetchSize, Map<String, String> queryFilterMap,
			String nextOffset) {
		return list(PaymentTransactionInfoListResponse.class, getListPaymentTransactionListURI(), fetchSize,
				queryFilterMap, nextOffset);
	}

	public PaymentTransactionInfoListResponse list(int fetchSize, String nextOffset) {
		return list(PaymentTransactionInfoListResponse.class, getListPaymentTransactionListURI(), fetchSize,
				nextOffset);
	}

	public PaymentTransactionInfoListResponse list(int fetchSize, Map<String, String> queryFilterMap) {
		return list(PaymentTransactionInfoListResponse.class, getListPaymentTransactionListURI(), fetchSize,
				queryFilterMap);
	}

	public PaymentTransactionInfoListResponse list(Map<String, String> queryFilterMap) {
		return list(PaymentTransactionInfoListResponse.class, getListPaymentTransactionListURI(), queryFilterMap);
	}

	public PaymentTransactionInfoListResponse list(String nextOffset) {
		return list(PaymentTransactionInfoListResponse.class, getListPaymentTransactionListURI(), nextOffset);
	}

	public PaymentTransactionInfoListResponse list(int fetchSize) {
		return list(PaymentTransactionInfoListResponse.class, getListPaymentTransactionListURI(), fetchSize);
	}

}
