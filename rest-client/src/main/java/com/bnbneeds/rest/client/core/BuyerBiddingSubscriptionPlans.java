package com.bnbneeds.rest.client.core;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.subscription.CreateBuyerSubscriptionPlanParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateBuyerSubscriptionPlanParam;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class BuyerBiddingSubscriptionPlans extends BiddingSubscriptionPlans {
	
	public BuyerBiddingSubscriptionPlans(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	@Override
	protected BiddingSubscriptionPlanFor getSubscriptionType() {
		return BiddingSubscriptionPlanFor.buyer;
	}
	
	public CreateResourceResponse create(CreateBuyerSubscriptionPlanParam param) {
		return create(param, getBiddingSubscriptionPlansURI());
	}

	public TaskResponse update(UpdateBuyerSubscriptionPlanParam param, String planId) {
		return update(param, getBiddingSubscriptionPlanInfoURI(planId));
	}
}
