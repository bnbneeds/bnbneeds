package com.bnbneeds.rest.client.exceptions;

import com.bnbneeds.app.model.TaskResponse;

public class BNBNeedsServiceException extends RuntimeException {

	private static final long serialVersionUID = -242549080887793797L;

	private int httpCode;
	private TaskResponse task;

	public BNBNeedsServiceException(int httpCode) {
		super();
		this.httpCode = httpCode;
	}

	public BNBNeedsServiceException(int httpCode, TaskResponse task) {
		super(task.toString());
		this.httpCode = httpCode;
		this.task = task;
	}

	public BNBNeedsServiceException(int httpCode, String message) {
		super(message);
		this.httpCode = httpCode;
	}

	public int getHttpCode() {
		return httpCode;
	}

	public TaskResponse getTask() {
		return task;
	}

	@Override
	public String getMessage() {
		if (task != null) {
			return task.getMessage();
		}
		return super.getMessage();
	}

}
