package com.bnbneeds.rest.client.impl.apachecxf.filters;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

public class CSRFRequestFilter implements ClientRequestFilter {

	private static final String REQUESTED_BY_HEADER = "X-Requested-By";
	private String requestedBy;

	public CSRFRequestFilter(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		requestContext.getHeaders().add(REQUESTED_BY_HEADER, requestedBy);
	}

}
