package com.bnbneeds.rest.client.core;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.subscription.CreateVendorSubscriptionPlanParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateVendorSubscriptionPlanParam;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class VendorBiddingSubscriptionPlans extends BiddingSubscriptionPlans {

	public VendorBiddingSubscriptionPlans(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	@Override
	protected BiddingSubscriptionPlanFor getSubscriptionType() {
		return BiddingSubscriptionPlanFor.vendor;
	}
	
	public CreateResourceResponse create(CreateVendorSubscriptionPlanParam param) {
		return create(param, getBiddingSubscriptionPlansURI());
	}

	public TaskResponse update(UpdateVendorSubscriptionPlanParam param, String planId) {
		return update(param, getBiddingSubscriptionPlanInfoURI(planId));
	}
}
