package com.bnbneeds.rest.client;

import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.ChangePasswordParam;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;

public class AuthClient {
	protected RestClient client;

	public AuthClient(RestClient client) {
		this.client = client;
	}

	/**
	 * Convenience method for calling constructor with new
	 * ClientConfig().withHost(host)
	 * 
	 * @param host
	 *            Hostname or IP address for the Virtual IP of the target
	 *            environment.
	 */
	public AuthClient(String host) {
		this(new ClientConfig().withHost(host));
	}

	/**
	 * Convenience method for calling constructor with new
	 * ClientConfig().withHost
	 * (host).withIgnoringCertificates(ignoreCertificates)
	 * 
	 * @param host
	 *            Hostname or IP address for the Virtual IP of the target
	 *            environment.
	 * @param ignoreCertificates
	 *            True if SSL certificates should be ignored.
	 */
	public AuthClient(String host, boolean ignoreCertificates) {
		this(new ClientConfig().withHost(host).withIgnoringCertificates(
				ignoreCertificates));
	}

	public AuthClient(ClientConfig config) {
		this(config.newClient());
	}

	public RestClient getClient() {
		return this.client;
	}

	/**
	 * Performs a login operation. The token is automatically associated with
	 * this client connection.
	 * 
	 * @param username
	 *            The username.
	 * @param password
	 *            The password.
	 * @return The authentication token.
	 */

	public UserAccountInfoResponse login(String username, String password) {
		return client.login(UserAccountInfoResponse.class, username, password,
				Endpoint.USER_ACCOUNT_LOGIN.get());
	}

	public UserAccountInfoResponse login(String username, String password,
			String clientIPAddress) {
		return client.login(UserAccountInfoResponse.class, username, password,
				clientIPAddress, Endpoint.USER_ACCOUNT_LOGIN.get());
	}

	public void logout() {
		client.post(String.class, null, Endpoint.USER_ACCOUNT_LOGOUT.get());
		client.setAuthToken(null);
	}

	public boolean isLoggedIn() {
		return (client.getAuthToken() != null)
				&& !("".equals(client.getAuthToken()));
	}

	public void changePassword(String currentPassword, String newPassword,
			String confirmNewPassword) {

		ChangePasswordParam request = new ChangePasswordParam();
		request.setCurrentPassword(currentPassword);
		request.setNewPassword(newPassword);
		request.setConfirmPassword(confirmNewPassword);
		changePassword(request);
	}

	public void changePassword(ChangePasswordParam request) {

		client.put(TaskResponse.class, request,
				Endpoint.USER_ACCOUNT_PASSWORDS.get());
	}

	public void resetPassword(String username) {
		client.post(TaskResponse.class, null,
				Endpoint.USER_ACCOUNT_PASSWORD_RESET.get(username));
	}
}
