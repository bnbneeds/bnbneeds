package com.bnbneeds.rest.client.core;

import java.net.URI;
import java.util.Map;

import com.bnbneeds.app.model.DataObjectInfoListResponse;
import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionPlanInfoListResponse;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionPlanInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.SubscriptionPlanInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionPlanAvailabilityStatus;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionPlanDefaultStatus;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionPlanInfoListResponse;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionPlanInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public abstract class BiddingSubscriptionPlans extends SearchableResource {

	public static enum BiddingSubscriptionPlanFor {
		buyer, vendor;
	}

	public BiddingSubscriptionPlans(RestClient restClient, BNBNeedsClient parent) {
		super(restClient, parent);
	}

	public BiddingSubscriptionPlans(RestClient client, BNBNeedsClient bnbNeedsClient, String vendorId) {
		this(client, bnbNeedsClient);
	}

	protected abstract BiddingSubscriptionPlanFor getSubscriptionType();

	protected URI getBiddingSubscriptionPlansURI() {
		switch (getSubscriptionType()) {
		case buyer:
			return Endpoint.BUYER_BIDDING_SUBSCRIPTION_PLANS.get();
		case vendor:
			return Endpoint.VENDOR_BIDDING_SUBSCRIPTION_PLANS.get();
		default:
			return null;
		}
	}

	protected URI getBiddingSubscriptionPlanInfoURI(String subscriptionId) {
		switch (getSubscriptionType()) {
		case buyer:
			return Endpoint.BUYER_BIDDING_SUBSCRIPTION_PLAN_INFO.get(subscriptionId);
		case vendor:
			return Endpoint.VENDOR_BIDDING_SUBSCRIPTION_PLAN_INFO.get(subscriptionId);
		default:
			return null;
		}
	}

	protected URI getBiddingSubscriptionPlanUpdateDefaultStatusURI(String planId) {
		switch (getSubscriptionType()) {
		case buyer:
			return Endpoint.BUYER_BIDDING_SUBSCRIPTION_PLAN_UPDATE_DEFAULT_STATUS.get(planId);
		case vendor:
			return Endpoint.VENDOR_BIDDING_SUBSCRIPTION_PLAN_UPDATE_DEFAULT_STATUS.get(planId);
		default:
			return null;
		}
	}

	protected URI getBiddingSubscriptionPlanUpdateAvailabilityStatusURI(String planId) {
		switch (getSubscriptionType()) {
		case buyer:
			return Endpoint.BUYER_BIDDING_SUBSCRIPTION_PLAN_UPDATE_AVAILABILITY_STATUS.get(planId);
		case vendor:
			return Endpoint.VENDOR_BIDDING_SUBSCRIPTION_PLAN_UPDATE_AVAILABILITY_STATUS.get(planId);
		default:
			return null;
		}
	}

	private Class<? extends SubscriptionPlanInfoResponse> getBiddingSubscriptionPlanInfoResponseClass() {
		switch (getSubscriptionType()) {
		case buyer:
			return BuyerBiddingSubscriptionPlanInfoResponse.class;
		case vendor:
			return VendorBiddingSubscriptionPlanInfoResponse.class;
		default:
			return null;
		}
	}

	private Class<? extends DataObjectInfoListResponse<? extends SubscriptionPlanInfoResponse>> getBiddingSubscriptionPlanInfoListResponseClass() {
		switch (getSubscriptionType()) {
		case buyer:
			return BuyerBiddingSubscriptionPlanInfoListResponse.class;
		case vendor:
			return VendorBiddingSubscriptionPlanInfoListResponse.class;
		default:
			return null;
		}
	}

	public TaskResponse delete(String planId) {
		return delete(getBiddingSubscriptionPlanInfoURI(planId));
	}

	public SubscriptionPlanInfoResponse get(String planId) {
		return restClient.get(getBiddingSubscriptionPlanInfoResponseClass(), getBiddingSubscriptionPlanInfoURI(planId));
	}

	public DataObjectInfoListResponse<? extends SubscriptionPlanInfoResponse> list() {
		return list(getBiddingSubscriptionPlanInfoListResponseClass(), getBiddingSubscriptionPlansURI());
	}

	public DataObjectInfoListResponse<? extends SubscriptionPlanInfoResponse> list(int fetchSize,
			Map<String, String> queryFilterMap, String nextOffset) {
		return list(getBiddingSubscriptionPlanInfoListResponseClass(), getBiddingSubscriptionPlansURI(), fetchSize,
				queryFilterMap, nextOffset);
	}

	public DataObjectInfoListResponse<? extends SubscriptionPlanInfoResponse> list(int fetchSize,
			Map<String, String> queryFilterMap) {
		return list(getBiddingSubscriptionPlanInfoListResponseClass(), getBiddingSubscriptionPlansURI(), fetchSize,
				queryFilterMap);
	}

	public DataObjectInfoListResponse<? extends SubscriptionPlanInfoResponse> list(int fetchSize, String nextOffset) {
		return list(getBiddingSubscriptionPlanInfoListResponseClass(), getBiddingSubscriptionPlansURI(), fetchSize,
				nextOffset);
	}

	// public VendorListResponse list(Map<String, String> queryFilterMap,
	// String nextOffset) {
	// return list(VendorListResponse.class,
	// getBiddingSubscriptionPlansURI(), queryFilterMap, nextOffset);
	// }

	public DataObjectInfoListResponse<? extends SubscriptionPlanInfoResponse> list(Map<String, String> queryFilterMap) {
		return list(getBiddingSubscriptionPlanInfoListResponseClass(), getBiddingSubscriptionPlansURI(),
				queryFilterMap);
	}

	public DataObjectInfoListResponse<? extends SubscriptionPlanInfoResponse> list(String nextOffset) {
		return list(getBiddingSubscriptionPlanInfoListResponseClass(), getBiddingSubscriptionPlansURI(), nextOffset);
	}

	public DataObjectInfoListResponse<? extends SubscriptionPlanInfoResponse> list(int fetchSize) {
		return list(getBiddingSubscriptionPlanInfoListResponseClass(), getBiddingSubscriptionPlansURI(), fetchSize);
	}

	public TaskResponse updateEntityStatus(String entityId, EntityApprovalParam param) {
		return restClient.put(TaskResponse.class, param, Endpoint.ADMIN_ENTITY_BY_ID_APPROVALS.get(entityId));
	}

	public TaskResponse updateDefaultStatus(String planId, UpdateSubscriptionPlanDefaultStatus param) {
		return restClient.put(TaskResponse.class, param, getBiddingSubscriptionPlanUpdateDefaultStatusURI(planId));
	}

	public TaskResponse updateAvailabilityStatus(String planId, UpdateSubscriptionPlanAvailabilityStatus param) {
		return restClient.put(TaskResponse.class, param, getBiddingSubscriptionPlanUpdateAvailabilityStatusURI(planId));
	}
}
