package com.bnbneeds.rest.client.core;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.CreateProductCategoryBulkParam;
import com.bnbneeds.app.model.product.CreateProductCategoryParam;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class ProductCategories extends AbstractMasterEntityTypes {

	public ProductCategories(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public CreateResourceResponse create(CreateProductCategoryParam param) {
		return create(param, Endpoint.PRODUCT_CATEGORIES.get());
	}

	public TaskResponse create(CreateProductCategoryBulkParam param) {
		return restClient.post(TaskResponse.class, param,
				Endpoint.PRODUCT_CATEGORIES_BULK.get());
	}

	@Override
	protected EntityType getEntityType() {
		return EntityType.product_category;
	}

}
