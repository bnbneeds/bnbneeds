package com.bnbneeds.rest.client;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.rest.client.model.request.FileAttachment;
import com.bnbneeds.rest.client.model.request.FormElement;

public interface RestClient {

	public ClientConfig getConfig();

	// public <T extends Object> T getClient();

	public String getAuthToken();

	public void setAuthToken(String token);

	public <T> T get(Class<T> responseType, String path);

	public <T> T get(Class<T> responseType, String path, Object... args);

	public <T> T get(Class<T> responseType, String path, Properties queryParams);

	public <T> T get(Class<T> responseType, String path,
			Properties queryParams, Object... args);

	public <T> T get(Class<T> responseType, URI uri);

	public <T> T get(Class<T> responseType, URI uri, Object... args);

	public <T> T get(GenericType<T> responseType, URI uri);

	public <T> T get(GenericType<T> responseType, URI uri, Object... args);

	public <T> T get(Class<T> responseType, URI path, Properties queryParams);

	public <T> T put(Class<T> responseType, Object request, String path);

	public <T> T get(GenericType<T> responseType, String path);

	public <T> T get(GenericType<T> responseType, String path, Object... args);

	public <T> T put(Class<T> responseType, Object request, String path,
			Object... args);

	public <T> T put(Class<T> responseType, Object request, URI uri);

	public <T> T put(Class<T> responseType, Object request, URI uri,
			Properties queryParams);

	public <T> T put(Class<T> responseType, Object request, String path,
			Properties queryParams);

	public <T> T put(Class<T> responseType, Object request, URI uri,
			Object... args);

	public <T> T post(Class<T> responseType, Object request, String path);

	public <T> T post(Class<T> responseType, Object request, String path,
			Properties queryParams);

	public <T> T post(Class<T> responseType, Object request, URI uri,
			Properties queryParams);

	public <T> T post(Class<T> responseType, Object request, String path,
			Object... args);

	public <T> T post(GenericType<T> responseType, Object request, String path,
			Object... args);

	public <T> T post(GenericType<T> responseType, Object request, String path);

	public <T> T postMultiPart(Class<T> responseType, Object request,
			String path);

	public <T> T postMultiPart(Class<T> responseType, Object request,
			String path, Object... args);

	public CreateResourceResponse post(Object request, String path);

	public CreateResourceResponse post(Object request, String path,
			Object... args);

	public <T> T post(Class<T> responseType, Object request, URI uri);

	public <T> T post(Class<T> responseType, Object request, URI uri,
			Object... args);

	public TaskResponse delete(String path);

	public TaskResponse delete(String path, Object... args);

	public TaskResponse delete(URI uri);

	public TaskResponse delete(URI uri, Object... args);

	public TaskResponse delete(URI uri, Properties queryParams);

	public TaskResponse delete(String uri, Properties queryParams);

	public <T> T login(Class<T> responseType, String username, String password,
			URI path);

	public <T> T login(Class<T> responseType, String username, String password,
			String path);

	public <T> T login(Class<T> responseType, String username, String password,
			String clientIPAddress, String path);

	public <T> T login(Class<T> responseType, String username, String password,
			String clientIPAddress, URI path);

	public <T> T postMultiPart(Class<T> responseType, String filename,
			InputStream request, String path);

	public <T> T postMultiPart(Class<T> responseType, String filename,
			InputStream request, String path, Object... args);

	public <T> T postMultiPart(Class<T> class1, String filename,
			String contentType, long fileSize, InputStream image,
			String imageUploadPath);

	public <T> T postMultiPart(Class<T> class1, String filename,
			String contentType, long fileSize, InputStream image,
			String imageUploadPath, Object... args);

	public <T> T postMultiPart(Class<T> responseType, File file, String path);

	public Response get(String path, String mediaType, Properties queryParams);

	public Response get(String path, String mediaType);

	public Response get(URI path, String mediaType, Properties queryParams);

	public Response get(URI path, String mediaType);

	byte[] getImage(String path, String mediaType, Properties queryParams);

	byte[] getImage(String path, String mediaType);

	byte[] getImage(URI path, String mediaType, Properties queryParams);

	byte[] getImage(URI path, String mediaType);

	public <T> T postMultiPart(Class<T> responseType,
			FileAttachment fileAttachment, String path);

	public <T> T postMutiPart(Class<T> responseType,
			List<FormElement> formDataList,
			List<FileAttachment> fileAttachments, URI path);

	public <T> T postMutiPart(Class<T> responseType,
			List<FormElement> formDataList,
			List<FileAttachment> fileAttachments, String path);
}
