package com.bnbneeds.rest.client.core;

import java.util.Properties;

import com.bnbneeds.app.model.EntityCountResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class Utilities extends SearchableResource {

	public Utilities(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public EntityCountResponse getEntityCount(String entityType) {
		Properties queryParams = new Properties();
		queryParams.put("entityType", entityType);
		return restClient.get(EntityCountResponse.class, Endpoint.UTILITIES_ENTITY_COUNT.get(), queryParams);
	}

	public String getEntityCountAsString(String entityType) {
		Properties queryParams = new Properties();
		queryParams.put("entityType", entityType);
		return restClient.get(String.class, Endpoint.UTILITIES_ENTITY_COUNT.get(), queryParams);
	}
}
