package com.bnbneeds.rest.client.core;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.CreateProductNameBulkParam;
import com.bnbneeds.app.model.product.CreateProductNameParam;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class ProductNames extends AbstractMasterEntityTypes {

	public ProductNames(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public CreateResourceResponse create(CreateProductNameParam param) {
		return create(param, Endpoint.PRODUCT_NAMES.get());
	}

	public TaskResponse create(CreateProductNameBulkParam param) {
		return restClient.post(TaskResponse.class, param,
				Endpoint.PRODUCT_NAMES_BULK.get());
	}

	@Override
	protected EntityType getEntityType() {
		return EntityType.product_name;
	}

}
