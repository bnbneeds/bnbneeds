package com.bnbneeds.rest.client.core;

import java.net.URI;
import java.util.Map;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.DataObjectInfoListResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.CreatePaymentTransactionParam;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionInfoListResponse;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.CreateSubscriptionParam;
import com.bnbneeds.app.model.dealer.subscription.DealerBiddingSubscriptionInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionStatusParam;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionInfoListResponse;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public abstract class BiddingSubscriptions extends SearchableResource {

	protected String dealerId;

	BiddingSubscriptions(RestClient restClient, BNBNeedsClient coreClient, String dealerId) {
		super(restClient, coreClient);
		this.dealerId = dealerId;
	}

	public static enum BiddingSubscriptionFor {
		buyer, vendor;
	}

	protected abstract BiddingSubscriptionFor getSubscriptionType();

	private URI getBiddingSubscriptionsURI() {
		switch (getSubscriptionType()) {
		case buyer:
			return Endpoint.BUYER_BIDDING_SUBSCRIPTIONS.get(dealerId);
		case vendor:
			return Endpoint.VENDOR_BIDDING_SUBSCRIPTIONS.get(dealerId);
		default:
			return null;
		}
	}

	private URI getBiddingSubscriptionInfoURI(String subscriptionId) {
		switch (getSubscriptionType()) {
		case buyer:
			return Endpoint.BUYER_BIDDING_SUBSCRIPTION_INFO.get(dealerId, subscriptionId);
		case vendor:
			return Endpoint.VENDOR_BIDDING_SUBSCRIPTION_INFO.get(dealerId, subscriptionId);
		default:
			return null;
		}
	}

	private URI getBiddingSubscriptionUpdateStatusURI(String subscriptionId) {
		switch (getSubscriptionType()) {
		case buyer:
			return Endpoint.BUYER_BIDDING_SUBSCRIPTION_UPDATE_STATUS.get(dealerId, subscriptionId);
		case vendor:
			return Endpoint.VENDOR_BIDDING_SUBSCRIPTION_UPDATE_STATUS.get(dealerId, subscriptionId);
		default:
			return null;
		}
	}

	private URI getBiddingSubscriptionCreatePaymentTransactionURI(String subscriptionId) {
		switch (getSubscriptionType()) {
		case buyer:
			return Endpoint.BUYER_BIDDING_SUBSCRIPTION_PAYMENT_TRANSACTIONS.get(dealerId, subscriptionId);
		case vendor:
			return Endpoint.VENDOR_BIDDING_SUBSCRIPTION_PAYMENT_TRANSACTIONS.get(dealerId, subscriptionId);
		default:
			return null;
		}
	}

	private Class<? extends DealerBiddingSubscriptionInfoResponse> getBiddingSubscriptionInfoResponseClass() {
		switch (getSubscriptionType()) {
		case buyer:
			return BuyerBiddingSubscriptionInfoResponse.class;
		case vendor:
			return VendorBiddingSubscriptionInfoResponse.class;
		default:
			return null;
		}
	}

	private Class<? extends DataObjectInfoListResponse<? extends DealerBiddingSubscriptionInfoResponse>> getBiddingSubscriptionInfoListResponseClass() {
		switch (getSubscriptionType()) {
		case buyer:
			return BuyerBiddingSubscriptionInfoListResponse.class;
		case vendor:
			return VendorBiddingSubscriptionInfoListResponse.class;
		default:
			return null;
		}
	}

	private URI getBiddingSubscriptionDefaultPlanURI() {
		switch (getSubscriptionType()) {
		case buyer:
			return Endpoint.BUYER_BIDDING_SUBSCRIPTION_DEFAULT_PLAN.get(dealerId);
		case vendor:
			return Endpoint.VENDOR_BIDDING_SUBSCRIPTION_DEFAULT_PLAN.get(dealerId);
		default:
			return null;
		}
	}

	public CreateResourceResponse createSubscription(CreateSubscriptionParam param) {
		return create(param, getBiddingSubscriptionsURI());
	}

	public TaskResponse updateSubscription(String subscriptionId, UpdateSubscriptionParam param) {
		return update(param, getBiddingSubscriptionInfoURI(subscriptionId));
	}

	public TaskResponse deleteSubscription(String subscriptionId) {
		return delete(getBiddingSubscriptionInfoURI(subscriptionId));
	}

	public DealerBiddingSubscriptionInfoResponse getSubscription(String subscriptionId) {
		return restClient.get(getBiddingSubscriptionInfoResponseClass(), getBiddingSubscriptionInfoURI(subscriptionId));
	}

	public DataObjectInfoListResponse<? extends DealerBiddingSubscriptionInfoResponse> list() {
		return list(getBiddingSubscriptionInfoListResponseClass(), getBiddingSubscriptionsURI());
	}

	public DataObjectInfoListResponse<? extends DealerBiddingSubscriptionInfoResponse> list(int fetchSize) {
		return list(getBiddingSubscriptionInfoListResponseClass(), getBiddingSubscriptionsURI(), fetchSize);
	}

	public DataObjectInfoListResponse<? extends DealerBiddingSubscriptionInfoResponse> list(int fetchSize,
			Map<String, String> queryMap) {
		return list(getBiddingSubscriptionInfoListResponseClass(), getBiddingSubscriptionsURI(), fetchSize, queryMap);
	}

	public DataObjectInfoListResponse<? extends DealerBiddingSubscriptionInfoResponse> list(int fetchSize,
			Map<String, String> queryMap, String nextOffset) {
		return list(getBiddingSubscriptionInfoListResponseClass(), getBiddingSubscriptionsURI(), fetchSize, queryMap,
				nextOffset);
	}

	public DataObjectInfoListResponse<? extends DealerBiddingSubscriptionInfoResponse> list(int fetchSize,
			String nextOffset) {
		return list(getBiddingSubscriptionInfoListResponseClass(), getBiddingSubscriptionsURI(), fetchSize, nextOffset);
	}

	public TaskResponse updateStatus(String subscriptionId, UpdateSubscriptionStatusParam param) {
		return update(param, getBiddingSubscriptionUpdateStatusURI(subscriptionId));
	}

	public CreateResourceResponse createPaymentTransaction(String subscriptionId, CreatePaymentTransactionParam param) {
		return create(param, getBiddingSubscriptionCreatePaymentTransactionURI(subscriptionId));
	}

	public CreateResourceResponse createSubscriptionWithDefaultPlan() {
		return create(getBiddingSubscriptionDefaultPlanURI());
	}

}
