package com.bnbneeds.rest.client.core;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.CreateBusinessTypeBulkParam;
import com.bnbneeds.app.model.dealer.CreateBusinessTypeParam;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class BusinessTypes extends AbstractMasterEntityTypes {

	public BusinessTypes(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public CreateResourceResponse create(CreateBusinessTypeParam param) {
		return create(param, Endpoint.BUSINESS_TYPES.get());
	}

	public TaskResponse create(CreateBusinessTypeBulkParam param) {
		return restClient.post(TaskResponse.class, param,
				Endpoint.BUSINESS_TYPES_BULK.get());
	}

	@Override
	protected EntityType getEntityType() {
		return EntityType.business_type;
	}

}
