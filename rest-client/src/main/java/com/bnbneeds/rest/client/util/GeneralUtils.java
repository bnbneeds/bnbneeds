package com.bnbneeds.rest.client.util;

import java.util.Collection;

import com.bnbneeds.rest.client.exceptions.BNBNeedsClientException;
import com.bnbneeds.rest.client.model.request.FileAttachment;

public class GeneralUtils {

	public static boolean isEmpty(String text) {
		return (text == null || text.trim().isEmpty());
	}

	public static boolean isEmpty(Collection<?> collection) {
		return (collection == null || collection.isEmpty());
	}

	public static void assertNotEmpty(String fieldValue, String message) {
		if (isEmpty(fieldValue)) {
			throw new BNBNeedsClientException(message);
		}
	}

	public static void assertNotEmpty(Collection<?> collection, String message) {
		if (isEmpty(collection)) {
			throw new BNBNeedsClientException(message);
		}
	}

	public static void assertNotNull(Object object, String message) {
		if (object == null) {
			throw new BNBNeedsClientException(message);
		}
	}

	public static boolean isImage(FileAttachment attachment) {
		if (attachment != null && attachment.getMediaType() != null) {
			return attachment.getMediaType().startsWith("image/");
		}
		return false;
	}

}
