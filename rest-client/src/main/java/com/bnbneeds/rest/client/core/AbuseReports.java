package com.bnbneeds.rest.client.core;

import java.util.Map;

import com.bnbneeds.app.model.AddRemarksParam;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.VendorListResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.report.AbuseReportInfoListResponse;
import com.bnbneeds.app.model.report.AbuseReportInfoResponse;
import com.bnbneeds.app.model.report.AbuseReportListResponse;
import com.bnbneeds.app.model.report.CloseReportParam;
import com.bnbneeds.app.model.report.CreateReportParam;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class AbuseReports extends SearchableResource {

	public AbuseReports(RestClient restClient, BNBNeedsClient parent) {
		super(restClient, parent);
	}

	public AbuseReports(RestClient client, BNBNeedsClient bnbNeedsClient,
			String vendorId) {
		this(client, bnbNeedsClient);
	}

	public CreateResourceResponse create(CreateReportParam param) {
		return create(param, Endpoint.ABUSE_REPORTS.get());
	}

	public TaskResponse delete(String abuseId) {
		return delete(Endpoint.ABUSE_REPORT_INFO.get(abuseId));
	}

	public AbuseReportInfoResponse get(String abuseId) {
		return restClient.get(AbuseReportInfoResponse.class,
				Endpoint.ABUSE_REPORT_INFO.get(abuseId));
	}

	public VendorListResponse list() {
		return list(VendorListResponse.class, Endpoint.ABUSE_REPORTS.get());
	}

	public AbuseReportListResponse list(int fetchSize,
			Map<String, String> queryFilterMap, String nextOffset) {
		return list(AbuseReportListResponse.class,
				Endpoint.ABUSE_REPORTS.get(), fetchSize, queryFilterMap,
				nextOffset);
	}

	public AbuseReportListResponse list(Map<String, String> queryFilterMap,
			String nextOffset) {
		return list(AbuseReportListResponse.class,
				Endpoint.ABUSE_REPORTS.get(), queryFilterMap, nextOffset);
	}

	public AbuseReportListResponse list(Map<String, String> queryFilterMap) {
		return list(AbuseReportListResponse.class,
				Endpoint.ABUSE_REPORTS.get(), queryFilterMap);
	}

	public AbuseReportListResponse list(String nextOffset) {
		return list(AbuseReportListResponse.class,
				Endpoint.ABUSE_REPORTS.get(), nextOffset);
	}

	public AbuseReportListResponse list(int fetchSize) {
		return list(AbuseReportListResponse.class,
				Endpoint.ABUSE_REPORTS.get(), fetchSize);
	}
	
	public AbuseReportInfoListResponse listBulk(int fetchSize,
			Map<String, String> queryFilterMap, String nextOffset) {
		return list(AbuseReportInfoListResponse.class,
				Endpoint.ABUSE_REPORTS_BULK.get(), fetchSize, queryFilterMap,
				nextOffset);
	}

	public AbuseReportInfoListResponse listBulk(Map<String, String> queryFilterMap,
			String nextOffset) {
		return list(AbuseReportInfoListResponse.class,
				Endpoint.ABUSE_REPORTS_BULK.get(), queryFilterMap, nextOffset);
	}

	public AbuseReportInfoListResponse listBulk(Map<String, String> queryFilterMap) {
		return list(AbuseReportInfoListResponse.class,
				Endpoint.ABUSE_REPORTS_BULK.get(), queryFilterMap);
	}

	public AbuseReportInfoListResponse listBulk(String nextOffset) {
		return list(AbuseReportInfoListResponse.class,
				Endpoint.ABUSE_REPORTS_BULK.get(), nextOffset);
	}

	public AbuseReportInfoListResponse listBulk(int fetchSize) {
		return list(AbuseReportInfoListResponse.class,
				Endpoint.ABUSE_REPORTS_BULK.get(), fetchSize);
	}

	public TaskResponse close(String accountId, CloseReportParam param) {
		return restClient.put(TaskResponse.class,param,
				Endpoint.ABUSE_REPORT_CLOSE.get(accountId));
	}
	
	public TaskResponse assignToSelf(String accountId, AddRemarksParam param) {
		return restClient.put(TaskResponse.class,param,
				Endpoint.ABUSE_REPORT_ASSIGN.get(accountId));
	}
	
	public TaskResponse addRemarks(String entityId,AddRemarksParam param) {
		return restClient.put(TaskResponse.class, param,
				Endpoint.ABUSE_REPORT_ADDREMARKS.get(entityId));		
	}
	
	

}
