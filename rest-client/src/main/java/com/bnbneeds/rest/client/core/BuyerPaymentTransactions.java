package com.bnbneeds.rest.client.core;

import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class BuyerPaymentTransactions extends PaymentTransactions {

	public BuyerPaymentTransactions(RestClient restClient, BNBNeedsClient coreClient, String dealerId) {
		super(restClient, coreClient, dealerId);
	}

	@Override
	protected PaymentTransactionOf getPaymentTransactionOf() {
		return PaymentTransactionOf.buyer;
	}

}
