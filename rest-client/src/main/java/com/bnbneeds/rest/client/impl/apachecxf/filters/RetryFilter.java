package com.bnbneeds.rest.client.impl.apachecxf.filters;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.rest.client.exceptions.BNBNeedsClientException;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.bnbneeds.rest.client.util.ClientUtils;

public class RetryFilter implements ClientResponseFilter {
	private final Logger log = LoggerFactory.getLogger(getClass());
	private int maxRetries;
	private int retryInterval;

	public RetryFilter(int maxRetries, int retryInterval) {
		this.maxRetries = maxRetries;
		this.retryInterval = retryInterval;
	}

	@Override
	public void filter(ClientRequestContext requestContext,
			ClientResponseContext responseContext) throws IOException {
		Throwable cause = null;
		for (int retryCount = 1; retryCount <= maxRetries; retryCount++) {
			try {

				responseContext = ClientUtils.makeRequest(requestContext,
						responseContext);
				// return response;
			} catch (BNBNeedsServiceException e) {
				cause = e;
			}
			log.info("Request failed {}, retrying (count: {})", requestContext
					.getUri().toString(), retryCount);
			try {
				Thread.sleep(retryInterval);
			} catch (InterruptedException exception) {
				// Ignore this
			}
		}
		throw new BNBNeedsClientException("Retry limit exceeded");
	}
}