package com.bnbneeds.rest.client.impl.springresttemplate.filters;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatus.Series;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;

import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.rest.client.exceptions.BNBNeedsClientException;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.bnbneeds.rest.client.util.ClientUtils;

public class BNBNeedsReponseErrorHandler implements ResponseErrorHandler {

	private DefaultResponseErrorHandler defaultHandler;
	private String acceptedMediaType;

	public BNBNeedsReponseErrorHandler(String acceptedMediaType) {
		defaultHandler = new DefaultResponseErrorHandler();
		this.acceptedMediaType = acceptedMediaType;
	}

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		return defaultHandler.hasError(response);
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		TaskResponse task = null;
		try {
			task = ClientUtils.parseObjectToPOJO(response.getBody(),
					TaskResponse.class, acceptedMediaType);
			HttpStatus status = response.getStatusCode();

			BNBNeedsServiceException e = null;
			if (task != null) {
				e = new BNBNeedsServiceException(response.getRawStatusCode(),
						task);
			} else {
				e = new BNBNeedsServiceException(response.getRawStatusCode(),
						"Request failed. Response message: "
								+ status.getReasonPhrase());
			}
			Series series = status.series();
			switch (series) {
			case CLIENT_ERROR:
			case SERVER_ERROR:
				throw e;
			default:
				break;

			}
		} catch (IOException e) {
			throw new BNBNeedsClientException("Error parsing response.", e);
		}

		try {
			defaultHandler.handleError(response);
		} catch (RestClientException e) {
			throw new BNBNeedsClientException(e);
		}
	}
}
