package com.bnbneeds.rest.client.core;

import java.net.URI;
import java.util.Map;
import java.util.Properties;

import org.apache.cxf.common.util.StringUtils;

import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

class SearchableResource extends Resource {

	SearchableResource(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	private static Properties constructQueryParams(int fetchSize,
			Map<String, String> filterMap, String nextOffset) {
		Properties p = new Properties();
		if (fetchSize > 0) {
			p.put("fetchSize", String.valueOf(fetchSize));
		}

		if (!StringUtils.isEmpty(nextOffset)) {
			p.put("offset", nextOffset);
		}

		if (filterMap != null) {
			// filter=key1>>value1|key2>>value2|key3>>value3...
			StringBuffer buffer = new StringBuffer();
			int i = 0;
			for (String key : filterMap.keySet()) {
				if (i > 0) {
					buffer.append("|");
				}
				buffer.append(key).append(">>").append(filterMap.get(key));
				i++;
			}

			p.put("filter", buffer.toString());
		}

		return p;

	}

	protected <T> T list(Class<T> responseType, URI path) {
		return restClient.get(responseType, path);
	}

	protected <T> T list(Class<T> responseType, URI path, int fetchSize,
			Map<String, String> queryFilterMap, String nextOffset) {
		Properties query = constructQueryParams(fetchSize, queryFilterMap,
				nextOffset);
		return restClient.get(responseType, path, query);
	}

	protected <T> T list(Class<T> responseType, URI path, int fetchSize,
			Map<String, String> queryFilterMap) {

		return list(responseType, path, fetchSize, queryFilterMap, null);
	}

	protected <T> T list(Class<T> responseType, URI path, int fetchSize) {
		return list(responseType, path, fetchSize, null, null);
	}

	protected <T> T list(Class<T> responseType, URI path, int fetchSize,
			String nextOffset) {
		return list(responseType, path, fetchSize, null, nextOffset);
	}

	protected <T> T list(Class<T> responseType, URI path, String nextOffset) {
		return list(responseType, path, -1, null, nextOffset);
	}

	protected <T> T list(Class<T> responseType, URI path,
			Map<String, String> queryFilterMap) {
		return list(responseType, path, -1, queryFilterMap, null);
	}

	protected <T> T list(Class<T> responseType, URI path,
			Map<String, String> queryFilterMap, String nextOffset) {
		return list(responseType, path, -1, queryFilterMap, nextOffset);
	}

}
