package com.bnbneeds.rest.client.impl;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.rest.client.ClientConfig;
import com.bnbneeds.rest.client.RestClient;
import com.bnbneeds.rest.client.exceptions.BNBNeedsClientException;
import com.bnbneeds.rest.client.model.request.FileAttachment;
import com.bnbneeds.rest.client.model.request.FormElement;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;

public abstract class AbstractRestClient implements RestClient {

	private static final Logger log = LoggerFactory
			.getLogger(AbstractRestClient.class);

	protected String apiBaseUri;
	protected ClientConfig config;
	protected String authToken;

	public AbstractRestClient(String apiBaseUri, ClientConfig config) {
		super();
		this.apiBaseUri = apiBaseUri;
		this.config = config;

		log.info("Base URL: {}", apiBaseUri);
		if (config.getSocketFactory() != null) {
			HttpsURLConnection.setDefaultSSLSocketFactory(config
					.getSocketFactory());
		}
		if (config.getHostnameVerifier() != null) {
			HttpsURLConnection.setDefaultHostnameVerifier(config
					.getHostnameVerifier());
		}
	}

	protected ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();

		mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

		return mapper;
	}

	public String getApiBaseUri() {
		return apiBaseUri;
	}

	public void setApiBaseUri(String apiBaseUri) {
		this.apiBaseUri = apiBaseUri;
	}

	@Override
	public String getAuthToken() {
		return authToken;
	}

	@Override
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	@Override
	public <T> T get(Class<T> responseType, URI uri) {
		return get(responseType, uri.toString());
	}

	@Override
	public <T> T get(Class<T> responseType, URI uri, Object... args) {
		return get(responseType, uri.toString(), args);
	}

	@Override
	public <T> T get(GenericType<T> responseType, URI uri) {
		return get(responseType, uri.toString());
	}

	@Override
	public <T> T get(GenericType<T> responseType, URI uri, Object... args) {
		return get(responseType, uri.toString(), args);
	}

	@Override
	public <T> T get(Class<T> responseType, URI path, Properties queryParams) {
		return get(responseType, path.toString(), queryParams);
	}

	@Override
	public <T> T login(Class<T> responseType, String username, String password,
			URI path) {
		return login(responseType, username, password, path.toString());
	}

	@Override
	public <T> T login(Class<T> responseType, String username, String password,
			String clientIPAddress, URI path) {

		return login(responseType, username, password, clientIPAddress,
				path.toString());
	}

	@Override
	public <T> T post(Class<T> responseType, Object request, URI uri) {
		return post(responseType, request, uri.toString());
	}

	@Override
	public <T> T post(Class<T> responseType, Object request, URI uri,
			Object... args) {
		return post(responseType, request, uri.toString(), args);
	}

	@Override
	public <T> T put(Class<T> responseType, Object request, URI uri) {
		return put(responseType, request, uri.toString());
	}

	@Override
	public <T> T put(Class<T> responseType, Object request, URI uri,
			Object... args) {
		return put(responseType, request, uri.toString(), args);
	}

	@Override
	public TaskResponse delete(URI uri, Properties queryParams) {
		return delete(uri.toString(), queryParams);
	}

	@Override
	public TaskResponse delete(URI uri) {
		return delete(uri.toString());
	}

	@Override
	public TaskResponse delete(URI uri, Object... args) {
		return delete(uri.toString(), args);
	}

	@Override
	public <T> T put(Class<T> responseType, Object request, URI uri,
			Properties queryParams) {
		return put(responseType, request, uri.toString(), queryParams);
	}

	@Override
	public <T> T post(Class<T> responseType, Object request, URI uri,
			Properties queryParams) {
		return post(responseType, request, uri.toString(), queryParams);
	}

	@Override
	public <T> T postMultiPart(Class<T> responseType, String filename,
			InputStream request, String path) {
		throw BNBNeedsClientException.methodNotSupported(config.getClientType()
				.name());
	}

	@Override
	public <T> T postMultiPart(Class<T> responseType, String filename,
			String contentType, long fileSize, InputStream request, String path) {
		throw BNBNeedsClientException.methodNotSupported(config.getClientType()
				.name());
	}

	@Override
	public <T> T postMultiPart(Class<T> responseType, String filename,
			String contentType, long fileSize, InputStream request,
			String path, Object... args) {
		throw BNBNeedsClientException.methodNotSupported(config.getClientType()
				.name());
	}

	@Override
	public <T> T postMultiPart(Class<T> responseType, Object request,
			String path) {

		throw BNBNeedsClientException.methodNotSupported(config.getClientType()
				.name());

	}

	@Override
	public <T> T postMultiPart(Class<T> responseType, Object request,
			String path, Object... args) {
		throw BNBNeedsClientException.methodNotSupported(config.getClientType()
				.name());
	}

	@Override
	public <T> T postMultiPart(Class<T> responseType, String filename,
			InputStream request, String path, Object... args) {
		throw BNBNeedsClientException.methodNotSupported(config.getClientType()
				.name());
	}

	@Override
	public <T> T postMultiPart(Class<T> responseType, File file, String path) {
		throw BNBNeedsClientException.methodNotSupported(config.getClientType()
				.name());
	}

	@Override
	public Response get(String path, String mediaType, Properties queryParams) {
		throw BNBNeedsClientException.methodNotSupported(config.getClientType()
				.name());
	}

	@Override
	public Response get(String path, String mediaType) {
		throw BNBNeedsClientException.methodNotSupported(config.getClientType()
				.name());
	}

	@Override
	public Response get(URI path, String mediaType, Properties queryParams) {
		return get(path.toString(), mediaType, queryParams);
	}

	@Override
	public Response get(URI path, String mediaType) {
		return get(path.toString(), mediaType);
	}

	@Override
	public byte[] getImage(String path, String mediaType, Properties queryParams) {
		throw BNBNeedsClientException.methodNotSupported(config.getClientType()
				.name());
	}

	@Override
	public byte[] getImage(String path, String mediaType) {
		throw BNBNeedsClientException.methodNotSupported(config.getClientType()
				.name());
	}

	@Override
	public byte[] getImage(URI path, String mediaType, Properties queryParams) {
		return getImage(path.toString(), mediaType, queryParams);
	}

	@Override
	public byte[] getImage(URI path, String mediaType) {
		return getImage(path.toString(), mediaType);
	}

	@Override
	public <T> T postMultiPart(Class<T> responseType,
			FileAttachment fileAttachment, String path) {
		throw BNBNeedsClientException.methodNotSupported(config.getClientType()
				.name());
	}

	@Override
	public <T> T postMutiPart(Class<T> responseType,
			List<FormElement> formElementList,
			List<FileAttachment> fileAttachments, String path) {
		throw BNBNeedsClientException.methodNotSupported(config.getClientType()
				.name());
	}

	@Override
	public <T> T postMutiPart(Class<T> responseType,
			List<FormElement> formElementList,
			List<FileAttachment> fileAttachments, URI path) {
		return postMutiPart(responseType, formElementList, fileAttachments,
				path.toString());
	}

}
