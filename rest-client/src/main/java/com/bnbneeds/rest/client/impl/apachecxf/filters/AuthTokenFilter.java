package com.bnbneeds.rest.client.impl.apachecxf.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.rest.client.impl.Constants;
import com.bnbneeds.rest.client.impl.TokenAccess;

public class AuthTokenFilter implements ClientResponseFilter,
		ClientRequestFilter {

	private static final Logger log = LoggerFactory
			.getLogger(AuthTokenFilter.class);

	private TokenAccess sessionAccess;

	public AuthTokenFilter(TokenAccess sessionAccess) {
		this.sessionAccess = sessionAccess;
	}

	@Override
	public void filter(ClientRequestContext requestContext,
			ClientResponseContext responseContext) throws IOException {

		int responseHttpCode = responseContext.getStatus();
		log.info("Response HTTP code: {}.", responseHttpCode);
		if (Status.OK.getStatusCode() == responseHttpCode) {
			Map<String, NewCookie> cookieMap = responseContext.getCookies();
			NewCookie authCookie = cookieMap.get(Constants.AUTH_TOKEN_KEY);
			if (authCookie != null) {
				log.info("Received auth cookie from response.");
				sessionAccess.setToken(authCookie.getValue());
			}

		} else if (Status.UNAUTHORIZED.getStatusCode() == responseHttpCode) {
			log.info(
					"Setting token to null as it was an unauthorized request. HTTP response code: {}.",
					responseHttpCode);
			sessionAccess.setToken(null);

		}
	}

	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		MultivaluedMap<String, Object> reqHeaderMap = requestContext
				.getHeaders();
		List<Object> valueList = reqHeaderMap.get(HttpHeaders.COOKIE);
		String token = sessionAccess.getToken();
		if (token == null && valueList != null) {
			log.info("Request auth token is null.");
			for (Iterator<Object> iterator = valueList.iterator(); iterator
					.hasNext();) {
				String value = (String) iterator.next();
				if (value.contains(Constants.AUTH_TOKEN_KEY)) {
					log.info("Removing auth cookie from the request header.");
					iterator.remove();
				}
			}
		} else {
			if (token != null) {

				if (valueList == null) {
					valueList = new ArrayList<Object>();
				}

				String cookieHeadervalue = new Cookie(Constants.AUTH_TOKEN_KEY,
						token).toString();
				if (!valueList.contains(cookieHeadervalue)) {
					valueList.add(cookieHeadervalue);
				}
				log.info("Setting auth token as cookie in the request header.");
				reqHeaderMap.addAll(HttpHeaders.COOKIE, valueList);
			}
		}

	}
}
