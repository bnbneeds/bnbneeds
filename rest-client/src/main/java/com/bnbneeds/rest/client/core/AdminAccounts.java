package com.bnbneeds.rest.client.core;

import java.util.Map;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.UpdateAccountStatusParam;
import com.bnbneeds.app.model.account.CreateUserAccountParam;
import com.bnbneeds.app.model.account.UpdateUserAccountParam;
import com.bnbneeds.app.model.account.UserAccountInfoListResponse;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class AdminAccounts extends SearchableResource {

	
	public UserAccountInfoListResponse listBulk() {
		return list(UserAccountInfoListResponse.class, Endpoint.ADMIN_USER_ACCOUNTS_BULK
				.get());
	}

	public UserAccountInfoListResponse listBulk(int fetchSize) {
		return list(UserAccountInfoListResponse.class, Endpoint.ADMIN_USER_ACCOUNTS_BULK
				.get(), fetchSize);
	}

	public UserAccountInfoListResponse listBulk(int fetchSize, String nextOffset) {
		return list(UserAccountInfoListResponse.class, Endpoint.ADMIN_USER_ACCOUNTS_BULK
				.get(), fetchSize, nextOffset);
	}

	public UserAccountInfoListResponse listBulk(int fetchSize,
			Map<String, String> queryMap, String nextOffset) {
		return list(UserAccountInfoListResponse.class, Endpoint.ADMIN_USER_ACCOUNTS_BULK
				.get(), fetchSize, queryMap, nextOffset);
	}

	public UserAccountInfoListResponse listBulk(int fetchSize,
			Map<String, String> queryMap) {
		return list(UserAccountInfoListResponse.class, Endpoint.ADMIN_USER_ACCOUNTS_BULK
				.get(), fetchSize, queryMap);
	}

	public String listBulkAsString(int fetchSize) {
		return list(String.class, Endpoint.ADMIN_USER_ACCOUNTS_BULK.get(), fetchSize);
	}

	public String listBulkAsString(int fetchSize,
			Map<String, String> queryFilterMap) {
		return list(String.class, Endpoint.ADMIN_USER_ACCOUNTS_BULK.get(),
				fetchSize, queryFilterMap);
	}

	public String listBulkAsString(int fetchSize, String nextOffset) {
		return list(String.class, Endpoint.ADMIN_USER_ACCOUNTS_BULK.get(),
				fetchSize, nextOffset);
	}

	public String listBulkAsString(int fetchSize,
			Map<String, String> queryFilterMap, String nextOffset) {
		return list(String.class, Endpoint.ADMIN_USER_ACCOUNTS_BULK.get(),
				fetchSize, queryFilterMap, nextOffset);
	}

	public AdminAccounts(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public CreateResourceResponse createAccount(CreateUserAccountParam user) {
		return create(user, Endpoint.ADMIN_USER_ACCOUNTS.get());

	}

	public TaskResponse updateAccount(UpdateUserAccountParam user) {
		return update(user, Endpoint.ADMIN_USER_ACCOUNTS.get());
	}
	

	public UserAccountInfoResponse getAccount() {
		return restClient.get(UserAccountInfoResponse.class,
				Endpoint.ADMIN_USER_ACCOUNT_INFO.get());

	}
	
	public UserAccountInfoResponse getAccount(String accountId) {
		return restClient.get(UserAccountInfoResponse.class,
				Endpoint.ADMIN_USER_ACCOUNT_INFO.get(accountId));

	}
	
	public TaskResponse deleteAccount(String accountId) {
		return delete(Endpoint.ADMIN_USER_ACCOUNT_INFO.get(accountId));

	}
	
	public TaskResponse updateAccountStatus(String id, UpdateAccountStatusParam param) {
		return restClient.put(TaskResponse.class, param,
				Endpoint.ADMIN_USER_ACCOUNTS_APPROVALS.get(id));
	}
	
	public String getUserInfo(String userId) {
		return restClient.get(String.class, Endpoint.ADMIN_GENERIC_USER_INFO.get(userId));
	}


}
