package com.bnbneeds.rest.client.core;

import com.bnbneeds.app.model.PipelineJobInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;

public class PipelineJobs extends SearchableResource {

	public PipelineJobs(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public PipelineJobInfoResponse getJobInfo(String jobId) {
		return restClient.get(PipelineJobInfoResponse.class, Endpoint.PIPELINE_JOB_INFO.get(jobId));
	}

	public String getJobInfoAsString(String jobId) {
		return restClient.get(String.class, Endpoint.PIPELINE_JOB_INFO.get(jobId));
	}

}
