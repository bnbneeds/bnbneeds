package com.bnbneeds.rest.client.core;

import java.util.ArrayList;
import java.util.List;

import com.bnbneeds.app.model.EmailMessageParam;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.RestClient;
import com.bnbneeds.rest.client.exceptions.BNBNeedsClientException;
import com.bnbneeds.rest.client.model.request.FileAttachment;
import com.bnbneeds.rest.client.model.request.FormElement;
import com.bnbneeds.rest.client.util.GeneralUtils;

public class Messaging extends Resource {

	public Messaging(RestClient restClient, BNBNeedsClient coreClient) {
		super(restClient, coreClient);
	}

	public TaskResponse sendEmail(EmailMessageParam param) {
		return restClient.post(TaskResponse.class, param,
				Endpoint.MESSAGING_MAIL.get());
	}

	public TaskResponse sendEmail(List<FormElement> formDataList,
			FileAttachment inlineImage, FileAttachment attachment) {
		GeneralUtils.assertNotEmpty(formDataList,
				"Form element list cannot be null or empty.");

		boolean attachmentProvided = (inlineImage != null || attachment != null);
		if (!attachmentProvided) {
			throw new BNBNeedsClientException(
					"Inline image or attachment must be provided.");
		}

		List<FileAttachment> fileAttachments = new ArrayList<FileAttachment>();

		if (attachment != null) {
			if (!"attachment".equals(attachment.getElementName())) {
				throw new BNBNeedsClientException(
						"attachment: Form element name must be 'attachment'.");
			}
			fileAttachments.add(attachment);
		}

		if (inlineImage != null) {
			if (!"inlineImage".equals(inlineImage.getElementName())) {
				throw new BNBNeedsClientException(
						"inlineImage: Form element name must be 'inlineImage'.");
			} else if (!GeneralUtils.isImage(inlineImage)) {
				throw new BNBNeedsClientException(
						"inlineImage: File is not an image: "
								+ inlineImage.getFileName() + ".");
			}

			fileAttachments.add(inlineImage);
		}

		return restClient.postMutiPart(TaskResponse.class, formDataList,
				fileAttachments, Endpoint.MESSAGING_MAIL_WITH_ATTACHMENT.get());

	}
}
