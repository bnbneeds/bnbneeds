package com.bnbneeds.rest.client;

import java.net.URI;

import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.rest.client.core.AbuseReports;
import com.bnbneeds.rest.client.core.AdminAccounts;
import com.bnbneeds.rest.client.core.AdminEntities;
import com.bnbneeds.rest.client.core.BusinessTypes;
import com.bnbneeds.rest.client.core.BuyerBiddingSubscriptionPlans;
import com.bnbneeds.rest.client.core.BuyerBiddingSubscriptions;
import com.bnbneeds.rest.client.core.BuyerPaymentTransactions;
import com.bnbneeds.rest.client.core.BuyerReviews;
import com.bnbneeds.rest.client.core.Buyers;
import com.bnbneeds.rest.client.core.DealerAccounts;
import com.bnbneeds.rest.client.core.Messaging;
import com.bnbneeds.rest.client.core.Notifications;
import com.bnbneeds.rest.client.core.PipelineJobs;
import com.bnbneeds.rest.client.core.ProductCategories;
import com.bnbneeds.rest.client.core.ProductNames;
import com.bnbneeds.rest.client.core.ProductReviews;
import com.bnbneeds.rest.client.core.ProductTypes;
import com.bnbneeds.rest.client.core.Products;
import com.bnbneeds.rest.client.core.TenderBids;
import com.bnbneeds.rest.client.core.Tenders;
import com.bnbneeds.rest.client.core.Users;
import com.bnbneeds.rest.client.core.Utilities;
import com.bnbneeds.rest.client.core.VendorBiddingSubscriptionPlans;
import com.bnbneeds.rest.client.core.VendorBiddingSubscriptions;
import com.bnbneeds.rest.client.core.VendorPaymentTransactions;
import com.bnbneeds.rest.client.core.VendorReviews;
import com.bnbneeds.rest.client.core.Vendors;
import com.bnbneeds.rest.client.util.ClientUtils;

public class BNBNeedsClient {
	private RestClient client;

	public BNBNeedsClient(ClientType clientType, String host, int port, boolean ssl) {

		ClientConfig clientConfig = new ClientConfig().withClientType(clientType).withSSL(ssl).withHost(host)
				.withPort(port);
		this.client = clientConfig.newClient();

	}

	public BNBNeedsClient(ClientType clientType, String baseURL) {

		ClientUtils.checkURLisValid(baseURL);
		URI uri = URI.create(baseURL);
		boolean ssl = "https".equals(uri.getScheme());
		String host = uri.getHost();
		int portNumber = uri.getPort();

		if (portNumber < 0) {
			portNumber = ssl ? 443 : 80;
		}
		ClientConfig clientConfig = new ClientConfig().withClientType(clientType).withSSL(ssl).withHost(host)
				.withPort(portNumber);

		this.client = clientConfig.newClient();

	}

	public static synchronized BNBNeedsClient newClient(ClientType clientType, String host, int port, boolean ssl) {

		ClientConfig clientConfig = new ClientConfig().withClientType(clientType).withSSL(ssl).withHost(host)
				.withPort(port);
		return new BNBNeedsClient(clientConfig);
	}

	public static synchronized BNBNeedsClient newClient(ClientType clientType, String baseURL) {

		ClientUtils.checkURLisValid(baseURL);
		URI uri = URI.create(baseURL);
		boolean ssl = "https".equals(uri.getScheme());
		String host = uri.getHost();
		int portNumber = uri.getPort();

		if (portNumber < 0) {
			portNumber = ssl ? 443 : 80;
		}
		return newClient(clientType, host, portNumber, ssl);
	}

	private BNBNeedsClient(ClientConfig config) {
		this.client = config.newClient();
	}

	public ClientConfig getConfig() {
		return client.getConfig();
	}

	public BNBNeedsClient ignoreCetificates(boolean ignore) {
		getConfig().setIgnoreCertificates(ignore);
		return this;
	}

	/**
	 * Sets the authentication token to be used for this client.
	 *
	 * @param authToken
	 *            The authentication token to set.
	 */
	public void setAuthToken(String authToken) {
		client.setAuthToken(authToken);
	}

	public AuthClient auth() {
		return new AuthClient(client);
	}

	/**
	 * Performs an authentication login and returns the updated client.
	 *
	 * @see AuthClient#login(String, String)
	 * @param username
	 *            The username.
	 * @param password
	 *            The password.
	 * @return The updated client.
	 */
	public UserAccountInfoResponse login(String username, String password) {
		return auth().login(username, password);
	}

	public UserAccountInfoResponse login(String username, String password, String clientIPAddress) {
		return auth().login(username, password, clientIPAddress);
	}

	public BNBNeedsClient logout() {
		auth().logout();
		return this;
	}

	/**
	 * Sets the authentication token and returns the updated client.
	 *
	 * @see #setAuthToken(String)
	 * @param token
	 *            The authentication token to set.
	 * @return The updated client.
	 */
	public BNBNeedsClient withAuthToken(String token) {
		setAuthToken(token);
		return this;
	}

	/*
	 * Below are resource specific factory methods. These will get the resource
	 * instances when called, and the caller will be able to perform operations on
	 * them.
	 */
	public Vendors vendors() {
		return new Vendors(client, this);
	}

	public Vendors vendors(String vendorId) {
		return new Vendors(client, this, vendorId);
	}

	public Users users() {
		return new Users(client, this);
	}

	public AdminAccounts adminAccounts() {
		return new AdminAccounts(client, this);
	}

	public Buyers buyers() {
		return new Buyers(client, this);
	}

	public Buyers buyers(String buyerId) {
		return new Buyers(client, this, buyerId);
	}

	public ProductTypes productTypes() {
		return new ProductTypes(client, this);
	}

	public ProductCategories productCategories() {
		return new ProductCategories(client, this);
	}

	public ProductNames productNames() {
		return new ProductNames(client, this);
	}

	public BusinessTypes businessTypes() {
		return new BusinessTypes(client, this);
	}

	public Products products() {
		return new Products(client, this);
	}

	public Products products(String productId) {
		return new Products(client, this, productId);
	}

	public Notifications notifications() {
		return new Notifications(client, this);
	}

	public AbuseReports abuseReports() {
		return new AbuseReports(client, this);
	}

	public BuyerReviews buyerReviews(String buyerId) {
		return new BuyerReviews(client, this, buyerId);
	}

	public VendorReviews vendorReviews(String vendorId) {
		return new VendorReviews(client, this, vendorId);
	}

	public ProductReviews productReviews(String productId) {
		return new ProductReviews(client, this, productId);
	}

	public BuyerBiddingSubscriptionPlans buyerBiddingSubscriptionPlans() {
		return new BuyerBiddingSubscriptionPlans(client, this);
	}

	public Messaging messaging() {
		return new Messaging(client, this);
	}

	public AdminEntities adminEntities() {
		return new AdminEntities(client, this);
	}

	public DealerAccounts adminDealerAccounts() {
		return new DealerAccounts(client, this);
	}

	public VendorBiddingSubscriptionPlans vendorBiddingSubscriptionPlans() {
		return new VendorBiddingSubscriptionPlans(client, this);
	}

	public BuyerBiddingSubscriptions buyerBiddingSubscriptions(String buyerId) {
		return new BuyerBiddingSubscriptions(client, this, buyerId);
	}

	public VendorBiddingSubscriptions vendorBiddingSubscriptions(String vendorId) {
		return new VendorBiddingSubscriptions(client, this, vendorId);
	}

	public VendorPaymentTransactions vendorPaymentTransactions(String vendorId) {
		return new VendorPaymentTransactions(client, this, vendorId);
	}

	public BuyerPaymentTransactions buyerPaymentTransactions(String buyerId) {
		return new BuyerPaymentTransactions(client, this, buyerId);
	}

	public Tenders tenders(String buyerId) {
		return new Tenders(client, this, buyerId);
	}

	public TenderBids tenderBids() {
		return new TenderBids(client, this);
	}

	public Utilities utilities() {
		return new Utilities(client, this);
	}

	public PipelineJobs pipelineJobs() {
		return new PipelineJobs(client, this);
	}
}
