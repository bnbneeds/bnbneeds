package com.bnbneeds.rest.client.model.request;

import java.io.InputStream;

import com.bnbneeds.rest.client.util.GeneralUtils;

public class FileAttachment extends FormElement {

	private String contentId;
	private String fileName;
	private String mediaType;
	private InputStream inputStream;

	public FileAttachment(String elementName, String fileName,
			String mediaType, InputStream inputStream) {
		super();
		GeneralUtils.assertNotEmpty(elementName,
				"Element name cannot be null or empty");
		GeneralUtils.assertNotEmpty(fileName,
				"File name cannot be null or empty");
		GeneralUtils.assertNotEmpty(mediaType,
				"File media type cannot be null or empty");
		GeneralUtils.assertNotNull(inputStream, "Input stream cannot be null");
		setElementName(elementName);
		this.fileName = fileName;
		this.mediaType = mediaType;
		this.inputStream = inputStream;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getFileName() {
		return fileName;
	}

	public String getMediaType() {
		return mediaType;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileAttachment [");
		if (contentId != null)
			builder.append("contentId=").append(contentId).append(", ");
		if (fileName != null)
			builder.append("fileName=").append(fileName).append(", ");
		if (mediaType != null)
			builder.append("mediaType=").append(mediaType);
		builder.append("]");
		return builder.toString();
	}

}
