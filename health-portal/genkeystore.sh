#!/usr/bin/env bash

if [ -n "$JAVA_HOME" ] ; then
    if [ -x "$JAVA_HOME/jre/sh/java" ] ; then
        # IBM's JDK on AIX uses strange locations for the executables
        JAVACMD="$JAVA_HOME/jre/sh/java"
    else
        JAVACMD="$JAVA_HOME/bin/java"
    fi
    if [ ! -x "$JAVACMD" ] ; then
        die "ERROR: JAVA_HOME is set to an invalid directory: $JAVA_HOME

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
    fi
else
    JAVACMD="java"
    which java >/dev/null 2>&1 || die "ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
fi

KEYSTORE_PATH=bnbneeds.keystore
KEYTOOL=${JAVA_HOME}/bin/keytool
"${KEYTOOL}" -genkey -alias server -dname "CN=127.0.0.1, OU=ASD, O=BNBNeeds, L=IN, S=KA, C=Bangalore" \
                         -keyalg RSA -keypass changeit -storepass changeit -keystore ${KEYSTORE_PATH} \
                         -validity 365

keytool_get_exit_code=$?
if [ $keytool_get_exit_code -ne 0 ]; then
    echo "Failed to create server keystore."
    exit $keytool_get_exit_code
fi