package com.bnbneeds.android.vendor.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.bean.RowItemVendor;

import java.util.List;

/**
 * Created by Len on 07-02-2017.
 */

public class CustomBuyerAdapter extends ArrayAdapter<RowItemVendor> {

    Context context;

    public CustomBuyerAdapter(Context context, int resourceId, List<RowItemVendor> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        // ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
        TextView vendor;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        RowItemVendor rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.vendorlist, null);
            holder = new ViewHolder();

            //  holder.imageView = (ImageView) convertView.findViewById(R.id.productImage);
            holder.vendor = (TextView) convertView.findViewById(R.id.vendor_name);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();


        // holder.imageView.setImageResource(rowItem.getImageId());
        holder.vendor.setText(rowItem.getVendor());

        return convertView;
    }
}
