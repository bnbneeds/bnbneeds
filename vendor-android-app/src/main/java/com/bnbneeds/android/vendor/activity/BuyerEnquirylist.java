package com.bnbneeds.android.vendor.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.adapter.CustomListviewAdapter;
import com.bnbneeds.android.vendor.bean.RowItem;
import com.bnbneeds.android.vendor.util.CheckConnectivity;
import com.bnbneeds.app.model.dealer.EnquiryInfoListResponse;
import com.bnbneeds.app.model.dealer.EnquiryInfoResponse;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.bnbneeds.android.vendor.activity.ConstantsActivity.pDialog;

public class BuyerEnquirylist extends BaseActivity {

    String productname, comments, postedon, reqty, likelytobuy, indays, buyerenqid;

    // List view
    private ListView lv;
    List<RowItem> rowItems;
    protected static String buyername,buyerNameid;

    // Listview Adapter
    CustomListviewAdapter adapter;

    ArrayList<String> productlist_Name = new ArrayList<>();
    ArrayList<String> buyerlist = new ArrayList<>();
    ArrayList<String> commentslist = new ArrayList<>();
    ArrayList<String> postedlist = new ArrayList<>();
    ArrayList<String> reqqtylist = new ArrayList<>();
    ArrayList<String> likelytolist = new ArrayList<>();
    ArrayList<String> likelytolistindays = new ArrayList<>();
    ArrayList<String> buyeridlist = new ArrayList<>();

    String[] productnamestring;
    String[] buyerliststring;
    String[] commentsliststring;
    String[] postedliststring;
    String[] reqqtyliststring;
    String[] likelytoliststring;
    String[] likelytodaysliststring;
    String[] buyeridstring;


    // Search EditText
    EditText inputSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer_enquirylist);

        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if (conn == true) {
            new BuyerEnquiryTask().execute();
        } else {
            Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent home = new Intent(this, HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }

    class BuyerEnquiryTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();

        }

        @Override
        protected String doInBackground(String... params) {

            try {

                EnquiryInfoListResponse elistresponse = client.vendors(LoginActivity.RELATIONSHIP_ID).getEnquiries(1000);
                if (elistresponse != null) {
                    List<EnquiryInfoResponse> respnse = elistresponse.getEnquiryList();
                    if (respnse != null && !respnse.isEmpty()) {
                        for (Iterator<EnquiryInfoResponse> enquiry = respnse.iterator(); enquiry.hasNext(); ) {
                            EnquiryInfoResponse enquirylist = enquiry.next();
                            productname = enquirylist.getProduct().getName();
                            buyername = enquirylist.getBuyer().getName();
                            comments = enquirylist.getDescription();
                            postedon = enquirylist.getRequestDate();
                            reqty = enquirylist.getRequiredQuantity();
                            likelytobuy = enquirylist.getLikelyToBuyIn().getTimeUnit();
                            indays = enquirylist.getLikelyToBuyIn().getValue().toString();
                            buyerenqid = enquirylist.getBuyer().getId().toString();


                            productlist_Name.add(productname);
                            buyerlist.add(buyername);
                            commentslist.add(comments);
                            postedlist.add(postedon);
                            reqqtylist.add(reqty);
                            likelytolist.add(likelytobuy);
                            likelytolistindays.add(indays);
                            buyeridlist.add(buyerenqid);

                            productnamestring = productlist_Name.toArray(new String[productlist_Name.size()]);
                            buyerliststring = buyerlist.toArray(new String[buyerlist.size()]);
                            commentsliststring = commentslist.toArray(new String[commentslist.size()]);
                            postedliststring = postedlist.toArray(new String[postedlist.size()]);
                            reqqtyliststring = reqqtylist.toArray(new String[reqqtylist.size()]);
                            likelytoliststring = likelytolist.toArray(new String[likelytolist.size()]);
                            likelytodaysliststring = likelytolistindays.toArray(new String[likelytolistindays.size()]);
                            buyeridstring = buyeridlist.toArray(new String[buyeridlist.size()]);

                        }
                    } else {
                        Intent home = new Intent(BuyerEnquirylist.this, HomeActivity.class);
                        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(home);
                        pDialog.dismiss();
                    }
                }
            }catch (BNBNeedsServiceException exp){

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();

            if (buyerliststring != null) {
                rowItems = new ArrayList<RowItem>();
                for (int i = 0; i < buyerliststring.length; i++) {
                    RowItem item = new RowItem(productnamestring[i], buyeridstring[i], buyerliststring[i], commentsliststring[i], postedliststring[i], reqqtyliststring[i], likelytoliststring[i], likelytodaysliststring[i]);
                    rowItems.add(item);
                }

                lv = (ListView) findViewById(R.id.list_view);


                // Adding items to listview

                adapter = new CustomListviewAdapter(BuyerEnquirylist.this, R.layout.listitem, rowItems);
                lv.setAdapter(adapter);

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        String selected_product = adapter.getItem(position).getBuyername();
                        buyerNameid = adapter.getItem(position).getBuyerenquiryid();


                        // Toast.makeText(getApplicationContext(),"You selected "+vendorid,Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(BuyerEnquirylist.this, BuyerDetailsEnquiryListActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("selectedname", selected_product);
                        bundle.putString("buyerid", buyerNameid);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                });
            } else {
                Toast.makeText(getApplicationContext(), "No data", Toast.LENGTH_LONG).show();
                Intent home = new Intent(BuyerEnquirylist.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);


            }

            inputSearch = (EditText) findViewById(R.id.search);
            if (inputSearch != null) {
                inputSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String text = inputSearch.getText().toString();
                        adapter.getFilter().filter(text);
                    }
                });
            } else {
                Intent home = new Intent(BuyerEnquirylist.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        }
    }

}
