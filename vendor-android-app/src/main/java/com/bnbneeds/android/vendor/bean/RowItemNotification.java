package com.bnbneeds.android.vendor.bean;

/**
 * Created by Len on 20-03-2017.
 */

public class RowItemNotification {

    // private int imageId;
    private String title;
    private String product;
    private String vendor;


    public RowItemNotification(String title, String vendor, String product) {
        // this.imageId = imageId;
        this.title = title;
        this.product = product;
        this.vendor = vendor;


    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }
}