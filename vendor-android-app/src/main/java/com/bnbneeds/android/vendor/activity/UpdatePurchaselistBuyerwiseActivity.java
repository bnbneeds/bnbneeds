package com.bnbneeds.android.vendor.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.adapter.PlistDisplayAdapter;
import com.bnbneeds.android.vendor.db.DbHelper;
import com.bnbneeds.app.model.dealer.order.PurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.UpdatePurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.UpdatePurchaseListParam;

import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;

import static com.bnbneeds.android.vendor.activity.ConstantsActivity.pDialog;

/**
 * Created by Len on 02-03-2017.
 */

public class UpdatePurchaselistBuyerwiseActivity extends BaseActivity {

    private ArrayList<String> pId = new ArrayList<String>();
    private ArrayList<String> pName = new ArrayList<String>();
    private ArrayList<String> pNameId = new ArrayList<>();
    private ArrayList<Double> pQuantity = new ArrayList<Double>();
    private ArrayList<String> pUnits = new ArrayList<String>();
    private ArrayList<Double> pDeliQuantity = new ArrayList<>();
    private ArrayList<String> pRemarks = new ArrayList<String>();
    private SQLiteDatabase dataBase;
    private DbHelper mHelper = new DbHelper(this);

    private String strupdatedStatus = "DELIVERED";
    private String strcomments = "General Comments";
    private String strunit, strPurchaseItemId;
    private Double strDeliveredQuantity;
    ListView userList;
    FloatingActionButton updatepurchaselist;
    private String connection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchaselist_buyerwise);
        userList = (ListView) findViewById(R.id.product_listview);
        displayData();
        updatepurchaselist = (FloatingActionButton) findViewById(R.id.updatePlist);


        updatepurchaselist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UpdatePurchaselistBuyerwiseTask().execute();
            }
        });


    }


    /**
     * displays data from SQLite
     */
    private void displayData() {
        dataBase = mHelper.getWritableDatabase();
        String select = "SELECT * FROM ItemListTable";


        Cursor mCursor = dataBase.rawQuery(select, null);


        pId.clear();
        pName.clear();
        pQuantity.clear();
        pUnits.clear();
        pNameId.clear();
        pRemarks.clear();
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        if (mCursor.moveToFirst()) {
            do {
                pId.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_ID)));
                pName.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_PRODUCTNAME)));
                pNameId.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_PURCHASEITEM_ID)));
                pQuantity.add(mCursor.getDouble(mCursor.getColumnIndex(DbHelper.KEY_REqQUANTITY)));
                pUnits.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_UNITS)));
                pDeliQuantity.add(mCursor.getDouble(mCursor.getColumnIndex(DbHelper.KEY_DElQUANTITY)));
                pRemarks.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_REMARKS)));


            } while (mCursor.moveToNext());
        }

        PlistDisplayAdapter disadpt = new PlistDisplayAdapter(UpdatePurchaselistBuyerwiseActivity.this, pId, pName,pRemarks, pQuantity, pUnits, pDeliQuantity, pUnits);
        userList.setAdapter(disadpt);

        userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent newActivity = new Intent(UpdatePurchaselistBuyerwiseActivity.this, UpdateActivity.class);
                newActivity.putExtra("id", pId.get(position));
                newActivity.putExtra("reqqty", pQuantity.get(position));
                newActivity.putExtra("delqty", pDeliQuantity.get(position));
                startActivity(newActivity);

            }
        });

        mCursor.close();
    }

    @Override
    public void onBackPressed() {
        deletedata();
        Intent home = new Intent(this, HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(home);
        return;
    }

    private void deletedata() {
        dataBase = mHelper.getWritableDatabase();
        dataBase.delete(DbHelper.TABLE_NAME, null, null);
        dataBase.close();
    }

    class UpdatePurchaselistBuyerwiseTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {

            try {

                UpdatePurchaseListParam listparam = new UpdatePurchaseListParam();

                for (int i = 0; i < pQuantity.size(); i++) {
                    UpdatePurchaseItemParam param = new UpdatePurchaseItemParam();

                    strunit = pUnits.get(i);
                    strPurchaseItemId = pNameId.get(i);
                    strDeliveredQuantity = pDeliQuantity.get(i);


                    PurchaseItemParam.PurchaseStatus status = new PurchaseItemParam.PurchaseStatus();
                    status.setComments(strcomments);
                    status.setStatus(strupdatedStatus);

                    param.setPurchaseItemId(strPurchaseItemId);
                    param.setDeliveredQuantity(strDeliveredQuantity);
                    param.setUnit(strunit);
                    param.setStatusUpdate(status);
                    listparam.add(param);
                    client.vendors().updatePurchaseList(LoginActivity.RELATIONSHIP_ID, listparam);
                }

            } catch (ResourceAccessException a) {
                connection = a.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            deletedata();
            pDialog.dismiss();
            if (connection == null) {
                Toast.makeText(getApplicationContext(), "Purchase List has been Updated Successfully!", Toast.LENGTH_LONG).show();
                Intent home = new Intent(UpdatePurchaselistBuyerwiseActivity.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            } else {
                Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }


        }
    }
}
