package com.bnbneeds.android.vendor.activity;

/**
 * Created by Len on 01-03-2017.
 */


import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.bean.Rowproductlist;
import com.bnbneeds.android.vendor.db.DbHelper;
import com.bnbneeds.android.vendor.util.CheckConnectivity;
import com.bnbneeds.app.model.dealer.order.PurchaseItemInfoResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.PurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.UpdatePurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.UpdatePurchaseListParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import static com.bnbneeds.android.vendor.activity.ConstantsActivity.pDialog;

public class PurchaselistBuyerwiseActivity extends BaseActivity {
    String buyername;
    String buyerid;
    String requestdate;

    FloatingActionButton updatepurchaselist;
    Map<String, String> queryParams = new HashMap<>();
    private SQLiteDatabase dataBase;
    private DbHelper mHelper = new DbHelper(this);


    ArrayList<String> productlist_Name = new ArrayList<>();
    ArrayList<Double> requestedqtylist = new ArrayList<>();
    ArrayList<String> unitslist = new ArrayList<>();
    ArrayList<String> remarks = new ArrayList<>();
    ArrayList<String> purchaseitemlist_id = new ArrayList<>();
    ArrayList<Double> deliveredqtylist = new ArrayList<>();
    ArrayList<String> buyerlist_name = new ArrayList<>();
    ArrayList<String> vendorlist_id = new ArrayList<>();

    String[] buyerstring;
    String[] vendoridstring;
    String[] purchaselistidstring;
    String[] productnamestring;
    String[] unitsliststring;
    String[] productidliststring;
    String[] remarksliststring = {"no data"};
    Double[] requestedqtyliststring;
    Double[] deliveredqtyliststring;


    private String strunit, strPurchaseItemId, strvendorId;
    Double strDeliveredQuantity;
    Double strRequiredQuantity;
    private String strupdatedStatus = "DELIVERED";
    private String strcomments = "General Comments";
    EditText search;
    Button getlist;
    List<Rowproductlist> productItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getpurchaselist);


        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        buyername = b.getString("selectedname");
        buyerid = b.getString("buyerid");
        requestdate = b.getString("requestdate");



        getlist = (Button) findViewById(R.id.plistbutton);
        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if (conn == true) {
            new PurchaselistBuyerwiseTask().execute();
        } else {
            Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }


        getlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PurchaselistBuyerwiseActivity.this, UpdatePurchaselistBuyerwiseActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent home = new Intent(this, HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }

    class PurchaselistBuyerwiseTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();

        }

        @Override
        protected String doInBackground(String... params) {

            queryParams.put("buyerId", buyerid);
            queryParams.put("requestDate", requestdate);


            PurchaseListResponse listResponse = client.vendors(LoginActivity.RELATIONSHIP_ID).getPurchaseList(100, queryParams);
            if (listResponse != null) {
                List<PurchaseItemInfoResponse> purchaseList = listResponse.getPurchaseItems();
                if (purchaseList != null && !purchaseList.isEmpty()) {
                    for (PurchaseItemInfoResponse item : purchaseList) {
                        productlist_Name.add(item.getProductName().getName());
                        purchaseitemlist_id.add(item.getId());
                        requestedqtylist.add(item.getQuantity().getRequired());
                        unitslist.add(item.getQuantity().getUnit());
                        deliveredqtylist.add(item.getQuantity().getDelivered());
                        remarks.add(item.getTags());
                        purchaselistidstring = purchaseitemlist_id.toArray(new String[purchaseitemlist_id.size()]);
                        productnamestring = productlist_Name.toArray(new String[productlist_Name.size()]);
                        unitsliststring = unitslist.toArray(new String[unitslist.size()]);
                        requestedqtyliststring = requestedqtylist.toArray(new Double[requestedqtylist.size()]);
                        deliveredqtyliststring = deliveredqtylist.toArray(new Double[deliveredqtylist.size()]);
                        remarksliststring = remarks.toArray(new String[remarks.size()]);

                    }
                }
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            dataBase = mHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            for (int i = 0; i < productnamestring.length; i++) {
                values.put(DbHelper.KEY_PURCHASEITEM_ID, purchaselistidstring[i]);
                values.put(DbHelper.KEY_PRODUCTNAME, productnamestring[i]);
                values.put(DbHelper.KEY_DElQUANTITY, requestedqtyliststring[i]);
                values.put(DbHelper.KEY_REqQUANTITY, requestedqtyliststring[i]);
                values.put(DbHelper.KEY_UNITS, unitsliststring[i]);
                values.put(DbHelper.KEY_REMARKS,remarksliststring[i]);
                Log.d("Inserted Values", values.toString());
                dataBase.insert(DbHelper.TABLE_NAME, null, values);
            }


            dataBase.close();
            pDialog.dismiss();

            AlertDialog.Builder alert = new AlertDialog.Builder(PurchaselistBuyerwiseActivity.this);

            alert.setTitle("Purchase List Buyerwise");
            alert.setMessage("Click Continue to get Purchase List Buyerwise");
            alert.setIcon(R.drawable.agg_list);

            alert.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Intent i = new Intent(PurchaselistBuyerwiseActivity.this, UpdatePurchaselistBuyerwiseActivity.class);
                    startActivity(i);
                }
            });

            alert.show();
        }
    }
}