package com.bnbneeds.android.vendor.activity;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.bnbneeds.android.vendor.R;


import com.bnbneeds.android.vendor.bean.Rowproductlist;
import com.bnbneeds.app.model.dealer.order.PurchaseItemInfoResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.PurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.UpdatePurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.UpdatePurchaseListParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bnbneeds.android.vendor.activity.ConstantsActivity.pDialog;

public class PurchaselistBuyerwise extends BaseActivity {
    String buyername;
    String buyerid;
    String requestdate;

    FloatingActionButton updatepurchaselist;
    Map<String, String> queryParams = new HashMap<>();

    List<Rowproductlist> item;

    ArrayList<String> productlist_Name = new ArrayList<>();
    ArrayList<Double> requestedqtylist = new ArrayList<>();
    ArrayList<String> unitslist = new ArrayList<>();
    ArrayList<String> postedlist = new ArrayList<>();
    ArrayList<String> purchaseitemlist_id = new ArrayList<>();
    ArrayList<Double> deliveredqtylist = new ArrayList<>();
    ArrayList<String> buyerlist_name = new ArrayList<>();
    ArrayList<String> vendorlist_id = new ArrayList<>();

    String[] buyerstring;
    String[] vendoridstring;
    String[] purchaselistidstring;
    String[] productnamestring;
    String[] unitsliststring;
    String[] productidliststring;
    String[] postedliststring;
    Double[] requestedqtyliststring;
    Double[] deliveredqtyliststring;


    private String strunit, strPurchaseItemId, strvendorId;
    Double strDeliveredQuantity;
    Double strRequiredQuantity;
    private String strupdatedStatus = "DELIVERED";
    private String strcomments = "General Comments";
    ListView productList;
    List<Rowproductlist> productItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchaselist_buyerwise);
        productList = (ListView) findViewById(R.id.product_listview);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        buyername = b.getString("selectedname");
        buyerid = b.getString("buyerid");
        requestdate = b.getString("requestdate");

        updatepurchaselist = (FloatingActionButton) findViewById(R.id.updatePlist);

        new PurchaselistBuyerwiseTask().execute();

        updatepurchaselist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UpdatePurchaselistBuyerwiseTask().execute();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent home = new Intent(this, HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }

    class PurchaselistBuyerwiseTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PurchaselistBuyerwise.this);
            pDialog.setMessage("Please Wait....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {

            queryParams.put("buyerId", buyerid);
            queryParams.put("requestDate", requestdate);

            PurchaseListResponse listResponse = client.vendors(LoginActivity.RELATIONSHIP_ID).getPurchaseList(100, queryParams);
            if (listResponse != null) {
                List<PurchaseItemInfoResponse> purchaseList = listResponse.getPurchaseItems();
                if (purchaseList != null && !purchaseList.isEmpty()) {
                    for (PurchaseItemInfoResponse item : purchaseList) {
                        productlist_Name.add(item.getProductName().getName());
                        purchaseitemlist_id.add(item.getId());
                        postedlist.add(item.getRequestDate());
                        requestedqtylist.add(item.getQuantity().getRequired());
                        unitslist.add(item.getQuantity().getUnit());
                        buyerlist_name.add(item.getBuyer().getName());
                        deliveredqtylist.add(item.getQuantity().getDelivered());
                        vendorlist_id.add(item.getVendor().getId());

                        purchaselistidstring = purchaseitemlist_id.toArray(new String[purchaseitemlist_id.size()]);
                        productnamestring = productlist_Name.toArray(new String[productlist_Name.size()]);
                        unitsliststring = unitslist.toArray(new String[unitslist.size()]);
                        productidliststring = purchaseitemlist_id.toArray(new String[purchaseitemlist_id.size()]);
                        postedliststring = postedlist.toArray(new String[postedlist.size()]);
                        requestedqtyliststring = requestedqtylist.toArray(new Double[requestedqtylist.size()]);
                        buyerstring = buyerlist_name.toArray(new String[buyerlist_name.size()]);
                        deliveredqtyliststring = deliveredqtylist.toArray(new Double[deliveredqtylist.size()]);
                        vendoridstring = vendorlist_id.toArray(new String[vendorlist_id.size()]);
                    }
                }
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            productItems = new ArrayList<>();
            for (int i = 0; i < productnamestring.length; i++) {
                Rowproductlist item = new Rowproductlist(productnamestring[i], buyerstring[i], vendoridstring[i], unitsliststring[i], postedliststring[i], deliveredqtyliststring[i], requestedqtyliststring[i], productidliststring[i], purchaselistidstring[i]);
                productItems.add(item);
            }




            pDialog.dismiss();
        }
    }

    class UpdatePurchaselistBuyerwiseTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PurchaselistBuyerwise.this);
            pDialog.setMessage("Please Wait....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            UpdatePurchaseListParam listparam = new UpdatePurchaseListParam();

            PurchaseItemParam.PurchaseStatus status = new PurchaseItemParam.PurchaseStatus();
            for (int i = 0; i < requestedqtylist.size(); i++) {
                UpdatePurchaseItemParam param = new UpdatePurchaseItemParam();
                strRequiredQuantity = requestedqtylist.get(i);
                strunit = unitslist.get(i);
                strPurchaseItemId = purchaseitemlist_id.get(i);
                // strDeliveredQuantity = deliveredqtylist.get(i);
                strvendorId = vendorlist_id.get(i);
                status.setComments(strcomments);
                status.setStatus(strupdatedStatus);

                param.setPurchaseItemId(strPurchaseItemId);
                param.setDeliveredQuantity(strRequiredQuantity);
                param.setUnit(strunit);
                param.setStatusUpdate(status);
                listparam.add(param);
            }





            client.vendors().updatePurchaseList(LoginActivity.RELATIONSHIP_ID, listparam);
            /*UpdatePurchaseItemParam param = new UpdatePurchaseItemParam();

            PurchaseItemParam.PurchaseStatus statusUpdate = new PurchaseItemParam.PurchaseStatus();
            statusUpdate.setComments(strcomments);
            statusUpdate.setStatus(strupdatedStatus);

             for(int i=0;i<productlist_Name.size();i++){
                 strRequiredQuantity = requestedqtylist.get(i);
                 strunit = unitslist.get(i);
                 strPurchaseItemId = purchaseitemlist_id.get(i);
                 strDeliveredQuantity = deliveredqtylist.get(i);
                 strvendorId = vendorlist_id.get(i);

            param.setRequiredQuantity(strRequiredQuantity);
            param.setUnit(strunit);
            param.setPurchaseItemId(strPurchaseItemId);
            param.setDeliveredQuantity(strDeliveredQuantity);
            param.setStatusUpdate(statusUpdate);
            UpdatePurchaseListParam list = new UpdatePurchaseListParam();
            list.add(param);
            client.vendors().updatePurchaseList(LoginActivity.RELATIONSHIP_ID,list);

             }*/
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Purchase List has been Updated Successfully!", Toast.LENGTH_LONG).show();
            Intent home = new Intent(PurchaselistBuyerwise.this, HomeActivity.class);
            home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(home);

        }
    }
}