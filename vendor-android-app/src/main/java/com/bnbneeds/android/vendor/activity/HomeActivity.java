package com.bnbneeds.android.vendor.activity;

import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnbneeds.android.vendor.AppRater.AppRater;
import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.client.BNBNeedsClientSingleton;
import com.bnbneeds.android.vendor.util.Utils;
import com.bnbneeds.rest.client.BNBNeedsClient;

import java.util.Timer;
import java.util.TimerTask;

import static com.bnbneeds.android.vendor.activity.BaseActivity.timer;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected BNBNeedsClient client;
    protected static String youtube = "uBUYK8-jgOQ";
    protected static String product_youtube = "UZcczrR7RCQ";
    protected static final String DEFAULT_TARGET_URI = "market://details?id=%s";
    public HomeActivity() {
        client = BNBNeedsClientSingleton.getInstance();
    }

    protected static boolean status;
    int mNotificationsCount = 0;
    TextView user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        AppRater apprater = new AppRater(this);
        apprater.show();
        if (status == false) {
            new FetchCountTask().execute();

        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView aggregateplist = (ImageView) findViewById(R.id.aggregate);
        ImageView buyersearch = (ImageView) findViewById(R.id.buyer);
        ImageView enquirylist = (ImageView) findViewById(R.id.enquirylist);
        ImageView buyerplist = (ImageView) findViewById(R.id.buyerplist);



        buyerplist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent purchaselist = new Intent(HomeActivity.this, AssociatedBuyerPurchaselist.class);
                startActivity(purchaselist);
            }
        });

        aggregateplist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aggregated = new Intent(HomeActivity.this, PurchaselistAggregated.class);
                startActivity(aggregated);
            }
        });
        enquirylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent buyerenq = new Intent(HomeActivity.this, BuyerEnquirylist.class);
                startActivity(buyerenq);
            }
        });
        buyersearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent buyer = new Intent(HomeActivity.this, BuyerlistActivity.class);
                startActivity(buyer);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, InformationActivity.class));
                Snackbar.make(view, "About BNBneeds", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        user = (TextView)headerView.findViewById(R.id.user);
        user.setText(LoginActivity.USER);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        timer = new Timer();
        Log.i("Main", "Invoking logout timer");
        LogOutTimerTask logoutTimeTask = new LogOutTimerTask();
        timer.schedule(logoutTimeTask, 3300000); // 55*60*1000 auto logout in 55 minutes
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (timer != null) {
            timer.cancel();
            Log.i("Main", "cancel timer");
            timer = null;
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        MenuItem item = menu.findItem(R.id.action_notify);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable

        Utils.setBadgeCount(this, icon, mNotificationsCount);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }*/
        if (id == R.id.action_notify) {
            status = true;
            Intent list = new Intent(HomeActivity.this, NotificationActivity.class);
            startActivity(list);
        }
        return super.onOptionsItemSelected(item);
    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.findbuyer) {

            Intent buyer = new Intent(HomeActivity.this, BuyerlistActivity.class);
            startActivity(buyer);

        } else if (id == R.id.purchaselist) {

            Intent aggregated = new Intent(HomeActivity.this, PurchaselistAggregated.class);
            startActivity(aggregated);

        } else if (id == R.id.agg_purchaselist) {
            Intent purchaselist = new Intent(HomeActivity.this, AssociatedBuyerPurchaselist.class);
            startActivity(purchaselist);

        } else if (id == R.id.proenquiry) {

            Intent buyerenq = new Intent(HomeActivity.this, BuyerEnquirylist.class);
            startActivity(buyerenq);

        } else if (id == R.id.usagevideo) {

            Intent usagevideo = new Intent(HomeActivity.this, VideoActivity.class);
            startActivity(usagevideo);

        }else if (id == R.id.addproductvideo) {

            Intent addproductvideo = new Intent(HomeActivity.this, ProductAddVideo.class);
            startActivity(addproductvideo);

        }
        else if (id == R.id.abuse) {

            Intent buyer = new Intent(HomeActivity.this, BuyerlistActivity.class);
            startActivity(buyer);
            Toast.makeText(getApplicationContext(), "Select Buyers from the list to Report", Toast.LENGTH_LONG).show();

        }else if(id == R.id.rateus) {
            Intent rateintent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format(DEFAULT_TARGET_URI,getPackageName())));
            startActivity(rateintent);

        }else if (id == R.id.logout) {
            new LogoutTask().execute();
            //client.auth().logout();
            Toast.makeText(this, "You have been Successfully Logged Out", Toast.LENGTH_LONG).show();
            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);
            System.exit(0);
        } else if (id == R.id.chngpswd) {

            Intent cp = new Intent(HomeActivity.this, ChangePasswordActivity.class);
            startActivity(cp);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    class LogOutTimerTask extends TimerTask {


        @Override
        public void run() {
            client.logout();
            //redirect user to login screen
            Intent i = new Intent(HomeActivity.this, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            return;
        }
    }

    /*
   Updates the count of notifications in the ActionBar.
    */
    private void updateNotificationsBadge(int count) {
        mNotificationsCount = count;

        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        invalidateOptionsMenu();
    }

    class FetchCountTask extends AsyncTask<Void, Void, Integer> {


        @Override
        protected Integer doInBackground(Void... params) {
            // example count. This is where you'd
            // query your data store for the actual count.


            return 1;

        }

        @Override
        protected void onPostExecute(Integer integer) {
            updateNotificationsBadge(1);
            super.onPostExecute(integer);
        }
    }

    class LogoutTask extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... params) {


            client.logout();



            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


        }
    }
}
