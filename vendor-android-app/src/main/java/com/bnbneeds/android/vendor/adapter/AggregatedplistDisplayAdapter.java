package com.bnbneeds.android.vendor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bnbneeds.android.vendor.R;

import java.util.ArrayList;

/**
 * Created by Len on 28-04-2017.
 */

public class AggregatedplistDisplayAdapter extends BaseAdapter {


        private Context mContext;
    private ArrayList<String> id;
        private ArrayList<String> productName;
        private ArrayList<Double> quantity;
        private ArrayList<String> remarks;



        public AggregatedplistDisplayAdapter(Context c,ArrayList<String> id,ArrayList<String> pname, ArrayList<String> remarks,ArrayList<Double> qty) {
            this.mContext = c;

            this.id = id;
            this.productName = pname;
            this.remarks = remarks;
            this.quantity = qty;

        }




        public int getCount() {
            // TODO Auto-generated method stub
            return id.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        public View getView(int pos, View child, ViewGroup parent) {
            Holder mHolder;
            LayoutInflater layoutInflater;
            if (child == null) {
                layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                child = layoutInflater.inflate(R.layout.listcell, null);
                mHolder = new Holder();

                mHolder.textViewPname = (TextView) child.findViewById(R.id.textViewPname);
                mHolder.textViewQty = (TextView) child.findViewById(R.id.textViewQty);
                mHolder.textViewRemarks = (TextView)child.findViewById(R.id.textRemarks);

                child.setTag(mHolder);
            } else {
                mHolder = (Holder) child.getTag();
            }

             mHolder.textViewPname.setText(productName.get(pos));
            mHolder.textViewRemarks.setText(remarks.get(pos));
            mHolder.textViewQty.setText(String.valueOf(quantity.get(pos)));
            return child;
        }



        public class Holder {
            TextView textViewPname;
            TextView textViewQty;
            TextView textViewRemarks;
        }

    }



