package com.bnbneeds.android.vendor.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.adapter.CustomReviewAdapter;
import com.bnbneeds.android.vendor.bean.RowItemReview;
import com.bnbneeds.android.vendor.util.CheckConnectivity;
import com.bnbneeds.app.model.review.AverageRatingResponse;
import com.bnbneeds.app.model.review.ReviewInfoListResponse;
import com.bnbneeds.app.model.review.ReviewInfoResponse;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.bnbneeds.android.vendor.activity.BuyerlistActivity.buyerNameid;
import static com.bnbneeds.android.vendor.activity.ConstantsActivity.pDialog;

public class ViewBuyerReviewsActivity extends BaseActivity {

    private ListView lv;
    List<RowItemReview> rowItemsReview;
    CustomReviewAdapter adapter;

    ArrayList<Float> ratinglist = new ArrayList<>();
    ArrayList<String> headlinelist = new ArrayList<>();
    ArrayList<String> commentslist = new ArrayList<>();
    ArrayList<String> reviewedlist = new ArrayList<>();
    ArrayList<String> timestamplist = new ArrayList<>();
    ArrayList<BarEntry> averageRating = new ArrayList<>();

    ArrayList<String> labels = new ArrayList<String>();
    BarChart barChart;


    String[] headlinestring;
    String[] commentstring;
    String[] reviewedstring;
    String[] timestampstring;
    Float[] ratingfloat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_buyer_review);

        barChart = (BarChart) findViewById(R.id.chart);
        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if (conn == true) {
            new ViewVendorReviewTask().execute();
        } else {
            Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    public static final int[] JOYFUL_COLORS = {
            Color.rgb(217, 80, 138), Color.rgb(254, 149, 7), Color.rgb(106, 150, 31),
            Color.rgb(106, 167, 134), Color.rgb(53, 194, 209)
    };

    class ViewVendorReviewTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            setDialog();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            AverageRatingResponse averagerating = client.buyerReviews(buyerNameid).getAverageRating();
            double five = averagerating.getFiveStarPercent();
            float fivestar = (float) five;
            averageRating.add(new BarEntry(fivestar, 4));
            double four = averagerating.getFourStarPercent();
            float fourstar = (float) four;
            averageRating.add(new BarEntry(fourstar, 3));
            double three = averagerating.getThreeStarPercent();
            float threestar = (float) three;
            averageRating.add(new BarEntry(threestar, 2));
            double two = averagerating.getTwoStarPercent();
            float twostar = (float) two;
            averageRating.add(new BarEntry(twostar, 1));
            double one = averagerating.getOneStarPercent();
            float onestar = (float) one;
            averageRating.add(new BarEntry(onestar, 0));


            ReviewInfoListResponse listresponse = client.buyerReviews(buyerNameid).getReviews();
            if (listresponse != null) {
                List<ReviewInfoResponse> resp = listresponse.getReviews();
                if (resp != null && !resp.isEmpty()) {
                    for (Iterator<ReviewInfoResponse> iterator = resp.iterator(); iterator.hasNext(); ) {
                        ReviewInfoResponse reviewResp = iterator.next();

                        ratinglist.add((float) reviewResp.getRating());
                        headlinelist.add(reviewResp.getHeadline());
                        commentslist.add(reviewResp.getDescription());
                        reviewedlist.add(reviewResp.getReviewedBy().getName());
                        timestamplist.add(reviewResp.getTimestamp());


                        ratingfloat = ratinglist.toArray(new Float[ratinglist.size()]);
                        headlinestring = headlinelist.toArray(new String[headlinelist.size()]);
                        commentstring = commentslist.toArray(new String[commentslist.size()]);
                        reviewedstring = reviewedlist.toArray(new String[reviewedlist.size()]);
                        timestampstring = timestamplist.toArray(new String[timestamplist.size()]);
                    }
                } else {
                    Intent home = new Intent(ViewBuyerReviewsActivity.this, HomeActivity.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(home);

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();

            labels.add("1 Star");
            labels.add("2 Star");
            labels.add("3 Star");
            labels.add("4 Star");
            labels.add("5 Star");
            BarDataSet dataset = new BarDataSet(averageRating, "# of Ratings %");
            BarData data = new BarData(labels, dataset);
            // dataset.setColors(ColorTemplate.LIBERTY_COLORS); //
            dataset.setColors(JOYFUL_COLORS);
            barChart.setData(data);
            barChart.animateY(5000);

            barChart.setDescription("");    // Hide the description
            barChart.getAxisLeft().setDrawLabels(false);
            barChart.getAxisRight().setDrawLabels(false);
            //barChart.getLegend().setEnabled(false);

            // to remove grid lines x and y axis
            barChart.getXAxis().setDrawGridLines(false);
            barChart.getAxisLeft().setDrawGridLines(false);
            barChart.getAxisRight().setDrawGridLines(false);


            if (headlinestring != null) {
                rowItemsReview = new ArrayList<RowItemReview>();
                for (int i = 0; i < headlinestring.length; i++) {
                    RowItemReview item = new RowItemReview(ratingfloat[i], headlinestring[i], commentstring[i], reviewedstring[i], timestampstring[i]);
                    rowItemsReview.add(item);
                }

                lv = (ListView) findViewById(R.id.list_review);

                adapter = new CustomReviewAdapter(ViewBuyerReviewsActivity.this, R.layout.reviewlist, rowItemsReview);
                lv.setAdapter(adapter);
            } else {
                Toast.makeText(getApplicationContext(), "No Reviews", Toast.LENGTH_LONG).show();
                Intent home = new Intent(ViewBuyerReviewsActivity.this, BuyerdetailsActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);


            }
        }
    }
}
