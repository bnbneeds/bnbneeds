package com.bnbneeds.android.vendor.bean;

/**
 * Created by Len on 09-12-2016.
 */

public class RowItem {
   //private int imageId;
    private String productname;
    private String buyername;
    private String comments;
    private String postedon;
    private String reqty;
    private String likelytobuy;
    private String likelytodays;
   private String buyerenquiryid;




    public RowItem(String productname, String buyerenquiryid, String buyername, String comments, String postedon, String reqty, String likelytobuy, String likelytodays) {
       // this.imageId = imageId;
        this.productname = productname;
        this.buyername = buyername;
        this.comments = comments;
        this.postedon = postedon;
        this.reqty = reqty;
        this.likelytobuy = likelytobuy;
        this.likelytodays = likelytodays;
        this.buyerenquiryid = buyerenquiryid;

    }

    public String getLikelytodays() {
        return likelytodays;
    }

    public void setLikelytodays(String likelytodays) {
        this.likelytodays = likelytodays;
    }
   /* public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }*/

    public String getBuyerenquiryid() {
        return buyerenquiryid;
    }

    public void setBuyerenquiryid(String buyerenquiryid) {
        this.buyerenquiryid = buyerenquiryid;
    }

    /*public String getBuyerids() {
        return buyerid;
    }


    public void setBuyerid(String buyerid) {
        this.buyerid = buyerid;
    }*/

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }



    public String getBuyername() {
        return buyername;
    }

    public void setBuyername(String buyername) {
        this.buyername = buyername;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getPostedon() {
        return postedon;
    }

    public void setPostedon(String postedon) {
        this.postedon = postedon;
    }

    public String getReqty() {
        return reqty;
    }

    public void setReqty(String reqty) {
        this.reqty = reqty;
    }

    public String getLikelytobuy() {
        return likelytobuy;
    }

    public void setLikelytobuy(String likelytobuy) {
        this.likelytobuy = likelytobuy;
    }

    @Override
    public String toString() {
        return productname + "\n" + buyerenquiryid;
    }
}
