package com.bnbneeds.android.vendor.activity;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.db.DbHelper;

public class UpdateActivity extends BaseActivity {

    String ID, reqqty, delqty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        // Read var from Intent
        Intent intent = getIntent();
        ID = intent.getStringExtra("id");
        reqqty = intent.getStringExtra("reqqty");
        delqty = intent.getStringExtra("delqty");

        TextView tMemberID = (TextView) findViewById(R.id.txtMemberID);
        TextView tName = (TextView) findViewById(R.id.txtName);
        EditText tTel = (EditText) findViewById(R.id.txtTel);

        tMemberID.setText(ID);
        tName.setText(reqqty);
        tTel.setText(delqty);

        // Show Data
        // ShowData(ID);

        // btnSave (Save)
        final Button save = (Button) findViewById(R.id.btnSave);
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // If Save Complete
                if (UpdateData(ID)) {
                    // Open Form ListUpdate
                    Intent newActivity = new Intent(UpdateActivity.this, UpdatePurchaselistBuyerwiseActivity.class);
                    startActivity(newActivity);
                }
            }
        });

        // btnCancel (Cancel)
        final Button cancel = (Button) findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Open Form ListUpdate
                Intent newActivity = new Intent(UpdateActivity.this, UpdatePurchaselistBuyerwiseActivity.class);
                startActivity(newActivity);
            }
        });

    }


    public boolean UpdateData(String ID) {

        // txtName, txtTel

        final EditText tTel = (EditText) findViewById(R.id.txtTel);

        // Dialog
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        AlertDialog ad = adb.create();


        // Check Tel
        if (tTel.getText().length() == 0) {
            ad.setMessage("Please Input [Qty] ");
            ad.show();
            tTel.requestFocus();
            return false;
        }

        // new Class DB
        final DbHelper myDb = new DbHelper(this);

        // Save Data
        long saveStatus = myDb.UpdateData(ID, tTel.getText().toString());
        if (saveStatus <= 0) {
            ad.setMessage("Error!! ");
            ad.show();
            return false;
        }

        Toast.makeText(UpdateActivity.this, "Update Data Successfully. ",
                Toast.LENGTH_SHORT).show();

        return true;
    }
}
