package com.bnbneeds.android.vendor.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.util.CheckConnectivity;
import com.bnbneeds.app.model.dealer.BuyerInfoResponse;
import com.bnbneeds.app.model.report.CreateReportParam;
import com.bnbneeds.app.model.review.AverageRatingResponse;
import com.getbase.floatingactionbutton.FloatingActionButton;

import org.springframework.web.client.ResourceAccessException;


import static com.bnbneeds.android.vendor.activity.BuyerlistActivity.buyerNameid;

public class BuyerdetailsActivity extends ConstantsActivity {

   /* private String buyername;
    private String buyerid;*/
    final Context context = this;
    private String description, orgname, contactperson, mobilenum, email, address, website;

    TextView organization, Contact, mobnum, emailid, desc, web, addr,rating;
    Button Back;
    EditText title, abusedesc;
    private String abuseTitle;
    private String abuseDescription;
    private String connection;
    RatingBar simplerating;
    TextView viewreview;
    //double ratevalue = 4.9;
    float vendorRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyerdetails);

       /* Intent intent = getIntent();
        Bundle b = intent.getExtras();
        buyername = b.getString("selectedname");
        buyerid = b.getString("buyerid");*/


        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if (conn == true) {
            new BuyerDetailsTask().execute();
        } else {
            Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }

        FloatingActionButton abusereport = (FloatingActionButton) findViewById(R.id.reportabuse);
        FloatingActionButton reviewvendor = (FloatingActionButton) findViewById(R.id.vendorreview);
        // Back = (Button)findViewById(R.id.back_btn);
        simplerating = (RatingBar) findViewById(R.id.ratingBarIndicator);
        rating = (TextView)findViewById(R.id.ratingvalue);
        organization = (TextView) findViewById(R.id.vendorname);
        Contact = (TextView) findViewById(R.id.contactname);
        mobnum = (TextView) findViewById(R.id.mobno);
        emailid = (TextView) findViewById(R.id.email);
        desc = (TextView) findViewById(R.id.desc);
        web = (TextView) findViewById(R.id.web);
        addr = (TextView) findViewById(R.id.adress);
           addr.setMovementMethod(new ScrollingMovementMethod());
        viewreview = (TextView)findViewById(R.id.viewreview);
        viewreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent review = new Intent(BuyerdetailsActivity.this,ViewBuyerReviewsActivity.class);
                startActivity(review);
            }
        });
        /*Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(BuyerdetailsActivity.this,BuyerlistActivity.class);
                startActivity(back);
            }
        });*/

        abusereport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(context);
                final View view = li.inflate(R.layout.activity_abuse, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(BuyerdetailsActivity.this);
                builder.setTitle("Report Abuse");
                builder.setView(view);
                builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        title = (EditText) view.findViewById(R.id.title);
                        abusedesc = (EditText) view.findViewById(R.id.abusedesc);
                        abuseTitle = title.getText().toString();
                        abuseDescription = abusedesc.getText().toString();
                        if (!abuseTitle.isEmpty() && !abuseDescription.isEmpty()) {
                            new ReportabuseTask().execute();
                        } else {
                            Toast.makeText(getApplicationContext(), "Required Fields are Missing", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        reviewvendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent review = new Intent(BuyerdetailsActivity.this,BuyerReviewActivity.class);
                startActivity(review);
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent home = new Intent(this, HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }

    class BuyerDetailsTask extends AsyncTask<String, Void, String> {

        BuyerInfoResponse buyerinfo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {

            AverageRatingResponse averageratingresponse = client.buyerReviews(buyerNameid).getAverageRating();
            double a =  averageratingresponse.getAverageRating();
            vendorRating = (float)a;

            BuyerInfoResponse response = client.buyers(buyerNameid).get();
            orgname = response.getName();
            contactperson = response.getContactName();
            mobilenum = response.getMobileNumber();
            email = response.getEmailAddress();
            address = response.getAddress();
            website = response.getWebsite();
            description = response.getDescription();


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            simplerating.setRating(vendorRating);
            rating.setText(String.valueOf(vendorRating));
            organization.setText(orgname);
            Contact.setText(contactperson);
            mobnum.setText(mobilenum);
            emailid.setText(email);
            if (!(description == null)) {
                desc.setText(description);
            }


            web.setText(website);
            addr.setText(address);


        }
    }

    class ReportabuseTask extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {
            try {
                CreateReportParam param = new CreateReportParam();
                param.setTitle(abuseTitle);
                param.setEntityId(buyerNameid);
                param.setDescription(abuseDescription);
                client.abuseReports().create(param);
            } catch (ResourceAccessException a) {
                connection = a.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (connection == null) {
                Toast.makeText(getApplicationContext(), "Report Abuse has been Created Successfully", Toast.LENGTH_SHORT).show();

            } else {

                Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(s);
        }
    }


}
