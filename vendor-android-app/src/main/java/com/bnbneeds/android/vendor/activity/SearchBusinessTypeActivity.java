package com.bnbneeds.android.vendor.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.adapter.CustomBuyerAdapter;
import com.bnbneeds.android.vendor.bean.RowItemVendor;
import com.bnbneeds.android.vendor.util.CheckConnectivity;
import com.bnbneeds.app.model.dealer.BuyerInfoListResponse;
import com.bnbneeds.app.model.dealer.BuyerInfoResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.bnbneeds.android.vendor.activity.BuyerlistActivity.buyerNameid;
import static com.bnbneeds.android.vendor.activity.BuyerlistActivity.buyername;
import static com.bnbneeds.android.vendor.activity.FilterActivity.businessTypeId;

public class SearchBusinessTypeActivity extends ConstantsActivity {

    Map<String, String> queryMap = new HashMap<String, String>();
    private String buyer;
    List<RowItemVendor> rowItems;
    ArrayList<String> arrbuyer = new ArrayList<>();
    ArrayList<String> buyernameid = new ArrayList<>();
    ListView list;
    CustomBuyerAdapter listbuyer;
    String[] buyerstring;
    String[] buyeridstring;
    private static final int SEARCH_RESULT_SIZE = 1000;
    EditText inputSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_business_type);
        list = (ListView) findViewById(R.id.vendorlist);
        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if (conn == true) {
            new VendorList().execute();
        } else {
            Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    class VendorList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {

            queryMap.put("businessTypeId",businessTypeId);
            BuyerInfoListResponse listResponse = client.buyers().list(SEARCH_RESULT_SIZE,queryMap);
            if (listResponse != null) {
                List<BuyerInfoResponse> buyerlist = listResponse.getBuyerList();
                if (buyerlist != null && !buyerlist.isEmpty()) {
                    for (Iterator<BuyerInfoResponse> iterator = buyerlist.iterator(); iterator.hasNext(); ) {
                        BuyerInfoResponse buyerinfo = iterator.next();
                        buyer = buyerinfo.getName();

                        arrbuyer.add(buyer);
                        buyernameid.add(buyerinfo.getId());
                        buyerstring = arrbuyer.toArray(new String[arrbuyer.size()]);
                        buyeridstring = buyernameid.toArray(new String[buyernameid.size()]);

                    }
                } else {
                    Intent home = new Intent(SearchBusinessTypeActivity.this, BuyerlistActivity.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(home);
                    pDialog.dismiss();
                }


            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            if (buyerstring != null) {
                rowItems = new ArrayList<RowItemVendor>();
                for (int i = 0; i < buyerstring.length; i++) {
                    RowItemVendor item = new RowItemVendor(buyerstring[i], buyeridstring[i]);
                    rowItems.add(item);
                }

                listbuyer = new CustomBuyerAdapter(SearchBusinessTypeActivity.this, R.layout.vendorlist, rowItems);
                list.setAdapter(listbuyer);
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        /*String buyername = rowItems.get(position).getVendor();
                        String buyernameid = rowItems.get(position).getVendorid();*/

                        buyername = listbuyer.getItem(position).getVendor();
                        buyerNameid = listbuyer.getItem(position).getVendorid();

                        Intent intent = new Intent(SearchBusinessTypeActivity.this, BuyerdetailsActivity.class);
                       /* Bundle bundle = new Bundle();
                        bundle.putString("selectedname", buyername);
                        bundle.putString("buyerid", buyernameid);
                        intent.putExtras(bundle);*/
                        startActivity(intent);

                        // Toast.makeText(getApplicationContext(),"You selected "+vendornameid,Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                Toast.makeText(getApplicationContext(), "No data found for the search", Toast.LENGTH_LONG).show();
                Intent home = new Intent(SearchBusinessTypeActivity.this, BuyerlistActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
            inputSearch = (EditText) findViewById(R.id.inputproduct);
            if (inputSearch != null) {
                inputSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String text = inputSearch.getText().toString();
                        listbuyer.getFilter().filter(text);
                    }
                });
            } else {
                Intent home = new Intent(SearchBusinessTypeActivity.this, BuyerlistActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        }
    }
}
