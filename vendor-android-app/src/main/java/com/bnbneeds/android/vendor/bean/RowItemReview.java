package com.bnbneeds.android.vendor.bean;

/**
 * Created by Len on 30-06-2017.
 */

public class RowItemReview {

    private Float rating;
    private String headline;
    private String comments;
    private String reviewedby;
    private String timestamp;




    public RowItemReview(Float rating, String headline, String comments, String reviewedby, String timestamp) {
        // this.imageId = imageId;
        this.rating = rating;
        this.headline = headline;
        this.comments = comments;
        this.reviewedby = reviewedby;
        this.timestamp = timestamp;

    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getReviewedby() {
        return reviewedby;
    }

    public void setReviewedby(String reviewedby) {
        this.reviewedby = reviewedby;
    }


    @Override
    public String toString() {
        return headline + "\n" + comments;
    }
}
