package com.bnbneeds.android.vendor.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.adapter.CustomBuyerAdapter;
import com.bnbneeds.android.vendor.bean.RowItemVendor;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.dealer.BuyerListResponse;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

import org.springframework.web.client.ResourceAccessException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import static com.bnbneeds.android.vendor.activity.ConstantsActivity.pDialog;

public class AssociatedBuyerPurchaselist extends BaseActivity {

    private DatePickerDialog dpDialog;
    private SimpleDateFormat dateFormatter;
    ArrayList<String> buyerlist = new ArrayList<>();
    ArrayList<String> buyeridlist = new ArrayList<>();
    List<RowItemVendor> rowItems;
    ListView list;
    CustomBuyerAdapter listvendor;
    String[] buyerliststring;
    String[] buyeridstring;
    String date;
    EditText daterange;
    EditText inputSearch;
    private String connection,blacklist;
    int errcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_associated_buyer_purchaselist);
        daterange = (EditText) findViewById(R.id.daterange);
        Button okay = (Button) findViewById(R.id.ok);

        dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        daterange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == daterange) {
                    dpDialog.show();

                }


            }
        });
        Calendar newCalendar = Calendar.getInstance();
        dpDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                daterange.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                date = daterange.getText().toString();
                if (!date.isEmpty()) {
                    new BuyerPurchaselistTask().execute();
                } else {
                    Toast.makeText(getApplicationContext(), "Please Enter Date", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent home = new Intent(this, HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }

    class BuyerPurchaselistTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {

            try {

                BuyerListResponse plist = client.vendors().getAssociatedBuyerListOfPurchaseItemsByDateRange(LoginActivity.RELATIONSHIP_ID, date, date);
                if (plist != null) {
                    List<NamedRelatedResourceRep> responselist = plist.getBuyerList();
                    if (responselist != null && !responselist.isEmpty()) {
                        buyeridlist.clear();
                        buyerlist.clear();
                        for (Iterator<NamedRelatedResourceRep> iteratorblist = responselist.iterator(); iteratorblist.hasNext(); ) {
                            NamedRelatedResourceRep name = iteratorblist.next();
                            buyerlist.add(name.getName());
                            buyeridlist.add(name.getId());
                            buyerliststring = buyerlist.toArray(new String[buyerlist.size()]);
                            buyeridstring = buyeridlist.toArray(new String[buyeridlist.size()]);
                        }
                    }else {

                        Intent home = new Intent(AssociatedBuyerPurchaselist.this, HomeActivity.class);
                        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(home);

                    }
                }

            } catch (ResourceAccessException a) {
                connection = a.getMessage();
            }catch (BNBNeedsServiceException se){
                blacklist = se.getTask().getMessage();
                errcode = se.getHttpCode();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            if (connection == null) {
                if (errcode == 403) {
                    Toast.makeText(getApplicationContext(), blacklist, Toast.LENGTH_SHORT).show();
                }else{
                if (buyerliststring != null) {
                    rowItems = new ArrayList<RowItemVendor>();
                    for (int i = 0; i < buyerliststring.length; i++) {
                        RowItemVendor item = new RowItemVendor(buyerliststring[i], buyeridstring[i]);
                        rowItems.add(item);
                    }

                    listvendor = new CustomBuyerAdapter(AssociatedBuyerPurchaselist.this, R.layout.vendorlist, rowItems);
                    list = (ListView) findViewById(R.id.vendorlist);
                    list.setAdapter(listvendor);
                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String vendornamelist = listvendor.getItem(position).getVendor();
                            String vendornameidlist = listvendor.getItem(position).getVendorid();

                            Intent intent = new Intent(AssociatedBuyerPurchaselist.this, PurchaselistBuyerwiseActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("selectedname", vendornamelist);
                            bundle.putString("buyerid", vendornameidlist);
                            bundle.putString("requestdate", date);
                            intent.putExtras(bundle);
                            startActivity(intent);

                        }
                    });
                } else {
                    Toast.makeText(getApplicationContext(), "No data", Toast.LENGTH_LONG).show();
                    Intent home = new Intent(AssociatedBuyerPurchaselist.this, HomeActivity.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(home);
                }
                }
            } else {
                Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }
            inputSearch = (EditText) findViewById(R.id.searchpro);

            if (inputSearch != null) {
                inputSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String text = inputSearch.getText().toString();
                        listvendor.getFilter().filter(text);
                    }
                });

            } else {
                Intent home = new Intent(AssociatedBuyerPurchaselist.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        }
    }
}