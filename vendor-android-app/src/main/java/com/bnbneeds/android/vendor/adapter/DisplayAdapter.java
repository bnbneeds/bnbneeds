package com.bnbneeds.android.vendor.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.bean.RowItemAggregated;
import com.bnbneeds.android.vendor.bean.RowItemVendor;

import java.util.List;

/**
 * Created by Len on 07-02-2017.
 */

public class DisplayAdapter extends ArrayAdapter<RowItemAggregated> {

    Context context;

    public DisplayAdapter(Context context, int resourceId, List<RowItemAggregated> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        // ImageView imageView;
        TextView Pname;
        TextView Qty;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        RowItemAggregated rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listcell, null);
            holder = new ViewHolder();

            //  holder.imageView = (ImageView) convertView.findViewById(R.id.productImage);
            holder.Pname = (TextView) convertView.findViewById(R.id.textViewPname);
            holder.Qty = (TextView) convertView.findViewById(R.id.textViewQty);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();


        // holder.imageView.setImageResource(rowItem.getImageId());
        holder.Pname.setText(rowItem.getProductname());
        holder.Qty.setText(rowItem.getQuantity().toString());

        return convertView;
    }
}
