package com.bnbneeds.android.vendor.activity;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.adapter.AggregatedplistDisplayAdapter;
import com.bnbneeds.android.vendor.adapter.CustomBuyerAdapter;
import com.bnbneeds.android.vendor.adapter.DisplayAdapter;
import com.bnbneeds.android.vendor.adapter.PlistDisplayAdapter;
import com.bnbneeds.android.vendor.bean.RowItemAggregated;
import com.bnbneeds.android.vendor.bean.RowItemVendor;
import com.bnbneeds.android.vendor.db.DbHelper;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.dealer.BuyerListResponse;
import com.bnbneeds.app.model.dealer.order.AggregatedPurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemInfoResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseListResponse;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;


import org.springframework.web.client.ResourceAccessException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.bnbneeds.android.vendor.activity.ConstantsActivity.pDialog;

public class PurchaselistAggregated extends BaseActivity {


    Map<String, String> queryParams = new HashMap<>();
    private DatePickerDialog dpDialog;
    private SimpleDateFormat dateFormatter;
    DbHelper mHelper = new DbHelper(this);
    SQLiteDatabase dataBase;
    /* List<RowItemAggregated> rowItems;
     ArrayList<String> productlist_Name = new ArrayList<>();

     ArrayList<String> productlistid = new ArrayList<>();
     ArrayList<String> postedlist = new ArrayList<>();
     ArrayList<Double> reqqtylist = new ArrayList<>();*/
    private ArrayList<String> pId = new ArrayList<String>();
    ArrayList<String> productlist_Name = new ArrayList<>();
    ArrayList<Double> requestedqtylist = new ArrayList<>();
    ArrayList<String> unitslist = new ArrayList<>();
    ArrayList<String> remarks = new ArrayList<>();
    ArrayList<String> purchaseitemlist_id = new ArrayList<>();
    ArrayList<Double> deliveredqtylist = new ArrayList<>();
    ArrayList<String> buyerlist_name = new ArrayList<>();
    ArrayList<String> vendorlist_id = new ArrayList<>();


    private ArrayList<String> pName = new ArrayList<String>();
    private ArrayList<Double> pQuantity = new ArrayList<Double>();
    private ArrayList<String> pRemarks = new ArrayList<String>();

    String[] buyerstring;
    String[] vendoridstring;
    String[] purchaselistidstring;
    //String[] productnamestring;
    String[] unitsliststring;
    String[] productidliststring;
    String[] remarksliststring;
    Double[] requestedqtyliststring;
    Double[] deliveredqtyliststring;

    String[] productnamestring;

    String[] productidstring;
    String[] postedliststring;
    Double[] reqqtyliststring;

    ListView listaggregated;
    EditText daterange;
    Button okay;
    String date;
    String Buyer;
    private String connection,blacklist;
    int errcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchaselist_aggregated);
        daterange = (EditText) findViewById(R.id.daterange);
        okay = (Button) findViewById(R.id.aggok);
        listaggregated = (ListView) findViewById(R.id.Listagg);


        dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        daterange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == daterange) {
                    dpDialog.show();

                }


            }
        });
        Calendar newCalendar = Calendar.getInstance();
        dpDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                daterange.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                date = daterange.getText().toString();
                if (!date.isEmpty()) {
                    //  new AggregatedPurchaselistTask().execute();
                    new PurchaselistDatewiseTask().execute();
                } else {
                    Toast.makeText(getApplicationContext(), "Please Enter Date", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    class PurchaselistDatewiseTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();

        }

        @Override
        protected String doInBackground(String... params) {


            queryParams.put("requestDate", date);
try {

    PurchaseListResponse listResponse = client.vendors(LoginActivity.RELATIONSHIP_ID).getPurchaseList(1000, queryParams);
    if (listResponse != null) {
        List<PurchaseItemInfoResponse> purchaseList = listResponse.getPurchaseItems();
        if (purchaseList != null && !purchaseList.isEmpty()) {
            productlist_Name.clear();
            purchaseitemlist_id.clear();
            requestedqtylist.clear();
            unitslist.clear();
            deliveredqtylist.clear();
            remarks.clear();
            for (PurchaseItemInfoResponse item : purchaseList) {
                productlist_Name.add(item.getProductName().getName());
                purchaseitemlist_id.add(item.getId());
                requestedqtylist.add(item.getQuantity().getRequired());
                unitslist.add(item.getQuantity().getUnit());
                deliveredqtylist.add(item.getQuantity().getDelivered());
                remarks.add(item.getTags());
                purchaselistidstring = purchaseitemlist_id.toArray(new String[purchaseitemlist_id.size()]);
                productnamestring = productlist_Name.toArray(new String[productlist_Name.size()]);
                unitsliststring = unitslist.toArray(new String[unitslist.size()]);
                requestedqtyliststring = requestedqtylist.toArray(new Double[requestedqtylist.size()]);
                deliveredqtyliststring = deliveredqtylist.toArray(new Double[deliveredqtylist.size()]);
                remarksliststring = remarks.toArray(new String[remarks.size()]);

            }
        }
    }
}catch (ResourceAccessException a) {
    connection = a.getMessage();

}catch (BNBNeedsServiceException se){
    blacklist = se.getMessage();
    errcode = se.getHttpCode();
}

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            deletedata();
            if (errcode == 403) {
                Toast.makeText(getApplicationContext(), blacklist, Toast.LENGTH_SHORT).show();
            }else{
            if (productnamestring != null) {
                dataBase = mHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                for (int i = 0; i < productnamestring.length; i++) {
                    values.put(DbHelper.KEY_PURCHASEITEM_ID, purchaselistidstring[i]);
                    values.put(DbHelper.KEY_PRODUCTNAME, productnamestring[i]);
                    values.put(DbHelper.KEY_DElQUANTITY, requestedqtyliststring[i]);
                    values.put(DbHelper.KEY_REqQUANTITY, requestedqtyliststring[i]);
                    values.put(DbHelper.KEY_UNITS, unitsliststring[i]);
                    values.put(DbHelper.KEY_REMARKS, remarksliststring[i]);
                    Log.d("Inserted Values", values.toString());
                    dataBase.insert(DbHelper.TABLE_NAME, null, values);
                }


                dataBase.close();
            } else {
                Toast.makeText(getApplicationContext(), "No data", Toast.LENGTH_LONG).show();
                Intent home = new Intent(PurchaselistAggregated.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        }
            pDialog.dismiss();

            AlertDialog.Builder alert = new AlertDialog.Builder(PurchaselistAggregated.this);

            alert.setTitle("Aggregated Purchase List Datewise");
            alert.setMessage("Click Continue to get  Aggregated Purchase List Datewise");
            alert.setIcon(R.drawable.purchase_list);

            alert.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    displayData();
                }
            });

            alert.show();
        }
    }

    private void displayData() {
        dataBase = mHelper.getWritableDatabase();
        //String select = "SELECT * FROM ItemListTable";
        String select = "SELECT ID,PRODUCT_NAME,SUM(REqQUANTITY) As REqQUANTITY,REMARKS FROM ItemListTable group by PRODUCT_NAME,REMARKS";

        Cursor mCursor = dataBase.rawQuery(select, null);


        pName.clear();
        pQuantity.clear();
        pRemarks.clear();
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        if (mCursor.moveToFirst()) {
            do {
                pId.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_ID)));
                pName.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_PRODUCTNAME)));
                pQuantity.add(mCursor.getDouble(mCursor.getColumnIndex(DbHelper.KEY_REqQUANTITY)));
                pRemarks.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_REMARKS)));


            } while (mCursor.moveToNext());
        }

        AggregatedplistDisplayAdapter listaggregate = new AggregatedplistDisplayAdapter(PurchaselistAggregated.this, pId, pName, pRemarks, pQuantity);
        listaggregated.setAdapter(listaggregate);

        mCursor.close();
    }

/*

    class AggregatedPurchaselistTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {

            try {

                AggregatedPurchaseListResponse aggresponse = client.vendors(LoginActivity.RELATIONSHIP_ID).getAggregatedPurchaseListByDateRange(date, date, null, null);
                if (aggresponse != null) {
                    List<AggregatedPurchaseListResponse.AggregatedPuchaseItem> response = aggresponse.getItemList();
                    if (response != null && !response.isEmpty()) {
                        productlist_Name.clear();
                        reqqtylist.clear();
                        postedlist.clear();
                        for (Iterator<AggregatedPurchaseListResponse.AggregatedPuchaseItem> iteratorlist = response.iterator(); iteratorlist.hasNext(); ) {
                            AggregatedPurchaseListResponse.AggregatedPuchaseItem api = iteratorlist.next();
                            productlist_Name.add(api.getProductName().getName());
                            reqqtylist.add(api.getQuantity().getRequired());
                            for(int i = 0; i<=0;i++) {
                                postedlist.add(api.getBuyerList().get(i).getTags());

                            }
                            postedliststring = postedlist.toArray(new String[postedlist.size()]);
                            productnamestring = productlist_Name.toArray(new String[productlist_Name.size()]);
                            reqqtyliststring = reqqtylist.toArray(new Double[reqqtylist.size()]);

                        }

                    } else {

                        Intent home = new Intent(PurchaselistAggregated.this, HomeActivity.class);
                        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(home);

                    }
                }
            } catch (ResourceAccessException a) {
                connection = a.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            if (connection == null) {
                if (reqqtyliststring != null) {
                    rowItems = new ArrayList<RowItemAggregated>();
                    for (int i = 0; i < reqqtyliststring.length; i++) {
                        RowItemAggregated item = new RowItemAggregated(productnamestring[i], reqqtyliststring[i]);
                        rowItems.add(item);
                    }

                    DisplayAdapter listaggregate = new DisplayAdapter(PurchaselistAggregated.this, R.layout.listcell, rowItems);
                    listaggregated.setAdapter(listaggregate);
                    Toast.makeText(getApplicationContext(),Arrays.toString(postedliststring), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "No data", Toast.LENGTH_LONG).show();
                    Intent home = new Intent(PurchaselistAggregated.this, HomeActivity.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(home);
                }

            } else {
                Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }

        }
    }
*/


    @Override
    public void onBackPressed() {
        deletedata();
        Intent home = new Intent(this, HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(home);
        return;
    }

    private void deletedata() {
        dataBase = mHelper.getWritableDatabase();
        dataBase.delete(DbHelper.TABLE_NAME, null, null);
        dataBase.close();
    }
}