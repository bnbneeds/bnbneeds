package com.bnbneeds.android.vendor.bean;

/**
 * Created by Len on 14-02-2017.
 */

public class RowItemAggregated {
    String productname;
    Double quantity;

    public RowItemAggregated(String productname, Double quantity) {
        this.productname = productname;
        this.quantity = quantity;

    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return productname + "\n" + quantity;
    }
}

