package com.bnbneeds.android.vendor.activity;

import android.app.ProgressDialog;

public class ConstantsActivity extends BaseActivity {
    protected static final String ACCOUNT_TYPE = "BUYER";
    protected static final String INTERNET_CONNECTION_ERROR = "Check your internet connection";
    protected static final String ACCOUNT_CREATION_ERROR = "Not able to create account.Try again!";
    protected static final String LOGIN_ERROR = "Not a registered user please sign up first.";
    protected static final String EMAIL_ERROR_MESSAGE = "Invalid Email address";
    protected static final String WEBSITE_ERROR_MESSAGE = "Invalid website";

    protected static final String INVALID_DATA_TITLE = "Invalid Data.";
    protected static final String INVALID_DATA_ERROR_MESSAGE = "Please, Enter valid data.";
    private static String BUNDLE_USERNAME_KEY;
    protected static String BUNDLE_PASSWORD_KEY;
    protected static String BUNDLE_USERNAME_VALUE;
    protected static String BUNDLE_PASSWORD_VALUE;
    protected static String MOBILE_NO;
    protected static String USER;
    protected static String RELATIONSHIP_ID;

    protected static String ROLE;
    protected static String inputText;
    protected static ProgressDialog pDialog;

}
