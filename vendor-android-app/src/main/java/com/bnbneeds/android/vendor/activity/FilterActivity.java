package com.bnbneeds.android.vendor.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.adapter.CustomBuyerAdapter;
import com.bnbneeds.android.vendor.bean.RowItemVendor;
import com.bnbneeds.android.vendor.util.CheckConnectivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.bnbneeds.android.vendor.activity.ConstantsActivity.pDialog;

public class FilterActivity extends BaseActivity {

    private static final int SEARCH_RESULT_SIZE = 1000;
    List<RowItemVendor> rowItems;
    String businessTypejson,businessTYPE;
    ArrayList<String> businessType = new ArrayList<>();
    ArrayList<String> businessTypeid = new ArrayList<>();
    String[] businessTypestring;
    String[] businessTypeidstring;
    AutoCompleteTextView actvBusinessType;
    Button apply;
    protected static String businessTypeId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if (conn == true) {
            new BusinessTypeTask().execute();
        }
        actvBusinessType = (AutoCompleteTextView)findViewById(R.id.actvbusinesstype);
        apply = (Button)findViewById(R.id.applyfilter);
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(actvBusinessType.length()!=0) {
                    actvBusinessType.setText("");
                    Intent search = new Intent(FilterActivity.this, SearchBusinessTypeActivity.class);
                    startActivity(search);
                }else{
                    Toast.makeText(getApplicationContext(), "Business Type Field Should not leave Blank", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    class BusinessTypeTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {


            businessTypejson = client.businessTypes().listBulkAsString(SEARCH_RESULT_SIZE);
            try {
                businessType.clear();
                businessTypeid.clear();
                JSONObject json = new JSONObject(businessTypejson);
                JSONArray productarray = json.getJSONArray("entity");
                for (int i = 0; i < productarray.length(); i++) {
                    JSONObject obj = productarray.getJSONObject(i);
                    businessTypeid.add(obj.getString("id"));
                    businessType.add(obj.getString("name"));
                    businessTypestring = businessType.toArray(new String[businessType.size()]);
                    businessTypeidstring = businessTypeid.toArray(new String[businessTypeid.size()]);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            if (businessType != null) {
                rowItems = new ArrayList<RowItemVendor>();
                for (int i = 0; i < businessTypestring.length; i++) {
                    RowItemVendor item = new RowItemVendor(businessTypestring[i],businessTypeidstring[i]);
                    rowItems.add(item);
                }

                final CustomBuyerAdapter adptbusinesstype = new CustomBuyerAdapter(FilterActivity.this,android.R.layout.simple_list_item_1,rowItems);
                actvBusinessType.setAdapter(adptbusinesstype);
                actvBusinessType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        businessTYPE = adptbusinesstype.getItem(position).getVendor().toString();
                        actvBusinessType.setText(businessTYPE);

                        businessTypeId =  adptbusinesstype.getItem(position).getVendorid().toString();


                    }
                });

            }
        }
    }
}
