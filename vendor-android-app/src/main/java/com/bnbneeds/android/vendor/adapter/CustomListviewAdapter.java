package com.bnbneeds.android.vendor.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.bnbneeds.android.vendor.R;
import com.bnbneeds.android.vendor.bean.RowItem;

import java.util.List;

/**
 * Created by Len on 08-12-2016.
 */

public class CustomListviewAdapter extends ArrayAdapter<RowItem> {

    Context context;

    public CustomListviewAdapter(Context context, int resourceId,
                                 List<RowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        // ImageView imageView;
        TextView proname;
        TextView buyername;
        TextView comments;
        TextView reqqty;
        TextView likelytobuy;
        TextView postedon;
        TextView valuesin;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        RowItem rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem, null);
            holder = new ViewHolder();
            holder.proname = (TextView) convertView.findViewById(R.id.product_name);
            holder.buyername = (TextView) convertView.findViewById(R.id.buyeridname);
            holder.comments = (TextView) convertView.findViewById(R.id.comments);
            holder.reqqty = (TextView) convertView.findViewById(R.id.reqqty);
            holder.likelytobuy = (TextView) convertView.findViewById(R.id.likelytobuy);
            holder.postedon = (TextView) convertView.findViewById(R.id.postedon);
            holder.valuesin = (TextView) convertView.findViewById(R.id.values);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.proname.setText(rowItem.getProductname());
        holder.buyername.setText(rowItem.getBuyername());
        holder.comments.setText(rowItem.getComments());
        holder.reqqty.setText(rowItem.getReqty());
        holder.likelytobuy.setText(rowItem.getLikelytobuy());
        holder.postedon.setText(rowItem.getPostedon());
        holder.valuesin.setText(rowItem.getLikelytodays());

        return convertView;
    }
}