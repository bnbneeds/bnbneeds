package com.bnbneeds.android.vendor.bean;

/**
 * Created by Len on 23-02-2017.
 */

public class Rowproductlist {
    private String productname;
    private String buyername;
    private String units;
    private String postedon;
    private Double reqty;
    private Double delqty;
    private String productid;
    private String purchaselistid;
    private String vendorid;


    public Rowproductlist(String productname, String buyername, String vendorid, String units, String postedon, Double delqty, Double reqty, String productid, String purchaselistid) {

        this.productname = productname;
        this.buyername = buyername;
        this.units = units;
        this.postedon = postedon;
        this.reqty = reqty;
        this.delqty = delqty;
        this.productid = productid;
        this.vendorid = vendorid;
        this.purchaselistid = purchaselistid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getBuyername() {
        return buyername;
    }

    public void setBuyername(String buyername) {
        this.buyername = buyername;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getPostedon() {
        return postedon;
    }

    public void setPostedon(String postedon) {
        this.postedon = postedon;
    }

    public Double getReqty() {
        return reqty;
    }

    public void setReqty(Double reqty) {
        this.reqty = reqty;
    }

    public String getProductid() {
        return productid;
    }

    public Double getDelqty() {
        return delqty;
    }

    public void setDelqty(Double delqty) {
        this.delqty = delqty;
    }

    public String getVendorid() {
        return vendorid;
    }

    public void setVendorid(String vendorid) {
        this.vendorid = vendorid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getPurchaselistid() {
        return purchaselistid;
    }

    public void setPurchaselistid(String purchaselistid) {
        this.purchaselistid = purchaselistid;
    }

    @Override
    public String toString() {
        return productname + "\n" + units;
    }
}
