package com.bnbneeds.android.vendor.adapter;

/**
 * Created by Len on 02-03-2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;



import com.bnbneeds.android.vendor.R;


public class PlistDisplayAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<String> id;
    //private ArrayList<String> templateName;
    private ArrayList<String> productName;
    private ArrayList<Double> quantity;
    private ArrayList<String> units;
    private ArrayList<Double> delQunatity;
    private ArrayList<String> delunits;
    private ArrayList<String> remarks;



    public PlistDisplayAdapter(Context c, ArrayList<String> id, ArrayList<String> pname, ArrayList<String> remarks,ArrayList<Double> qty, ArrayList<String> units, ArrayList<Double> delQunatity, ArrayList<String> delunits) {
        this.mContext = c;

        this.id = id;
        //this.templateName=tName;
        this.productName = pname;
        this.remarks = remarks;
        this.quantity = qty;
        this.units = units;
        this.delQunatity = delQunatity;
        this.delunits = delunits;
    }




    public int getCount() {
        // TODO Auto-generated method stub
        return id.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public View getView(int pos, View child, ViewGroup parent) {
        Holder mHolder;
        LayoutInflater layoutInflater;
        if (child == null) {
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            child = layoutInflater.inflate(R.layout.listbuyerwise, null);
            mHolder = new Holder();
            mHolder.textViewId = (TextView) child.findViewById(R.id.textViewId);
            mHolder.textViewPname = (TextView) child.findViewById(R.id.textViewPname);
            mHolder.textViewQty = (TextView) child.findViewById(R.id.textViewQty);
            mHolder.textViewUnits = (TextView) child.findViewById(R.id.textViewUnits);
            mHolder.textViewDelqty = (TextView) child.findViewById(R.id.textViewDelqty);
            mHolder.textViewdelUnits = (TextView) child.findViewById(R.id.textViewDelUnits);
            mHolder.textViewRemarks = (TextView)child.findViewById(R.id.textViewRemarks);

            child.setTag(mHolder);
        } else {
            mHolder = (Holder) child.getTag();
        }
        mHolder.textViewId.setText(id.get(pos));
        mHolder.textViewPname.setText(productName.get(pos));
        mHolder.textViewRemarks.setText(remarks.get(pos));
        mHolder.textViewQty.setText(String.valueOf(quantity.get(pos)));
        mHolder.textViewUnits.setText(units.get(pos));
        mHolder.textViewDelqty.setText(String.valueOf(delQunatity.get(pos)));
        mHolder.textViewdelUnits.setText(units.get(pos));
        return child;
    }



    public class Holder {
        TextView textViewId;
        //TextView textViewTname;
        TextView textViewPname;
        TextView textViewQty;
        TextView textViewUnits;
        TextView textViewDelqty;
        TextView textViewdelUnits;
        TextView textViewRemarks;
    }

}

