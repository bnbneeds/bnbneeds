package com.bnbneeds.app.model.dealer.order;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.bnbneeds.app.model.DescribedDataObjectRestResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "purchase_item")
public class PurchaseItemInfoResponse extends DescribedDataObjectRestResponse {

	private NamedRelatedResourceRep productName;
	private NamedRelatedResourceRep buyer;
	private NamedRelatedResourceRep vendor;
	private String requestDate;
	private List<PurchaseStatus> statusUpdates;
	private Quantity quantity;
	private String tags;

	@XmlElement(name = "product_name")
	public NamedRelatedResourceRep getProductName() {
		return productName;
	}

	@XmlElementWrapper(name = "item_progress")
	@XmlElement(name = "status")
	@JsonProperty("status")
	public List<PurchaseStatus> getStatusUpdates() {
		return statusUpdates;
	}

	@XmlElement(name = "buyer")
	public NamedRelatedResourceRep getBuyer() {
		return buyer;
	}

	@XmlElement(name = "request_date")
	public String getRequestDate() {
		return requestDate;
	}

	@XmlElement(name = "vendor")
	public NamedRelatedResourceRep getVendor() {
		return vendor;
	}

	@XmlElement(name = "quantity")
	public Quantity getQuantity() {
		return quantity;
	}

	@XmlElement(name = "tags")
	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public void setQuantity(Quantity quantity) {
		this.quantity = quantity;
	}

	public void setVendor(String vendorId, String name) {
		this.vendor = new NamedRelatedResourceRep(new RelatedResourceRep(
				vendorId, new RestLinkRep("self",
						Endpoint.VENDOR_INFO.get(vendorId))), name);
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public void setProductName(String productNameId, String name) {
		this.productName = new NamedRelatedResourceRep(new RelatedResourceRep(
				productNameId, new RestLinkRep("self",
						Endpoint.PRODUCT_NAME_INFO.get(productNameId))), name);
	}

	public void setBuyer(String buyerId, String name) {
		this.buyer = new NamedRelatedResourceRep(new RelatedResourceRep(
				buyerId, new RestLinkRep("self",
						Endpoint.BUYER_INFO.get(buyerId))), name);
	}

	public void setStatusUpdates(List<PurchaseStatus> statusUpdates) {
		this.statusUpdates = statusUpdates;
	}

	public void setProductName(NamedRelatedResourceRep productName) {
		this.productName = productName;
	}

	public void setBuyer(NamedRelatedResourceRep buyer) {
		this.buyer = buyer;
	}

	public void setVendor(NamedRelatedResourceRep vendor) {
		this.vendor = vendor;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PurchaseItemInfoResponse [");
		builder.append(super.toString());
		if (productName != null)
			builder.append("productName=").append(productName).append(", ");
		if (quantity != null)
			builder.append("quantity=").append(quantity).append(", ");
		if (statusUpdates != null)
			builder.append("statusUpdates=").append(statusUpdates);

		builder.append("]");
		return builder.toString();
	}

	@XmlRootElement
	@XmlType(propOrder = { "timestamp", "state", "comments", "updatedBy" })
	public static class PurchaseStatus {

		private String timestamp;
		private String state;
		private String comments;
		private String updatedBy;

		@XmlElement(name = "timestamp")
		public String getTimestamp() {
			return timestamp;
		}

		@XmlElement(name = "state")
		public String getState() {
			return state;
		}

		@XmlElement(name = "comments")
		public String getComments() {
			return comments;
		}

		@XmlElement(name = "updated_by")
		public String getUpdatedBy() {
			return updatedBy;
		}

		public void setTimestamp(String timestamp) {
			this.timestamp = timestamp;
		}

		public void setState(String state) {
			this.state = state;
		}

		public void setComments(String comments) {
			this.comments = comments;
		}

		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("PurchaseStatus [");
			if (timestamp != null)
				builder.append("timestamp=").append(timestamp).append(", ");
			if (state != null)
				builder.append("state=").append(state).append(", ");
			if (comments != null)
				builder.append("comments=").append(comments).append(", ");
			if (updatedBy != null)
				builder.append("updatedBy=").append(updatedBy);
			builder.append("]");
			return builder.toString();
		}

	}

	@Override
	protected void setLink() {
		/*
		 * This is intentionally left blank. Link is set based on buyer or
		 * vendor resource respectively
		 */
	}
}
