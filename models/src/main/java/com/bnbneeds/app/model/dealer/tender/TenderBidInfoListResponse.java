package com.bnbneeds.app.model.dealer.tender;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectInfoListResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "tender_bid_list")
public class TenderBidInfoListResponse extends DataObjectInfoListResponse<TenderBidInfoResponse> {

	@Override
	@XmlElementWrapper(name = "tender_bids")
	@XmlElement(name = "bid")
	@JsonProperty("bid")
	public List<TenderBidInfoResponse> getList() {
		return super.getList();
	}

}
