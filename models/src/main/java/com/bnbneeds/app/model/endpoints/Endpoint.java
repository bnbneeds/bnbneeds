package com.bnbneeds.app.model.endpoints;

import java.net.URI;
import java.text.MessageFormat;

public enum Endpoint {
	
	USER_ACCOUNTS("/user-accounts"),
	USER_ACCOUNT_TOKEN("/user-accounts/token"),
	USER_ACCOUNT_LOGIN("/user-accounts/login"),
	USER_ACCOUNT_LOGOUT("/user-accounts/logout"),
	USER_ACCOUNT_PASSWORDS("/user-accounts/passwords"),
	USER_ACCOUNT_PASSWORD_RESET("/user-accounts/{0}/passwords"),
	USER_ACCOUNT_VERIFICATIONS("/user-accounts/{0}/verifications"),
	USER_ACCOUNT_INFO("/user-accounts/{0}"),
	
	VENDORS("/vendors"),
	VENDORS_BULK("/vendors/bulk"),
	VENDOR_INFO("/vendors/{0}"),
	VENDOR_DEACTIVATE("/vendors/{0}/deactivate"),
	
	PRODUCT_NAMES("/product-names"),
	PRODUCT_NAMES_BULK("/product-names/bulk"),
	PRODUCT_NAME_INFO("/product-names/{0}"),
	PRODUCT_NAME_IMAGES("/product-names/{0}/images"),
	PRODUCT_NAME_IMAGE("/product-names/{0}/images/{1}"),
	PRODUCT_NAME_IMAGE_UPLOAD_FORM("/product-names/{0}/upload-image-form"),
	
	PRODUCT_CATEGORIES("/product-categories"),
	PRODUCT_CATEGORIES_BULK("/product-categories/bulk"),
	PRODUCT_CATEGORY_INFO("/product-categories/{0}"),
	
	PRODUCT_TYPES("/product-types"),
	PRODUCT_TYPES_BULK("/product-types/bulk"),
	PRODUCT_TYPE_INFO("/product-types/{0}"),
	
	BUSINESS_TYPES("/business-types"),
	BUSINESS_TYPES_BULK("/business-types/bulk"),
	BUSINESS_TYPE_INFO("/business-types/{0}"),
	
	VENDOR_PRODUCTS("/vendors/{0}/products"),
	VENDOR_PRODUCT_INFO("/vendors/{0}/products/{1}"),
	VENDOR_PRODUCT_DEACTIVATE("/vendors/{0}/products/{1}/deactivate"),
	VENDOR_PRODUCT_IMAGE_UPLOAD_FORM("/vendors/{0}/products/{1}/upload-image-form"),
	VENDOR_PRODUCT_IMAGES("/vendors/{0}/products/{1}/images"),
	VENDOR_PRODUCT_IMAGE("/vendors/{0}/products/{1}/images/{2}"),
	
	PRODUCTS("/products"),
	PRODUCT_INFO("/products/{0}"),
	PRODUCT_IMAGES("/products/{0}/images"),
	PRODUCT_IMAGE("/products/{0}/images/{1}"),
	
	BUYERS("/buyers"),
	BUYERS_BULK("/buyers/bulk"),
	BUYER_INFO("/buyers/{0}"),
	BUYER_DEACTIVATE("/buyers/{0}/deactivate"),
	BUYER_PURCHASE_ITEMS("/buyers/{0}/purchase-items"),
	BUYER_PURCHASE_ITEMS_ASSOCIATED_PRODUCT_NAMES("/buyers/{0}/purchase-items/associated-product-names"),
	BUYER_PURCHASE_ITEMS_ASSOCIATED_VENDORS("/buyers/{0}/purchase-items/associated-vendors"),
	BUYER_AGGREGATED_PURCHASE_LIST("/buyers/{0}/purchase-items/aggregated-list"),
	BUYER_PURCHASE_ITEM_INFO("/buyers/{0}/purchase-items/{1}"),
	
	VENDOR_PURCHASE_ITEMS("/vendors/{0}/purchase-items"),
	VENDOR_PURCHASE_ITEMS_ASSOCIATED_PRODUCT_NAMES("/vendors/{0}/purchase-items/associated-product-names"),
	VENDOR_PURCHASE_ITEMS_ASSOCIATED_BUYERS("/vendors/{0}/purchase-items/associated-buyers"),
	VENDOR_AGGREGATED_PURCHASE_LIST("/vendors/{0}/purchase-items/aggregated-list"),
	VENDOR_PURCHASE_ITEM_INFO("/vendors/{0}/purchase-items/{1}"),
	
	BUYER_PRODUCT_ENQUIRIES("/buyers/{0}/product-enquiries"),
	BUYER_PRODUCT_ENQUIRY_INFO("/buyers/{0}/product-enquiries/{1}"),
	BUYER_PRODUCT_ENQUIRIES_ASSOCIATED_VENDORS("/buyers/{0}/product-enquiries/associated-vendors"),
	
	ADMIN_PRODUCT_ENQUIRIES("/entities/product-enquiries"),
	ADMIN_PRODUCT_ENQUIRY_INFO("/entities/product-enquiries/{0}"),
	ADMIN_PRODUCT_ENQUIRIES_ASSOCIATED_BUYERS("/entities/product-enquiries/associated-buyers"),
	
	VENDOR_PRODUCT_ENQUIRIES("/vendors/{0}/product-enquiries"),
	VENDOR_PRODUCT_ENQUIRIES_ASSOCIATED_BUYERS("/vendors/{0}/product-enquiries/associated-buyers"),
	VENDOR_PRODUCT_ENQUIRY_INFO("/vendors/{0}/product-enquiries/{1}"),
	
	PRODUCT_OFFERS("/product-offers"),
	PRODUCT_OFFER_INFO("/product-offers/{0}"),
	
	NOTIFICATIONS("/notifications"),
	MARK_NOTIFICATIONS_AS_READ("/notifications/mark-as-read"),
	MARK_NOTIFICATIONS_AS_UNREAD("/notifications/mark-as-unread"),
	
	ABUSE_REPORTS("/abuse-reports"),
	ABUSE_REPORTS_BULK("/abuse-reports/bulk"),
	ABUSE_REPORT_INFO("/abuse-reports/{0}"),
	ABUSE_REPORT_CLOSE("/abuse-reports/{0}/close"),
	ABUSE_REPORT_ASSIGN("/abuse-reports/{0}//assign-to-self"),
	ABUSE_REPORT_ADDREMARKS("/abuse-reports/{0}/add-remarks"),
	
	ADMIN_USER_ACCOUNTS("/admin-accounts"),
	ADMIN_USER_ACCOUNTS_BULK("/admin-accounts/bulk"),
	ADMIN_USER_ACCOUNT_INFO("/admin-accounts/{0}"),
	ADMIN_GENERIC_USER_INFO("/admin-accounts/{0}/userInfo"),
	ADMIN_USER_ACCOUNTS_APPROVALS("/admin-accounts/{0}/approvals"),
	ADMIN_USER_ACCOUNT_BY_RELATIONSHIP("/entities/dealers/{0}"),
	
	DEALER_USER_ACCOUNTS("/dealers/user-accounts"),
	DEALER_USER_ACCOUNT_INFO("/dealers/user-accounts/{0}"),
	DEALER_USER_ACCOUNTS_APPROVALS("/dealers/user-accounts/{0}/approvals"),
	
	ADMIN_ENTITY_BY_ID("/entities/{0}"),
	ADMIN_ENTITY_BY_ID_APPROVALS("/entities/{0}/approvals"),
	
	BUYER_REVIEWS("/buyers/{0}/reviews"),
	BUYER_REVIEWS_AVERAGE_RATING("/buyers/{0}/reviews/average-rating"),
	BUYER_REVIEW_INFO("/buyers/{0}/reviews/{1}"),
	VENDOR_REVIEWS_AVERAGE_RATING("/vendors/{0}/reviews/average-rating"),
	VENDOR_REVIEWS("/vendors/{0}/reviews"),
	VENDOR_REVIEW_INFO("/vendors/{0}/reviews/{1}"),
	PRODUCT_REVIEWS("/products/{0}/reviews"),
	PRODUCT_REVIEWS_AVERAGE_RATING("/products/{0}/reviews/average-rating"),
	PRODUCT_REVIEW_INFO("/products/{0}/reviews/{1}"),
	ADMIN_REVIEW("/entities/reviews/{0}"),
	
	MESSAGING_MAIL("/messaging/email"),
	MESSAGING_MAIL_WITH_ATTACHMENT("/messaging/email-with-attachment"),
	
	BUYER_BIDDING_SUBSCRIPTION_PLANS("/buyers/bidding-subscription-plans"),
    BUYER_BIDDING_SUBSCRIPTION_PLAN_INFO("/buyers/bidding-subscription-plans/{0}"),
    BUYER_BIDDING_SUBSCRIPTION_PLAN_UPDATE_DEFAULT_STATUS("/buyers/bidding-subscription-plans/{0}/default-status"),
    BUYER_BIDDING_SUBSCRIPTION_PLAN_UPDATE_AVAILABILITY_STATUS("/buyers/bidding-subscription-plans/{0}/availability-status"),
    VENDOR_BIDDING_SUBSCRIPTION_PLANS("/vendors/bidding-subscription-plans"),
    VENDOR_BIDDING_SUBSCRIPTION_PLAN_INFO("/vendors/bidding-subscription-plans/{0}"),
    VENDOR_BIDDING_SUBSCRIPTION_PLAN_UPDATE_DEFAULT_STATUS("/vendors/bidding-subscription-plans/{0}/default-status"),
    VENDOR_BIDDING_SUBSCRIPTION_PLAN_UPDATE_AVAILABILITY_STATUS("/vendors/bidding-subscription-plans/{0}/availability-status"),
    
    BUYER_TENDERS("/buyers/{0}/tenders"),
    BUYER_TENDER_INFO("/buyers/{0}/tenders/{1}"),
    BUYER_TENDER_STATUS("/buyers/{0}/tenders/{1}/status"),
    BUYER_TENDER_PRODUCTS("/buyers/{0}/tenders/{1}/products"),
    BUYER_TENDER_PRODUCT_INFO("/buyers/{0}/tenders/{1}/products/{2}"),
    BUYER_TENDER_BIDS("/buyers/{0}/tenders/{1}/bids"),
    BUYER_TENDER_LOWEST_BID("/buyers/{0}/tenders/{1}/lowest-bid"),
    TENDERS("/tenders"),
	TENDER_INFO("/tenders/{0}"),
	ACCESS_TENDER("/tenders/{0}/access-tender"),
	TENDER_BID_PRODUCT_INFO("/tenders/{0}/products/{1}"),
	TENDER_PREVIEW("/tenders/{0}/preview"),
	TENDER_PREVIEW_PRODUCTS("/tenders/{0}/preview-products"),
	TENDERS_PREVIEWS("/tenders/previews"),
	TENDER_BIDS("/tenders/{0}/bids"),
	TENDER_PRODUCTS("/tenders/{0}/products"),
	TENDER_BID_INFO("/tenders/{0}/bids/{1}"),
	TENDER_LOWEST_BID("/tenders/{0}/bids/lowest-bid"),
	TENDER_BID_CURRENT_BID("/tenders/{0}/bids/my-bid"),
    
    BUYER_BIDDING_SUBSCRIPTIONS("/buyers/{0}/bidding-subscriptions"),
    BUYER_BIDDING_SUBSCRIPTION_INFO("/buyers/{0}/bidding-subscriptions/{1}"),
    BUYER_BIDDING_SUBSCRIPTION_UPDATE_STATUS("/buyers/{0}/bidding-subscriptions/{1}/status"),
    BUYER_BIDDING_SUBSCRIPTION_PAYMENT_TRANSACTIONS("/buyers/{0}/bidding-subscriptions/{1}/payment-transactions"),
    BUYER_BIDDING_SUBSCRIPTION_PAYMENT_TRANSACTION_INFO("/buyers/{0}/bidding-subscriptions/{1}/payment-transactions/{2}"),
    BUYER_BIDDING_SUBSCRIPTION_DEFAULT_PLAN("/buyers/{0}/bidding-subscriptions/create-subscription-with-default-plan"),
    VENDOR_BIDDING_SUBSCRIPTIONS("/vendors/{0}/bidding-subscriptions"),
    VENDOR_BIDDING_SUBSCRIPTION_INFO("/vendors/{0}/bidding-subscriptions/{1}"),
    VENDOR_BIDDING_SUBSCRIPTION_UPDATE_STATUS("/vendors/{0}/bidding-subscriptions/{1}/status"),
    VENDOR_BIDDING_SUBSCRIPTION_UPDATE_USAGE("/vendors/{0}/bidding-subscriptions/{1}/usage"),
    VENDOR_BIDDING_SUBSCRIPTION_PAYMENT_TRANSACTIONS("/vendors/{0}/bidding-subscriptions/{1}/payment-transactions"),
    VENDOR_BIDDING_SUBSCRIPTION_PAYMENT_TRANSACTION_INFO("/vendors/{0}/bidding-subscriptions/{1}/payment-transactions/{2}"),
    VENDOR_BIDDING_SUBSCRIPTION_DEFAULT_PLAN("/vendors/{0}/bidding-subscriptions/create-subscription-with-default-plan"),
    
    BUYER_PAYMENT_TRANSACTIONS("/buyers/{0}/payment-transactions"),
    VENDOR_PAYMENT_TRANSACTIONS("/vendors/{0}/payment-transactions"),
    BUYER_PAYMENT_TRANSACTION_INFO("/buyers/{0}/payment-transactions/{1}"),
    VENDOR_PAYMENT_TRANSACTION_INFO("/vendors/{0}/payment-transactions/{1}"),
    UTILITIES_ENTITY_COUNT("/utilities/entity-count"),
    PIPELINE_JOB_INFO("/pipeline-jobs/{0}")
	;
	
	public static final String CONTEXT_PATH = "/api";
	private String endpoint;

	private Endpoint(String endpoint) {
		this.endpoint = endpoint;

	}
	
	public URI get(String ...args) {
		return URI.create(CONTEXT_PATH + MessageFormat.format(endpoint, (Object[])args));
	}
	
	public URI get() {
		return URI.create(CONTEXT_PATH + endpoint);
	}
	
}
