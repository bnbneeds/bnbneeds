package com.bnbneeds.app.model.review;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "buyer_review")
public class BuyerReviewInfoResponse extends ReviewInfoResponse {

	private NamedRelatedResourceRep buyer;

	@XmlElement(name = "buyer")
	public NamedRelatedResourceRep getBuyer() {
		return buyer;
	}

	public void setBuyer(NamedRelatedResourceRep buyer) {
		this.buyer = buyer;
	}

	public void setBuyer(String id, String name) {
		setBuyer(new NamedRelatedResourceRep(id, new RestLinkRep("self",
				Endpoint.BUYER_INFO.get(id)), name));
	}

	public void setReviewedBy(String id, String name) {
		setReviewedBy(new NamedRelatedResourceRep(id, new RestLinkRep("self",
				Endpoint.VENDOR_INFO.get(id)), name));
	}

	@Override
	public void setLink() {
	}

}
