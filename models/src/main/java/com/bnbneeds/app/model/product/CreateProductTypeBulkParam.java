package com.bnbneeds.app.model.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "create_product_types")
public class CreateProductTypeBulkParam {

	private List<CreateProductTypeParam> productTypeList;

	@XmlElementWrapper(name = "product_types")
	@XmlElement(name = "product_type")
	@JsonProperty("product_type")
	public List<CreateProductTypeParam> getProductTypeList() {
		return productTypeList;
	}

	public void setProductTypeList(List<CreateProductTypeParam> productTypeList) {
		this.productTypeList = productTypeList;
	}

	public void add(CreateProductTypeParam param) {
		if (productTypeList == null) {
			productTypeList = new ArrayList<CreateProductTypeParam>();
		}
		productTypeList.add(param);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductTypeBulkParam [");
		if (productTypeList != null) {
			builder.append("productTypeList=");
			builder.append(productTypeList);
		}
		builder.append("]");
		return builder.toString();
	}

}
