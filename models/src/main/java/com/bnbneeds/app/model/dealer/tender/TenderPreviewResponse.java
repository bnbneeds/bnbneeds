package com.bnbneeds.app.model.dealer.tender;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DescribedDataObjectRestResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "tender_preview")
public class TenderPreviewResponse extends DescribedDataObjectRestResponse {

	private Double expectedPrice;
	private String deliveryFrequency;
	private String paymentMode;
	private String bidValueType;
	private String creditTerms;
	private String deliveryTerms;
	private NamedRelatedResourceRep businessType;
	private String deliveryLocation;

	@XmlElement(name = "expected_price")
	public Double getExpectedPrice() {
		return expectedPrice;
	}

	@XmlElement(name = "delivery_frequency")
	public String getDeliveryFrequency() {
		return deliveryFrequency;
	}

	@XmlElement(name = "payment_mode")
	public String getPaymentMode() {
		return paymentMode;
	}

	@XmlElement(name = "bid_value_type")
	public String getBidValueType() {
		return bidValueType;
	}

	@XmlElement(name = "credit_terms")
	public String getCreditTerms() {
		return creditTerms;
	}

	@XmlElement(name = "delivery_terms")
	public String getDeliveryTerms() {
		return deliveryTerms;
	}

	@XmlElement(name = "business_type")
	public NamedRelatedResourceRep getBusinessType() {
		return businessType;
	}

	@XmlElement(name = "delivery_location")
	public String getDeliveryLocation() {
		return deliveryLocation;
	}

	public void setBusinessType(NamedRelatedResourceRep businessType) {
		this.businessType = businessType;
	}

	public void setBusinessType(String id, String name) {
		setBusinessType(
				new NamedRelatedResourceRep(id, new RestLinkRep("self", Endpoint.BUSINESS_TYPE_INFO.get(id)), name));
	}

	public void setDeliveryLocation(String deliveryLocation) {
		this.deliveryLocation = deliveryLocation;
	}

	public void setExpectedPrice(Double expectedPrice) {
		this.expectedPrice = expectedPrice;
	}

	public void setExpectedPrice(double expectedPrice) {
		this.expectedPrice = expectedPrice;
	}

	public void setDeliveryFrequency(String deliveryFrequency) {
		this.deliveryFrequency = deliveryFrequency;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public void setBidValueType(String bidValueType) {
		this.bidValueType = bidValueType;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TenderParam [");
		builder.append("expectedPrice=").append(expectedPrice).append(", ");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

	@Override
	protected void setLink() {
		setLink(Endpoint.TENDER_PREVIEW.get(getId()));
	}

}
