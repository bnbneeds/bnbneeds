package com.bnbneeds.app.model.report;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "abuse_report_list")
public class AbuseReportListResponse extends DataObjectListResponse {

	@XmlElementWrapper(name = "abuse_reports")
	@XmlElement(name = "abuse_report")
	@JsonProperty("abuse_report")
	public List<NamedRelatedResourceRep> getAbuseReportList() {
		return dataObjectList;
	}
	
	public void setAbuseReportList(List<NamedRelatedResourceRep> dataObjectList) {
		super.setNamedRelatedResourceList(dataObjectList);
	}
	
}
