package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "create_bidding_subscription_plan")
public class CreateBuyerSubscriptionPlanParam extends BiddingSubscriptionPlanParam {

	private Integer numberOfTendersAllowedToCreate;

	@XmlElement(name = "number_of_tenders_allowed_to_create")
	public Integer getNumberOfTendersAllowedToCreate() {
		return numberOfTendersAllowedToCreate;
	}

	public void setNumberOfTendersAllowedToCreate(Integer numberOfAllowedTendersToCreate) {
		this.numberOfTendersAllowedToCreate = numberOfAllowedTendersToCreate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateSubscriptionPlanParam [");
		if (numberOfTendersAllowedToCreate != null)
			builder.append("numberOfTendersAllowedToCreate=").append(numberOfTendersAllowedToCreate).append(", ");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
