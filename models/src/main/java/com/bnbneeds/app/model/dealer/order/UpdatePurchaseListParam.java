package com.bnbneeds.app.model.dealer.order;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "update_purchase_list")
public class UpdatePurchaseListParam {

	private List<UpdatePurchaseItemParam> purchaseList;

	@XmlElementWrapper(name = "items")
	@XmlElement(name = "item")
	@JsonProperty("item")
	public List<UpdatePurchaseItemParam> getPurchaseList() {
		return purchaseList;
	}

	public void setPurchaseList(List<UpdatePurchaseItemParam> purchaseList) {
		this.purchaseList = purchaseList;
	}

	public void add(UpdatePurchaseItemParam itemParam) {
		if (purchaseList == null) {
			purchaseList = new ArrayList<UpdatePurchaseItemParam>();
		}
		purchaseList.add(itemParam);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdatePurchaseItemParam [");
		if (purchaseList != null)
			builder.append("purchaseList=").append(purchaseList);
		builder.append("]");
		return builder.toString();
	}

}
