package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.EntityParam;

@XmlRootElement(name = "create_business_type")
public class CreateBusinessTypeParam extends EntityParam {

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateBusinessTypeParam [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
