package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class BlobResourceRep extends RelatedResourceRep {

	private String mediaType;
	private Long size;

	public BlobResourceRep() {

	}

	public BlobResourceRep(String id, String mediaType, Long size,
			RestLinkRep selfLink) {
		super(id, selfLink);
		this.mediaType = mediaType;
		this.size = size;
	}

	@XmlElement(name = "media_type")
	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	@XmlElement(name = "size")
	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

}
