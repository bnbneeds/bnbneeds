package com.bnbneeds.app.model.dealer.tender;

import java.net.URI;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectRestResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.dealer.order.Quantity;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "tender_product")
public class TenderProductInfoResponse extends DataObjectRestResponse {

	private NamedRelatedResourceRep tender;
	private NamedRelatedResourceRep productName;
	private Quantity quantity;
	private String qualitySpecification;
	private String specialRequirements;

	@XmlElement(name = "tender")
	public NamedRelatedResourceRep getTender() {
		return tender;
	}

	public void setTender(NamedRelatedResourceRep tender) {
		this.tender = tender;
	}

	public void setTender(String id, String name, URI link) {
		setTender(new NamedRelatedResourceRep(new RelatedResourceRep(id, new RestLinkRep("self", link)), name));
	}

	@XmlElement(name = "product")
	@JsonProperty("product")
	public NamedRelatedResourceRep getProductName() {
		return productName;
	}

	public void setProductName(NamedRelatedResourceRep productName) {
		this.productName = productName;
	}

	public void setProductName(String id, String name) {
		setProductName(new NamedRelatedResourceRep(
				new RelatedResourceRep(id, new RestLinkRep("self", Endpoint.PRODUCT_NAME_INFO.get(id))), name));
	}

	@XmlElement(name = "quantity")
	public Quantity getQuantity() {
		return quantity;
	}

	public void setQuantity(Quantity quantity) {
		this.quantity = quantity;
	}

	@XmlElement(name = "quality_specification")
	public String getQualitySpecification() {
		return qualitySpecification;
	}

	public void setQualitySpecification(String qualitySpecification) {
		this.qualitySpecification = qualitySpecification;
	}

	@XmlElement(name = "special_requirements")
	public String getSpecialRequirements() {
		return specialRequirements;
	}

	public void setSpecialRequirements(String specialRequirements) {
		this.specialRequirements = specialRequirements;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BidProductInfoResponse [");
		if (productName != null)
			builder.append("productName=").append(productName).append(", ");
		builder.append("quantity=").append(quantity).append(", ");
		if (qualitySpecification != null)
			builder.append("qualitySpecification=").append(qualitySpecification).append(", ");
		if (specialRequirements != null)
			builder.append("specialRequirements=").append(specialRequirements);
		builder.append("]");
		return builder.toString();
	}

	@Override
	protected void setLink() {
		// TODO Auto-generated method stub

	}

}
