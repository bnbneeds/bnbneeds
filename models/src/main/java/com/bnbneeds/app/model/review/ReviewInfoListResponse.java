package com.bnbneeds.app.model.review;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "review_list")
public class ReviewInfoListResponse<T extends ReviewInfoResponse> extends
		DataObjectListResponse {

	private List<T> reviews;

	@XmlElementWrapper(name = "reviews")
	@XmlElement(name = "review")
	@JsonProperty("review")
	public List<T> getReviews() {
		return reviews;
	}

	public void setReviews(List<T> reviews) {
		this.reviews = reviews;
	}

	public void addReviewInfoResponse(T item) {

		if (reviews == null) {
			reviews = new ArrayList<T>();
		}
		reviews.add(item);
	}

	public int size() {
		return reviews == null ? 0 : reviews.size();
	}
}
