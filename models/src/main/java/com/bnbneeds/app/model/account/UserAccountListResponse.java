package com.bnbneeds.app.model.account;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;

@XmlRootElement(name="user_account_list")
public class UserAccountListResponse extends DataObjectListResponse {
    @XmlElementWrapper(name = "user_accounts")
    @XmlElement(name = "user_account")
    public List<NamedRelatedResourceRep> getUserAccountList() {
        return dataObjectList;
    }
	
}

