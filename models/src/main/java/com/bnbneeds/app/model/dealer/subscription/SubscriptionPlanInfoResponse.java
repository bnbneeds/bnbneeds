package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DescribedDataObjectRestResponse;

@XmlRootElement(name = "bidding_subscription_plan")
public class SubscriptionPlanInfoResponse extends DescribedDataObjectRestResponse {

	private String title;
	private String durationType;
	private Integer durationValue;
	private Double premiumAmount;
	private String type;
	private Boolean defaultPlan;
	private String availablityStatus;

	@XmlElement(name = "default_plan")
	public Boolean getDefaultPlan() {
		return defaultPlan;
	}

	public void setDefaultPlan(Boolean defaultPlan) {
		this.defaultPlan = defaultPlan;
	}

	@XmlElement(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlElement(name = "title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@XmlElement(name = "duration_type")
	public String getDurationType() {
		return durationType;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	@XmlElement(name = "duration_value")
	public Integer getDurationValue() {
		return durationValue;
	}

	public void setDurationValue(Integer durationValue) {
		this.durationValue = durationValue;
	}

	@XmlElement(name = "premium_amount")
	public Double getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(Double premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	@XmlElement(name = "availablity_status")
	public String getAvailablityStatus() {
		return availablityStatus;
	}

	public void setAvailablityStatus(String availablityStatus) {
		this.availablityStatus = availablityStatus;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SubscriptionPlanInfoResponse [");
		if (title != null)
			builder.append("title=").append(title).append(", ");
		if (durationType != null)
			builder.append("durationType=").append(durationType).append(", ");
		if (durationValue != null)
			builder.append("durationValue=").append(durationValue).append(", ");
		if (premiumAmount != null)
			builder.append("premiumAmount=").append(premiumAmount).append(", ");
		if (type != null)
			builder.append("type=").append(type).append(", ");
		if (defaultPlan != null)
			builder.append("defaultPlan=").append(defaultPlan).append(", ");
		if (availablityStatus != null)
			builder.append("availablityStatus=").append(availablityStatus);
		builder.append("]");
		return builder.toString();
	}

	@Override
	protected void setLink() {
		// TODO Auto-generated method stub

	}

}
