package com.bnbneeds.app.model.dealer.tender;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectRestResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.review.AverageRatingResponse;

@XmlRootElement(name = "tender_bid")
public class TenderBidInfoResponse extends DataObjectRestResponse {

	private NamedRelatedResourceRep vendor;
	private NamedRelatedResourceRep tender;
	private AverageRatingResponse vendorAverageRating;
	private Double value;
	private String comments;

	@XmlElement(name = "vendor")
	public NamedRelatedResourceRep getVendor() {
		return vendor;
	}

	@XmlElement(name = "tender")
	public RelatedResourceRep getTender() {
		return tender;
	}

	@XmlElement(name = "value")
	public Double getValue() {
		return value;
	}

	@XmlElement(name = "comments")
	public String getComments() {
		return comments;
	}

	@XmlElement(name = "vendor_average_rating")
	public AverageRatingResponse getVendorAverageRating() {
		return vendorAverageRating;
	}

	public void setVendorAverageRating(AverageRatingResponse vendorAverageRating) {
		this.vendorAverageRating = vendorAverageRating;
	}

	public void setVendor(NamedRelatedResourceRep vendor) {
		this.vendor = vendor;
	}

	public void setVendor(String id, String name) {
		setVendor(new NamedRelatedResourceRep(
				new RelatedResourceRep(id, new RestLinkRep("self", Endpoint.VENDOR_INFO.get(id))), name));
	}

	public void setTender(NamedRelatedResourceRep tender) {
		this.tender = tender;
	}

	public void setTender(String id, String name) {
		setTender(new NamedRelatedResourceRep(
				new RelatedResourceRep(id, new RestLinkRep("self", Endpoint.TENDER_INFO.get(id))), name));
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	protected void setLink() {
		// TODO Auto-generated method stub
	}

}
