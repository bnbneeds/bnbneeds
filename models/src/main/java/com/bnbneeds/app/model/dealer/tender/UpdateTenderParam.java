package com.bnbneeds.app.model.dealer.tender;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "update_tender")
public class UpdateTenderParam extends TenderParam {


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateTenderParam [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
