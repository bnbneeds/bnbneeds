package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "bidding_subscription_plan")
public class BiddingSubscriptionPlanParam extends SubscriptionPlanParam {

	public static enum PlanType {
		DURATION, QUOTA
	}

	private String type;

	@XmlElement(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isPlanType(PlanType type) {
		return type == PlanType.valueOf(getType());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BiddingSubscriptionPlanParam [");
		if (type != null)
			builder.append("type=").append(type).append(", ");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
