package com.bnbneeds.app.model.account;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "user_account_list")
public class UserAccountInfoListResponse {

	private List<UserAccountInfoResponse> userAccountList;
	private String nextOffset;

	@XmlElementWrapper(name = "user_accounts")
	@XmlElement(name = "user_account")
	@JsonProperty("user_account")
	public List<UserAccountInfoResponse> getUserAccountList() {
		return userAccountList;
	}

	public void setUserAccountList(List<UserAccountInfoResponse> userAccountList) {
		this.userAccountList = userAccountList;
	}

	public void add(UserAccountInfoResponse response) {
		if (userAccountList == null) {
			userAccountList = new ArrayList<UserAccountInfoResponse>();
		}
		userAccountList.add(response);
	}

	@XmlElement(name = "next_offset")
	public String getNextOffset() {
		return nextOffset;
	}

	public void setNextOffset(String nextOffset) {
		this.nextOffset = nextOffset;
	}

	public int size() {
		return userAccountList == null ? 0 : userAccountList.size();
	}

}
