package com.bnbneeds.app.model.product;

import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.EntityParam;

@XmlRootElement(name = "create_product_category")
public class CreateProductCategoryParam extends EntityParam {

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateProductCategoryParam [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
