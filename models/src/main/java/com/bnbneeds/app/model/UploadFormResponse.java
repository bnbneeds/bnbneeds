package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "upload_form")
public class UploadFormResponse {

	private String actionURL;
	private String inputFileElementName;
	private String acceptedFileTypes;

	@XmlElement(name = "action_url")
	@JsonProperty("action_url")
	public String getActionURL() {
		return actionURL;
	}

	@XmlElement(name = "accepted_file_types")
	public String getAcceptedFileTypes() {
		return acceptedFileTypes;
	}

	@XmlElement(name = "input_file_element_name")
	public String getInputFileElementName() {
		return inputFileElementName;
	}

	public void setInputFileElementName(String inputFileElementName) {
		this.inputFileElementName = inputFileElementName;
	}

	public void setActionURL(String actionURL) {
		this.actionURL = actionURL;
	}

	public void setAcceptedFileTypes(String acceptedFileTypes) {
		this.acceptedFileTypes = acceptedFileTypes;
	}

}
