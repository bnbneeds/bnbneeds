package com.bnbneeds.app.model.product;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DescribedDataObjectRestResponse;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "product_name")
public class ProductNameInfoResponse extends DescribedDataObjectRestResponse {

	private RestLinkRep image;

	@XmlElement(name = "link")
	public RestLinkRep getImage() {
		return image;
	}

	public void setImage(String imageKey) {
		if (imageKey != null) {
			this.image = new RestLinkRep("image",
					Endpoint.PRODUCT_NAME_IMAGE.get(getId(), imageKey));
		}
	}

	public void setLink() {
		setLink(Endpoint.PRODUCT_NAME_INFO.get(getId()));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductNameInfoResponse [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
