package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "update_enquiry")
public class UpdateEnquiryParam {

	private String description;
	private String requiredQuantity;
	private LikelyToBuyInParam likelyToBuyIn;

	@XmlElement(name = "description")
	public String getDescription() {
		return description;
	}

	@XmlElement(name = "required_quantity")
	public String getRequiredQuantity() {
		return requiredQuantity;
	}

	@XmlElement(name = "likely_to_buy_in")
	public LikelyToBuyInParam getLikelyToBuyIn() {
		return likelyToBuyIn;
	}

	public void setLikelyToBuyIn(LikelyToBuyInParam likelyToBuyIn) {
		this.likelyToBuyIn = likelyToBuyIn;
	}

	public void setLikelyToBuyIn(Integer likelyToBuyInValue, String timeUnit) {
		LikelyToBuyInParam likelyToBuyIn = new LikelyToBuyInParam(
				likelyToBuyInValue, timeUnit);
		setLikelyToBuyIn(likelyToBuyIn);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setRequiredQuantity(String requiredQuantity) {
		this.requiredQuantity = requiredQuantity;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateEnquiryParam [");
		if (description != null)
			builder.append("description=").append(description).append(", ");
		if (requiredQuantity != null)
			builder.append("requiredQuantity=").append(requiredQuantity);
		builder.append("]");
		return builder.toString();
	}

}
