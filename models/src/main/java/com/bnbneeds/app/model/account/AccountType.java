package com.bnbneeds.app.model.account;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(String.class)
public enum AccountType {

	@XmlEnumValue("VENDOR") VENDOR,
	@XmlEnumValue("BUYER") BUYER,
	@XmlEnumValue("ADMIN") ADMIN;

}
