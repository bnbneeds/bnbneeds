package com.bnbneeds.app.model.product;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DescribedDataObjectRestResponse;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "product_offer")
public class ProductOfferInfoResponse extends DescribedDataObjectRestResponse {

	private RelatedResourceRep product;
	private String offerTitle;
	private String startDate;
	private String endDate;

	@XmlElement(name = "offer_title")
	public String getOfferTitle() {
		return offerTitle;
	}

	@XmlElement(name = "start_date")
	public String getStartDate() {
		return startDate;
	}

	@XmlElement(name = "end_date")
	public String getEndDate() {
		return endDate;
	}

	@XmlElement(name = "product")
	public RelatedResourceRep getProduct() {
		return product;
	}

	public void setProduct(String productId) {
		this.product = new RelatedResourceRep(productId, new RestLinkRep(
				"self", Endpoint.PRODUCT_INFO.get(productId)));
	}

	public void setOfferTitle(String offerTitle) {
		this.offerTitle = offerTitle;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setLink() {
		setLink(Endpoint.PRODUCT_OFFER_INFO.get(getId()));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductOfferInfoResponse [");
		builder.append(super.toString());
		if (product != null)
			builder.append("product=").append(product).append(", ");
		if (offerTitle != null)
			builder.append("offerTitle=").append(offerTitle).append(", ");
		if (startDate != null)
			builder.append("startDate=").append(startDate).append(", ");
		if (endDate != null)
			builder.append("endDate=").append(endDate);
		builder.append("]");
		return builder.toString();
	}

	
}
