package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class NamedRelatedResourceRep extends RelatedResourceRep {

    private String name;

    public NamedRelatedResourceRep() {
    }

    public NamedRelatedResourceRep(String id, RestLinkRep selfLink, String name) {
        super(id, selfLink);
        this.name = name;
    }
    
    public NamedRelatedResourceRep(String id, RestLinkRep selfLink) {
        super(id, selfLink);
    }
    
    public NamedRelatedResourceRep(RelatedResourceRep relatedResourceRep, String name) {
        super(relatedResourceRep.getId(), relatedResourceRep.getLink());
        this.name = name;
    }
    
    public NamedRelatedResourceRep(RelatedResourceRep relatedResourceRep) {
        super(relatedResourceRep.getId(), relatedResourceRep.getLink());
    }

    /**
     * The name of the resource
     * 
     * @valid none
     */
    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj)) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            NamedRelatedResourceRep other = (NamedRelatedResourceRep) obj;
            if (name == null) {
                if (other.name != null) {
                    return false;
                }
            } else if (!name.equals(other.name)) {
                return false;
            }
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        return getId().toString() + " " + getName();
    }
}
