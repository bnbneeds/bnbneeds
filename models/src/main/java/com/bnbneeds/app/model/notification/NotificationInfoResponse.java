package com.bnbneeds.app.model.notification;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectRestResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "notification")
public class NotificationInfoResponse extends DataObjectRestResponse {

	private Long timestamp;
	private String notificationType;
	private String notificationText;
	private String recipientType;
	private String recipientId;
	private String entityId;
	private String entityName;
	private String entityLink;
	private String triggeredBy;
	private boolean read;

	@XmlElement(name = "notification_type")
	public String getNotificationType() {
		return notificationType;
	}

	@XmlElement(name = "notification_text")
	public String getNotificationText() {
		return notificationText;
	}

	@XmlElement(name = "recipient_type")
	public String getRecipientType() {
		return recipientType;
	}

	@XmlElement(name = "recipient_id")
	public String getRecipientId() {
		return recipientId;
	}

	@XmlElement(name = "timestamp")
	public Long getTimestamp() {
		return timestamp;
	}

	@XmlElement(name = "triggered_by")
	public String getTriggeredBy() {
		return triggeredBy;
	}

	public void setTriggeredBy(String triggeredBy) {
		this.triggeredBy = triggeredBy;
	}

	@XmlElement(name = "entity_id")
	public String getEntityId() {
		return entityId;
	}

	@XmlElement(name = "entity_name")
	public String getEntityName() {
		return entityName;
	}

	@XmlElement(name = "entity_link")
	public String getEntityLink() {
		return entityLink;
	}

	@XmlElement(name = "read")
	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public void setEntityLink(String entityLink) {
		this.entityLink = entityLink;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public void setNotificationText(String notificationText) {
		this.notificationText = notificationText;
	}

	public void setRecipientType(String recipientType) {
		this.recipientType = recipientType;
	}

	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}

	@Override
	protected void setLink() {
		setLink(Endpoint.NOTIFICATIONS.get());
	}

}
