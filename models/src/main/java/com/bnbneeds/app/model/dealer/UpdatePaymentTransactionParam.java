package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "update_payment_transaction")
public class UpdatePaymentTransactionParam extends PaymentTransactionParam {

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdatePaymentTransactionParam [");
		if (super.toString() != null)
			builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
