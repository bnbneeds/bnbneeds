package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DescribedDataObjectRestResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;

@XmlRootElement(name = "enquiry")
public class EnquiryInfoResponse extends DescribedDataObjectRestResponse {

	private NamedRelatedResourceRep product;
	private NamedRelatedResourceRep buyer;
	private NamedRelatedResourceRep vendor;
	private String requestDate;
	private String requiredQuantity;
	private LikelyToBuyInParam likelyToBuyIn;

	@XmlElement(name = "product")
	public NamedRelatedResourceRep getProduct() {
		return product;
	}

	@XmlElement(name = "buyer")
	public NamedRelatedResourceRep getBuyer() {
		return buyer;
	}

	@XmlElement(name = "request_date")
	public String getRequestDate() {
		return requestDate;
	}

	@XmlElement(name = "required_quantity")
	public String getRequiredQuantity() {
		return requiredQuantity;
	}

	@XmlElement(name = "likely_to_buy_in")
	public LikelyToBuyInParam getLikelyToBuyIn() {
		return likelyToBuyIn;
	}

	@XmlElement(name = "vendor")
	public NamedRelatedResourceRep getVendor() {
		return vendor;
	}

	public void setVendor(NamedRelatedResourceRep vendor) {
		this.vendor = vendor;
	}

	public void setLikelyToBuyIn(LikelyToBuyInParam likelyToBuyIn) {
		this.likelyToBuyIn = likelyToBuyIn;
	}

	public void setLikelyToBuyIn(Integer likelyToBuyInValue, String timeUnit) {
		setLikelyToBuyIn(new LikelyToBuyInParam(likelyToBuyInValue, timeUnit));
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public void setRequiredQuantity(String requiredQuantity) {
		this.requiredQuantity = requiredQuantity;
	}

	public void setProduct(NamedRelatedResourceRep product) {
		this.product = product;
	}

	public void setBuyer(NamedRelatedResourceRep buyer) {
		this.buyer = buyer;
	}

	@Override
	protected void setLink() {
		// This is intentionally left empty as the response depends on whether
		// its buyer or vendor
	}

}
