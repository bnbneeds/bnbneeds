package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "create_enquiry")
public class CreateEnquiryParam {

	private String productId;
	private String requiredQuantity;
	private String description;
	// Likely to buy in "2 days", "3 months."
	private LikelyToBuyInParam likelyToBuyIn;

	@XmlElement(name = "product_id")
	public String getProductId() {
		return productId;
	}

	@XmlElement(name = "required_quantity")
	public String getRequiredQuantity() {
		return requiredQuantity;
	}

	@XmlElement(name = "description")
	public String getDescription() {
		return description;
	}

	@XmlElement(name = "likely_to_buy_in", required = true)
	public LikelyToBuyInParam getLikelyToBuyIn() {
		return likelyToBuyIn;
	}

	public void setLikelyToBuyIn(LikelyToBuyInParam likelyToBuyIn) {
		this.likelyToBuyIn = likelyToBuyIn;
	}

	public void setLikelyToBuyIn(int value, String timeUnit) {
		LikelyToBuyInParam param = new LikelyToBuyInParam(value, timeUnit);
		setLikelyToBuyIn(param);
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public void setRequiredQuantity(String requiredQuantity) {
		this.requiredQuantity = requiredQuantity;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateEnquiryParam [");
		if (productId != null)
			builder.append("productId=").append(productId).append(", ");
		if (requiredQuantity != null)
			builder.append("requiredQuantity=").append(requiredQuantity)
					.append(", ");
		if (description != null)
			builder.append("description=").append(description).append(", ");
		if (likelyToBuyIn != null)
			builder.append("likelyToBuyIn=").append(likelyToBuyIn);
		builder.append("]");
		return builder.toString();
	}

}
