package com.bnbneeds.app.model.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "product_list")
public class ProductInfoListResponse {

	private List<ProductInfoResponse> productInfoResponseList;
	private String nextOffset;

	@XmlElementWrapper(name = "products")
	@XmlElement(name = "product_info")
	@JsonProperty("product_info")
	public List<ProductInfoResponse> getProductInfoResponseList() {
		return productInfoResponseList;
	}

	@XmlElement(name = "next_offset")
	public String getNextOffset() {
		return nextOffset;
	}

	public void setNextOffset(String nextOffset) {
		this.nextOffset = nextOffset;
	}

	public void setProductInfoResponseList(
			List<ProductInfoResponse> productInfoResponseList) {

		if (productInfoResponseList != null
				&& !productInfoResponseList.isEmpty()) {
			this.productInfoResponseList = productInfoResponseList;
		}
	}

	public void addProductInfoResponseList(
			List<ProductInfoResponse> productInfoResponseList) {

		if (productInfoResponseList != null
				&& !productInfoResponseList.isEmpty()) {

			if (this.productInfoResponseList == null) {
				this.productInfoResponseList = new ArrayList<ProductInfoResponse>();
			}
			this.productInfoResponseList.addAll(productInfoResponseList);
		}
	}

	public void addProductInfoResponse(ProductInfoResponse productInfoResponse) {

		if (productInfoResponse != null) {
			if (this.productInfoResponseList == null) {
				this.productInfoResponseList = new ArrayList<ProductInfoResponse>();
			}
			this.productInfoResponseList.add(productInfoResponse);
		}

	}

	public int size() {
		return productInfoResponseList == null ? 0 : productInfoResponseList
				.size();
	}

	public boolean isEmpty() {
		return size() == 0;
	}

}
