package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "create_vendor")
public class CreateVendorParam  extends DealerParam {

	private String vatNumber;
	private String serviceTaxNumber;
	private String cities;

	@XmlElement(name = "vat_number", required = true)
	public String getVatNumber() {
		return vatNumber;
	}

	@XmlElement(name = "service_tax_number", required = true)
	public String getServiceTaxNumber() {
		return serviceTaxNumber;
	}

	@XmlElement(name = "cities", required = true)
	public String getCities() {
		return cities;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public void setServiceTaxNumber(String serviceTaxNumber) {
		this.serviceTaxNumber = serviceTaxNumber;
	}

	public void setCities(String cities) {
		this.cities = cities;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Vendor [");
		builder.append(super.toString());
		if (vatNumber != null) {
			builder.append("vatNumber=");
			builder.append(vatNumber);
			builder.append(", ");
		}
		if (serviceTaxNumber != null) {
			builder.append("serviceTaxNumber=");
			builder.append(serviceTaxNumber);
			builder.append(", ");
		}
		if (cities != null) {
			builder.append("cities=");
			builder.append(cities);
		}
		builder.append("]");
		return builder.toString();
	}

}
