package com.bnbneeds.app.model.dealer.tender;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "create_tender")
public class CreateTenderParam extends TenderParam {


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateTenderParam [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
