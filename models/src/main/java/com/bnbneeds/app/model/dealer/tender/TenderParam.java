package com.bnbneeds.app.model.dealer.tender;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.EntityParam;

@XmlRootElement(name = "tender_param")
public class TenderParam extends EntityParam {

	public static enum DeliveryFrequency {
		ONETIME, DAILY, WEEKLY, FORTNIGHTLY, MONTHLY, QUATERLY
	}

	public static enum PaymentMode {
		CASH, CHEQUE, ONLINE, CREDIT
	}

	public static enum CreditTerms {
		DAYS_15, DAYS_30, DAYS_45, DAYS_60, DAYS_90
	}

	public static enum BidValueType {
		UNIT_PRICE, TOTAL_PRICE
	}

	private Double expectedPrice;
	private String deliveryFrequency;
	private String paymentMode;
	private String bidValueType;
	private String creditTerms;
	private String deliveryTerms;
	private String deliveryLocation;
	private String closeDate;

	@XmlElement(name = "expected_price")
	public Double getExpectedPrice() {
		return expectedPrice;
	}

	public void setExpectedPrice(Double expectedPrice) {
		this.expectedPrice = expectedPrice;
	}

	@XmlElement(name = "delivery_frequency")
	public String getDeliveryFrequency() {
		return deliveryFrequency;
	}

	public void setDeliveryFrequency(String deliveryFrequency) {
		this.deliveryFrequency = deliveryFrequency;
	}

	@XmlElement(name = "payment_mode")
	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	@XmlElement(name = "bid_value_type")
	public String getBidValueType() {
		return bidValueType;
	}

	public void setBidValueType(String bidValueType) {
		this.bidValueType = bidValueType;
	}

	@XmlElement(name = "credit_terms")
	public String getCreditTerms() {
		return creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	@XmlElement(name = "delivery_terms")
	public String getDeliveryTerms() {
		return deliveryTerms;
	}

	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}

	@XmlElement(name = "delivery_location")
	public String getDeliveryLocation() {
		return deliveryLocation;
	}

	public void setDeliveryLocation(String deliveryLocation) {
		this.deliveryLocation = deliveryLocation;
	}

	@XmlElement(name = "close_date")
	public String getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TenderParam [");
		if (expectedPrice != null)
			builder.append("expectedPrice=").append(expectedPrice).append(", ");
		if (deliveryFrequency != null)
			builder.append("deliveryFrequency=").append(deliveryFrequency).append(", ");
		if (paymentMode != null)
			builder.append("paymentMode=").append(paymentMode).append(", ");
		if (bidValueType != null)
			builder.append("bidValueType=").append(bidValueType).append(", ");
		if (creditTerms != null)
			builder.append("creditTerms=").append(creditTerms).append(", ");
		if (deliveryTerms != null)
			builder.append("deliveryTerms=").append(deliveryTerms).append(", ");
		if (deliveryLocation != null)
			builder.append("deliveryLocation=").append(deliveryLocation).append(", ");
		if (closeDate != null)
			builder.append("closeDate=").append(closeDate);
		builder.append("]");
		return builder.toString();
	}

}
