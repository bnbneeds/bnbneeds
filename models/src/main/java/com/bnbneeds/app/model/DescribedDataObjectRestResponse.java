package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlElement;

public abstract class DescribedDataObjectRestResponse extends
		ApprovedDataObjectRestResponse {

	private String description;

	@XmlElement(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DescribedDataObjectRestResponse [");
		builder.append(super.toString());
		if (description != null)
			builder.append("description=").append(description);
		builder.append("]");
		return builder.toString();
	}

}
