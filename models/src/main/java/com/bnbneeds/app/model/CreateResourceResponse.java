package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "create_resource_response")
public class CreateResourceResponse extends TaskResponse {

	private RelatedResourceRep resourceRep;

	public CreateResourceResponse() {
		super();
	}

	public CreateResourceResponse(int httpCode, OperationStatus status,
			String message) {

		super(httpCode, status, message, null);
	}

	@XmlElement(name = "resource")
	@JsonProperty(value = "resource")
	public RelatedResourceRep getResourceRep() {
		return resourceRep;
	}

	public void setResourceRep(RelatedResourceRep resourceRep) {
		this.resourceRep = resourceRep;
	}

}
