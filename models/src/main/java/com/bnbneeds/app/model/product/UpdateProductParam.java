package com.bnbneeds.app.model.product;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "update_product")
public class UpdateProductParam extends ProductParam {

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateProductParam [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
