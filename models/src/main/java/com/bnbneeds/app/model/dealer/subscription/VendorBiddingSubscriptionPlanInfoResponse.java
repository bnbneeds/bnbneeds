package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "bidding_subscription_plan")
public class VendorBiddingSubscriptionPlanInfoResponse extends SubscriptionPlanInfoResponse {

	private Integer numberOfTendersAllowedToAccess;

	@XmlElement(name = "number_of_tenders_allowed_to_access")
	public Integer getNumberOfTendersAllowedToAccess() {
		return numberOfTendersAllowedToAccess;
	}

	public void setNumberOfTendersAllowedToAccess(Integer numberOfTendersAllowedToAccess) {
		this.numberOfTendersAllowedToAccess = numberOfTendersAllowedToAccess;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SubscriptionPlansResponse [");
		if (numberOfTendersAllowedToAccess != null)
			builder.append("numberOfTendersAllowedToAccess=").append(numberOfTendersAllowedToAccess).append(", ");
		builder.append("]");
		return builder.toString();
	}

	@Override
	protected void setLink() {
		setLink(Endpoint.VENDOR_BIDDING_SUBSCRIPTION_PLAN_INFO.get(getId()));
	}

}
