package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlElement;

import com.bnbneeds.app.model.DescribedDataObjectRestResponse;

public abstract class DealerInfoResponse extends DescribedDataObjectRestResponse {

	private String address;
	private String contactName;
	private String mobileNumber;
	private String emailAddress;
	private String website;
	private String pan;
	
	@XmlElement(name = "address")
	public String getAddress() {
		return address;
	}

	@XmlElement(name = "contact_name")
	public String getContactName() {
		return contactName;
	}

	@XmlElement(name = "pan")
	public String getPan() {
		return pan;
	}

	@XmlElement(name = "mobile_number")
	public String getMobileNumber() {
		return mobileNumber;
	}

	@XmlElement(name = "email_address")
	public String getEmailAddress() {
		return emailAddress;
	}
	
	@XmlElement(name = "website")
	public String getWebsite() {
		return website;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DealerInfoResponse [");
		if (address != null)
			builder.append("address=").append(address).append(", ");
		if (contactName != null)
			builder.append("contactName=").append(contactName).append(", ");
		if (mobileNumber != null)
			builder.append("mobileNumber=").append(mobileNumber).append(", ");
		if (emailAddress != null)
			builder.append("emailAddress=").append(emailAddress).append(", ");
		if (website != null)
			builder.append("website=").append(website).append(", ");
		if (pan != null)
			builder.append("pan=").append(pan);
		builder.append("]");
		return builder.toString();
	}

}
