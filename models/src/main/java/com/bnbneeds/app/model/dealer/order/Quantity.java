package com.bnbneeds.app.model.dealer.order;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "quantity")
public class Quantity {

	private Double required;
	private Double delivered;
	private String unit;

	public Quantity() {
		super();
	}

	public Quantity(Double required, Double delivered, String unit) {
		super();
		this.required = required;
		this.delivered = delivered;
		this.unit = unit;
	}

	public Quantity(Double required, String unit) {
		super();
		this.required = required;
		this.unit = unit;
	}

	@XmlElement(name = "required")
	public Double getRequired() {
		return required;
	}

	@XmlElement(name = "delivered")
	public Double getDelivered() {
		return delivered;
	}

	@XmlElement(name = "unit")
	public String getUnit() {
		return unit;
	}

	public void setRequired(Double required) {
		this.required = required;
	}

	public void setDelivered(Double delivered) {
		this.delivered = delivered;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Quantity [");
		if (required != null)
			builder.append("required=").append(required).append(", ");
		if (delivered != null)
			builder.append("delivered=").append(delivered).append(", ");
		if (unit != null)
			builder.append("unit=").append(unit);
		builder.append("]");
		return builder.toString();
	}

}
