package com.bnbneeds.app.model.dealer;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectInfoListResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "payment_transaction_list")
public class PaymentTransactionInfoListResponse extends DataObjectInfoListResponse<PaymentTransactionInfoResponse> {

	@Override
	@XmlElementWrapper(name = "payment_transactions")
	@XmlElement(name = "payment_transaction")
	@JsonProperty("payment_transaction")
	public List<PaymentTransactionInfoResponse> getList() {
		return super.getList();
	}

}
