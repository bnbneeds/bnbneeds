package com.bnbneeds.app.model;

import java.net.URI;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement
public abstract class DataObjectRestResponse {

	private String id;
	private String name;
	private boolean inactive;
	private String createTimestamp;
	private String updateTimestamp;
	private RestLinkRep link;

	@XmlElement(name = "id")
	public String getId() {
		return id;
	}

	@XmlElement(name = "link")
	@JsonIgnore
	public RestLinkRep getLink() {
		return link;
	}

	@XmlElement(name = "name")
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@XmlElement(name = "inactive")
	@JsonProperty("inactive")
	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(String id) {
		this.id = id;
		setLink();
	}

	public void setLink(URI href) {
		this.link = new RestLinkRep("self", href);
	}

	@XmlElement(name = "create_timestamp")
	@JsonProperty("create_timestamp")
	public String getCreateTimestamp() {
		return createTimestamp;
	}

	@XmlElement(name = "update_timestamp")
	@JsonProperty("update_timestamp")
	public String getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setCreateTimestamp(String createTimestamp) {
		this.createTimestamp = createTimestamp;
	}

	public void setUpdateTimestamp(String updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	/**
	 * Set the hypermedia link to this object. Must be called only after object Id
	 * is set
	 */
	protected abstract void setLink();

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataObjectRestResponse [");
		if (id != null)
			builder.append("id=").append(id).append(", ");
		if (name != null)
			builder.append("name=").append(name).append(", ");
		if (createTimestamp != null)
			builder.append("createTimestamp=").append(createTimestamp).append(", ");
		if (createTimestamp != null)
			builder.append("updateTimestamp=").append(updateTimestamp).append(", ");
		if (link != null)
			builder.append("link=").append(link);
		builder.append("]");
		return builder.toString();
	}

}
