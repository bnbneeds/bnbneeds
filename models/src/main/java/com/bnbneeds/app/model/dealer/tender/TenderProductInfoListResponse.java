package com.bnbneeds.app.model.dealer.tender;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectInfoListResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "tender_product_list")
public class TenderProductInfoListResponse extends DataObjectInfoListResponse<TenderProductInfoResponse> {

	@Override
	@XmlElementWrapper(name = "tender_products")
	@XmlElement(name = "tender_product")
	@JsonProperty("tender_product")
	public List<TenderProductInfoResponse> getList() {
		return super.getList();
	}
}
