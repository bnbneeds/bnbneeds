package com.bnbneeds.app.model.review;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "null_review")
public class NullReviewInfoResponse extends ReviewInfoResponse {

	@Override
	public void setLink() {
	}

}
