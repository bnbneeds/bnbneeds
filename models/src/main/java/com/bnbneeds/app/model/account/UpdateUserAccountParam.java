package com.bnbneeds.app.model.account;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "update_user_account")
public class UpdateUserAccountParam {

	private String accountName;
	private String mobileNumber;
	private String username;
	private AccountType type;

	@XmlElement(name = "account_name")
	public String getAccountName() {
		return accountName;
	}

	@XmlElement(name = "mobile_number")
	public String getMobileNumber() {
		return mobileNumber;
	}

	@XmlElement(name = "email_address")
	@JsonProperty("email_address")
	public String getUsername() {
		return username;
	}

	@XmlElement(name = "account_type")
	@JsonProperty(value = "account_type")
	public AccountType getType() {
		return type;
	}

	public void setType(AccountType type) {
		this.type = type;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateUserAccountParam [");
		if (accountName != null)
			builder.append("accountName=").append(accountName).append(", ");
		if (mobileNumber != null)
			builder.append("mobileNumber=").append(mobileNumber).append(", ");
		if (username != null)
			builder.append("username=").append(username).append(", ");
		if (type != null)
			builder.append("type=").append(type);
		builder.append("]");
		return builder.toString();
	}

}
