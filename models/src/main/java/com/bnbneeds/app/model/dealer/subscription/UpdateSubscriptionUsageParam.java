package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "update_subscription_usage")
public class UpdateSubscriptionUsageParam {

	private String tenderId;

	@XmlElement(name = "tender_id")
	public String getTenderId() {
		return tenderId;
	}

	public void setTenderId(String tenderId) {
		this.tenderId = tenderId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateSubscriptionUsageParam [");
		if (tenderId != null)
			builder.append("tenderId=").append(tenderId);
		builder.append("]");
		return builder.toString();
	}

}
