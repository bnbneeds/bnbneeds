package com.bnbneeds.app.model.dealer.tender;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "update_tender_product")
public class UpdateTenderProductParam extends TenderProductParam {

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateTenderProductParam [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
