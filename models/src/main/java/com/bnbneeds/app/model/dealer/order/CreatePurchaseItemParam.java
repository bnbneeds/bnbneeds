package com.bnbneeds.app.model.dealer.order;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "create_purchase_item")
public class CreatePurchaseItemParam extends PurchaseItemParam {

	private String productNameId;

	@XmlElement(name = "product_name_id")
	public String getProductNameId() {
		return productNameId;
	}

	public void setProductNameId(String productNameId) {
		this.productNameId = productNameId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreatePurchaseItemParam [");
		builder.append(super.toString());
		if (productNameId != null)
			builder.append("productNameId=").append(productNameId);
		builder.append("]");
		return builder.toString();
	}

}
