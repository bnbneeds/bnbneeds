package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "update_subscription_status")
public class UpdateSubscriptionStatusParam {

	public static enum SubscriptionStatus {
		ACTIVATE, INITIATE_ACTIVATION, ACTIVATION_FAILED
	}

	private String status;

	@XmlElement(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setStatus(SubscriptionStatus status) {
		setStatus(status.name());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateSubscriptionStatusParam [");
		if (status != null)
			builder.append("status=").append(status);
		builder.append("]");
		return builder.toString();
	}

}
