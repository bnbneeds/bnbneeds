package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "job_info")
public class PipelineJobInfoResponse {

	private String id;
	private String status;

	@XmlElement(name = "id")
	public String getId() {
		return id;
	}

	@XmlElement(name = "status")
	public String getStatus() {
		return status;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
