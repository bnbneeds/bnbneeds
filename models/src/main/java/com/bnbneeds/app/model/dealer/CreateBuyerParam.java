package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "create_buyer")
public class CreateBuyerParam extends DealerParam {

	private String businessTypeId;

	@XmlElement(name = "business_type_id")
	public String getBusinessTypeId() {
		return businessTypeId;
	}

	public void setBusinessTypeId(String businessTypeId) {
		this.businessTypeId = businessTypeId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateBuyerParam [");
		builder.append(super.toString());
		if (businessTypeId != null)
			builder.append("businessType=").append(businessTypeId);
		builder.append("]");
		return builder.toString();
	}
	
}
