package com.bnbneeds.app.model.dealer.order;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "create_purchase_list")
public class CreatePurchaseListParam {

	private List<CreatePurchaseItemParam> purchaseList;

	@XmlElementWrapper(name = "items")
	@XmlElement(name = "item")
	@JsonProperty("item")
	public List<CreatePurchaseItemParam> getPurchaseList() {
		return purchaseList;
	}

	public void setPurchaseList(List<CreatePurchaseItemParam> purchaseList) {
		this.purchaseList = purchaseList;
	}

	public void add(CreatePurchaseItemParam itemParam) {
		if (purchaseList == null) {
			purchaseList = new ArrayList<CreatePurchaseItemParam>();
		}
		purchaseList.add(itemParam);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreatePurchaseListParam [");
		if (purchaseList != null)
			builder.append("purchaseList=").append(purchaseList);
		builder.append("]");
		return builder.toString();
	}

}
