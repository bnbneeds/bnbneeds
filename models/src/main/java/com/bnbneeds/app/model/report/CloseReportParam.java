package com.bnbneeds.app.model.report;

import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.EntityParam;

@XmlRootElement(name = "close_report")
public class CloseReportParam extends EntityParam {

	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CloseReportParam [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
