package com.bnbneeds.app.model.notification;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "nottification_list_param")
public class NotificationListParam {

	private List<String> notificationIdList;

	@XmlElementWrapper(name = "notification_id_list")
	@XmlElement(name = "notification_id")
	@JsonProperty("notification_id")
	public List<String> getNotificationIdList() {
		return notificationIdList;
	}

	public void setNotificationIdList(List<String> notificationIdList) {
		this.notificationIdList = notificationIdList;
	}

}
