package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "likely_to_buy_in")
public class LikelyToBuyInParam {
	private Integer value;
	private String timeUnit;

	public LikelyToBuyInParam() {
		super();
	}

	public LikelyToBuyInParam(Integer value, String timeUnit) {
		super();
		this.value = value;
		this.timeUnit = timeUnit;
	}

	@XmlElement(name = "value")
	public Integer getValue() {
		return value;
	}

	@XmlElement(name = "time_unit")
	public String getTimeUnit() {
		return timeUnit;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public void setTimeUnit(String timeUnit) {
		this.timeUnit = timeUnit;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LikelyToBuyIn [");
		if (value != null)
			builder.append("value=").append(value).append(", ");
		if (timeUnit != null)
			builder.append("timeUnit=").append(timeUnit);
		builder.append("]");
		return builder.toString();
	}

}