package com.bnbneeds.app.model.dealer.subscription;

import java.net.URI;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectRestResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class DealerBiddingSubscriptionInfoResponse extends DataObjectRestResponse {

	private NamedRelatedResourceRep subscriptionPlan;
	private String createdTimestamp;
	private String expiryTimestamp;
	private String activationTimestamp;
	private String subscriptionType;
	private String subscriptionState;
	private Quota quota;
	private String emailNotificationStatus;
	private RestLinkRep paymentTransactionsLink;

	@XmlElement(name = "created_timestamp")
	public String getCreatedTimestamp() {
		return createdTimestamp;
	}

	@XmlElement(name = "expiry_timestamp")
	public String getExpiryTimestamp() {
		return expiryTimestamp;
	}

	@XmlElement(name = "subscription_state")
	public String getSubscriptionState() {
		return subscriptionState;
	}

	@XmlElement(name = "email_notification_status")
	public String getEmailNotificationStatus() {
		return emailNotificationStatus;
	}

	@XmlElement(name = "subscription_type")
	public String getSubscriptionType() {
		return subscriptionType;
	}

	@XmlElement(name = "quota")
	public Quota getQuota() {
		return quota;
	}

	public void setQuota(Quota quota) {
		this.quota = quota;
	}

	public void setQuota(Integer subscribed, Integer used, Integer remaining) {
		Quota quota = new Quota(subscribed, used, remaining);
		setQuota(quota);
	}

	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	@XmlElement(name = "payment_transactions")
	public RestLinkRep getPaymentTransactionsLink() {
		return paymentTransactionsLink;
	}

	@XmlElement(name = "activation_timestamp")
	public String getActivationTimestamp() {
		return activationTimestamp;
	}

	public void setActivationTimestamp(String activationTimestamp) {
		this.activationTimestamp = activationTimestamp;
	}

	public void setPaymentTransactionsLink(RestLinkRep paymentTransactionsLink) {
		this.paymentTransactionsLink = paymentTransactionsLink;
	}

	@JsonIgnore
	public void setPaymentTransactionsLink(URI paymentTransactionsLink) {
		setPaymentTransactionsLink(new RestLinkRep("self", paymentTransactionsLink));
	}

	public void setEmailNotificationStatus(String emailNotificationStatus) {
		this.emailNotificationStatus = emailNotificationStatus;
	}

	public void setCreatedTimestamp(String createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public void setExpiryTimestamp(String expiryTimestamp) {
		this.expiryTimestamp = expiryTimestamp;
	}

	public void setSubscriptionState(String subscriptionState) {
		this.subscriptionState = subscriptionState;
	}

	@XmlElement(name = "subscription_plan")
	public NamedRelatedResourceRep getSubscriptionPlan() {
		return subscriptionPlan;
	}

	public void setSubscriptionPlan(NamedRelatedResourceRep subscriptionPlan) {
		this.subscriptionPlan = subscriptionPlan;
	}

	public abstract void setSubscriptionPlan(String id, String name);

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DealerBiddingSubscriptionInfoResponse [");
		if (subscriptionPlan != null)
			builder.append("subscriptionPlan=").append(subscriptionPlan).append(", ");
		if (createdTimestamp != null)
			builder.append("createdTimestamp=").append(createdTimestamp).append(", ");
		if (expiryTimestamp != null)
			builder.append("expiryTimestamp=").append(expiryTimestamp).append(", ");
		if (subscriptionType != null)
			builder.append("subscriptionType=").append(subscriptionType).append(", ");
		if (subscriptionState != null)
			builder.append("subscriptionState=").append(subscriptionState).append(", ");
		if (quota != null)
			builder.append("quota=").append(quota).append(", ");
		if (emailNotificationStatus != null)
			builder.append("emailNotificationStatus=").append(emailNotificationStatus).append(", ");
		if (paymentTransactionsLink != null)
			builder.append("paymentTransactionsLink=").append(paymentTransactionsLink);
		builder.append("]");
		return builder.toString();
	}

	@XmlRootElement(name = "quota")
	public static class Quota {
		private Integer subscribed;
		private Integer used;
		private Integer remaining;

		public Quota() {
			super();
		}

		public Quota(Integer subscribed, Integer used, Integer remaining) {
			super();
			this.subscribed = subscribed;
			this.used = used;
			this.remaining = remaining;
		}

		@XmlElement(name = "subscribed")
		public Integer getSubscribed() {
			return subscribed;
		}

		@XmlElement(name = "used")
		public Integer getUsed() {
			return used;
		}

		@XmlElement(name = "remaining")
		public Integer getRemaining() {
			return remaining;
		}

		public void setSubscribed(Integer subscribed) {
			this.subscribed = subscribed;
		}

		public void setUsed(Integer used) {
			this.used = used;
		}

		public void setRemaining(Integer remaining) {
			this.remaining = remaining;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("Quota [");
			if (subscribed != null)
				builder.append("subscribed=").append(subscribed).append(", ");
			if (used != null)
				builder.append("used=").append(used).append(", ");
			if (remaining != null)
				builder.append("remaining=").append(remaining);
			builder.append("]");
			return builder.toString();
		}

	}

}
