package com.bnbneeds.app.model.account;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "verify_account_request")
public class VerifyAccountParam {

	private String emailVerificationCode;
	private String smsVerificationCode;

	@XmlElement(name = "email_verification_code")
	public String getEmailVerificationCode() {
		return emailVerificationCode;
	}

	@XmlElement(name = "sms_verification_code")
	public String getSmsVerificationCode() {
		return smsVerificationCode;
	}

	public void setEmailVerificationCode(String emailVerificationCode) {
		this.emailVerificationCode = emailVerificationCode;
	}

	public void setSmsVerificationCode(String smsVerificationCode) {
		this.smsVerificationCode = smsVerificationCode;
	}

}
