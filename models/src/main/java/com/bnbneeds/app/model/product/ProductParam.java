package com.bnbneeds.app.model.product;

import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement
public class ProductParam {

	private String productCategoryId;
	private String productTypeId;
	private String productNameId;
	private String description;
	private String costPrice;
	private String modelId;
	private String serviceLocations;
	private Map<String, String> attributes;
	private String videoURL;
	private String tutorialVideoURL;

	public ProductParam() {

	}

	@XmlElement(name = "product_category_id")
	public String getProductCategoryId() {
		return productCategoryId;
	}

	@XmlElement(name = "product_type_id")
	public String getProductTypeId() {
		return productTypeId;
	}

	@XmlElement(name = "description")
	public String getDescription() {
		return description;
	}

	@XmlElement(name = "cost_price")
	public String getCostPrice() {
		return costPrice;
	}

	@XmlElement(name = "model_id")
	public String getModelId() {
		return modelId;
	}

	@XmlElement(name = "service_locations")
	public String getServiceLocations() {
		return serviceLocations;
	}

	@XmlElementWrapper(name = "attributes")
	@XmlElement(name = "attribute")
	@JsonProperty("attribute")
	public Map<String, String> getAttributes() {
		return attributes;
	}

	@XmlElement(name = "product_name_id")
	public String getProductNameId() {
		return productNameId;
	}

	@XmlElement(name = "video_url")
	public String getVideoURL() {
		return videoURL;
	}

	@XmlElement(name = "tutorial_video_url")
	public String getTutorialVideoURL() {
		return tutorialVideoURL;
	}

	public void setProductNameId(String productNameId) {
		this.productNameId = productNameId;
	}

	public void setProductCategoryId(String productCategoryId) {
		this.productCategoryId = productCategoryId;
	}

	public void setProductTypeId(String productTypeId) {
		this.productTypeId = productTypeId;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public void setServiceLocations(String serviceLocations) {
		this.serviceLocations = serviceLocations;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	public void setVideoURL(String videoURL) {
		this.videoURL = videoURL;
	}

	public void setTutorialVideoURL(String tutorialVideoURL) {
		this.tutorialVideoURL = tutorialVideoURL;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductParam [");
		if (productCategoryId != null) {
			builder.append("productCategoryId=");
			builder.append(productCategoryId);
			builder.append(", ");
		}
		if (productTypeId != null) {
			builder.append("productTypeId=");
			builder.append(productTypeId);
			builder.append(", ");
		}
		if (productNameId != null) {
			builder.append("productNameId=");
			builder.append(productNameId);
			builder.append(", ");
		}
		if (description != null) {
			builder.append("description=");
			builder.append(description);
			builder.append(", ");
		}
		if (costPrice != null) {
			builder.append("costPrice=");
			builder.append(costPrice);
			builder.append(", ");
		}
		if (modelId != null) {
			builder.append("modelId=");
			builder.append(modelId);
			builder.append(", ");
		}
		if (serviceLocations != null) {
			builder.append("serviceLocations=");
			builder.append(serviceLocations);
			builder.append(", ");
		}
		if (attributes != null) {
			builder.append("attributes=");
			builder.append(attributes);
		}
		builder.append("]");
		return builder.toString();
	}

}
