package com.bnbneeds.app.model.dealer.tender;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "create_tender_bid")
public class CreateTenderBidParam {

	private Double value;
	private String comments;

	@XmlElement(name = "value")
	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	@XmlElement(name = "comments")
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateTenderBidParam [");
		if (value != null)
			builder.append("value=").append(value).append(", ");
		if (comments != null)
			builder.append("comment=").append(comments);
		builder.append("]");
		return builder.toString();
	}

}
