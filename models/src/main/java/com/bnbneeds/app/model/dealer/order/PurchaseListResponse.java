package com.bnbneeds.app.model.dealer.order;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "purchase_list")
public class PurchaseListResponse extends DataObjectListResponse {

	private List<PurchaseItemInfoResponse> purchaseItems;

	public void addPurchaseItemInfo(PurchaseItemInfoResponse response) {
		if (this.purchaseItems == null) {
			this.purchaseItems = new ArrayList<PurchaseItemInfoResponse>();
		}

		if (response != null) {
			this.purchaseItems.add(response);
		}
	}

	@XmlElementWrapper(name = "items")
	@XmlElement(name = "item")
	@JsonProperty("item")
	public List<PurchaseItemInfoResponse> getPurchaseItems() {
		return purchaseItems;
	}

	public void setPurchaseItems(List<PurchaseItemInfoResponse> purchaseItems) {
		if (purchaseItems != null && !purchaseItems.isEmpty()) {
			this.purchaseItems = purchaseItems;
		}
	}

	@Override
	public int size() {
		return purchaseItems == null ? 0 : purchaseItems.size();
	}

}
