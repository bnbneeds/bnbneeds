package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "entity_count")
public class EntityCountResponse {

	private int count;

	public EntityCountResponse() {
	}

	public EntityCountResponse(int count) {
		super();
		this.count = count;
	}

	@XmlElement(name = "value")
	@JsonProperty("value")
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
