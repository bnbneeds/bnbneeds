package com.bnbneeds.app.model;

import java.net.URI;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class represent a link for the Rest API
 * 
 */
public class RestLinkRep {

	private String rel;
	private URI linkRef;

	public RestLinkRep() {
	}

	public RestLinkRep(String rel, URI ref) {
		this.rel = rel;
		this.linkRef = ref;
	}

	@XmlAttribute(name = "rel")
	public String getRel() {
		return rel;
	}

	public void setRel(String rel) {
		this.rel = rel;
	}

	@XmlAttribute(name = "href")
	public URI getLinkRef() {
		return linkRef;
	}

	public void setLinkRef(URI ref) {
		linkRef = ref;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rel == null) ? 0 : rel.hashCode());
		result = prime * result + ((linkRef == null) ? 0 : linkRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RestLinkRep other = (RestLinkRep) obj;
		if (rel == null) {
			if (other.rel != null) {
				return false;
			}
		} else if (!rel.equals(other.rel)) {
			return false;
		}
		if (linkRef == null) {
			if (other.linkRef != null) {
				return false;
			}
		} else if (!linkRef.equals(other.linkRef)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RestLinkRep [");
		if (rel != null)
			builder.append("rel=").append(rel).append(", ");
		if (linkRef != null)
			builder.append("linkRef=").append(linkRef);
		builder.append("]");
		return builder.toString();
	}

}
