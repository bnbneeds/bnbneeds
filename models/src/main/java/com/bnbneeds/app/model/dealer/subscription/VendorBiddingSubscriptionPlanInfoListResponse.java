package com.bnbneeds.app.model.dealer.subscription;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectInfoListResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "bidding_subscription_plan_list")
public class VendorBiddingSubscriptionPlanInfoListResponse
		extends DataObjectInfoListResponse<VendorBiddingSubscriptionPlanInfoResponse> {

	@Override
	@XmlElementWrapper(name = "bidding_subscription_plans")
	@XmlElement(name = "bidding_subscription_plan")
	@JsonProperty("bidding_subscription_plan")
	public List<VendorBiddingSubscriptionPlanInfoResponse> getList() {
		return super.getList();
	}

}
