package com.bnbneeds.app.model.review;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.bnbneeds.app.model.DescribedDataObjectRestResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;

@XmlRootElement(name = "review")
@XmlSeeAlso({ BuyerReviewInfoResponse.class, VendorReviewInfoResponse.class,
		ProductReviewInfoResponse.class })
public abstract class ReviewInfoResponse extends
		DescribedDataObjectRestResponse {

	private String timestamp;
	private Integer rating;
	private String headline;
	private NamedRelatedResourceRep reviewedBy;

	@XmlElement(name = "rating")
	public Integer getRating() {
		return rating;
	}

	@XmlElement(name = "headline")
	public String getHeadline() {
		return headline;
	}

	@XmlElement(name = "reviewed_by")
	public NamedRelatedResourceRep getReviewedBy() {
		return reviewedBy;
	}

	@XmlElement(name = "timestamp")
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public void setReviewedBy(NamedRelatedResourceRep reviewedBy) {
		this.reviewedBy = reviewedBy;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ReviewInfoResponse [");
		builder.append(super.toString());
		if (timestamp != null)
			builder.append("timestamp=").append(timestamp).append(", ");
		if (rating != null)
			builder.append("rating=").append(rating).append(", ");
		if (headline != null)
			builder.append("headline=").append(headline).append(", ");
		if (reviewedBy != null)
			builder.append("reviewedBy=").append(reviewedBy);
		builder.append("]");
		return builder.toString();
	}

}
