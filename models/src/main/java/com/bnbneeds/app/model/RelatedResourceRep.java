package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class RelatedResourceRep {

	private String id;
	private RestLinkRep selfLink;

	public RelatedResourceRep() {
	}

	public RelatedResourceRep(String id, RestLinkRep selfLink) {
		this.id = id;
		this.selfLink = selfLink;
	}

	/**
	 * ID of the related object
	 * 
	 * @valid none
	 */
	@XmlElement(name = "id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 
	 * A hyperlink to the related object
	 * 
	 * @valid none
	 */
	@XmlElement(name = "link")
	@JsonIgnore
	public RestLinkRep getLink() {
		return selfLink;
	}

	public void setLink(RestLinkRep link) {
		selfLink = link;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((selfLink == null) ? 0 : selfLink.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RelatedResourceRep other = (RelatedResourceRep) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (selfLink == null) {
			if (other.selfLink != null) {
				return false;
			}
		} else if (!selfLink.equals(other.selfLink)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return id.toString();
	}
}
