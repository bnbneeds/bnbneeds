package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "buyer")
public class BuyerInfoResponse extends DealerInfoResponse {

	private NamedRelatedResourceRep businessType;

	@XmlElement(name = "business_type")
	public NamedRelatedResourceRep getBusinessType() {
		return businessType;
	}

	public void setBusinessType(NamedRelatedResourceRep businessType) {
		this.businessType = businessType;
	}

	public void setBusinessType(String id, String name) {
		this.businessType = new NamedRelatedResourceRep(new RelatedResourceRep(
				id,
				new RestLinkRep("self", Endpoint.BUSINESS_TYPE_INFO.get(id))),
				name);
	}

	@Override
	public void setLink() {
		setLink(Endpoint.BUYER_INFO.get(getId()));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BuyerInfoResponse [");
		builder.append(super.toString());
		if (businessType != null)
			builder.append("businessType=").append(businessType);
		builder.append("]");
		return builder.toString();
	}

}
