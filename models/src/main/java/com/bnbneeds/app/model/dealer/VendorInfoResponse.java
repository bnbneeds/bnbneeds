package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "vendor")
public class VendorInfoResponse extends DealerInfoResponse {

	private String vatNumber;
	private String serviceTaxNumber;
	private String cities;
	private RestLinkRep productsLink;

	@XmlElement(name = "vat_number")
	public String getVatNumber() {
		return vatNumber;
	}

	@XmlElement(name = "service_tax_number")
	public String getServiceTaxNumber() {
		return serviceTaxNumber;
	}

	@XmlElement(name = "cities")
	public String getCities() {
		return cities;
	}

	@XmlElement(name = "link")
	public RestLinkRep getProductsLink() {
		return productsLink;
	}

	@Override
	public void setLink() {
		setLink(Endpoint.VENDOR_INFO.get(getId()));
		this.productsLink = new RestLinkRep("products",
				Endpoint.VENDOR_PRODUCTS.get(getId()));
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public void setServiceTaxNumber(String serviceTaxNumber) {
		this.serviceTaxNumber = serviceTaxNumber;
	}

	public void setCities(String cities) {
		this.cities = cities;
	}

	public void setProductsLink(RestLinkRep productsLink) {
		this.productsLink = productsLink;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VendorInfoResponse [");
		builder.append(super.toString());

		if (vatNumber != null) {
			builder.append("vatNumber=");
			builder.append(vatNumber);
			builder.append(", ");
		}
		if (serviceTaxNumber != null) {
			builder.append("serviceTaxNumber=");
			builder.append(serviceTaxNumber);
			builder.append(", ");
		}
		if (cities != null) {
			builder.append("cities=");
			builder.append(cities);
		}
		builder.append("]");
		return builder.toString();
	}

}
