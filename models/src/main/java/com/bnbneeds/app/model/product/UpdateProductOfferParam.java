package com.bnbneeds.app.model.product;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.EntityParam;

@XmlRootElement(name = "update_product_offer")
public class UpdateProductOfferParam extends EntityParam {

	private String offerTitle;
	private String startDate;
	private String endDate;

	@XmlElement(name = "offer_title")
	public String getOfferTitle() {
		return offerTitle;
	}
	
	@XmlElement(name = "start_date")
	public String getStartDate() {
		return startDate;
	}

	@XmlElement(name = "end_date")
	public String getEndDate() {
		return endDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setOfferTitle(String offerTitle) {
		this.offerTitle = offerTitle;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateProductOfferParam [");
		if (offerTitle != null)
			builder.append("offerTitle=").append(offerTitle).append(", ");
		if (startDate != null)
			builder.append("startDate=").append(startDate).append(", ");
		if (endDate != null)
			builder.append("endDate=").append(endDate);
		builder.append("]");
		return builder.toString();
	}
	
	

}
