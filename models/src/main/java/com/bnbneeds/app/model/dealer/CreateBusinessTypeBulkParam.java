package com.bnbneeds.app.model.dealer;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "create_business_types")
public class CreateBusinessTypeBulkParam {

	private List<CreateBusinessTypeParam> businessTypeList;

	@XmlElementWrapper(name = "business_types")
	@XmlElement(name = "business_type")
	@JsonProperty("business_type")
	public List<CreateBusinessTypeParam> getBusinessTypeList() {
		return businessTypeList;
	}

	public void setBusinessTypeList(
			List<CreateBusinessTypeParam> businessTypeList) {
		this.businessTypeList = businessTypeList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateBusinessTypeBulkParam [");
		if (businessTypeList != null) {
			builder.append("businessTypeList=");
			builder.append(businessTypeList);
		}
		builder.append("]");
		return builder.toString();
	}

	public void add(CreateBusinessTypeParam eachParam) {
		if (businessTypeList == null) {
			businessTypeList = new ArrayList<CreateBusinessTypeParam>();
		}
		businessTypeList.add(eachParam);

	}

}
