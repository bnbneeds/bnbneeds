package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "update_default_status_of_subscription_plan")
public class UpdateSubscriptionPlanDefaultStatus {

	private Boolean setAsDefaultPlan;

	@XmlElement(name = "default")
	@JsonProperty("default")
	public Boolean getSetAsDefaultPlan() {
		return setAsDefaultPlan;
	}

	public void setSetAsDefaultPlan(Boolean setAsDefaultPlan) {
		this.setAsDefaultPlan = setAsDefaultPlan;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateSubscriptionPlanAsDefaultPlanParam [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
