package com.bnbneeds.app.model.product;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.BlobResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "product_images")
public class ProductImageListResponse {

	private List<BlobResourceRep> imageLinks;
	private String productId;

	@XmlElement(name = "product_id")
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@XmlElement(name = "image")
	@JsonProperty("image")
	public List<BlobResourceRep> getImageLinks() {
		return imageLinks;
	}

	public void setImageLinks(List<BlobResourceRep> imageLinks) {
		this.imageLinks = imageLinks;
	}

	public void addImage(String key, String mediaType, Long size) {

		if (this.imageLinks == null) {
			this.imageLinks = new LinkedList<BlobResourceRep>();
		}
		this.imageLinks.add(new BlobResourceRep(key, mediaType, size,
				new RestLinkRep("self", Endpoint.PRODUCT_IMAGE.get(productId,
						key))));
	}

}
