package com.bnbneeds.app.model.product;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "product_name_list")
public class ProductNameListResponse extends DataObjectListResponse {

	@XmlElementWrapper(name = "product_names")
	@XmlElement(name = "product_name")
	@JsonProperty("product_name")
	public List<NamedRelatedResourceRep> getProductNameList() {
		return dataObjectList;
	}
}
