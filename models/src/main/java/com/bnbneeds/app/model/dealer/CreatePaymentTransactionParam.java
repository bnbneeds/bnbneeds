package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "create_payment_transaction")
public class CreatePaymentTransactionParam extends PaymentTransactionParam {

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreatePaymentTransactionParam [");
		if (super.toString() != null)
			builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
