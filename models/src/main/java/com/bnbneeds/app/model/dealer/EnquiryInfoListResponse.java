package com.bnbneeds.app.model.dealer;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "enquiry_list")
public class EnquiryInfoListResponse extends DataObjectListResponse {

	private List<EnquiryInfoResponse> enquiryList;

	@XmlElementWrapper(name = "enquiries")
	@XmlElement(name = "enquiry")
	@JsonProperty("enquiry")
	public List<EnquiryInfoResponse> getEnquiryList() {
		return enquiryList;
	}

	public void setEnquiryList(List<EnquiryInfoResponse> enquiryList) {
		this.enquiryList = enquiryList;
	}

	public void addEnquiryInfoResponse(EnquiryInfoResponse infoResponse) {
		if (enquiryList == null) {
			enquiryList = new ArrayList<EnquiryInfoResponse>();
		}
		enquiryList.add(infoResponse);
	}

	@Override
	public int size() {
		return enquiryList == null ? 0 : enquiryList.size();
	}

}
