package com.bnbneeds.app.model.report;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DescribedDataObjectRestResponse;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "abuse_report")
public class AbuseReportInfoResponse extends DescribedDataObjectRestResponse {

	private Long timestamp;
	private String title;
	private String entityId;
	private String entityType;
	private String resourceId;
	private String reportedByUserId;
	private String reportStatus;
	private String assignedUserId;
	private UserAccountInfoResponse reportedUser;
	private String assignedUserName;

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	@XmlElement(name = "reported_user")
	public UserAccountInfoResponse getReportedUser() {
		return reportedUser;
	}

	public void setReportedByUser(UserAccountInfoResponse reportedUser) {
		this.reportedUser = reportedUser;
	}

	@XmlElement(name = "assigned_user_name")
	public String getAssignedUserName() {
		return assignedUserName;
	}

	public void setAssignedUserName(String assignedUserName) {
		this.assignedUserName = assignedUserName;
	}

	@XmlElement(name = "report_status")
	public String getReportStatus() {
		return reportStatus;
	}

	public void setReportStatus(String reportStatus) {
		this.reportStatus = reportStatus;
	}

	@XmlElement(name = "assigned_user_id")
	public String getAssignedUserId() {
		return assignedUserId;
	}

	public void setAssignedUserId(String assignedUserId) {
		this.assignedUserId = assignedUserId;
	}

	@XmlElement(name = "resource_id")
	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	@XmlElement(name = "reported_by_user_id")
	public String getReportedByUserId() {
		return reportedByUserId;
	}

	public void setReportedByUserId(String reportedByUserId) {
		this.reportedByUserId = reportedByUserId;
	}

	@XmlElement(name = "timestamp")
	public Long getTimestamp() {
		return timestamp;
	}

	@XmlElement(name = "title")
	public String getTitle() {
		return title;
	}

	@XmlElement(name = "entity_id")
	public String getEntityId() {
		return entityId;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@Override
	protected void setLink() {
		setLink(Endpoint.ABUSE_REPORT_INFO.get(getId()));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AbuseReportInfoResponse [");
		builder.append(super.toString());
		if (timestamp != null)
			builder.append("timestamp=").append(timestamp).append(", ");
		if (title != null)
			builder.append("title=").append(title).append(", ");
		if (entityId != null)
			builder.append("entityId=").append(entityId);
		builder.append("]");
		return builder.toString();
	}

}
