package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "create_bidding_subscription_plan")
public class CreateVendorSubscriptionPlanParam extends BiddingSubscriptionPlanParam {

	private Integer numberOfTendersAllowedToAccess;

	@XmlElement(name = "number_of_tenders_allowed_to_access")
	public Integer getNumberOfTendersAllowedToAccess() {
		return numberOfTendersAllowedToAccess;
	}

	public void setNumberOfTendersAllowedToAccess(Integer numberOfTendersAllowedToAccess) {
		this.numberOfTendersAllowedToAccess = numberOfTendersAllowedToAccess;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateSubscriptionPlanParam [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
