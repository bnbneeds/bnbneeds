package com.bnbneeds.app.model.dealer.tender;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "update_tender_status")
public class UpdateTenderStatusParam {

	public static enum TenderStatus {
		OPEN, CANCEL, EDIT
	}

	private String status;
	private String reasonForCancellation;

	public UpdateTenderStatusParam() {
		super();
	}

	public UpdateTenderStatusParam(TenderStatus status) {
		super();
		setStatus(status);
	}

	@XmlElement(name = "status")
	public TenderStatus getStatus() {
		return TenderStatus.valueOf(status);
	}

	public void setStatus(TenderStatus status) {
		this.status = status.name();
	}

	@XmlElement(name = "reason_for_cancellation")
	public String getReasonForCancellation() {
		return reasonForCancellation;
	}

	public void setReasonForCancellation(String reasonForCancellation) {
		this.reasonForCancellation = reasonForCancellation;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TenderStatusParam [");
		if (status != null)
			builder.append("status=").append(status).append(", ");
		builder.append("]");
		return builder.toString();
	}

}
