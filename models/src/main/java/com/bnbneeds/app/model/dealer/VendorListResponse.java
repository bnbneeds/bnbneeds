package com.bnbneeds.app.model.dealer;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "vendor_list")
public class VendorListResponse extends DataObjectListResponse {

	@XmlElementWrapper(name = "vendors")
	@XmlElement(name = "vendor")
	@JsonProperty("vendor")
	public List<NamedRelatedResourceRep> getVendorList() {
		return dataObjectList;
	}

	public void setVendorList(List<NamedRelatedResourceRep> dataObjectList) {
		super.setNamedRelatedResourceList(dataObjectList);
	}

}