package com.bnbneeds.app.model.review;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectRestResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "average_rating")
public class AverageRatingResponse extends DataObjectRestResponse {

	private Double averageRating;
	private Double fiveStarPercent;
	private Double fourStarPercent;
	private Double threeStarPercent;
	private Double twoStarPercent;
	private Double oneStarPercent;

	public AverageRatingResponse() {
		super();
		setAverageRating(0.0);
		setFiveStarPercent(0.0);
		setFourStarPercent(0.0);
		setThreeStarPercent(0.0);
		setTwoStarPercent(0.0);
		setOneStarPercent(0.0);
	}

	@XmlElement(name = "value")
	@JsonProperty("value")
	public Double getAverageRating() {
		return averageRating;
	}

	@XmlElement(name = "five_star_percent")
	public Double getFiveStarPercent() {
		return fiveStarPercent;
	}

	@XmlElement(name = "four_star_percent")
	public Double getFourStarPercent() {
		return fourStarPercent;
	}

	@XmlElement(name = "three_star_percent")
	public Double getThreeStarPercent() {
		return threeStarPercent;
	}

	@XmlElement(name = "two_star_percent")
	public Double getTwoStarPercent() {
		return twoStarPercent;
	}

	@XmlElement(name = "one_star_percent")
	public Double getOneStarPercent() {
		return oneStarPercent;
	}

	public void setFiveStarPercent(Double fiveStarPercent) {
		this.fiveStarPercent = fiveStarPercent;
	}

	public void setFourStarPercent(Double fourStarPercent) {
		this.fourStarPercent = fourStarPercent;
	}

	public void setThreeStarPercent(Double threeStarPercent) {
		this.threeStarPercent = threeStarPercent;
	}

	public void setTwoStarPercent(Double twoStarPercent) {
		this.twoStarPercent = twoStarPercent;
	}

	public void setOneStarPercent(Double oneStarPercent) {
		this.oneStarPercent = oneStarPercent;
	}

	public void setAverageRating(Double averageRating) {
		this.averageRating = averageRating;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AverageRatingResponse [");
		if (averageRating != null)
			builder.append("averageRating=").append(averageRating).append(", ");
		if (fiveStarPercent != null)
			builder.append("fiveStarPercent=").append(fiveStarPercent).append(", ");
		if (fourStarPercent != null)
			builder.append("fourStarPercent=").append(fourStarPercent).append(", ");
		if (threeStarPercent != null)
			builder.append("threeStarPercent=").append(threeStarPercent).append(", ");
		if (twoStarPercent != null)
			builder.append("twoStarPercent=").append(twoStarPercent).append(", ");
		if (oneStarPercent != null)
			builder.append("oneStarPercent=").append(oneStarPercent);
		builder.append("]");
		return builder.toString();
	}

	@Override
	protected void setLink() {

	}

}
