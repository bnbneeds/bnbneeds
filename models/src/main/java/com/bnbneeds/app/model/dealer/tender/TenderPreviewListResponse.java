package com.bnbneeds.app.model.dealer.tender;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectInfoListResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "tender_preview_list")
public class TenderPreviewListResponse extends DataObjectInfoListResponse<TenderPreviewResponse> {

	@Override
	@XmlElementWrapper(name = "tender_previews")
	@XmlElement(name = "tender_preview")
	@JsonProperty("tender_preview")
	public List<TenderPreviewResponse> getList() {
		return super.getList();
	}
}
