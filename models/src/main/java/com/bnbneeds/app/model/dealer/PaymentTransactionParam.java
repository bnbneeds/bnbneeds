package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PaymentTransactionParam {

	private Long timestamp;
	private String transactionId;
	private String transactionStatus;
	private String transactionDescription;
	private String paymentGateway;
	private String paymentMethod;
	private Double amount;

	@XmlElement(name = "timestamp")
	public Long getTimestamp() {
		return timestamp;
	}

	@XmlElement(name = "transaction_id")
	public String getTransactionId() {
		return transactionId;
	}

	@XmlElement(name = "transaction_status")
	public String getTransactionStatus() {
		return transactionStatus;
	}

	@XmlElement(name = "transaction_description")
	public String getTransactionDescription() {
		return transactionDescription;
	}

	@XmlElement(name = "payment_gateway")
	public String getPaymentGateway() {
		return paymentGateway;
	}

	@XmlElement(name = "payment_method")
	public String getPaymentMethod() {
		return paymentMethod;
	}

	@XmlElement(name = "amount")
	public Double getAmount() {
		return amount;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PaymentTransactionParam [");
		if (timestamp != null)
			builder.append("timestamp=").append(timestamp).append(", ");
		if (transactionId != null)
			builder.append("transactionId=").append(transactionId).append(", ");
		if (transactionStatus != null)
			builder.append("transactionStatus=").append(transactionStatus).append(", ");
		if (transactionDescription != null)
			builder.append("transactionDescription=").append(transactionDescription).append(", ");
		if (paymentGateway != null)
			builder.append("paymentGateway=").append(paymentGateway).append(", ");
		if (paymentMethod != null)
			builder.append("paymentMethod=").append(paymentMethod).append(", ");
		if (amount != null)
			builder.append("amount=").append(amount);
		builder.append("]");
		return builder.toString();
	}

}
