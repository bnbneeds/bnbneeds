package com.bnbneeds.app.model.dealer;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "business_type_list")
public class BusinessTypeListResponse extends DataObjectListResponse {

	@XmlElementWrapper(name = "business_types")
	@XmlElement(name = "business_type")
	@JsonProperty("business_type")
	public List<NamedRelatedResourceRep> getBusinessTypeList() {
		return dataObjectList;
	}
}
