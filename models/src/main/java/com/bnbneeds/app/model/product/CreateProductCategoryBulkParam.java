package com.bnbneeds.app.model.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "create_product_categories")
public class CreateProductCategoryBulkParam {

	private List<CreateProductCategoryParam> productCategoryList;

	@XmlElementWrapper(name = "product_categories")
	@XmlElement(name = "product_category")
	@JsonProperty("product_category")
	public List<CreateProductCategoryParam> getProductCategoryList() {
		return productCategoryList;
	}

	public void setProductCategoryList(
			List<CreateProductCategoryParam> productCategoryList) {
		this.productCategoryList = productCategoryList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductCategoryBulkParam [");
		if (productCategoryList != null) {
			builder.append("productCategoryList=");
			builder.append(productCategoryList);
		}
		builder.append("]");
		return builder.toString();
	}

	public void add(CreateProductCategoryParam param) {
		if (productCategoryList == null) {
			productCategoryList = new ArrayList<CreateProductCategoryParam>();
		}
		productCategoryList.add(param);

	}

}
