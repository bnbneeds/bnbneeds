package com.bnbneeds.app.model.dealer.tender;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DescribedDataObjectRestResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "tender")
public class TenderInfoResponse extends DescribedDataObjectRestResponse {

	private Double expectedPrice;
	private String deliveryFrequency;
	private String paymentMode;
	private String bidValueType;
	private String creditTerms;
	private String deliveryTerms;
	private NamedRelatedResourceRep buyer;
	private String deliveryLocation;
	private String status;
	private String closeTimestamp;
	private String openTimestamp;
	private List<TenderProductInfoResponse> bidProducts;

	@XmlElement(name = "open_timestamp")
	public String getOpenTimestamp() {
		return openTimestamp;
	}

	public void setOpenTimestamp(String openTimestamp) {
		this.openTimestamp = openTimestamp;
	}

	@XmlElement(name = "close_timestamp")
	public String getCloseTimestamp() {
		return closeTimestamp;
	}

	public void setCloseTimestamp(String closeTimestamp) {
		this.closeTimestamp = closeTimestamp;
	}

	@XmlElement(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@XmlElement(name = "buyer")
	public NamedRelatedResourceRep getBuyer() {
		return buyer;
	}

	@XmlElement(name = "expected_price")
	public Double getExpectedPrice() {
		return expectedPrice;
	}

	@XmlElement(name = "delivery_frequency")
	public String getDeliveryFrequency() {
		return deliveryFrequency;
	}

	@XmlElement(name = "bid_value_type")
	public String getBidValueType() {
		return bidValueType;
	}

	@XmlElement(name = "payment_mode")
	public String getPaymentMode() {
		return paymentMode;
	}

	@XmlElement(name = "credit_terms")
	public String getCreditTerms() {
		return creditTerms;
	}

	@XmlElement(name = "delivery_terms")
	public String getDeliveryTerms() {
		return deliveryTerms;
	}

	@XmlElement(name = "delivery_location")
	public String getDeliveryLocation() {
		return deliveryLocation;
	}

	public void addBidProduct(TenderProductInfoResponse bidProduct) {
		if (bidProducts == null) {
			bidProducts = new ArrayList<>();
		}
		bidProducts.add(bidProduct);
	}

	@XmlElementWrapper(name = "bid_products")
	@XmlElement(name = "bid_product")
	@JsonProperty("bid_product")
	public List<TenderProductInfoResponse> getBidProducts() {
		return bidProducts;
	}

	public void setBidProducts(List<TenderProductInfoResponse> bidProducts) {
		this.bidProducts = bidProducts;
	}

	public void setDeliveryLocation(String deliveryLocation) {
		this.deliveryLocation = deliveryLocation;
	}

	public void setBuyer(NamedRelatedResourceRep buyer) {
		this.buyer = buyer;
	}

	public void setBuyer(String buyerId, String buyerName) {
		setBuyer(new NamedRelatedResourceRep(buyerId, new RestLinkRep("self", Endpoint.BUYER_INFO.get(buyerId)),
				buyerName));
	}

	public void setExpectedPrice(Double expectedPrice) {
		this.expectedPrice = expectedPrice;
	}

	public void setExpectedPrice(double expectedPrice) {
		this.expectedPrice = expectedPrice;
	}

	public void setDeliveryFrequency(String deliveryFrequency) {
		this.deliveryFrequency = deliveryFrequency;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public void setBidValueType(String bidValueType) {
		this.bidValueType = bidValueType;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}

	@Override
	protected void setLink() {
		// This is intentionally left blank
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TenderInfoResponse [");
		builder.append(super.toString());
		if (expectedPrice != null)
			builder.append("expectedPrice=").append(expectedPrice).append(", ");
		if (deliveryFrequency != null)
			builder.append("deliveryFrequency=").append(deliveryFrequency).append(", ");
		if (paymentMode != null)
			builder.append("paymentMode=").append(paymentMode).append(", ");
		if (bidValueType != null)
			builder.append("bidValueType=").append(bidValueType).append(", ");
		if (creditTerms != null)
			builder.append("creditTerms=").append(creditTerms).append(", ");
		if (deliveryTerms != null)
			builder.append("deliveryTerms=").append(deliveryTerms).append(", ");
		if (buyer != null)
			builder.append("buyer=").append(buyer).append(", ");
		if (deliveryLocation != null)
			builder.append("deliveryLocation=").append(deliveryLocation);
		builder.append("]");
		return builder.toString();
	}

}
