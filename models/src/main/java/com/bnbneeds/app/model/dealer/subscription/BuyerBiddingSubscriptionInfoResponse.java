package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "buyer_bidding_subscription")
public class BuyerBiddingSubscriptionInfoResponse extends DealerBiddingSubscriptionInfoResponse {

	private NamedRelatedResourceRep buyer;
	private Integer numberOfTendersCreated;

	@XmlElement(name = "buyer")
	public NamedRelatedResourceRep getBuyer() {
		return buyer;
	}

	@XmlElement(name = "number_of_tenders_created")
	public Integer getNumberOfTendersCreated() {
		return numberOfTendersCreated;
	}

	public void setBuyer(NamedRelatedResourceRep buyer) {
		this.buyer = buyer;
	}

	public void setBuyer(String buyerId, String name) {
		this.buyer = new NamedRelatedResourceRep(
				new RelatedResourceRep(buyerId, new RestLinkRep("self", Endpoint.BUYER_INFO.get(buyerId))), name);
	}

	public void setNumberOfTendersCreated(Integer numberOfTendersCreated) {
		this.numberOfTendersCreated = numberOfTendersCreated;
	}

	@Override
	protected void setLink() {
		// This is intentionally left blank
	}

	@Override
	public void setSubscriptionPlan(String id, String name) {
		setSubscriptionPlan(new NamedRelatedResourceRep(new RelatedResourceRep(id,
				new RestLinkRep("self", Endpoint.BUYER_BIDDING_SUBSCRIPTION_PLAN_INFO.get(id))), name));

	}

}
