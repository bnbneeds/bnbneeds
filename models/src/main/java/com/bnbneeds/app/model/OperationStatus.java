package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(String.class)
public enum OperationStatus {

	@XmlEnumValue("SUCCESS") SUCCESS,
	@XmlEnumValue("PENDING") PENDING,
	@XmlEnumValue("FAIL") FAIL,
	@XmlEnumValue("ERROR") ERROR;

}
