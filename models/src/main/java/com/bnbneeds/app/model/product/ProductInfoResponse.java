package com.bnbneeds.app.model.product;

import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DescribedDataObjectRestResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "product")
public class ProductInfoResponse extends DescribedDataObjectRestResponse {

	private NamedRelatedResourceRep productName;
	private NamedRelatedResourceRep productCategory;
	private NamedRelatedResourceRep productType;
	private NamedRelatedResourceRep vendor;
	private Double costPrice;
	private String modelId;
	private String serviceLocations;
	private Map<String, String> attributes;
	private RestLinkRep images;
	private ProductImageListResponse imageList;
	private String videoURL;
	private String tutorialVideoURL;

	@XmlElement(name = "product_category")
	public RelatedResourceRep getProductCategory() {
		return productCategory;
	}

	@XmlElement(name = "product_type")
	public RelatedResourceRep getProductType() {
		return productType;
	}

	@XmlElement(name = "cost_price")
	public Double getCostPrice() {
		return costPrice;
	}

	@XmlElement(name = "model_id")
	public String getModelId() {
		return modelId;
	}

	@XmlElement(name = "service_locations")
	public String getServiceLocations() {
		return serviceLocations;
	}

	@XmlElement(name = "attributes")
	public Map<String, String> getAttributes() {
		return attributes;
	}

	@XmlElement(name = "vendor")
	public RelatedResourceRep getVendor() {
		return vendor;
	}

	@XmlElement(name = "link")
	public RestLinkRep getImages() {
		return images;
	}

	@XmlElement(name = "product_name")
	public NamedRelatedResourceRep getProductName() {
		return productName;
	}

	@XmlElement(name = "image_list")
	public ProductImageListResponse getImageList() {
		return imageList;
	}

	@XmlElement(name = "video_url")
	public String getVideoURL() {
		return videoURL;
	}

	@XmlElement(name = "tutorial_video_url")
	public String getTutorialVideoURL() {
		return tutorialVideoURL;
	}

	public void setVideoURL(String videoURL) {
		this.videoURL = videoURL;
	}

	public void setTutorialVideoURL(String tutorialVideoURL) {
		this.tutorialVideoURL = tutorialVideoURL;
	}

	public void setImageList(ProductImageListResponse imageList) {
		this.imageList = imageList;
	}

	public void setProductName(String id, String name) {
		this.productName = new NamedRelatedResourceRep(
				new RelatedResourceRep(id, new RestLinkRep("self",
						Endpoint.PRODUCT_NAME_INFO.get(id))), name);
	}

	public void setProductName(NamedRelatedResourceRep productName) {
		this.productName = productName;
	}

	public void setVendorByIdAndName(String vendorId, String vendorName) {
		this.vendor = new NamedRelatedResourceRep(vendorId, new RestLinkRep(
				"self", Endpoint.VENDOR_INFO.get(vendorId)), vendorName);
	}

	public void setVendor(NamedRelatedResourceRep vendor) {
		this.vendor = vendor;
	}

	public void setProductCategoryById(String productCategoryId, String name) {
		this.productCategory = new NamedRelatedResourceRep(productCategoryId,
				new RestLinkRep("self", Endpoint.PRODUCT_CATEGORY_INFO
						.get(productCategoryId)),
				name);
	}

	public void setProductCategory(NamedRelatedResourceRep productCategory) {
		this.productCategory = productCategory;
	}

	public void setProductTypeById(String productTypeId, String name) {
		this.productType = new NamedRelatedResourceRep(productTypeId,
				new RestLinkRep("self",
						Endpoint.PRODUCT_TYPE_INFO.get(productTypeId)), name);
	}

	public void setProductType(NamedRelatedResourceRep productType) {
		this.productType = productType;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public void setServiceLocations(String serviceLocations) {
		this.serviceLocations = serviceLocations;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	@Override
	public void setLink() {
		setLink(Endpoint.PRODUCT_INFO.get(getId()));
		this.images = new RestLinkRep("images",
				Endpoint.PRODUCT_IMAGES.get(getId()));
	}

}
