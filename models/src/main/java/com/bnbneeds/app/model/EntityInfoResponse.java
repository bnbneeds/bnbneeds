package com.bnbneeds.app.model;

public class EntityInfoResponse extends DescribedDataObjectRestResponse {

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EntityInfoResponse [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

	@Override
	protected void setLink() {
		// Intentionally left balnk
	}

}
