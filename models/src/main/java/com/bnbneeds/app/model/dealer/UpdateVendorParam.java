package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "update_vendor")
public class UpdateVendorParam extends CreateVendorParam {

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateVendorParam [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
