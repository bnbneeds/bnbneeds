package com.bnbneeds.app.model.account;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "create_user_account")
public class CreateUserAccountParam {

	private String accountName;
	private String username;
	private String password;
	private AccountType type;
	private String mobileNumber;

	@XmlElement(name = "email_address")
	@JsonProperty(value = "email_address")
	public String getUsername() {
		return username;
	}

	@XmlElement(name = "password")
	public String getPassword() {
		return password;
	}

	@XmlElement(name = "account_type")
	@JsonProperty(value = "account_type")
	public AccountType getType() {
		return type;
	}

	@XmlElement(name = "account_name")
	public String getAccountName() {
		return accountName;
	}

	@XmlElement(name = "mobile_number")
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setType(AccountType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateAccountParam [");
		builder.append(super.toString());
		if (accountName != null)
			builder.append("accountName=").append(accountName).append(", ");
		if (username != null)
			builder.append("username=").append(username).append(", ");
		if (password != null)
			builder.append("password=").append("[****], ");
		if (type != null)
			builder.append("type=").append(type).append(", ");
		if (mobileNumber != null)
			builder.append("mobileNumber=").append(mobileNumber);
		builder.append("]");
		return builder.toString();
	}

}
