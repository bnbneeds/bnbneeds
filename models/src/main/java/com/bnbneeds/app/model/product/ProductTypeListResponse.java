package com.bnbneeds.app.model.product;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "product_type_list")
public class ProductTypeListResponse extends DataObjectListResponse {

	@XmlElementWrapper(name = "product_types")
	@XmlElement(name = "product_type")
	@JsonProperty("product_type")
	public List<NamedRelatedResourceRep> getProductTypeList() {
		return dataObjectList;
	}

}
