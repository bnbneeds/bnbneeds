package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "response")
@XmlType(propOrder = { "httpCode", "status", "message", "data" })
public class TaskResponse {

	private int httpCode;
	private OperationStatus status;
	private String message;
	private String data;

	public TaskResponse() {
		super();
	}

	public TaskResponse(int httpCode, OperationStatus status, String message,
			String data) {
		super();
		this.httpCode = httpCode;
		this.status = status;
		this.message = message;
		this.data = data;
	}

	@XmlElement(name = "status")
	public OperationStatus getStatus() {
		return status;
	}

	public void setStatus(OperationStatus status) {
		this.status = status;
	}

	@XmlElement(name = "code")
	// @JsonProperty("code")
	public int getHttpCode() {
		return httpCode;
	}

	@XmlElement(name = "message")
	public String getMessage() {
		return message;
	}

	@XmlElement(name = "data")
	public String getData() {
		return data;
	}

	public void setCode(int httpCode) {
		this.httpCode = httpCode;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TaskResponse [httpCode=").append(httpCode);
		if (status != null)
			builder.append(", ").append("status=").append(status);
		if (message != null)
			builder.append(", ").append("message=").append(message);
		if (data != null)
			builder.append(", ").append("data=").append(data);
		builder.append("]");
		return builder.toString();
	}

}
