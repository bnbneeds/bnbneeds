package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "entity_approval")
public class EntityApprovalParam {
	
	public static enum EntityStatus {
		APPROVED, DISAPPROVED, BLACKLISTED;
	}

	private String status;
	private String remarks;

	@XmlElement(name = "status")
	public EntityStatus getStatus() {
		return EntityStatus.valueOf(status);
	}
	

	@XmlElement(name = "remarks")
	public String getRemarks() {
		return remarks;
	}

	public void setStatus(EntityStatus entityStatus) {
		this.status = entityStatus.name();
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResourceApprovalParam [");
		if (status != null)
			builder.append("status=").append(status).append(", ");
		if (remarks != null)
			builder.append("remarks=").append(remarks);
		builder.append("]");
		return builder.toString();
	}

}
