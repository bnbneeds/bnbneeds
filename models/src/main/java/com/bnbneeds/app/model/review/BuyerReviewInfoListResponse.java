package com.bnbneeds.app.model.review;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "review_list")
public class BuyerReviewInfoListResponse extends
		ReviewInfoListResponse<BuyerReviewInfoResponse> {

}
