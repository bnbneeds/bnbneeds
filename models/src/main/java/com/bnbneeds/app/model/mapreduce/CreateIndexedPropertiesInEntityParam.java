package com.bnbneeds.app.model.mapreduce;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "create_indexed_properties_in_entities")
public class CreateIndexedPropertiesInEntityParam {

	private List<String> entityTypes;
	private List<String> properties;

	@XmlElementWrapper(name = "entity_types")
	@XmlElement(name = "entity_type")
	@JsonProperty("entity_type")
	public List<String> getEntityTypes() {
		return entityTypes;
	}

	@XmlElementWrapper(name = "properties")
	@XmlElement(name = "property")
	@JsonProperty("property")
	public List<String> getProperties() {
		return properties;
	}

	public void setEntityTypes(List<String> entityTypes) {
		this.entityTypes = entityTypes;
	}

	public void setProperties(List<String> properties) {
		this.properties = properties;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateIndexedPropertiesInEntityParam [");
		if (entityTypes != null)
			builder.append("entityTypes=").append(entityTypes).append(", ");
		if (properties != null)
			builder.append("properties=").append(properties);
		builder.append("]");
		return builder.toString();
	}

}
