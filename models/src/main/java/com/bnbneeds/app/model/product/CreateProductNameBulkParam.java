package com.bnbneeds.app.model.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "create_product_names")
public class CreateProductNameBulkParam {

	private List<CreateProductNameParam> productNameList;

	@XmlElementWrapper(name = "product_names")
	@XmlElement(name = "product_name")
	@JsonProperty("product_name")
	public List<CreateProductNameParam> getProductNameList() {
		return productNameList;
	}

	public void setProductNameList(List<CreateProductNameParam> productNameList) {
		this.productNameList = productNameList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductNameBulkParam [");
		if (productNameList != null) {
			builder.append("productNameList=");
			builder.append(productNameList);
		}
		builder.append("]");
		return builder.toString();
	}

	public void add(CreateProductNameParam eachParam) {
		if (productNameList == null) {
			productNameList = new ArrayList<CreateProductNameParam>();
		}
		productNameList.add(eachParam);
	}

}
