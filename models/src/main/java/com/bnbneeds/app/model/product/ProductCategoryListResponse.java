package com.bnbneeds.app.model.product;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "product_category_list")
public class ProductCategoryListResponse extends DataObjectListResponse {

	@XmlElementWrapper(name = "product_categories")
	@XmlElement(name = "product_category")
	@JsonProperty("product_category")
	public List<NamedRelatedResourceRep> getProductCategoryList() {
		return dataObjectList;
	}
}
