package com.bnbneeds.app.model.review;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "vendor_review")
public class VendorReviewInfoResponse extends ReviewInfoResponse {

	private NamedRelatedResourceRep vendor;

	@XmlElement(name = "vendor")
	public NamedRelatedResourceRep getVendor() {
		return vendor;
	}

	public void setVendor(NamedRelatedResourceRep vendor) {
		this.vendor = vendor;
	}

	public void setVendor(String id, String name) {
		setVendor(new NamedRelatedResourceRep(id, new RestLinkRep("self",
				Endpoint.VENDOR_INFO.get(id)), name));
	}

	public void setReviewedBy(String id, String name) {
		setReviewedBy(new NamedRelatedResourceRep(id, new RestLinkRep("self",
				Endpoint.BUYER_INFO.get(id)), name));
	}

	@Override
	public void setLink() {
	}

}
