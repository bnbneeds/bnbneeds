package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DescribedDataObjectRestResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "business_type")
public class BusinessTypeInfoResponse extends DescribedDataObjectRestResponse {

	public void setLink() {
		setLink(Endpoint.BUSINESS_TYPE_INFO.get(getId()));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BusinessTypeInfoResponse [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
