package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "update_entity")
public class UpdateEntityParam extends EntityParam {

	private String status;
	private String remark;
	

	@XmlElement(name = "status")
	public String getStatus() {
		return status;
	}

	@XmlElement(name = "remark")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateEntityParam [");
		builder.append(super.toString());
		if (status != null)
			builder.append("status=").append(status).append(", ");
		if (remark != null)
			builder.append("remark=").append(remark);
		builder.append("]");
		return builder.toString();
	}

}
