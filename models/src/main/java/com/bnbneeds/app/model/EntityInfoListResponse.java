package com.bnbneeds.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "entity_list")
public class EntityInfoListResponse {

	private List<EntityInfoResponse> entityInfoList;
	private String nextOffset;

	@XmlElementWrapper(name = "entities")
	@XmlElement(name = "entity")
	@JsonProperty("entity")
	public List<EntityInfoResponse> getEntityInfoList() {
		return entityInfoList;
	}

	public void setEntityInfoList(List<EntityInfoResponse> entityInfoList) {
		this.entityInfoList = entityInfoList;
	}

	public void add(EntityInfoResponse response) {
		if (entityInfoList == null) {
			entityInfoList = new ArrayList<EntityInfoResponse>();
		}
		entityInfoList.add(response);
	}

	@XmlElement(name = "next_offset")
	public String getNextOffset() {
		return nextOffset;
	}

	public void setNextOffset(String nextOffset) {
		this.nextOffset = nextOffset;
	}

	public int size() {
		return entityInfoList == null ? 0 : entityInfoList.size();
	}

}
