package com.bnbneeds.app.model.dealer.order;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "update_purchase_item")
public class UpdatePurchaseItemParam extends PurchaseItemParam {

	private String purchaseItemId;

	@XmlElement(name = "item_id")
	@JsonProperty("item_id")
	public String getPurchaseItemId() {
		return purchaseItemId;
	}

	public void setPurchaseItemId(String purchaseItemId) {
		this.purchaseItemId = purchaseItemId;
	}

}
