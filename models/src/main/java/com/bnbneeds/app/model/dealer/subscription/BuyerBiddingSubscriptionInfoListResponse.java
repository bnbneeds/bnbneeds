package com.bnbneeds.app.model.dealer.subscription;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectInfoListResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "buyer_bidding_subscription_list")
public class BuyerBiddingSubscriptionInfoListResponse
		extends DataObjectInfoListResponse<BuyerBiddingSubscriptionInfoResponse> {

	@Override
	@XmlElementWrapper(name = "subscriptions")
	@XmlElement(name = "subscription")
	@JsonProperty("subscription")
	public List<BuyerBiddingSubscriptionInfoResponse> getList() {
		return super.getList();
	}
}
