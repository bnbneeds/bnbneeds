package com.bnbneeds.app.model.report;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "abuse_report_list")
public class AbuseReportInfoListResponse  {
	
	private List<AbuseReportInfoResponse> abuseReportsList;
	private String nextOffset;

	@XmlElementWrapper(name = "abuse_reports")
	@XmlElement(name = "abuse_report")
	@JsonProperty("abuse_report")
	public List<AbuseReportInfoResponse> getAbuseReportList() {
		return abuseReportsList;
	}
	
	public void setAbuseReportList(List<AbuseReportInfoResponse> abuseReportsList) {
		this.abuseReportsList = abuseReportsList;
	}

	public void add(AbuseReportInfoResponse response) {
		if (abuseReportsList == null) {
			abuseReportsList = new ArrayList<AbuseReportInfoResponse>();
		}
		abuseReportsList.add(response);
	}

	@XmlElement(name = "next_offset")
	public String getNextOffset() {
		return nextOffset;
	}

	public void setNextOffset(String nextOffset) {
		this.nextOffset = nextOffset;
	}

	public int size() {
		return abuseReportsList == null ? 0 : abuseReportsList.size();
	}
	
}
