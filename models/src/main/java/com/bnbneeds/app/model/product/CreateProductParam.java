package com.bnbneeds.app.model.product;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "create_product")
public class CreateProductParam extends ProductParam {

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateProductParam [");
		if (super.toString() != null)
			builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
