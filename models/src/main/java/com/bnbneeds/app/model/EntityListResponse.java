package com.bnbneeds.app.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "entity_list")
public class EntityListResponse extends DataObjectListResponse {

	private String entityType;
	private String entityStatus;

	@XmlElement(name = "entity_type")
	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	@XmlElement(name = "entity_status")
	public String getEntityStatus() {
		return entityStatus;
	}

	public void setEntityStatus(String entityStatus) {
		this.entityStatus = entityStatus;
	}

	@XmlElementWrapper(name = "entities")
	@XmlElement(name = "entity")
	public List<NamedRelatedResourceRep> getEntityList() {
		return this.dataObjectList;

	}

}
