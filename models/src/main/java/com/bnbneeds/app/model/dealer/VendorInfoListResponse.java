package com.bnbneeds.app.model.dealer;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "vendor_list")
public class VendorInfoListResponse {

	private List<VendorInfoResponse> vendorList;
	private String nextOffset;

	@XmlElementWrapper(name = "vendors")
	@XmlElement(name = "vendor")
	@JsonProperty("vendor")
	public List<VendorInfoResponse> getVendorList() {
		return vendorList;
	}

	@XmlElement(name = "next_offset")
	public String getNextOffset() {
		return nextOffset;
	}

	public void setVendorList(List<VendorInfoResponse> vendorList) {
		this.vendorList = vendorList;
	}

	public void setNextOffset(String nextOffset) {
		this.nextOffset = nextOffset;
	}

	public void add(VendorInfoResponse vendor) {
		if (vendorList == null) {
			vendorList = new ArrayList<VendorInfoResponse>();
		}
		vendorList.add(vendor);
	}

	public int size() {
		return vendorList == null ? 0 : vendorList.size();
	}

}
