package com.bnbneeds.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class DataObjectListResponse {

	protected List<NamedRelatedResourceRep> dataObjectList;
	private String nextOffset;

	public void setNamedRelatedResourceList(
			List<NamedRelatedResourceRep> dataObjectList) {

		if (dataObjectList != null && !dataObjectList.isEmpty()) {
			this.dataObjectList = dataObjectList;
		}
	}

	public void addNamedRelatedResourceList(
			List<NamedRelatedResourceRep> dataObjectList) {

		if (dataObjectList != null) {
			if (this.dataObjectList == null) {
				this.dataObjectList = new ArrayList<NamedRelatedResourceRep>();
			}
			this.dataObjectList.addAll(dataObjectList);
		}
	}

	public void addNamedResourceResponse(NamedRelatedResourceRep resourceRep) {
		if (dataObjectList == null) {
			dataObjectList = new ArrayList<NamedRelatedResourceRep>();
		}
		dataObjectList.add(resourceRep);
	}

	@XmlElement(name = "next_offset")
	public String getNextOffset() {
		return nextOffset;
	}

	public void setNextOffset(String nextOffset) {
		if (nextOffset != null && !nextOffset.isEmpty()) {
			this.nextOffset = nextOffset;
		}
	}

	public int size() {
		return dataObjectList == null ? 0 : dataObjectList.size();
	}

}
