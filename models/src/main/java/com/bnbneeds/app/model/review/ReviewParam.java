package com.bnbneeds.app.model.review;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "review")
public class ReviewParam {

	private Integer rating;
	private String headline;
	private String description;

	@XmlElement(name = "rating")
	public Integer getRating() {
		return rating;
	}

	@XmlElement(name = "headline")
	public String getHeadline() {
		return headline;
	}

	@XmlElement(name = "description")
	public String getDescription() {
		return description;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ReviewParam [");
		if (rating != null)
			builder.append("rating=").append(rating).append(", ");
		if (headline != null)
			builder.append("headline=").append(headline).append(", ");
		if (description != null)
			builder.append("description=").append(description);
		builder.append("]");
		return builder.toString();
	}

}
