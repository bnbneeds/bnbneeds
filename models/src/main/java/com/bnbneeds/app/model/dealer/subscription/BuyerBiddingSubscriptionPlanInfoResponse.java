package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "bidding_subscription_plans")
public class BuyerBiddingSubscriptionPlanInfoResponse extends SubscriptionPlanInfoResponse {

	private Integer numberOfTendersAllowedToCreate;

	@XmlElement(name = "number_of_tenders_allowed_to_create")
	public Integer getNumberOfTendersAllowedToCreate() {
		return numberOfTendersAllowedToCreate;
	}

	public void setNumberOfTendersAllowedToCreate(Integer numberOfTendersAllowedToCreate) {
		this.numberOfTendersAllowedToCreate = numberOfTendersAllowedToCreate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SubscriptionPlansResponse [");
		if (numberOfTendersAllowedToCreate != null)
			builder.append("numberOfTendersAllowedToCreate=").append(numberOfTendersAllowedToCreate).append(", ");
		builder.append("]");
		return builder.toString();
	}

	@Override
	protected void setLink() {
		setLink(Endpoint.BUYER_BIDDING_SUBSCRIPTION_PLAN_INFO.get(getId()));
	}

}
