package com.bnbneeds.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class ApprovedDataObjectRestResponse extends
		DataObjectRestResponse {

	private String entityStatus;
	private List<String> remarks;

	@XmlElementWrapper(name = "remarks")
	@XmlElement(name = "remark")
	@JsonProperty("remark")
	public List<String> getRemarks() {
		return remarks;
	}

	@XmlElement(name = "entity_status")
	@JsonProperty("entity_status")
	public String getEntityStatus() {
		return entityStatus;
	}

	public void setEntityStatus(String entityStatus) {
		this.entityStatus = entityStatus;
	}

	public void setRemarks(List<String> remarks) {
		if (remarks != null && !remarks.isEmpty())
			this.remarks = remarks;
	}

	public void addRemark(String remark) {
		if (remark != null && !remark.isEmpty()) {
			if (remarks == null) {
				remarks = new ArrayList<String>();
			}
			remarks.add(remark);
		}
	}

}
