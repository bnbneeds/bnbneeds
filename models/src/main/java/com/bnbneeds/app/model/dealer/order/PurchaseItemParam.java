package com.bnbneeds.app.model.dealer.order;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "purchase_item")
public class PurchaseItemParam {

	private String description;
	private String vendorId;
	private Double requiredQuantity;
	private Double deliveredQuantity;
	private String unit;
	private PurchaseStatus statusUpdate;
	private String tags;

	@XmlElement(name = "required_quantity")
	public Double getRequiredQuantity() {
		return requiredQuantity;
	}

	@XmlElement(name = "delivered_quantity")
	public Double getDeliveredQuantity() {
		return deliveredQuantity;
	}

	@XmlElement(name = "status_update")
	public PurchaseStatus getStatusUpdate() {
		return statusUpdate;
	}

	@XmlElement(name = "vendor_id")
	public String getVendorId() {
		return vendorId;
	}

	@XmlElement(name = "description")
	public String getDescription() {
		return description;
	}

	@XmlElement(name = "unit")
	public String getUnit() {
		return unit;
	}

	@XmlElement(name = "tags")
	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public void setDeliveredQuantity(Double deliveredQuantity) {
		this.deliveredQuantity = deliveredQuantity;
	}

	public void setStatusUpdate(PurchaseStatus statusUpdate) {
		this.statusUpdate = statusUpdate;
	}

	public void setRequiredQuantity(Double requiredQuantity) {
		this.requiredQuantity = requiredQuantity;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PurchaseItemParam [");

		if (description != null)
			builder.append("description=").append(description).append(", ");
		if (vendorId != null)
			builder.append("vendorId=").append(vendorId).append(", ");
		if (requiredQuantity != null)
			builder.append("requiredQuantity=").append(requiredQuantity)
					.append(", ");
		if (deliveredQuantity != null)
			builder.append("deliveredQuantity=").append(deliveredQuantity)
					.append(", ");
		if (unit != null)
			builder.append("unit=").append(unit).append(", ");
		if (statusUpdate != null)
			builder.append("statusUpdate=").append(statusUpdate);
		builder.append("]");
		return builder.toString();
	}

	@XmlRootElement(name = "purchase_status")
	public static class PurchaseStatus {
		private String status;
		private String comments;

		@XmlElement(name = "status")
		public String getStatus() {
			return status;
		}

		@XmlElement(name = "comments")
		public String getComments() {
			return comments;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public void setComments(String comments) {
			this.comments = comments;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("PurchaseStatus [");
			if (status != null)
				builder.append("status=").append(status).append(", ");
			if (comments != null)
				builder.append("comments=").append(comments);
			builder.append("]");
			return builder.toString();
		}

	}

}
