package com.bnbneeds.app.model.dealer.tender;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "add_bid_products")
public class AddTenderProductsParam {

	private List<TenderProductParam> bidProductList;

	@XmlElementWrapper(name = "bid_products")
	@XmlElement(name = "bid_product")
	@JsonProperty("bid_product")
	public List<TenderProductParam> getBidProductList() {
		return bidProductList;
	}

	public void setBidProductList(List<TenderProductParam> bidProductList) {
		this.bidProductList = bidProductList;
	}

	public void add(TenderProductParam bidProduct) {
		if (bidProductList == null) {
			bidProductList = new ArrayList<>();
		}
		bidProductList.add(bidProduct);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AddBidProductsParam [");
		if (bidProductList != null)
			builder.append("bidProductList=").append(bidProductList);
		builder.append("]");
		return builder.toString();
	}

}
