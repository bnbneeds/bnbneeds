package com.bnbneeds.app.model.dealer.tender;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "tender_product")
public class TenderProductParam {

	private Double quantity;
	private String unit;
	private String qualitySpecification;
	private String specialRequirements;
	private String productNameId;

	public TenderProductParam() {
		super();
	}

	/**
	 * Constructor to create a bid product
	 * 
	 * @param productNameId
	 * @param quantity
	 * @param unit
	 * @param qualitySpecification
	 * @param specialRequirements
	 */
	public TenderProductParam(String productNameId, double quantity, String unit, String qualitySpecification,
			String specialRequirements) {
		super();
		this.quantity = quantity;
		this.unit = unit;
		this.qualitySpecification = qualitySpecification;
		this.specialRequirements = specialRequirements;
		this.productNameId = productNameId;
	}

	@XmlElement(name = "quantity")
	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	@XmlElement(name = "unit")
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@XmlElement(name = "quality_specification")
	public String getQualitySpecification() {
		return qualitySpecification;
	}

	public void setQualitySpecification(String qualitySpecification) {
		this.qualitySpecification = qualitySpecification;
	}

	@XmlElement(name = "special_requirements")
	public String getSpecialRequirements() {
		return specialRequirements;
	}

	public void setSpecialRequirements(String specialRequirements) {
		this.specialRequirements = specialRequirements;
	}

	@XmlElement(name = "product_name_id")
	public String getProductNameId() {
		return productNameId;
	}

	public void setProductNameId(String productNameId) {
		this.productNameId = productNameId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BidProductParam [quantity=").append(quantity).append(", ");
		if (unit != null)
			builder.append("unit=").append(unit).append(", ");
		if (qualitySpecification != null)
			builder.append("qualitySpecification=").append(qualitySpecification).append(", ");
		if (specialRequirements != null)
			builder.append("specialRequirements=").append(specialRequirements).append(", ");
		if (productNameId != null)
			builder.append("productNameId=").append(productNameId);
		builder.append("]");
		return builder.toString();
	}

}
