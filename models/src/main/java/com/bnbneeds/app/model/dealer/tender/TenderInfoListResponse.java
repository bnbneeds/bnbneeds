package com.bnbneeds.app.model.dealer.tender;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectInfoListResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "tender_list")
public class TenderInfoListResponse
		extends DataObjectInfoListResponse<TenderInfoResponse> {

	@Override
	@XmlElementWrapper(name = "tenders")
	@XmlElement(name = "tender")
	@JsonProperty("tender")
	public List<TenderInfoResponse> getList() {
		return super.getList();
	}
}
