package com.bnbneeds.app.model.account;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "change_password")
public class ChangePasswordParam {

	private String currentPassword;
	private String newPassword;
	private String confirmPassword;

	@XmlElement(name = "current_password")
	public String getCurrentPassword() {
		return currentPassword;
	}

	@XmlElement(name = "new_password")
	public String getNewPassword() {
		return newPassword;
	}

	@XmlElement(name = "confirm_new_password")
	@JsonProperty("confirm_new_password")
	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ChangePasswordParam [");
		if (currentPassword != null)
			builder.append("currentPassword=").append(" ****, ");
		if (newPassword != null)
			builder.append("newPassword=").append(" ****, ");
		if (confirmPassword != null)
			builder.append("confirmPassword=").append(" ****");
		builder.append("]");
		return builder.toString();
	}
}
