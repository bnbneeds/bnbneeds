package com.bnbneeds.app.model.product;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "product_list")
public class ProductListResponse extends DataObjectListResponse {

	private RelatedResourceRep vendorLink;

	@XmlElement(name = "vendor")
	public RelatedResourceRep getVendorLink() {
		return vendorLink;
	}

	public void setVendorLink(String vendorId) {
		this.vendorLink = new RelatedResourceRep(vendorId, new RestLinkRep(
				"self", Endpoint.VENDOR_INFO.get(vendorId)));
	}

	@XmlElementWrapper(name = "products")
	@XmlElement(name = "product")
	public List<NamedRelatedResourceRep> getProductList() {
		return dataObjectList;
	}
}