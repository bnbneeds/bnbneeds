package com.bnbneeds.app.model.report;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.EntityParam;

@XmlRootElement(name = "create_report")
public class CreateReportParam extends EntityParam {

	private String entityId;
	private String title;
	
	//created for testing purpose
	private String userId;

	@XmlElement(name = "user_id")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@XmlElement(name = "entity_id")
	public String getEntityId() {
		return entityId;
	}

	@XmlElement(name = "title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setEntityId(String resourceId) {
		this.entityId = resourceId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CreateReportParam [");
		builder.append(super.toString());
		if (entityId != null)
			builder.append("entityId=").append(entityId).append(", ");
		if (title != null)
			builder.append("title=").append(title);
		builder.append("]");
		return builder.toString();
	}

}
