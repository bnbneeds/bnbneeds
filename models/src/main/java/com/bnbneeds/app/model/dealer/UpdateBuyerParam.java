package com.bnbneeds.app.model.dealer;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "update_buyer")
public class UpdateBuyerParam extends CreateBuyerParam {

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateBuyerParam [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
