package com.bnbneeds.app.model.review;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "product_review")
public class ProductReviewInfoResponse extends ReviewInfoResponse {

	private NamedRelatedResourceRep product;

	@XmlElement(name = "product")
	public NamedRelatedResourceRep getProduct() {
		return product;
	}

	public void setProduct(NamedRelatedResourceRep product) {
		this.product = product;
	}

	public void setProduct(String id, String name) {
		setProduct(new NamedRelatedResourceRep(id, new RestLinkRep("self",
				Endpoint.PRODUCT_INFO.get(id)), name));
	}

	public void setReviewedBy(String id, String name) {
		setReviewedBy(new NamedRelatedResourceRep(id, new RestLinkRep("self",
				Endpoint.BUYER_INFO.get(id)), name));
	}

	@Override
	public void setLink() {
	}

}
