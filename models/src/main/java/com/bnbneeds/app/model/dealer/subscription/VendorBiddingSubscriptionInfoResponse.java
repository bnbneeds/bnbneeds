package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "vendor_bidding_subscription")
public class VendorBiddingSubscriptionInfoResponse extends DealerBiddingSubscriptionInfoResponse {

	private NamedRelatedResourceRep vendor;
	private Integer numberOfTendersAccessed;

	@XmlElement(name = "vendor")
	public NamedRelatedResourceRep getVendor() {
		return vendor;
	}

	@XmlElement(name = "number_of_tenders_accessed")
	public Integer getNumberOfTendersAccessed() {
		return numberOfTendersAccessed;
	}

	public void setVendor(NamedRelatedResourceRep vendor) {
		this.vendor = vendor;
	}

	public void setVendor(String vendorId, String name) {
		this.vendor = new NamedRelatedResourceRep(
				new RelatedResourceRep(vendorId, new RestLinkRep("self", Endpoint.VENDOR_INFO.get(vendorId))), name);
	}

	public void setNumberOfTendersAccessed(Integer numberOfTendersAccessed) {
		this.numberOfTendersAccessed = numberOfTendersAccessed;
	}

	@Override
	protected void setLink() {
		// This is intentionally left blank
	}

	@Override
	public void setSubscriptionPlan(String id, String name) {
		setSubscriptionPlan(new NamedRelatedResourceRep(new RelatedResourceRep(id,
				new RestLinkRep("self", Endpoint.VENDOR_BIDDING_SUBSCRIPTION_PLAN_INFO.get(id))), name));

	}

}
