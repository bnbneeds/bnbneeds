package com.bnbneeds.app.model.dealer;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "buyer_list")
public class BuyerInfoListResponse {

	private List<BuyerInfoResponse> buyerList;
	private String nextOffset;

	@XmlElementWrapper(name = "buyers")
	@XmlElement(name = "buyer")
	@JsonProperty("buyer")
	public List<BuyerInfoResponse> getBuyerList() {
		return buyerList;
	}

	public void setBuyerList(List<BuyerInfoResponse> buyerList) {
		if (buyerList != null && !buyerList.isEmpty()) {
			this.buyerList = buyerList;
		}
	}

	@XmlElement(name = "next_offset")
	public String getNextOffset() {
		return nextOffset;
	}

	public void setNextOffset(String nextOffset) {
		this.nextOffset = nextOffset;
	}

	public void add(BuyerInfoResponse buyer) {
		if (buyerList == null) {
			buyerList = new ArrayList<BuyerInfoResponse>();
		}
		buyerList.add(buyer);
	}

	public int size() {
		return buyerList == null ? 0 : buyerList.size();
	}

}
