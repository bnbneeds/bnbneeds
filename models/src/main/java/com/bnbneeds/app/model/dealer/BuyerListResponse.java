package com.bnbneeds.app.model.dealer;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "buyer_list")
public class BuyerListResponse extends DataObjectListResponse {

	@XmlElementWrapper(name = "buyers")
	@XmlElement(name = "buyer")
	@JsonProperty("buyer")
	public List<NamedRelatedResourceRep> getBuyerList() {
		return dataObjectList;
	}

	public void setBuyerList(List<NamedRelatedResourceRep> dataObjectList) {
		super.setNamedRelatedResourceList(dataObjectList);
	}

}