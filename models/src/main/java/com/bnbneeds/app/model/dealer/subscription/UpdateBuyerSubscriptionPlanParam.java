package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "update_bidding_subscription_plan")
public class UpdateBuyerSubscriptionPlanParam extends BiddingSubscriptionPlanParam {

	private Integer numberOfTendersAllowedToCreate;

	@XmlElement(name = "number_of_tenders_allowed_to_create")
	public Integer getNumberOfTendersAllowedToCreate() {
		return numberOfTendersAllowedToCreate;
	}

	public void setNumberOfTendersAllowedToCreate(Integer numberOfTendersAllowedToCreate) {
		this.numberOfTendersAllowedToCreate = numberOfTendersAllowedToCreate;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UpdateSubscriptionPlanParam [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

	
}
