package com.bnbneeds.app.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class DataObjectInfoListResponse<T extends DataObjectRestResponse> {

	private String nextOffset;
	private List<T> list;

	@XmlElement(name = "next_offset")
	public String getNextOffset() {
		return nextOffset;
	}

	public void setNextOffset(String nextOffset) {
		if (nextOffset != null && !nextOffset.isEmpty()) {
			this.nextOffset = nextOffset;
		}
	}

	protected List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public void add(T item) {
		if (list == null) {
			list = new ArrayList<>();
		}
		list.add(item);
	}

	public int size() {
		return list == null ? 0 : list.size();
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public Iterator<T> getIterator() {
		return list == null ? null : list.iterator();
	}

}
