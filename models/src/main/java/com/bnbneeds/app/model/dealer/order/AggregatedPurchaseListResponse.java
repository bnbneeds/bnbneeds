package com.bnbneeds.app.model.dealer.order;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "aggregated_purchase_list")
public class AggregatedPurchaseListResponse {

	private List<AggregatedPuchaseItem> itemList;
	private String requestDate;
	private Map<String, AggregatedPuchaseItem> productNameToPurchaseItemMap;

	public AggregatedPurchaseListResponse() {
		productNameToPurchaseItemMap = new ConcurrentHashMap<String, AggregatedPuchaseItem>();
	}

	@XmlElementWrapper(name = "aggregated_purchase_items")
	@XmlElement(name = "item")
	@JsonProperty("item")
	public List<AggregatedPuchaseItem> getItemList() {
		return itemList;
	}

	public void setItemList(List<AggregatedPuchaseItem> itemList) {
		this.itemList = itemList;
	}

	@XmlElement(name = "request_date")
	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public void add(AggregatedPuchaseItem item) {
		if (itemList == null) {
			itemList = new ArrayList<AggregatedPuchaseItem>();
		}

		productNameToPurchaseItemMap.put(item.getProductName().getId(), item);
		itemList.add(item);
	}

	public AggregatedPuchaseItem getItemByProductNameId(String productNameId) {
		return productNameToPurchaseItemMap.get(productNameId);
	}

	public int size() {
		return itemList == null ? 0 : itemList.size();
	}

	@XmlRootElement(name = "purchase_item")
	public static class AggregatedPuchaseItem {

		private NamedRelatedResourceRep productName;
		private List<DealerPurchaseItemInfo> buyerList;
		private List<DealerPurchaseItemInfo> vendorList;
		private Quantity quantity;

		@XmlElement(name = "product_name")
		public NamedRelatedResourceRep getProductName() {
			return productName;
		}

		@XmlElementWrapper(name = "buyers")
		@XmlElement(name = "buyer")
		@JsonProperty("buyer")
		public List<DealerPurchaseItemInfo> getBuyerList() {
			return buyerList;
		}

		@XmlElementWrapper(name = "vendors")
		@XmlElement(name = "vendor")
		@JsonProperty("vendor")
		public List<DealerPurchaseItemInfo> getVendorList() {
			return vendorList;
		}

		@XmlElement(name = "quantity")
		public Quantity getQuantity() {
			return quantity;
		}

		public void setQuantity(Quantity quantity) {
			this.quantity = quantity;
		}

		public void addRequiredQuantity(Double quantity) {
			Double required = this.quantity.getRequired() + quantity;
			this.quantity.setRequired(required);
		}

		public void addDeliveredQuantity(Double quantity) {
			Double delivered = this.quantity.getDelivered() + quantity;
			this.quantity.setDelivered(delivered);
		}

		public void setBuyerList(List<DealerPurchaseItemInfo> buyerList) {
			this.buyerList = buyerList;
		}

		public void addBuyerPurchaseItem(DealerPurchaseItemInfo buyer) {
			if (buyerList == null) {
				buyerList = new ArrayList<DealerPurchaseItemInfo>();
			}
			buyerList.add(buyer);
		}

		public void addBuyerPurchaseItem(String requestDate,
				RelatedResourceRep purchaseItem, NamedRelatedResourceRep buyer,
				Quantity quantity) {
			this.addBuyerPurchaseItem(new DealerPurchaseItemInfo(requestDate,
					purchaseItem, buyer, quantity));
		}

		public void setVendorList(List<DealerPurchaseItemInfo> vendorList) {
			this.vendorList = vendorList;
		}

		public void addVendorPurchaseItem(DealerPurchaseItemInfo vendor) {
			if (vendorList == null) {
				vendorList = new ArrayList<DealerPurchaseItemInfo>();
			}
			vendorList.add(vendor);
		}

		public void addVendorPurchaseItem(String requestDate,
				RelatedResourceRep purchaseItem, NamedRelatedResourceRep buyer,
				Quantity quantity) {
			this.addVendorPurchaseItem(new DealerPurchaseItemInfo(requestDate,
					purchaseItem, buyer, quantity));
		}

		public void setProductName(NamedRelatedResourceRep productName) {
			this.productName = productName;
		}

		@XmlRootElement(name = "dealer_purchase_item")
		public static class DealerPurchaseItemInfo {
			private RelatedResourceRep purchaseItem;
			private NamedRelatedResourceRep dealer;
			private Quantity quantity;
			private String requestDate;
			private String tags;

			public DealerPurchaseItemInfo() {
				super();
			}

			public DealerPurchaseItemInfo(String requestDate,
					RelatedResourceRep purchaseItem,
					NamedRelatedResourceRep dealer, Quantity quantity) {
				super();
				this.requestDate = requestDate;
				this.dealer = dealer;
				this.quantity = quantity;
				this.purchaseItem = purchaseItem;
			}

			@XmlElement(name = "dealer")
			public NamedRelatedResourceRep getDealer() {
				return dealer;
			}

			@XmlElement(name = "quantity")
			public Quantity getQuantity() {
				return quantity;
			}

			@XmlElement(name = "purchase_item")
			public RelatedResourceRep getPurchaseItem() {
				return purchaseItem;
			}

			@XmlElement(name = "request_date")
			public String getRequestDate() {
				return requestDate;
			}

			@XmlElement(name = "tags")
			public String getTags() {
				return tags;
			}

			public void setTags(String tags) {
				this.tags = tags;
			}

			public void setRequestDate(String requestDate) {
				this.requestDate = requestDate;
			}

			public void setPurchaseItem(RelatedResourceRep purchaseItem) {
				this.purchaseItem = purchaseItem;
			}

			public void setDealer(NamedRelatedResourceRep dealer) {
				this.dealer = dealer;
			}

			public void setQuantity(Quantity quantity) {
				this.quantity = quantity;
			}
		}
	}
}
