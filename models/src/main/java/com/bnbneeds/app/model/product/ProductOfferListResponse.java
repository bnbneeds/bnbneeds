package com.bnbneeds.app.model.product;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;

@XmlRootElement(name = "product_offer_list")
public class ProductOfferListResponse extends DataObjectListResponse {

	@XmlElementWrapper(name = "product_offers")
	@XmlElement(name = "product_offer")
	public List<NamedRelatedResourceRep> getProductOfferList() {
		return dataObjectList;
	}
}
