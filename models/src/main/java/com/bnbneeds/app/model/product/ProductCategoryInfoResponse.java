package com.bnbneeds.app.model.product;

import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DescribedDataObjectRestResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;

@XmlRootElement(name = "product_category")
public class ProductCategoryInfoResponse extends DescribedDataObjectRestResponse {

	public void setLink() {
		setLink(Endpoint.PRODUCT_CATEGORY_INFO.get(getId()));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductCategoryInfoResponse [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
