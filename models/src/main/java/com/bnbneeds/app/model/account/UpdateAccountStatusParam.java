package com.bnbneeds.app.model.account;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "update_user_account_status")
public class UpdateAccountStatusParam {
	
	public static enum AccountStatus {
		ENABLED, DISABLED;
	}
	
	private String status;
	
	public UpdateAccountStatusParam() {
		super();
		status = AccountStatus.ENABLED.name();
	}

	@XmlElement(name = "status")
	public AccountStatus getStatus() {
		return AccountStatus.valueOf(status);
	}


	public void setStatus(AccountStatus status) {
		this.status = status.name();
	}
	
	public boolean hasStatus(AccountStatus status) {
		return status == getStatus();
	}

	public boolean isEnabled() {
		return hasStatus(AccountStatus.ENABLED);
	}

	public boolean isDisabled() {
		return hasStatus(AccountStatus.DISABLED);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ChangeAccountStatusParam [");
		if (status != null)
			builder.append("status=").append(status);
		builder.append("]");
		return builder.toString();
	}
}
