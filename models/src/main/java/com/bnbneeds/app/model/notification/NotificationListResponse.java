package com.bnbneeds.app.model.notification;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.DataObjectListResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "notification_list")
public class NotificationListResponse extends DataObjectListResponse {

	private List<NotificationInfoResponse> notificationList;

	@XmlElementWrapper(name = "notifications")
	@XmlElement(name = "notification")
	@JsonProperty("notification")
	public List<NotificationInfoResponse> getNotificationList() {
		return notificationList;
	}

	public void setNotificationList(
			List<NotificationInfoResponse> notificationList) {
		this.notificationList = notificationList;
	}

	public void addInfoResponse(NotificationInfoResponse infoResponse) {
		if (this.notificationList == null) {
			this.notificationList = new ArrayList<NotificationInfoResponse>();
		}
		this.notificationList.add(infoResponse);
	}

	public Iterator<NotificationInfoResponse> iterator() {
		if (notificationList != null) {
			return notificationList.iterator();
		}
		return null;
	}

	@Override
	public int size() {
		return notificationList == null ? 0 : notificationList.size();
	}

}
