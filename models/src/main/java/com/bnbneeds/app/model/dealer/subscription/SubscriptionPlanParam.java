package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.bnbneeds.app.model.EntityParam;

@XmlRootElement(name = "subscription_plan_param")
public class SubscriptionPlanParam extends EntityParam {

	private String title;
	private String durationType;
	private Integer durationValue;
	private Double premiumAmount;
	
	@XmlElement(name = "title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@XmlElement(name = "duration_type")
	public String getDurationType() {
		return durationType;
	}
	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}
	
	@XmlElement(name = "duration_value")
	public Integer getDurationValue() {
		return durationValue;
	}
	public void setDurationValue(Integer durationValue) {
		this.durationValue = durationValue;
	}
	
	@XmlElement(name = "premium_amount")
	public Double getPremiumAmount() {
		return premiumAmount;
	}
	public void setPremiumAmount(Double premiumAmount) {
		this.premiumAmount = premiumAmount;
	}
}
