package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "add_remarks")
public class AddRemarksParam {
	
	private String remarks;
	
	@XmlElement(name = "remarks")
	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AddRemarksParam [");
		if (remarks != null)
			builder.append("remarks=").append(remarks);
		builder.append("]");
		return builder.toString();
	}

	
}
