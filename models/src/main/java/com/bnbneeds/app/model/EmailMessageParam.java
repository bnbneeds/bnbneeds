package com.bnbneeds.app.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "email_message")
public class EmailMessageParam {

	private String subject;
	private String message;
	private String to;
	private String cc;
	private String bcc;

	@XmlElement(name = "subject")
	public String getSubject() {
		return subject;
	}

	@XmlElement(name = "message")
	public String getMessage() {
		return message;
	}

	@XmlElement(name = "to")
	public String getTo() {
		return to;
	}

	@XmlElement(name = "cc")
	public String getCc() {
		return cc;
	}

	@XmlElement(name = "bcc")
	public String getBcc() {
		return bcc;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmailMessageParam [");
		if (subject != null)
			builder.append("subject=").append(subject).append(", ");
		if (message != null)
			builder.append("message=").append(message).append(", ");
		if (to != null)
			builder.append("to=").append(to).append(", ");
		if (cc != null)
			builder.append("cc=").append(cc).append(", ");
		if (bcc != null)
			builder.append("bcc=").append(bcc);
		builder.append("]");
		return builder.toString();
	}

}
