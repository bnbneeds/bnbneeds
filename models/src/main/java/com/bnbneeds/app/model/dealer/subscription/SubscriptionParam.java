package com.bnbneeds.app.model.dealer.subscription;

import javax.xml.bind.annotation.XmlElement;

public class SubscriptionParam {

	private String subscriptionPlanId;

	@XmlElement(name = "subscription_plan_id")
	public String getSubscriptionPlanId() {
		return subscriptionPlanId;
	}

	public void setSubscriptionPlanId(String subscriptionPlanId) {
		this.subscriptionPlanId = subscriptionPlanId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SubscriptionParam [");
		if (subscriptionPlanId != null)
			builder.append("subscriptionPlanId=").append(subscriptionPlanId);
		builder.append("]");
		return builder.toString();
	}

}
