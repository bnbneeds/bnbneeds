# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/


# LETS ENCRYPT:IMPORTANT NOTES:
#  - Congratulations! Your certificate and chain have been saved at
#    /etc/letsencrypt/live/bnbneeds.com/fullchain.pem. Your cert will
#    expire on 2017-12-08. To obtain a new or tweaked version of this
#    certificate in the future, simply run certbot again with the
#    "certonly" option. To non-interactively renew *all* of your
#    certificates, run "certbot renew"
#  - Your account credentials have been saved in your Certbot
#    configuration directory at /etc/letsencrypt. You should make a
#    secure backup of this folder now. This configuration directory will
#    also contain certificates and private keys obtained by Certbot so
#    making regular backups of this folder is ideal.
#
#    Web links:
#    1. https://certbot.eff.org/#centosrhel7-nginx
#    2. https://letsencrypt.org/



user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
    use epoll;
}


http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;
    server_tokens off;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;
  
     real_ip_header X-Real-IP;
     real_ip_recursive on;

     proxy_set_header  Host $host;
     proxy_set_header  X-Real-IP $remote_addr;
     proxy_set_header  X-Forwarded-Host $remote_addr;
     proxy_set_header  X-Forwarded-For $remote_addr;
     
     include portal-security-headers.conf;

     client_header_timeout 3000;
     client_body_timeout 3000;
     fastcgi_read_timeout 3000;
     client_max_body_size 32m;
     fastcgi_buffers 8 128k;
     fastcgi_buffer_size 128k;

     gzip on;
     gzip_comp_level    5;
     gzip_min_length    256;
     gzip_proxied       any;
     gzip_vary          on;
     gzip_disable       "MSIE [1-6]\.(?!.*SV1)";

     gzip_types
     application/atom+xml
     application/javascript
     application/json
     application/ld+json
     application/manifest+json
     application/rss+xml
     application/vnd.geo+json
     application/vnd.ms-fontobject
     application/x-font-ttf
     application/x-web-app-manifest+json
     application/xhtml+xml
     application/xml
     font/opentype
     image/bmp
     image/svg+xml
     image/x-icon
     text/cache-manifest
     text/css
     text/plain
     text/vcard
     text/vnd.rim.location.xloc
     text/vtt
     text/x-component
     text/x-cross-domain-policy;

     proxy_cache_path /tmp/nginx levels=1:2 keys_zone=my_zone:10m inactive=60m;
     proxy_cache_key "$scheme$request_method$host$request_uri";

    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  bnbneeds.com;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;
        

        location / {
            proxy_pass http://localhost:8080;
            include portal-restrict-access.conf;
        }

        include portal-error.conf;
    
        listen 443 ssl; # managed by Certbot
        ssl_certificate /etc/letsencrypt/live/bnbneeds.com/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/bnbneeds.com/privkey.pem; # managed by Certbot
        include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot


        if ($scheme != "https") {
            return 301 https://$host$request_uri;
        } # managed by Certbot

    }

    server {
        listen       80;
        server_name  admin.bnbneeds.com;

        location / {
            proxy_pass http://localhost:8084;
            include portal-restrict-access.conf;
        }
        
        include portal-error.conf;
    
        listen 443 ssl; # managed by Certbot
        ssl_certificate /etc/letsencrypt/live/bnbneeds.com/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/bnbneeds.com/privkey.pem; # managed by Certbot
        include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot


        if ($scheme != "https") {
            return 301 https://$host$request_uri;
        } # managed by Certbot

    }

    server {
        listen       80;
        server_name  test-dealer-portal.bnbneeds.com;

        location / {
            proxy_pass http://localhost:8081;
            include portal-restrict-access.conf;
        }
        
        include portal-error.conf;
    
        listen 443 ssl; # managed by Certbot
        ssl_certificate /etc/letsencrypt/live/bnbneeds.com/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/bnbneeds.com/privkey.pem; # managed by Certbot
        include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot


        if ($scheme != "https") {
            return 301 https://$host$request_uri;
        } # managed by Certbot

    }

    server {
        listen       80;
        server_name  test-admin-portal.bnbneeds.com;

        location / {
            proxy_pass http://localhost:8085;
            include portal-restrict-access.conf;
        }
        
        include portal-error.conf;
    
        listen 443 ssl; # managed by Certbot
        ssl_certificate /etc/letsencrypt/live/bnbneeds.com/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/bnbneeds.com/privkey.pem; # managed by Certbot
        include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot


        if ($scheme != "https") {
            return 301 https://$host$request_uri;
        } # managed by Certbot

    }

# Settings for a TLS enabled server.
#
#    server {
#        listen       443 ssl http2 default_server;
#        listen       [::]:443 ssl http2 default_server;
#        server_name  _;
#        root         /usr/share/nginx/html;
#
#        ssl_certificate "/etc/pki/nginx/server.crt";
#        ssl_certificate_key "/etc/pki/nginx/private/server.key";
#        ssl_session_cache shared:SSL:1m;
#        ssl_session_timeout  10m;
#        ssl_ciphers HIGH:!aNULL:!MD5;
#        ssl_prefer_server_ciphers on;
#
#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;
#
#        location / {
#            proxy_pass https://127.0.0.1:8081;
#        }
#
#        error_page 404 /404.html;
#            location = /40x.html {
#        }
#
#        error_page 500 502 503 504 /50x.html;
#            location = /50x.html {
#        }
#    }

}

