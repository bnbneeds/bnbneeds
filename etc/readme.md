###LetsEncrypt auto renewal script###

certbot_renew.sh



```
#!shell

#!/bin/bash

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/usr/java/jre1.8.0_121/bin:/root/bin
certbot renew > /opt/bnbneeds/certbot_renewal_cron_job.txt 2>&1

```