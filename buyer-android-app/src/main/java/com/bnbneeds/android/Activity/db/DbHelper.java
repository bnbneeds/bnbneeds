package com.bnbneeds.android.Activity.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * sqlite database helper to create table into SQLite database
 *
 */
public class DbHelper extends SQLiteOpenHelper {
    static String DATABASE_NAME = "PurchaseItemDB";
    public static final String TABLE_NAME = "ItemListTable";
    public static final String KEY_TEMPLATENAME = "TEMPLATE_NAME";
    public static final String KEY_PRODUCTNAME = "PRODUCT_NAME";
    public static final String KEY_PRODUCTNAMEID = "PRODUCT_NAME_ID";
    public static final String KEY_QUANTITY = "QUANTITY";
    public static final String KEY_UNITS = "UNITS";
    public static final String KEY_VENDOR = "VENDOR";
    public static final String KEY_VENDORID = "VENDOR_ID";
    public static final String KEY_EMAIL = "EMAIL_ID";
    public static final String KEY_REMARKS = "REMARKS";
    public static final String KEY_ID = "ID";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, 2);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" + KEY_ID + " INTEGER PRIMARY KEY, "+KEY_TEMPLATENAME + " TEXT, " +KEY_PRODUCTNAMEID+ " TEXT, " + KEY_PRODUCTNAME + " TEXT, " + KEY_QUANTITY + " REAL, " + KEY_UNITS + " TEXT, " + KEY_VENDORID + " TEXT, " + KEY_VENDOR + " TEXT," + KEY_EMAIL + " TEXT, "+ KEY_REMARKS + " TEXT)";
        db.execSQL(CREATE_TABLE);

    }

    // Update Data
    public long UpdateData(String ID,String strTel) {
        // TODO Auto-generated method stub

        try {

            SQLiteDatabase db;
            db = this.getWritableDatabase(); // Write Data

            ContentValues Val = new ContentValues();
            Val.put(KEY_QUANTITY, strTel);

            long rows = db.update(TABLE_NAME, Val,KEY_ID + "=" +ID,null);

            db.close();
            return rows; // return rows updated.

        } catch (Exception e) {
            return -1;
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);

    }

}
