package com.bnbneeds.android.Activity.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import android.widget.Toast;

import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.client.BNBNeedsClientSingleton;
import com.bnbneeds.android.Activity.config.Config;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.Timer;
import java.util.TimerTask;

import static com.bnbneeds.android.Activity.Activity.BaseActivity.timer;

public class VideoActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    protected BNBNeedsClient client;

    public VideoActivity() {
        client = BNBNeedsClientSingleton.getInstance();
    }


    private YouTubePlayerView myouTubePlayerView;



    /*String youtube;
    String youtubeTut;*/
    private static final int RECOVERY_DIALOG_REQUEST = 1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        /*Bundle bundle = new Bundle();
        bundle = getIntent().getExtras();
        youtube = bundle.getString("youtube");
        youtubeTut = bundle.getString("youtubeTut");*/

        myouTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_view);

        // Initializing video player with developer key
        myouTubePlayerView.initialize(Config.YOUTUBE_API_KEY, this);


        }

        @Override
        public void onInitializationFailure(YouTubePlayer.Provider provider,
                YouTubeInitializationResult errorReason) {
            if (errorReason.isUserRecoverableError()) {
                errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
            } else {
                String errorMessage = String.format(
                        getString(R.string.error_player), errorReason.toString());
                Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onInitializationSuccess(YouTubePlayer.Provider provider,
                YouTubePlayer player, boolean wasRestored) {
            if (!wasRestored) {

                // loadVideo() will auto play video
                // Use cueVideo() method, if you don't want to play it automatically
                player.loadVideo(EnquiryActivity.youtube);



            }
        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == RECOVERY_DIALOG_REQUEST) {
                // Retry initialization if user performed a recovery action
                getYouTubePlayerProvider().initialize(Config.YOUTUBE_API_KEY, this);

            }
        }

        private YouTubePlayer.Provider getYouTubePlayerProvider() {
            return (YouTubePlayerView) findViewById(R.id.youtube_player_view);

        }



    @Override
    protected void onPause() {
        super.onPause();

        timer = new Timer();
        Log.i("Main", "Invoking logout timer");
        LogOutTimerTask logoutTimeTask = new LogOutTimerTask();
        timer.schedule(logoutTimeTask, 3300000); //auto logout in 55 minutes
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (timer != null) {
            timer.cancel();
            Log.i("Main", "cancel timer");
            timer = null;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent home = new Intent(this, EnquiryActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(home);
    }


    class LogOutTimerTask extends TimerTask {


        @Override
        public void run() {
            client.logout();
            //redirect user to login screen
            Intent i = new Intent(VideoActivity.this, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            return;
        }
    }
}