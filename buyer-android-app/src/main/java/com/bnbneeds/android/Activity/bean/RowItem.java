package com.bnbneeds.android.Activity.bean;

/**
 * Created by Len on 09-12-2016.
 */

public class RowItem {
   // private int imageId;
    private String title;
    private String desc;
    private String vendor;
    private String productid;
    private String vendorid;
    private String modelid;




    public RowItem(String title, String modelid, String desc, String vendor, String productid, String vendorid) {
       // this.imageId = imageId;
        this.title = title;
        this.desc = desc;
        this.vendor = vendor;
        this.productid = productid;
        this.vendorid = vendorid;
        this.modelid = modelid;

    }



    /* public int getImageId() {
            return imageId;

        }
        public void setImageId(int imageId) {
            this.imageId = imageId;
        }*/
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getvendor() {
        return vendor;
    }
    public void setvendor(String vendor) {
        this.vendor = vendor;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }
    public String getVendorid() {
        return vendorid;
    }

    public void setVendorid(String vendorid) {
        this.vendorid = vendorid;
    }
    public String getModelid() {
        return modelid;
    }

    public void setModelid(String modelid) {
        this.modelid = modelid;
    }

    @Override
    public String toString() {
        return title + "\n" + desc;
    }
}
