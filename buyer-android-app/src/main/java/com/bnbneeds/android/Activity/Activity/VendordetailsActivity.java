package com.bnbneeds.android.Activity.Activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Rating;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;


import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.util.CheckConnectivity;
import com.bnbneeds.app.model.dealer.VendorInfoResponse;
import com.bnbneeds.app.model.product.ProductInfoResponse;
import com.bnbneeds.app.model.report.CreateReportParam;
import com.bnbneeds.app.model.review.AverageRatingResponse;
import com.getbase.floatingactionbutton.FloatingActionButton;


import org.springframework.web.client.ResourceAccessException;

import static com.bnbneeds.android.Activity.Activity.VendorlistActivity.vendorId;

public class VendordetailsActivity extends ConstantsActivity {

    private String Vendorname;
    private String Vendorid;
    final Context context = this;
    private String description, orgname, contactperson, mobilenum, email, cities, address, website;

    TextView organization, Contact, mobnum, emailid, desc, web, addr, city,rating;
    Button Back;
    EditText title, abusedesc;
    private String abuseTitle;
    private String abuseDescription;
    private String connection;
    RatingBar simplerating;

    float vendorRating;
    TextView viewreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendordetails);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
       /* Vendorname = b.getString("selectedname");
        Vendorid = b.getString("vendorid");*/
        viewreview = (TextView)findViewById(R.id.viewreview);
        viewreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent review = new Intent(VendordetailsActivity.this,ViewVendorReviewsActivity.class);
                startActivity(review);
            }
        });
        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if(conn == true) {
            new VendorDetailsTask().execute();
        }else{
            Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();
        }

        FloatingActionButton abusereport = (FloatingActionButton) findViewById(R.id.reportabuse);
        FloatingActionButton reviewvendor = (FloatingActionButton) findViewById(R.id.vendorreview);

        // Back = (Button)findViewById(R.id.back_btn);
         simplerating = (RatingBar) findViewById(R.id.ratingBarIndicator);
        rating = (TextView)findViewById(R.id.ratingvalue);
        organization = (TextView) findViewById(R.id.vendorname);
        Contact = (TextView) findViewById(R.id.contactname);
        mobnum = (TextView) findViewById(R.id.mobno);
        emailid = (TextView) findViewById(R.id.email);
        desc = (TextView) findViewById(R.id.desc);
        web = (TextView) findViewById(R.id.web);
        addr = (TextView) findViewById(R.id.adress);
        city = (TextView) findViewById(R.id.cities);
        addr.setMovementMethod(new ScrollingMovementMethod());

        mobnum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call = new Intent(Intent.ACTION_CALL);
                call.setData(Uri.parse(mobnum.getText().toString()));
                if (ActivityCompat.checkSelfPermission(VendordetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(call);
            }
        });

        abusereport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(context);
                final  View view = li.inflate(R.layout.activity_abuse, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(VendordetailsActivity.this);
                builder.setTitle("Report Abuse");
                builder.setView(view);
                builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        title = (EditText)view.findViewById(R.id.title);
                        abusedesc = (EditText)view.findViewById(R.id.abusedesc);
                        abuseTitle = title.getText().toString();
                        abuseDescription = abusedesc.getText().toString();
                        if(!abuseTitle.isEmpty() && !abuseDescription.isEmpty()) {
                            new ReportabuseTask().execute();
                        }else{
                            Toast.makeText(getApplicationContext(), "Required Fields are Missing", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        reviewvendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent review = new Intent(VendordetailsActivity.this,VendorReviewActivity.class);
                startActivity(review);
            }
        });

    }
    @Override
    public void onBackPressed() {
        Intent home=new Intent(this,HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }
    class VendorDetailsTask extends AsyncTask<String, Void, String> {

        ProductInfoResponse productInfo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {

            AverageRatingResponse averageratingresponse = client.vendorReviews(vendorId).getAverageRating();
            double a =  averageratingresponse.getAverageRating();
            vendorRating = (float)a;


            VendorInfoResponse response = client.vendors(vendorId).get();
            orgname = response.getName();
            contactperson = response.getContactName();
            mobilenum = response.getMobileNumber();
            email = response.getEmailAddress();
          cities =  response.getCities();
           address = response.getAddress();
            website = response.getWebsite();
            description = response.getDescription();




            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            simplerating.setRating(vendorRating);
            rating.setText(String.valueOf(vendorRating));
            organization.setText(orgname);
            Contact.setText(contactperson);
            mobnum.setText(mobilenum);
            emailid.setText(email);
            if(!(description ==null)){
                desc.setText(description);
            }

            city.setText(cities);
            web.setText(website);
            addr.setText(address);



        }
    }

    class ReportabuseTask extends AsyncTask<String,Void,String>{



        @Override
        protected String doInBackground(String... params) {

            try {

                CreateReportParam param = new CreateReportParam();
                param.setTitle(abuseTitle);
                param.setEntityId(vendorId);
                param.setDescription(abuseDescription);
                client.abuseReports().create(param);
            }catch(ResourceAccessException a){
                connection = a.getMessage();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if(connection == null){
                Toast.makeText(getApplicationContext(), "Report Abuse has been Created Successfully", Toast.LENGTH_SHORT).show();
            }
else{
                Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }

            super.onPostExecute(s);
        }
    }
}
