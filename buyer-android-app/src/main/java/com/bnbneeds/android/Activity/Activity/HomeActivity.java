package com.bnbneeds.android.Activity.Activity;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnbneeds.android.Activity.AppRater.AppRater;
import com.bnbneeds.android.Activity.R;

import com.bnbneeds.android.Activity.adapter.CustomListviewAdapter;
import com.bnbneeds.android.Activity.adapter.CustomNotifiactionAdapter;
import com.bnbneeds.android.Activity.bean.RowItem;
import com.bnbneeds.android.Activity.bean.RowItemNotification;
import com.bnbneeds.android.Activity.client.BNBNeedsClientSingleton;
import com.bnbneeds.android.Activity.fragment.InputTemplateNameFragment;
import com.bnbneeds.android.Activity.util.Utils;
import com.bnbneeds.app.model.notification.NotificationInfoResponse;
import com.bnbneeds.app.model.notification.NotificationListResponse;
import com.bnbneeds.rest.client.BNBNeedsClient;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.bnbneeds.android.Activity.Activity.BaseActivity.timer;


public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, InputTemplateNameFragment.InputNameDialogListener {

    protected BNBNeedsClient client;

    protected static String templetname;
    protected static String youtube = "cHJgoAR1I5Y";
    protected static  String DEFAULT_TARGET_URI = "market://details?id=%s";

    final Context context = this;


    public HomeActivity() {
        client = BNBNeedsClientSingleton.getInstance();
    }

    ImageView productslist, vendorslist, purchaselist, createdplist;
    protected static boolean status;
    int mNotificationsCount = 0;
    TextView user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        AppRater appRater = new AppRater(this);
        appRater.show();
        if (status == false) {
            new FetchCountTask().execute();

        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        productslist = (ImageView) findViewById(R.id.imageproducts);
        vendorslist = (ImageView) findViewById(R.id.imagevendors);
        purchaselist = (ImageView) findViewById(R.id.imageplist);
        createdplist = (ImageView) findViewById(R.id.imagesavedplist);

        productslist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent products = new Intent(HomeActivity.this, ListviewTestActivity.class);
                startActivity(products);
            }
        });

        vendorslist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vendors = new Intent(HomeActivity.this, VendorlistActivity.class);
                startActivity(vendors);
            }
        });

        purchaselist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInputNameDialog();
            }
        });

        createdplist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tempIntent = new Intent(getApplicationContext(), TemplateActivity.class);
                startActivity(tempIntent);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, InformationActivity.class));
                Snackbar.make(view, "About BNBneeds", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        user = (TextView) headerView.findViewById(R.id.user);
        user.setText(LoginActivity.USER);
    }


    @Override
    protected void onPause() {
        super.onPause();

        timer = new Timer();
        Log.i("Main", "Invoking logout timer");
        LogOutTimerTask logoutTimeTask = new LogOutTimerTask();
        timer.schedule(logoutTimeTask, 3300000); //auto logout in 55 minutes
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (timer != null) {
            timer.cancel();
            Log.i("Main", "cancel timer");
            timer = null;
        }
    }


    private void showInputNameDialog() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        InputTemplateNameFragment inputNameDialog = new InputTemplateNameFragment();
        inputNameDialog.setCancelable(false);
        inputNameDialog.setDialogTitle("Template Name");
        inputNameDialog.show(fragmentManager, "Input Dialog");
    }


    @Override
    public void onFinishInputDialog(String inputText) {
        templetname = inputText;
        if (!templetname.isEmpty()) {
            Intent disp_intent = new Intent(this, DisplayTestActivity.class);
            disp_intent.putExtra("TEMPLATE_NAME", inputText);

            startActivity(disp_intent);

            Toast.makeText(this, "Input Name to dialog: " + inputText, Toast.LENGTH_SHORT).show();
            templetname = null;
        } else {
            Toast.makeText(getApplicationContext(), "Empty Template Name", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        MenuItem item = menu.findItem(R.id.action_notify);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable

        Utils.setBadgeCount(this, icon, mNotificationsCount);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }*/
        if (id == R.id.action_notify) {
            status = true;
            Intent list = new Intent(HomeActivity.this, NotificationActivity.class);
            startActivity(list);
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_groceries) {
            // Handle the camera action
            Intent products = new Intent(HomeActivity.this, ListviewTestActivity.class);
            startActivity(products);
           // Toast.makeText(getApplicationContext(), "You Selected Groceries", Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_vendorlist) {

            Intent vendors = new Intent(HomeActivity.this, VendorlistActivity.class);
            startActivity(vendors);
            //Toast.makeText(getApplicationContext(), "You Selected Vendors", Toast.LENGTH_LONG).show();

        } else if (id == R.id.nav_newpurchaselist) {
            showInputNameDialog();
        } else if (id == R.id.nav_savedpurchaselist) {
            Intent tempIntent = new Intent(getApplicationContext(), TemplateActivity.class);
            startActivity(tempIntent);

        }else if(id == R.id.rateus) {
            Intent rateintent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format(DEFAULT_TARGET_URI,getPackageName())));
            startActivity(rateintent);

        }else if (id == R.id.logout) {
            new LogoutTask().execute();
            //client.auth().logout();
            Toast.makeText(this, "You have been Successfully Logged Out", Toast.LENGTH_LONG).show();
            int pid = android.os.Process.myPid();
            android.os.Process.killProcess(pid);
            System.exit(0);
        }else if (id == R.id.usagevideo) {

            Intent usagevideo = new Intent(HomeActivity.this, AppUsageVideoActivity.class);
            startActivity(usagevideo);

        } else if (id == R.id.abuses) {

            Intent vendors = new Intent(HomeActivity.this, VendorlistActivity.class);
            startActivity(vendors);
            Toast.makeText(getApplicationContext(), "Select Vendors from the list to Report", Toast.LENGTH_LONG).show();
        } else if (id == R.id.chngpswd) {

            Intent cp = new Intent(HomeActivity.this, ChangePasswordActivity.class);
            startActivity(cp);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    class LogOutTimerTask extends TimerTask {


        @Override
        public void run() {
            client.logout();
            //redirect user to login screen
            Intent i = new Intent(HomeActivity.this, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            return;
        }
    }

    /*
      Updates the count of notifications in the ActionBar.
       */
    private void updateNotificationsBadge(int count) {
        mNotificationsCount = count;

        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        invalidateOptionsMenu();
    }

    class FetchCountTask extends AsyncTask<Void, Void, Integer> {


        @Override
        protected Integer doInBackground(Void... params) {
            // example count. This is where you'd
            // query your data store for the actual count.


            return 1;

        }

        @Override
        protected void onPostExecute(Integer integer) {
            updateNotificationsBadge(1);
            super.onPostExecute(integer);
        }
    }

    class LogoutTask extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... params) {


                client.logout();



            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


        }
    }
}