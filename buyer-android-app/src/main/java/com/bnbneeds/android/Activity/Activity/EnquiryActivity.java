package com.bnbneeds.android.Activity.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.util.CheckConnectivity;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.CreateEnquiryParam;
import com.bnbneeds.app.model.dealer.LikelyToBuyInParam;
import com.bnbneeds.app.model.dealer.VendorInfoResponse;
import com.bnbneeds.app.model.product.ProductInfoResponse;
import com.bnbneeds.app.model.report.CreateReportParam;
import com.bnbneeds.app.model.review.AverageRatingResponse;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.getbase.floatingactionbutton.FloatingActionButton;

import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.bnbneeds.android.Activity.Activity.ListviewTestActivity.ids;
import static com.bnbneeds.android.Activity.Activity.VendorlistActivity.vendorId;

public class EnquiryActivity extends ConstantsActivity {

    private String productDescription, productServiceLocation, enquiryDescription, enquiryQuantity, vendorAddress, orgname, contactperson, mobilenum, email, enquiryPeriod;
    private Integer enquiryLikelytobuyin;
    final Context context = this;
    TextView product_name, organization, Contact, mobnum, emailid, prokey, rating,productdescrription,productservice,vendoraddress;
    Button enquiry;
    EditText Desc, qty, likelybuy;
    Spinner days;
    Map<String, String> map = new HashMap<>();
    Map.Entry<String, String> entry;
    FloatingActionButton enquiryform;
    EditText title, abusedesc;
    private String abuseTitle;
    private String abuseDescription;
    private String errorMessage;
    private String connection;
    FloatingActionButton video, images, video_tut;
    protected static String youtube = "M-VQTP64Azk";
    protected static String youtubeTut = "M-VQTP64Azk";
    ListView lvkeys, lvvalues;
    RatingBar simplerating;
    float vendorRating;
    TextView ratingClick;
    TextView viewreview;
    private static int enquiry_response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog);
        productdescrription = (TextView)findViewById(R.id.comments);
        productservice = (TextView)findViewById(R.id.servicelocation);
        vendoraddress = (TextView)findViewById(R.id.vendoraddress);
        simplerating = (RatingBar) findViewById(R.id.ratingBarIndicator);
        rating = (TextView) findViewById(R.id.ratingvalue);
        video = (FloatingActionButton) findViewById(R.id.fab_video);
        images = (FloatingActionButton) findViewById(R.id.fab_image);
        video_tut = (FloatingActionButton) findViewById(R.id.fab_video_tut);
        ratingClick = (TextView)findViewById(R.id.reviewclick);
        viewreview = (TextView)findViewById(R.id.viewreview);
        viewreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent review = new Intent(EnquiryActivity.this,ViewProductReviewsActivity.class);
                startActivity(review);
            }
        });
        ratingClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent setreview = new Intent(EnquiryActivity.this,ProductReviewActivity.class);
                startActivity(setreview);
            }
        });
        images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent images = new Intent(EnquiryActivity.this, ImageActivity.class);
                /*Bundle pid = new Bundle();
               pid.putString("Productid",Productid);
                images.putExtras(pid);*/
                startActivity(images);
            }
        });

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent videos = new Intent(EnquiryActivity.this, VideoActivity.class);
               /* Bundle b = new Bundle();
                b.putString("youtube", youtube);
                b.putString("youtubeTut", youtubeTut);
                videos.putExtras(b);*/

                startActivity(videos);


            }
        });

        video_tut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent videos = new Intent(EnquiryActivity.this, VideoTutActivity.class);
               /* Bundle b = new Bundle();
                b.putString("youtubeTut", youtubeTut);
                videos.putExtras(b);*/
                startActivity(videos);


            }
        });

       /* Intent intent = getIntent();
        Bundle b = intent.getExtras();
        Productname = b.getString("selectedname");
        Productid = b.getString("selectedid");
        Vendorid = b.getString("vendorid");*/

        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if (conn == true) {
            new VendorDetailsTask().execute();
        } else {
            Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();
        }

        FloatingActionButton abusereport = (FloatingActionButton) findViewById(R.id.reportabuse);
        product_name = (TextView) findViewById(R.id.productname);
        organization = (TextView) findViewById(R.id.org_name);
        Contact = (TextView) findViewById(R.id.cont_name);
        mobnum = (TextView) findViewById(R.id.mob_num);
        emailid = (TextView) findViewById(R.id.email_id);
        qty = (EditText) findViewById(R.id.qty);
       // Desc = (EditText) findViewById(R.id.comments);
        // enquiry = (Button)findViewById(R.id.sbtn);
        // prokey = (TextView)findViewById(R.id.productkey);
        lvkeys = (ListView) findViewById(R.id.prokeys);
        lvvalues = (ListView) findViewById(R.id.provalues);
        enquiryform = (FloatingActionButton) findViewById(R.id.enquiry);



         /*prokey.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                    Intent intent = new Intent(EnquiryActivity.this,ProductDetailsActivity.class);
                 Bundle bundle = new Bundle();
                 bundle.putString("Productid",Productid);
                 intent.putExtras(bundle);
                 startActivity(intent);
             }
         });*/

        abusereport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(context);
                final View view = li.inflate(R.layout.activity_abuse, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(EnquiryActivity.this);
                builder.setTitle("Report Abuse");
                builder.setView(view);
                builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        title = (EditText) view.findViewById(R.id.title);
                        abusedesc = (EditText) view.findViewById(R.id.abusedesc);
                        abuseTitle = title.getText().toString();
                        abuseDescription = abusedesc.getText().toString();
                        if (!abuseTitle.isEmpty() && !abuseDescription.isEmpty()) {
                            new ReportabuseTask().execute();
                        } else {
                            Toast.makeText(getApplicationContext(), "Required Fields are Missing", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.proinfo);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EnquiryActivity.this, ProductDetailsActivity.class);
                Bundle bundle = new Bundle();
               // bundle.putString("Productid", Productid);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });*/

        /*VendorInfoResponse response = client.vendors(Vendorid).get();
        Toast.makeText(getApplicationContext(),"organization Name " +response.getName().toString(),Toast.LENGTH_LONG).show();*/

        product_name.setText(ListviewTestActivity.selected_product);

        //organization.setText(orgname);

        enquiryform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(context);
                final View view = li.inflate(R.layout.activity_enquiry, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(EnquiryActivity.this);
                builder.setTitle("Product Enquiry");
                builder.setView(view);
                builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        likelybuy = (EditText) view.findViewById(R.id.tobuy);
                        days = (Spinner) view.findViewById(R.id.likelybuy);
                        qty = (EditText) view.findViewById(R.id.qty);
                        Desc = (EditText) view.findViewById(R.id.comments);

                        enquiryPeriod = days.getSelectedItem().toString();
                        enquiryDescription = Desc.getText().toString();
                        enquiryQuantity = qty.getText().toString();
                        enquiryLikelytobuyin = Integer.parseInt(likelybuy.getText().toString());
                        if (!enquiryQuantity.isEmpty() && !enquiryDescription.isEmpty()) {
                            new EnquiryAsyncTask().execute();
                        } else {
                            Toast.makeText(getApplicationContext(), "Required Fields are Missing", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
               /* Description = Desc.getText().toString();
                Quantity = qty.getText().toString();

                new EnquiryAsyncTask().execute();
                Intent i = new Intent(EnquiryActivity.this,ListviewTestActivity.class);
                startActivity(i);*/
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }

        });


    }

    @Override
    public void onBackPressed() {
        youtube = "M-VQTP64Azk";
        youtubeTut = "M-VQTP64Azk";
        Intent home = new Intent(this, HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }

    class ReportabuseTask extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {
            try {

                CreateReportParam param = new CreateReportParam();
                param.setTitle(abuseTitle);
                param.setEntityId(ids);
                param.setDescription(abuseDescription);
                client.abuseReports().create(param);
            } catch (ResourceAccessException a) {
                connection = a.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (connection == null) {
                Toast.makeText(getApplicationContext(), "Report Abuse has been Created Successfully", Toast.LENGTH_SHORT).show();

            } else {

                Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }

        }
    }

    class EnquiryAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            setDialog();

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                LikelyToBuyInParam likelytobuyin = new LikelyToBuyInParam();
                likelytobuyin.setValue(enquiryLikelytobuyin);
                likelytobuyin.setTimeUnit(enquiryPeriod);

                CreateEnquiryParam param = new CreateEnquiryParam();
                param.setProductId(ids);
                param.setDescription(enquiryDescription);
                param.setRequiredQuantity(enquiryQuantity);
                param.setLikelyToBuyIn(likelytobuyin);
                CreateResourceResponse createResponse = client.buyers(LoginActivity.RELATIONSHIP_ID.trim()).createProductEnquiry(param);
               enquiry_response = createResponse.getHttpCode();

            } catch (BNBNeedsServiceException e) {
                e.printStackTrace();
                TaskResponse resp = e.getTask();
                errorMessage = resp.getMessage().toString();
            } catch (ResourceAccessException a) {
                connection = a.getMessage();
            }

            //  "urn:bnb:Buyer:92b8f49b-334e-4378-b15a-bf42881abf69"

            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
            if (connection == null) {
                if (enquiry_response == 201) {
                    Intent i = new Intent(EnquiryActivity.this, ListviewTestActivity.class);
                    startActivity(i);
                    enquiry_response = 0;
                    Toast.makeText(getApplicationContext(), "Your Enquiry Sent Sucessfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(EnquiryActivity.this, errorMessage, Toast.LENGTH_SHORT).show();

                }
            } else {
                Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }

            super.onPostExecute(s);
        }
    }

    class VendorDetailsTask extends AsyncTask<String, Void, String> {

        ProductInfoResponse productInfo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {

            AverageRatingResponse averageratingresponse = client.productReviews(ids).getAverageRating();
            double a =  averageratingresponse.getAverageRating();
            vendorRating = (float)a;


            VendorInfoResponse response = client.vendors(ListviewTestActivity.vendorid).get();
            orgname = response.getName();
            contactperson = response.getContactName();
            mobilenum = response.getMobileNumber();
            email = response.getEmailAddress();
            vendorAddress = response.getAddress();

            productInfo = client.products(ids).get();
           productDescription =  productInfo.getDescription();
            productServiceLocation = productInfo.getServiceLocations();
            String url = productInfo.getVideoURL();
            String tutUrl = productInfo.getTutorialVideoURL();
            if (!(url == null) && !(tutUrl == null)) {
                youtube = url.substring(url.indexOf('=') + 1);
                youtubeTut = tutUrl.substring(tutUrl.indexOf('=') + 1);
            } else if (!(url == null) && tutUrl == null) {
                youtube = url.substring(url.indexOf('=') + 1);
            } else if (url == null && !(tutUrl == null)) {
                youtubeTut = tutUrl.substring(tutUrl.indexOf('=') + 1);
            }
            map = productInfo.getAttributes();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            simplerating.setRating(vendorRating);
            rating.setText(String.valueOf(vendorRating));
            organization.setText(orgname);
            Contact.setText(contactperson);
            mobnum.setText(mobilenum);
            emailid.setText(email);
            productdescrription.setText(productDescription);
            productservice.setText(productServiceLocation);
            vendoraddress.setText(vendorAddress);
            if (map == null) {
            } else {
                ArrayList<String> arrlistkeys = new ArrayList<String>(map.keySet());
                String[] pro = arrlistkeys.toArray(new String[arrlistkeys.size()]);
                ArrayAdapter<String> adapterkey = new ArrayAdapter<String>(EnquiryActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, pro);
                lvkeys.setAdapter(adapterkey);

                ArrayList<String> arrlistvalues = new ArrayList<String>(map.values());
                String[] productvalue = arrlistvalues.toArray(new String[arrlistvalues.size()]);
                ArrayAdapter<String> adaptervalues = new ArrayAdapter<String>(EnquiryActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, productvalue);
                lvvalues.setAdapter(adaptervalues);
            }

        }
    }

}
