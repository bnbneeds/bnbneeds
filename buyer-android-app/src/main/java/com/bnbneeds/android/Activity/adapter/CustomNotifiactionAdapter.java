package com.bnbneeds.android.Activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.bean.RowItem;
import com.bnbneeds.android.Activity.bean.RowItemNotification;

import java.util.List;

/**
 * Created by Len on 20-03-2017.
 */

public class CustomNotifiactionAdapter extends ArrayAdapter<RowItemNotification>{

    Context context;

    public CustomNotifiactionAdapter(Context context, int resourceId,
                                 List<RowItemNotification> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        // ImageView imageView;
        TextView txtTitle;
        TextView txtproduct;
        TextView vendor;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        RowItemNotification rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listnotify, null);
            holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.message);
            holder.vendor = (TextView) convertView.findViewById(R.id.vendor);
            //  holder.imageView = (ImageView) convertView.findViewById(R.id.productImage);
            holder.txtproduct = (TextView) convertView.findViewById(R.id.productname);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.txtproduct.setText(rowItem.getProduct());
        holder.txtTitle.setText(rowItem.getTitle());
        // holder.imageView.setImageResource(rowItem.getImageId());
        holder.vendor.setText(rowItem.getVendor());

        return convertView;
    }

}
