package com.bnbneeds.android.Activity.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.util.StringUtils;
import com.bnbneeds.app.model.account.ChangePasswordParam;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

import org.springframework.web.client.ResourceAccessException;

import static com.bnbneeds.android.Activity.Activity.ConstantsActivity.pDialog;


public class ChangePasswordActivity extends BaseActivity {

   // private   String PASSWORD_ERROR_MESSAGE = "Password should contain at least 8 letters including small, capital, special character and number.";
   private   String PASSWORD_ERROR_MESSAGE = "Password should be minimum 6 characters with atleast one numeric value.";
    private   String CONFIRM_PASSWORD_ERROR_MESSAGE = "Password does not match. ";

    Button changepwd,cancel;
    EditText currpassword,chnpassword,conpassword;

    String currentPass,changePass,confirmPass,connection,errmsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        changepwd = (Button)findViewById(R.id.ok_btn);
        cancel = (Button)findViewById(R.id.can_btn);
        currpassword = (EditText)findViewById(R.id.curpas);
        chnpassword = (EditText)findViewById(R.id.chngpas);
        conpassword = (EditText)findViewById(R.id.conpas);



        changepwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPass = currpassword.getText().toString();
                changePass = chnpassword.getText().toString();
                confirmPass = conpassword.getText().toString();
                if (!StringUtils.isValidPassword(changePass) && !currentPass.isEmpty()) {
                    chnpassword.setError(PASSWORD_ERROR_MESSAGE);
                } else if (!changePass.equals(confirmPass)) {
                    Toast.makeText(getApplicationContext(), CONFIRM_PASSWORD_ERROR_MESSAGE,
                            Toast.LENGTH_LONG).show();
                }else{
                    if(StringUtils.isValidPassword(changePass)){
                        new ChangePasswordTask().execute();
                    }
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(ChangePasswordActivity.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(home);
            }
        });
    }
    public void onBackPressed() {
        Intent home = new Intent(this, HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }
    class ChangePasswordTask extends AsyncTask<String, Void, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {
       try{
            ChangePasswordParam param = new ChangePasswordParam();
            param.setCurrentPassword(currentPass);
            param.setNewPassword(changePass);
            param.setConfirmPassword(confirmPass);

            client.auth().changePassword(param);

       }catch(ResourceAccessException a){
           connection = a.getMessage();
       }catch(BNBNeedsServiceException e){
           errmsg = e.getMessage();
       }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            if(connection == null) {
                         if(errmsg == null) {
                             Intent success = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                             success.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                             success.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                             success.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                             startActivity(success);
                             Toast.makeText(getApplicationContext(), "Your password has be changed successfully. Please log in again using your new password.", Toast.LENGTH_LONG).show();
                         }else{
                             Intent success = new Intent(ChangePasswordActivity.this, HomeActivity.class);
                             success.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                             success.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                             success.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                             startActivity(success);
                             Toast.makeText(ChangePasswordActivity.this, "Parameter provided is invalid", Toast.LENGTH_SHORT).show();

                         }
            }else {
                connectivityredirect();
            }
        }
    }


}
