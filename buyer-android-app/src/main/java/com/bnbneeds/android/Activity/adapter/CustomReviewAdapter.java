package com.bnbneeds.android.Activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.bean.RowItemReview;

import java.util.List;

/**
 * Created by Len on 27-06-2017.
 */

public class CustomReviewAdapter extends ArrayAdapter<RowItemReview> {

    Context context;

    public CustomReviewAdapter(Context context, int resourceId,
                                 List<RowItemReview> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        // ImageView imageView;
        RatingBar rating;
        TextView headline;
        TextView comments;
        TextView reviewby;
        TextView timestamp;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        RowItemReview rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.reviewlist, null);
            holder = new ViewHolder();
            holder.rating = (RatingBar) convertView.findViewById(R.id.ratingreview);
            holder.headline = (TextView) convertView.findViewById(R.id.headline);
            holder.comments = (TextView) convertView.findViewById(R.id.comments);
            holder.reviewby = (TextView) convertView.findViewById(R.id.review_by);
            holder.timestamp = (TextView) convertView.findViewById(R.id.timestamp);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.rating.setRating(rowItem.getRating());
        holder.headline.setText(rowItem.getHeadline());
        holder.comments.setText(rowItem.getComments());
        holder.reviewby.setText("-"+""+rowItem.getReviewedby());
        holder.timestamp.setText("on"+" "+rowItem.getTimestamp());


        return convertView;
    }

}
