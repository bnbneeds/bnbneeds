package com.bnbneeds.android.Activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.bean.RowItem;

import java.util.List;

/**
 * Created by Len on 08-12-2016.
 */

public class CustomListviewAdapter extends ArrayAdapter<RowItem> {

    Context context;

    public CustomListviewAdapter(Context context, int resourceId,
                                 List<RowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
       // ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
        TextView vendor;
        TextView modelid;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        RowItem rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem, null);
            holder = new ViewHolder();
            holder.txtDesc = (TextView) convertView.findViewById(R.id.product_mrpvalue);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.product_name);
          //  holder.imageView = (ImageView) convertView.findViewById(R.id.productImage);
            holder.vendor = (TextView) convertView.findViewById(R.id.vendor_name);
            holder.modelid = (TextView)convertView.findViewById(R.id.model_id);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
       // holder.imageView.setImageResource(rowItem.getImageId());
        holder.vendor.setText(rowItem.getvendor());
        holder.modelid.setText(rowItem.getModelid());

        return convertView;
    }
}