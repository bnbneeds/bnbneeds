package com.bnbneeds.android.Activity.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.adapter.TemplateAdapter;
import com.bnbneeds.android.Activity.db.DbHelper;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.order.CreatePurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.CreatePurchaseListParam;
import com.bnbneeds.app.model.dealer.order.PurchaseItemInfoResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemParam;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.bnbneeds.rest.client.impl.springresttemplate.filters.BNBNeedsReponseErrorHandler;

import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static com.bnbneeds.android.Activity.Activity.ConstantsActivity.pDialog;
import static com.bnbneeds.android.Activity.Activity.HomeActivity.templetname;


/**
 * activity to display all records from SQLite database
 *
 */
public class DisplayTestActivity extends BaseActivity {

    private DbHelper mHelper;
    private SQLiteDatabase dataBase;

    private ArrayList<String> pId = new ArrayList<String>();
    private ArrayList<String> pName = new ArrayList<String>();
    private ArrayList<String> pNameId = new ArrayList<>();
    private ArrayList<Double> pQuantity = new ArrayList<Double>();
    private ArrayList<String> pUnits = new ArrayList<String>();
    private ArrayList<String> pVendor = new ArrayList<String>();
    private ArrayList<String> pVendorId = new ArrayList<String>();
    private ArrayList<String> pRemarks= new ArrayList<String>();
    private TextView TextView_Template_Name;
    private ListView userList;
    private AlertDialog.Builder build;
    private String tempname;
    protected String temp = "NO";
    private String errorMessage;
    private String connection;


    DisplayAdapter disadpt;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_purchase_items_view);

        Bundle b = getIntent().getExtras();


        userList = (ListView) findViewById(R.id.List);
        TextView_Template_Name = (TextView) findViewById(R.id.tv_Template_name);
        mHelper = new DbHelper(this);

        if(templetname == null){
            switch (tempname = b.getString("TEMPLATE_NAME")) {
                case "dummy":Log.d("Error",tempname);
                    break;
            }
        }else if(!(templetname == null)){
            tempname = b.getString("TEMPLATE_NAME");

        }
        TextView_Template_Name.setText("Template : " + tempname);
        //add new record
        findViewById(R.id.btnAdd).setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(),PurchaselistVendorActivity.class);
                i.putExtra("STATUS",temp);
                i.putExtra("TEMPLATE_NAME", tempname);
                startActivity(i);

            }
        });
        //Create PurchaseList
        findViewById(R.id.purchaselist).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                      if(!(disadpt.getCount() == 0)) {
                          new PurchaselistTask().execute();
                      }else{
                          Toast.makeText(getApplicationContext(), "PurchaseList is empty", Toast.LENGTH_SHORT).show();
                      }

            }
        });
        userList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent newActivity = new Intent(DisplayTestActivity.this,UpdateActivity.class);
                newActivity.putExtra("id",pId.get(position));
                newActivity.putExtra("TEMPLATE_NAME", tempname);
                startActivity(newActivity);
            }
        });
        //long click to delete data
        userList.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {

                build = new AlertDialog.Builder(DisplayTestActivity.this);
                build.setTitle("Delete " + pName.get(arg2) + " "
                        + pQuantity.get(arg2) + " " + pUnits.get(arg2) + " " + pVendor.get(arg2));
                build.setMessage("Do you want to delete ?");
                build.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                                int which) {

                                Toast.makeText(
                                        getApplicationContext(),
                                        pName.get(arg2) + " "
                                                + pQuantity.get(arg2) + " "
                                                + pUnits.get(arg2) + " "
                                                + pVendor.get(arg2)
                                                + " is deleted.", Toast.LENGTH_LONG).show();

                                dataBase.delete(
                                        DbHelper.TABLE_NAME,
                                        DbHelper.KEY_ID + "="
                                                + pId.get(arg2), null);
                                displayData();
                                dialog.cancel();
                            }
                        });


                build.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = build.create();
                alert.show();

                return true;
            }
        });
    }



    @Override
    protected void onResume() {
        displayData();
        super.onResume();
    }


    /**
     * displays data from SQLite
     */
    private void displayData() {
        dataBase = mHelper.getWritableDatabase();
        String select = "SELECT * FROM ItemListTable WHERE TEMPLATE_NAME='" + tempname + "'";

        Cursor mCursor = dataBase.rawQuery(select, null);


        pId.clear();
        pName.clear();
        pQuantity.clear();
        pUnits.clear();
        pVendor.clear();
        pNameId.clear();
        pVendorId.clear();
        pRemarks.clear();
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        if (mCursor.moveToFirst()) {
            do {
                pId.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_ID)));
                pName.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_PRODUCTNAME)));
                pNameId.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_PRODUCTNAMEID)));
                pQuantity.add(mCursor.getDouble(mCursor.getColumnIndex(DbHelper.KEY_QUANTITY)));
                pUnits.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_UNITS)));
                pVendor.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_VENDOR)));
                pVendorId.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_VENDORID)));
                pRemarks.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_REMARKS)));

            } while (mCursor.moveToNext());
        }
         disadpt = new DisplayAdapter(DisplayTestActivity.this, pId, pName, pRemarks, pQuantity, pUnits, pVendor);
        userList.setAdapter(disadpt);
        mCursor.close();
    }
    @Override
    public void onBackPressed() {


        Intent home=new Intent(this,HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }

    class PurchaselistTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           setDialog();
        }

        @Override
        protected Integer doInBackground(String... params) {


try {
    CreatePurchaseListParam listParam = new CreatePurchaseListParam();

    for (int i = 0; i < pQuantity.size(); i++) {
        CreatePurchaseItemParam purchaseItemParam = new CreatePurchaseItemParam();
        purchaseItemParam.setProductNameId(pNameId.get(i));
        purchaseItemParam.setRequiredQuantity(pQuantity.get(i));
        purchaseItemParam.setUnit(pUnits.get(i));
        purchaseItemParam.setVendorId(pVendorId.get(i));
        purchaseItemParam.setTags(pRemarks.get(i));
        listParam.add(purchaseItemParam);

    }

    client.buyers().createPurchaseList(LoginActivity.RELATIONSHIP_ID, listParam);
    //   param.setRequiredQuantity(Double.parseDouble(strqty));




}catch (BNBNeedsServiceException e){
    e.printStackTrace();
    TaskResponse resp = e.getTask();
    errorMessage = resp.getMessage().toString();
}catch(ResourceAccessException a){
    connection = a.getMessage();
}
            return 200;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pDialog.dismiss();

            if(connection == null){
                if (errorMessage == null) {
                    Intent home = new Intent(DisplayTestActivity.this, HomeActivity.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(home);
                    Toast.makeText(DisplayTestActivity.this, "Purchaselist has been Created Succesfully", Toast.LENGTH_LONG).show();
                }
                else{
                    String message = errorMessage.substring(0,errorMessage.indexOf(':'));
                    Toast.makeText(DisplayTestActivity.this,message, Toast.LENGTH_SHORT).show();
                    errorMessage = null;
                }


            }else {
                connectivityredirect();

            }
            }
        }
    }

