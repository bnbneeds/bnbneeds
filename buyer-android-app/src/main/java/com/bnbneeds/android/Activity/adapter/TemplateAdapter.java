package com.bnbneeds.android.Activity.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;


import com.bnbneeds.android.Activity.Activity.DisplayActivity;
import com.bnbneeds.android.Activity.Activity.DisplayTestActivity;
import com.bnbneeds.android.Activity.Activity.TemplateActivity;
import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.db.DbHelper;

import java.util.ArrayList;


public class TemplateAdapter extends BaseAdapter {

    SQLiteDatabase dataBase;
    DbHelper mHelper;
    protected static String tempname;
    private Context mContext;
    private ArrayList<String> templateName;

    public TemplateAdapter(Context c, ArrayList<String> tName) {
        this.mContext = c;
        this.templateName=tName;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return templateName.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public View getView(final int pos, View child, ViewGroup parent) {
        Holder mHolder;
        LayoutInflater layoutInflater;
        if (child == null) {
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            child = layoutInflater.inflate(R.layout.listcelltemp, null);
            mHolder = new Holder();
            mHolder.textViewTname=(TextView)child.findViewById(R.id.textViewTempname);
            child.setTag(mHolder);
        }
        else {
            mHolder = (Holder) child.getTag();
        }

        mHolder.textViewTname.setText(templateName.get(pos));
        mHolder.textViewTname.setTextColor(Color.BLACK);
        child.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
               // Toast.makeText(mContext, String.format("%s", "You Clicked " + templateName.toArray()[pos]), Toast.LENGTH_LONG).show();
                tempname= String.format(String.valueOf(templateName.toArray()[pos]));
                Intent in = new Intent(mContext, DisplayTestActivity.class);
                in.putExtra("TEMPLATE_NAME", tempname);
                mContext.startActivity(in);

            }
        });
        /*child.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                tempname= String.format(String.valueOf(templateName.toArray()[pos]));
                AlertDialog.Builder build = new AlertDialog.Builder(mContext);
                build.setTitle("Delete " +tempname);
                build.setMessage("Do you want to delete ?");
                build.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dataBase.delete(DbHelper.TABLE_NAME, DbHelper.KEY_TEMPLATENAME + "=" + tempname, null);
                        dialog.cancel();
                    }
                });
                build.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = build.create();
                alert.show();
                return false;
            }
        });*/
        return child;
    }

    public class Holder {

        TextView textViewTname;

           }

}
