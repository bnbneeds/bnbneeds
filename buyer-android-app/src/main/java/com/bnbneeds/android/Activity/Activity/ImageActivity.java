package com.bnbneeds.android.Activity.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.util.CheckConnectivity;
import com.bnbneeds.app.model.BlobResourceRep;
import com.bnbneeds.app.model.product.ProductImageListResponse;
import com.bnbneeds.app.model.product.ProductInfoListResponse;
import com.bnbneeds.app.model.product.ProductInfoResponse;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

import org.springframework.http.MediaType;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.util.Iterator;
import java.util.List;

import static com.bnbneeds.android.Activity.Activity.ConstantsActivity.pDialog;

public class ImageActivity extends BaseActivity {

    String productId;
    byte[] image, image1, image2, image3 = null;
    String mediaType = null;
    ImageView imageview, imageview1, imageview2, imageview3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if (conn == true) {
            new ImageviewTask().execute();
        } else {
            Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();
        }
        // productId = getIntent().getExtras().getString("Productid");


        imageview = (ImageView) findViewById(R.id.proimg1);
        imageview1 = (ImageView) findViewById(R.id.proimg2);
        imageview2 = (ImageView) findViewById(R.id.proimg3);
        imageview3 = (ImageView) findViewById(R.id.proimg4);

    }

    class ImageviewTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            setDialog();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            try {

               /* ProductInfoResponse productInfo;
                productInfo = client.products(ListviewTestActivity.ids).get();

                ProductImageListResponse imageListResponse = productInfo.getImageList();
                List<BlobResourceRep> blobResourceReps = imageListResponse.getImageLinks();
                for(Iterator<BlobResourceRep> resp = blobResourceReps.iterator();resp.hasNext();) {
                BlobResourceRep imageResponse = resp.next();

                    image = client.products(ListviewTestActivity.ids).getImage(imageResponse.getId(),imageResponse.getMediaType());
                    Log.d("imageid",image.toString());
                }*/
               /* for (BlobResourceRep imageLink : imageListResponse.getImageLinks()) {
                    image = client.products(ListviewTestActivity.ids).getImage(imageLink.getId(), imageLink.getMediaType());
                   Log.d("ImageLink",image.toString());
                }*/



               ProductImageListResponse imageListResponse = client
                        .products(ListviewTestActivity.ids).getImageLinks();
                if (imageListResponse != null) {
                    List<BlobResourceRep> imageListResp = imageListResponse
                            .getImageLinks();
                    if (imageListResp != null && !imageListResp.isEmpty()) {
                           /* BlobResourceRep firstImage = imageListResp.get(0);
                        image = client.products(ListviewTestActivity.ids).getImage(
                                firstImage.getId(), firstImage.getMediaType());*/
                        if(imageListResp.size() >= 1) {
                            image = client.products(ListviewTestActivity.ids).getImage(
                                    imageListResp.get(0).getId(), imageListResp.get(0).getMediaType());
                        }
                        if(imageListResp.size() >= 2) {
                            image1 = client.products(ListviewTestActivity.ids).getImage(
                                    imageListResp.get(1).getId(), imageListResp.get(1).getMediaType());
                        }
                        if(imageListResp.size() >= 3) {
                            image2 = client.products(ListviewTestActivity.ids).getImage(
                                    imageListResp.get(2).getId(), imageListResp.get(2).getMediaType());
                        }
                        if(imageListResp.size() >= 4) {
                            image3 = client.products(ListviewTestActivity.ids).getImage(
                                    imageListResp.get(3).getId(), imageListResp.get(3).getMediaType());
                        }
                       // mediaType = firstImage.getMediaType();
                    }

                }

            } catch (BNBNeedsServiceException e) {
                e.getMessage();

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();

            if (image == null) {
            } else {

                Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
                imageview.setImageBitmap(bmp);
            }
            if (image1 == null) {

            } else {
                Bitmap bmp1 = BitmapFactory.decodeByteArray(image1, 0, image1.length);
                imageview1.setImageBitmap(bmp1);
            }
            if (image2 == null) {

            } else {
                Bitmap bmp2 = BitmapFactory.decodeByteArray(image2, 0, image2.length);
                imageview2.setImageBitmap(bmp2);
            }
            if (image3 == null) {

            } else {
                Bitmap bmp3 = BitmapFactory.decodeByteArray(image3, 0, image3.length);
                imageview3.setImageBitmap(bmp3);
            }
            super.onPostExecute(s);
        }
    }
}
