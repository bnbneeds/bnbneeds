package com.bnbneeds.android.Activity.config;

/**
 * Created by Len on 21-04-2017.
 */

public class SocialMediaURL {

    public static final String fbURL = "https://www.facebook.com/BNBneeds-200313427133066";

    public static final String tweeterURL = "https://twitter.com/BNBneeds";

    public static final String gplusURL = "https://plus.google.com/u/0/109535255238573739544";
}
