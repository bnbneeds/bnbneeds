package com.bnbneeds.android.Activity.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.util.CheckConnectivity;
import com.bnbneeds.android.Activity.util.StringUtils;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.AccountType;
import com.bnbneeds.app.model.account.CreateUserAccountParam;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

import org.springframework.web.client.ResourceAccessException;

/**
 * Created by Len on 25-01-2017.
 */

public class CreateAccountActivity extends ConstantsActivity {

    //private   String PASSWORD_ERROR_MESSAGE = "Password should contain at least 8 letters including small, capital, special character and number.";
    private   String PASSWORD_ERROR_MESSAGE = "Password should be minimum 6 characters with atleast one numeric value.";

    private   String CONFIRM_PASSWORD_ERROR_MESSAGE = "Password does not match. ";

    private EditText regemail,regpassword,regmobile,regusername,regconfpswd;
    static String BUNDLE_USERNAME_VALUE,BUNDLE_PASSWORD_VALUE;
    private Button bcreate,bcancel;
    private String connection;
    private static String registration_response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dialog);
               checkInternetConenction();

          bcreate = (Button)findViewById(R.id.createid);
        bcancel   = (Button)findViewById(R.id.cancelid);
        regemail = (EditText) findViewById(R.id.regemailid);
        regpassword = (EditText) findViewById(R.id.regpassword);
        regmobile = (EditText) findViewById(R.id.regphoneno);
        regusername = (EditText) findViewById(R.id.regusername);
        regconfpswd = (EditText) findViewById(R.id.regcnfpwd);

        int maxLength = 10;
        regmobile.setFilters(new InputFilter[] {
                new InputFilter.LengthFilter(maxLength)
        });
        bcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(CreateAccountActivity.this,LoginActivity.class);
                back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                back.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(back);
            }
        });

        bcreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = regemail.getText().toString();
                String mob = regmobile.getText().toString();
                String pass1 = regpassword.getText().toString();
                String pass2 = regconfpswd.getText().toString();



                if (!StringUtils.isValidEmail(email)) {
                    regemail.setError("Invalid Email address");
                }
                if (!StringUtils.isValidPhoneNumber(mob)) {
                    regmobile.setError("Invalid Mobile Number");
                }

                if (!StringUtils.isValidPassword(pass1)) {
                    regpassword.setError(PASSWORD_ERROR_MESSAGE);
                } else if (!pass1.equals(pass2)) {
                    Toast.makeText(getApplicationContext(), CONFIRM_PASSWORD_ERROR_MESSAGE,
                            Toast.LENGTH_LONG).show();
                } else {
                    if (StringUtils.isValidEmail(email) && StringUtils.isValidPassword(pass1)
                            && StringUtils.isValidPhoneNumber(mob)) {
                            BUNDLE_USERNAME_VALUE = regemail.getText().toString();
                            BUNDLE_PASSWORD_VALUE = regpassword.getText().toString();
                            MOBILE_NO = regmobile.getText().toString();
                            USER = regusername.getText().toString();

                            new SignupTask().execute();




                    } else {
                        Toast.makeText(CreateAccountActivity.this, "Enter Valid Data in the given Fields",
                                Toast.LENGTH_LONG).show();

                    }
                }

            }
        });





    }

    class SignupTask extends AsyncTask<String, Void, Integer> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           setDialog();
        }

        @Override
        protected Integer doInBackground(String... params) {

            try {
                CreateUserAccountParam user = new CreateUserAccountParam();

                user.setUsername(BUNDLE_USERNAME_VALUE);
                user.setPassword(BUNDLE_PASSWORD_VALUE);
                user.setType(AccountType.BUYER);
                user.setAccountName(USER);
                user.setMobileNumber(MOBILE_NO);
                client.users().createAccount(user);
            } catch (BNBNeedsServiceException e) {
                TaskResponse response = e.getTask();

                Log.e("LoginTask",response.getStatus().name());


                return e.getHttpCode();
            }catch(ResourceAccessException a){
                connection = a.getMessage();
            }

            return 200;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pDialog.dismiss();
            if(!(registration_response == "ERROR")) {
                if (connection == null) {

                    Intent success = new Intent(CreateAccountActivity.this, LoginActivity.class);
                    success.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    success.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    success.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(success);
                    Toast.makeText(getApplicationContext(), "Your Account Has Been Created Successfully Please Login to Register", Toast.LENGTH_LONG).show();
                } else {
                    connectivityredirect();
                }
            }else{
                Toast.makeText(getApplicationContext(), "Buyer with name " +BUNDLE_USERNAME_VALUE+ " already exists.", Toast.LENGTH_LONG).show();

            }
        }

    }

}
