package com.bnbneeds.android.Activity.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;


import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnbneeds.android.Activity.BuildConfig;
import com.bnbneeds.android.Activity.R;

import com.bnbneeds.android.Activity.config.SocialMediaURL;
import com.bnbneeds.android.Activity.util.StringUtils;
import com.bnbneeds.app.model.EntityCountResponse;
import com.bnbneeds.app.model.EntityInfoResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;

import org.jsoup.Jsoup;
import org.springframework.web.client.ResourceAccessException;
import android.animation.ValueAnimator;

import static com.bnbneeds.android.Activity.Activity.HomeActivity.DEFAULT_TARGET_URI;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends ConstantsActivity {

    private static final String PASSWORD_ERROR_MESSAGE = "Password should contain at least 8 letters including small, capital, special character and number.";
    private static final String CONFIRM_PASSWORD_ERROR_MESSAGE = "Password does not match. ";
    private CheckBox mpassshow;

    final Context context = this;

    private String connection;

    EditText resetpassword;
    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private Button mEmailSignInButton;
    private Button regbtn;
    TextView forgotpswd, buyercount, vendorcount, productcount;
    int errorMessage;
    private EditText regemail, regpassword, regmobile, regusername, regconfpswd;
    static String BUNDLE_USERNAME_VALUE, BUNDLE_PASSWORD_VALUE;
    String passwordrst, invalidData;
    ImageView fb, twitter, gplus;
    int vendor_count, buyer_count, product_counter;
    boolean autogenratedPassword;
    String currentVersion;
    String newVersion = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        checkInternetConenction();
        new DisplayCount().execute();

        regbtn = (Button) findViewById(R.id.Register);
        forgotpswd = (TextView) findViewById(R.id.rstpwd);
        fb = (ImageView) findViewById(R.id.fb);
        twitter = (ImageView) findViewById(R.id.twitter);
        gplus = (ImageView) findViewById(R.id.gplus);
        buyercount = (TextView) findViewById(R.id.buyerCount);
        vendorcount = (TextView) findViewById(R.id.vendorCount);
        productcount = (TextView) findViewById(R.id.productCount);

            //currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            currentVersion = BuildConfig.VERSION_NAME;
            Log.d("CURRENT",currentVersion);

        new GetVersionCode().execute();
        fb.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fbIntent = new Intent("android.intent.action.VIEW", Uri.parse(SocialMediaURL.fbURL));
                startActivity(fbIntent);
            }
        });
        twitter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent twtIntent = new Intent("android.intent.action.VIEW", Uri.parse(SocialMediaURL.tweeterURL));
                startActivity(twtIntent);
            }
        });
        gplus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gplusIntent = new Intent("android.intent.action.VIEW", Uri.parse(SocialMediaURL.gplusURL));
                startActivity(gplusIntent);
            }
        });
        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mpassshow = (CheckBox) findViewById(R.id.cbshowpwd);
        mpassshow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    // show password
                    mPasswordView.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    mPasswordView.setSelection(mPasswordView.getText().length());
                } else {
                    // hide password
                    mPasswordView.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    mPasswordView.setSelection(mPasswordView.getText().length());

                }
            }
        });
        forgotpswd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(context);
                final View view = li.inflate(R.layout.reset_password, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Reset Password");
                builder.setView(view);
                builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        resetpassword = (EditText) view.findViewById(R.id.reset);
                        passwordrst = resetpassword.getText().toString();
                        if (!passwordrst.isEmpty() && StringUtils.isValidEmail(passwordrst)) {
                            new ResetPasswordTask().execute();
                        } else {
                            Toast.makeText(getApplicationContext(), "Required Fields are Missing", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });


        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                final String email = mEmailView.getText().toString();
                if (!StringUtils.isValidEmail(email)) {
                    mEmailView.setError(getString(R.string.error_invalid_email));
                }

                if (StringUtils.isValidEmail(email)) {
                    BUNDLE_USERNAME_VALUE = mEmailView.getText().toString();
                    BUNDLE_PASSWORD_VALUE = mPasswordView.getText().toString();
                    new LoginTask().execute();
                }
            }
        });

        regbtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent create = new Intent(LoginActivity.this, CreateAccountActivity.class);
                startActivity(create);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
        System.exit(0);
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */

    class ResetPasswordTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                client.auth().resetPassword(passwordrst);
            } catch (ResourceAccessException a) {
                connection = a.getMessage();
            } catch (BNBNeedsServiceException e) {
                invalidData = e.getMessage();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            if (connection == null) {

                if (invalidData == null) {
                    // autogenratedPassword = true;
                    Toast.makeText(getApplicationContext(), "Your password has been reset and has been sent to your registered email.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Record not found with id: " + passwordrst, Toast.LENGTH_SHORT).show();

                }
            } else {

                Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }

        }
    }

    class DisplayCount extends AsyncTask<String, Void, Integer> {


        @Override
        protected Integer doInBackground(String... params) {
            try {
                EntityCountResponse productResponse = client.utilities().getEntityCount("Product");
                product_counter = Math.round(((productResponse.getCount() + 25) / 50) * 50);

                EntityCountResponse buyerResponse = client.utilities().getEntityCount("Buyer");
                buyer_count = Math.round(((buyerResponse.getCount() + 25) / 50) * 50);

                EntityCountResponse vendorResponse = client.utilities().getEntityCount("Vendor");
                vendor_count = Math.round(((vendorResponse.getCount() + 25) / 50) * 50);
            } catch (ResourceAccessException a) {

            }


            return 200;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            animateTextView(0, product_counter, productcount);
            animateTextView(0, buyer_count, buyercount);
            animateTextView(0, vendor_count, vendorcount);
        }
    }

    public void animateTextView(int initialValue, int finalValue, final TextView textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(1500);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                textview.setText(valueAnimator.getAnimatedValue().toString() + "+");

            }
        });
        valueAnimator.start();

    }

    class LoginTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }


        protected Integer doInBackground(String... params) {


            try {


                UserAccountInfoResponse response = client.login(BUNDLE_USERNAME_VALUE.trim().toLowerCase(), BUNDLE_PASSWORD_VALUE);
                RELATIONSHIP_ID = response.getRelationshipId();
                autogenratedPassword = response.isAutoGeneratedPassword();
                USER = response.getAccountName();
                EMAIL_ID = response.getUsername();
                ROLE = response.getRole();

            } catch (BNBNeedsServiceException e) {
                TaskResponse response = e.getTask();
                errorMessage = e.getHttpCode();

                Log.e("LoginTask", response.getStatus().name());
                // [     user     ]

                return e.getHttpCode();
            } catch (ResourceAccessException a) {
                connection = a.getMessage();
            }
            return 200;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pDialog.dismiss();
            try {
                if (401 == errorMessage) {
                    Intent home = new Intent(LoginActivity.this, LoginActivity.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(home);
                    Toast.makeText(LoginActivity.this, "Invalid username or password.", Toast.LENGTH_LONG).show();

                    return;
                }
                if (!connection.isEmpty()) {
                    Intent refresh = new Intent(LoginActivity.this, LoginActivity.class);
                    startActivity(refresh);
                    Toast.makeText(LoginActivity.this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();

                    return;
                }
            } catch (Exception e) {
                Log.e("Login", "Exception ocurred");
            }
            if (autogenratedPassword == true && RELATIONSHIP_ID.contains("Buyer")) {

                Intent reset = new Intent(LoginActivity.this, ChangePasswordActivity.class);
                startActivity(reset);
            } else if (RELATIONSHIP_ID == null) {
                if (ROLE.equals("BUYER")) {
                    Intent registration = new Intent(LoginActivity.this, RegistrationActivity.class);
                    startActivity(registration);
                } else {
                    int pid = android.os.Process.myPid();
                    android.os.Process.killProcess(pid);
                    System.exit(0);
                }


            } else if (RELATIONSHIP_ID.contains("Buyer")) {

                Intent home = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(home);
            } else {
                int pid = android.os.Process.myPid();
                android.os.Process.killProcess(pid);
                System.exit(0);
            }
        }
    }

     class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {



            try {

                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + LoginActivity.this.getPackageName() + "&hl=it").timeout(30000)

                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")


                        .referrer("http://www.google.com")

                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                   Log.d("ONLINE",newVersion);
                return newVersion;

            } catch (Exception e) {

                return newVersion;

            }

        }


        @Override

        protected void onPostExecute(String onlineVersion) {

            super.onPostExecute(onlineVersion);

            if (onlineVersion != null && !onlineVersion.isEmpty()) {

                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {

                    /*AlertDialog alertDialog = new AlertDialog.Builder(
                            LoginActivity.this).create();*/
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);

                    // Setting Dialog Title
                    alertDialog.setTitle("New Version Avaliable");


                    // Setting Dialog Message
                    alertDialog.setMessage("Please update the app to enjoy all the new features.");

                    // Setting Icon to Dialog
                    alertDialog.setIcon(R.drawable.danger);

                    // Setting OK Button

                    alertDialog.setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent rateintent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format(DEFAULT_TARGET_URI,getPackageName())));
                            startActivity(rateintent);
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();

                }

            }

            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);

        }


    }

}