package com.bnbneeds.android.Activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.bean.RowItemVendor;

import java.util.List;

/**
 * Created by Len on 07-01-2017.
 */

public class CustomVendorAdapter extends ArrayAdapter<RowItemVendor> {

    Context context;

    public CustomVendorAdapter(Context context, int resourceId, List<RowItemVendor> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {

        TextView txtTitle;
        TextView txtDesc;
        TextView vendor;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        CustomVendorAdapter.ViewHolder holder = null;
        RowItemVendor rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.vendorlist, null);
            holder = new CustomVendorAdapter.ViewHolder();


            holder.vendor = (TextView) convertView.findViewById(R.id.vendor_name);
            convertView.setTag(holder);
        } else
            holder = (CustomVendorAdapter.ViewHolder) convertView.getTag();



        holder.vendor.setText(rowItem.getVendor());

        return convertView;
    }
}
