package com.bnbneeds.android.Activity.Activity;


import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bnbneeds.android.Activity.R;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.TaskResponse;

import com.bnbneeds.app.model.review.ReviewInfoListResponse;
import com.bnbneeds.app.model.review.ReviewInfoResponse;
import com.bnbneeds.app.model.review.ReviewParam;
import com.bnbneeds.rest.client.core.Reviews;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.bnbneeds.android.Activity.Activity.ConstantsActivity.RELATIONSHIP_ID;
import static com.bnbneeds.android.Activity.Activity.ConstantsActivity.pDialog;
import static com.bnbneeds.android.Activity.Activity.VendorlistActivity.vendorId;

public class VendorReviewActivity extends BaseActivity {
    RatingBar simplerating;
    TextView ratingTitle;
    EditText headline, comments;
    float ratingvalue;
    int ratevalue;
    Button cancel, submit;
    String headLine, vendorComments;
    TextView viewreview;
    protected static String reviewedById, uniquereviewedById, reviewId;
    Map<String, String> queryMap = new HashMap<String, String>();
    JSONObject obj;
    String a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_review);
        checkInternetConenction();
        simplerating = (RatingBar) findViewById(R.id.ratingBar);
        ratingTitle = (TextView) findViewById(R.id.ratetext);
        headline = (EditText) findViewById(R.id.headline);
        comments = (EditText) findViewById(R.id.vendor_comments);
        cancel = (Button) findViewById(R.id.cancelreview);
        submit = (Button) findViewById(R.id.submitreview);
       /* viewreview = (TextView)findViewById(R.id.viewreview);
        viewreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent review = new Intent(VendorReviewActivity.this,ViewVendorReviewsActivity.class);
                startActivity(review);
            }
        });*/
        simplerating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingvalue = simplerating.getRating();
                Toast.makeText(getApplicationContext(), "Rating" + ratingvalue, Toast.LENGTH_SHORT).show();


                if (ratingvalue == 1.0) {
                    ratingTitle.setText("Bad");
                    ratingTitle.setTextColor(Color.RED);
                } else if (ratingvalue == 2.0) {
                    ratingTitle.setText("Fair");
                    ratingTitle.setTextColor(Color.RED);
                } else if (ratingvalue == 3.0) {
                    ratingTitle.setText("Good");
                    ratingTitle.setTextColor(Color.parseColor("#FF8800"));
                } else if (ratingvalue == 4.0) {
                    ratingTitle.setText("Very Good");
                    ratingTitle.setTextColor(Color.parseColor("#458b00"));
                } else if (ratingvalue == 5.0) {
                    ratingTitle.setText("Excellent");
                    ratingTitle.setTextColor(Color.parseColor("#458b00"));
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ratevalue = (int) ratingvalue;
                headLine = headline.getText().toString();
                vendorComments = comments.getText().toString();
                if (!(ratevalue == 0)) {
                    new VendorReviewTask().execute();
                } else {
                    Toast.makeText(getApplicationContext(), "Minimum 1 star should be given", Toast.LENGTH_SHORT).show();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(VendorReviewActivity.this, VendorlistActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });

    }

    class VendorReviewTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
               /* ReviewParam param = new ReviewParam();
                param.setRating(ratevalue);
                param.setHeadline(headLine);
                param.setDescription(vendorComments);
                client.vendorReviews(vendorId).createReview(param);*/


                ReviewInfoListResponse reviewresponse = client.vendorReviews(vendorId).getReviews();


                if (reviewresponse != null) {
                    List<ReviewInfoResponse> review = reviewresponse.getReviews();
                    if (review != null && !review.isEmpty()) {
                        for (Iterator<ReviewInfoResponse> iterator = review.iterator(); iterator.hasNext(); ) {
                            ReviewInfoResponse info = iterator.next();
                            reviewId = info.getId();
                            reviewedById = info.getReviewedBy().getId();
                            if (reviewedById != null) {
                                if (reviewedById.equals(RELATIONSHIP_ID)) {
                                    uniquereviewedById = reviewedById;
                                    break;
                                }


                            }


                        }
                    }
                }

                ReviewParam param = new ReviewParam();
                param.setRating(ratevalue);
                param.setHeadline(headLine);
                param.setDescription(vendorComments);
                if (reviewId == null && uniquereviewedById == null) {
                    client.vendorReviews(vendorId).createReview(param);
                } else if (uniquereviewedById != null) {
                    client.vendorReviews(vendorId).updateReview(reviewId, param);
                } else if (uniquereviewedById == null && reviewId != null) {
                    client.vendorReviews(vendorId).createReview(param);


                }


            } catch (BNBNeedsServiceException e) {
                TaskResponse response = e.getTask();
                a = response.getMessage();
                Log.d("Response review ", a);
                return e.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Thank You for Your Feedback", Toast.LENGTH_SHORT).show();
            Intent home = new Intent(VendorReviewActivity.this, VendordetailsActivity.class);
            home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(home);


        }
    }
}
