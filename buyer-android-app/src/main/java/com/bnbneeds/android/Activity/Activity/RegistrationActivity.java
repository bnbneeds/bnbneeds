package com.bnbneeds.android.Activity.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnbneeds.android.Activity.Constants;
import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.util.CaptchaImage;
import com.bnbneeds.android.Activity.util.CheckConnectivity;
import com.bnbneeds.android.Activity.util.StringUtils;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.EntityInfoResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.BuyerInfoResponse;
import com.bnbneeds.app.model.dealer.CreateBuyerParam;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.mailgun.rest.client.ClientType;
import com.mailgun.rest.client.MailgunClient;
import com.mailgun.rest.client.model.AddMailingListMemberParam;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;

/**
 * Created by Len on 25-01-2017.
 */
public class RegistrationActivity extends ConstantsActivity {

    private static final String CAPTCHA_ERROR_MESSAGE = "Incorrect Captcha";
    private static final String PAN_NUMBER_ERROR_MESSAGE = "Invalid PAN number.";
    private static String full_address;
    private static String txtcaptcha;
    private static String usercaptcha;
    private static String selected_state_value;
    private static int registration_response;
    private static String field_exception;
    private static String selected_city_value;
    private static String selected_salutation;
    private static String selected_buisiness_value;
    private static String selected_buisiness_id;
    private TextView captchatext, panText;
    private ImageView captchaimg;
    private EditText editText_addressline1;
    private EditText editText_addressline2;
    private EditText editText_pincode;
    private EditText editText_organisationname;
    private EditText editText_contactperson;
    private EditText editText_description;
    private EditText editText_secondarymobilenumber;
    private EditText editcaptcha;
    private EditText editText_website;
    private EditText editText_secondaryemail;
    private EditText editText_panno;
    private EditText editText_businesstype;
    private Spinner spinner_state;
    //private Spinner spinner_city;
    private AutoCompleteTextView spinner_city;
    private Spinner spinner_business;
    private Spinner spinner_salutation;
    private String NewBusinessType = "Other";
    private static String description;
    private static String organization_name;
    private static String contact_person;
    private static String panno;
    private static String website;
    private static String mobile2;
    private static String email2;
    private static String bid;
    private String connection;
    TextView terms;

    ArrayList<String> arr = new ArrayList<String>();
    ArrayList<EntityInfoResponse> busiessid = new ArrayList<EntityInfoResponse>();
    // JSON Node names
    private static final String TAG_DATA = "entity";
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "name";
    JSONArray businesstypejson;
    String a;
    JSONObject c;
    String Selected_item;
    String text = "By registering with us you agree that you have understood and accepted the";
    String termsandc = getColoredSpanned("Terms &amp; Conditions", "#4da6ff");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.new_registration_view);
        terms = (TextView) findViewById(R.id.termscond);
        terms.setText(Html.fromHtml(text + " " + termsandc + " " + "of BNBneeds."));
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
                builder.setTitle("BNBneeds Terms & Conditions");
                builder.setMessage(R.string.terms_conditions);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if (conn == true) {
            new BusinessTypeTask().execute();
        } else {
            Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();
        }


        captchaimg = (ImageView) findViewById(R.id.imageView_captcha);

        captchatext = (TextView) findViewById(R.id.textview_captcha);
        editcaptcha = (EditText) findViewById(R.id.edit_usercaptcha);
        CaptchaClass c = new CaptchaImage(5, CaptchaImage.TextOptions.NUMBERS_AND_LETTERS);

        captchaimg.setImageBitmap(c.image);
        captchaimg.setLayoutParams(new LinearLayout.LayoutParams(c.width * 2, c.height * 2));
        captchatext.setText(c.answer);
        captchatext.setTextColor(Color.parseColor("#F84525"));

       /* Intent in = getIntent();
        Bundle rec = in.getExtras();
        BUNDLE_USERNAME_VALUE = rec.getString("BUNDLE_USERNAME_KEY");
        BUNDLE_PASSWORD_VALUE = rec.getString("BUNDLE_PASSWORD_KEY");*/


        // spinner_state = (Spinner) findViewById(R.id.spinner_state);
        //spinner_city = (Spinner) findViewById(R.id.spinner_city);
        //  spinner_city = (AutoCompleteTextView) findViewById(R.id.spinner_city);
        spinner_business = (Spinner) findViewById(R.id.spinner_business_type);
        spinner_salutation = (Spinner) findViewById(R.id.spinner_salutation);
        // editText_pincode = (EditText) findViewById(R.id.edit_pincode);
        editText_panno = (EditText) findViewById(R.id.edit_panno);
        editText_addressline1 = (EditText) findViewById(R.id.edit_address_line1);
        // editText_addressline2 = (EditText) findViewById(R.id.edit_address_line2);
        editText_organisationname = (EditText) findViewById(R.id.edit_organisation_name);
        editText_contactperson = (EditText) findViewById(R.id.edit_contact_person);
        editText_website = (EditText) findViewById(R.id.website);
        editText_secondaryemail = (EditText) findViewById(R.id.edit_secondary_email);
        editText_description = (EditText) findViewById(R.id.edit_description);
        editText_secondarymobilenumber = (EditText) findViewById(R.id.edit_secondary_mobile_number);
        //editText_businesstype = (EditText)findViewById(R.id.newBusiness_type);
        panText = (TextView) findViewById(R.id.pantext);
        panText.setText("Note: PAN Number Will be used to check the genuineness of the firm.This Number Will not be displayed or disclosed to anyone.");
        //selected_state_value = spinner_state.getSelectedItem().toString();
        //selected_city_value = spinner_city.getSelectedItem().toString();
        selected_salutation = spinner_salutation.getSelectedItem().toString();

      /*  ArrayAdapter adpt = new ArrayAdapter(RegistrationActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.array_city));
        spinner_city.setAdapter(adpt);
        spinner_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selected_city_value = parent.getItemAtPosition(position).toString();
                Toast.makeText(getApplicationContext(), "Selected City is " + selected_city_value, Toast.LENGTH_SHORT).show();
            }
        });*/


        //selected_buisiness_value = spinner_business.getSelectedItem().toString();

        //btype.add(new EntityInfoResponse());







        /*full_address = editText_addressline1.getText().toString() + "," + editText_addressline2.getText().toString() + "," +
                selected_state_value + "," + selected_city_value + "," + editText_pincode.getText().toString();*/

        //  textView_primaryemail.setText(BUNDLE_USERNAME_VALUE);

        final Button register = (Button) findViewById(R.id.register_button);
        register.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

              /*  String code = editText_pincode.getText().toString();
                if (code.length() < 6) {
                    editText_pincode.setError("Invalid Pincode");
                }*/

                String orgname = editText_organisationname.getText().toString();
                if (orgname.length() == 0) {
                    editText_organisationname.setError("Please Enter Orgination Name");
                    Toast.makeText(RegistrationActivity.this, "Organisation Name is missing.Scroll to fill", Toast.LENGTH_SHORT).show();
                }
                String personName = editText_contactperson.getText().toString();
                if (personName.length() == 0) {
                    editText_contactperson.setError("Please Enter Person Name");
                }
                String organisationAddress = editText_addressline1.getText().toString();
                if (organisationAddress.length() == 0) {
                    editText_addressline1.setError("Please Enter Valid Address");
                }
                String pan = editText_panno.getText().toString();
                if (!StringUtils.isValidPAN(pan)) {
                    editText_panno.setError(PAN_NUMBER_ERROR_MESSAGE);
                }

                String secondary_email = editText_secondaryemail.getText().toString();
                if (!StringUtils.isValidEmail(secondary_email)) {
                    editText_secondaryemail.setError(EMAIL_ERROR_MESSAGE);
                }

               /* String web = editText_website.getText().toString();
                if (!StringUtils.isValidWebsite(web)) {
                    editText_website.setError(WEBSITE_ERROR_MESSAGE);
                }*/

                String secondary_mobile = editText_secondarymobilenumber.getText().toString();
                if (!StringUtils.isValidPhoneNumber(secondary_mobile)) {
                    editText_secondarymobilenumber.setError(MOBILE_ERROR_MESSAGE);
                }

                txtcaptcha = captchatext.getText().toString();
                usercaptcha = editcaptcha.getText().toString();
                if (!txtcaptcha.equals(usercaptcha)) {
                    editcaptcha.setText("");
                    editcaptcha.setError(CAPTCHA_ERROR_MESSAGE);
                    CaptchaClass c = new CaptchaImage(5, CaptchaImage.TextOptions.NUMBERS_AND_LETTERS);
                    captchaimg.setImageBitmap(c.image);
                    captchaimg.setLayoutParams(new LinearLayout.LayoutParams(c.width * 2, c.height * 2));
                    captchatext.setText(c.answer);
                }
               
                if (orgname.length() > 0 && personName.length() > 0 && organisationAddress.length() > 0 &&  StringUtils.isValidPAN(pan) && txtcaptcha.equals(usercaptcha) && StringUtils.isValidPhoneNumber(secondary_mobile) && StringUtils.isValidEmail(secondary_email)) {

                    register.setBackgroundColor(Color.GREEN);

                    organization_name = editText_organisationname.getText().toString();
                    contact_person = selected_salutation + " " + editText_contactperson.getText().toString();
                    panno = editText_panno.getText().toString();
                    email2 = editText_secondaryemail.getText().toString();
                    mobile2 = editText_secondarymobilenumber.getText().toString();
                    website = editText_website.getText().toString();
                    description = editText_description.getText().toString();
                   /* full_address = editText_addressline1.getText().toString() + "," + editText_addressline2.getText().toString() + "," +
                            selected_state_value + "," + selected_city_value + "," + editText_pincode.getText().toString(); */

                    full_address = editText_addressline1.getText().toString();


                    new BuyerRegistrationTask().execute();


                }
            }

        });

    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public void onBackPressed() {
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
        System.exit(0);
        client.logout();

        return;
    }

    class BuyerRegistrationTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected Integer doInBackground(String... params) {
            try {


                //Creating Business Type before buyer registration
              /*  CreateBusinessTypeParam business = new CreateBusinessTypeParam();
                business.setName(email2);
                business.setDescription(description);
                client.businessTypes().create(business);*/


                //Trying to get businessType id
               /* BusinessTypeInfoResponse response = client.businessTypes().getBusinesstypeid();
                  business_type = response.getId();*/


                // Registring New Buyer
                CreateBuyerParam param = new CreateBuyerParam();
                param.setOrganizationName(organization_name);
                param.setAddress(full_address);
                param.setContactName(contact_person);
                param.setPan(panno);
                param.setEmailAddress(email2);
                param.setMobileNumber(mobile2);
                param.setDescription(description);
                param.setWebsite(website);
                param.setBusinessTypeId(bid);
                //param.setBusinessTypeId(email2);

               CreateResourceResponse createResponse = client.buyers().create(param);
                   registration_response = createResponse.getHttpCode();

            } catch (BNBNeedsServiceException e) {
                TaskResponse response = e.getTask();
                field_exception = response.getMessage();
                Log.e("Registartion Task", response.getMessage());
                return e.getHttpCode();
            } catch (ResourceAccessException a) {
                connection = a.getMessage();
            }
         return registration_response;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pDialog.dismiss();
            if (registration_response == 201) {
                if (connection == null) {
                    Intent createbuyerIntent = new Intent(RegistrationActivity.this, LoginActivity.class);
                    createbuyerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    createbuyerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    createbuyerIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(createbuyerIntent);
                    new MailListingTask().execute();
                    Toast.makeText(getApplicationContext(), "You have been Registered Successfully with us", Toast.LENGTH_LONG).show();
                } else {
                    connectivityredirect();
                }
            } else {
                Intent createbuyerIntent = new Intent(RegistrationActivity.this, LoginActivity.class);
                createbuyerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                createbuyerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                createbuyerIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(createbuyerIntent);
                Toast.makeText(getApplicationContext(), field_exception, Toast.LENGTH_LONG).show();

            }
        }
    }

    class BusinessTypeTask extends AsyncTask<String, Void, Integer> {


        @Override
        protected Integer doInBackground(String... params) {




            /*EntityInfoListResponse response = client.businessTypes().listBulk();
            btype = response.getEntityInfoList();*/

            a = client.businessTypes().listBulkAsString(1000);
            try {
                c = new JSONObject(a);

                businesstypejson = c.getJSONArray(TAG_DATA);
                for (int i = 0; i < businesstypejson.length(); i++) {

                    JSONObject obj = businesstypejson.getJSONObject(i);

                    //To get businessType id
                    EntityInfoResponse et = new EntityInfoResponse();
                    et.setId(obj.getString(TAG_ID));
                    busiessid.add(et);

                    selected_buisiness_value = obj.getString(TAG_NAME);
                    selected_buisiness_id = obj.getString(TAG_ID);

                    arr.add(selected_buisiness_value);


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            // System.out.println("businessTypeData " +selected_buisiness_value);


            return 200;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);


            ArrayAdapter adp = new ArrayAdapter(RegistrationActivity.this, android.R.layout.simple_spinner_dropdown_item, arr);

            spinner_business.setAdapter(adp);


            spinner_business.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    // TODO Auto-generated method stub

                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    Selected_item = parent.getSelectedItem().toString();
                    bid = busiessid.get(position).getId();
                   /* if(Selected_item.equals(NewBusinessType)){
                        editText_businesstype.setVisibility(View.VISIBLE);
                    }*/

                    try {
                        JSONObject jsonobj = new JSONObject(new String(Selected_item));
                        bid = jsonobj.getString(TAG_ID);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    // Toast.makeText(RegistrationActivity.this, "Your Selected : -" +bid, Toast.LENGTH_SHORT).show();


                }

                public void onNothingSelected(AdapterView<?> arg0) {
                    // TODO Auto-generated method stub
                    Toast.makeText(RegistrationActivity.this, "Your Selected : -",
                            Toast.LENGTH_SHORT).show();
                }

            });

        }
    }

    class MailListingTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                MailgunClient mailgunClient = new MailgunClient(ClientType.android, Constants.domain, Constants.apikey);

                AddMailingListMemberParam param = new AddMailingListMemberParam();
                param.setAddress(EMAIL_ID);
                param.setDescription(description);
                param.setName(USER);
                mailgunClient.addMailingListMember(param, Constants.BUYERS);
                mailgunClient.addMailingListMember(param, Constants.ALL_DELEARS);
            } catch (ResourceAccessException r) {
                  Log.d("Mailing Exception",r.toString());
            }

            return null;
        }
    }

}
