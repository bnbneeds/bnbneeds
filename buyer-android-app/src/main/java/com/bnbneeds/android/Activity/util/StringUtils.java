package com.bnbneeds.android.Activity.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Admin on 10/17/2015.
 */
public class StringUtils {

    // validating email id
    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    public static boolean isValidPassword(String pass) {
       // String password_pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!.,])(?=\\S+$).{8,}$";
        String password_pattern = "^.*(?=.{6,})(?=.*\\d)(?=.*[A-Za-z]).*$";
        Pattern pattern = Pattern.compile(password_pattern);
        Matcher matcher = pattern.matcher(pass);
        return matcher.matches();
    }
              //validating PAN no
    public static boolean isValidPAN(String pan) {
        String pan_pattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
        Pattern pattern = Pattern.compile(pan_pattern);
        Matcher matcher = pattern.matcher(pan);
        return matcher.matches();
    }
                // Mobile no validation
    public static boolean isValidPhoneNumber(String phoneNumber) {
        String mob_pattern = "[0-9]{10}";
        Pattern pattern = Pattern.compile(mob_pattern);
        Matcher matcher = pattern.matcher(phoneNumber);
        return matcher.matches();
    }

    public static boolean isValidWebsite(String website){
        String website_pattern = "^(www)*(\\.[_A-Za-z0-9-\\+])+([_A-Za-z0-9-]+)*(\\.[A-Za-z]{2,3})*(\\.[A-Za-z]{2,3}?)$";
        Pattern pattern = Pattern.compile(website_pattern);
        Matcher matcher = pattern.matcher(website);
        return matcher.matches();

    }

}
