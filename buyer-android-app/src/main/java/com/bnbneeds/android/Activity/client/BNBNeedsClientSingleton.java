package com.bnbneeds.android.Activity.client;

import com.bnbneeds.android.Activity.Constants;
import com.bnbneeds.android.Activity.config.Config;
import com.bnbneeds.rest.client.BNBNeedsClient;
import com.bnbneeds.rest.client.ClientType;

/**
 * Created by Len on 21-01-2017.
 */

public class BNBNeedsClientSingleton {

    private static BNBNeedsClient client;

    private BNBNeedsClientSingleton() {

    }
    public static synchronized void createNewInstance(String URL) {
        //URL = http://192.168.2.1:8082
        client = BNBNeedsClient.newClient(ClientType.android, URL);
    }

    public static BNBNeedsClient getInstance() {
        if(client == null) {
            synchronized (BNBNeedsClientSingleton.class) {
                createNewInstance(Constants.URL);
            }
        }
        return client;
    }
}
