package com.bnbneeds.android.Activity.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.adapter.CustomListviewAdapter;
import com.bnbneeds.android.Activity.bean.RowItem;
import com.bnbneeds.android.Activity.util.CheckConnectivity;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.product.ProductInfoListResponse;
import com.bnbneeds.app.model.product.ProductInfoResponse;
import com.bnbneeds.rest.client.exceptions.BNBNeedsServiceException;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static com.bnbneeds.android.Activity.Activity.ConstantsActivity.pDialog;


public class ListviewTestActivity extends BaseActivity {

    // List view
    private ListView lv;
    List<RowItem> rowItems;

    private String errorexcep;

    // Listview Adapter
    CustomListviewAdapter adapter;

    ArrayList<String> productlist_Name = new ArrayList<>();
    ArrayList<String> productlist_Price = new ArrayList<>();
    ArrayList<String> productlist_Id = new ArrayList<>();
    ArrayList<String> vendorlist = new ArrayList<>();
    ArrayList<String> vendoridlist = new ArrayList<>();
    ArrayList<String> modelidlist = new ArrayList<>();

    protected static String ids, vendorid, selected_product;


    private Double Productprice;
    private String Productname, Productid, vendorname, modelId;

    String[] productnamestring;
    String[] productpricestring;
    String[] productidstring;
    String[] vendorstring;
    String[] vendoridstring;
    String[] modelidstring;

    FloatingActionButton fab;

    // Search EditText
    EditText inputSearch;


    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;
    // Listview Data


    public String[] titles = new String[]{"Carrot",
            "Orange", "Bag", "Magnifier"};

    public String[] vendor = new String[]{"MM",
            "BNB", "ABC", "QRC", "DEG"};

    public String[] descriptions = new String[]{
            "120",
            "60", "250",
            "54", "99"};

    // public Integer[] images = { R.drawable.carrot, R.drawable.orange, R.drawable.bnbneeds, R.drawable.search, R.drawable.carrot,R.drawable.orange,R.drawable.carrot };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        fab = (FloatingActionButton) findViewById(R.id.filter);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent filter = new Intent(ListviewTestActivity.this, FilterActivity.class);
                startActivity(filter);
            }
        });
        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if (conn == true) {
            new ListViewTask().execute();
        } else {
            Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onBackPressed() {
        Intent home = new Intent(this, HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }

    class ListViewTask extends AsyncTask<String, Void, Integer> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected Integer doInBackground(String... params) {


            ProductInfoListResponse listResponse = client.products().list(1000);
            if (listResponse != null) {
                List<ProductInfoResponse> productlist = listResponse.getProductInfoResponseList();
                if (productlist != null && !productlist.isEmpty()) {

                    for (Iterator<ProductInfoResponse> iterator = productlist.iterator(); iterator.hasNext(); ) {
                        ProductInfoResponse productInfo = iterator.next();

                        Productname = productInfo.getProductName().getName();
                        Productprice = productInfo.getCostPrice();
                        Productid = productInfo.getId();
                        vendorname = productInfo.getVendor().toString();
                        vendorid = productInfo.getVendor().getId();
                        modelId = productInfo.getModelId();


               /* URI uri = productInfo.getImages().getLinkRef();
             Response ab=   client.products().getImage("TIk5lJkwoR_8ehIyyj_DgA","image/png");

                 System.out.println("Image link"+uri);*/

                        productlist_Name.add(Productname);
                        productlist_Price.add(String.valueOf(Productprice));
                        productlist_Id.add(Productid);
                        vendorlist.add(vendorname.substring(52));
                        vendoridlist.add(vendorid);
                        modelidlist.add(modelId);


                        productnamestring = productlist_Name.toArray(new String[productlist_Name.size()]);
                        productpricestring = productlist_Price.toArray(new String[productlist_Price.size()]);
                        productidstring = productlist_Id.toArray(new String[productlist_Id.size()]);
                        vendorstring = vendorlist.toArray(new String[vendorlist.size()]);
                        vendoridstring = vendoridlist.toArray(new String[vendoridlist.size()]);
                        modelidstring = modelidlist.toArray(new String[modelidlist.size()]);


                    }
                } else {
                    Intent home = new Intent(ListviewTestActivity.this, HomeActivity.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(home);
                    pDialog.dismiss();
                }
            }
            return 200;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pDialog.dismiss();

            if (productnamestring != null) {
                rowItems = new ArrayList<RowItem>();
                for (int i = 0; i < productpricestring.length; i++) {
                    RowItem item = new RowItem(productnamestring[i], modelidstring[i], productpricestring[i], vendorstring[i], productidstring[i], vendoridstring[i]);
                    rowItems.add(item);
                }

                lv = (ListView) findViewById(R.id.list_view);
                inputSearch = (EditText) findViewById(R.id.inputSearch);


                // Adding items to listview

                adapter = new CustomListviewAdapter(ListviewTestActivity.this, R.layout.listitem, rowItems);
                lv.setAdapter(adapter);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                 /*String selected_product = rowItems.get(position).getTitle().toString();
                    ids = rowItems.get(position).getProductid().toString();
                   vendorid = rowItems.get(position).getVendorid().toString();*/

                        selected_product = adapter.getItem(position).getTitle().toString();
                        ids = adapter.getItem(position).getProductid().toString();
                        vendorid = adapter.getItem(position).getVendorid().toString();


                        // Toast.makeText(getApplicationContext(),"You selected "+vendorid,Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(ListviewTestActivity.this, EnquiryActivity.class);
                        Bundle bundle = new Bundle();
                   /*bundle.putString("selectedname",selected_product);
                   bundle.putString("selectedid",ids);
                   bundle.putString("vendorid",vendorid);*/
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                });
            } else {
                Toast.makeText(getApplicationContext(), "No data", Toast.LENGTH_LONG).show();
                Intent home = new Intent(ListviewTestActivity.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);


            }
            if (inputSearch != null) {
                inputSearch.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        // When user changed the Text
                        ListviewTestActivity.this.adapter.getFilter().filter(cs);
                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {
                        // TODO Auto-generated method stub
                    }
                });
            } else {
                Intent home = new Intent(ListviewTestActivity.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }


        }
    }


}
