package com.bnbneeds.android.Activity.Activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.adapter.CustomplistVendorAdapter;
import com.bnbneeds.android.Activity.bean.RowItemPlistVendor;
import com.bnbneeds.android.Activity.bean.RowItemVendor;
import com.bnbneeds.android.Activity.util.CheckConnectivity;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.dealer.VendorListResponse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.bnbneeds.android.Activity.Activity.ConstantsActivity.pDialog;

/**
 * Created by Len on 20-02-2017.
 */

public class PurchaselistVendorActivity extends BaseActivity {
    private String vendor;
    List<RowItemPlistVendor> rowItems;
    ArrayList<String> arrvendor = new ArrayList<>();
    ArrayList<String> vendornameid = new ArrayList<>();
    ListView list;
    CustomplistVendorAdapter listvendor;
    String[] vendorstring;
    String[] vendoridstring;
    String tempname;
    EditText inputSearch;
protected static String vendorname;
    protected static String vendorid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendorlist_purchase);

        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if(conn == true) {
            new VendorList().execute();
        }else{
            Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();
        }


        Bundle b = getIntent().getExtras();
       tempname = b.getString("TEMPLATE_NAME");

        list = (ListView)findViewById(R.id.vendorlist);
    }

    @Override
    public void onBackPressed() {
        Intent home=new Intent(this,HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }
    class VendorList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {



            VendorListResponse response = client.vendors().list(1000);
            if (response != null) {
            List<NamedRelatedResourceRep> vendorlist = response.getVendorList();
                if (vendorlist != null && !vendorlist.isEmpty()) {
            for (Iterator<NamedRelatedResourceRep> iterator = vendorlist.iterator(); iterator.hasNext();) {
                NamedRelatedResourceRep vendorinfo = iterator.next();
                vendor =  vendorinfo.getName();

                arrvendor.add(vendor);
                vendornameid.add(vendorinfo.getId());

                vendorstring = arrvendor.toArray(new String[arrvendor.size()]);
                vendoridstring = vendornameid.toArray(new String[vendornameid.size()]);

            }


                } else {
                    Intent home = new Intent(PurchaselistVendorActivity.this, HomeActivity.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(home);
                    pDialog.dismiss();

                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
           pDialog.dismiss();
            if (vendorstring != null) {
            rowItems = new ArrayList<RowItemPlistVendor>();
            for (int i = 0; i < vendoridstring.length; i++) {
                RowItemPlistVendor item = new RowItemPlistVendor(vendorstring[i],vendoridstring[i]);
                rowItems.add(item);
            }
            inputSearch = (EditText)findViewById(R.id.inputSearchvendor);
            listvendor = new CustomplistVendorAdapter(PurchaselistVendorActivity.this,R.layout.plistvendor,rowItems);
            list.setAdapter(listvendor);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                     vendorname = listvendor.getItem(position).getVendor();
                    vendorid =listvendor.getItem(position).getVendorid();




                        Intent intent = new Intent(PurchaselistVendorActivity.this, PurchaselistProductActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("selectedname", vendorname);
                        bundle.putString("vendorid", vendorid);
                        bundle.putString("TEMPLATE_NAME",tempname);
                        intent.putExtras(bundle);
                        startActivity(intent);






                    // Toast.makeText(getApplicationContext(),"You selected "+vendornameid,Toast.LENGTH_LONG).show();
                }
            });
            } else {
                Toast.makeText(getApplicationContext(),"No data",Toast.LENGTH_LONG).show();
                Intent home = new Intent(PurchaselistVendorActivity.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
            if (inputSearch != null) {
            inputSearch.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                    // When user changed the Text
                    PurchaselistVendorActivity.this.listvendor.getFilter().filter(cs);
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                              int arg3) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                }
            });
            } else {
                Intent  home = new Intent(PurchaselistVendorActivity.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        }
    }
}

