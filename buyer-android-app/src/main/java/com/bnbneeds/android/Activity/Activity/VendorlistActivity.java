package com.bnbneeds.android.Activity.Activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.adapter.CustomVendorAdapter;
import com.bnbneeds.android.Activity.bean.RowItemVendor;
import com.bnbneeds.android.Activity.util.CheckConnectivity;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.dealer.VendorListResponse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.bnbneeds.android.Activity.Activity.ConstantsActivity.pDialog;

public class VendorlistActivity extends BaseActivity {
    private String vendor;
    List<RowItemVendor> rowItems;
    ArrayList<String> arrvendor = new ArrayList<>();
    ArrayList<String> vendornameid = new ArrayList<>();
   private ListView list;
    CustomVendorAdapter listvendor;
    String[] vendorstring;
    String[] vendoridstring;
    protected static String vendorId;
    EditText inputSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendorlist);


        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if(conn == true) {
            new VendorList().execute();
        }else{
            Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onBackPressed() {
        Intent home=new Intent(this,HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }
    class VendorList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {

            VendorListResponse response = client.vendors().list(1000);
            if (response != null) {
            List<NamedRelatedResourceRep> vendorlist = response.getVendorList();

                if (vendorlist != null && !vendorlist.isEmpty()) {
            for (Iterator<NamedRelatedResourceRep> iterator = vendorlist.iterator(); iterator.hasNext();) {
                NamedRelatedResourceRep vendorinfo = iterator.next();
                vendor =  vendorinfo.getName();

                arrvendor.add(vendor);
               vendornameid.add(vendorinfo.getId());

                vendorstring = arrvendor.toArray(new String[arrvendor.size()]);
                vendoridstring = vendornameid.toArray(new String[vendornameid.size()]);

            }
                } else {
                    Intent home = new Intent(VendorlistActivity.this, HomeActivity.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(home);
                    pDialog.dismiss();

                }
            }



            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
               pDialog.dismiss();
            if (vendorstring != null) {
            rowItems = new ArrayList<RowItemVendor>();
            for (int i = 0; i < vendoridstring.length; i++) {
                RowItemVendor item = new RowItemVendor(vendorstring[i],vendoridstring[i]);
                rowItems.add(item);

            }

            list = (ListView)findViewById(R.id.vendorlist);


            listvendor = new CustomVendorAdapter(VendorlistActivity.this,R.layout.vendorlist,rowItems);
            list.setAdapter(listvendor);
            list.setTextFilterEnabled(true);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String vendorname = listvendor.getItem(position).getVendor();
                     vendorId =listvendor.getItem(position).getVendorid();

                    Intent intent = new Intent(VendorlistActivity.this,VendordetailsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("selectedname",vendorname);
                    bundle.putString("vendorid",vendorId);
                    intent.putExtras(bundle);
                    startActivity(intent);

                   // Toast.makeText(getApplicationContext(),"You selected "+vendornameid,Toast.LENGTH_LONG).show();
                }
            });
            } else {
                Toast.makeText(getApplicationContext(),"No data",Toast.LENGTH_LONG).show();
                Intent home = new Intent(VendorlistActivity.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
            inputSearch = (EditText) findViewById(R.id.inputSearch);
            if (inputSearch != null) {
            inputSearch.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                    // When user changed the Text

                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                              int arg3) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                    String text = inputSearch.getText().toString();
                    listvendor.getFilter().filter(text);
                }
            });
            } else {
                Intent home = new Intent(VendorlistActivity.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        }
    }
}
