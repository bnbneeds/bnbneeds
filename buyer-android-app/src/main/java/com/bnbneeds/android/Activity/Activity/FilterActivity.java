package com.bnbneeds.android.Activity.Activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.adapter.CustomplistVendorAdapter;
import com.bnbneeds.android.Activity.bean.RowItemPlistVendor;
import com.bnbneeds.android.Activity.util.CheckConnectivity;
import com.bnbneeds.app.model.EntityInfoResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.dealer.VendorListResponse;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.bnbneeds.android.Activity.Activity.ConstantsActivity.pDialog;

public class FilterActivity extends BaseActivity {

    List<RowItemPlistVendor> rowItems;
    ArrayList<String> arrvendor = new ArrayList<>();
    ArrayList<String> vendornameid = new ArrayList<>();
    ArrayList<String> arrproName = new ArrayList<>();
    ArrayList<String> productnameid = new ArrayList<>();
    ArrayList<String> arrproCategory = new ArrayList<>();
    ArrayList<String> productcategoryid = new ArrayList<>();
    ArrayList<String> arrproType = new ArrayList<>();
    ArrayList<String> producttypeid = new ArrayList<>();
    String[] productNamestring;
    String[] productNameidstring;
    String[] vendorstring;
    String[] vendoridstring;
    Spinner productcategoryspinner,producttypespinner;
    protected static String vendorId = null, productNameId = null,productTypeId,productCategoryId;
    String vendor, productname,productName,productCategory,productType;
    String projson,procategoryjson,protypejson;
    Button apply;
    AutoCompleteTextView actvproductName,actvvendorName;
    EditText priceFrom, priceTo;
    private static final int SEARCH_RESULT_SIZE = 1000;
    protected static String maxCostPrice, minCostPrice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        priceFrom = (EditText) findViewById(R.id.cpfrom);
        priceTo = (EditText) findViewById(R.id.cpto);
        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if (conn == true) {
            new VendorList().execute();
        }



        apply = (Button) findViewById(R.id.applyfilter);
        actvproductName = (AutoCompleteTextView)findViewById(R.id.actvproductnames);
        actvvendorName = (AutoCompleteTextView)findViewById(R.id.actvvendors);
        productcategoryspinner = (Spinner)findViewById(R.id.productCategoryspinner);
        producttypespinner = (Spinner)findViewById(R.id.productTypespinner);

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(priceFrom.length()!=0 && priceTo.length()!=0) {
                    int checkminprice = Integer.parseInt(priceFrom.getText().toString());
                    int checkmaxprice = Integer.parseInt(priceTo.getText().toString());
                    if (checkminprice >= 0 && checkmaxprice >= checkminprice) {
                        minCostPrice = priceFrom.getText().toString();
                        maxCostPrice = priceTo.getText().toString();
                        actvproductName.setText("");
                        actvvendorName.setText("");

                        Intent search = new Intent(FilterActivity.this, SearchProductActivity.class);
                        // Toast.makeText(getApplicationContext(), "Min n Max Price "+minCostPrice+" "+maxCostPrice, Toast.LENGTH_SHORT).show();
                        startActivity(search);

                    } else {
                        Toast.makeText(getApplicationContext(), "Please Check Your Price Range From and To values", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (priceFrom.length() == 0) {
                        priceFrom.setError("Field Should not be Blank");
                    }
                    if(priceTo.length()==0){
                        priceTo.setError("Field Should not be Blank");
                }
                }

            }
        });
    }

    class VendorList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {

            projson = client.productNames().listBulkAsString(SEARCH_RESULT_SIZE);


            try {
                  arrproName.clear();
                productnameid.clear();
                JSONObject json = new JSONObject(projson);
                JSONArray productarray = json.getJSONArray("entity");
                for (int i = 0; i < productarray.length(); i++) {
                    JSONObject obj = productarray.getJSONObject(i);



                    productnameid.add(obj.getString("id"));

                    productname = obj.getString("name");

                    arrproName.add(productname);
                     productNamestring = arrproName.toArray(new String[arrproName.size()]);
                    productNameidstring = productnameid.toArray(new String[productnameid.size()]);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            procategoryjson = client.productCategories().listBulkAsString(SEARCH_RESULT_SIZE);


            try {
                arrproCategory.add("Select Product Category");
                productcategoryid.add("No Id");
                JSONObject json = new JSONObject(procategoryjson);
                JSONArray productarray = json.getJSONArray("entity");
                for (int i = 0; i < productarray.length(); i++) {
                    JSONObject obj = productarray.getJSONObject(i);

                    //To get ProductnameId
                    EntityInfoResponse ent = new EntityInfoResponse();
                    ent.setId(obj.getString("id"));

                    productcategoryid.add(obj.getString("id"));

                    arrproCategory.add(obj.getString("name"));

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            protypejson = client.productTypes().listBulkAsString(SEARCH_RESULT_SIZE);


            try {
                arrproType.add("Select Product Type");
                producttypeid.add("No Id");
                JSONObject json = new JSONObject(protypejson);
                JSONArray productarray = json.getJSONArray("entity");
                for (int i = 0; i < productarray.length(); i++) {
                    JSONObject obj = productarray.getJSONObject(i);

                    //To get ProductnameId
                    EntityInfoResponse ent = new EntityInfoResponse();
                    ent.setId(obj.getString("id"));

                    producttypeid.add(obj.getString("id"));

                    arrproType.add(obj.getString("name"));

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            VendorListResponse response = client.vendors().list(SEARCH_RESULT_SIZE);
            if (response != null) {
                List<NamedRelatedResourceRep> vendorlist = response.getVendorList();

                if (vendorlist != null && !vendorlist.isEmpty()) {
                    arrvendor.clear();
                    vendornameid.clear();
                    for (Iterator<NamedRelatedResourceRep> iterator = vendorlist.iterator(); iterator.hasNext(); ) {
                        NamedRelatedResourceRep vendorinfo = iterator.next();

                        arrvendor.add(vendorinfo.getName());
                        vendornameid.add(vendorinfo.getId());
                        vendorstring = arrvendor.toArray(new String[arrvendor.size()]);
                        vendoridstring = vendornameid.toArray(new String[vendornameid.size()]);

                    }
                } else {
                    Intent home = new Intent(FilterActivity.this, ListviewTestActivity.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(home);
                    pDialog.dismiss();

                }
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            if (arrvendor != null) {
                rowItems = new ArrayList<RowItemPlistVendor>();
                for (int i = 0; i < vendorstring.length; i++) {
                    RowItemPlistVendor item = new RowItemPlistVendor(vendorstring[i],vendoridstring[i]);
                    rowItems.add(item);
                }
                final CustomplistVendorAdapter  adptvendor = new CustomplistVendorAdapter(FilterActivity.this,android.R.layout.simple_list_item_1,rowItems);
                actvvendorName.setAdapter(adptvendor);
                actvvendorName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        vendor = adptvendor.getItem(position).getVendor().toString();
                        actvvendorName.setText(vendor);
                        vendorId = adptvendor.getItem(position).getVendorid().toString();
                       // Toast.makeText(getApplicationContext(), "Ur id is " + vendorId, Toast.LENGTH_LONG).show();
                    }
                });
            }
            if (arrproName != null) {
                rowItems = new ArrayList<RowItemPlistVendor>();
                for (int i = 0; i < productNamestring.length; i++) {
                    RowItemPlistVendor item = new RowItemPlistVendor(productNamestring[i],productNameidstring[i]);
                    rowItems.add(item);
                }

              final CustomplistVendorAdapter  adptproduct = new CustomplistVendorAdapter(FilterActivity.this,android.R.layout.simple_list_item_1,rowItems);
                actvproductName.setAdapter(adptproduct);
                actvproductName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        productName = adptproduct.getItem(position).getVendor().toString();
                      actvproductName.setText(productName);

                       productNameId =  adptproduct.getItem(position).getVendorid().toString();


                    }
                });

            }
            if (arrproCategory != null) {
                ArrayAdapter<String> adppcategory = new ArrayAdapter<>(FilterActivity.this, android.R.layout.simple_list_item_1, arrproCategory);
                productcategoryspinner.setAdapter(adppcategory);
                productcategoryspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                        productCategory = parent.getSelectedItem().toString();
                        productCategoryId = productcategoryid.get(position);


                        try {
                            JSONObject c = new JSONObject(new String(productCategory));
                            productCategoryId = c.getString("id");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
            if (arrproType != null) {
                ArrayAdapter<String> adpptype = new ArrayAdapter<>(FilterActivity.this, android.R.layout.simple_list_item_1, arrproType);
                producttypespinner.setAdapter(adpptype);
                producttypespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                        productType = parent.getSelectedItem().toString();
                        productTypeId = producttypeid.get(position);


                        try {
                            JSONObject c = new JSONObject(new String(productType));
                            productTypeId = c.getString("id");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }

    }
}