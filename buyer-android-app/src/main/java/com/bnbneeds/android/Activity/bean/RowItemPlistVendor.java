package com.bnbneeds.android.Activity.bean;

/**
 * Created by Len on 21-02-2017.
 */

public class RowItemPlistVendor {
    String vendor;
    String vendorid;

    public  RowItemPlistVendor(String vendor, String vendorid) {
        this.vendor = vendor;
        this.vendorid = vendorid;

    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getVendorid() {
        return vendorid;
    }

    public void setVendorid(String vendorid) {
        this.vendorid = vendorid;
    }

    @Override
    public String toString() {
        return vendor + "\n" + vendorid;
    }
}
