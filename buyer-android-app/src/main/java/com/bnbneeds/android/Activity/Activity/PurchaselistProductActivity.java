package com.bnbneeds.android.Activity.Activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.adapter.CustomVendorAdapter;
import com.bnbneeds.android.Activity.adapter.CustomplistVendorAdapter;
import com.bnbneeds.android.Activity.bean.RowItemPlistVendor;
import com.bnbneeds.android.Activity.bean.RowItemVendor;
import com.bnbneeds.android.Activity.util.CheckConnectivity;
import com.bnbneeds.app.model.EntityInfoResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.dealer.EnquiryInfoListResponse;
import com.bnbneeds.app.model.dealer.VendorListResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.bnbneeds.android.Activity.Activity.ConstantsActivity.pDialog;

/**
 * Created by Len on 21-02-2017.
 */

public class PurchaselistProductActivity extends BaseActivity {
    private String vendor;
    List<RowItemPlistVendor> rowItems;
    ArrayList<String> arrproductName = new ArrayList<>();
    ArrayList<String> productNameid = new ArrayList<>();
    ListView list;
    Map<String, String> queryParams = new HashMap<>();
    CustomplistVendorAdapter listvendor;
    String[] productNamestring;
    String[] productNameidstring;
String productNamelist;
    EditText inputSearch;
    protected  String productName;
    protected  String productid;
String tempname,vendorname,vendorid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productsearch);

        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if(conn == true) {
            new ProductNameList().execute();
        }else{
            Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();
        }


        inputSearch = (EditText)findViewById(R.id.inputSearchvendor);
        list = (ListView)findViewById(R.id.list_view);
        Bundle b = getIntent().getExtras();
       tempname = b.getString("TEMPLATE_NAME");
        vendorname = b.getString("selectedname");
        vendorid = b.getString("vendorid");
    }

    @Override
    public void onBackPressed() {
        Intent home=new Intent(this,HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }
    class ProductNameList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {

            queryParams.put("status","APPROVED");

            productNamelist = client.productNames().listBulkAsString(1000,queryParams);

            if (productNamelist != null) {

            try {
                JSONObject json = new JSONObject(productNamelist);
                JSONArray productarray = json.getJSONArray("entity");
                for(int i = 0; i<productarray.length(); i++){
                    JSONObject obj = productarray.getJSONObject(i);


                    arrproductName.add(obj.getString("name"));
                    productNameid.add(obj.getString("id"));
                    productNamestring = arrproductName.toArray(new String[arrproductName.size()]);
                    productNameidstring = productNameid.toArray(new String[productNameid.size()]);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            } else {
                Intent home = new Intent(PurchaselistProductActivity.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
                pDialog.dismiss();

            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
              pDialog.dismiss();
            if (productNamestring != null) {
            rowItems = new ArrayList<RowItemPlistVendor>();
            for (int i = 0; i < productNameidstring.length; i++) {
                RowItemPlistVendor item = new RowItemPlistVendor(productNamestring[i],productNameidstring[i]);
                rowItems.add(item);
            }

            listvendor = new CustomplistVendorAdapter(PurchaselistProductActivity.this,R.layout.plistvendor,rowItems);
            list.setAdapter(listvendor);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                     productName = listvendor.getItem(position).getVendor();
                    productid =listvendor.getItem(position).getVendorid();





                        Intent intent = new Intent(PurchaselistProductActivity.this, AddActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("productselectedname", productName);
                        bundle.putString("productNameid", productid);
                        bundle.putString("selectedname", vendorname);
                        bundle.putString("vendorid", vendorid);
                        bundle.putString("TEMPLATE_NAME", tempname);

                        intent.putExtras(bundle);
                        startActivity(intent);


                    // Toast.makeText(getApplicationContext(),"You selected "+vendornameid,Toast.LENGTH_LONG).show();
                }
            });
            } else {
                Toast.makeText(getApplicationContext(),"No data",Toast.LENGTH_LONG).show();
                Intent home = new Intent(PurchaselistProductActivity.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);


            }
            inputSearch = (EditText)findViewById(R.id.inputSearch);
            if (inputSearch != null) {
            inputSearch.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                    // When user changed the Text
                    PurchaselistProductActivity.this.listvendor.getFilter().filter(cs);
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                              int arg3) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                }
            });
            } else {
                Intent home = new Intent(PurchaselistProductActivity.this, HomeActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        }
    }
}