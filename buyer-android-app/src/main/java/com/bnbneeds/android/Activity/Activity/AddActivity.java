package com.bnbneeds.android.Activity.Activity;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.db.DbHelper;
import com.bnbneeds.app.model.EntityInfoResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.dealer.VendorListResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * activity to get input from user and insert into SQLite database
 */
public class AddActivity extends BaseActivity implements OnClickListener {
    protected static final String INVALID_DATA_TITLE = "Invalid Data";
    protected static final String INVALID_DATA_ERROR_MESSAGE = "Quantity Should not be Zero";
    protected String tempname, vendorname, vendorid, productName, productid;
    private Button btn_save;
    private DbHelper mHelper;
    private EditText EditQuantity, Remarks;
    private Spinner SpinnerUnits;
    private SQLiteDatabase dataBase;
    private String id;
    private double quantity;
    private String units;
    private String email,remarks;
    private boolean isUpdate;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_item_view);
        btn_save = (Button) findViewById(R.id.save_btn);
        EditQuantity = (EditText) findViewById(R.id.edit_Quantity);
        SpinnerUnits = (Spinner) findViewById(R.id.spinnerUnits);
        Remarks = (EditText) findViewById(R.id.subproduct);

        isUpdate = getIntent().getExtras().getBoolean("update");
        /*if (isUpdate) {
            id = getIntent().getExtras().getString("ID");


            productname = getIntent().getExtras().getString("PRODUCT_NAME");
            quantity = getIntent().getExtras().getDouble("QUANTITY");
            units = getIntent().getExtras().getString("UNITS");
            vendor = getIntent().getExtras().getString("VENDOR");
            EditQuantity.setText(String.valueOf(quantity));

        }*/

        btn_save.setOnClickListener(this);

        mHelper = new DbHelper(this);

    }

    // saveButton click event 
    public void onClick(View v) {

        quantity = Double.parseDouble(EditQuantity.getText().toString());
        units = SpinnerUnits.getSelectedItem().toString().trim();
        email = LoginActivity.RELATIONSHIP_ID;
        productName = getIntent().getExtras().getString("productselectedname");
        productid = getIntent().getExtras().getString("productNameid");
        vendorname = getIntent().getExtras().getString("selectedname");
        vendorid = getIntent().getExtras().getString("vendorid");
        if(!(Remarks == null)) {
            remarks = Remarks.getText().toString();
        }else{
            remarks = "Not Defined";
        }
        if (HomeActivity.templetname == null) {
            switch (tempname = getIntent().getExtras().getString("TEMPLATE_NAME")) {
                case "dummy":Log.d("Error",tempname);
                    break;
            }
        } else {
            tempname = HomeActivity.templetname;
        }

        if (productName.length() > 0 && quantity > 0 && units.length() > 0 && vendorname.length() > 0 && productid.length() > 0 && vendorid.length() > 0) {

            saveData();

            // btn_save.setBackgroundColor(Color.GREEN);
        } else {

            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(AddActivity.this);
            alertBuilder.setTitle(INVALID_DATA_TITLE);
            alertBuilder.setMessage(INVALID_DATA_ERROR_MESSAGE);
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();

                }
            });
            alertBuilder.create().show();
        }
    }

    /**
     * save data into SQLite
     */
    private void saveData() {

        dataBase = mHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DbHelper.KEY_TEMPLATENAME, tempname);
        values.put(DbHelper.KEY_PRODUCTNAME, productName);
        values.put(DbHelper.KEY_PRODUCTNAMEID, productid);
        values.put(DbHelper.KEY_QUANTITY, quantity);
        values.put(DbHelper.KEY_UNITS, units);
        values.put(DbHelper.KEY_VENDOR, vendorname);
        values.put(DbHelper.KEY_VENDORID, vendorid);
        values.put(DbHelper.KEY_EMAIL, email);
        values.put(DbHelper.KEY_REMARKS,remarks);
        System.out.println("");

        if (isUpdate) {
            //update database with new data
            dataBase.update(DbHelper.TABLE_NAME, values, DbHelper.KEY_ID + "=" + id, null);
        } else {

            dataBase.insert(DbHelper.TABLE_NAME, null, values);

        }
        //close database
        dataBase.close();
        finish();
        Intent disp = new Intent(this, DisplayTestActivity.class);
        disp.putExtra("TEMPLATE_NAME", tempname);


        startActivity(disp);


    }


    @Override
    public void onBackPressed() {
        Intent home = new Intent(this, HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }
}
