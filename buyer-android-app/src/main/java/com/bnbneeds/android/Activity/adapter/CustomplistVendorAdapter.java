package com.bnbneeds.android.Activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.bean.RowItemPlistVendor;
import com.bnbneeds.android.Activity.bean.RowItemVendor;

import java.util.List;

/**
 * Created by Len on 07-01-2017.
 */

public class CustomplistVendorAdapter extends ArrayAdapter<RowItemPlistVendor> {

    Context context;

    public CustomplistVendorAdapter(Context context, int resourceId, List<RowItemPlistVendor> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        // ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
        TextView vendor;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        CustomplistVendorAdapter.ViewHolder holder = null;
        RowItemPlistVendor rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.plistvendor, null);
            holder = new CustomplistVendorAdapter.ViewHolder();

            //  holder.imageView = (ImageView) convertView.findViewById(R.id.productImage);
            holder.vendor = (TextView) convertView.findViewById(R.id.vendor_name);
            convertView.setTag(holder);
        } else
            holder = (CustomplistVendorAdapter.ViewHolder) convertView.getTag();


        // holder.imageView.setImageResource(rowItem.getImageId());
        holder.vendor.setText(rowItem.getVendor());

        return convertView;
    }
}
