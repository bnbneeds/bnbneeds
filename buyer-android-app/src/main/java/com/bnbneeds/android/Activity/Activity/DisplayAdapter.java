package com.bnbneeds.android.Activity.Activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.bnbneeds.android.Activity.R;

import java.util.ArrayList;

public class DisplayAdapter extends BaseAdapter {
	private Context mContext;
	private ArrayList<String> id;
	//private ArrayList<String> templateName;
	private ArrayList<String> productName;
	private ArrayList<Double> quantity;
	private ArrayList<String> units;
	private ArrayList<String> vendor;
	private ArrayList<String> remarks;
	

	public DisplayAdapter(Context c, ArrayList<String> id, ArrayList<String> pname,  ArrayList<String> remarks, ArrayList<Double> qty, ArrayList<String> unts, ArrayList<String> ven) {
		this.mContext = c;

		this.id = id;
		//this.templateName=tName;
		this.productName = pname;
		this.quantity = qty;
		this.units = unts;
		this.vendor = ven;
		this.remarks = remarks;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return id.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int pos, View child, ViewGroup parent) {
		Holder mHolder;
		LayoutInflater layoutInflater;
		if (child == null) {
			layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			child = layoutInflater.inflate(R.layout.listcell, null);
			mHolder = new Holder();
			mHolder.textViewId = (TextView) child.findViewById(R.id.textViewId);
			mHolder.textViewPname = (TextView) child.findViewById(R.id.textViewPname);
			mHolder.textViewRemarks = (TextView) child.findViewById(R.id.textViewremarks);
			mHolder.textViewQty = (TextView) child.findViewById(R.id.textViewQty);
			mHolder.textViewUnits = (TextView) child.findViewById(R.id.textViewUnits);
			mHolder.textViewVendor = (TextView) child.findViewById(R.id.textViewVendors);

			child.setTag(mHolder);
		} else {
			mHolder = (Holder) child.getTag();
		}
		mHolder.textViewId.setText(id.get(pos));
	//	mHolder.textViewTname.setText(templateName.get(pos));
		mHolder.textViewPname.setText(productName.get(pos));
		mHolder.textViewRemarks.setText(remarks.get(pos));
		mHolder.textViewQty.setText(String.valueOf(quantity.get(pos)));
        mHolder.textViewUnits.setText(units.get(pos));
		mHolder.textViewVendor.setText(vendor.get(pos));

		return child;
	}

	public class Holder {
		TextView textViewId;
		//TextView textViewTname;
		TextView textViewPname;
		TextView textViewQty;
		TextView textViewUnits;
		TextView textViewVendor;
		TextView textViewRemarks;
	}

}
