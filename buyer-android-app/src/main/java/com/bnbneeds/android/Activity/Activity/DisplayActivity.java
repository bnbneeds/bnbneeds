package com.bnbneeds.android.Activity.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.db.DbHelper;
import com.bnbneeds.app.model.dealer.order.CreatePurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.CreatePurchaseListParam;
import com.bnbneeds.app.model.dealer.order.PurchaseItemParam;

import java.util.ArrayList;

import static com.bnbneeds.android.Activity.Activity.ConstantsActivity.pDialog;


/**
 * activity to display all records from SQLite database
 *
 */
public class DisplayActivity extends BaseActivity {

    private DbHelper mHelper;
    private SQLiteDatabase dataBase;

    private ArrayList<String> pId = new ArrayList<String>();
    private ArrayList<String> pName = new ArrayList<String>();
    private ArrayList<String> pNameId = new ArrayList<>();
    private ArrayList<Double> pQuantity = new ArrayList<Double>();
    private ArrayList<String> pUnits = new ArrayList<String>();
    private ArrayList<String> pVendor = new ArrayList<String>();
    private ArrayList<String> pVendorId = new ArrayList<String>();
    private ArrayList<String> pRemarks= new ArrayList<String>();
    private ArrayList<String> tempName = new ArrayList<String>();
    private TextView TextView_Template_Name;
    private ListView userList;
    private AlertDialog.Builder build;
    protected static String tempname;
    private String strproductid,strvendorid,strqty,strunits;
private Double quantity;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_purchase_items_view);

        Bundle b = getIntent().getExtras();
        tempname = b.getString("TEMPLATE_NAME");

        userList = (ListView) findViewById(R.id.List);
        TextView_Template_Name = (TextView) findViewById(R.id.tv_Template_name);
        mHelper = new DbHelper(this);
        TextView_Template_Name.setText("Template : " + tempname);
        //add new record
        findViewById(R.id.btnAdd).setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(),EditActivity.class);
                i.putExtra("TEMPLATE_NAME", tempname);
                i.putExtra("update", false);
                startActivity(i);

            }
        });

        //Create PurchaseList
        findViewById(R.id.purchaselist).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new PurchaselistTask().execute();

            }
        });

        //click to update data

        userList.setOnItemClickListener(new OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                Intent i = new Intent(DisplayActivity.this,
                        AddActivity.class);

                i.putExtra("TEMPLATE_NAME", tempname);
                i.putExtra("PRODUCT_NAME", pName.get(arg2));
                i.putExtra("QUANTITY", pQuantity.get(arg2));
                i.putExtra("UNITS", pUnits.get(0));
                i.putExtra("VENDOR", pVendor.get(arg2));
                i.putExtra("EMAIL_ID", pVendor.get(0));
                i.putExtra("ID", pId.get(arg2));
                i.putExtra("update", true);

                Log.d("values offffffff " ,tempname+pName.get(arg2)+pQuantity.get(arg2)+pUnits.get(0)+ pVendor.get(arg2)+pId.get(arg2));

                startActivity(i);


            }
        });


        //long click to delete data
        userList.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {

                build = new AlertDialog.Builder(DisplayActivity.this);
                build.setTitle("Delete " + pName.get(arg2) + " "
                        + pQuantity.get(arg2) + " " + pUnits.get(arg2) + " " + pVendor.get(arg2));
                build.setMessage("Do you want to delete ?");
                build.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                                int which) {

                                Toast.makeText(
                                        getApplicationContext(),
                                        pName.get(arg2) + " "
                                                + pQuantity.get(arg2) + " "
                                                + pUnits.get(arg2) + " "
                                                + pVendor.get(arg2)
                                                + " is deleted.", Toast.LENGTH_LONG).show();

                                dataBase.delete(
                                        DbHelper.TABLE_NAME,
                                        DbHelper.KEY_ID + "="
                                                + pId.get(arg2), null);
                                displayData();
                                dialog.cancel();
                            }
                        });


                build.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = build.create();
                alert.show();

                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        displayData();
        super.onResume();
    }


    /**
     * displays data from SQLite
     */
    private void displayData() {
        dataBase = mHelper.getWritableDatabase();
        String select = "SELECT * FROM ItemListTable WHERE TEMPLATE_NAME='" + tempname + "'";

        Cursor mCursor = dataBase.rawQuery(select, null);


        pId.clear();
        pName.clear();
        pQuantity.clear();
        pUnits.clear();
        pVendor.clear();
        pNameId.clear();
        pVendorId.clear();
        pRemarks.clear();
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        if (mCursor.moveToFirst()) {
            do {
                pId.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_ID)));
                pName.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_PRODUCTNAME)));
                pNameId.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_PRODUCTNAMEID)));
                pQuantity.add(mCursor.getDouble(mCursor.getColumnIndex(DbHelper.KEY_QUANTITY)));
                pUnits.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_UNITS)));
                pVendor.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_VENDOR)));
                pVendorId.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_VENDORID)));
                pRemarks.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_REMARKS)));


            } while (mCursor.moveToNext());
        }
        DisplayAdapter disadpt = new DisplayAdapter(DisplayActivity.this, pId, pName,pRemarks, pQuantity, pUnits, pVendor);
        userList.setAdapter(disadpt);
        mCursor.close();
    }
    @Override
    public void onBackPressed() {
        Intent home=new Intent(this,HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }

    class PurchaselistTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(DisplayActivity.this);
            pDialog.setMessage("Please Wait....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {


           CreatePurchaseItemParam param = new CreatePurchaseItemParam();

              for(int i=0;i<pQuantity.size();i++) {
                  strproductid = pNameId.get(i);
                  quantity = pQuantity.get(i);
                  strunits = pUnits.get(i);
                  strvendorid = pVendorId.get(i);
                  param.setRequiredQuantity(quantity);
                  param.setUnit(strunits);
                  param.setVendorId(strvendorid);
                  param.setProductNameId(strproductid);
                  CreatePurchaseListParam list = new CreatePurchaseListParam();
                  list.add(param);
                  client.buyers().createPurchaseList(LoginActivity.RELATIONSHIP_ID.trim(),list);
              }




             //   param.setRequiredQuantity(Double.parseDouble(strqty));






            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
             pDialog.dismiss();
            Toast.makeText(DisplayActivity.this, "Purchaselist has been Created Succesfully!!", Toast.LENGTH_SHORT).show();

        }
    }

}