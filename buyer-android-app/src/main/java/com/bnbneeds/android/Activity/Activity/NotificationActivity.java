package com.bnbneeds.android.Activity.Activity;

import android.app.Notification;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.adapter.CustomNotifiactionAdapter;
import com.bnbneeds.android.Activity.bean.RowItemNotification;
import com.bnbneeds.android.Activity.util.CheckConnectivity;
import com.bnbneeds.app.model.notification.NotificationInfoResponse;
import com.bnbneeds.app.model.notification.NotificationListResponse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.bnbneeds.android.Activity.Activity.ConstantsActivity.pDialog;

public class NotificationActivity extends BaseActivity {

    ListView notifylv;
    String[] notify, notifyven, notifypname;
    ArrayList<String> notifymsg = new ArrayList<>();
    ArrayList<String> notifyvendor = new ArrayList<>();
    ArrayList<String> notifyproduct = new ArrayList<>();
    List<RowItemNotification> notificationrowItem;
    CustomNotifiactionAdapter adapter;
String a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        notifylv = (ListView) findViewById(R.id.notify_list);

        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if(conn == true) {

            new NotificationTask().execute();
        }else{
            Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent home = new Intent(this, HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }

    class NotificationTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setDialog();
        }

        @Override
        protected String doInBackground(String... params) {



            NotificationListResponse response = client.notifications().list(20);
            if (response != null) {
                List<NotificationInfoResponse> inforesponse = response.getNotificationList();
                if (inforesponse != null && !inforesponse.isEmpty()) {
                for (Iterator<NotificationInfoResponse> iterator = inforesponse.iterator(); iterator.hasNext(); ) {
                    NotificationInfoResponse notifyInfo = iterator.next();

                    notifyproduct.add(notifyInfo.getEntityName());
                    notifyvendor.add(notifyInfo.getTriggeredBy());
                    notifymsg.add(notifyInfo.getNotificationText());

                    notify = notifymsg.toArray(new String[notifymsg.size()]);
                    notifyven = notifyvendor.toArray(new String[notifyvendor.size()]);
                    notifypname = notifyproduct.toArray(new String[notifyproduct.size()]);
                }
            } else {
                    Intent home = new Intent(NotificationActivity.this, HomeActivity.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(home);
                    pDialog.dismiss();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            if (notify != null) {
            notificationrowItem = new ArrayList<RowItemNotification>();
            for (int i = 0; i < notify.length; i++) {
                RowItemNotification item = new RowItemNotification(notify[i], notifyven[i], notifypname[i]);
                notificationrowItem.add(item);
            }


            adapter = new CustomNotifiactionAdapter(NotificationActivity.this, R.layout.listnotify, notificationrowItem);
            notifylv.setAdapter(adapter);
        } else {
            Toast.makeText(getApplicationContext(),"No data",Toast.LENGTH_LONG).show();
            Intent home = new Intent(NotificationActivity.this, HomeActivity.class);
            home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(home);


        }

        }
    }
}
