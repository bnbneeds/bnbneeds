package com.bnbneeds.android.Activity.Activity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.db.DbHelper;


public class UpdateActivity extends BaseActivity {

    String ID,tempname,delqty;

    private DbHelper mHelper;
    private SQLiteDatabase dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        // Read var from Intent
        Intent intent= getIntent();
        ID = intent.getStringExtra("id");
        tempname = intent.getStringExtra("TEMPLATE_NAME");


        TextView tMemberID = (TextView) findViewById(R.id.txtMemberID);
        EditText tTel = (EditText) findViewById(R.id.txtTel);

        tMemberID.setText(ID);
        tTel.setText(delqty);

        // Show Data
       // ShowData(ID);

        // btnSave (Save)
        final Button save = (Button) findViewById(R.id.btnSave);
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // If Save Complete
                if(UpdateData(ID))
                {
                    // Open Form ListUpdate
                    Intent newActivity = new Intent(UpdateActivity.this,DisplayTestActivity.class);
                    newActivity.putExtra("TEMPLATE_NAME",tempname);
                    startActivity(newActivity);
                }
            }
        });

        // btnCancel (Cancel)
        final Button cancel = (Button) findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Open Form ListUpdate
                Intent newActivity = new Intent(UpdateActivity.this,DisplayTestActivity.class);
                newActivity.putExtra("TEMPLATE_NAME",tempname);
                startActivity(newActivity);
            }
        });

    }



    public boolean UpdateData(String ID)
    {

        // txtName, txtTel

        final EditText tTel = (EditText) findViewById(R.id.txtTel);

        // Dialog
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        AlertDialog ad = adb.create();


        // Check Tel
        if(tTel.getText().length() == 0)
        {
            ad.setMessage("Please input [Tel] ");
            ad.show();
            tTel.requestFocus();
            return false;
        }

        // new Class DB
        final DbHelper myDb = new DbHelper(this);

        // Save Data
        long saveStatus = myDb.UpdateData(ID,tTel.getText().toString());
        if(saveStatus <=  0)
        {
            ad.setMessage("Error!! ");
            ad.show();
            return false;
        }

        Toast.makeText(UpdateActivity.this,"Update Data Successfully. ",
                Toast.LENGTH_SHORT).show();

        return true;
    }
}
