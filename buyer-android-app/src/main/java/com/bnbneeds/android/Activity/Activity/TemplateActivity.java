package com.bnbneeds.android.Activity.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.adapter.TemplateAdapter;
import com.bnbneeds.android.Activity.db.DbHelper;

import java.util.ArrayList;

public class TemplateActivity extends Activity {

    DbHelper mHelper = new DbHelper(this);
    ListView listView;
    private ArrayList<String> tempName = new ArrayList<String>();
    private SQLiteDatabase dataBase;
    protected static String templateName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.template_view);

        listView = (ListView) findViewById(R.id.list);
        displayData();
     }

   /* @Override
    protected void onResume() {
        displayData();
        super.onResume();
    }*/

    @Override
    public void onBackPressed() {
        Intent home=new Intent(this,HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }

    private void displayData() {
        dataBase = mHelper.getWritableDatabase();
        String select = "SELECT DISTINCT TEMPLATE_NAME FROM ItemListTable WHERE EMAIL_ID ='" + LoginActivity.RELATIONSHIP_ID + "'";

            Cursor mCursor = dataBase.rawQuery(select, null);


            if (mCursor.moveToFirst()) {
                do {
                    tempName.add(mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_TEMPLATENAME)));
                } while (mCursor.moveToNext());
            }
        if (tempName.isEmpty()) {
            Intent home=new Intent(this,HomeActivity.class);
            home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(home);
            Toast.makeText(getApplicationContext(), "No PurchaseLists are Created to Display", Toast.LENGTH_SHORT).show();
        } else {
            //listView.setAdapter(new TemplateAdapter(TemplateActivity.this,tempName));
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent in = new Intent(TemplateActivity.this, DisplayTestActivity.class);
                    templateName = tempName.get(position);
                    in.putExtra("TEMPLATE_NAME", templateName);
                    startActivity(in);
                }
            });
            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {

                    AlertDialog.Builder build = new AlertDialog.Builder(TemplateActivity.this);
                    build.setTitle("Delete " + tempName.get(arg2));
                    build.setMessage("Do you want to delete ?");
                    build.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dataBase.delete(DbHelper.TABLE_NAME, DbHelper.KEY_TEMPLATENAME + "= '" + tempName.get(arg2) + "'", null);
                            Toast.makeText(getApplicationContext(), tempName.get(arg2) + " is deleted.", Toast.LENGTH_LONG).show();
                            Intent home = new Intent(TemplateActivity.this, HomeActivity.class);
                            home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(home);
                            dialog.cancel();

                        }
                    });
                    build.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = build.create();
                    alert.show();
                    return true;
                }
            });
            ArrayAdapter<String> adpt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tempName);
            listView.setAdapter(adpt);

            mCursor.close();
        }
    }

}
