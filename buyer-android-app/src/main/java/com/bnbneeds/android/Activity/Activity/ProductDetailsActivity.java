package com.bnbneeds.android.Activity.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.config.Config;
import com.bnbneeds.android.Activity.util.CheckConnectivity;
import com.bnbneeds.app.model.product.ProductInfoResponse;
import com.getbase.floatingactionbutton.FloatingActionButton;


import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductDetailsActivity extends BaseActivity {
    protected static String Productid;
    Map<String, String> map = new HashMap<>();
    ListView lvkeys, lvvalues;
    String uri;
    URI url;
    ImageView proimage;
    byte[] image;
    Bitmap bmp;
    Button okay;
    FloatingActionButton video, images, video_tut;
    protected static String youtube = "M-VQTP64Azk";
    protected static String youtubeTut = "M-VQTP64Azk";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        video = (FloatingActionButton) findViewById(R.id.fab_video);
        images = (FloatingActionButton) findViewById(R.id.fab_image);
        video_tut = (FloatingActionButton) findViewById(R.id.fab_video_tut);


        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if(conn == true) {
            new ProductDetailsTask().execute();
        }else{
            Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();
        }


        Intent in = getIntent();
        Bundle b = in.getExtras();
       // Productid = b.getString("Productid");
        proimage = (ImageView)findViewById(R.id.imageView);
        lvkeys = (ListView) findViewById(R.id.prokeys);
        lvvalues = (ListView) findViewById(R.id.provalues);
        //okay = (Button)findViewById(R.id.ok);

        images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent images = new Intent(ProductDetailsActivity.this, ImageActivity.class);
                /*Bundle pid = new Bundle();
               pid.putString("Productid",Productid);
                images.putExtras(pid);*/
                startActivity(images);
            }
        });

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent videos = new Intent(ProductDetailsActivity.this, VideoActivity.class);
               /* Bundle b = new Bundle();
                b.putString("youtube", youtube);
                b.putString("youtubeTut", youtubeTut);
                videos.putExtras(b);*/

                startActivity(videos);


            }
        });

        video_tut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent videos = new Intent(ProductDetailsActivity.this, VideoTutActivity.class);
               /* Bundle b = new Bundle();
                b.putString("youtubeTut", youtubeTut);
                videos.putExtras(b);*/
                startActivity(videos);


            }
        });

        }

    @Override
    public void onBackPressed() {
        youtube = "M-VQTP64Azk";
        youtubeTut = "M-VQTP64Azk";
        Intent home=new Intent(this,EnquiryActivity.class);
/*        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);*/

        startActivity(home);
        return;
    }





    class ProductDetailsTask extends AsyncTask<String, Void, String> {
        ProductInfoResponse productInfo;
      // ProductImageListResponse resp;


    @Override
    protected String doInBackground(String... params) {

        productInfo = client.products(ListviewTestActivity.ids).get();
        String url = productInfo.getVideoURL();
        String tutUrl = productInfo.getTutorialVideoURL();
        if(!(url == null) && !(tutUrl == null)) {
            youtube = url.substring(url.indexOf('=') + 1);
            youtubeTut = tutUrl.substring(tutUrl.indexOf('=') + 1);
        }else if(!(url == null) && tutUrl == null){
            youtube = url.substring(url.indexOf('=') + 1);
        }
        else if(url == null && !(tutUrl == null)){
            youtubeTut = tutUrl.substring(tutUrl.indexOf('=') + 1);
        }

        map = productInfo.getAttributes();
    //URI url =productInfo.getImages().getLinkRef();
        /*ProductImageListResponse imageListResponse = productInfo.getImageList();

        for (BlobResourceRep imageLink : imageListResponse.getImageLinks()) {
             image = client.products(Productid).getImage(imageLink.getId(), imageLink.getMediaType());




        }*/


        //resp = client.products().getImageLinks(Productid);


          // uri = resp.getImageLinks().toString();



        return null;
    }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            /*bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
           proimage.setImageBitmap(bmp);*/



            if(map == null){
            }
            else{
                ArrayList<String> arrlistkeys = new ArrayList<String>(map.keySet());
                String[] pro = arrlistkeys.toArray(new String[arrlistkeys.size()]);
                ArrayAdapter<String> adapterkey = new ArrayAdapter<String>(ProductDetailsActivity.this,android.R.layout.simple_list_item_1, android.R.id.text1,pro);
                lvkeys.setAdapter(adapterkey);

               ArrayList<String> arrlistvalues = new ArrayList<String>(map.values());
                String[] productvalue = arrlistvalues.toArray(new String[arrlistvalues.size()]);
                ArrayAdapter<String> adaptervalues = new ArrayAdapter<String>(ProductDetailsActivity.this,android.R.layout.simple_list_item_1,android.R.id.text1,productvalue);
                lvvalues.setAdapter(adaptervalues);
            }
        }
    }
    }
