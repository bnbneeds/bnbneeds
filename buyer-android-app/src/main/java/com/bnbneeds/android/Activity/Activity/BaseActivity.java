package com.bnbneeds.android.Activity.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;


import com.bnbneeds.android.Activity.client.BNBNeedsClientSingleton;
import com.bnbneeds.android.Activity.util.CheckConnectivity;
import com.bnbneeds.rest.client.BNBNeedsClient;


import java.util.Timer;
import java.util.TimerTask;

import static com.bnbneeds.android.Activity.Activity.ConstantsActivity.pDialog;

/**
 * Created by Admin on 10/17/2015.
 */
public class BaseActivity extends Activity {
    Context context;

    protected static Timer timer;
    protected BNBNeedsClient client;

    public BaseActivity() {
        client = BNBNeedsClientSingleton.getInstance();
    }


    protected void checkInternetConenction() {
        CheckConnectivity check = new CheckConnectivity();
        Boolean conn = check.checkNow(this.getApplicationContext());
        if (conn == true) {
            //run your normal code path here
        } else {
            //Send a warning message to the user
            connectivityMessage("Check Network Connection.");
        }
    }

    public void connectivityMessage(String msg) {
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, "", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setText(msg);
        toast.show();
    }

    public void connectivityredirect() {
        Toast.makeText(this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();
        Intent home = new Intent(this, LoginActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(home);

    }

    public void setDialog() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait....");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();

        timer = new Timer();
        Log.i("Main", "Invoking logout timer");
        LogOutTimerTask logoutTimeTask = new LogOutTimerTask();
        timer.schedule(logoutTimeTask, 3300000); //auto logout in 55 minutes
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (timer != null) {
            timer.cancel();
            Log.i("Main", "cancel timer");
            timer = null;
        }
    }

    class LogOutTimerTask extends TimerTask {


        @Override
        public void run() {
             client.logout();
           //redirect user to login screen
            Intent i = new Intent(BaseActivity.this, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
                return;
        }
    }


}
