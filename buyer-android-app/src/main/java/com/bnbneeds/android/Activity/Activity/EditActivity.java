package com.bnbneeds.android.Activity.Activity;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.bnbneeds.android.Activity.R;
import com.bnbneeds.android.Activity.db.DbHelper;
import com.bnbneeds.app.model.EntityInfoResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.dealer.VendorListResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class EditActivity extends ConstantsActivity implements OnClickListener {
    private Button btn_save;
    private DbHelper mHelper;
    private Spinner SpinnerProductname;
    private EditText EditQuantity;
    private Spinner SpinnerUnits;
    private Spinner SpinnerVendor;
    private TextView product;
    private TextView vendorname;
    private SQLiteDatabase dataBase;
    private String id;
    protected String tempname;
    protected String temp = "YES";
    private String productname;
    private double quantity;
    private String units;
    private String vendor;
    private String email;
    private boolean isUpdate;
    String a,productName,productid,pid,Selected_ProductName,Selected_Vendor_Name,vid;
    ArrayList<String> arr = new ArrayList<String>();
    ArrayList<String> arrvendor = new ArrayList<String>();
    ArrayList<EntityInfoResponse> productnameid = new ArrayList<EntityInfoResponse>();
    ArrayList<String> vendornameid = new ArrayList<>();
    String listvendor,listproduct;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_item_view);
        /*Bundle b=getIntent().getExtras();
        tempname=b.getString("TEMPLATE_NAME");
        listvendor = PurchaselistVendorActivity.vendorname;
        vid = PurchaselistVendorActivity.vendorid;
        pid =PurchaselistProductActivity.productid;
        listproduct = PurchaselistProductActivity.productName;

        btn_save = (Button) findViewById(R.id.save_btn);
        SpinnerProductname = (Spinner) findViewById(R.id.spinnerProducts);
        EditQuantity = (EditText) findViewById(R.id.edit_Quantity);
        SpinnerUnits = (Spinner) findViewById(R.id.spinnerUnits);
        SpinnerVendor = (Spinner) findViewById(R.id.spinnerVendor);
        product = (TextView)findViewById(R.id.textViewProductName);
        vendorname = (TextView)findViewById(R.id.textViewVendor);*/
        product.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent product = new Intent(EditActivity.this,PurchaselistProductActivity.class);

                product.putExtra("STATUS",temp);

                startActivity(product);
            }
        });
        vendorname.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EditActivity.this,PurchaselistVendorActivity.class);
                i.putExtra("STATUS",temp);
                startActivity(i);
            }
        });
        if(!(listvendor ==null)){
            vendorname.setText(listvendor);
        }
        if(!(listproduct ==null)){
            product.setText(listproduct);
        }

        isUpdate = getIntent().getExtras().getBoolean("update");
        if (isUpdate) {
            id = getIntent().getExtras().getString("ID");


            productname = getIntent().getExtras().getString("PRODUCT_NAME");
            quantity = getIntent().getExtras().getDouble("QUANTITY");
            units = getIntent().getExtras().getString("UNITS");
            vendor = getIntent().getExtras().getString("VENDOR");


            EditQuantity.setText(String.valueOf(quantity));
          //  Toast.makeText(this,"Tempname update in editactivity 1st time: " +tempname+ productname + quantity + units + vendor + email, Toast.LENGTH_LONG).show();
        }

        btn_save.setOnClickListener(this);

        mHelper = new DbHelper(this);

    }

    // saveButton click event 
    public void onClick(View v) {

       // Toast.makeText(this,"Temp 2nd time edit:" + temp,Toast.LENGTH_LONG).show();
        // temp=getIntent().getExtras().getString("TEMPLATE");

        productname = product.getText().toString();
        quantity = Double.parseDouble(EditQuantity.getText().toString());
        units = SpinnerUnits.getSelectedItem().toString().trim();
        tempname = DisplayActivity.tempname;
        vendor = vendorname.getText().toString();
        email = LoginActivity.RELATIONSHIP_ID;

        if (productname.length() > 0 && quantity > 0 && units.length() > 0 && vendor.length() > 0 && pid.length() > 0 && vid.length() >0 ) {
           // btn_save.setBackgroundColor(Color.GREEN);
            saveData();
        } else {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(EditActivity.this);
            alertBuilder.setTitle(INVALID_DATA_TITLE);
            alertBuilder.setMessage(INVALID_DATA_ERROR_MESSAGE);
            alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();

                }
            });
            alertBuilder.create().show();
        }
    }

    /**
     * save data into SQLite
     */
    private void saveData() {

        dataBase = mHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DbHelper.KEY_TEMPLATENAME,tempname);
        values.put(DbHelper.KEY_PRODUCTNAME, productname);
        values.put(DbHelper.KEY_PRODUCTNAMEID, pid);
        values.put(DbHelper.KEY_QUANTITY, quantity);
        values.put(DbHelper.KEY_UNITS, units);
        values.put(DbHelper.KEY_VENDOR, vendor);
        values.put(DbHelper.KEY_VENDORID, vid);
        values.put(DbHelper.KEY_EMAIL, email);
        System.out.println("");
        if (isUpdate) {
            //update database with new data
            dataBase.update(DbHelper.TABLE_NAME, values, DbHelper.KEY_ID + "=" + id, null);
        } else {
            //insert data into database
            // dataBase.delete(DbHelper.TABLE_NAME,null,null);
            dataBase.insert(DbHelper.TABLE_NAME, null, values);

           /* Toast.makeText(this, "Input Name to dialog: " + tempname + productname + quantity + units + vendor + email,
                    Toast.LENGTH_SHORT).show();*/
        }
        //close database
        dataBase.close();
        finish();
        Intent disp=new Intent(this,DisplayActivity.class);
        disp.putExtra("TEMPLATE_NAME", tempname);

        startActivity(disp);
        startActivity(disp);

    }

    @Override
    public void onBackPressed() {
        Intent home=new Intent(this,HomeActivity.class);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(home);
        return;
    }
    class ProductTask extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {
            VendorListResponse response = client.vendors().list(1000);
            List<NamedRelatedResourceRep> vendorlist = response.getVendorList();

            for (Iterator<NamedRelatedResourceRep> iterator = vendorlist.iterator(); iterator.hasNext();) {
                NamedRelatedResourceRep vendorinfo = iterator.next();
                vendor =  vendorinfo.getName();
                arrvendor.add(vendor);


                vendornameid.add(vendorinfo.getId());

            }

            a = client.productNames().listBulkAsString(1000);


            try {
                JSONObject json = new JSONObject(a);
                JSONArray productarray = json.getJSONArray("entity");
                for(int i = 0; i<productarray.length(); i++){
                    JSONObject obj = productarray.getJSONObject(i);

                    //To get ProductnameId
                    EntityInfoResponse ent = new EntityInfoResponse();
                    ent.setId(obj.getString("id"));
                    productnameid.add(ent);

                    productName = obj.getString("name");
                    productid = obj.getString("id");
                    arr.add(productName);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("Productnames ",a);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ArrayAdapter<String> adp = new ArrayAdapter<String>(EditActivity.this,android.R.layout.simple_list_item_1,arr);
            SpinnerProductname.setAdapter(adp);
            SpinnerProductname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    Selected_ProductName = parent.getSelectedItem().toString();
                    pid = productnameid.get(position).getId();



                    try {
                        JSONObject c = new JSONObject(new String(Selected_ProductName));
                        pid = c.getString("id");

                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                    Toast.makeText(getApplicationContext(),"Ur id is "+pid, Toast.LENGTH_LONG).show();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            ArrayAdapter<String> adpvendor = new ArrayAdapter<String>(EditActivity.this,android.R.layout.simple_list_item_1,arrvendor);
            SpinnerVendor.setAdapter(adpvendor);
            SpinnerVendor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Selected_Vendor_Name = parent.getItemAtPosition(position).toString();
                    vid =  vendornameid.get(position);

                    try {
                        JSONObject c = new JSONObject(new String(Selected_Vendor_Name));
                        vid = c.getString("id");

                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                    Toast.makeText(getApplicationContext(),"Ur id is "+vid, Toast.LENGTH_LONG).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }
    }

}
