package com.bnbneeds.android.Activity;

/**
 * Created by Len on 26-04-2017.
 */

public class Constants {
    public static final String URL = "https://bnbneeds-test-apisvc.appspot.com";
    public static  final String ALL_DELEARS = "dealers@test.bnbneeds.com";
    public static  final String BUYERS = "buyers@test.bnbneeds.com";
    public static final String apikey = "key-a5c093a84d845f3662d065d8e7b07595";
    public static final String domain = "test.bnbneeds.com";
}
