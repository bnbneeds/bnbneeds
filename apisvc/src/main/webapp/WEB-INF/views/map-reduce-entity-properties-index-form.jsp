<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Index Entity Properties</title>
</head>
<body>
	<h2>Map Reduce Job To Index Entity Properties</h2>
	<c:if test="${fn:length(pipelineURL) gt 0}">
		<p>Job has been successfully submitted. <a href="${pipelineURL}" target="_blank">Click to check its status</a></p>
	</c:if>
	
	<c:if test="${fn:length(errorMessage) gt 0}">
		<p style="color: red;">ERROR: ${errorMessage}</p>
	</c:if>
	
	<form action="/api/map-reduce-jobs/index-entity-properties" method="post">
		<table>
			<tbody>
				<tr>
					<td><label for="entityTypes">Entity Types (csv)</label></td>
					<td><input type="text" id="entityTypes" name="entityTypes" required="required"
						 autofocus="autofocus" size="10"></td>
				</tr>
				<tr>
					<td><label for="properties">Properties to be indexed
							(csv)</label></td>
					<td><input type="text" id="properties" name="properties" required="required"
						 size="10" ></td>
				</tr>
			</tbody>
		</table>
		<button type="submit">Submit Job</button>
	</form>
	<br/>
	<a href="/api/map-reduce-jobs">Return to job list</a>
</body>
</html>