<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>BNBneeds Map Reduce Jobs</title>
</head>
<body>

	<h2>Map Reduce Job List</h2>
	
	<c:if test="${fn:length(pipelineURL) gt 0}">
		<p>Job has been successfully submitted. <a href="${pipelineURL}" target="_blank">Click to check its status</a></p>
	</c:if>

	<ol>
		<c:forEach var="job" items="${mapReduceJobMap}">
	    <li>
			<a href="${job.value}">${job.key}</a>
		</li>
		</c:forEach>
	</ol>
	
</body>
</html>