package com.bnbneeds.app.util;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class JAXB2XmlJsonUtils {

	public static String pojo2Xml(Object object) throws JAXBException {

		JAXBContext context = JAXBContext.newInstance(object.getClass());

		Marshaller marshaller = context.createMarshaller();

		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		StringWriter writer = new StringWriter();

		marshaller.marshal(object, writer);

		String xmlStringData = writer.toString();

		return xmlStringData;

	}

	public static String pojo2Json(Object object)
			throws JsonProcessingException {
		final Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
		builder.indentOutput(true);
		ObjectMapper mapper = builder.build();
		return mapper.writeValueAsString(object);

	}

}
