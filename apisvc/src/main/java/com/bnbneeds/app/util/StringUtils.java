package com.bnbneeds.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils extends org.springframework.util.StringUtils {

	// private static final String PASSWORD_PATTERN =
	// "^.*(?=.{8,})(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$";
	// Alphanumeric word of at least 6 characters.
	private static final String PASSWORD_REGEX = "^.*(?=.{6,})(?=.*\\d)(?=.*[A-Za-z]).*$";
	private static final String CSV_STRING_REGEX = "(^$)|(^([-\\w]+)(,\\s*[-\\w]+)*$)";
	private static final Pattern VALID_EMAIL_ADDRESS_PATTERN = Pattern
			.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	private static final Pattern TYPE_PATTERN = Pattern.compile("urn\\:bnb\\:([^\\:]+)");

	public static String getTypeName(String id) {
		Matcher m = TYPE_PATTERN.matcher(id);
		if (m.find()) {
			return m.group(1);
		}
		return null;
	}

	public static boolean isEmpty(String text) {
		return (text == null || text.trim().isEmpty());
	}

	public static boolean isEmpty(String[] text) {
		return (text == null || text.length == 0
				|| ((text.length == 1) && (text[0] == null || text[0].trim().isEmpty())));
	}

	public static boolean isNumber(String text) {

		if (text != null) {
			if (text.matches("[0-9]+([.]{1}[0-9]+)?")) {
				return true;
			}
		}
		return false;

	}

	public static boolean isPhoneNumberValid(String phoneNo) {
		// validate phone numbers of format "1234567890"
		if (phoneNo.matches("\\d{10}"))
			return true;
		// validating phone number with -, . or spaces
		else if (phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}"))
			return true;
		// validating phone number where area code is in braces ()
		else if (phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}"))
			return true;
		// return false if nothing matches the input
		else
			return false;

	}

	public static boolean isPasswordValid(String password) {
		return password.matches(PASSWORD_REGEX);
	}

	public static boolean isCSVStringValid(String text) {
		return text.matches(CSV_STRING_REGEX);
	}

	public static boolean isEmailAddressValid(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_PATTERN.matcher(emailStr);
		return matcher.find();
	}

	public static boolean isDateFormatValid(String text, String pattern) {

		if (StringUtils.isEmpty(text)) {
			return false;
		}
		boolean valid = false;
		SimpleDateFormat f = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			date = f.parse(text);
		} catch (ParseException e) {
			return false;
		}
		if (date != null) {
			valid = f.format(date).equals(text);
		}

		return valid;
	}

	public static boolean isDateFormatValid(String text, SimpleDateFormat dateFormat) {

		if (StringUtils.isEmpty(text)) {
			return false;
		}
		boolean valid = false;
		Date date = null;
		try {
			date = dateFormat.parse(text);
		} catch (ParseException e) {
			return false;
		}
		if (date != null) {
			valid = dateFormat.format(date).equals(text);
		}

		return valid;
	}

	public static String getFormattedDate(Date date, SimpleDateFormat dateFormat) {
		return dateFormat.format(date);
	}

	public static String getFormattedDate(long date, SimpleDateFormat dateFormat) {
		Date dateObject = new Date(date);
		return getFormattedDate(dateObject, dateFormat);
	}

	public static String getFormattedDate(Date date, String pattern) {
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		return getFormattedDate(date, df);
	}

	public static String getFormattedDate(long date, String pattern) {
		Date dateObject = new Date(date);
		return getFormattedDate(dateObject, pattern);
	}

	public static String generateConfirmationCode() {
		String text = UUID.randomUUID().toString();
		return text.substring(0, text.indexOf("-"));
	}

	public static String getTokensByLength(String word, int tokenLength) {
		return word.length() < tokenLength ? word : word.substring(0, tokenLength);
	}

	public static String[] getWordTokens(String line, int minTokenLength) {
		String[] words = StringUtils.tokenizeToStringArray(line, " ");
		List<String> tokens = new ArrayList<>();
		for (String word : words) {
			for (int i = minTokenLength; i < word.length(); i++) {
				tokens.add(getTokensByLength(word, i));
			}
		}
		return StringUtils.toStringArray(tokens);
	}

	public static String getWordTokensAsString(String line, int minTokenLength) {
		String[] tokens = getWordTokens(line, minTokenLength);
		return StringUtils.arrayToDelimitedString(tokens, ",");
	}

}
