package com.bnbneeds.app.util;

import java.net.URI;
import java.net.URISyntaxException;

public class URIUtils {

	public static URI appendQuery(String uri, String query) throws URISyntaxException {
		URI oldUri = new URI(uri);

		String newQuery = oldUri.getQuery();
		if (newQuery == null) {
			newQuery = query;
		} else {
			newQuery += "&" + query;
		}
		URI newUri = new URI(oldUri.getScheme(), oldUri.getAuthority(), oldUri.getPath(), newQuery,
				oldUri.getFragment());
		return newUri;
	}

	public static URI appendQuery(URI uri, String query) throws URISyntaxException {
		return appendQuery(uri.toString(), query);
	}

}
