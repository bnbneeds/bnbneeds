package com.bnbneeds.app.api.service.response;

import java.net.URI;
import java.text.SimpleDateFormat;

import com.bnbneeds.app.db.client.model.TenderProduct;
import com.bnbneeds.app.db.client.model.BusinessType;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.ProductName;
import com.bnbneeds.app.db.client.model.Tender;
import com.bnbneeds.app.db.client.model.TenderBid;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.dealer.order.Quantity;
import com.bnbneeds.app.model.dealer.tender.TenderProductInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderPreviewResponse;
import com.bnbneeds.app.util.StringUtils;

public class TenderResponseMapper extends DataObjectResponseMapper {

	protected static final SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss z");

	public static TenderInfoResponse map(Tender from, URI link) {
		TenderInfoResponse to = new TenderInfoResponse();
		map(from, to, link);
		return to;
	}

	public static void map(Tender from, TenderInfoResponse to, URI link) {
		mapDescribedDataObject(from, to);
		to.setBidValueType(from.getBidValueType());
		to.setCreateTimestamp(from.getCreateTimestamp());
		to.setCreditTerms(from.getCreditTerms());
		to.setDeliveryFrequency(from.getDeliveryFrequency());
		to.setDeliveryTerms(from.getDeliveryTerms());
		to.setExpectedPrice(from.getExpectedPrice());
		to.setPaymentMode(from.getPaymentMode());
		to.setStatus(from.getStatus());
		Buyer buyer = from.getBuyer();
		to.setBuyer(buyer.getId(), buyer.getName());
		to.setDeliveryLocation(from.getDeliveryLocation());
		to.setCloseTimestamp(StringUtils.getFormattedDate(from.getCloseTimestamp(), TIMESTAMP_FORMAT));
		to.setOpenTimestamp(StringUtils.getFormattedDate(from.getOpenTimestamp(), TIMESTAMP_FORMAT));
		to.setLink(link);
	}

	public static TenderPreviewResponse mapPreview(Tender from) {
		TenderPreviewResponse to = new TenderPreviewResponse();
		mapPreview(from, to);
		return to;
	}

	public static void mapPreview(Tender from, TenderPreviewResponse to) {
		mapDescribedDataObject(from, to);
		to.setBidValueType(from.getBidValueType());
		to.setCreateTimestamp(from.getCreateTimestamp());
		to.setCreditTerms(from.getCreditTerms());
		to.setDeliveryFrequency(from.getDeliveryFrequency());
		to.setDeliveryTerms(from.getDeliveryTerms());
		to.setExpectedPrice(from.getExpectedPrice());
		to.setPaymentMode(from.getPaymentMode());
		BusinessType businessType = from.getBuyer().getBusinessType();
		to.setBusinessType(businessType.getId(), businessType.getName());
		to.setDeliveryLocation(from.getDeliveryLocation());
	}

	public static void map(TenderProduct from, TenderProductInfoResponse to, URI link) {
		mapDataObject(from, to);
		ProductName product = from.getProduct();
		to.setProductName(product.getId(), product.getName());
		Tender tender = from.getTender();
		to.setTender(tender.getId(), tender.getName(), link);
		to.setQualitySpecification(from.getQualitySpecification());
		to.setQuantity(new Quantity(from.getQuantity(), from.getUnit()));
		to.setSpecialRequirements(from.getSpecialRequirements());
	}

	public static TenderProductInfoResponse map(TenderProduct from, URI link) {
		TenderProductInfoResponse to = new TenderProductInfoResponse();
		map(from, to, link);
		return to;
	}

	public static void map(TenderBid from, TenderBidInfoResponse to) {
		mapDataObject(from, to);
		to.setValue(from.getValue());
		String comments = from.getComments();
		if (StringUtils.hasText(comments)) {
			to.setComments(comments);
		}
		Vendor vendor = from.getVendor();
		to.setVendor(vendor.getId(), vendor.getName());
		Tender tender = from.getTender();
		to.setTender(tender.getId(), tender.getName());
	}

	public static TenderBidInfoResponse map(TenderBid from) {
		TenderBidInfoResponse to = new TenderBidInfoResponse();
		map(from, to);
		return to;
	}

}
