package com.bnbneeds.app.api.service.mapreduce;

import com.bnbneeds.app.api.service.pipeline.PipelineUtils;
import com.bnbneeds.app.api.service.pipeline.vendor.PutVendorIntoDocumentIndexJob;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.tools.mapreduce.DatastoreMutationPool;
import com.google.appengine.tools.mapreduce.MapOnlyMapper;

public class IndexVendorPropertiesForDoumentSearchMapper extends MapOnlyMapper<Entity, Void> {

	private static final long serialVersionUID = 657105403454678131L;

	private transient DatastoreMutationPool batcher;

	@Override
	public void beginSlice() {
		batcher = DatastoreMutationPool.create();
	}

	@Override
	public void endSlice() {
		batcher.flush();
	}

	@Override
	public void map(Entity value) {
		String vendorId = value.getKey().getName();
		PipelineUtils.startPipelineService(new PutVendorIntoDocumentIndexJob(vendorId));
	}
}
