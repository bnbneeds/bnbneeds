package com.bnbneeds.app.api.service.response;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.OrderStatus;
import com.bnbneeds.app.db.client.model.ProductName;
import com.bnbneeds.app.db.client.model.PurchaseItem;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.dealer.order.PurchaseItemInfoResponse;
import com.bnbneeds.app.model.dealer.order.Quantity;
import com.bnbneeds.app.util.StringUtils;

public abstract class PurchaseItemResponseMapper extends
		DataObjectResponseMapper {

	public static PurchaseItemInfoResponse map(PurchaseItem item,
			SimpleDateFormat dateFormat) {

		PurchaseItemInfoResponse infoResponse = new PurchaseItemInfoResponse();
		if (item != null) {

			mapDescribedDataObject(item, infoResponse);

			Buyer buyer = item.getBuyer();
			Vendor vendor = item.getVendor();
			ProductName productName = item.getProductName();
			infoResponse.setVendor(vendor.getId(), vendor.getName());
			infoResponse.setBuyer(buyer.getId(), buyer.getName());
			infoResponse.setProductName(productName.getId(),
					productName.getName());
			Date date = new Date(item.getRequestTimestamp());
			infoResponse.setRequestDate(dateFormat.format(date));
			Quantity quantity = mapQuantity(item);
			infoResponse.setQuantity(quantity);
			String tags = item.getTags();
			if (StringUtils.hasText(tags)) {
				infoResponse.setTags(tags);
			}
			List<PurchaseItemInfoResponse.PurchaseStatus> statusUpdates = null;
			List<OrderStatus> statusList = item.getStatusUpdates();
			if (statusList != null && !statusList.isEmpty()) {
				statusUpdates = new ArrayList<PurchaseItemInfoResponse.PurchaseStatus>();

				for (Iterator<OrderStatus> iterator = statusList.iterator(); iterator
						.hasNext();) {
					OrderStatus orderStatus = iterator.next();
					PurchaseItemInfoResponse.PurchaseStatus statusUpdate = new PurchaseItemInfoResponse.PurchaseStatus();
					statusUpdate.setState(orderStatus.getState());
					statusUpdate.setComments(orderStatus.getComments());
					statusUpdate.setTimestamp(orderStatus.getTimestamp());
					statusUpdate.setUpdatedBy(orderStatus.getUpdatedBy());
					statusUpdates.add(statusUpdate);
				}
			}

			infoResponse.setStatusUpdates(statusUpdates);
		}

		return infoResponse;
	}

	public static Quantity mapQuantity(PurchaseItem purchaseItem) {
		return new Quantity(purchaseItem.getRequiredQuantity(),
				purchaseItem.getDeliveredQuantity(), purchaseItem.getUnit()
						.name());
	}

}
