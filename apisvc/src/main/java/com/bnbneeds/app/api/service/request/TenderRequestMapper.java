package com.bnbneeds.app.api.service.request;

import java.text.SimpleDateFormat;

import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.model.TenderProduct;
import com.bnbneeds.app.db.client.model.Tender;
import com.bnbneeds.app.db.client.model.Tender.Status;
import com.bnbneeds.app.model.dealer.tender.TenderProductParam;
import com.bnbneeds.app.model.dealer.tender.CreateTenderParam;
import com.bnbneeds.app.model.dealer.tender.TenderParam;
import com.bnbneeds.app.util.DateUtils;

public class TenderRequestMapper {

	public static void map(TenderParam from, Tender to, SimpleDateFormat closeTimestampFormat) {

		to.setName(from.getName());
		to.setDescription(from.getDescription());
		to.setBidValueType(from.getBidValueType());
		to.setCreditTerms(from.getCreditTerms());
		to.setDeliveryFrequency(from.getDeliveryFrequency());
		to.setDeliveryTerms(from.getDeliveryTerms());
		to.setExpectedPrice(from.getExpectedPrice());
		to.setPaymentMode(from.getPaymentMode());
		to.setDeliveryLocation(from.getDeliveryLocation());
		long closeTimestamp = DateUtils.getTimestamp(closeTimestampFormat, from.getCloseDate());
		closeTimestamp = DateUtils.getEndOfDayTimestamp(closeTimestamp);
		to.setCloseTimestamp(closeTimestamp);
	}

	public static Tender createAndMapCreateTenderRequest(CreateTenderParam from,
			SimpleDateFormat closeTimestampFormat) {
		Tender to = new Tender();
		String uid = UUIDUtil.createId(Tender.class);
		to.setId(uid);
		map(from, to, closeTimestampFormat);
		// status is set to CREATED
		to.setStatus(Status.CREATED);
		return to;
	}

	public static void map(TenderProductParam from, TenderProduct to) {
		to.setQualitySpecification(from.getQualitySpecification());
		to.setQuantity(from.getQuantity());
		to.setSpecialRequirements(from.getSpecialRequirements());
		to.setUnit(from.getUnit());
	}

	public static TenderProduct createAndMapNewBidProduct(TenderProductParam from) {
		TenderProduct bidProductEntity = new TenderProduct();
		String uid = UUIDUtil.createId(TenderProduct.class);
		bidProductEntity.setId(uid);
		map(from, bidProductEntity);
		return bidProductEntity;
	}

}
