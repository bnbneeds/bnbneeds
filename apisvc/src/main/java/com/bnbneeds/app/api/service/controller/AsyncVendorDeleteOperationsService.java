package com.bnbneeds.app.api.service.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.AsyncTaskUtility;
import com.bnbneeds.app.api.service.util.BlobStoreUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.BuyerReview;
import com.bnbneeds.app.db.client.model.Enquiry;
import com.bnbneeds.app.db.client.model.PaymentTransactionLinkedEntity;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.ProductAverageRating;
import com.bnbneeds.app.db.client.model.ProductOffer;
import com.bnbneeds.app.db.client.model.ProductReview;
import com.bnbneeds.app.db.client.model.PurchaseItem;
import com.bnbneeds.app.db.client.model.Task;
import com.bnbneeds.app.db.client.model.TenderBid;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.db.client.model.VendorAverageRating;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscription;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscriptionUsage;
import com.bnbneeds.app.db.client.model.VendorReview;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;

/**
 * This class using GAE push queue for asynchronous task execution. Although it
 * is a queue, the tasks are not executed in FIFO order. So the status of each
 * task has to be tracked manually using persistence. This class is saved for
 * learning purposes only.
 * 
 * @see DeleteVendorJobROOT
 * @author Amit Herlekar
 *
 */
@Deprecated
@RestController
@RequestMapping(method = RequestMethod.POST, path = AsyncVendorDeleteOperationsService.BASE_REQUEST_PATH)
public class AsyncVendorDeleteOperationsService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(AsyncVendorDeleteOperationsService.class);

	protected static final String BASE_REQUEST_PATH = "/async-tasks/vendor";

	private static final String DELETE_VENDOR_TASK_WORKER_URI = "/delete-vendor";
	private static final String NULLIFY_VENDOR_IN_PURCHASE_ITEMS_TASK_WORKER_URI = "/nullify-vendor-in-purchase-items";
	private static final String DELETE_PRODUCTS_TASK_WORKER_URI = "/delete-products";
	private static final String DELETE_PRODUCT_TASK_WORKER_URI = "/delete-product";
	private static final String DELETE_REVIEWS_TASK_WORKER_URI = "/delete-reviews";
	private static final String DELETE_REVIEWS_IN_BUYER_REVIEWS_TASK_WORKER_URI = "/delete-reviews-in-buyer-reviews";
	private static final String DELETE_BIDDING_SUBSCRIPTION_USAGES_TASK_WORKER_URI = "/delete-bidding-subscription-usages";
	private static final String DELETE_TENDER_BIDS_TASK_WORKER_URI = "/delete-tender-bids";
	private static final String DELETE_BIDDING_SUBSCRIPTIONS_TASK_WORKER_URI = "/delete-bidding-subscriptions";
	private static final String DELETE_PAYMENT_TRANSACTIONS_TASK_WORKER_URI = "/delete-payment-transactions";

	private static final String QUEUE_NAME = "delete-dealer-task-queue";
	private static final String VENDOR_ID_QUERY_PARAM = "vendorId";
	private static final String FORCE_DELETE_QUERY_PARAM = "forceDelete";
	private static final String TASK_ID_QUERY_PARAM = "taskId";

	private static void addTaskToQueue(String workerUri, Map<String, String> paramMap) {
		String absoluteURI = String.format("%1$s%2$s%3$s", Endpoint.CONTEXT_PATH, BASE_REQUEST_PATH, workerUri);
		AsyncTaskUtility.addTaskToQueue(QUEUE_NAME, absoluteURI, paramMap);
	}

	private Vendor queryVendor(String vendorId) {
		ArgValidator.checkFieldUriType(vendorId, Vendor.class, VENDOR_ID_QUERY_PARAM);
		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		logger.info("Is vendor info of vendorID {} null?: [{}]", vendorId, vendor == null);
		ArgValidator.checkEntityNotNull(vendor, vendorId, false);
		return vendor;
	}

	private QueryFields vendorField(Vendor vendor) {
		return new QueryFields("vendor", vendor);
	}

	public static void enqueueNullifyVendorInPurchaseItemsTask(String vendorId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(NULLIFY_VENDOR_IN_PURCHASE_ITEMS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = NULLIFY_VENDOR_IN_PURCHASE_ITEMS_TASK_WORKER_URI, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> nullifyVendorInPurchaseItems(
			@RequestParam(VENDOR_ID_QUERY_PARAM) String vendorId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);
		logger.info("Executing worker: Nullify vendor in purchase items...");

		Vendor vendor = queryVendor(vendorId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);
		List<PurchaseItem> purchaseItems = dbClient.queryList(PurchaseItem.class, vendorField(vendor));
		if (!CollectionUtils.isEmpty(purchaseItems)) {
			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(vendorId, Vendor.class, PurchaseItem.class);
			}

			logger.info("Get purchase items of the assigned to vendor and nullify them...");
			DBOperationList purchaseItemDeleteOperationList = new DBOperationList();
			for (Iterator<PurchaseItem> iterator = purchaseItems.iterator(); iterator.hasNext();) {
				PurchaseItem purchaseItem = iterator.next();
				purchaseItem.setVendorToNull();
				logger.debug("Nullifying vendor field in purchase item: {}", purchaseItem);
				purchaseItemDeleteOperationList.addOperation(OperationType.UPDATE, purchaseItem);
			}
			taskHelper.transact(purchaseItemDeleteOperationList, taskId);
		}
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Purchase items of vendor nullified successfully.");
	}

	public static void enqueueDeleteProductTask(String productId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("productId", productId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_PRODUCT_TASK_WORKER_URI, paramMap);
	}

	public static void enqueueDeleteProductTask(String productId, Boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("productId", productId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_PRODUCT_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_PRODUCT_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteProduct(@RequestParam("productId") String productId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);
		logger.info("Executing worker: Delete vendor product: {}", productId);

		ArgValidator.checkFieldUriType(productId, Product.class, "productId");
		Product product = dbClient.queryObject(Product.class, productId);
		ArgValidator.checkEntityNotNull(product, productId, false);
		boolean forceDeleteFlag = Boolean.valueOf(forceDelete);

		DBOperationList productOfferDeleteOperationList = new DBOperationList();
		DBOperationList productEnquiryDeleteOperationList = new DBOperationList();
		DBOperationList productReviewDeleteOperationList = new DBOperationList();
		DBOperationList productDeleteOperationList = new DBOperationList();
		List<String> imageKeys = new ArrayList<>();

		QueryFields productField = new QueryFields("product", product);
		List<ProductOffer> productOffers = dbClient.queryList(ProductOffer.class, productField);
		if (!CollectionUtils.isEmpty(productOffers)) {

			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(productId, Product.class, ProductOffer.class);
			}

			logger.info("Get the offer for the products and delete them...");
			for (Iterator<ProductOffer> iterator = productOffers.iterator(); iterator.hasNext();) {
				ProductOffer productOffer = iterator.next();
				productOfferDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, productOffer);
			}
		}

		List<Enquiry> productEnquiries = dbClient.queryList(Enquiry.class, productField);
		if (!CollectionUtils.isEmpty(productEnquiries)) {

			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(productId, Product.class, Enquiry.class);
			}

			logger.info("Get the enquiries of the products and delete them...");
			for (Iterator<Enquiry> iterator = productEnquiries.iterator(); iterator.hasNext();) {
				Enquiry productEnquiry = iterator.next();
				productEnquiryDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, productEnquiry);
			}
		}

		// Delete product reviews
		List<ProductReview> productReviews = dbClient.queryList(ProductReview.class, productField);
		if (!CollectionUtils.isEmpty(productReviews)) {

			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(productId, Product.class, ProductReview.class);
			}

			logger.info("Get the review of the products and delete them...");
			for (Iterator<ProductReview> iterator = productReviews.iterator(); iterator.hasNext();) {
				ProductReview productReview = iterator.next();
				productReviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, productReview);
			}

			logger.info("Get the products and delete them...");
			ProductAverageRating productAverageRating = dbClient.queryObjectByFields(ProductAverageRating.class,
					productField);
			if (productAverageRating != null) {
				productDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, productAverageRating);
			}
		}

		Set<String> images = product.getImageKeys();
		if (images != null && !images.isEmpty()) {
			imageKeys.addAll(images);
		}
		productDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, product);

		taskHelper.transact(productOfferDeleteOperationList, taskId);
		taskHelper.transact(productEnquiryDeleteOperationList, taskId);
		taskHelper.transact(productReviewDeleteOperationList, taskId);
		taskHelper.transact(productDeleteOperationList, taskId);
		if (imageKeys != null && !imageKeys.isEmpty()) {
			logger.debug("Deleting product images...");
			BlobStoreUtility.deleteBlobs(imageKeys);
		}
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Product deleted successfully.");
	}

	public static void enqueueDeleteProductsTask(String vendorId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_PRODUCTS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_PRODUCTS_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteVendorProducts(@RequestParam(VENDOR_ID_QUERY_PARAM) String vendorId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);
		logger.info("Executing worker: Delete products of vendor with ID: {}", vendorId);

		Vendor vendor = queryVendor(vendorId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);
		List<Product> vendorProducts = dbClient.queryList(Product.class, vendorField(vendor));

		if (!CollectionUtils.isEmpty(vendorProducts)) {

			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(vendorId, Vendor.class, Product.class);
			}
			Task task = null;
			for (Iterator<Product> productIterator = vendorProducts.iterator(); productIterator.hasNext();) {
				Product product = productIterator.next();
				String productId = product.getId();
				task = taskHelper.createTask(productId, "Delete product with ID [" + productId + "]", task);
				enqueueDeleteProductTask(productId, forceDeleteFlag, task);
			}
		}
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSubmittedResponse("Vendor products deleted successfully.");
	}

	public static void enqueueDeleteReviewsTask(String vendorId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_REVIEWS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_REVIEWS_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteVendorReviews(@RequestParam(VENDOR_ID_QUERY_PARAM) String vendorId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);
		logger.info("Executing worker: Delete reviews of vendor with ID: {}", vendorId);

		Vendor vendor = queryVendor(vendorId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);
		// Get vendor reviews
		List<VendorReview> vendorReviews = dbClient.queryList(VendorReview.class, vendorField(vendor));
		if (!CollectionUtils.isEmpty(vendorReviews)) {

			DBOperationList vendorReviewDeleteOperationList = new DBOperationList();

			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(vendorId, Vendor.class, VendorReview.class);
			}

			logger.info("Get the reviews of the vendor and delete them...");
			for (Iterator<VendorReview> iterator = vendorReviews.iterator(); iterator.hasNext();) {
				VendorReview vendorReview = iterator.next();
				vendorReviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, vendorReview);
			}

			VendorAverageRating vendorAverageRating = dbClient.queryObjectByFields(VendorAverageRating.class,
					vendorField(vendor));
			if (vendorAverageRating != null) {
				logger.debug("Delete the vendor average rating.");
				vendorReviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, vendorAverageRating);
			}
			taskHelper.transact(vendorReviewDeleteOperationList, taskId);
		}
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Vendor reviews deleted successfully.");
	}

	public static void enqueueDeleteReviewsInBuyerReviewsTask(String vendorId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_REVIEWS_IN_BUYER_REVIEWS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_REVIEWS_IN_BUYER_REVIEWS_TASK_WORKER_URI, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteVendorReviewsInBuyerReviews(
			@RequestParam(VENDOR_ID_QUERY_PARAM) String vendorId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);
		logger.info("Executing worker: Delete buyer reviews by vendor with ID {}", vendorId);

		Vendor vendor = queryVendor(vendorId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);
		List<BuyerReview> buyerReviews = dbClient.queryList(BuyerReview.class, new QueryFields("reviewedBy", vendor));
		if (!CollectionUtils.isEmpty(buyerReviews)) {

			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(vendorId, Vendor.class, BuyerReview.class);
			}
			DBOperationList buyerReviewDeleteOperationList = new DBOperationList();

			logger.info("Get the reviews of the vendor and delete them...");
			for (Iterator<BuyerReview> iterator = buyerReviews.iterator(); iterator.hasNext();) {
				BuyerReview buyerReview = iterator.next();
				buyerReviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, buyerReview);
			}
			taskHelper.transact(buyerReviewDeleteOperationList, taskId);
		}
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Vendor reviews in buyer reviews deleted successfully.");
	}

	public static void enqueueDeleteVendorTask(String vendorId, boolean deleteUserAccount) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put("deleteUserAccount", String.valueOf(deleteUserAccount));
		addTaskToQueue(DELETE_VENDOR_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_VENDOR_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteVendorTask(@RequestParam(VENDOR_ID_QUERY_PARAM) String vendorId,
			@RequestParam("deleteUserAccount") String deleteUserAccount,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);
		logger.info("Executing worker: Delete vendor with ID {}", vendorId);

		Vendor vendor = queryVendor(vendorId);
		Boolean deleteUserAccountFlag = new Boolean(deleteUserAccount);
		DBOperationList operationList = new DBOperationList();

		logger.info("Delete the vendor entity: {}", vendor);
		operationList.addOperation(OperationType.MARK_AS_DELETED, vendor);

		UserAccount userAccount = dbClient.queryObjectByFields(UserAccount.class,
				new QueryFields("relationshipId", vendor.getId()));
		if (userAccount != null) {
			if (deleteUserAccountFlag) {
				logger.info("Finally, delete the user account: {}", userAccount);
				operationList.addOperation(OperationType.MARK_AS_DELETED, userAccount);
			} else {
				logger.info("Finally, updating the user account by setting relationship id to null");
				userAccount.setRelationshipId(null);
				operationList.addOperation(OperationType.UPDATE, userAccount);
			}
		}

		taskHelper.transact(operationList, taskId);
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Vendor deleted successfully");
	}

	public static void enqueueDeleteBiddingSubscriptionUsagesTask(String vendorId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_BIDDING_SUBSCRIPTION_USAGES_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_BIDDING_SUBSCRIPTION_USAGES_TASK_WORKER_URI, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteBiddingSubscriptionUsageTask(
			@RequestParam(VENDOR_ID_QUERY_PARAM) String vendorId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);
		logger.info("Executing worker: Delete bidding subscription usages of vendor with ID {}", vendorId);

		Vendor vendor = queryVendor(vendorId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);

		List<VendorBiddingSubscriptionUsage> biddingSubscriptionUsages = dbClient
				.queryList(VendorBiddingSubscriptionUsage.class, vendorField(vendor));

		if (!CollectionUtils.isEmpty(biddingSubscriptionUsages)) {
			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(vendorId, Vendor.class,
						VendorBiddingSubscriptionUsage.class);
			}

			logger.info("Get the bidding subscription usages of the vendor and delete them...");

			DBOperationList operationList = new DBOperationList();

			for (Iterator<VendorBiddingSubscriptionUsage> iterator = biddingSubscriptionUsages.iterator(); iterator
					.hasNext();) {
				VendorBiddingSubscriptionUsage vendorBiddingSubscriptionUsage = iterator.next();
				operationList.addOperation(OperationType.MARK_AS_DELETED, vendorBiddingSubscriptionUsage);
			}

			taskHelper.transact(operationList, taskId);
		}

		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Vendor bidding subscription usages deleted successfully");
	}

	public static void enqueueDeleteTenderBidsTask(String vendorId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_TENDER_BIDS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_TENDER_BIDS_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteTenderBidsTask(@RequestParam(VENDOR_ID_QUERY_PARAM) String vendorId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);
		logger.info("Executing worker: Delete tender bids by vendor with ID {}", vendorId);

		Vendor vendor = queryVendor(vendorId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);

		List<TenderBid> tenderBids = dbClient.queryList(TenderBid.class, vendorField(vendor));

		if (!CollectionUtils.isEmpty(tenderBids)) {
			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(vendorId, Vendor.class, TenderBid.class);
			}

			logger.info("Get the tender bids of the vendor and delete them...");

			DBOperationList operationList = new DBOperationList();

			for (Iterator<TenderBid> iterator = tenderBids.iterator(); iterator.hasNext();) {
				TenderBid tenderBid = iterator.next();
				operationList.addOperation(OperationType.MARK_AS_DELETED, tenderBid);
			}

			taskHelper.transact(operationList, taskId);
		}
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Vendor tender bids deleted successfully");
	}

	public static void enqueueDeleteBiddingSubscriptionsTask(String vendorId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_BIDDING_SUBSCRIPTIONS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_BIDDING_SUBSCRIPTIONS_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteBiddingSubscriptionsTask(
			@RequestParam(VENDOR_ID_QUERY_PARAM) String vendorId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);
		logger.info("Executing worker: Delete bidding subscriptions of vendor with ID {}", vendorId);

		Vendor vendor = queryVendor(vendorId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);

		List<VendorBiddingSubscription> biddingSubscriptions = dbClient.queryList(VendorBiddingSubscription.class,
				vendorField(vendor));

		if (!CollectionUtils.isEmpty(biddingSubscriptions)) {
			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(vendorId, Vendor.class,
						VendorBiddingSubscription.class);
			}

			logger.info("Get the bidding subscriptions of the vendor and delete them...");

			DBOperationList operationList = new DBOperationList();

			for (Iterator<VendorBiddingSubscription> iterator = biddingSubscriptions.iterator(); iterator.hasNext();) {
				VendorBiddingSubscription biddingSubscription = iterator.next();
				operationList.addOperation(OperationType.MARK_AS_DELETED, biddingSubscription);
			}

			taskHelper.transact(operationList, taskId);
		}

		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Vendor bidding subscriptions deleted successfully");
	}

	public static void enqueueDeletePaymentTransactionsTask(String vendorId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_PAYMENT_TRANSACTIONS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_PAYMENT_TRANSACTIONS_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deletePaymentTransactionsTask(
			@RequestParam(VENDOR_ID_QUERY_PARAM) String vendorId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);
		logger.info("Executing worker: Delete payment transactions of vendor with ID {}", vendorId);

		queryVendor(vendorId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);

		List<PaymentTransactionLinkedEntity> paymentTransactions = dbClient
				.queryList(PaymentTransactionLinkedEntity.class, new QueryFields("dealerId", vendorId));

		if (!CollectionUtils.isEmpty(paymentTransactions)) {
			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(vendorId, Vendor.class,
						PaymentTransactionLinkedEntity.class);
			}

			logger.info("Get the bidding subscriptions of the vendor and delete them...");

			DBOperationList operationList = new DBOperationList();

			for (Iterator<PaymentTransactionLinkedEntity> iterator = paymentTransactions.iterator(); iterator
					.hasNext();) {
				PaymentTransactionLinkedEntity paymentTransaction = iterator.next();
				operationList.addOperation(OperationType.MARK_AS_DELETED, paymentTransaction.getPaymentTransaction());
				operationList.addOperation(OperationType.MARK_AS_DELETED, paymentTransaction);
			}

			taskHelper.transact(operationList, taskId);
		}
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Vendor payment transactions deleted successfully");
	}

	public static void enqueueNullifyVendorInPurchaseItemsTask(String vendorId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(NULLIFY_VENDOR_IN_PURCHASE_ITEMS_TASK_WORKER_URI, paramMap);
	}

	public static void enqueueDeleteProductsTask(String vendorId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_PRODUCTS_TASK_WORKER_URI, paramMap);
	}

	public static void enqueueDeleteReviewsTask(String vendorId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_REVIEWS_TASK_WORKER_URI, paramMap);

	}

	public static void enqueueDeleteReviewsInBuyerReviewsTask(String vendorId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_REVIEWS_IN_BUYER_REVIEWS_TASK_WORKER_URI, paramMap);
	}

	public static void enqueueDeleteBiddingSubscriptionUsagesTask(String vendorId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_BIDDING_SUBSCRIPTION_USAGES_TASK_WORKER_URI, paramMap);
	}

	public static void enqueueDeleteBiddingSubscriptionsTask(String vendorId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_BIDDING_SUBSCRIPTIONS_TASK_WORKER_URI, paramMap);
	}

	public static void enqueueDeleteTenderBidsTask(String vendorId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_TENDER_BIDS_TASK_WORKER_URI, paramMap);
	}

	public static void enqueueDeletePaymentTransactionsTask(String vendorId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_PAYMENT_TRANSACTIONS_TASK_WORKER_URI, paramMap);
	}

	public static void enqueueDeleteVendorTask(String vendorId, boolean deleteUserAccount, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(VENDOR_ID_QUERY_PARAM, vendorId);
		paramMap.put("deleteUserAccount", String.valueOf(deleteUserAccount));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_VENDOR_TASK_WORKER_URI, paramMap);
	}
}
