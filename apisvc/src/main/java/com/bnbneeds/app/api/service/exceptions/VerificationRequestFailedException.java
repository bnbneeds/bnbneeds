package com.bnbneeds.app.api.service.exceptions;

public class VerificationRequestFailedException extends BadRequestException {

	private static final long serialVersionUID = -7895134103395491369L;
	private final static String VERIFICATION_CODE_INCORRECT = "Verification code is incorrect.";
	private final static String VERIFICATION_CODE_INCORRECT_BY_TYPE = "%s verification code is incorrect.";

	public VerificationRequestFailedException() {
	}

	public VerificationRequestFailedException(String message) {
		super(message);
	}

	public static VerificationRequestFailedException create(String message) {
		return new VerificationRequestFailedException(message);
	}

	public static VerificationRequestFailedException invalidVerificationCode() {
		return new VerificationRequestFailedException(
				VERIFICATION_CODE_INCORRECT);
	}

	public static VerificationRequestFailedException invalidVerificationCode(
			String type) {
		return new VerificationRequestFailedException(getMessage(
				VERIFICATION_CODE_INCORRECT_BY_TYPE, type));
	}

}
