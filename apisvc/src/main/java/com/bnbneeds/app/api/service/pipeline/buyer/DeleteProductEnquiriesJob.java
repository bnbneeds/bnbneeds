package com.bnbneeds.app.api.service.pipeline.buyer;

import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.Enquiry;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeleteProductEnquiriesJob extends Job2<Void, String, Boolean> {

	private static final Logger logger = LoggerFactory.getLogger(DeleteProductEnquiriesJob.class);

	private static final long serialVersionUID = 407749591090108344L;

	@Override
	public Value<Void> run(String buyerId, Boolean forceDelete) throws Exception {
		Buyer buyer = DeleteBuyerJobROOT.queryBuyer(buyerId);
		if (buyer == null) {
			return null;
		}
		List<Enquiry> productEnquiries = queryList(Enquiry.class, DeleteBuyerJobROOT.buyerField(buyer));
		if (!CollectionUtils.isEmpty(productEnquiries)) {
			if (!forceDelete) {
				throw DeleteDealerJob.cannotDeleteHasReferences(buyerId, Buyer.class, Enquiry.class);
			}

			logger.info("Get the product enquiries of the buyer and delete them...");
			DBOperationList operationList = new DBOperationList();

			for (Iterator<Enquiry> iterator = productEnquiries.iterator(); iterator.hasNext();) {
				Enquiry enquiry = iterator.next();
				operationList.addOperation(OperationType.MARK_AS_DELETED, enquiry);
			}
			transact(operationList);
		}
		return null;
	}
}
