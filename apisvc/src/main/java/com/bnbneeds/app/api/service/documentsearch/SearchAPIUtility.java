package com.bnbneeds.app.api.service.documentsearch;

import com.bnbneeds.app.util.StringUtils;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.Field.Builder;
import com.google.appengine.api.search.Index;
import com.google.appengine.api.search.IndexSpec;
import com.google.appengine.api.search.PutException;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SearchServiceFactory;
import com.google.appengine.api.search.StatusCode;

public class SearchAPIUtility {

	/**
	 * Put a given document into an index with the given indexName.
	 *
	 * @param indexName
	 *            The name of the index.
	 * @param document
	 *            A document to add.
	 * @throws InterruptedException
	 *             When Thread.sleep is interrupted.
	 */
	public static void indexDocument(String indexName, Document document, boolean ignoreException)
			throws InterruptedException {

		Index index = getDocumentIndex(indexName);

		final int maxRetry = 3;
		int attempts = 0;
		int delay = 2;
		while (true) {
			try {
				index.put(document);
			} catch (PutException e) {
				if (StatusCode.TRANSIENT_ERROR.equals(e.getOperationResult().getCode()) && ++attempts < maxRetry) { // retrying
					Thread.sleep(delay * 1000);
					delay *= 2; // easy exponential backoff
					continue;
				} else {
					if (!ignoreException) {
						throw e; // otherwise throw
					}
				}
			}
			break;
		}
	}

	public static Index getDocumentIndex(String indexName) {
		IndexSpec indexSpec = IndexSpec.newBuilder().setName(indexName).build();
		Index index = SearchServiceFactory.getSearchService().getIndex(indexSpec);
		return index;
	}

	public static void deleteDocument(String indexName, String documentId) {

		// Delete the Records from the Index
		getDocumentIndex(indexName).delete(documentId);
	}

	/**
	 * This method is used to retrieve a particular Document from the Index
	 * 
	 * @param indexName
	 *            the name of the index
	 * @param documentId
	 *            This is the key field that uniquely identifies a document in the
	 *            collection i.e. the Index.
	 * @return An instance of the Document object from the Index.
	 */
	public static Document getDocument(String indexName, String documentId) {
		// Retrieve the Record from the Index
		return getDocumentIndex(indexName).get(documentId);
	}

	/**
	 * This method is used to retrieve a list of documents from the Index that match
	 * the text.
	 * 
	 * 
	 * @param indexName
	 *            the name of the index.
	 * @param searchText
	 *            The search term to find matching documents. By default, if you do
	 *            not use the Search Language Syntax, it will retrieve all the
	 *            records that contain a partial or full text match for all
	 *            attributes of a document
	 * @param fetchSize
	 *            the number of records to be fetched.
	 * @param nextOffset
	 *            the offset to get the next set of results.
	 * @param returnDocIdsOnly
	 *            flag to return document IDs only or not
	 * @return A collection of Documents that were found
	 */
	public static Results<ScoredDocument> searchDocuments(String indexName, String searchText, int fetchSize,
			String nextOffset, boolean returnDocIdsOnly) {

		Index index = getDocumentIndex(indexName);
		Cursor cursor = null;
		int limit = 10;

		if (fetchSize > 0) {
			limit = fetchSize;
		}
		if (StringUtils.hasText(nextOffset)) {
			cursor = Cursor.newBuilder().build(nextOffset);
		} else {
			cursor = Cursor.newBuilder().build();
		}

		QueryOptions options = QueryOptions.newBuilder().setCursor(cursor).setLimit(limit)
				.setReturningIdsOnly(returnDocIdsOnly).build();
		Query query = Query.newBuilder().setOptions(options).build(searchText);
		// Retrieve the Records from the Index
		return index.search(query);
	}

	public static Builder createTextField(String name, String value) {
		return Field.newBuilder().setName(name).setText(value);
	}

	public static Builder createSearchTokensField(String value) {
		return Field.newBuilder().setName("searchTokens").setText(value);
	}

}
