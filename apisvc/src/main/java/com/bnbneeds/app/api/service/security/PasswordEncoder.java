package com.bnbneeds.app.api.service.security;

import java.util.UUID;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import com.bnbneeds.app.util.StringUtils;

public class PasswordEncoder {

	private StandardPBEStringEncryptor encryptor;
	private String algorithm;
	private int iterations;

	public PasswordEncoder() {
		super();
		// this.algorithm = "PBEWithMD5AndDES";
		// this.algorithm = "PBEWithMD5AndTripleDES";
		this.iterations = 100000;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public void setIterations(int iterations) {
		this.iterations = iterations;
	}

	private void init() {
		encryptor = new StandardPBEStringEncryptor();
		if (StringUtils.hasText(algorithm)) {
			encryptor.setAlgorithm(algorithm);
		}
		encryptor.setKeyObtentionIterations(iterations);
	}

	public String encrypt(String passKey, String plainTextPassword) {
		init();
		encryptor.setPassword(passKey);
		String encryptedPassword = encryptor.encrypt(plainTextPassword);
		return encryptedPassword;
	}

	public String decrypt(String passKey, String encryptedPassword) {
		init();
		encryptor.setPassword(passKey);
		String decryptedPassword = encryptor.decrypt(encryptedPassword);
		return decryptedPassword;
	}

	public String generateEncrytedPassword(String passKey) {
		String plainTextPassword = generateRandomPassword();
		return encrypt(passKey, plainTextPassword);
	}

	public String generateRandomPassword() {
		StringBuffer buffer = new StringBuffer(UUID.randomUUID().toString());
		String plainTextPassword = buffer.substring(
				buffer.lastIndexOf("-") + 1, buffer.length());
		return plainTextPassword;
	}

}
