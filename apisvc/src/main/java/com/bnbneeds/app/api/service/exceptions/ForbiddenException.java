package com.bnbneeds.app.api.service.exceptions;

import com.bnbneeds.app.db.client.model.DataObject;

public class ForbiddenException extends APIException {

	private static final long serialVersionUID = 4507969646304306503L;

	private static final String USER_NOT_REGISTERED_AS_DEALER = "User has not yet registered his/her organization of type: %s.";
	private static final String USER_ALREADY_REGISTERED_AS_DEALER = "User is already registered his/her organization of type: %s.";
	private static final String USER_NOT_AUTHORIZED = "User is not authorized to access this resource.";
	private static final String OBJECT_WITH_ID_IS_BLACKLISTED = "Object with id: %s is blacklisted.";
	private static final String OBJECT_WITH_ID_IS_DISAPPROVED = "Object with id: %s is disapproved.";
	private static final String OBJECT_WITH_ID_IS_APPROVED = "Object with id: %s is approved.";
	private static final String OBJECT_WITH_ID_IS_IN_USE = "Object with id: %s is in use.";
	private static final String DELETE_RESOURCE_FAILED_HAS_REFERENCES = "Cannot delete %1$s with id: %2$s. It is referenced by records of %3$s.";
	private static final String SUBSCRIPTION_PLAN_NOT_AVAILABLE = "This plan is not available for subscription: %1$s.";
	private static final String DEALER_SUBSCRIPTION_DOES_NOT_EXIST = "No active subscription exists for the %1$s: %2$s.";
	private static final String DEALER_SUBSCRIPTION_QUOTA_EXCEEDED = "Subscription quota exceeded for the %1$s: %2$s. Please renew your subscription.";
	private static final String DEALER_SUBSCRIPTION_ALREADY_EXISTS = "Subscription already exists. State : %s.";
	private static final String DEALER_SUBSCRIPTION_EXISTS_IN_STATE = "Subscription state is: %s.";
	private static final String DEALER_SUBSCRIPTION_EXISTS_IN_STATE_WITH_MESSAGE = "Subscription state is: %1$s. %2$s.";
	private static final String DEALER_SUBSCRIPTION_EXPIRED = "Subscription is expired. It is no longer valid after %s.";
	private static final String DEALER_SUBSCRIPTION_DUE_TO_BE_MARKED_EXPIRED = "Subscription is no longer valid after %s. Please renew your subscription.";
	private static final String TENDER_FUNCTION_NOT_ALLOWED_WITH_MESSAGE = "Tender status is: %1$s. %2$s";
	private static final String SUBSCRIPTION_WITH_PLAN_EXISTS = "Cannot update the plan. An active subscription with the plan %s exists.";
	private static final String TENDER_NOT_OPEN_FOR_BIDDING = "This tender is not open for bidding.";
	private static final String TENDER_NOT_OPEN_FOR_BIDDING_WITH_MESSAGE = TENDER_NOT_OPEN_FOR_BIDDING + " %s.";
	private static final String TENDER_NOT_ACCESSED = "This tender is not accessed. First access the tender to update the subscription usage.";
	private static final String DEALER_SUBSCRIPTION_ASSOCIATED = "A subscription is already associated with the dealer: %s.";

	public ForbiddenException() {
	}

	public ForbiddenException(String message) {
		super(message);
	}

	public ForbiddenException(Throwable cause) {
		super(cause);
	}

	public static ForbiddenException create(String message) {
		return new ForbiddenException(message);
	}

	public static ForbiddenException userNotRegisteredAsDealer(String role) {
		return new ForbiddenException(getMessage(USER_NOT_REGISTERED_AS_DEALER, role));
	}

	public static ForbiddenException userAlreadyRegisteredAsDealer(String role) {
		return new ForbiddenException(getMessage(USER_ALREADY_REGISTERED_AS_DEALER, role));
	}

	public static ForbiddenException unauthorizedAccess() {
		return new ForbiddenException(USER_NOT_AUTHORIZED);
	}

	public static ForbiddenException objectIsBlacklisted(String id) {
		return new ForbiddenException(getMessage(OBJECT_WITH_ID_IS_BLACKLISTED, id));
	}

	public static ForbiddenException objectIsDisapproved(String id) {
		return new ForbiddenException(getMessage(OBJECT_WITH_ID_IS_DISAPPROVED, id));
	}

	public static ForbiddenException objectIsApproved(String id) {
		return new ForbiddenException(getMessage(OBJECT_WITH_ID_IS_APPROVED, id));
	}

	public static ForbiddenException objectIsInUse(String id) {
		return new ForbiddenException(getMessage(OBJECT_WITH_ID_IS_IN_USE, id));

	}

	public static ForbiddenException deleteFailedHasReferences(String id, Class<? extends DataObject> clazz,
			Class<? extends DataObject> referencedClass) {
		return new ForbiddenException(getMessage(DELETE_RESOURCE_FAILED_HAS_REFERENCES, clazz.getSimpleName(), id,
				referencedClass.getSimpleName()));
	}

	public static ForbiddenException subscriptionPlanNotAvailable(String subscriptionPlanId) {
		return new ForbiddenException(getMessage(SUBSCRIPTION_PLAN_NOT_AVAILABLE, subscriptionPlanId));
	}

	public static ForbiddenException dealerSubscriptionAlreadyExists(String state) {
		return new ForbiddenException(getMessage(DEALER_SUBSCRIPTION_ALREADY_EXISTS, state));
	}

	public static ForbiddenException dealerSubscriptionExistsInState(String state) {
		return new ForbiddenException(getMessage(DEALER_SUBSCRIPTION_EXISTS_IN_STATE, state));
	}

	public static ForbiddenException dealerSubscriptionExistsInState(String state, String message) {
		return new ForbiddenException(getMessage(DEALER_SUBSCRIPTION_EXISTS_IN_STATE_WITH_MESSAGE, state, message));
	}

	public static ForbiddenException dealerSubscriptionDoesNotExist(String dealerType, String dealerName) {
		return new ForbiddenException(getMessage(DEALER_SUBSCRIPTION_DOES_NOT_EXIST, dealerType, dealerName));
	}

	public static ForbiddenException dealerSubscriptionQuotaExceeded(String dealerType, String dealerName) {
		return new ForbiddenException(getMessage(DEALER_SUBSCRIPTION_QUOTA_EXCEEDED, dealerType, dealerName));
	}

	public static ForbiddenException cannotProcessRequestOnTender(String currentStatus, String message) {
		return new ForbiddenException(getMessage(TENDER_FUNCTION_NOT_ALLOWED_WITH_MESSAGE, currentStatus, message));
	}

	public static ForbiddenException dealerSubscriptionExpired(String expiryTimestamp) {
		return new ForbiddenException(getMessage(DEALER_SUBSCRIPTION_EXPIRED, expiryTimestamp));
	}

	public static ForbiddenException dealerSubscriptionDueToBeMarkedExpired(String expiryTimestamp) {
		return new ForbiddenException(getMessage(DEALER_SUBSCRIPTION_DUE_TO_BE_MARKED_EXPIRED, expiryTimestamp));
	}

	public static ForbiddenException dealerSubscriptionAlreadyExistsForPlan(String id) {
		return new ForbiddenException(getMessage(SUBSCRIPTION_WITH_PLAN_EXISTS, id));
	}

	public static ForbiddenException tenderNotOpenForBidding() {
		return new ForbiddenException(TENDER_NOT_OPEN_FOR_BIDDING);
	}

	public static ForbiddenException tenderNotOpenForBidding(String message) {
		return new ForbiddenException(getMessage(TENDER_NOT_OPEN_FOR_BIDDING_WITH_MESSAGE, message));
	}

	public static ForbiddenException tenderIsNotAccessed() {
		return new ForbiddenException(TENDER_NOT_ACCESSED);
	}

	public static ForbiddenException dealerAlreadyHasSubsctiption(String dealerId) {
		return new ForbiddenException(getMessage(DEALER_SUBSCRIPTION_ASSOCIATED, dealerId));
	}
}
