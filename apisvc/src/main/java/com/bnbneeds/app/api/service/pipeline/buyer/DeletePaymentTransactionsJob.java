package com.bnbneeds.app.api.service.pipeline.buyer;

import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.PaymentTransactionLinkedEntity;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeletePaymentTransactionsJob extends Job2<Void, String, Boolean> {

	private static final Logger logger = LoggerFactory.getLogger(DeletePaymentTransactionsJob.class);

	private static final long serialVersionUID = 3753930085444932031L;

	@Override
	public Value<Void> run(String buyerId, Boolean forceDelete) throws Exception {

		List<PaymentTransactionLinkedEntity> paymentTransactions = queryList(PaymentTransactionLinkedEntity.class,
				new QueryFields("dealerId", buyerId));

		if (!CollectionUtils.isEmpty(paymentTransactions)) {
			if (!forceDelete) {
				throw DeleteDealerJob.cannotDeleteHasReferences(buyerId, Buyer.class,
						PaymentTransactionLinkedEntity.class);
			}

			logger.info("Get the payment transactions of the buyer and delete them...");
			DBOperationList operationList = new DBOperationList();

			for (Iterator<PaymentTransactionLinkedEntity> iterator = paymentTransactions.iterator(); iterator
					.hasNext();) {
				PaymentTransactionLinkedEntity paymentTransaction = iterator.next();
				operationList.addOperation(OperationType.MARK_AS_DELETED, paymentTransaction.getPaymentTransaction());
				operationList.addOperation(OperationType.MARK_AS_DELETED, paymentTransaction);
			}

			transact(operationList);
		}
		return null;
	}
}
