package com.bnbneeds.app.api.service.security;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.api.service.exceptions.InternalException;
import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Dealer;
import com.bnbneeds.app.db.client.model.UserAccount;

public class LoginHelper {

	private final static Logger logger = LoggerFactory.getLogger(LoginHelper.class);

	private static final SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss z");

	private DbClient dbClient;

	public void setDbClient(DbClient dbClient) {
		this.dbClient = dbClient;
	}

	public UserAccount getUserAccountByUsername(String username) throws InternalException {

		UserAccount userLogin = null;
		QueryFields queryFields = new QueryFields("name", username);
		userLogin = dbClient.queryObjectByFields(UserAccount.class, queryFields);

		return userLogin;
	}

	public UserAccount getUserAccountByRelationshipId(String relationshipId) throws InternalException {

		UserAccount userLogin = null;
		QueryFields queryFields = new QueryFields("relationshipId", relationshipId);
		userLogin = dbClient.queryObjectByFields(UserAccount.class, queryFields);

		return userLogin;
	}

	public UserAccount getUserAccountOfDealer(Dealer dealer) {
		return getUserAccountByRelationshipId(dealer.getId());
	}

	public void logAuthenticationAttempt(UserAccount userLogin) {

		userLogin.setLastLoginTime(TIMESTAMP_FORMAT.format(new Date()));
		logger.info("Logging authentication attempt of user {} at {}", userLogin.getName(),
				userLogin.getLastLoginTime());
		DBOperationList dbOperationList = new DBOperationList();
		dbOperationList.addOperation(OperationType.UPDATE, userLogin);
		dbClient.transact(dbOperationList);
	}

}
