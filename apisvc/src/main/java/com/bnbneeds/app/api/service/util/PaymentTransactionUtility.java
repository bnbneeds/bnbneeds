package com.bnbneeds.app.api.service.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import com.bnbneeds.app.api.service.response.PaymentTransactionResponseMapper;
import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.db.client.model.Dealer;
import com.bnbneeds.app.db.client.model.PaymentTransaction;
import com.bnbneeds.app.db.client.model.PaymentTransactionLinkedEntity;
import com.bnbneeds.app.model.dealer.CreatePaymentTransactionParam;
import com.bnbneeds.app.model.dealer.PaymentTransactionInfoListResponse;
import com.bnbneeds.app.model.dealer.PaymentTransactionInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.util.StringUtils;
import com.bnbneeds.app.util.URIUtils;

public class PaymentTransactionUtility {

	public static void validatePaymentTransactionParam(CreatePaymentTransactionParam param) {
		ArgValidator.checkFieldNotNull(param, "create_payment_transaction");
		ArgValidator.checkFieldNotEmpty(param.getTransactionId(), "transaction_id");
		ArgValidator.checkFieldNotEmpty(param.getTransactionStatus(), "transaction_status");
		ArgValidator.checkFieldValue(param.getAmount() > 0, "amount");
		ArgValidator.checkFieldNotEmpty(param.getPaymentMethod(), "payment_method");
		ArgValidator.checkFieldNotEmpty(param.getPaymentGateway(), "payment_gateway");
	}

	/**
	 * Creates two records: one of type {@code PaymentTransaction} and
	 * {@code PaymentTransactionLinkedEntity}
	 * 
	 * @param dbClient
	 *            the dbClient
	 * @param param
	 *            the create payment transaction request
	 * @param dealerType
	 *            the dealer type class
	 * @param dealerId
	 *            the dealer ID
	 * @param entityType
	 *            the linked entity type
	 * @param entityId
	 *            the linked entity ID
	 * @return the ID of new created PaymentTransaction
	 */
	public static String savePaymentTransaction(DbClient dbClient, CreatePaymentTransactionParam param,
			Class<? extends Dealer> dealerType, String dealerId, Class<? extends DataObject> entityType,
			String entityId) {

		DBOperationList dbOperationList = new DBOperationList();

		PaymentTransaction transaction = new PaymentTransaction();
		String transactionEntityId = UUIDUtil.createId(PaymentTransaction.class);
		transaction.setId(transactionEntityId);
		transaction.setAmount(param.getAmount());
		transaction.setTransactionId(param.getTransactionId());
		transaction.setTransactionStatus(param.getTransactionStatus());
		transaction.setTransactionDescription(param.getTransactionDescription());
		transaction.setPaymentMethod(param.getPaymentMethod());
		transaction.setPaymentGateway(param.getPaymentGateway());
		transaction.setTimestamp(param.getTimestamp());
		dbOperationList.addOperation(OperationType.INSERT, transaction);

		PaymentTransactionLinkedEntity linkedTransactionEntity = new PaymentTransactionLinkedEntity();
		String linkedTransactionEntityId = UUIDUtil.createId(PaymentTransactionLinkedEntity.class);
		linkedTransactionEntity.setId(linkedTransactionEntityId);
		linkedTransactionEntity.setDealerId(dealerId);
		linkedTransactionEntity.setDealerType(dealerType.getSimpleName());
		linkedTransactionEntity.setEntityType(entityType.getSimpleName());
		linkedTransactionEntity.setEntityId(entityId);
		linkedTransactionEntity.setPaymentTransaction(transaction);
		dbOperationList.addOperation(OperationType.INSERT, linkedTransactionEntity);

		dbClient.transact(dbOperationList);

		return transactionEntityId;
	}

	public static PaymentTransactionInfoListResponse getPaymentTransactionList(DbClient dbClient, String dealerId,
			String filter, String offset, int fetchSize) {

		if (!StringUtils.hasText(dealerId)) {
			throw new IllegalArgumentException("dealerId cannot be null or empty.");
		}

		PaymentTransactionInfoListResponse response = new PaymentTransactionInfoListResponse();

		QueryFields fields = new QueryFields("dealerId", dealerId);

		final String[] QUERY_FILTER_KEYS = { "entityId", "transactionId" };

		Map<String, String> keyValueMap = ResourceUtility.getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);
		String transactionId = null;
		for (String key : keyValueMap.keySet()) {

			String value = keyValueMap.get(key);
			switch (key) {
			case "entityId":
				fields.add("entityId", value);
				break;
			case "transactionId":
				transactionId = value;
			default:
				break;
			}
		}

		do {
			ResultSetIterator<PaymentTransactionLinkedEntity> resultSetIterator = DbClientUtility
					.getResultSetIterator(dbClient, PaymentTransactionLinkedEntity.class, fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					PaymentTransactionLinkedEntity item = resultSetIterator.next();
					if (item != null) {
						PaymentTransaction transaction = item.getPaymentTransaction();
						if (StringUtils.hasText(transactionId)
								&& !transactionId.equals(transaction.getTransactionId())) {
							continue;
						}
						Endpoint paymentTransactionEndpoint = getPaymentTransactionInfoEndpoint(item.getDealerType());
						PaymentTransactionInfoResponse transactionInfoResponse = PaymentTransactionResponseMapper
								.map(transaction, paymentTransactionEndpoint.get(dealerId, transaction.getId()));
						response.add(transactionInfoResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());

				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (response.size() < fetchSize);

		return response;
	}

	public static Endpoint getPaymentTransactionListEndpoint(String dealerType) {
		switch (dealerType.toLowerCase()) {
		case "buyer":
			return Endpoint.BUYER_PAYMENT_TRANSACTIONS;
		case "vendor":
			return Endpoint.VENDOR_PAYMENT_TRANSACTIONS;
		default:
			return null;
		}
	}

	public static URI getPaymentTransactionListURI(String dealerType, String dealerId, String entityId) {

		URI uri = null;

		try {
			uri = URIUtils.appendQuery(getPaymentTransactionListEndpoint(dealerType).get(dealerId),
					"entityId=" + entityId);
		} catch (URISyntaxException e) {
			// ignore this exception
		}
		return uri;
	}

	public static Endpoint getPaymentTransactionInfoEndpoint(String dealerType) {
		switch (dealerType.toLowerCase()) {
		case "buyer":
			return Endpoint.BUYER_PAYMENT_TRANSACTION_INFO;
		case "vendor":
			return Endpoint.VENDOR_PAYMENT_TRANSACTION_INFO;
		default:
			return null;
		}
	}

}
