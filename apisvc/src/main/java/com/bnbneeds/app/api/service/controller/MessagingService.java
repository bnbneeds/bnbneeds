package com.bnbneeds.app.api.service.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.IOUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.messaging.email.Attachment;
import com.bnbneeds.app.messaging.email.EmailMessage;
import com.bnbneeds.app.model.EmailMessageParam;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(path = "/messaging")
public class MessagingService extends ResourceService {

	private static final Logger logger = LoggerFactory
			.getLogger(MessagingService.class);

	private static final String FORM_DATA_INLINE_IMAGE_ATTACHMENT = "inlineImage";
	private static final String FORM_DATA_ATTACHMENT = "attachment";
	private static final String FORM_DATA_SUBJECT = "subject";
	private static final String FORM_DATA_MESSAGE = "message";
	private static final String FORM_DATA_TO = "to";
	private static final String FORM_DATA_CC = "cc";
	private static final String FORM_DATA_BCC = "bcc";
	private static final String INLINE_IMAGE_PLACE_HOLDER = "{INLINE_IMAGE}";
	private static final long MAIL_ATTACHMENT_MAX_SIZE_IN_BYTES = 3145728; // 3
																			// MB

	@RequestMapping(method = RequestMethod.POST, path = "/email", consumes = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> postTextEmailMessage(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody EmailMessageParam emailParam) throws IOException {

		logger.info("Request recieved to send email: {}", emailParam);

		ArgValidator.checkFieldNotNull(emailParam, "email_message");
		String subject = emailParam.getSubject();
		String message = emailParam.getMessage();
		String toText = emailParam.getTo();
		ArgValidator.checkFieldNotEmpty(subject, "subject");
		ArgValidator.checkFieldNotEmpty(message, "message");
		ArgValidator.checkFieldNotEmpty(toText, "to");
		Set<String> toSet = StringUtils.commaDelimitedListToSet(toText);
		ArgValidator.checkFieldNotEmpty(toSet, "to");

		EmailMessage email = new EmailMessage(subject, message,
				new ArrayList<String>(toSet));

		String ccText = emailParam.getCc();
		if (StringUtils.hasText(ccText)) {
			Set<String> ccSet = StringUtils.commaDelimitedListToSet(ccText);
			if (!ccSet.isEmpty()) {
				email.setCc(new ArrayList<String>(ccSet));
			}
		}

		String bccText = emailParam.getBcc();
		if (StringUtils.hasText(bccText)) {
			Set<String> bccSet = StringUtils.commaDelimitedListToSet(bccText);
			if (!bccSet.isEmpty()) {
				email.setBcc(new ArrayList<String>(bccSet));
			}
		}

		emailHelper.send(email);

		return TaskResponseUtility
				.createTaskSubmittedResponse("Task submitted to send email message.");

	}

	@RequestMapping(method = RequestMethod.POST, path = "/email-with-attachment", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> postEmailWithAttachmentMessage(
			@AuthenticationPrincipal BNBNeedsUser user,
			HttpServletRequest request) throws IOException {

		if (!ServletFileUpload.isMultipartContent(request)) {
			throw BadRequestException.parameterMissing(FORM_DATA_ATTACHMENT);
		}

		String subject = null;
		String message = null;
		String to = null;
		String cc = null;
		String bcc = null;
		Attachment emailAttachment = null;
		Attachment inlineImageAttachment = null;

		try {
			ServletFileUpload upload = new ServletFileUpload();
			upload.setSizeMax(-1);
			upload.setFileSizeMax(MAIL_ATTACHMENT_MAX_SIZE_IN_BYTES);
			FileItemIterator uploadIterator = upload.getItemIterator(request);

			while (uploadIterator.hasNext()) {
				FileItemStream item = uploadIterator.next();
				if (item.isFormField()) {

					if (FORM_DATA_SUBJECT.equals(item.getFieldName())) {
						subject = IOUtility.getString(item);
					}

					if (FORM_DATA_MESSAGE.equals(item.getFieldName())) {
						message = IOUtility.getString(item);
					}

					if (FORM_DATA_TO.equals(item.getFieldName())) {
						to = IOUtility.getString(item);
					}

					if (FORM_DATA_CC.equals(item.getFieldName())) {
						cc = IOUtility.getString(item);
					}
					if (FORM_DATA_BCC.equals(item.getFieldName())) {
						bcc = IOUtility.getString(item);
					}
				} else {
					if (FORM_DATA_ATTACHMENT.equals(item.getFieldName())) {
						emailAttachment = IOUtility.getAttachment(item);
					}

					if (FORM_DATA_INLINE_IMAGE_ATTACHMENT.equals(item
							.getFieldName())) {

						if (!IOUtility.isImage(item)) {
							throw BadRequestException.parameterInvalid(
									FORM_DATA_INLINE_IMAGE_ATTACHMENT,
									"This file is not an image: "
											+ item.getName() + " [Type: "
											+ item.getContentType() + "]");
						}
						inlineImageAttachment = IOUtility.getAttachment(item);
					}
				}
			}

			ArgValidator.checkFieldNotEmpty(subject, FORM_DATA_SUBJECT);
			ArgValidator.checkFieldNotEmpty(message, FORM_DATA_MESSAGE);
			ArgValidator.checkFieldNotEmpty(to, FORM_DATA_TO);

			boolean attachmentUploaded = (emailAttachment != null)
					|| (inlineImageAttachment != null);
			ArgValidator.checkField(attachmentUploaded, FORM_DATA_ATTACHMENT);

			Set<String> toSet = StringUtils.commaDelimitedListToSet(to);
			ArgValidator.checkFieldNotEmpty(toSet, FORM_DATA_TO);

			EmailMessage email = new EmailMessage(subject, message,
					new ArrayList<String>(toSet));
			if (emailAttachment != null) {
				email.addAttachment(emailAttachment);
			}

			if (inlineImageAttachment != null) {
				if (!message.contains(INLINE_IMAGE_PLACE_HOLDER)) {
					throw BadRequestException.parameterInvalid(
							FORM_DATA_MESSAGE, "The placeholder "
									+ INLINE_IMAGE_PLACE_HOLDER + " not found");
				}
				email.setInlineImage(inlineImageAttachment);
			} else {
				if (message.contains(INLINE_IMAGE_PLACE_HOLDER)) {
					throw BadRequestException
							.parameterMissing(FORM_DATA_INLINE_IMAGE_ATTACHMENT);
				}
			}

			Set<String> ccSet = StringUtils.commaDelimitedListToSet(cc);
			if (!ccSet.isEmpty()) {
				email.setCc(new ArrayList<String>(ccSet));
			}
			Set<String> bccSet = StringUtils.commaDelimitedListToSet(bcc);
			if (!bccSet.isEmpty()) {
				email.setBcc(new ArrayList<String>(bccSet));
			}
			emailHelper.send(email);

			return TaskResponseUtility
					.createTaskSubmittedResponse("Task submitted to send email message.");
		} catch (FileUploadException e) {
			String errorMessage = e.getMessage();
			logger.error(errorMessage);
			throw BadRequestException.parameterInvalid(FORM_DATA_ATTACHMENT,
					errorMessage);
		}

	}
}
