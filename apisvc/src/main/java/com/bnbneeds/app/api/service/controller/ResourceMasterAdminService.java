package com.bnbneeds.app.api.service.controller;

import static com.bnbneeds.app.api.service.response.DBObjectMapper.toNamedRelatedListResponse;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.api.service.exceptions.APIException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.DataObjectResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.BlobStoreUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.ApprovedDataObject;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.TenderProduct;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.db.client.model.DescribedDataObject;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.ProductCategory;
import com.bnbneeds.app.db.client.model.ProductName;
import com.bnbneeds.app.db.client.model.ProductType;
import com.bnbneeds.app.model.DataObjectListResponse;
import com.bnbneeds.app.model.DescribedDataObjectRestResponse;
import com.bnbneeds.app.model.EntityInfoListResponse;
import com.bnbneeds.app.model.EntityInfoResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.UpdateEntityParam;
import com.bnbneeds.app.model.dealer.BusinessTypeInfoResponse;
import com.bnbneeds.app.model.dealer.BusinessTypeListResponse;
import com.bnbneeds.app.model.product.ProductNameInfoResponse;
import com.bnbneeds.app.model.product.ProductNameListResponse;
import com.bnbneeds.app.model.product.ProductTypeInfoResponse;
import com.bnbneeds.app.model.product.ProductTypeListResponse;
import com.bnbneeds.app.util.StringUtils;

public abstract class ResourceMasterAdminService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(ResourceMasterAdminService.class);

	protected static final int MAX_ENTITIES_SIZE = 25;

	public abstract Class<? extends DescribedDataObject> getDataModelClass();

	private DescribedDataObjectRestResponse getDataObjectInfoResponse() {
		Class<? extends DescribedDataObject> clazz = getDataModelClass();
		String className = clazz.getSimpleName().toLowerCase();

		switch (className) {
		case "productname":
			return new ProductNameInfoResponse();
		case "producttype":
			return new ProductTypeInfoResponse();
		case "businesstype":
			return new BusinessTypeInfoResponse();
		case "productcategory":
			return new ProductNameInfoResponse();
		}

		return null;
	}

	private DataObjectListResponse getDataObjectListResponse() {
		Class<? extends DescribedDataObject> clazz = getDataModelClass();
		String className = clazz.getSimpleName().toLowerCase();

		switch (className) {
		case "productname":
			return new ProductNameListResponse();
		case "producttype":
			return new ProductTypeListResponse();
		case "businesstype":
			return new BusinessTypeListResponse();
		case "productcategory":
			return new ProductNameListResponse();
		}
		return null;
	}

	protected void setStatusOfNewEntity(ApprovedDataObject dataObject, BNBNeedsUser user) {
		StringBuffer remark = new StringBuffer(dataObject.getClass().getSimpleName());
		remark.append(" entity created by ").append(user.getUsername());
		if (user.isAdmin()) {
			dataObject.setEntityStatus(EntityStatus.APPROVED);
			remark.append(", Role: ").append(user.getRole()).append(".");
		} else {
			dataObject.setEntityStatus(EntityStatus.DISAPPROVED);
		}
		dataObject.addRemark(remark.toString());
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public ResponseEntity<TaskResponse> updateResource(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String id, @RequestBody UpdateEntityParam param) {

		logger.info("Request received to update resource with id: {},", id);

		ArgValidator.checkFieldUriType(id, getDataModelClass(), "id");
		ArgValidator.checkFieldNotNull(param, "update_entity");
		DescribedDataObject resource = dbClient.queryObject(getDataModelClass(), id);
		ArgValidator.checkEntityNotNull(resource, id, true);

		boolean changed = false;
		String newName = param.getName();
		if (!StringUtils.isEmpty(newName)) {
			resource.setName(newName);
			changed = true;
		}
		String newDescription = param.getDescription();
		if (!StringUtils.isEmpty(newDescription)) {
			resource.setDescription(newDescription);
			changed = true;
		}
		String newStatus = param.getStatus();
		if (!StringUtils.isEmpty(newStatus)) {
			ArgValidator.checkFieldValueFromEnum(newStatus, "status", ApprovedDataObject.EntityStatus.class);
			resource.setEntityStatus(EntityStatus.valueOf(newStatus));
			changed = true;
		}
		String remark = param.getRemark();
		if (!StringUtils.isEmpty(remark)) {
			resource.addRemark(user.getUsername(), Role.ADMIN, remark);
			changed = true;
		}

		if (changed) {
			dbClient.updateObject(resource);
		}

		logger.info("Resource: {} updated successfully.", id);

		return TaskResponseUtility
				.createTaskSuccessResponse(getDataModelClass().getSimpleName() + " updated  successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "', '" + Role.ADMIN + "')")
	public DescribedDataObjectRestResponse getResourceInfo(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String id) throws APIException {

		logger.info("Request received to get information of resource with id: {}", id);

		ArgValidator.checkFieldUriType(id, getDataModelClass(), "id");

		DescribedDataObjectRestResponse resp = getDataObjectInfoResponse();
		DescribedDataObject entity = dbClient.queryObject(getDataModelClass(), id);

		if (user.isAdmin()) {
			ArgValidator.checkEntityNotNull(entity, id, true);
		} else {
			ArgValidator.checkEntityBlacklisted(entity, id, true);
		}

		DataObjectResponseMapper.mapDescribedDataObject(entity, resp);

		if (entity instanceof ProductName) {
			ProductName productName = (ProductName) entity;
			ProductNameInfoResponse productNameInfoResponse = (ProductNameInfoResponse) resp;
			productNameInfoResponse.setImage(productName.getImageKey());
		}

		logger.info("Exit returning response: {}", resp);

		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "',  '" + Role.ADMIN + "')")
	public DataObjectListResponse getResourceList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = "filter", required = false) String filter,
			@RequestParam(name = "fetchSize", defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = "offset", required = false) String offset) {

		logger.info("Request received to get product names by filter: {}, fetchSize: {}, offset: {}", filter, fetchSize,
				offset);
		final String[] QUERY_FILTER_KEYS = { "name", "status" };
		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);
		QueryFields fields = new QueryFields();

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case "name":
				fields.add(key, value);
				break;
			case "status":
				if (!user.isAdmin()) {
					ArgValidator.checkFieldValue(!EntityStatus.BLACKLISTED.name().equals(value.toUpperCase()), key);
				}
				fields.add("entityStatus", value.toUpperCase());
				break;
			default:
				break;
			}
		}

		DataObjectListResponse resp = getDataObjectListResponse();
		ResultSetIterator<? extends DataObject> resultSetIterator = null;
		do {
			resultSetIterator = getResultSetIterator(getDataModelClass(), fields, offset, fetchSize);
			if (resultSetIterator != null) {
				if (user.isAdmin()) {
					resp.addNamedRelatedResourceList(toNamedRelatedListResponse(resultSetIterator));
				} else {
					resp.addNamedRelatedResourceList(toNamedRelatedListResponse(resultSetIterator, null,
							new EntityStatus[] { EntityStatus.BLACKLISTED }));
				}
				resp.setNextOffset(resultSetIterator.getOffset());

				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (resp.size() < fetchSize);

		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/bulk", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "',  '" + Role.ADMIN + "')")
	public EntityInfoListResponse getResourceInfoList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get product names by filter: {}, fetchSize: {}, offset: {}", filter, fetchSize,
				offset);

		final String[] QUERY_FILTER_KEYS = { "name", "status" };
		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields();

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case "name":
				fields.add(key, value);
				break;
			case "status":
				if (!user.isAdmin()) {
					ArgValidator.checkFieldValue(!EntityStatus.BLACKLISTED.name().equals(value.toUpperCase()), key);
				}
				fields.add("entityStatus", value.toUpperCase());
				break;
			default:
				break;
			}
		}

		EntityInfoListResponse response = new EntityInfoListResponse();
		ResultSetIterator<? extends DescribedDataObject> resultSetIterator = null;

		do {
			resultSetIterator = getResultSetIterator(getDataModelClass(), fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					DescribedDataObject dataObject = resultSetIterator.next();
					EntityInfoResponse objectResponse = new EntityInfoResponse();
					DataObjectResponseMapper.mapDescribedDataObject(dataObject, objectResponse);
					if (user.isAdmin()) {
						response.add(objectResponse);
					} else {
						if (!dataObject.isBlacklisted()) {
							response.add(objectResponse);
						}
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());

				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}
		} while (response.size() < fetchSize);
		return response;
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public ResponseEntity<TaskResponse> deleteResource(@PathVariable("id") String id) {

		logger.info("Request received to delete resource with id: {},", id);

		ArgValidator.checkFieldUriType(id, getDataModelClass(), "id");
		Class<? extends DescribedDataObject> entityType = getDataModelClass();
		ApprovedDataObject resource = dbClient.queryObject(entityType, id);
		ArgValidator.checkEntityApproved(resource, id, true);

		switch (entityType.getSimpleName().toLowerCase()) {
		case "productname":
			List<Product> productsByName = dbClient.queryList(Product.class, new QueryFields("productName", resource));
			if (!CollectionUtils.isEmpty(productsByName)) {
				throw ForbiddenException.deleteFailedHasReferences(id, ProductName.class, Product.class);
			}

			List<TenderProduct> bidProducts = dbClient.queryList(TenderProduct.class, new QueryFields("product", resource));
			if (!CollectionUtils.isEmpty(bidProducts)) {
				throw ForbiddenException.deleteFailedHasReferences(id, ProductName.class, TenderProduct.class);
			}
			if (resource instanceof ProductName) {
				ProductName productName = (ProductName) resource;
				String imageKey = productName.getImageKey();
				if (!StringUtils.isEmpty(imageKey)) {
					logger.debug("Deleting image blob of the product name: {},", imageKey);
					BlobStoreUtility.deleteBlob(imageKey);
				}
			}
			break;
		case "productcategory":
			List<Product> productsByCategory = dbClient.queryList(Product.class,
					new QueryFields("productCategory", resource));
			if (!CollectionUtils.isEmpty(productsByCategory)) {
				throw ForbiddenException.deleteFailedHasReferences(id, ProductCategory.class, Product.class);
			}
			break;
		case "producttype":
			List<Product> productsByType = dbClient.queryList(Product.class, new QueryFields("productType", resource));
			if (!CollectionUtils.isEmpty(productsByType)) {
				throw ForbiddenException.deleteFailedHasReferences(id, ProductType.class, Product.class);
			}
			break;
		case "businesstype":
			List<Buyer> buyers = dbClient.queryList(Buyer.class, new QueryFields("businessType", resource));
			if (!CollectionUtils.isEmpty(buyers)) {
				throw ForbiddenException.deleteFailedHasReferences(id, Buyer.class, Buyer.class);
			}
			break;
		}

		dbClient.markAsDeleted(resource);

		logger.info("Resource: {} deleted successfully.", id);
		return TaskResponseUtility
				.createTaskSuccessResponse(getDataModelClass().getSimpleName() + " deleted  successfully.");
	}
}
