package com.bnbneeds.app.api.service.util;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.util.StringUtils;

public abstract class ResourceUtility {

	private static final Logger logger = LoggerFactory.getLogger(ResourceUtility.class);

	public static UserAccount getUserAccount(DbClient dbClient, BNBNeedsUser user) {
		UserAccount userAccount = dbClient.queryObjectByFields(UserAccount.class,
				new QueryFields("name", user.getUsername()));
		return userAccount;
	}

	public static Map<String, String> getKeyValuePairFromFilter(final String filter, final String[] validFilterKeys) {

		// final String QUERY_FILTER_PATTERN =
		// "^(?:\\s*((?:[^\\:\\:\\|\\s]+\\s+)*[^\\:\\:\\|\\s]+)\\s*\\:\\:\\s*((?:[^\\:\\:\\|\\s]+\\s+)*[^\\:\\:\\|\\s]+)\\s*(?:\\|(?!\\s*$)|$))+$";
		final String QUERY_FILTER_PATTERN = "^(?:\\s*((?:[^>\\|\\s]+\\s+)*[^>\\|\\s]+)\\s*>>\\s*((?:[^>\\|\\s]+\\s+)*[^>\\|\\s]+)\\s*(?:\\|(?!\\s*$)|$))+$";
		final String QUERY_PARAM_PAIR_DELIMITER = "\\|";
		// final String QUERY_PARAM_VALUE_DELIMITER = "::";
		final String QUERY_PARAM_VALUE_DELIMITER = ">>";

		Map<String, String> map = new LinkedHashMap<>();

		if (!StringUtils.isEmpty(filter)) {

			if (!filter.matches(QUERY_FILTER_PATTERN)) {
				// throw
				// BadRequestException.create("Filter pattern is invalid. Valid pattern:
				// filter=key1>>value1|key2>>value2|key3>>value3...");
				/*
				 * throw BadRequestException.parameterInvalidExpectedValue( "filter",
				 * "key1::value1|key2::value2|key3::value3...");
				 */
				throw BadRequestException.parameterInvalidExpectedValue("filter",
						"key1>>value1|key2>>value2|key3>>value3...");
			}

			String[] queryParamArray = filter.split(QUERY_PARAM_PAIR_DELIMITER);

			if (!StringUtils.isEmpty(queryParamArray)) {

				List<String> validQueryParamList = Arrays.asList(validFilterKeys);

				for (int i = 0; i < queryParamArray.length; i++) {

					String queryParamPair = queryParamArray[i];

					if (!StringUtils.isEmpty(queryParamPair)) {
						String[] queryParamPairArray = queryParamPair.split(QUERY_PARAM_VALUE_DELIMITER);

						if (!StringUtils.isEmpty(queryParamPairArray) && queryParamPairArray.length == 2) {
							String key = queryParamPairArray[0].trim();
							if (validQueryParamList.contains(key)) {
								String valueText = queryParamPairArray[1].trim();
								logger.debug("Adding key:value pair in the map: [{} : {}]", key, valueText);
								map.put(key, valueText);
							}
						}
					}
				}
			}
		}

		return map;
	}

}
