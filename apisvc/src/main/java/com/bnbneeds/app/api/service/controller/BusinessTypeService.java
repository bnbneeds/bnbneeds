package com.bnbneeds.app.api.service.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.BusinessType;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.CreateBusinessTypeBulkParam;
import com.bnbneeds.app.model.dealer.CreateBusinessTypeParam;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/business-types")
public class BusinessTypeService extends ResourceMasterAdminService {

	private static final Logger logger = LoggerFactory
			.getLogger(BusinessTypeService.class);

	@RequestMapping(method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.BUYER + "', '" + Role.ADMIN + "')")
	public ResponseEntity<CreateResourceResponse> createBusinessType(
			@RequestBody CreateBusinessTypeParam param,
			@AuthenticationPrincipal BNBNeedsUser user) {

		logger.info("API request received to add a new business type: {}",
				param);

		ArgValidator.checkFieldNotEmpty(param.getName(), "name");

		QueryFields queryFields = new QueryFields();
		queryFields.add("name", param.getName());

		BusinessType businessTypeEntity = dbClient.queryObjectByFields(
				BusinessType.class, queryFields);

		if (businessTypeEntity != null) {
			logger.error(
					"Business type with the given name already exists: {}",
					param.getName());
			throw BadRequestException.nameAlreadyExists("Business type",
					param.getName());
		}

		businessTypeEntity = new BusinessType();
		String uid = UUIDUtil.createId(BusinessType.class);
		businessTypeEntity.setId(uid);
		businessTypeEntity.setName(param.getName());
		String description = param.getDescription();
		if (!StringUtils.isEmpty(description)) {
			businessTypeEntity.setDescription(description);
		}

		setStatusOfNewEntity(businessTypeEntity, user);

		logger.debug("New business type record creating in DB: {}",
				businessTypeEntity);

		dbClient.createObject(businessTypeEntity);

		logger.info("New business type persisted in DB: {}", uid);

		recordEvent(SystemEventType.BUSINESS_TYPE_CREATED, RecipientType.ADMIN,
				uid, param.getName(), null, user.getUsername());

		return TaskResponseUtility.createResourceResponse(uid,
				Endpoint.BUSINESS_TYPE_INFO,
				"New business type created successfully");
	}

	@RequestMapping(value = "/bulk", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.BUYER + "', '" + Role.ADMIN + "')")
	public ResponseEntity<TaskResponse> createBusinessTypes(
			@RequestBody CreateBusinessTypeBulkParam bulkParam,
			@AuthenticationPrincipal BNBNeedsUser user) {

		logger.info("API request received to add business types: {}", bulkParam);

		ArgValidator.checkFieldNotNull(bulkParam, "create_business_types");
		ArgValidator.checkFieldSize(bulkParam.getBusinessTypeList(),
				"business_types", MAX_ENTITIES_SIZE);

		DBOperationList dbOperationList = new DBOperationList();
		Set<String> namesInRequest = new HashSet<String>();
		for (Iterator<CreateBusinessTypeParam> iterator = bulkParam
				.getBusinessTypeList().iterator(); iterator.hasNext();) {
			CreateBusinessTypeParam requestParam = iterator.next();
			String name = requestParam.getName();

			ArgValidator.checkFieldNotEmpty(name, "name");
			if (namesInRequest.contains(name)) {
				logger.error(
						"Business type with same name already exists in the request: {}",
						name);
				throw BadRequestException.nameAlreadyExists("Business type",
						name);
			}
			namesInRequest.add(name);
			QueryFields queryFields = new QueryFields("name", name);

			BusinessType businessTypeEntity = dbClient.queryObjectByFields(
					BusinessType.class, queryFields);

			if (businessTypeEntity != null) {
				logger.error("Business type with same name already exists: {}",
						name);
				throw BadRequestException.nameAlreadyExists("Business type",
						name);
			}

			businessTypeEntity = new BusinessType();
			String uid = UUIDUtil.createId(BusinessType.class);
			businessTypeEntity.setId(uid);
			businessTypeEntity.setName(name);
			String description = requestParam.getDescription();
			if (!StringUtils.isEmpty(description)) {
				businessTypeEntity.setDescription(description);
			}

			setStatusOfNewEntity(businessTypeEntity, user);

			dbOperationList.addOperation(OperationType.INSERT,
					businessTypeEntity);

		}

		logger.debug("New business types in a DB transaction: {}",
				dbOperationList);

		dbClient.transact(dbOperationList);

		recordEvent(SystemEventType.BUSINESS_TYPES_CREATED,
				RecipientType.ADMIN, null, null, null, user.getUsername());

		String logMessage = "Task initiated to create business types.";

		logger.info(logMessage);

		return TaskResponseUtility.createTaskSubmittedResponse(logMessage);
	}

	@Override
	public Class<BusinessType> getDataModelClass() {
		return BusinessType.class;
	}

}