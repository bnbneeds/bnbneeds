package com.bnbneeds.app.api.service.pipeline.vendor;

import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.Vendor;
import com.google.appengine.tools.pipeline.FutureValue;
import com.google.appengine.tools.pipeline.ImmediateValue;
import com.google.appengine.tools.pipeline.Value;

/**
 * This class deletes the vendor entity along with all its references. It
 * employs GAE pipelines to execute tasks sequentially.
 * 
 * See <a href=
 * "https://github.com/GoogleCloudPlatform/appengine-pipelines/wiki/Java"
 * >AppEngine Pipelines using Java</a>
 * 
 * @author Amit Herlekar
 *
 */
public class DeleteVendorJobROOT extends DeleteDealerJob {

	private static final Logger logger = LoggerFactory.getLogger(DeleteVendorJobROOT.class);

	private static final long serialVersionUID = 4939125748442384823L;

	private String vendorId;
	private Boolean deleteUserAccount;
	private Boolean forcedelete;

	public DeleteVendorJobROOT(String vendorId, Boolean deleteUserAccount, Boolean forcedelete) {
		super();
		this.vendorId = vendorId;
		this.deleteUserAccount = deleteUserAccount;
		this.forcedelete = forcedelete;
	}

	static QueryFields vendorField(Vendor vendor) {
		return new QueryFields("vendor", vendor);
	}

	static Vendor queryVendor(String vendorId) {
		return queryObject(Vendor.class, vendorId);
	}

	@Override
	public String getJobDisplayName() {
		return "Delete Vendor entity and all its child references. Vendor ID: " + vendorId;
	}

	@Override
	public Value<Void> run() throws Exception {

		logger.info("Request receieved to delete the vendor record and all its children: [{}]", vendorId);

		/*
		 * The order of these calls are very important
		 */

		Value<String> vendorIdInput = new ImmediateValue<>(vendorId);
		Value<Boolean> forceDeleteInput = new ImmediateValue<>(forcedelete);

		FutureValue<Void> job = futureCall(new NullifyPurchaseItemsJob(), vendorIdInput, forceDeleteInput,
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteProductsJob(), vendorIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteReviewsJob(), vendorIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteReviewsOfBuyersJob(), vendorIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteBiddingSubscriptionUsageJob(), vendorIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteBiddingSubscriptionsJob(), vendorIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteTenderBidsJob(), vendorIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeletePaymentTransactionsJob(), vendorIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteVendorEntityJob(), vendorIdInput, new ImmediateValue<>(deleteUserAccount),
				waitFor(job), DELETE_DEALER_JOB_SETTING);
		futureCall(new DeleteVendorFromDocumentIndexJob(vendorId), waitFor(job), DELETE_DEALER_JOB_SETTING);
		return null;
	}

}
