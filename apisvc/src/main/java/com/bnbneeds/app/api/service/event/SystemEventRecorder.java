package com.bnbneeds.app.api.service.event;

import org.springframework.context.ApplicationListener;

import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.model.Notification;

public class SystemEventRecorder implements ApplicationListener<SystemEvent> {

	private DbClient dbClient;

	public void setDbClient(DbClient dbClient) {
		this.dbClient = dbClient;
	}

	@Override
	public void onApplicationEvent(SystemEvent event) {
		Notification notification = getNotification(event);
		notification.setId(UUIDUtil.createId(Notification.class));
		dbClient.createObject(notification);
	}

	private Notification getNotification(SystemEvent event) {
		Notification notification = new Notification();
		notification.setType(event.getType().name());
		notification.setNotificationText(event.getType().getMessage());
		notification.setRecipientId(event.getRecipientId());
		notification.setRecipientType(event.getRecipientType().name());
		notification.setTimestamp(event.getTimestamp());
		notification.setLink(event.getLink());
		return notification;
	}

}
