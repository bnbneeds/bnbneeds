package com.bnbneeds.app.api.service.mapreduce;

import org.springframework.util.Assert;

import com.google.appengine.tools.mapreduce.MapJob;
import com.google.appengine.tools.mapreduce.MapReduceResult;
import com.google.appengine.tools.mapreduce.MapSettings;
import com.google.appengine.tools.pipeline.FutureValue;
import com.google.appengine.tools.pipeline.Job0;

public class IndexBuyerRecordsForDocumentSearchJob extends Job0<MapReduceResult<Void>> {

	private static final long serialVersionUID = 3740472460677231171L;

	private int shardCount;

	public IndexBuyerRecordsForDocumentSearchJob(int shardCount) {
		super();

		Assert.isTrue(shardCount > 0, "shardCount must be greater than zero.");
		this.shardCount = shardCount;
	}

	@Override
	public FutureValue<MapReduceResult<Void>> run() throws Exception {
		MapSettings settings = MapReduceUtils.getMapSettings();
		FutureValue<MapReduceResult<Void>> task = futureCall(
				new MapJob<>(IndexBuyerRecordsForDocumentSearchJobSpecification.jobSpec(shardCount), settings));
		return task;
	}
}
