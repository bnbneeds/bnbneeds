package com.bnbneeds.app.api.service.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.ProductCategory;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.CreateProductCategoryBulkParam;
import com.bnbneeds.app.model.product.CreateProductCategoryParam;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/product-categories")
public class ProductCategoryService extends ResourceMasterAdminService {

	private static final Logger logger = LoggerFactory
			.getLogger(ProductCategoryService.class);

	@RequestMapping(method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN
			+ "') and hasDealership()")
	public ResponseEntity<CreateResourceResponse> createProductCategory(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody CreateProductCategoryParam categoryParam) {

		logger.info("API request received to add a new product category: {}",
				categoryParam);

		ArgValidator.checkFieldNotEmpty(categoryParam.getName(), "name");

		QueryFields queryFields = new QueryFields();
		queryFields.add("name", categoryParam.getName());

		ProductCategory category = dbClient.queryObjectByFields(
				ProductCategory.class, queryFields);

		if (category != null) {
			logger.error("Product category with same name already exists: {}",
					categoryParam.getName());
			throw BadRequestException.nameAlreadyExists("Product category",
					categoryParam.getName());
		}

		category = new ProductCategory();
		String uid = UUIDUtil.createId(ProductCategory.class);
		category.setId(uid);
		category.setName(categoryParam.getName());
		String description = categoryParam.getDescription();
		if (!StringUtils.isEmpty(description)) {
			category.setDescription(description);
		}
		setStatusOfNewEntity(category, user);

		logger.debug("New product category record creating in DB: {}", category);

		dbClient.createObject(category);

		logger.info("New product category persisted in DB: {}", uid);
		
		recordEvent(SystemEventType.PRODUCT_CATEGORY_CREATED,
				RecipientType.ADMIN, uid, categoryParam.getName(), null,
				user.getUsername());

		return TaskResponseUtility.createResourceResponse(uid,
				Endpoint.PRODUCT_CATEGORY_INFO,
				"New product category created successfully.");
	}

	@RequestMapping(value = "/bulk", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN
			+ "') and hasDealership()")
	public ResponseEntity<TaskResponse> createProductCategories(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody CreateProductCategoryBulkParam bulkParam) {

		logger.info("API request received to add product categories: {}",
				bulkParam);

		ArgValidator.checkFieldNotNull(bulkParam, "create_product_categories");
		ArgValidator.checkFieldSize(bulkParam.getProductCategoryList(),
				"product_categories", MAX_ENTITIES_SIZE);

		DBOperationList dbOperationList = new DBOperationList();
		Set<String> namesInRequest = new HashSet<String>();
		for (Iterator<CreateProductCategoryParam> iterator = bulkParam
				.getProductCategoryList().iterator(); iterator.hasNext();) {
			CreateProductCategoryParam requestParam = iterator.next();
			String name = requestParam.getName();

			ArgValidator.checkFieldNotEmpty(name, "name");
			if (namesInRequest.contains(name)) {
				logger.error(
						"Product category with same name already exists in the request: {}",
						name);
				throw BadRequestException.nameAlreadyExists("Product category",
						name);
			}
			namesInRequest.add(name);
			QueryFields queryFields = new QueryFields("name", name);

			ProductCategory category = dbClient.queryObjectByFields(
					ProductCategory.class, queryFields);

			if (category != null) {
				logger.error(
						"Product category with same name already exists: {}",
						name);
				throw BadRequestException.nameAlreadyExists("Product category",
						name);
			}

			category = new ProductCategory();
			String uid = UUIDUtil.createId(ProductCategory.class);
			category.setId(uid);
			category.setName(name);
			String description = requestParam.getDescription();
			if (!StringUtils.isEmpty(description)) {
				category.setDescription(description);
			}
			setStatusOfNewEntity(category, user);
			dbOperationList.addOperation(OperationType.INSERT, category);
			
		}

		logger.debug("New product categories in a DB transaction: {}",
				dbOperationList);

		dbClient.transact(dbOperationList);
		
		recordEvent(SystemEventType.PRODUCT_CATEGORIES_CREATED,
				RecipientType.ADMIN, null, null, null,
				user.getUsername());

		String logMessage = "Task initiated to create product categories.";

		logger.info(logMessage);
		return TaskResponseUtility.createTaskSubmittedResponse(logMessage);
	}

	@Override
	public Class<ProductCategory> getDataModelClass() {
		return ProductCategory.class;
	}

}
