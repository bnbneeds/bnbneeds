package com.bnbneeds.app.api.service.mapreduce;

import java.util.List;

import org.springframework.util.Assert;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.tools.mapreduce.DatastoreMutationPool;
import com.google.appengine.tools.mapreduce.MapOnlyMapper;

public class IndexEntityPropertiesMapper extends MapOnlyMapper<Entity, Void> {

	private static final long serialVersionUID = 657105403454678131L;

	private transient DatastoreMutationPool batcher;

	private List<String> propertiesToBeIndexed;

	/**
	 * 
	 * @param propertiesToBeIndexed
	 *            the properties to be indexed
	 */
	public IndexEntityPropertiesMapper(List<String> propertiesToBeIndexed) {
		super();
		Assert.notEmpty(propertiesToBeIndexed, "propertiesToBeIndexed must not be empty.");
		this.propertiesToBeIndexed = propertiesToBeIndexed;
	}

	@Override
	public void beginSlice() {
		batcher = DatastoreMutationPool.create();
	}

	@Override
	public void endSlice() {
		batcher.flush();
	}

	@Override
	public void map(Entity value) {
		for (String property : propertiesToBeIndexed) {
			Object existingValue = value.getProperty(property);
			value.setIndexedProperty(property, existingValue);
		}
		batcher.put(value);
	}
}
