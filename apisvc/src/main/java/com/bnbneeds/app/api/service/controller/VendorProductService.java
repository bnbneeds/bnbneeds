package com.bnbneeds.app.api.service.controller;

import static com.bnbneeds.app.api.service.response.DBObjectMapper.toNamedRelatedListResponse;

import java.io.IOException;
import java.net.URI;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.exceptions.ResourceNotFoundException;
import com.bnbneeds.app.api.service.pipeline.PipelineUtils;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.BlobStoreUtility;
import com.bnbneeds.app.api.service.util.FileUploadConstants;
import com.bnbneeds.app.api.service.util.ProductUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.BlobObject;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.ProductCategory;
import com.bnbneeds.app.db.client.model.ProductName;
import com.bnbneeds.app.db.client.model.ProductType;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.UploadFormResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.CreateProductParam;
import com.bnbneeds.app.model.product.ProductListResponse;
import com.bnbneeds.app.model.product.UpdateProductParam;
import com.bnbneeds.app.util.StringUtils;
import com.google.appengine.api.blobstore.BlobInfo;

@RestController
@RequestMapping(value = "/vendors/{vendorId}/products")
public class VendorProductService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(VendorProductService.class);

	/**
	 * If validation succeeds, then a new instance of product is returned setting
	 * all the attributes of request payload
	 * 
	 * @param createProductParam
	 * @param vendorId
	 * @return instance of <code>Product</code>
	 * @throws BadRequestException
	 *             if validation fails
	 */
	private Product validateCreateProductParam(CreateProductParam createProductParam, String vendorId) {

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);

		ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, true);

		ArgValidator.checkFieldUriType(createProductParam.getProductNameId(), ProductName.class, "product_name_id");
		ArgValidator.checkFieldUriType(createProductParam.getProductCategoryId(), ProductCategory.class,
				"product_category_id");
		ArgValidator.checkFieldUriType(createProductParam.getProductTypeId(), ProductType.class, "product_type_id");

		ArgValidator.checkFieldNotEmpty(createProductParam.getCostPrice(), "cost_price");
		ArgValidator.checkFieldValue(Double.valueOf(createProductParam.getCostPrice()) > 0, "cost_price");

		String id = createProductParam.getProductNameId();

		ProductName productName = dbClient.queryObject(ProductName.class, id);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(productName, id, false);

		id = createProductParam.getProductCategoryId();

		ProductCategory productCategory = dbClient.queryObject(ProductCategory.class, id);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(productCategory, id, false);
		id = createProductParam.getProductTypeId();
		ProductType productType = dbClient.queryObject(ProductType.class, id);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(productType, id, false);

		// Validation succeeded.
		Product product = new Product();
		product.setProductName(productName);
		product.setVendor(vendor);
		product.setProductType(productType);
		product.setProductCategory(productCategory);
		product.setCostPrice(Double.valueOf(createProductParam.getCostPrice()));
		product.setDescription(createProductParam.getDescription());
		product.setServiceLocations(createProductParam.getServiceLocations());
		String videoURL = createProductParam.getVideoURL();
		if (StringUtils.hasText(videoURL)) {
			product.setVideoURL(videoURL);
		}
		String tutorialVideoURL = createProductParam.getTutorialVideoURL();
		if (StringUtils.hasText(tutorialVideoURL)) {
			product.setTutorialVideoURL(tutorialVideoURL);
		}
		product.setModelId(createProductParam.getModelId());
		Map<String, String> attributes = createProductParam.getAttributes();
		if (attributes != null && !attributes.isEmpty()) {
			product.setAttributes(attributes);
		}

		String productNativeId = ProductUtility.generateProductNativeId(vendor, product);

		Product tempProduct = dbClient.queryObjectByFields(Product.class, new QueryFields("nativeId", productNativeId));
		if (tempProduct != null) {
			logger.error("Product with same name already exists: {}", productName.getName());
			throw BadRequestException.nameAlreadyExists("Product", productName.getName());
		}
		product.setNativeId(productNativeId);

		return product;
	}

	/**
	 * Returns the product object if the attributes of the entity has changed, null
	 * otherwise
	 * 
	 * @param updateProductParam
	 * @param user
	 *            the logged in user
	 * @param vendorId
	 *            the vendorId of the product
	 * @param productId
	 *            the productId
	 * @return the product if there are new attributes, null otherwise
	 */
	private Product validateUpdateProductParam(UpdateProductParam updateProductParam, BNBNeedsUser user,
			String vendorId, String productId) {

		boolean nativeIdChanged = false;

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");
		ArgValidator.checkFieldNotNull(updateProductParam, "update_product");

		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, true);

		Product product = dbClient.queryObject(Product.class, productId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(product, productId, true);

		ProductUtility.checkProductOwnership(user, product);

		String productNameId = updateProductParam.getProductNameId();
		if (!StringUtils.isEmpty(productNameId)) {
			ArgValidator.checkFieldUriType(productNameId, ProductName.class, "product_name_id");
			ProductName productName = dbClient.queryObject(ProductName.class, productNameId);
			ArgValidator.checkEntityDisapprovedOrBlacklisted(productName, productNameId, false);
			product.setProductName(productName);
			nativeIdChanged = true;
		}

		String productCategoryId = updateProductParam.getProductCategoryId();
		if (!StringUtils.isEmpty(productCategoryId)) {
			ArgValidator.checkFieldUriType(productCategoryId, ProductCategory.class, "product_category_id");
			ProductCategory productCategory = dbClient.queryObject(ProductCategory.class, productCategoryId);
			ArgValidator.checkEntityDisapprovedOrBlacklisted(productCategory, productCategoryId, false);
			product.setProductCategory(productCategory);
			nativeIdChanged = true;
		}

		String productTypeId = updateProductParam.getProductTypeId();
		if (!StringUtils.isEmpty(productTypeId)) {
			ArgValidator.checkFieldUriType(productTypeId, ProductType.class, "product_type_id");
			ProductType productType = dbClient.queryObject(ProductType.class, productTypeId);
			ArgValidator.checkEntityDisapprovedOrBlacklisted(productType, productTypeId, false);
			product.setProductType(productType);
			nativeIdChanged = true;
		}

		String costPrice = updateProductParam.getCostPrice();
		if (!StringUtils.isEmpty(costPrice)) {
			double doubleCostPrice = Double.valueOf(costPrice);
			ArgValidator.checkFieldValue(doubleCostPrice > 0, "cost_price");
			product.setCostPrice(doubleCostPrice);
		}

		String modelId = updateProductParam.getModelId();
		if (!StringUtils.isEmpty(modelId)) {
			product.setModelId(modelId);
			nativeIdChanged = true;
		}

		if (nativeIdChanged) {
			product.setNativeId(ProductUtility.generateProductNativeId(vendor, product));
		}

		Map<String, String> attributes = updateProductParam.getAttributes();
		product.setAttributes(attributes);

		String description = updateProductParam.getDescription();
		if (!StringUtils.isEmpty(description)) {
			product.setDescription(description);
		}

		String serviceLocations = updateProductParam.getServiceLocations();
		product.setServiceLocations(serviceLocations);

		String videoURL = updateProductParam.getVideoURL();
		product.setVideoURL(videoURL);

		String tutorialVideoURL = updateProductParam.getTutorialVideoURL();
		product.setTutorialVideoURL(tutorialVideoURL);

		return product;
	}

	@RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasRole('" + Role.VENDOR + "') and isRelated(#vendorId)")
	public ResponseEntity<CreateResourceResponse> createProduct(@PathVariable("vendorId") String vendorId,
			@RequestBody CreateProductParam createProductParam) {

		logger.info("Request received to create new product for vendor: {}", vendorId);

		Product newProduct = validateCreateProductParam(createProductParam, vendorId);
		String productId = UUIDUtil.createId(Product.class);
		newProduct.setId(productId);

		dbClient.createObject(newProduct);

		URI uri = Endpoint.PRODUCT_INFO.get(productId);

		recordEvent(SystemEventType.NEW_PRODUCT_ADDED, RecipientType.ALL_BUYERS, productId,
				newProduct.getProductName().getName(), uri, newProduct.getVendor().getName());

		return TaskResponseUtility.createResourceResponse(productId, uri, "New product created successfully.");

	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{productId}", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "(hasRole('" + Role.VENDOR + "') and isRelated(#vendorId)) or (hasAnyRole('" + Role.ADMIN
			+ "', '" + Role.SUPER_ADMIN + "' )) ")
	public ResponseEntity<TaskResponse> updateProduct(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId, @PathVariable("productId") String productId,
			@RequestBody UpdateProductParam updateProductParam) {

		logger.info("Request received to update product for vendor: {}", vendorId);

		Product product = validateUpdateProductParam(updateProductParam, user, vendorId, productId);

		if (product == null) {
			throw BadRequestException.create("There was nothing to update in the product");
		}
		dbClient.updateObject(product);
		logger.info("Product: {} updated successfully.", productId);
		recordEvent(SystemEventType.PRODUCT_UPDATED, RecipientType.ALL_BUYERS, productId,
				product.getProductName().getName(), Endpoint.PRODUCT_INFO.get(product.getId()),
				product.getVendor().getName());

		return TaskResponseUtility.createTaskSuccessResponse("Product updated successfully.");

	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("(hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "' ) and hasDealership()) or (hasRole('"
			+ Role.ADMIN + "'))")
	public ProductListResponse getProductList(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId) {

		logger.info("Request received to retrieve product list for vendor id: {}", vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");

		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);

		ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);

		if (user.isVendor() && !vendorId.equals(user.getRelationshipId())) {
			logger.error(
					"Unauthorized access: Requested vendorId is not related to user logged in. Throw forbidden exception.");
			throw ForbiddenException.unauthorizedAccess();
		}

		QueryFields fields = new QueryFields("vendor", vendor);

		List<Product> productList = dbClient.queryList(Product.class, fields);
		ProductListResponse resp = new ProductListResponse();

		if (productList != null && !productList.isEmpty()) {
			resp.setVendorLink(vendorId);
			for (Iterator<Product> iterator = productList.iterator(); iterator.hasNext();) {
				Product product = iterator.next();
				product.setName(product.getProductName().getName());
			}
			resp.setNamedRelatedResourceList(
					toNamedRelatedListResponse(productList, null, new EntityStatus[] { EntityStatus.BLACKLISTED }));
		}

		return resp;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{productId}/images", consumes = {
			MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.IMAGE_JPEG_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CreateResourceResponse> uploadProductImage(
			/*
			 * TODO Security breach? Because the blobstore URL will be out of the app scope
			 * and it will not be having user authentication. But I still think its safe,
			 * unless anything unusual happens. Because image will already be uploaded when
			 * this method is invoked.
			 */
			// @AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId, @PathVariable("productId") String productId)
			throws IOException, ServletException {

		logger.info("Request received to upload image of product id: {},  belonging to vendor id: {}", productId,
				vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");

		Product product = dbClient.queryObject(Product.class, productId);

		ArgValidator.checkEntityDisapprovedOrBlacklisted(product, productId, true);
		ArgValidator.checkFieldValue(vendorId.equals(product.getVendor().getId()), "vendor_id");
		ArgValidator.checkEntityDisapprovedOrBlacklisted(product.getVendor(), product.getVendor().getId(), true);

		// ProductUtility.checkProductOwnership(user, product);

		Set<BlobObject> imageKeys = product.getImages();
		if (imageKeys != null && imageKeys.size() == 4) {
			throw BadRequestException.imageUploadsExceeded("four");

		}

		BlobInfo fileInfo = BlobStoreUtility.getUploadedBlobKey(httpRequest,
				FileUploadConstants.IMAGE_FILE_FORM_ELEMENT_NAME);

		if (fileInfo == null) {
			throw BadRequestException.create("No image file selected for upload.");
		}

		String blobKey = fileInfo.getBlobKey().getKeyString();
		product.addImage(fileInfo.getContentType(), blobKey, fileInfo.getSize());

		dbClient.updateObject(product);

		return TaskResponseUtility.createResourceResponse(blobKey, Endpoint.PRODUCT_IMAGE.get(productId, blobKey),
				"New image for the product uploaded successfully");

	}

	@RequestMapping(method = RequestMethod.POST, value = "/{productId}/deactivate", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasAnyRole('" + Role.VENDOR + "','" + Role.ADMIN + "') and isRelated(#vendorId)")
	public ResponseEntity<CreateResourceResponse> deactivateProduct(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId, @PathVariable("productId") String productId,
			@RequestParam(name = REQ_PARAM_FORCE_DELETE, defaultValue = "false") String forceDelete) {

		logger.info("Request received to delete product id: {},  belonging to vendor id: {}", productId, vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");

		Product product = dbClient.queryObject(Product.class, productId);

		if (user.isAdmin()) {
			ArgValidator.checkEntityApproved(product, productId, true);
		} else {
			// Vendor
			ArgValidator.checkEntityDisapprovedOrBlacklisted(product, productId, true);
			ArgValidator.checkEntityDisapprovedOrBlacklisted(product.getVendor(), product.getVendor().getId(), true);
		}

		ArgValidator.checkFieldValue(vendorId.equals(product.getVendor().getId()), "vendor_id");

		ProductUtility.checkProductOwnership(user, product);

		String jobId = ProductUtility.deleteProduct(productId, true, Boolean.valueOf(forceDelete));
		return PipelineUtils.createPipelineJobInfoResponse(jobId,
				"Task submitted to delete product with ID: " + productId);

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{productId}/images/{imageId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasRole('" + Role.VENDOR + "') and isRelated(#vendorId)")
	public ResponseEntity<TaskResponse> deleteProductImage(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId, @PathVariable("productId") String productId,
			@PathVariable("imageId") String imageKey) {

		logger.info("Request received to delete image with id: {}, of product id: {},  belonging to vendor id: {}",
				imageKey, productId, vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");
		ArgValidator.checkFieldNotEmpty(imageKey, "image_key");

		Product product = dbClient.queryObject(Product.class, productId);

		ArgValidator.checkEntityDisapprovedOrBlacklisted(product, productId, true);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(product.getVendor(), product.getVendor().getId(), true);
		ArgValidator.checkFieldValue(vendorId.equals(product.getVendor().getId()), "vendor_id");

		ProductUtility.checkProductOwnership(user, product);

		if (!product.containsImageKey(imageKey)) {
			throw ResourceNotFoundException.idNotFound(imageKey);
		}

		BlobStoreUtility.deleteBlob(imageKey);

		product.removeImage(imageKey);

		dbClient.updateObject(product);

		return TaskResponseUtility
				.createTaskSuccessResponse("Product image with id: [" + imageKey + "] was deleted successfully.");

	}

	@RequestMapping(method = RequestMethod.GET, value = "/{productId}/upload-image-form", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "') and isRelated(#vendorId)")
	public UploadFormResponse getProductImageUploadFormElement(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId, @PathVariable("productId") String productId) {

		final URI UPLOAD_IMAGE_ACTION_URL = Endpoint.VENDOR_PRODUCT_IMAGES.get(vendorId, productId);

		logger.info("Request received to get upload image form element for product id: {},  belonging to vendor id: {}",
				productId, vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");

		Product product = dbClient.queryObject(Product.class, productId);

		ArgValidator.checkEntityDisapprovedOrBlacklisted(product, productId, true);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(product.getVendor(), product.getVendor().getId(), true);

		ProductUtility.checkProductOwnership(user, product);

		Set<BlobObject> imageKeys = product.getImages();
		if (imageKeys != null && imageKeys.size() == 4) {
			logger.error("Cannot upload more than four images for the product.");
			throw BadRequestException.imageUploadsExceeded("four");
		}

		UploadFormResponse uploadForm = new UploadFormResponse();
		String uploadURL = BlobStoreUtility.createUploadURL(UPLOAD_IMAGE_ACTION_URL);
		uploadForm.setActionURL(uploadURL);
		uploadForm.setInputFileElementName(FileUploadConstants.IMAGE_FILE_FORM_ELEMENT_NAME);
		uploadForm.setAcceptedFileTypes(FileUploadConstants.IMAGE_FILE_ACCEPTED_TYPES);
		return uploadForm;
	}

}
