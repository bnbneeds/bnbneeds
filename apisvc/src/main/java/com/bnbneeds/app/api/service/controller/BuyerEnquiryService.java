package com.bnbneeds.app.api.service.controller;

import static com.bnbneeds.app.api.service.response.DBObjectMapper.toNamedRelatedRestResponse;

import java.net.URI;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.ProductResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.Enquiry;
import com.bnbneeds.app.db.client.model.Enquiry.LikelyToBuyIn;
import com.bnbneeds.app.db.client.model.Enquiry.LikelyToBuyIn.TimeUnit;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.messaging.email.MessageKeys;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.CreateEnquiryParam;
import com.bnbneeds.app.model.dealer.EnquiryInfoListResponse;
import com.bnbneeds.app.model.dealer.EnquiryInfoResponse;
import com.bnbneeds.app.model.dealer.LikelyToBuyInParam;
import com.bnbneeds.app.model.dealer.UpdateEnquiryParam;
import com.bnbneeds.app.model.dealer.VendorListResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.util.DateUtils;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/buyers/{buyerId}/product-enquiries")
public class BuyerEnquiryService extends ResourceService {

	private static final Logger logger = LoggerFactory
			.getLogger(BuyerEnquiryService.class);

	private static final String REQ_PARAM_ENQUIRY_DATE = "enquiryDate";
	private static final String REQ_PARAM_START_ENQUIRY_DATE = "startEnquiryDate";
	private static final String REQ_PARAM_END_ENQUIRY_DATE = "endEnquiryDate";

	@RequestMapping(method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and isRelated(#buyerId)")
	public ResponseEntity<CreateResourceResponse> createEnquiry(
			@PathVariable("buyerId") String buyerId,
			@RequestBody CreateEnquiryParam param) {

		logger.info("Request received to create enquiry: {}", param);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		String productId = param.getProductId();
		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");
		String description = param.getDescription();
		String requiredQuantity = param.getRequiredQuantity();
		ArgValidator.checkFieldNotEmpty(requiredQuantity, "required_quantity");
		LikelyToBuyInParam likelyToBuyIn = param.getLikelyToBuyIn();
		ArgValidator.checkFieldNotNull(likelyToBuyIn, "likely_to_buy_in");
		Integer likelyToBuyInValue = likelyToBuyIn.getValue();
		ArgValidator.checkFieldNotNull(likelyToBuyInValue, "value");
		ArgValidator.checkFieldValue(likelyToBuyInValue > 0, "value");
		String timeUnit = likelyToBuyIn.getTimeUnit();
		ArgValidator.checkFieldValueFromEnum(timeUnit, "time_unit",
				TimeUnit.class);
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);

		Product product = dbClient.queryObject(Product.class, productId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(product, productId,
				false);
		Vendor vendor = product.getVendor();
		ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor,
				vendor.getId(), false);

		Date date = new Date();
		String dateToday = DATE_FORMAT.format(date);
		String nativeId = Enquiry.generateNativeId(dateToday, buyerId,
				productId);

		QueryFields fields = new QueryFields("nativeId", nativeId);
		Enquiry enquiry = dbClient.queryObjectByFields(Enquiry.class, fields);
		if (enquiry != null) {
			throw BadRequestException.create("Enquiry already exists: buyer [ "
					+ buyer.getName() + " ], product [ "
					+ product.getProductName().getName() + " ], date [ "
					+ dateToday + " ]");
		}

		String uid = UUIDUtil.createId(Enquiry.class);
		enquiry = new Enquiry();
		enquiry.setId(uid);
		enquiry.setBuyer(buyer);
		enquiry.setProduct(product);
		enquiry.setNativeId(nativeId);
		enquiry.setEnquiryTimestamp(date.getTime());
		enquiry.setRequiredQuantity(requiredQuantity);
		enquiry.setLikelyToBuyIn(new LikelyToBuyIn(likelyToBuyInValue, timeUnit));
		if (StringUtils.hasText(description)) {
			enquiry.setDescription(description);
		}
		enquiry.setEntityStatus(EntityStatus.APPROVED);
		dbClient.createObject(enquiry);

		logger.info("Enquiry created in DB: {}", enquiry);

		URI uri = Endpoint.BUYER_PRODUCT_ENQUIRY_INFO.get(buyerId, uid);

		String vendorId = vendor.getId();
		recordEvent(SystemEventType.NEW_PRODUCT_ENQUIRY_POSTED,
				RecipientType.VENDOR, vendorId, uid, product.getProductName()
						.getName(), uri, buyer.getName());

		UserAccount vendorUserAccount = loginHelper
				.getUserAccountByRelationshipId(vendorId);

		String[] recipients = new String[] { vendorUserAccount.getName(),
				vendor.getEmailAddress() };
		String[] mailBodyPlaceHolderValues = new String[] { vendor.getName() };
		emailHelper.send(MessageKeys.PRODUCT_ENQUIRY_POSTED,
				mailBodyPlaceHolderValues, recipients);

		return TaskResponseUtility.createResourceResponse(uid, uri,
				"Product enquiry posted successfully");

	}

	@RequestMapping(method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER
			+ "') and hasDealership() and isRelated(#buyerId)")
	public EnquiryInfoListResponse getEnquiryList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info(
				"Request received to get enquiries by filter: {}, fetchSize: {}, offset: {} of buyer {}",
				filter, fetchSize, offset, buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");

		final String[] QUERY_FILTER_KEYS = { "name", "status", "vendorId",
				REQ_PARAM_ENQUIRY_DATE, REQ_PARAM_START_ENQUIRY_DATE,
				REQ_PARAM_END_ENQUIRY_DATE };

		EnquiryInfoListResponse response = new EnquiryInfoListResponse();

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields();

		fields.add("buyer", dbClient.queryObject(Buyer.class, buyerId));

		boolean requestDateGiven = false;

		for (String key : keyValueMap.keySet()) {

			String value = keyValueMap.get(key);

			switch (key) {
			/*
			 * Commented this case because we cannot use IN operator for
			 * cursors. We will uncomment this part of code once Google adds
			 * support for it.
			 * 
			 * Therefore when name is given, then we need to filter the list
			 * manually. That is done after the switch block.
			 * 
			 * case "name": ProductName productNameEntity =
			 * dbClient.queryObjectByFields( ProductName.class, new
			 * QueryFields("name", keyValueMap.get(key)));
			 * 
			 * if (productNameEntity != null) { List<Product> productList =
			 * dbClient.queryList( Product.class, new QueryFields("productName",
			 * productNameEntity));
			 * 
			 * if (productList != null && !productList.isEmpty()) {
			 * fields.add("product in", productList); }
			 * 
			 * 
			 * } break;
			 */
			case "status":
				ArgValidator.checkFieldValue(!EntityStatus.BLACKLISTED.name()
						.equals(value.toUpperCase()), key);
				fields.add("entityStatus", value.toUpperCase());
				break;
			case "vendorId":
				ArgValidator.checkFieldUriType(value, Vendor.class, key);
				break;
			case REQ_PARAM_ENQUIRY_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("enquiryTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("enquiryTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_ENQUIRY_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_ENQUIRY_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				fields.add("enquiryTimestamp >=", startRequestDate.getTime());
				break;
			case REQ_PARAM_END_ENQUIRY_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_ENQUIRY_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils
						.getEndOfDayTimestamp(endRequestDate);
				fields.add("enquiryTimestamp <=", endTimestamp2);
				break;
			default:
				break;
			}

		}

		ResultSetIterator<Enquiry> resultSetIterator = null;
		String queryByProductNameValue = keyValueMap.get("name");
		String queryByVendorIdValue = keyValueMap.get("vendorId");

		do {
			resultSetIterator = getResultSetIterator(Enquiry.class, fields,
					offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					Enquiry item = resultSetIterator.next();

					if (StringUtils.hasText(queryByProductNameValue)) {
						if (!queryByProductNameValue.equals(item.getProduct()
								.getProductName().getName())) {
							continue;
						}
					}

					if (item != null && !item.isBlacklisted()
							&& !item.getBuyer().isBlacklisted()
							&& !item.getProduct().isBlacklisted()
							&& !item.getProduct().getVendor().isBlacklisted()) {

						if (StringUtils.hasText(queryByVendorIdValue)
								&& !queryByVendorIdValue.equals(item
										.getProduct().getVendor().getId())) {
							logger.info(
									"Queried vendor Id {} does not match with product's vendor id: {}",
									queryByVendorIdValue, item.getProduct()
											.getVendor().getId());
							continue;

						}

						EnquiryInfoResponse infoResponse = ProductResponseMapper
								.mapProductEnquiry(item, DATE_FORMAT);

						response.addEnquiryInfoResponse(infoResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null
						&& offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}
		} while (response.size() < fetchSize);
		return response;

	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER
			+ "') and hasDealership() and isRelated(#buyerId)")
	public EnquiryInfoResponse getEnquiry(
			@PathVariable("buyerId") String buyerId,
			@PathVariable("id") String enquiryId) {

		logger.info(
				"Request received to retrive enquiry details of {} belonging to buyer {}",
				enquiryId, buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(enquiryId, Enquiry.class, "enquiry_id");

		Enquiry enquiry = dbClient.queryObject(Enquiry.class, enquiryId);

		ArgValidator.checkEntityBlacklisted(enquiry, enquiryId, true);
		ArgValidator.checkEntityBlacklisted(enquiry.getBuyer(), enquiry
				.getBuyer().getId(), true);
		Product product = enquiry.getProduct();
		ArgValidator.checkEntityBlacklisted(product, product.getId(), false);
		ArgValidator.checkEntityBlacklisted(product.getVendor(), product
				.getVendor().getId(), false);

		if (!buyerId.equals(enquiry.getBuyer().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}
		EnquiryInfoResponse resp = new EnquiryInfoResponse();
		if (enquiry != null) {
			ProductResponseMapper.mapProductEnquiry(enquiry, DATE_FORMAT, resp);

		}
		return resp;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER
			+ "') and hasDealership() and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> updateEnquiry(
			@PathVariable("buyerId") String buyerId,
			@PathVariable("id") String enquiryId,
			@RequestBody UpdateEnquiryParam param) {

		logger.info("Request received to update enquiry {} of buyer {}",
				enquiryId, buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(enquiryId, Enquiry.class, "enquiry_id");

		Enquiry enquiry = dbClient.queryObject(Enquiry.class, enquiryId);

		ArgValidator.checkEntityDisapprovedOrBlacklisted(enquiry, enquiryId,
				true);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(enquiry.getBuyer(),
				enquiry.getBuyer().getId(), true);

		Product product = enquiry.getProduct();
		ArgValidator.checkEntityDisapprovedOrBlacklisted(product,
				product.getId(), false);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(product.getVendor(),
				product.getVendor().getId(), false);

		if (!buyerId.equals(enquiry.getBuyer().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		ArgValidator.checkFieldNotNull(param, "update_enquiry");
		boolean changed = false;
		String requiredQuantity = param.getRequiredQuantity();
		if (!StringUtils.isEmpty(requiredQuantity)) {
			enquiry.setRequiredQuantity(requiredQuantity);
			changed = true;
		}
		String description = param.getDescription();
		if (!StringUtils.isEmpty(description)) {
			enquiry.setDescription(description);
			changed = true;
		}

		LikelyToBuyInParam likelyToBuyParam = param.getLikelyToBuyIn();
		if (likelyToBuyParam != null) {
			LikelyToBuyIn likelyToBuyIn = enquiry.getLikelyToBuyIn();
			Integer likelyToBuyValue = likelyToBuyParam.getValue();
			if (likelyToBuyValue != null) {
				ArgValidator.checkFieldValue(likelyToBuyValue > 0, "value");
				likelyToBuyIn.setValue(likelyToBuyValue);
				changed = true;
			}
			String timeUnit = likelyToBuyParam.getTimeUnit();
			if (StringUtils.hasText(timeUnit)) {
				ArgValidator.checkFieldValueFromEnum(timeUnit, "time_unit",
						TimeUnit.class);
				likelyToBuyIn.setTimeUnit(timeUnit);
				changed = true;
			}
		}

		if (changed) {
			dbClient.updateObject(enquiry);
		}
		String vendorId = enquiry.getProduct().getVendor().getId();
		recordEvent(
				SystemEventType.PRODUCT_ENQUIRY_UPDATED,
				RecipientType.VENDOR,
				enquiry.getProduct().getVendor().getId(),
				enquiry.getId(),
				enquiry.getProduct().getProductName().getName(),
				Endpoint.VENDOR_PRODUCT_ENQUIRY_INFO.get(vendorId,
						enquiry.getId()), enquiry.getBuyer().getName());
		return TaskResponseUtility
				.createTaskSuccessResponse("Enquiry updated successfully");
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER
			+ "') and hasDealership() and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> deleteEnquiry(
			@PathVariable("buyerId") String buyerId,
			@PathVariable("id") String enquiryId) {

		logger.info("Request received to delete enquiry {} of buyer {}",
				enquiryId, buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(enquiryId, Enquiry.class, "enquiry_id");

		Enquiry enquiry = dbClient.queryObject(Enquiry.class, enquiryId);

		ArgValidator.checkEntityDisapprovedOrBlacklisted(enquiry.getBuyer(),
				enquiry.getBuyer().getId(), true);

		ArgValidator.checkEntityDisapprovedOrBlacklisted(enquiry, enquiryId,
				true);

		Product product = enquiry.getProduct();
		ArgValidator.checkEntityDisapprovedOrBlacklisted(product,
				product.getId(), false);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(product.getVendor(),
				product.getVendor().getId(), false);

		if (!buyerId.equals(enquiry.getBuyer().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		dbClient.markAsDeleted(enquiry);

		recordEvent(SystemEventType.PRODUCT_ENQUIRY_DELETED,
				RecipientType.VENDOR, enquiry.getProduct().getVendor().getId(),
				enquiry.getId(), enquiry.getProduct().getProductName()
						.getName(), null, enquiry.getBuyer().getName());

		return TaskResponseUtility
				.createTaskSuccessResponse("Enquiry deleted successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, path = "/associated-vendors", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER
			+ "') and hasDealership() and isRelated(#buyerId)")
	public VendorListResponse getAssociatedVendorList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			@RequestParam(name = REQ_PARAM_FILTER) String filter) {

		logger.info(
				"Request received to get associated vendors of product enquiries made by buyer with Id: {}",
				buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);

		ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);

		QueryFields fields = new QueryFields("buyer", buyer);

		final String[] QUERY_FILTER_KEYS = { REQ_PARAM_ENQUIRY_DATE,
				REQ_PARAM_START_ENQUIRY_DATE, REQ_PARAM_END_ENQUIRY_DATE };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);

		boolean requestDateGiven = false;

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case REQ_PARAM_ENQUIRY_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("enquiryTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("enquiryTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_ENQUIRY_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_ENQUIRY_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				fields.add("enquiryTimestamp >=", startRequestDate.getTime());
				break;
			case REQ_PARAM_END_ENQUIRY_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_ENQUIRY_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils
						.getEndOfDayTimestamp(endRequestDate);
				fields.add("enquiryTimestamp <=", endTimestamp2);
				break;
			default:
				break;
			}
		}

		List<Enquiry> enquiryList = dbClient.queryList(Enquiry.class, fields);

		VendorListResponse vendorListResponse = new VendorListResponse();
		Set<String> uriSet = new HashSet<String>();
		for (Iterator<Enquiry> iterator = enquiryList.iterator(); iterator
				.hasNext();) {
			Enquiry enquiry = iterator.next();
			if (enquiry.isBlacklisted()) {
				continue;
			}

			Product product = enquiry.getProduct();
			if (product.isBlacklisted()) {
				continue;
			}

			Vendor vendor = enquiry.getProduct().getVendor();
			if (vendor.isBlacklisted()) {
				continue;
			}

			if (uriSet.add(vendor.getId())) {
				vendorListResponse
						.addNamedResourceResponse(toNamedRelatedRestResponse(vendor));
			}
		}
		logger.info("Returning vendor list of size: {}.",
				vendorListResponse.size());

		return vendorListResponse;
	}
}
