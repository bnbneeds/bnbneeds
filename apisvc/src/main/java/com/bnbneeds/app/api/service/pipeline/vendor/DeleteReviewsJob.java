package com.bnbneeds.app.api.service.pipeline.vendor;

import static com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT.queryVendor;
import static com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT.vendorField;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryObjectByFields;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.db.client.model.VendorAverageRating;
import com.bnbneeds.app.db.client.model.VendorReview;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeleteReviewsJob extends Job2<Void, String, Boolean> {

	private static final long serialVersionUID = -4992567439945486643L;
	private static final Logger logger = LoggerFactory.getLogger(DeleteReviewsJob.class);

	@Override
	public Value<Void> run(String vendorId, Boolean forceDelete) throws Exception {
		Vendor vendor = queryVendor(vendorId);
		if (vendor == null) {
			return null;
		}

		List<VendorReview> vendorReviews = queryList(VendorReview.class, vendorField(vendor));
		if (!CollectionUtils.isEmpty(vendorReviews)) {
			if (!forceDelete) {
				throw DeleteDealerJob.cannotDeleteHasReferences(vendorId, Vendor.class, VendorReview.class);
			}

			DBOperationList vendorReviewDeleteOperationList = new DBOperationList();

			logger.info("Delete reviews of the vendor [{}]...", vendorId);
			for (Iterator<VendorReview> iterator = vendorReviews.iterator(); iterator.hasNext();) {
				VendorReview vendorReview = iterator.next();
				vendorReviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, vendorReview);
			}

			VendorAverageRating vendorAverageRating = queryObjectByFields(VendorAverageRating.class,
					vendorField(vendor));
			if (vendorAverageRating != null) {
				logger.debug("Delete the vendor average rating.");
				vendorReviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, vendorAverageRating);
			}
			transact(vendorReviewDeleteOperationList);
		}
		return null;
	}

}
