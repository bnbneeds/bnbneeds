package com.bnbneeds.app.api.service.response;

import com.bnbneeds.app.db.client.model.BusinessType;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.Dealer;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.dealer.BuyerInfoResponse;
import com.bnbneeds.app.model.dealer.DealerInfoResponse;
import com.bnbneeds.app.model.dealer.VendorInfoResponse;

public class DealerResponseMapper extends DataObjectResponseMapper {

	public static void mapDealer(Dealer from, DealerInfoResponse to) {
		mapDescribedDataObject(from, to);
		to.setAddress(from.getAddress());
		to.setContactName(from.getContactName());
		to.setMobileNumber(from.getMobileNumber());
		to.setEmailAddress(from.getEmailAddress());
		to.setPan(from.getPan());
		to.setWebsite(from.getWebsite());
	}

	public static void mapVendor(Vendor from, VendorInfoResponse to) {
		mapDealer(from, to);
		to.setVatNumber(from.getVatNumber());
		to.setServiceTaxNumber(from.getServiceTaxNumber());
		to.setCities(from.getCities());
	}

	public static VendorInfoResponse mapVendor(Vendor from) {
		VendorInfoResponse to = new VendorInfoResponse();
		mapVendor(from, to);
		return to;
	}

	public static void mapBuyer(Buyer from, BuyerInfoResponse to) {
		mapDealer(from, to);
		BusinessType businessType = from.getBusinessType();
		to.setBusinessType(businessType.getId(), businessType.getName());
	}

	public static BuyerInfoResponse mapBuyer(Buyer from) {
		BuyerInfoResponse to = new BuyerInfoResponse();
		mapBuyer(from, to);
		return to;
	}

}
