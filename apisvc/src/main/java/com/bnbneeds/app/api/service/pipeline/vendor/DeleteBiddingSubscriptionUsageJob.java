package com.bnbneeds.app.api.service.pipeline.vendor;

import static com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT.queryVendor;
import static com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT.vendorField;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscriptionUsage;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeleteBiddingSubscriptionUsageJob extends Job2<Void, String, Boolean> {

	private static final long serialVersionUID = 2196487865805515491L;
	private static final Logger logger = LoggerFactory.getLogger(DeleteBiddingSubscriptionUsageJob.class);

	@Override
	public Value<Void> run(String vendorId, Boolean forceDelete) throws Exception {
		Vendor vendor = queryVendor(vendorId);
		if (vendor == null) {
			return null;
		}

		List<VendorBiddingSubscriptionUsage> biddingSubscriptionUsages = queryList(VendorBiddingSubscriptionUsage.class,
				vendorField(vendor));

		if (!CollectionUtils.isEmpty(biddingSubscriptionUsages)) {
			if (!forceDelete) {
				throw DeleteDealerJob.cannotDeleteHasReferences(vendorId, Vendor.class,
						VendorBiddingSubscriptionUsage.class);
			}

			logger.info("Get the bidding subscription usages of the vendor and delete them...");

			DBOperationList operationList = new DBOperationList();

			for (Iterator<VendorBiddingSubscriptionUsage> iterator = biddingSubscriptionUsages.iterator(); iterator
					.hasNext();) {
				VendorBiddingSubscriptionUsage vendorBiddingSubscriptionUsage = iterator.next();
				operationList.addOperation(OperationType.MARK_AS_DELETED, vendorBiddingSubscriptionUsage);
			}

			transact(operationList);
		}
		return null;
	}

}
