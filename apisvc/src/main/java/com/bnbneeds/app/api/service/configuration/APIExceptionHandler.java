package com.bnbneeds.app.api.service.configuration;

import javax.servlet.ServletException;

import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import com.bnbneeds.app.api.service.exceptions.APIException;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.exceptions.ResourceNotFoundException;
import com.bnbneeds.app.api.service.exceptions.VerificationRequestFailedException;
import com.bnbneeds.app.model.OperationStatus;
import com.bnbneeds.app.model.TaskResponse;

@ControllerAdvice
@EnableWebMvc
public class APIExceptionHandler {

	private ResponseEntity<TaskResponse> respond(TaskResponse task) {

		return new ResponseEntity<TaskResponse>(task, HttpStatus.valueOf(task
				.getHttpCode()));
	}

	/*
	 * 400 - BadRequest
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	@ExceptionHandler({ MissingServletRequestPartException.class,
			MissingServletRequestParameterException.class,
			MethodArgumentNotValidException.class,
			HttpMessageNotReadableException.class, BindException.class,
			TypeMismatchException.class, BadRequestException.class,
			NumberFormatException.class,
			VerificationRequestFailedException.class })
	public TaskResponse handleBadRequestException(Exception ex) {

		TaskResponse response = new TaskResponse(
				HttpStatus.BAD_REQUEST.value(), OperationStatus.ERROR,
				ex.getMessage(), ex.getClass().getSimpleName());
		return response;
		// return respond(response);

	}

	/*
	 * 404 - NotFound
	 */
	@SuppressWarnings("deprecation")
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	@ExceptionHandler({ NoSuchRequestHandlingMethodException.class,
			NoHandlerFoundException.class, ResourceNotFoundException.class })
	public TaskResponse handleResourceNotFoundException(Exception ex) {

		TaskResponse response = new TaskResponse(HttpStatus.NOT_FOUND.value(),
				OperationStatus.ERROR, ex.getMessage(), ex.getClass()
						.getSimpleName());
		return response;

	}

	/*
	 * 405 Method Not Allowed
	 */
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	@ResponseBody
	@ExceptionHandler({ HttpRequestMethodNotSupportedException.class })
	public TaskResponse handleMethodNotSupportedException(Exception ex) {

		TaskResponse response = new TaskResponse(
				HttpStatus.METHOD_NOT_ALLOWED.value(), OperationStatus.ERROR,
				ex.getMessage(), ex.getClass().getSimpleName());
		return response;

	}

	/*
	 * 500 Internal Server Error
	 */
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	@ExceptionHandler({ ConversionNotSupportedException.class,
			HttpMessageNotWritableException.class, ServletException.class,
			APIException.class })
	public TaskResponse handleInternalServerError(Exception ex) {

		TaskResponse response = new TaskResponse(
				HttpStatus.INTERNAL_SERVER_ERROR.value(), OperationStatus.FAIL,
				ex.getMessage(), ex.getClass().getSimpleName());
		return response;

	}

	/*
	 * 415 Unsupported Media Type.
	 */
	@ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
	@ResponseBody
	@ExceptionHandler({ HttpMediaTypeNotSupportedException.class })
	public TaskResponse handleMediaTypeNotSupportedException(Exception ex) {

		TaskResponse response = new TaskResponse(
				HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(),
				OperationStatus.ERROR, ex.getMessage(), ex.getClass()
						.getSimpleName());
		return response;

	}

	/*
	 * 406 - Not Acceptable
	 */
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
	@ResponseBody
	@ExceptionHandler({ HttpMediaTypeNotAcceptableException.class })
	public TaskResponse handleMediaTypeNotAcceptedException(Exception ex) {

		TaskResponse response = new TaskResponse(
				HttpStatus.NOT_ACCEPTABLE.value(), OperationStatus.ERROR,
				ex.getMessage(), ex.getClass().getSimpleName());
		return response;

	}

	/*
	 * 403 - Forbidden
	 */
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	@ExceptionHandler({ ForbiddenException.class })
	public TaskResponse handleForbiddenException(Exception ex) {

		TaskResponse response = new TaskResponse(HttpStatus.FORBIDDEN.value(),
				OperationStatus.ERROR, ex.getMessage(), ex.getClass()
						.getSimpleName());
		return response;

	}

}
