package com.bnbneeds.app.api.service.util;

public interface FileUploadConstants {

	String IMAGE_FILE_FORM_ELEMENT_NAME = "imageFile";
	String IMAGE_FILE_ACCEPTED_TYPES = ".jpg,.png";
}
