package com.bnbneeds.app.api.service.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.DBObjectMapper;
import com.bnbneeds.app.api.service.response.TenderResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.BiddingSubscriptionUtility;
import com.bnbneeds.app.api.service.util.NotificationUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.api.service.util.TenderUtility;
import com.bnbneeds.app.db.client.OrderBy;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.BiddingSubscriptionPlan.PlanType;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription.SubscriptionState;
import com.bnbneeds.app.db.client.model.Notification;
import com.bnbneeds.app.db.client.model.Tender;
import com.bnbneeds.app.db.client.model.TenderBid;
import com.bnbneeds.app.db.client.model.TenderProduct;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscription;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.tender.CreateTenderBidParam;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderPreviewListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderPreviewResponse;
import com.bnbneeds.app.model.dealer.tender.TenderProductInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderProductInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.ProductNameListResponse;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/tenders")
public class TenderBidService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(TenderBidService.class);

	/**
	 * Checks for subscription validity of the given vendor
	 * 
	 * @param vendor
	 *            the vendor
	 * @return the vendor bidding subscription
	 * @throws ForbiddenException
	 *             in any of these cases:
	 *             <ol>
	 *             <li>No ACTIVE subscription exists.</li>
	 *             <li>Subscription is expired.</li>
	 *             <li>Quota usage is full or exceeded.</li>
	 */
	private VendorBiddingSubscription verifySubscriptionValidity(Vendor vendor) {
		QueryFields subscriptionQueryfields = new QueryFields("vendor", vendor);
		subscriptionQueryfields.add("subscriptionState", SubscriptionState.ACTIVE.name());
		VendorBiddingSubscription subscription = dbClient.queryObjectByFields(VendorBiddingSubscription.class,
				subscriptionQueryfields);
		if (subscription == null) {
			logger.error("No active bidding subscription exists for vendor: {}", vendor.getId());
			throw ForbiddenException.dealerSubscriptionDoesNotExist("Vendor", vendor.getName());
		}
		boolean subscriptionQuotaFull = false;
		boolean subscriptionExpiryDateCrossed = false;
		PlanType planType = subscription.getSubscriptionPlanType();
		switch (planType) {
		case QUOTA:
			if (subscription.isQuotaUsageFullOrExceeded()) {
				logger.info("Subscription quota is full for subscription: {}", subscription.getId());
				subscriptionQuotaFull = true;
			}
			break;
		case DURATION:
			long expiryTimestamp = subscription.getExpiryTimestamp();
			long currentTimestamp = System.currentTimeMillis();
			if (currentTimestamp >= expiryTimestamp) {
				logger.info("Subscription expiry timestamp has crossed for subscription: {}", subscription.getId());
				subscriptionExpiryDateCrossed = true;
			}
			break;
		default:
			break;
		}

		if (subscriptionQuotaFull || subscriptionExpiryDateCrossed) {
			subscription.setSubscriptionState(SubscriptionState.EXPIRED);
			DBOperationList opList = new DBOperationList();
			opList.addOperation(OperationType.UPDATE, subscription);
			logger.info("Setting the bidding subscription to EXPIRED: {}", subscription.getId());
			Notification notification = NotificationUtility.createNotification(
					SystemEventType.VENDOR_BIDDING_SUBSCRIPTION_EXPIRED, RecipientType.VENDOR, vendor.getId(),
					subscription.getId(), null,
					Endpoint.VENDOR_BIDDING_SUBSCRIPTION_INFO.get(vendor.getId(), subscription.getId()), "SYSTEM");
			opList.addOperation(OperationType.INSERT, notification);
			dbClient.transact(opList);

			if (subscriptionQuotaFull) {
				throw ForbiddenException.dealerSubscriptionQuotaExceeded("Vendor", vendor.getName());
			}

			if (subscriptionExpiryDateCrossed) {
				long expiryTimestamp = subscription.getExpiryTimestamp();
				String expiryTimestampText = StringUtils.getFormattedDate(expiryTimestamp, TIMESTAMP_FORMAT);
				throw ForbiddenException.dealerSubscriptionDueToBeMarkedExpired(expiryTimestampText);
			}
		}

		return subscription;
	}

	private void forbidIfTenderIsNotAccessed(Vendor vendor, Tender tender) {
		boolean accessed = BiddingSubscriptionUtility.isTenderAccessed(dbClient, vendor, tender);
		logger.info("Is tender: [{}] accessed by vendor {} : {}", tender.getId(), vendor.getId(), accessed);
		if (!accessed) {
			throw ForbiddenException.tenderIsNotAccessed();
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/previews", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "') and hasDealership()")
	public TenderPreviewListResponse getTenderPreviews(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get previews of all OPEN tenders.");

		Vendor vendor = null;
		if (user.isVendor()) {
			String vendorId = user.getRelationshipId();
			vendor = dbClient.queryObject(Vendor.class, vendorId);
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);
		}

		QueryFields fields = new QueryFields("status", Tender.Status.OPEN.name());

		TenderPreviewListResponse response = new TenderPreviewListResponse();

		do {
			ResultSetIterator<Tender> resultSetIterator = getResultSetIterator(Tender.class, fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					Tender tender = resultSetIterator.next();
					if (tender != null) {
						if (user.isVendor()) {
							if (tender.isBlacklisted()) {
								continue;
							} else if (BiddingSubscriptionUtility.isTenderAccessed(dbClient, vendor, tender)) {
								/**
								 * If the user is vendor, then return previews of the tenders which are not
								 * accessed by him.
								 */
								continue;
							}
						}
						TenderPreviewResponse infoResponse = TenderResponseMapper.mapPreview(tender);
						response.add(infoResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (response.size() < fetchSize);

		logger.info("Returning response with a list of size: {}", response.size());

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/preview", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "') and hasDealership()")
	public TenderPreviewResponse previewTender(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId) {

		logger.info("Request received to preview Tender ID: {}.", tenderId);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tenderId");

		if (user.isVendor()) {
			Vendor vendor = dbClient.queryObject(Vendor.class, user.getRelationshipId());
			ArgValidator.checkEntityBlacklisted(vendor, user.getRelationshipId(), true);
		}

		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityNotNull(tender, tenderId, true);

		if (user.isVendor()) {
			ArgValidator.checkEntityBlacklisted(tender, tenderId, true);
		}

		TenderPreviewResponse response = TenderResponseMapper.mapPreview(tender);
		logger.info("Exit returning preview of tender: {}", response);

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "') and hasDealership()")
	public TenderInfoResponse getTenderDetails(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId) {

		logger.info("Request received to get Tender ID: {}.", tenderId);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tenderId");

		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityNotNull(tender, tenderId, true);

		if (user.isVendor()) {
			String vendorId = user.getRelationshipId();
			Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, false);
			ArgValidator.checkEntityBlacklisted(tender, tenderId, true);
			forbidIfTenderIsNotAccessed(vendor, tender);
		}

		TenderInfoResponse response = TenderResponseMapper.map(tender, Endpoint.TENDER_INFO.get(tenderId));

		logger.info("Returning details for tender: {}", response);
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/products", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "') and hasDealership()")
	public TenderProductInfoListResponse getTenderProducts(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get Bid products for Tender ID: {}.", tenderId);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tender_id");

		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityNotNull(tender, tenderId, true);

		if (user.isVendor()) {
			String vendorId = user.getRelationshipId();
			Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, false);
			ArgValidator.checkEntityBlacklisted(tender, tender.getId(), true);
			forbidIfTenderIsNotAccessed(vendor, tender);
		}

		// Get BidProducts for tender
		TenderProductInfoListResponse bidProductList = new TenderProductInfoListResponse();
		ResultSetIterator<TenderProduct> resultSetIterator = null;
		do {
			QueryFields fields = new QueryFields("tender", tender);
			resultSetIterator = getResultSetIterator(TenderProduct.class, fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					TenderProduct bidProduct = resultSetIterator.next();
					if (bidProduct != null) {
						TenderProductInfoResponse infoResponse = TenderResponseMapper.map(bidProduct,
								Endpoint.TENDER_BID_PRODUCT_INFO.get(tenderId, bidProduct.getId()));
						bidProductList.add(infoResponse);
					}
				}
				bidProductList.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (bidProductList.size() < fetchSize);

		logger.info("Returning products of tender: {} of size: {}", tenderId, bidProductList.size());

		return bidProductList;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/preview-products", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "') and hasDealership()")
	public ProductNameListResponse previewTenderProducts(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get products of the tender with ID: {}.", tenderId);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tender_id");

		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityNotNull(tender, tenderId, true);

		if (user.isVendor()) {
			String vendorId = user.getRelationshipId();
			Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, false);
			ArgValidator.checkEntityBlacklisted(tender, tender.getId(), true);
		}

		// Get BidProducts for tender
		ProductNameListResponse bidProductList = new ProductNameListResponse();
		ResultSetIterator<TenderProduct> resultSetIterator = null;
		do {
			QueryFields fields = new QueryFields("tender", tender);
			resultSetIterator = getResultSetIterator(TenderProduct.class, fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					TenderProduct bidProduct = resultSetIterator.next();
					if (bidProduct != null) {
						bidProductList.addNamedResourceResponse(
								DBObjectMapper.toNamedRelatedRestResponse(bidProduct.getProduct()));
					}
				}
				bidProductList.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (bidProductList.size() < fetchSize);

		logger.info("Returning product previews of tender: {} of size: {}", tenderId, bidProductList.size());

		return bidProductList;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{tenderId}/bids", consumes = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR + "') and hasDealership()")
	public ResponseEntity<CreateResourceResponse> postBid(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId, @RequestBody CreateTenderBidParam param) {

		logger.info("Request received to post bid for tender ID: {}. Request: {}", tenderId, param);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tenderId");
		ArgValidator.checkFieldNotNull(param, "create_tender_bid");
		Double value = param.getValue();
		String comments = param.getComments();
		ArgValidator.checkFieldNotNull(value, "value");
		ArgValidator.checkFieldValue(value > 0, "value");

		String vendorId = user.getRelationshipId();
		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, false);

		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(tender, tenderId, true);

		TenderUtility.checkTenderIsOpenForBidding(tender);

		forbidIfTenderIsNotAccessed(vendor, tender);

		QueryFields fields = new QueryFields("tender", tender);

		TenderBid bid = dbClient.queryObjectByFields(TenderBid.class, fields);
		boolean firstBid = (bid == null);

		fields.add("vendor", vendor);
		bid = dbClient.queryObjectByFields(TenderBid.class, fields);
		boolean vendorBidExists = (bid != null);

		if (!vendorBidExists) {
			bid = new TenderBid();
			String uid = UUIDUtil.createId(TenderBid.class);
			bid.setId(uid);
			bid.setVendor(vendor);
			bid.setTender(tender);
		}
		bid.setValue(value);
		bid.setComments(comments);

		if (vendorBidExists) {
			dbClient.updateObject(bid);
		} else {
			dbClient.createObject(bid);
		}

		if (firstBid) {
			recordEvent(SystemEventType.FIRST_TENDER_BID_POSTED, RecipientType.BUYER, tender.getBuyer().getId(),
					tenderId, tender.getName(), null, vendor.getName());
		}

		return TaskResponseUtility.createResourceResponse(bid.getId(),
				Endpoint.TENDER_BID_INFO.get(tenderId, bid.getId()), "Bid for the tender posted successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/bids", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "') and hasDealership()")
	public TenderBidInfoListResponse getTenderBids(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get bids of tender: {}", tenderId);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tenderId");

		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityNotNull(tender, tenderId, true);

		Vendor vendor = null;
		boolean tenderAccessed = false;
		if (user.isVendor()) {
			String vendorId = user.getRelationshipId();
			vendor = dbClient.queryObject(Vendor.class, vendorId);
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, false);
			ArgValidator.checkEntityBlacklisted(tender, tender.getId(), true);
			tenderAccessed = BiddingSubscriptionUtility.isTenderAccessed(dbClient, vendor, tender);
		}

		QueryFields fields = new QueryFields("tender", tender);
		TenderBidInfoListResponse response = new TenderBidInfoListResponse();
		ResultSetIterator<TenderBid> resultSetIterator = null;
		do {

			resultSetIterator = dbClient.queryResultSetIteratorBySortOrder(TenderBid.class, fields, offset, fetchSize,
					OrderBy.ASC, "value");

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					TenderBid bid = resultSetIterator.next();
					if (bid != null) {
						TenderBidInfoResponse infoResponse = TenderResponseMapper.map(bid);
						if (user.isVendor() && !tenderAccessed) {
							// Hide the bid comments if the vendor has not
							// accessed the tender.
							infoResponse.setComments(null);
						}
						response.add(infoResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (response.size() < fetchSize);
		logger.info("Exit returning response list of size: {}", response.size());
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/bids/my-bid", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR + "') and hasDealership()")
	public TenderBidInfoResponse getMyBid(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId) {

		logger.info("Request received to get my bid of tender: {}", tenderId);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tenderId");
		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityBlacklisted(tender, tenderId, true);

		String vendorId = user.getRelationshipId();
		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityBlacklisted(vendor, vendorId, false);

		forbidIfTenderIsNotAccessed(vendor, tender);

		QueryFields fields = new QueryFields("tender", tender);
		fields.add("vendor", vendor);
		TenderBidInfoResponse response = new TenderBidInfoResponse();
		TenderBid bid = dbClient.queryObjectByFields(TenderBid.class, fields);
		if (bid != null) {
			TenderResponseMapper.map(bid, response);
		}

		logger.info("Exit returning response : {}", response);
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/bids/lowest-bid", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "') and hasDealership()")
	public TenderBidInfoResponse getLowestBid(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId) {

		logger.info("Request received to get my bid of tender: {}", tenderId);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tenderId");

		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityNotNull(tender, tenderId, true);

		Vendor vendor = null;
		boolean tenderAccessed = false;
		String vendorId = user.getRelationshipId();
		if (user.isVendor()) {
			vendorId = user.getRelationshipId();
			vendor = dbClient.queryObject(Vendor.class, vendorId);
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, false);
			ArgValidator.checkEntityBlacklisted(tender, tender.getId(), true);
			tenderAccessed = BiddingSubscriptionUtility.isTenderAccessed(dbClient, vendor, tender);
		}

		QueryFields fields = new QueryFields("tender", tender);
		TenderBidInfoResponse response = new TenderBidInfoResponse();
		List<TenderBid> bidList = dbClient.queryListBySortOrder(TenderBid.class, fields, 1, OrderBy.ASC, "value");

		if (!bidList.isEmpty()) {
			TenderResponseMapper.map(bidList.get(0), response);
			if (user.isVendor() && !tenderAccessed) {
				// Hide the bid comments if the vendor has not accessed the
				// tender.
				response.setComments(null);
			}
		}

		logger.info("Exit returning response : {}", response);
		return response;
	}

	@RequestMapping(method = RequestMethod.DELETE, path = "/{tenderId}/bids/{bidId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "','" + Role.ADMIN + "') and hasDealership()")
	public ResponseEntity<TaskResponse> deleteBid(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId, @PathVariable("bidId") String bidId) {

		logger.info("Request received to delete bid: {} of tender: {} ", bidId, tenderId);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tenderId");
		ArgValidator.checkFieldUriType(bidId, TenderBid.class, "bidId");
		TenderBid tenderBid = dbClient.queryObject(TenderBid.class, bidId);
		ArgValidator.checkEntityNotNull(tenderBid, bidId, true);

		Tender tender = tenderBid.getTender();
		if (!tender.getId().equals(tenderId)) {
			throw ForbiddenException.unauthorizedAccess();
		}

		if (user.isVendor()) {
			String vendorId = user.getRelationshipId();
			Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
			ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, false);
			ArgValidator.checkEntityDisapprovedOrBlacklisted(tender, tenderId, true);

			if (!vendorId.equals(tenderBid.getVendor().getId())) {
				throw ForbiddenException.unauthorizedAccess();
			}
			TenderUtility.checkTenderIsOpenForBidding(tender);
		}

		dbClient.markAsDeleted(tenderBid);

		return TaskResponseUtility.createTaskSuccessResponse("Tender bid deleted successfully.");
	}

	/**
	 * Returns the list of tenders with details. If the user is vendor, then it
	 * returns only the tenders which are accessed by him. Otherwise, if the user is
	 * admin, it returns all the tenders.
	 * 
	 * @param user
	 *            the authenticated user
	 * @param filter
	 *            the query filter. It can take {@code status} alone as a filter
	 * @param fetchSize
	 *            the result set fetch size. Defaults to 10.
	 * @param offset
	 *            the offset of the next batch of results
	 * @return the {@code TenderInfoListResponse}
	 */
	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "','" + Role.ADMIN + "') and hasDealership()")
	public TenderInfoListResponse getTenders(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get all tenders with filter: {}.", filter);

		Vendor vendor = null;
		if (user.isVendor()) {
			String vendorId = user.getRelationshipId();
			vendor = dbClient.queryObject(Vendor.class, vendorId);
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, false);
		}

		final String[] QUERY_FILTER_KEYS = { "status" };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields();
		for (String key : keyValueMap.keySet()) {

			String value = keyValueMap.get(key);

			switch (key) {
			case "status":
				ArgValidator.checkFieldValueFromEnum(value, key, Tender.Status.class);
				fields.add("status", value);
				break;
			default:
				break;
			}
		}

		TenderInfoListResponse response = new TenderInfoListResponse();
		ResultSetIterator<Tender> resultSetIterator = null;

		do {
			resultSetIterator = getResultSetIterator(Tender.class, fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					Tender tender = resultSetIterator.next();
					if (tender != null) {
						if (user.isVendor()) {
							if (tender.isBlacklisted()) {
								continue;
							}
							if (!BiddingSubscriptionUtility.isTenderAccessed(dbClient, vendor, tender)) {
								/*
								 * If the user is a vendor and the tender is not accessed by him, skip the
								 * tender from adding it to the list response.
								 */
								continue;
							}
						}
						TenderInfoResponse infoResponse = TenderResponseMapper.map(tender,
								Endpoint.TENDER_INFO.get(tender.getId()));
						response.add(infoResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (response.size() < fetchSize);

		logger.info("Returning response with a list of size: {}", response.size());

		return response;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{tenderId}/access-tender", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR + "') and hasDealership()")
	public ResponseEntity<TaskResponse> accessTender(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("tenderId") String tenderId) {

		logger.info("Request received to access tender tender: {} ", tenderId);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tenderId");
		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(tender, tenderId, true);

		String vendorId = user.getRelationshipId();
		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, false);

		boolean tenderAccessed = BiddingSubscriptionUtility.isTenderAccessed(dbClient, vendor, tender);

		if (!tenderAccessed) {
			VendorBiddingSubscription subscription = verifySubscriptionValidity(vendor);
			BiddingSubscriptionUtility.updateSubscriptionUsage(dbClient, vendor, subscription, tender);
		}

		return TaskResponseUtility.createTaskSuccessResponse("Tender is successfully accessed by vendor.");
	}

}
