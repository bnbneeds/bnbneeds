package com.bnbneeds.app.api.service.security;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;

import com.bnbneeds.app.db.client.DbClient;

public class UserIdentitySecurityExpressionHandler extends
		DefaultMethodSecurityExpressionHandler implements
		MethodSecurityExpressionHandler {

	private LoginHelper loginHelper;
	private DbClient dbClient;

	public void setLoginHelper(LoginHelper loginHelper) {
		this.loginHelper = loginHelper;
	}

	public void setDbClient(DbClient dbClient) {
		this.dbClient = dbClient;
	}

	@Override
	protected MethodSecurityExpressionOperations createSecurityExpressionRoot(
			Authentication authentication, MethodInvocation invocation) {

		UserIdentitySecurityExpressionRoot root = new UserIdentitySecurityExpressionRoot(
				authentication);
		root.setDbClient(dbClient);
		root.setLoginHelper(loginHelper);
		root.setThis(invocation.getThis());
		root.setRoleHierarchy(getRoleHierarchy());
		root.setTrustResolver(new AuthenticationTrustResolverImpl());
		root.setPermissionEvaluator(getPermissionEvaluator());
		root.setDefaultRolePrefix("");
		return root;
	}
}
