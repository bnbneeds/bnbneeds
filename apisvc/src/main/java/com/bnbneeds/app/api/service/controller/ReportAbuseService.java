package com.bnbneeds.app.api.service.controller;

import static com.bnbneeds.app.api.service.response.DBObjectMapper.toNamedRelatedListResponse;

import java.net.URI;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.APIException;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.response.DataObjectResponseMapper;
import com.bnbneeds.app.api.service.response.UserAccountResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.AbusiveEntityReport;
import com.bnbneeds.app.db.client.model.AbusiveEntityReport.ReportStatus;
import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.model.AddRemarksParam;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.report.AbuseReportInfoListResponse;
import com.bnbneeds.app.model.report.AbuseReportInfoResponse;
import com.bnbneeds.app.model.report.AbuseReportListResponse;
import com.bnbneeds.app.model.report.CloseReportParam;
import com.bnbneeds.app.model.report.CreateReportParam;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/abuse-reports")
public class ReportAbuseService extends ResourceService {

	private static final Logger logger = LoggerFactory
			.getLogger(ReportAbuseService.class);

	@RequestMapping(method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasDealership()")
	public ResponseEntity<CreateResourceResponse> createReport(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody CreateReportParam createReportParam) {

		logger.info("API request received to register a new report: {}",
				createReportParam);

		String title = createReportParam.getTitle();
		ArgValidator.checkFieldNotEmpty(title, "title");
		String description = createReportParam.getDescription();
		ArgValidator.checkFieldNotEmpty(description, "description");

		AbusiveEntityReport report = new AbusiveEntityReport();
		report.setTimestamp(System.currentTimeMillis());
		String uid = UUIDUtil.createId(AbusiveEntityReport.class);
		report.setId(uid);
		report.setTitle(title);
		report.setDescription(description);
		report.setAssignedUserId(null);
		report.setReportStatus(ReportStatus.OPEN);

		// TODO remove if statement as abuse reports are created with users id
		if (StringUtils.hasText(createReportParam.getUserId())) {
			report.setReportedUserAccountId(createReportParam.getUserId());
		} else {
			report.setReportedUserAccountId(user.getAccount().getId());
		}

		String entityId = createReportParam.getEntityId();
		if (!StringUtils.isEmpty(entityId)) {
			ArgValidator.checkUri(entityId);
			String typeName = UUIDUtil.getTypeName(entityId);
			DataObject dataObject = new DataObject();

			// need this as Product name cannot be retrieved directly like other
			// entities
			if (typeName.equalsIgnoreCase("product")) {
				dataObject = dbClient.queryObject(Product.class, entityId);
			} else {
				dataObject = dbClient.queryObject(typeName, entityId);
			}

			ArgValidator.checkEntityNotNull(dataObject, entityId, false);
			report.setEntityId(entityId);
			if (!StringUtils.hasText(createReportParam.getName())) {
				if (typeName.equalsIgnoreCase("product")) {
					report.setName(((Product) dataObject).getProductName()
							.getName());
				} else {
					report.setName(dataObject.getName());
				}
			}
		}

		logger.debug("New abuse report creating in DB: {}", report);

		dbClient.createObject(report);

		URI uri = Endpoint.ABUSE_REPORT_INFO.get(uid);

		recordEvent(SystemEventType.ABUSE_REPORT_POSTED, RecipientType.ADMIN,
				uid, report.getName(), uri, user.getUsername());

		return TaskResponseUtility.createResourceResponse(uid, uri,
				"New report created successfully.");

	}

	@RequestMapping(method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public AbuseReportListResponse getReports(
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info(
				"Request received to get notifications by filter: {}, fetchSize: {}, offset: {}",
				filter, fetchSize, offset);

		final String[] QUERY_FILTER_KEYS = { "startTimestamp", "endTimestamp",
				"resourceId" };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);
		QueryFields fields = new QueryFields();
		for (String key : keyValueMap.keySet()) {
			switch (key) {
			case "resourceId":
				fields.add(key, keyValueMap.get(key));
				break;
			case "startTimestamp":
				fields.add("timestamp >=", Long.valueOf(keyValueMap.get(key)));
				break;
			case "endTimestamp":
				fields.add("timestamp <=", Long.valueOf(keyValueMap.get(key)));
				break;
			default:
				break;
			}
		}

		AbuseReportListResponse response = new AbuseReportListResponse();
		ResultSetIterator<AbusiveEntityReport> resultSetIterator = null;
		do {
			resultSetIterator = getResultSetIterator(AbusiveEntityReport.class,
					fields, offset, fetchSize);

			if (resultSetIterator != null) {

				response.addNamedRelatedResourceList(toNamedRelatedListResponse(resultSetIterator));
				response.setNextOffset(resultSetIterator.getOffset());

				if (offset != null
						&& offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (response.size() < fetchSize);

		return response;

	}

	@RequestMapping(method = RequestMethod.GET, value = "/bulk", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public AbuseReportInfoListResponse getReportslist(
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info(
				"Request received to get abuse reports by filter: {}, fetchSize: {}, offset: {}",
				filter, fetchSize, offset);

		final String[] QUERY_FILTER_KEYS = { "startTimestamp", "endTimestamp",
				"resourceId" };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);
		QueryFields fields = new QueryFields();
		for (String key : keyValueMap.keySet()) {
			switch (key) {
			case "resourceId":
				fields.add(key, keyValueMap.get(key));
				break;
			case "startTimestamp":
				fields.add("timestamp >=", Long.valueOf(keyValueMap.get(key)));
				break;
			case "endTimestamp":
				fields.add("timestamp <=", Long.valueOf(keyValueMap.get(key)));
				break;
			default:
				break;
			}
		}

		AbuseReportInfoListResponse resp = new AbuseReportInfoListResponse();
		ResultSetIterator<AbusiveEntityReport> resultSetIterator = null;
		do {

			resultSetIterator = getResultSetIterator(AbusiveEntityReport.class,
					fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					AbusiveEntityReport dataObject = resultSetIterator.next();
					AbuseReportInfoResponse response = new AbuseReportInfoResponse();
					DataObjectResponseMapper.mapDescribedDataObject(dataObject,
							response);
					response.setTitle(dataObject.getTitle());
					response.setTimestamp(dataObject.getTimestamp());
					response.setReportedByUserId(dataObject
							.getReportedUserAccountId());
					if (dataObject.getReportStatus() != null) {
						response.setReportStatus(dataObject.getReportStatus()
								.name());
					}

					response.setAssignedUserId(dataObject.getAssignedUserId());
					String entityId = dataObject.getEntityId();
					if (!StringUtils.isEmpty(entityId)) {
						response.setEntityId(entityId);
					}

					if (!StringUtils.isEmpty(dataObject.getReportStatus())
							&& !dataObject.isClosed()) {
						resp.add(response);
					}

					String reportedUserId = dataObject
							.getReportedUserAccountId();
					// get Dealer info
					UserAccount reportedUser = dbClient.queryObject(
							UserAccount.class, reportedUserId);
					UserAccountInfoResponse reportedUserInfo = UserAccountResponseMapper
							.map(reportedUser);
					response.setReportedByUser(reportedUserInfo);

					// get Assigned user name
					if (!StringUtils.isEmpty(dataObject.getAssignedUserId())) {
						UserAccount assignedUser = dbClient.queryObject(
								UserAccount.class,
								dataObject.getAssignedUserId());
						ArgValidator.checkEntityNotNull(assignedUser,
								dataObject.getAssignedUserId(), true);
						response.setAssignedUserName(assignedUser.getName());
					}
				}
				resp.setNextOffset(resultSetIterator.getOffset());

				if (offset != null
						&& offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (resp.size() < fetchSize);

		return resp;

	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.ADMIN + "')")
	public AbuseReportInfoResponse getReportInfo(@PathVariable("id") String id) {

		logger.info("Request received to get report details of id: {}", id);

		ArgValidator.checkFieldUriType(id, AbusiveEntityReport.class,
				"report_id");
		AbusiveEntityReport report = dbClient.queryObject(
				AbusiveEntityReport.class, id);
		ArgValidator.checkEntityNotNull(report, id, true);

		AbuseReportInfoResponse response = new AbuseReportInfoResponse();
		DataObjectResponseMapper.mapDescribedDataObject(report, response);
		response.setTitle(report.getTitle());
		response.setTimestamp(report.getTimestamp());
		response.setReportedByUserId(report.getReportedUserAccountId());
		response.setReportStatus(report.getReportStatus().name());
		response.setAssignedUserId(report.getAssignedUserId());
		String entityId = report.getEntityId();
		if (!StringUtils.isEmpty(entityId)) {
			response.setEntityId(entityId);
		}

		// get Dealer info
		UserAccount reportedUser = dbClient.queryObject(UserAccount.class,
				report.getReportedUserAccountId());
		UserAccountInfoResponse reportedUserInfo = UserAccountResponseMapper
				.map(reportedUser);
		response.setReportedByUser(reportedUserInfo);

		// get Assigned user name
		if (!StringUtils.isEmpty(report.getAssignedUserId())) {
			UserAccount assignedUser = dbClient.queryObject(UserAccount.class,
					report.getAssignedUserId());
			ArgValidator.checkEntityNotNull(assignedUser,
					report.getAssignedUserId(), true);
			response.setAssignedUserName(assignedUser.getName());
		}

		logger.info("Returning report details: {}", response);

		return response;

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public ResponseEntity<TaskResponse> deleteReport(
			@PathVariable("id") String id) {

		logger.info("Request received to delete report with id: {}", id);

		ArgValidator.checkFieldUriType(id, AbusiveEntityReport.class,
				"report_id");
		AbusiveEntityReport report = dbClient.queryObject(
				AbusiveEntityReport.class, id);
		ArgValidator.checkEntityNotNull(report, id, true);

		dbClient.markAsDeleted(report);
		logger.info("Report deleted successfully.");

		return TaskResponseUtility
				.createTaskSuccessResponse("Report deleted successfully.");

	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/close", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public ResponseEntity<TaskResponse> closeReport(
			@PathVariable("id") String id,
			@RequestBody CloseReportParam closeReportParam) throws APIException {

		logger.info("API request received to close abuse report with id: {}",
				id);

		ArgValidator.checkFieldUriType(id, AbusiveEntityReport.class,
				"report_id");
		AbusiveEntityReport abuseReport = dbClient.queryObject(
				AbusiveEntityReport.class, id);

		ArgValidator.checkEntityNotNull(abuseReport, id, true);

		if (!StringUtils.isEmpty(abuseReport.getReportStatus())
				&& abuseReport.isClosed()) {
			throw BadRequestException.create("Report is already closed");
		}

		abuseReport.setReportStatus(ReportStatus.CLOSED);

		logger.debug("Closing the report record in DB: {}.", abuseReport);
		dbClient.updateObject(abuseReport);

		logger.info("Closed the report and persisted in DB: {}", abuseReport);

		return TaskResponseUtility
				.createTaskSuccessResponse("Abuse report is closed successfully.");
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/assign-to-self", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public ResponseEntity<TaskResponse> assignReportToSelf(
			@PathVariable("id") String id,
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody AddRemarksParam addRemarksParam) throws APIException {

		logger.info(
				"API request received to assign abuse report to self with id: {}",
				id);

		ArgValidator.checkFieldUriType(id, AbusiveEntityReport.class,
				"report_id");
		AbusiveEntityReport abuseReport = dbClient.queryObject(
				AbusiveEntityReport.class, id);

		ArgValidator.checkEntityNotNull(abuseReport, id, true);

		if (!StringUtils.isEmpty(abuseReport.getReportStatus())
				&& abuseReport.isClosed()) {
			throw BadRequestException.create("Report is already closed");
		}

		abuseReport.setReportStatus(ReportStatus.ASSIGNED);
		abuseReport.setAssignedUserId(user.getAccount().getId());

		logger.debug("Assigning the report record in DB: {}.", abuseReport);
		dbClient.updateObject(abuseReport);

		logger.info("Assinged the report and persisted in DB: {}", abuseReport);

		return TaskResponseUtility
				.createTaskSuccessResponse("Abuse report is assinged to self successfully.");
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/add-remarks", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public ResponseEntity<TaskResponse> addRemark(
			@PathVariable("id") String id,
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody AddRemarksParam addRemarksParam) throws APIException {

		logger.info(
				"API request received to add remarks to abuse report to self with id: {}",
				id);

		ArgValidator.checkFieldUriType(id, AbusiveEntityReport.class,
				"report_id");
		AbusiveEntityReport abuseReport = dbClient.queryObject(
				AbusiveEntityReport.class, id);

		ArgValidator.checkEntityNotNull(abuseReport, id, true);

		if (!StringUtils.isEmpty(abuseReport.getReportStatus())
				&& abuseReport.isClosed()) {
			throw BadRequestException
					.create("Report is already closed, can't add remarks.");
		}

		abuseReport.addRemark(addRemarksParam.getRemarks());

		logger.debug("Adding the remarks record in DB: {}.", abuseReport);
		dbClient.updateObject(abuseReport);

		logger.info("Adding the remarks and persisted in DB: {}", abuseReport);

		return TaskResponseUtility
				.createTaskSuccessResponse("Remarks added to abuse report successfully.");
	}

}
