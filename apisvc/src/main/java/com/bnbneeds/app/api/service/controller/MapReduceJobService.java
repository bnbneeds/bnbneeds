package com.bnbneeds.app.api.service.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnbneeds.app.api.service.mapreduce.IndexBuyerRecordsForDocumentSearchJob;
import com.bnbneeds.app.api.service.mapreduce.IndexPropertiesInMultipleEntitiesJob;
import com.bnbneeds.app.api.service.mapreduce.IndexVendorRecordsForDocumentSearchJob;
import com.bnbneeds.app.api.service.pipeline.PipelineUtils;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.util.StringUtils;

/**
 * This service handles requests for appengine map-reduce jobs. These jobs can
 * only be executed by appengine application administrator. These service
 * methods implemented here are not conforming to REST API design standards.
 * Because they require Google OAuth login.
 * 
 * See <a href=
 * "https://github.com/GoogleCloudPlatform/appengine-mapreduce/wiki/1-MapReduce"
 * >MapReduce in AppEngine</a>
 * 
 * @see <a href=
 *      "https://github.com/GoogleCloudPlatform/appengine-pipelines/wiki/Java">Google
 *      App Engine Pipeline Framework in Java</a>
 * 
 * @author Amit Herlekar
 *
 */
@Controller
@RequestMapping(path = MapReduceJobService.BASE_REQUEST_PATH)
public class MapReduceJobService {

	static final String SHARD_COUNT_PARAM = "shardCount";
	static final String SHARD_COUNT_DEFAULT_VALUE = "10";

	private static final String MAP_REDUCE_JOBS_LIST_VIEW = "map-reduce-job-list";
	private static final String ENTITY_INDEX_PROPERTIES_FORM_VIEW = "map-reduce-entity-properties-index-form";
	private static final String MODEL_ATTRIBUTE_PIPELINE_URL = "pipelineURL";
	private static final String MODEL_ATTRIBUTE_ERROR_MESSAGE = "errorMessage";
	private static final String MODEL_ATTRIBUTE_MAP_REDUCE_JOB_MAP = "mapReduceJobMap";

	protected static final String BASE_REQUEST_PATH = "/map-reduce-jobs";
	private static final String INDEX_ENTITY_PROPERTIES_FORM_URI = "/index-entity-properties-form";
	private static final String INDEX_ENTITIES_INTO_SEARCH_DOCUMENTS_URI = "/index-entities-into-search-documents";

	private static final Map<String, String> MAP_REDUCE_JOB_URL_MAP;

	static {
		MAP_REDUCE_JOB_URL_MAP = new HashMap<>();
		MAP_REDUCE_JOB_URL_MAP.put("Index Entity Properties", getAbsoluteURI(INDEX_ENTITY_PROPERTIES_FORM_URI));
		MAP_REDUCE_JOB_URL_MAP.put("Index Buyers Into Search Documents (Must be executed only once)",
				getAbsoluteURI(INDEX_ENTITIES_INTO_SEARCH_DOCUMENTS_URI) + "?entityType=Buyer");
		MAP_REDUCE_JOB_URL_MAP.put("Index Vendors Into Search Documents (Must be executed only once)",
				getAbsoluteURI(INDEX_ENTITIES_INTO_SEARCH_DOCUMENTS_URI) + "?entityType=Vendor");

	}

	private static String getAbsoluteURI(String uri) {
		return String.format("%1$s%2$s%3$s", Endpoint.CONTEXT_PATH, BASE_REQUEST_PATH, uri);
	}

	private static void setErrorMessage(String message, Model model) {
		model.addAttribute(MODEL_ATTRIBUTE_ERROR_MESSAGE, message);
	}

	private static void setPipelineURL(String pipelineId, Model model) {
		if (StringUtils.hasText(pipelineId)) {
			model.addAttribute(MODEL_ATTRIBUTE_PIPELINE_URL, PipelineUtils.getPipelineStatusUrl(pipelineId));
		}
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getMapReduceJobs(Model model) {
		model.addAttribute(MODEL_ATTRIBUTE_MAP_REDUCE_JOB_MAP, MAP_REDUCE_JOB_URL_MAP);

		return MAP_REDUCE_JOBS_LIST_VIEW;
	}

	@RequestMapping(method = RequestMethod.GET, path = INDEX_ENTITY_PROPERTIES_FORM_URI)
	public String getEntityIndexPropertiesForm() {
		return ENTITY_INDEX_PROPERTIES_FORM_VIEW;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/index-entity-properties", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public String indexEntityProperties(
			@RequestParam(name = SHARD_COUNT_PARAM, defaultValue = SHARD_COUNT_DEFAULT_VALUE) int shardCount,
			String entityTypes, String properties, Model model) {

		String[] entityTypeArray = StringUtils.commaDelimitedListToStringArray(entityTypes);
		String[] propertyArray = StringUtils.commaDelimitedListToStringArray(properties);

		entityTypeArray = StringUtils.trimArrayElements(entityTypeArray);
		propertyArray = StringUtils.trimArrayElements(propertyArray);

		List<String> entityTypeList = Arrays.asList(entityTypeArray);
		List<String> propertyList = Arrays.asList(propertyArray);

		if (entityTypeList == null || entityTypeList.isEmpty()) {
			setErrorMessage("Entity Types cannot be empty.", model);
			return ENTITY_INDEX_PROPERTIES_FORM_VIEW;
		}
		if (propertyList == null || propertyList.isEmpty()) {
			setErrorMessage("Properties cannot be empty.", model);
			return ENTITY_INDEX_PROPERTIES_FORM_VIEW;
		}

		String pipelineId = PipelineUtils.startPipelineService(
				new IndexPropertiesInMultipleEntitiesJob(entityTypeList, propertyList, shardCount));
		setPipelineURL(pipelineId, model);

		return ENTITY_INDEX_PROPERTIES_FORM_VIEW;
	}

	@RequestMapping(method = RequestMethod.GET, path = INDEX_ENTITIES_INTO_SEARCH_DOCUMENTS_URI, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public String indexEntitiesIntoDocuments(
			@RequestParam(name = SHARD_COUNT_PARAM, defaultValue = SHARD_COUNT_DEFAULT_VALUE) int shardCount,
			@RequestParam(name = "entityType") String entityType, Model model) {

		String pipelineId = null;
		switch (entityType.toLowerCase()) {
		case "buyer":
			pipelineId = PipelineUtils.startPipelineService(new IndexBuyerRecordsForDocumentSearchJob(shardCount));
			break;
		case "vendor":
			pipelineId = PipelineUtils.startPipelineService(new IndexVendorRecordsForDocumentSearchJob(shardCount));
			break;
		default:
			break;
		}
		model.addAttribute(MODEL_ATTRIBUTE_MAP_REDUCE_JOB_MAP, MAP_REDUCE_JOB_URL_MAP);
		setPipelineURL(pipelineId, model);
		return MAP_REDUCE_JOBS_LIST_VIEW;
	}
}
