package com.bnbneeds.app.api.service.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.ProductName;
import com.bnbneeds.app.db.client.model.Tender;
import com.bnbneeds.app.db.client.model.Tender.Status;
import com.bnbneeds.app.db.client.model.TenderBid;
import com.bnbneeds.app.db.client.model.TenderProduct;
import com.bnbneeds.app.db.client.model.Units;
import com.bnbneeds.app.model.dealer.tender.CreateTenderParam;
import com.bnbneeds.app.model.dealer.tender.TenderProductParam;
import com.bnbneeds.app.model.dealer.tender.UpdateTenderParam;
import com.bnbneeds.app.model.dealer.tender.UpdateTenderStatusParam;
import com.bnbneeds.app.util.DateUtils;
import com.bnbneeds.app.util.StringUtils;

public class TenderUtility {

	private static final Logger logger = LoggerFactory.getLogger(TenderUtility.class);

	public static final int MAX_DAYS_BETWEEN_OPEN_AND_CLOSE_DATES = 15;
	public static final int MIN_DAYS_BETWEEN_OPEN_AND_CLOSE_DATES = 2;
	private static final String TENDER_CLOSE_DATE_ERROR_MESSAGE_FORMAT = "The close date must be at least %1$s and cannot be later than %2$s";

	public static void validateAndMapUpdateTenderRequest(UpdateTenderParam param, Tender tender,
			SimpleDateFormat closeDateFormat) {

		String tenderStatus = tender.getStatus();
		switch (tender.getStatusEnum()) {
		case OPEN:
			throw ForbiddenException.cannotProcessRequestOnTender(tenderStatus,
					"Cannot update tender. Change the status to 'EDIT_IN_PROGRESS'.");
		case CLOSED:
		case CANCELLED:
			throw ForbiddenException.cannotProcessRequestOnTender(tenderStatus, "Cannot update tender.");
		default:
			break;
		}

		String name = param.getName();
		if (StringUtils.hasText(name)) {
			tender.setName(name);
		}

		String description = param.getDescription();
		if (StringUtils.hasText(description)) {
			tender.setDescription(description);
		}

		Double expectedPrice = param.getExpectedPrice();
		if (expectedPrice != null && StringUtils.hasText(expectedPrice.toString())) {
			ArgValidator.checkFieldValue(expectedPrice > 0, "expected_price");
			tender.setExpectedPrice(expectedPrice);
		}

		String creditTerms = param.getCreditTerms();
		if (StringUtils.hasText(creditTerms)) {
			ArgValidator.checkFieldValueFromEnum(creditTerms, "credit_terms", Tender.CreditTerms.class);
			tender.setCreditTerms(creditTerms);
		}
		String deliveryTerms = param.getDeliveryTerms();
		if (StringUtils.hasText(deliveryTerms)) {
			tender.setDeliveryTerms(deliveryTerms);
		}

		String paymentMode = param.getPaymentMode();
		if (StringUtils.hasText(paymentMode)) {
			ArgValidator.checkFieldValueFromEnum(paymentMode, "payment_mode", Tender.PaymentMode.class);
			tender.setPaymentMode(paymentMode);
		}
		String bidValueType = param.getBidValueType();
		if (StringUtils.hasText(bidValueType)) {
			ArgValidator.checkFieldValueFromEnum(bidValueType, "bid_value_type", Tender.BidValueType.class);
			tender.setBidValueType(bidValueType);
		}
		String deliveryFrequency = param.getDeliveryFrequency();
		if (StringUtils.hasText(deliveryFrequency)) {
			ArgValidator.checkFieldValueFromEnum(deliveryFrequency, "delivery_frequency",
					Tender.DeliveryFrequency.class);
			tender.setDeliveryFrequency(deliveryFrequency);
		}

		String deliveryLocation = param.getDeliveryLocation();
		if (StringUtils.hasText(deliveryLocation)) {
			tender.setDeliveryLocation(deliveryLocation);
		}
		String closeDateText = param.getCloseDate();
		if (StringUtils.hasText(closeDateText)) {
			ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(closeDateText, closeDateFormat), "close_date");
			Date closeDate = DateUtils.getEnd(DateUtils.getDate(closeDateFormat, closeDateText));
			Date currentDate = new Date();
			ArgValidator.checkField(hasCorrectDiffernceInDaysForTenderCloseDate(currentDate, closeDate), "close_date",
					getErrorMessageForIncorrectTenderCloseDate(currentDate, closeDateFormat));
			tender.setCloseTimestamp(closeDate.getTime());
		}
	}

	public static boolean hasCorrectDiffernceInDaysForTenderCloseDate(Date fromDate, Date closeDate) {
		int differenceInDays = DateUtils.differenceInDays(fromDate, closeDate);
		return hasCorrectDiffernceInDaysForTenderCloseDate(differenceInDays);
	}

	public static boolean hasCorrectDiffernceInDaysForTenderCloseDateFromCurrentDate(Date closeDate) {
		int differenceInDays = DateUtils.differenceInDays(new Date(), closeDate);
		return hasCorrectDiffernceInDaysForTenderCloseDate(differenceInDays);
	}

	public static boolean hasCorrectDiffernceInDaysForTenderCloseDate(int differenceInDays) {
		return (differenceInDays >= MIN_DAYS_BETWEEN_OPEN_AND_CLOSE_DATES
				&& differenceInDays <= MAX_DAYS_BETWEEN_OPEN_AND_CLOSE_DATES);
	}

	/*
	 * Validate the Tender status transition.
	 */
	public static void validateUpdateTenderStatusRequest(DbClient dbClient, String dealerId, Tender tender,
			UpdateTenderStatusParam param) {

		String currentTenderStatus = tender.getStatus();
		switch (tender.getStatusEnum()) {
		case CLOSED:
		case CANCELLED:
			throw ForbiddenException.cannotProcessRequestOnTender(currentTenderStatus, "Cannot update tender.");
		default:
			break;
		}

		switch (param.getStatus()) {

		case OPEN:
			if (!(tender.hasTenderStatus(Status.CREATED) || tender.hasTenderStatus(Status.EDIT_IN_PROGRESS))) {
				throw ForbiddenException.cannotProcessRequestOnTender(currentTenderStatus, "Cannot open the tender.");
			}
			// check if there are any bidProducts in the tender
			QueryFields fields = new QueryFields("tender", tender);
			TenderProduct bidProduct = dbClient.queryObjectByFields(TenderProduct.class, fields);
			if (bidProduct == null) {
				throw ForbiddenException.cannotProcessRequestOnTender(currentTenderStatus,
						"Cannot open the tender when there are no product requirements for it.");
			}
			break;
		case EDIT:
			if (tender.hasTenderStatus(Status.OPEN)) {
				TenderBid bid = dbClient.queryObjectByFields(TenderBid.class, new QueryFields("tender", tender));
				if (bid != null) {
					throw ForbiddenException.cannotProcessRequestOnTender(currentTenderStatus,
							"Cannot update the tender when bids are being placed against it.");
				} else {
					Date openedDate = new Date(tender.getOpenTimestamp());
					int differenceInDays = DateUtils.differenceInDays(openedDate, new Date());
					if (differenceInDays < 1) {
						throw ForbiddenException.cannotProcessRequestOnTender(currentTenderStatus,
								"Cannot update the tender when it is open for bidding. Please wait for one more day to update when there are no bids placed against it.");
					}
				}
			} else {
				if (tender.hasTenderStatus(Status.CLOSED) || tender.hasTenderStatus(Status.CANCELLED))
					throw ForbiddenException.cannotProcessRequestOnTender(currentTenderStatus,
							"Cannot update the tender.");
			}
			break;
		case CANCEL:
			/*
			 * if (!(tender.hasTenderStatus(Status.OPEN) ||
			 * tender.hasTenderStatus(Status.EDIT_IN_PROGRESS))) { throw
			 * ForbiddenException.cannotProcessRequestOnTender(currentTenderStatus,
			 * "Cannot cancel the tender."); }
			 */
			ArgValidator.checkFieldNotEmpty(param.getReasonForCancellation(), "reason_for_cancellation");
			break;
		default:
			break;
		}
	}

	public static void checkIfTenderIsAllowedToManageBidProducts(String dealerId, Tender tender) {

		String tenderStatusText = tender.getStatus();
		switch (tender.getStatusEnum()) {
		case OPEN:
			throw ForbiddenException.cannotProcessRequestOnTender(tenderStatusText,
					"Change the status of the tender to 'EDIT_IN_PROGRESS'.");
		case CLOSED:
		case CANCELLED:
			logger.error("Tender is already {} for dealerId: {}. Tender: {}", tender.getStatus(), dealerId,
					tender.getId());
			throw ForbiddenException.cannotProcessRequestOnTender(tenderStatusText,
					"Cannot manage bid products of the tender.");
		default:
			break;
		}
	}

	/**
	 * Checks if the tender is open for bidding operations.
	 * 
	 * @param tender
	 *            the tender
	 * @throws ForbiddenException
	 *             If tender is not OPEN or if tender has passed deadline.
	 */
	public static void checkTenderIsOpenForBidding(Tender tender) {
		if (!tender.hasTenderStatus(Status.OPEN)) {
			throw ForbiddenException.tenderNotOpenForBidding();
		} else {
			if (System.currentTimeMillis() >= tender.getCloseTimestamp()) {
				throw ForbiddenException.tenderNotOpenForBidding("It has passed deadline");
			}
		}
	}

	public static String getErrorMessageForIncorrectTenderCloseDate(Date currentDate, SimpleDateFormat dateFormat) {

		Date maxDate = DateUtils.addDays(currentDate, MAX_DAYS_BETWEEN_OPEN_AND_CLOSE_DATES);
		Date minDate = DateUtils.addDays(currentDate, MIN_DAYS_BETWEEN_OPEN_AND_CLOSE_DATES);
		String maxDateText = StringUtils.getFormattedDate(maxDate, dateFormat);
		String minDateText = StringUtils.getFormattedDate(minDate, dateFormat);
		return String.format(TENDER_CLOSE_DATE_ERROR_MESSAGE_FORMAT, minDateText, maxDateText);
	}

	public static void validateCreateTenderRequest(CreateTenderParam param, SimpleDateFormat closeDateFormat) {
		ArgValidator.checkFieldNotEmpty(param.getName(), "name");
		Double expectedPrice = param.getExpectedPrice();
		String closeDateText = param.getCloseDate();
		ArgValidator.checkFieldNotEmpty(closeDateText, "close_date");
		ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(closeDateText, closeDateFormat), "close_date");
		Date closeDate = DateUtils.getEnd(DateUtils.getDate(closeDateFormat, closeDateText));
		Date currentDate = new Date();
		ArgValidator.checkField(hasCorrectDiffernceInDaysForTenderCloseDate(currentDate, closeDate), "close_date",
				getErrorMessageForIncorrectTenderCloseDate(currentDate, closeDateFormat));

		ArgValidator.checkFieldNotEmpty(expectedPrice.toString(), "expected_price");
		ArgValidator.checkFieldValue(expectedPrice > 0, "expected_price");
		ArgValidator.checkFieldValueFromEnum(param.getCreditTerms(), "credit_terms", Tender.CreditTerms.class);
		// ArgValidator.checkFieldNotEmpty(param.getDeliveryTerms(), "delivery_terms");
		ArgValidator.checkFieldValueFromEnum(param.getPaymentMode(), "payment_mode", Tender.PaymentMode.class);
		ArgValidator.checkFieldValueFromEnum(param.getBidValueType(), "bid_value_type", Tender.BidValueType.class);
		ArgValidator.checkFieldValueFromEnum(param.getDeliveryFrequency(), "delivery_frequency",
				Tender.DeliveryFrequency.class);
	}

	public static void validateAddBidProductRequest(TenderProductParam param, Tender tender, String buyerId) {
		ArgValidator.checkFieldNotNull(param, "bid_product");
		ArgValidator.checkFieldUriType(param.getProductNameId(), ProductName.class, "product_name");
		ArgValidator.checkFieldNotNull(param.getQuantity(), "quantity");
		ArgValidator.checkField(param.getQuantity() > 0, "quantity", "Quantity must be greater than 0");
		ArgValidator.checkFieldValueFromEnum(param.getUnit(), "unit", Units.Quantity.class);
		ArgValidator.checkFieldNotEmpty(param.getQualitySpecification(), "quality_specification");

	}

}
