package com.bnbneeds.app.api.service.controller;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.mapreduce.DeleteInactiveRecordsJob;
import com.bnbneeds.app.api.service.pipeline.PipelineUtils;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.ProductUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.Task;
import com.bnbneeds.app.db.client.model.Task.Status;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;

/**
 * See
 * <a href="https://cloud.google.com/appengine/docs/standard/java/config/cron"
 * >Scheduling Tasks With Cron for Java</a>
 * 
 * @author Amit Herlekar
 *
 */

@RestController
@RequestMapping(method = RequestMethod.GET, path = "/cron-jobs/system")
public class SystemCronJobService extends ResourceService {

	@Resource(name = "entityTypeListForDeletionOfInactiveRecords")
	private List<String> entityTypeListForDeletionOfInactiveRecords;

	@RequestMapping(path = "/update-product-native-id", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> updateProductNativeId() {

		List<Product> products = dbClient.queryList(Product.class);
		DBOperationList opList = new DBOperationList();
		for (Iterator<Product> iterator = products.iterator(); iterator.hasNext();) {
			Product product = iterator.next();
			String newNativeId = ProductUtility.generateProductNativeId(product.getVendor(), product);
			product.setNativeId(newNativeId);
			opList.addOperation(OperationType.UPDATE, product);
		}

		dbClient.transact(opList);

		return TaskResponseUtility.createTaskSuccessResponse("Job completed succesfully.");

	}

	@RequestMapping(path = "/delete-inactive-records", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CreateResourceResponse> deleteInactiveRecords(
			@RequestParam(name = MapReduceJobService.SHARD_COUNT_PARAM, defaultValue = MapReduceJobService.SHARD_COUNT_DEFAULT_VALUE) int shardCount) {

		ArgValidator.checkFieldNotEmpty(entityTypeListForDeletionOfInactiveRecords, "entityTypes");

		String pipelineId = PipelineUtils.startPipelineService(
				new DeleteInactiveRecordsJob(entityTypeListForDeletionOfInactiveRecords, shardCount));

		return PipelineUtils.createPipelineResponse(pipelineId);

	}

	@RequestMapping(path = "/delete-successful-tasks", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deactivateSuccessfulTasks(
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = "50") int fetchSize) {

		List<Task> tasks = dbClient.queryList(Task.class, fetchSize);
		if (!CollectionUtils.isEmpty(tasks)) {
			DBOperationList opList = new DBOperationList();
			for (Iterator<Task> iterator = tasks.iterator(); iterator.hasNext();) {
				Task task = iterator.next();
				if (task.hasStatus(Status.SUCCESS)) {
					opList.addOperation(OperationType.MARK_AS_DELETED, task);
				}
			}

			dbClient.transact(opList);
		}
		return TaskResponseUtility.createTaskSuccessResponse("Job completed succesfully.");
	}
}
