package com.bnbneeds.app.api.service.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.bnbneeds.app.db.client.model.UserAccount;

@Service
public class AuthenticationService implements UserDetailsService {

	private static final Logger logger = LoggerFactory
			.getLogger(AuthenticationService.class);

	private LoginHelper loginHelper;
	private PasswordEncoder passwordEncoder;

	public void setLoginHelper(LoginHelper loginHelper) {
		this.loginHelper = loginHelper;
	}

	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public BNBNeedsUser loadUserByUsername(String username)
			throws UsernameNotFoundException {

		BNBNeedsUser userDetails = null;

		UserAccount userLogin = loginHelper.getUserAccountByUsername(username);

		if (userLogin == null) {
			logger.error("Username not found: {}", username);
			throw new UsernameNotFoundException("User: " + username
					+ "is invalid");
		}
		String passwordPlainText = passwordEncoder.decrypt(
				userLogin.getPassKey(), userLogin.getEncryptedPassword());

		boolean enabled = !userLogin.isDisabled();
		userDetails = new BNBNeedsUser(username, passwordPlainText, enabled,
				true, true, true, this.getAuthorities(userLogin.getRole()));

		userDetails.setAccount(userLogin);

		return userDetails;

	}

	public Collection<GrantedAuthority> getAuthorities(String access) {

		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();

		authList.add(new SimpleGrantedAuthority(access));
		if (Role.SUPER_ADMIN.equals(access)) {
			authList.add(new SimpleGrantedAuthority(Role.ADMIN));
		}

		return authList;
	}

}
