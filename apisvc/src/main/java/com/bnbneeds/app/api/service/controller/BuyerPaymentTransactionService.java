package com.bnbneeds.app.api.service.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.PaymentTransactionInfoListResponse;
import com.bnbneeds.app.model.dealer.PaymentTransactionInfoResponse;
import com.bnbneeds.app.model.dealer.UpdatePaymentTransactionParam;

@RestController
@RequestMapping(path = "/buyers/{dealerId}/payment-transactions")
@PreAuthorize("hasAnyRole('" + Role.BUYER + "','" + Role.ADMIN + "') and isRelated(#dealerId)")
public class BuyerPaymentTransactionService extends PaymentTransactionService {

	@Override
	public Class<Buyer> getDealerType() {
		return Buyer.class;
	}

	@Override
	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public PaymentTransactionInfoListResponse getPaymentTransactionList(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("dealerId") String dealerId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {
		return super.getPaymentTransactionList(user, dealerId, filter, fetchSize, offset);
	}

	@Override
	@RequestMapping(method = RequestMethod.GET, path = "/{paymentTransactionId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public PaymentTransactionInfoResponse getPaymentTransaction(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("dealerId") String dealerId,
			@PathVariable("paymentTransactionId") String paymentTransactionId) {
		return super.getPaymentTransaction(user, dealerId, paymentTransactionId);
	}

	@Override
	@RequestMapping(method = RequestMethod.PUT, path = "/{paymentTransactionId}", consumes = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> updatePaymentTransaction(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("dealerId") String dealerId,
			@PathVariable("paymentTransactionId") String paymentTransactionId,
			@RequestBody UpdatePaymentTransactionParam param) {
		return super.updatePaymentTransaction(user, dealerId, paymentTransactionId, param);
	}

}
