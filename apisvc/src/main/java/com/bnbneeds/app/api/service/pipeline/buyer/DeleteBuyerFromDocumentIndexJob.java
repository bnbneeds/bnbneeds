package com.bnbneeds.app.api.service.pipeline.buyer;

import com.bnbneeds.app.api.service.documentsearch.Constants;
import com.bnbneeds.app.api.service.documentsearch.SearchAPIUtility;
import com.google.appengine.tools.pipeline.Job0;
import com.google.appengine.tools.pipeline.Value;

public class DeleteBuyerFromDocumentIndexJob extends Job0<Void> {

	private static final long serialVersionUID = 7758526280568388045L;

	private String buyerId;

	public DeleteBuyerFromDocumentIndexJob(String buyerId) {
		super();
		this.buyerId = buyerId;
	}

	@Override
	public String getJobDisplayName() {
		return "Remove document of buyer with ID: " + buyerId + " from index";
	}

	@Override
	public Value<Void> run() throws Exception {

		try {
			SearchAPIUtility.deleteDocument(Constants.BUYER_INDEX, buyerId);
		} catch (Exception e) {
			// ignore
		}
		return null;
	}

}
