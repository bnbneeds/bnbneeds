package com.bnbneeds.app.api.service.controller;

import static com.bnbneeds.app.api.service.response.DBObjectMapper.toNamedRelatedListResponse;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.documentsearch.Constants;
import com.bnbneeds.app.api.service.documentsearch.SearchAPIUtility;
import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.APIException;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.pipeline.PipelineUtils;
import com.bnbneeds.app.api.service.pipeline.buyer.PutBuyerIntoDocumentIndexJob;
import com.bnbneeds.app.api.service.pipeline.buyer.ReindexBuyerDocumentJob;
import com.bnbneeds.app.api.service.response.DealerResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.DealerUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.BusinessType;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.messaging.email.MessageKeys;
import com.bnbneeds.app.messaging.email.Template;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.BuyerInfoListResponse;
import com.bnbneeds.app.model.dealer.BuyerInfoResponse;
import com.bnbneeds.app.model.dealer.BuyerListResponse;
import com.bnbneeds.app.model.dealer.CreateBuyerParam;
import com.bnbneeds.app.model.dealer.UpdateBuyerParam;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;

@RestController
@RequestMapping(value = "/buyers")
public class BuyerService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(BuyerService.class);

	private ResultSetIterator<Buyer> getResultSetIterator(String filter, String offset, int fetchSize) {

		final String[] QUERY_FILTER_KEYS = { "name", "status", "businessType", "businessTypeId" };
		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);
		QueryFields fields = new QueryFields();

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case "name":
				fields.add(key, value);
				break;
			case "status":
				ArgValidator.checkFieldValue(!EntityStatus.BLACKLISTED.name().equals(value.toUpperCase()), key);
				fields.add("entityStatus", value.toUpperCase());
				break;
			case "businessType":
				BusinessType businessType = dbClient.queryObjectByFields(BusinessType.class,
						new QueryFields("name", value));
				if (businessType != null) {
					fields.add(key, businessType);
				}
				break;
			case "businessTypeId":
				BusinessType businessTypeById = dbClient.queryObject(BusinessType.class, value);
				if (businessTypeById != null) {
					fields.add("businessType", businessTypeById);
				}
				break;
			default:
				break;
			}
		}

		ResultSetIterator<Buyer> resultSetIterator = getResultSetIterator(Buyer.class, fields, offset, fetchSize);

		return resultSetIterator;

	}

	@RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and canMakeRelationship()")
	public ResponseEntity<CreateResourceResponse> createBuyer(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody CreateBuyerParam buyerParam) {

		logger.info("API request received to register a new buyer: {}", buyerParam);

		DealerUtility.validateCreateDealerParam(buyerParam, dbClient);

		Buyer buyer = new Buyer();
		String uid = UUIDUtil.createId(Buyer.class);
		buyer.setId(uid);
		buyer.setName(buyerParam.getOrganizationName());
		buyer.setDescription(buyerParam.getDescription());
		BusinessType businessType = dbClient.queryObject(BusinessType.class, buyerParam.getBusinessTypeId());
		buyer.setBusinessType(businessType);
		buyer.setAddress(buyerParam.getAddress());
		buyer.setContactName(buyerParam.getContactName());
		buyer.setMobileNumber(buyerParam.getMobileNumber());
		buyer.setEmailAddress(buyerParam.getEmailAddress());
		buyer.setPan(buyerParam.getPan());
		buyer.setWebsite(buyerParam.getWebsite());

		logger.debug("New buyer record creating in DB: {}", buyer);

		DBOperationList dbOperationList = new DBOperationList();
		dbOperationList.addOperation(OperationType.INSERT, buyer);

		UserAccount userLogin = user.getAccount();
		userLogin.setRelationshipId(uid);
		dbOperationList.addOperation(OperationType.UPDATE, userLogin);
		dbClient.transact(dbOperationList);
		user.setAccount(userLogin);
		logger.debug("Updated relationshipId in user login: {}", uid);
		logger.info("New buyer persisted in DB: {}", uid);

		URI uri = Endpoint.BUYER_INFO.get(uid);

		recordEvent(SystemEventType.NEW_BUYER_REGISTERED, RecipientType.VENDORS_AND_ADMIN, uid,
				buyerParam.getOrganizationName(), uri, null);

		PipelineUtils.startPipelineService(new PutBuyerIntoDocumentIndexJob(uid));

		Template message = emailHelper.createMessageTemplate(MessageKeys.DEALER_REGISTERED);
		message.resolveBodyValues(user.getAccount().getAccountName());
		emailHelper.send(message, user.getAccount().getName());

		return TaskResponseUtility.createResourceResponse(uid, uri, "New buyer created successfully");

	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "')  and hasDealership()")
	public BuyerListResponse getBuyerList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get buyers by filter: {}, fetchSize: {}, offset: {}", filter, fetchSize,
				offset);

		BuyerListResponse resp = new BuyerListResponse();

		if (user.isBuyer()) {
			String relationshipId = user.getRelationshipId();
			Buyer buyer = dbClient.queryObject(Buyer.class, relationshipId);
			if (buyer != null) {
				if (!buyer.isBlacklisted()) {
					List<Buyer> buyerList = new ArrayList<>();
					buyerList.add(buyer);
					resp.setNamedRelatedResourceList(toNamedRelatedListResponse(buyerList));
				}
			}
		} else {
			ResultSetIterator<Buyer> resultSetIterator = null;
			do {
				resultSetIterator = getResultSetIterator(filter, offset, fetchSize);
				if (resultSetIterator != null) {
					resp.addNamedRelatedResourceList(toNamedRelatedListResponse(resultSetIterator, null,
							new EntityStatus[] { EntityStatus.BLACKLISTED }));
					resp.setNextOffset(resultSetIterator.getOffset());

					if (offset != null && offset.equals(resultSetIterator.getOffset())) {
						break;
					}
					offset = resultSetIterator.getOffset();
				} else {
					break;
				}
			} while (resp.size() < fetchSize);
		}

		logger.debug("Exit returning buyers of size: {}", resp.getBuyerList() == null ? 0 : resp.getBuyerList().size());
		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/bulk", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "','" + Role.ADMIN + "') and hasDealership()")
	public BuyerInfoListResponse getBuyerInfoList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get buyers by filter: {}, fetchSize: {}, offset: {}", filter, fetchSize,
				offset);

		BuyerInfoListResponse resp = new BuyerInfoListResponse();

		if (user.isBuyer()) {
			String relationshipId = user.getRelationshipId();
			Buyer buyer = dbClient.queryObject(Buyer.class, relationshipId);
			if (buyer != null) {
				if (!buyer.isBlacklisted()) {
					BuyerInfoResponse buyerInfo = new BuyerInfoResponse();
					DealerResponseMapper.mapBuyer(buyer, buyerInfo);
					resp.add(buyerInfo);
				}
			}
		} else {

			ResultSetIterator<Buyer> resultSetIterator = null;
			do {
				resultSetIterator = getResultSetIterator(filter, offset, fetchSize);
				if (resultSetIterator != null) {
					while (resultSetIterator.hasNext()) {
						Buyer buyer = resultSetIterator.next();
						if (buyer != null && (!buyer.isBlacklisted() || user.isAdmin())) {
							BuyerInfoResponse buyerInfo = new BuyerInfoResponse();
							DealerResponseMapper.mapBuyer(buyer, buyerInfo);
							resp.add(buyerInfo);
						}
					}
					resp.setNextOffset(resultSetIterator.getOffset());
					if (offset != null && offset.equals(resultSetIterator.getOffset())) {
						break;
					}
					offset = resultSetIterator.getOffset();
				} else {
					break;
				}
			} while (resp.size() < fetchSize);
		}

		logger.debug("Exit returning buyers of size: {}", resp.size());
		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "', '" + Role.ADMIN + "') and hasDealership()")
	public BuyerInfoResponse getBuyer(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String id)
			throws APIException {

		logger.info("Request received to retrieve the buyer details: buyer id: {}", id);

		ArgValidator.checkFieldUriType(id, Buyer.class, "buyer_id");

		if (user.isBuyer() && !id.equals(user.getRelationshipId())) {
			logger.error(
					"Unauthorized access: Requested buyerId is not related to user logged in. Throw forbidden exception.");
			throw ForbiddenException.unauthorizedAccess();
		}

		Buyer buyer = dbClient.queryObject(Buyer.class, id);
		ArgValidator.checkEntityNotNull(buyer, id, true);
		if (!user.isAdmin()) {
			ArgValidator.checkEntityBlacklisted(buyer, id, true);
		}

		BuyerInfoResponse resp = new BuyerInfoResponse();
		DealerResponseMapper.mapBuyer(buyer, resp);

		logger.info("Exit returning buyer info: {}", resp);
		return resp;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/deactivate", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasAnyRole('" + Role.BUYER + "', '" + Role.ADMIN + "') and isRelated(#id)")
	public ResponseEntity<CreateResourceResponse> deactivateBuyer(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String id,
			@RequestParam(name = REQ_PARAM_FORCE_DELETE, defaultValue = "false") String forceDelete)
			throws APIException {

		logger.info("Request received to delete buyer. Buyer id: {}", id);

		ArgValidator.checkFieldUriType(id, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, id);

		if (user.isAdmin()) {
			ArgValidator.checkEntityApproved(buyer, id, true);
		} else {
			// Buyer
			ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, id, true);
		}

		String jobId = DealerUtility.deleteBuyer(id, false, Boolean.valueOf(forceDelete));

		return PipelineUtils.createPipelineJobInfoResponse(jobId, "Task initiated to delete buyer.");
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasAnyRole('" + Role.BUYER + "','" + Role.ADMIN + "') and isRelated(#id)")
	public ResponseEntity<TaskResponse> updateBuyer(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String id, @RequestBody UpdateBuyerParam updateParam) throws APIException {

		logger.info("Request received to update buyer. Buyer id: {}", id);

		ArgValidator.checkFieldNotNull(updateParam, "update_buyer");

		Buyer buyer = (Buyer) DealerUtility.validateUpdateDealerParam(id, updateParam, user, dbClient);

		if (buyer == null) {
			throw BadRequestException.create("There is nothing to update in buyer information.");
		}

		if (user.isBuyer()) {
			ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, id, true);
		}

		dbClient.updateObject(buyer);

		PipelineUtils.startPipelineService(new ReindexBuyerDocumentJob(id));

		return TaskResponseUtility.createTaskSuccessResponse("Buyer information updated successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, value = "/search", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasAnyRole('" + Role.VENDOR + "','" + Role.ADMIN + "') and hasDealership()")
	public BuyerInfoListResponse searchVendors(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_SEARCH_TEXT) String searchText,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) throws APIException {

		logger.info("Request received to search buyers with search text: {}", searchText);

		if (user.isVendor()) {
			String vendorId = user.getRelationshipId();
			Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, false);
		}

		BuyerInfoListResponse resp = new BuyerInfoListResponse();
		Results<ScoredDocument> results = SearchAPIUtility.searchDocuments(Constants.BUYER_INDEX, searchText, fetchSize,
				offset, true);

		for (ScoredDocument doc : results) {
			String buyerId = doc.getId();
			Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
			if (user.isVendor() && buyer.isBlacklisted()) {
				continue;
			}
			BuyerInfoResponse buyerInfo = DealerResponseMapper.mapBuyer(buyer);
			resp.add(buyerInfo);
		}
		Cursor cursor = results.getCursor();
		if (cursor != null) {
			resp.setNextOffset(cursor.toWebSafeString());
		}

		return resp;
	}

}
