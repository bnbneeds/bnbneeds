package com.bnbneeds.app.api.service.controller;

import static com.bnbneeds.app.api.service.response.DBObjectMapper.toNamedRelatedRestResponse;
import static com.bnbneeds.app.api.service.response.PurchaseItemResponseMapper.mapQuantity;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.PurchaseItemResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.ProductName;
import com.bnbneeds.app.db.client.model.PurchaseItem;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.BuyerListResponse;
import com.bnbneeds.app.model.dealer.order.AggregatedPurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.AggregatedPurchaseListResponse.AggregatedPuchaseItem;
import com.bnbneeds.app.model.dealer.order.AggregatedPurchaseListResponse.AggregatedPuchaseItem.DealerPurchaseItemInfo;
import com.bnbneeds.app.model.dealer.order.PurchaseItemInfoResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.PurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.UpdatePurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.UpdatePurchaseListParam;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.ProductNameListResponse;
import com.bnbneeds.app.util.DateUtils;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/vendors/{vendorId}/purchase-items")
public class VendorPurchaseListService extends ResourceService {

	private static final Logger logger = LoggerFactory
			.getLogger(VendorPurchaseListService.class);

	private static final String REQ_PARAM_REQUEST_DATE = "requestDate";
	private static final String REQ_PARAM_START_REQUEST_DATE = "startRequestDate";
	private static final String REQ_PARAM_END_REQUEST_DATE = "endRequestDate";

	private static enum State {
		// RECEIVED, CANNOT_BE_DELIVERED, SHIPPED, DISPATCHED, PROCESSING,
		// DELIVER_QUANTITY_UPDATED,
		DELIVERED, CANNOT_BE_DELIVERED, PARTIALLY_DELIVERED, GENERAL_UPDATE
	}

	@RequestMapping(method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR
			+ "') and hasDealership() and isRelated(#vendorId)")
	public PurchaseListResponse getPurchaseList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info(
				"Request received to get purchase items by filter: {}, fetchSize: {}, offset: {}",
				filter, fetchSize, offset);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");

		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);

		ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);

		final String[] QUERY_FILTER_KEYS = { "name", "productNameId",
				"buyerId", "status", REQ_PARAM_REQUEST_DATE,
				REQ_PARAM_START_REQUEST_DATE, REQ_PARAM_END_REQUEST_DATE };

		PurchaseListResponse response = new PurchaseListResponse();

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields("vendor", vendor);
		boolean requestDateGiven = false;

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case "name":
				ProductName productNameEntity = dbClient.queryObjectByFields(
						ProductName.class, new QueryFields("name", value));
				ArgValidator.checkEntityBlacklisted(productNameEntity, value,
						true);
				fields.add("productName", productNameEntity);
				break;
			case "productNameId":
				ArgValidator.checkFieldUriType(value, ProductName.class, key);
				ProductName productName = dbClient.queryObject(
						ProductName.class, value);
				ArgValidator.checkEntityBlacklisted(productName, value, true);
				fields.add("productName", productName);
				break;
			case "buyerId":
				ArgValidator.checkFieldUriType(value, Buyer.class, key);
				Buyer buyer = dbClient.queryObject(Buyer.class, value);
				ArgValidator.checkEntityBlacklisted(buyer, value, true);
				fields.add("buyer", buyer);
				break;
			case "status":
				ArgValidator.checkFieldValue(!EntityStatus.BLACKLISTED.name()
						.equals(value.toUpperCase()), key);
				fields.add("entityStatus", value.toUpperCase());
				break;
			case REQ_PARAM_REQUEST_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("requestTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("requestTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				fields.add("requestTimestamp >=", startRequestDate.getTime());
				break;
			case REQ_PARAM_END_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils
						.getEndOfDayTimestamp(endRequestDate);
				fields.add("requestTimestamp <=", endTimestamp2);
				break;
			default:
				break;
			}
		}

		ResultSetIterator<PurchaseItem> resultSetIterator = null;

		do {

			resultSetIterator = getResultSetIterator(PurchaseItem.class,
					fields, offset, fetchSize);

			if (resultSetIterator != null) {

				while (resultSetIterator.hasNext()) {

					PurchaseItem item = resultSetIterator.next();
					if (item == null || item.isBlacklisted()) {
						continue;
					}
					Buyer buyer = item.getBuyer();
					if (buyer == null || buyer.isBlacklisted()) {
						continue;
					}

					PurchaseItemInfoResponse infoResponse = PurchaseItemResponseMapper
							.map(item, DATE_FORMAT);
					infoResponse.setLink(Endpoint.VENDOR_PURCHASE_ITEM_INFO
							.get(vendorId, item.getId()));
					response.addPurchaseItemInfo(infoResponse);

				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null
						&& offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}
		} while (response.size() < fetchSize);
		return response;

	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR
			+ "') and hasDealership() and isRelated(#vendorId)")
	public PurchaseItemInfoResponse getPurchaseItem(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId,
			@PathVariable("id") String itemId) {

		logger.info(
				"Request received to retrive item details of {} requested to vendor {}",
				itemId, vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(itemId, PurchaseItem.class, "item_id");

		PurchaseItem purchaseItem = dbClient.queryObject(PurchaseItem.class,
				itemId);

		ArgValidator.checkEntityBlacklisted(purchaseItem, itemId, true);
		ArgValidator.checkEntityBlacklisted(purchaseItem.getBuyer(),
				purchaseItem.getBuyer().getId(), false);

		if (!vendorId.equals(purchaseItem.getVendor().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		ArgValidator.checkEntityBlacklisted(purchaseItem.getVendor(), vendorId,
				true);

		PurchaseItemInfoResponse resp = PurchaseItemResponseMapper.map(
				purchaseItem, DATE_FORMAT);
		if (purchaseItem != null) {
			resp.setLink(Endpoint.VENDOR_PURCHASE_ITEM_INFO.get(vendorId,
					itemId));
		}
		return resp;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR
			+ "') and hasDealership() and isRelated(#vendorId)")
	public ResponseEntity<TaskResponse> updatePurchaseItem(
			@PathVariable("vendorId") String vendorId,
			@PathVariable("id") String itemId,
			@RequestBody PurchaseItemParam param) {

		logger.info(
				"Request received to update item {} requested to vendor {}",
				itemId, vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(itemId, PurchaseItem.class, "item_id");

		PurchaseItem purchaseItem = dbClient.queryObject(PurchaseItem.class,
				itemId);

		ArgValidator.checkEntityDisapprovedOrBlacklisted(purchaseItem, itemId,
				true);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(
				purchaseItem.getVendor(), purchaseItem.getVendor().getId(),
				true);

		if (!vendorId.equals(purchaseItem.getVendor().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		ArgValidator.checkFieldNotNull(param, "purchase_item");
		ArgValidator.checkFieldValue(StringUtils.isEmpty(param.getVendorId()),
				"vendor_id");

		ArgValidator.checkFieldValue(
				StringUtils.isEmpty(param.getRequiredQuantity()),
				"required_quantity");

		ArgValidator
				.checkFieldNotNull(param.getStatusUpdate(), "status_update");

		ArgValidator.checkFieldValueFromEnum(param.getStatusUpdate()
				.getStatus(), "status", State.class);
		ArgValidator.checkFieldNotEmpty(param.getStatusUpdate().getComments(),
				"comments");

		Double deliveredQuantity = param.getDeliveredQuantity();

		if (deliveredQuantity != null) {
			purchaseItem.setDeliveredQuantity(deliveredQuantity);
		}

		purchaseItem.addOrderStatus(TIMESTAMP_FORMAT.format(new Date()), param
				.getStatusUpdate().getStatus(), param.getStatusUpdate()
				.getComments(), purchaseItem.getVendor().getName());

		dbClient.updateObject(purchaseItem);
		String buyerId = purchaseItem.getBuyer().getId();
		recordEvent(SystemEventType.PURCHASE_ITEM_STATUS_UPDATED,
				RecipientType.BUYER, buyerId, itemId, purchaseItem
						.getProductName().getName(),
				Endpoint.BUYER_PURCHASE_ITEM_INFO.get(buyerId, itemId),
				purchaseItem.getVendor().getName());

		return TaskResponseUtility
				.createTaskSuccessResponse("Purchase item updated successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, path = "/associated-product-names", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR
			+ "') and hasDealership() and isRelated(#vendorId)")
	public ProductNameListResponse getAssociatedProductNameList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId,
			@RequestParam(name = REQ_PARAM_FILTER) String filter) {

		logger.info(
				"Request received to get associated product names of purchase list requested to vendor: {}",
				vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);

		ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);

		QueryFields fields = new QueryFields("vendor", vendor);
		/*
		 * Only one inequality filter per query is supported in datastore.
		 * 
		 * fields.add("entityStatus !=", EntityStatus.BLACKLISTED.name());
		 */

		final String[] QUERY_FILTER_KEYS = { REQ_PARAM_REQUEST_DATE,
				REQ_PARAM_START_REQUEST_DATE, REQ_PARAM_END_REQUEST_DATE };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);

		boolean validRequest = keyValueMap.containsKey(REQ_PARAM_REQUEST_DATE)
				|| (keyValueMap.containsKey(REQ_PARAM_START_REQUEST_DATE) && keyValueMap
						.containsKey(REQ_PARAM_END_REQUEST_DATE));

		if (!validRequest) {
			throw BadRequestException.parameterMissing(REQ_PARAM_REQUEST_DATE);
		}

		boolean requestDateGiven = false;

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case REQ_PARAM_REQUEST_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("requestTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("requestTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				fields.add("requestTimestamp >=", startRequestDate.getTime());
				break;
			case REQ_PARAM_END_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils
						.getEndOfDayTimestamp(endRequestDate);
				fields.add("requestTimestamp <=", endTimestamp2);
				break;
			default:
				break;
			}
		}

		List<PurchaseItem> purchaseItemList = dbClient.queryList(
				PurchaseItem.class, fields);

		ProductNameListResponse productNameListResponse = new ProductNameListResponse();
		Set<String> uriSet = new HashSet<String>();
		for (Iterator<PurchaseItem> iterator = purchaseItemList.iterator(); iterator
				.hasNext();) {
			PurchaseItem purchaseItem = iterator.next();
			if (!purchaseItem.isBlacklisted()) {
				ProductName productName = purchaseItem.getProductName();
				if (productName != null && uriSet.add(productName.getId())) {
					productNameListResponse
							.addNamedResourceResponse(toNamedRelatedRestResponse(productName));
				}
			}
		}
		logger.info("Returning product name list of size: {}.",
				productNameListResponse.size());

		return productNameListResponse;
	}

	@RequestMapping(method = RequestMethod.PUT, consumes = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR
			+ "') and hasDealership() and isRelated(#vendorId)")
	public ResponseEntity<TaskResponse> updatePurchaseList(
			@PathVariable("vendorId") String vendorId,
			@RequestBody UpdatePurchaseListParam param) {

		logger.info(
				"Request received to update prchase items requested to vendor {}",
				vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator
				.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, true);

		List<UpdatePurchaseItemParam> purchaseList = param.getPurchaseList();
		ArgValidator.checkFieldNotEmpty(purchaseList, "items");

		DBOperationList operationList = new DBOperationList();
		boolean changed = false;

		Set<String> buyerIds = new HashSet<String>();

		for (Iterator<UpdatePurchaseItemParam> iterator = purchaseList
				.iterator(); iterator.hasNext();) {

			UpdatePurchaseItemParam updatePurchaseItemParam = iterator.next();
			ArgValidator.checkFieldNotNull(updatePurchaseItemParam,
					"update_purchase_item");

			ArgValidator.checkFieldValue(
					StringUtils.isEmpty(updatePurchaseItemParam.getVendorId()),
					"vendor_id");
			ArgValidator.checkFieldValue(StringUtils
					.isEmpty(updatePurchaseItemParam.getRequiredQuantity()),
					"required_quantity");

			ArgValidator.checkFieldNotNull(
					updatePurchaseItemParam.getStatusUpdate(), "status_update");

			ArgValidator.checkFieldValueFromEnum(updatePurchaseItemParam
					.getStatusUpdate().getStatus(), "status", State.class);
			ArgValidator.checkFieldNotEmpty(updatePurchaseItemParam
					.getStatusUpdate().getComments(), "comments");

			String itemId = updatePurchaseItemParam.getPurchaseItemId();

			ArgValidator.checkFieldUriType(itemId, PurchaseItem.class,
					"item_id");

			PurchaseItem purchaseItem = dbClient.queryObject(
					PurchaseItem.class, itemId);

			ArgValidator.checkEntityDisapprovedOrBlacklisted(purchaseItem,
					itemId, true);

			ArgValidator.checkEntityDisapprovedOrBlacklisted(
					purchaseItem.getVendor(), purchaseItem.getVendor().getId(),
					true);

			if (!vendorId.equals(purchaseItem.getVendor().getId())) {
				throw ForbiddenException.unauthorizedAccess();
			}

			Double deliveredQuantity = updatePurchaseItemParam
					.getDeliveredQuantity();

			if (deliveredQuantity != null) {
				purchaseItem.setDeliveredQuantity(deliveredQuantity);
				changed = true;
			}

			purchaseItem.addOrderStatus(TIMESTAMP_FORMAT.format(new Date()),
					updatePurchaseItemParam.getStatusUpdate().getStatus(),
					updatePurchaseItemParam.getStatusUpdate().getComments(),
					purchaseItem.getVendor().getName());

			if (changed) {
				operationList.addOperation(OperationType.UPDATE, purchaseItem);
				buyerIds.add(purchaseItem.getBuyer().getId());
				changed = false;
			}

		}

		logger.info("Updating purchase items requested to vendor {}", vendorId);
		dbClient.transact(operationList);

		recordEvent(SystemEventType.PURCHASE_ITEMS_UPDATED,
				RecipientType.BUYER, buyerIds, null, null, null,
				vendor.getName());

		return TaskResponseUtility
				.createTaskSubmittedResponse("Task submitted to update purchase list successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, path = "/associated-buyers", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR
			+ "') and hasDealership() and isRelated(#vendorId)")
	public BuyerListResponse getAssociatedBuyerList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId,
			@RequestParam(name = REQ_PARAM_FILTER) String filter) {

		logger.info(
				"Request received to get associated buyers of purchase list requested to vendor: {}",
				vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);

		ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);

		QueryFields fields = new QueryFields("vendor", vendor);
		/*
		 * Cannot have more than one inequality operator in a query
		 * fields.add("entityStatus !=", EntityStatus.BLACKLISTED.name());
		 */

		final String[] QUERY_FILTER_KEYS = { REQ_PARAM_REQUEST_DATE,
				REQ_PARAM_START_REQUEST_DATE, REQ_PARAM_END_REQUEST_DATE };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);

		boolean validRequest = keyValueMap.containsKey(REQ_PARAM_REQUEST_DATE)
				|| (keyValueMap.containsKey(REQ_PARAM_START_REQUEST_DATE) && keyValueMap
						.containsKey(REQ_PARAM_END_REQUEST_DATE));

		if (!validRequest) {
			throw BadRequestException.parameterMissing(REQ_PARAM_REQUEST_DATE);
		}

		boolean requestDateGiven = false;

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case REQ_PARAM_REQUEST_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("requestTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("requestTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				fields.add("requestTimestamp >=", startRequestDate.getTime());
				break;
			case REQ_PARAM_END_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils
						.getEndOfDayTimestamp(endRequestDate);
				fields.add("requestTimestamp <=", endTimestamp2);
				break;
			default:
				break;
			}
		}

		List<PurchaseItem> purchaseItemList = dbClient.queryList(
				PurchaseItem.class, fields);

		BuyerListResponse buyerListResponse = new BuyerListResponse();
		Set<String> uriSet = new HashSet<String>();
		for (Iterator<PurchaseItem> iterator = purchaseItemList.iterator(); iterator
				.hasNext();) {
			PurchaseItem purchaseItem = iterator.next();
			if (!purchaseItem.isBlacklisted()) {
				Buyer buyer = purchaseItem.getBuyer();
				if (buyer != null && uriSet.add(buyer.getId())) {
					buyerListResponse
							.addNamedResourceResponse(toNamedRelatedRestResponse(buyer));
				}
			}
		}
		logger.info("Returning buyer list of size: {}.",
				buyerListResponse.size());

		return buyerListResponse;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/aggregated-list", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR
			+ "') and hasDealership() and isRelated(#vendorId)")
	public AggregatedPurchaseListResponse getAggregatedPurchaseList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId,
			@RequestParam(name = REQ_PARAM_FILTER) String filter) {

		logger.info(
				"Request received to get aggregated purchase list requested to vendor: {}",
				vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);

		ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);

		/*
		 * Cannot have more than one field with inequality operator in a query
		 * fields.add("entityStatus !=", EntityStatus.BLACKLISTED.name());
		 */

		final String[] QUERY_FILTER_KEYS = { REQ_PARAM_REQUEST_DATE,
				REQ_PARAM_START_REQUEST_DATE, REQ_PARAM_END_REQUEST_DATE,
				"buyerId", "productNameId" };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);
		boolean validRequest = keyValueMap.containsKey(REQ_PARAM_REQUEST_DATE)
				|| (keyValueMap.containsKey(REQ_PARAM_START_REQUEST_DATE) && keyValueMap
						.containsKey(REQ_PARAM_END_REQUEST_DATE));

		if (!validRequest) {
			throw BadRequestException.parameterMissing(REQ_PARAM_REQUEST_DATE);
		}
		QueryFields fields = new QueryFields("vendor", vendor);
		boolean requestDateGiven = false;

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case REQ_PARAM_REQUEST_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("requestTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("requestTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp2 = startRequestDate.getTime();
				fields.add("requestTimestamp >=", startTimestamp2);
				break;
			case REQ_PARAM_END_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils
						.getEndOfDayTimestamp(endRequestDate);

				fields.add("requestTimestamp <=", endTimestamp2);
				break;
			case "buyerId":
				ArgValidator.checkFieldUriType(value, Buyer.class, key);
				Buyer buyer = dbClient.queryObject(Buyer.class, value);
				ArgValidator.checkEntityBlacklisted(buyer, value, true);
				fields.add("buyer", buyer);
				break;
			case "productNameId":
				ArgValidator.checkFieldUriType(value, ProductName.class, key);
				ProductName productName = dbClient.queryObject(
						ProductName.class, value);
				ArgValidator.checkEntityBlacklisted(productName, value, true);
				fields.add("productName", productName);
				break;
			default:
				break;
			}
		}

		List<PurchaseItem> purchaseItemList = dbClient.queryList(
				PurchaseItem.class, fields);

		AggregatedPurchaseListResponse aggregatedListResponse = new AggregatedPurchaseListResponse();
		String requestDateParam = null;
		if (keyValueMap.containsKey(REQ_PARAM_REQUEST_DATE)) {
			requestDateParam = keyValueMap.get(REQ_PARAM_REQUEST_DATE);

		} else {
			requestDateParam = keyValueMap.get(REQ_PARAM_START_REQUEST_DATE)
					+ " - " + keyValueMap.get(REQ_PARAM_END_REQUEST_DATE);
		}
		aggregatedListResponse.setRequestDate(requestDateParam);

		for (Iterator<PurchaseItem> iterator = purchaseItemList.iterator(); iterator
				.hasNext();) {

			PurchaseItem purchaseItem = iterator.next();
			if (purchaseItem.isBlacklisted()) {
				continue;
			}

			Buyer buyer = purchaseItem.getBuyer();
			ProductName productName = purchaseItem.getProductName();
			String requestDate = DATE_FORMAT.format(new Date(purchaseItem
					.getRequestTimestamp()));

			AggregatedPuchaseItem item = aggregatedListResponse
					.getItemByProductNameId(productName.getId());

			if (item == null) {
				item = new AggregatedPuchaseItem();
				item.setProductName(toNamedRelatedRestResponse(productName));
				item.setQuantity(mapQuantity(purchaseItem));
				aggregatedListResponse.add(item);
			} else {
				item.addRequiredQuantity(purchaseItem.getRequiredQuantity());
				item.addDeliveredQuantity(purchaseItem.getDeliveredQuantity());
			}
			if (buyer != null) {
				DealerPurchaseItemInfo buyerPurchaseItemInfo = new DealerPurchaseItemInfo(
						requestDate, new RelatedResourceRep(
								purchaseItem.getId(), new RestLinkRep("self",
										Endpoint.BUYER_PURCHASE_ITEM_INFO.get(
												buyer.getId(),
												purchaseItem.getId()))),
						toNamedRelatedRestResponse(buyer),
						mapQuantity(purchaseItem));
				String tags = purchaseItem.getTags();

				if (StringUtils.hasText(tags)) {
					buyerPurchaseItemInfo.setTags(tags);
				}
				item.addBuyerPurchaseItem(buyerPurchaseItemInfo);
			}

		}
		logger.info("Returning aggregated list of size: {}.",
				aggregatedListResponse.size());

		return aggregatedListResponse;
	}

}
