package com.bnbneeds.app.api.service.mapreduce;

import java.util.Iterator;
import java.util.List;

import org.springframework.util.Assert;

import com.google.appengine.tools.mapreduce.MapJob;
import com.google.appengine.tools.mapreduce.MapReduceResult;
import com.google.appengine.tools.mapreduce.MapSettings;
import com.google.appengine.tools.pipeline.FutureValue;
import com.google.appengine.tools.pipeline.Job0;

public class IndexPropertiesInMultipleEntitiesJob extends
		Job0<MapReduceResult<Void>> {

	private static final long serialVersionUID = 3740472460677231171L;
	private List<String> entityTypes;
	private List<String> properties;
	private int shardCount;

	public IndexPropertiesInMultipleEntitiesJob(List<String> entityTypes,
			List<String> properties, int shardCount) {
		super();
		Assert.notEmpty(entityTypes, "entityTypes must not be empty.");
		Assert.notEmpty(properties, "properties must not be empty.");
		Assert.isTrue(shardCount > 0, "shardCount must be greater than zero.");
		this.entityTypes = entityTypes;
		this.properties = properties;
		this.shardCount = shardCount;
	}

	@Override
	public FutureValue<MapReduceResult<Void>> run() throws Exception {
		MapSettings settings = MapReduceUtils.getMapSettings();
		FutureValue<MapReduceResult<Void>> task = null;
		for (Iterator<String> iterator = entityTypes.iterator(); iterator
				.hasNext();) {
			String entityType = iterator.next();

			if (task != null) {
				task = futureCall(
						new MapJob<>(IndexEntityPropertiesJobSpecification.jobSpec(
								entityType, properties, shardCount), settings),
						waitFor(task));
			} else {
				task = futureCall(new MapJob<>(
						IndexEntityPropertiesJobSpecification.jobSpec(entityType,
								properties, shardCount), settings));
			}
		}

		return task;
	}
}
