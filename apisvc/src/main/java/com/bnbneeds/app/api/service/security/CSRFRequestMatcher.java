package com.bnbneeds.app.api.service.security;

import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.Assert;

public class CSRFRequestMatcher implements RequestMatcher {

	private static final String REQUESTED_BY_HEADER = "X-Requested-By";

	private Pattern allowedMethods = Pattern
			.compile("^(GET|HEAD|TRACE|OPTIONS)$");
	private static final AntPathMatcher ANT_REQUEST_PATH_MATCHER = new AntPathMatcher();

	private List<String> allowedRequestedByHeaderValues;
	private List<String> ignoreUrls;

	public void setIgnoreUrls(List<String> ignoreUrls) {
		Assert.notEmpty(ignoreUrls, "URLs to be ignored cannot be null");
		this.ignoreUrls = ignoreUrls;
	}

	public void setAllowedRequestedByHeaderValues(
			List<String> allowedRequestedByHeaderValues) {
		Assert.notEmpty(allowedRequestedByHeaderValues,
				"Header values cannot be null");
		this.allowedRequestedByHeaderValues = allowedRequestedByHeaderValues;
	}

	@Override
	public boolean matches(HttpServletRequest request) {

		String requestedByHeader = request.getHeader(REQUESTED_BY_HEADER);
		if (allowedRequestedByHeaderValues.contains(requestedByHeader)) {
			return false;
		}

		String requestURI = request.getRequestURI();
		for (String url : ignoreUrls) {
			if (ANT_REQUEST_PATH_MATCHER.match(url, requestURI)) {
				return false;
			}
		}

		return !allowedMethods.matcher(request.getMethod()).matches();
	}

}
