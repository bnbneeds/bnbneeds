package com.bnbneeds.app.api.service.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.SubscriptionPlanResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.BiddingSubscriptionPlanUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription.SubscriptionState;
import com.bnbneeds.app.db.client.model.SubscriptionPlan.AvailabilityStatus;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscription;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscriptionPlan;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.subscription.BiddingSubscriptionPlanParam.PlanType;
import com.bnbneeds.app.model.dealer.subscription.CreateVendorSubscriptionPlanParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionPlanAvailabilityStatus;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionPlanDefaultStatus;
import com.bnbneeds.app.model.dealer.subscription.UpdateVendorSubscriptionPlanParam;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionPlanInfoListResponse;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionPlanInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/vendors/bidding-subscription-plans")
public class VendorBiddingSubscriptionPlanService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(VendorBiddingSubscriptionPlanService.class);

	/**
	 * Checks if the buyer has a bidding subscription in ACTIVATION_IN_PROGRESS
	 * which uses the given plan and it is available for access
	 * 
	 * @param vendor
	 *            the vendor
	 * @param subscriptionPlan
	 *            the {@code BuyerBiddingSubscriptionPlan}
	 * @return true if buyer can access the plan; false otherwise
	 */
	private boolean canBuyerAccessSubscriptionPlan(Vendor vendor, VendorBiddingSubscriptionPlan subscriptionPlan) {
		// check if there is subscription in ACTIVATION_IN_PROGRESS with this plan for
		// this buyer
		QueryFields fields = new QueryFields("subscriptionPlan", subscriptionPlan);
		fields.add("subscriptionState", SubscriptionState.ACTIVATION_IN_PROGRESS.name());
		fields.add("vendor", vendor);

		VendorBiddingSubscription buyerSubscription = dbClient.queryObjectByFields(VendorBiddingSubscription.class,
				fields);
		if (buyerSubscription == null && !subscriptionPlan.isAvailable()) {
			// If there is no subscription in ACTIVATION_IN_PROGRESS using this plan and
			// the plan is NOT_AVAILABLE, then forbid access to this plan
			return false;
		}
		return true;
	}

	@RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public ResponseEntity<CreateResourceResponse> createSubscriptionPlan(
			@RequestBody CreateVendorSubscriptionPlanParam param, @AuthenticationPrincipal BNBNeedsUser user) {

		logger.info("API request received to add a new subscription plan: {}", param);

		BiddingSubscriptionPlanUtility.validateBiddingSubscriptionPlanRequest(param);

		String planName = param.getName();

		QueryFields queryFields = new QueryFields();
		queryFields.add("name", planName);
		logger.info("querying with name: {}", queryFields);
		VendorBiddingSubscriptionPlan subscriptionPlanEntity = dbClient
				.queryObjectByFields(VendorBiddingSubscriptionPlan.class, queryFields);

		if (subscriptionPlanEntity != null) {
			logger.error("Vendor bidding subscription plan with the given name already exists: {}", param.getName());
			throw BadRequestException.nameAlreadyExists("Vendor bidding subscription plan", param.getName());
		}

		subscriptionPlanEntity = new VendorBiddingSubscriptionPlan();
		String uid = UUIDUtil.createId(VendorBiddingSubscriptionPlan.class);
		subscriptionPlanEntity.setId(uid);
		subscriptionPlanEntity.setName(param.getName());
		subscriptionPlanEntity.setTitle(param.getTitle());
		subscriptionPlanEntity.setDescription(param.getDescription());
		subscriptionPlanEntity.setPremiumAmount(param.getPremiumAmount());

		if (param.isPlanType(PlanType.QUOTA)) {
			ArgValidator.checkFieldValue(param.getNumberOfTendersAllowedToAccess() > 0,
					"number_of_tenders_allowed_to_access");
			subscriptionPlanEntity.setNumberOfTendersAllowedToAccess(param.getNumberOfTendersAllowedToAccess());
			subscriptionPlanEntity.setType(VendorBiddingSubscriptionPlan.PlanType.QUOTA.name());
		} else {
			BiddingSubscriptionPlanUtility.setSubscriptionPlanEntityFromRequestForDurationPlanType(param,
					subscriptionPlanEntity);
		}

		BiddingSubscriptionPlanUtility.setStatusOfNewEntity(subscriptionPlanEntity, user);

		logger.debug("New subscription plan record creating in DB: {}", subscriptionPlanEntity);

		dbClient.createObject(subscriptionPlanEntity);

		logger.info("New subscription plan persisted in DB: {}", uid);

		recordEvent(SystemEventType.BIDDING_SUBSCRIPTION_PLAN_CREATED, RecipientType.ADMIN, uid, param.getName(), null,
				user.getUsername());

		return TaskResponseUtility.createResourceResponse(uid, Endpoint.VENDOR_BIDDING_SUBSCRIPTION_PLAN_INFO,
				"New Subscription plan created successfully");
	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "')")
	public VendorBiddingSubscriptionPlanInfoListResponse getSubscriptionPlanList(
			@AuthenticationPrincipal BNBNeedsUser user, @RequestParam(name = "filter", required = false) String filter,
			@RequestParam(name = "fetchSize", defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = "offset", required = false) String offset) {

		logger.info("Request received to get subscription plans by filter: {}, fetchSize: {}, offset: {}", filter,
				fetchSize, offset);

		if (user.isVendor()) {
			String vendorId = user.getRelationshipId();
			Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
			ArgValidator.checkEntityNotNull(vendor, vendorId, true);
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);
		}

		final String[] QUERY_FILTER_KEYS = { "name", "defaultPlan" };
		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);
		QueryFields fields = new QueryFields();

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case "name":
				fields.add(key, value);
				break;
			case "defaultPlan":
				fields.add(key, Boolean.valueOf(value));
				break;
			default:
				break;
			}
		}

		VendorBiddingSubscriptionPlanInfoListResponse response = new VendorBiddingSubscriptionPlanInfoListResponse();
		ResultSetIterator<VendorBiddingSubscriptionPlan> resultSetIterator = null;
		do {
			resultSetIterator = getResultSetIterator(VendorBiddingSubscriptionPlan.class, fields, offset, fetchSize);
			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					VendorBiddingSubscriptionPlan item = resultSetIterator.next();

					if (item != null) {
						// if user is not Admin, then return only approved Plans
						if (user.isVendor()) {
							if (!item.isApproved()) {
								continue;
							} else if (!item.isAvailable()) {
								continue;
							}
						}
						VendorBiddingSubscriptionPlanInfoResponse subscriptionPlansResponse = SubscriptionPlanResponseMapper
								.mapSubscriptionPlan(item);
						response.add(subscriptionPlansResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}
		} while (response.size() < fetchSize);

		return response;
	}

	/**
	 * This GET API is only used to get and verify the premium amount when
	 * subscription activation is in progress. This means, the linked plan may not
	 * by available for new subscriptions. But if there is buyer subscription in
	 * ACTIVATION_IN_PROGRESS and the plan is marked NOT_AVAILABLE by the admin, the
	 * buyer can still access this plan in order to get the details for making and
	 * verifying the payment.
	 * 
	 * @param user
	 *            the authenticated user; can be admin or vendor
	 * @param planId
	 *            the vendor bidding subscription plan ID
	 * @return the {@code VendorSubscriptionPlanInfoResponse}
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "')")
	public VendorBiddingSubscriptionPlanInfoResponse getSubscriptionPlan(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String planId) {

		logger.info("Request received to retrive subcription plan details of {} ", planId);
		ArgValidator.checkFieldUriType(planId, VendorBiddingSubscriptionPlan.class, "id");

		Vendor vendor = null;
		if (user.isVendor()) {
			String vendorId = user.getRelationshipId();
			vendor = dbClient.queryObject(Vendor.class, vendorId);
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);
		}

		VendorBiddingSubscriptionPlan subscriptionPlan = dbClient.queryObject(VendorBiddingSubscriptionPlan.class,
				planId);

		ArgValidator.checkEntityNotNull(subscriptionPlan, planId, true);
		if (user.isVendor()) {
			ArgValidator.checkEntityDisapprovedOrBlacklisted(subscriptionPlan, planId, true);
			boolean isPlanAccessibleToBuyer = canBuyerAccessSubscriptionPlan(vendor, subscriptionPlan);
			if (!isPlanAccessibleToBuyer) {
				logger.error("Subscription plan is not accessible to the vendor: {}", planId);
				throw ForbiddenException.subscriptionPlanNotAvailable(planId);
			}
		}

		VendorBiddingSubscriptionPlanInfoResponse resp = SubscriptionPlanResponseMapper
				.mapSubscriptionPlan(subscriptionPlan);

		return resp;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public ResponseEntity<TaskResponse> updateSubscriptionPlan(@PathVariable("id") String planId,
			@RequestBody UpdateVendorSubscriptionPlanParam param, @AuthenticationPrincipal BNBNeedsUser user) {

		logger.info("Request received to update subscription plan {} ", planId);

		ArgValidator.checkFieldUriType(planId, VendorBiddingSubscriptionPlan.class, "id");
		ArgValidator.checkFieldNotNull(param, "update_bidding_subscription_plan");

		VendorBiddingSubscriptionPlan subscriptionPlan = dbClient.queryObject(VendorBiddingSubscriptionPlan.class,
				planId);

		ArgValidator.checkEntityApproved(subscriptionPlan, planId, true);

		// check if there are any active subscription with this plan.
		QueryFields fields = new QueryFields("subscriptionPlan", subscriptionPlan);
		VendorBiddingSubscription vendorSubscription = dbClient.queryObjectByFields(VendorBiddingSubscription.class,
				fields);
		if (vendorSubscription != null) {
			throw ForbiddenException.dealerSubscriptionAlreadyExistsForPlan(planId);
		}

		boolean changed = false;
		String title = param.getTitle();
		if (!StringUtils.isEmpty(title)) {
			subscriptionPlan.setTitle(title);
			changed = true;
		}
		String name = param.getName();
		if (!StringUtils.isEmpty(name)) {
			subscriptionPlan.setName(name);
			changed = true;
		}
		String description = param.getDescription();
		if (!StringUtils.isEmpty(description)) {
			subscriptionPlan.setDescription(description);
			changed = true;
		}
		String planType = param.getType();
		if (!StringUtils.isEmpty(planType) && !planType.equalsIgnoreCase(subscriptionPlan.getType())) {
			subscriptionPlan.setType(planType);
			changed = true;
		}

		if (subscriptionPlan.getPlanType() == VendorBiddingSubscriptionPlan.PlanType.DURATION) {
			String durationType = param.getDurationType();
			if (!StringUtils.isEmpty(durationType)) {
				subscriptionPlan.setDurationType(durationType);
				changed = true;
			}
			Integer durationValue = param.getDurationValue();
			if (durationValue != null) {
				subscriptionPlan.setDurationValue(durationValue);
				changed = true;
			}
		}
		if (subscriptionPlan.getPlanType() == VendorBiddingSubscriptionPlan.PlanType.QUOTA) {
			Integer numberOfAllowedTendersToAccess = param.getNumberOfTendersAllowedToAccess();
			if (numberOfAllowedTendersToAccess != null) {
				subscriptionPlan.setNumberOfTendersAllowedToAccess(numberOfAllowedTendersToAccess);
				changed = true;
			}
		}
		Double premiumAmount = param.getPremiumAmount();
		if (!StringUtils.isEmpty(premiumAmount.toString())) {
			subscriptionPlan.setPremiumAmount(premiumAmount);
			changed = true;
		}

		if (changed) {
			dbClient.updateObject(subscriptionPlan);
		}
		recordEvent(SystemEventType.BIDDING_SUBSCRIPTION_PLAN_UPDATED, RecipientType.ADMIN, subscriptionPlan.getId(),
				subscriptionPlan.getName(), Endpoint.VENDOR_BIDDING_SUBSCRIPTION_PLAN_INFO.get(planId),
				user.getUsername());

		return TaskResponseUtility.createTaskSuccessResponse("Subscription plan updated successfully");
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/default-status", consumes = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public ResponseEntity<TaskResponse> updateSubscriptionPlanDefaultStatus(@PathVariable("id") String planId,
			@RequestBody UpdateSubscriptionPlanDefaultStatus param, @AuthenticationPrincipal BNBNeedsUser user) {

		logger.info("Request received to update the default status of subscription plan: {} ", planId);

		ArgValidator.checkFieldUriType(planId, VendorBiddingSubscriptionPlan.class, "id");
		ArgValidator.checkFieldNotNull(param, "update_default_status_of_subscription_plan");
		VendorBiddingSubscriptionPlan subscriptionPlan = dbClient.queryObject(VendorBiddingSubscriptionPlan.class,
				planId);

		ArgValidator.checkEntityApproved(subscriptionPlan, planId, true);

		BiddingSubscriptionPlanUtility.setDefaultStatus(subscriptionPlan, param.getSetAsDefaultPlan(), dbClient);

		recordEvent(SystemEventType.BIDDING_SUBSCRIPTION_PLAN_UPDATED, RecipientType.ADMIN, subscriptionPlan.getId(),
				subscriptionPlan.getName(), Endpoint.VENDOR_BIDDING_SUBSCRIPTION_PLAN_INFO.get(planId),
				user.getUsername());

		return TaskResponseUtility.createTaskSubmittedResponse(
				"Task initiated to update default status of subscription plan successfully");
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/availability-status", consumes = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public ResponseEntity<TaskResponse> updateSubscriptionPlanAvailabilityStatus(
			@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String planId,
			@RequestBody UpdateSubscriptionPlanAvailabilityStatus param) {

		logger.info("Request received to update the availability status of subscription plan: {} ", planId);

		ArgValidator.checkFieldUriType(planId, VendorBiddingSubscriptionPlan.class, "id");
		ArgValidator.checkFieldNotNull(param, "update_availability_status_of_subscription_plan");
		String newStatus = param.getStatus();
		ArgValidator.checkFieldValueFromEnum(newStatus, "status", AvailabilityStatus.class);
		VendorBiddingSubscriptionPlan subscriptionPlan = dbClient.queryObject(VendorBiddingSubscriptionPlan.class,
				planId);
		ArgValidator.checkEntityNotNull(subscriptionPlan, planId, true);
		if (newStatus.equals(subscriptionPlan.getAvailabilityStatus())) {
			throw BadRequestException.parameterInvalid("status",
					"Availability of subscription plan is already " + newStatus);
		}
		subscriptionPlan.setAvailabilityStatus(newStatus);
		dbClient.updateObject(subscriptionPlan);

		recordEvent(SystemEventType.BIDDING_SUBSCRIPTION_PLAN_UPDATED, RecipientType.ADMIN, subscriptionPlan.getId(),
				subscriptionPlan.getName(), Endpoint.VENDOR_BIDDING_SUBSCRIPTION_PLAN_INFO.get(planId),
				user.getUsername());

		return TaskResponseUtility
				.createTaskSuccessResponse("Availability status of the subscription plan updated successfully.");
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public ResponseEntity<TaskResponse> deleteSubscriptionPlan(@PathVariable("id") String id) {

		logger.info("Request received to delete subscription plan with id: {},", id);

		ArgValidator.checkFieldUriType(id, VendorBiddingSubscriptionPlan.class, "id");
		VendorBiddingSubscriptionPlan subscriptionPlan = dbClient.queryObject(VendorBiddingSubscriptionPlan.class, id);

		ArgValidator.checkEntityApproved(subscriptionPlan, id, true);

		// if the plan is associated with a subscription, then it cant't be deleted
		QueryFields queryFields = new QueryFields("subscriptionPlan", subscriptionPlan);
		VendorBiddingSubscription vendorBiddingSubscriptionEntity = dbClient
				.queryObjectByFields(VendorBiddingSubscription.class, queryFields);

		if (vendorBiddingSubscriptionEntity != null) {
			throw ForbiddenException.deleteFailedHasReferences(id, VendorBiddingSubscriptionPlan.class,
					VendorBiddingSubscription.class);
		}

		dbClient.markAsDeleted(subscriptionPlan);

		logger.info("subscription plan: {} deleted successfully.", id);
		return TaskResponseUtility.createTaskSuccessResponse(
				VendorBiddingSubscriptionPlan.class.getSimpleName() + " deleted successfully.");
	}

}