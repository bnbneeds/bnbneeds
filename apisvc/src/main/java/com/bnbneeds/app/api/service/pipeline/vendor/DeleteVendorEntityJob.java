package com.bnbneeds.app.api.service.pipeline.vendor;

import static com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT.queryVendor;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryObjectByFields;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.util.NotificationUtility;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Notification;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeleteVendorEntityJob extends Job2<Void, String, Boolean> {

	private static final long serialVersionUID = 4088173560803116633L;
	private static final Logger logger = LoggerFactory.getLogger(DeleteVendorEntityJob.class);

	@Override
	public Value<Void> run(String vendorId, Boolean deleteUserAccount) throws Exception {

		Vendor vendor = queryVendor(vendorId);
		if (vendor == null) {
			return null;
		}
		DBOperationList operationList = new DBOperationList();
		logger.info("Delete the vendor entity: {}", vendor);
		operationList.addOperation(OperationType.MARK_AS_DELETED, vendor);

		UserAccount userAccount = queryObjectByFields(UserAccount.class, new QueryFields("relationshipId", vendorId));
		if (userAccount != null) {
			if (deleteUserAccount) {
				logger.info("Finally, delete the user account: {}", userAccount);
				operationList.addOperation(OperationType.MARK_AS_DELETED, userAccount);
			} else {
				logger.info("Finally, update the user account by setting relationship id to null");
				userAccount.setRelationshipId(null);
				operationList.addOperation(OperationType.UPDATE, userAccount);
			}
		}
		Notification notification = NotificationUtility.createNotification(SystemEventType.VENDOR_DELETED,
				RecipientType.BUYERS_AND_ADMIN, null, vendorId, vendor.getName(), Endpoint.VENDOR_INFO.get(vendorId),
				null);
		operationList.addOperation(OperationType.INSERT, notification);
		transact(operationList);
		return null;
	}

}