package com.bnbneeds.app.api.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.pipeline.PipelineUtils;
import com.bnbneeds.app.api.service.pipeline.vendor.DeleteProductsJob;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.util.StringUtils;

public abstract class ProductUtility {

	private static final Logger logger = LoggerFactory.getLogger(ProductUtility.class);

	/**
	 * Generates the product's native id.
	 * 
	 * @param vendorId
	 * @param productTypeId
	 * @param productCategoryId
	 * @param productNameId
	 * @param modelId
	 * @return the native Id of the vendor's product
	 */
	public static String generateProductNativeId(String vendorId, String productTypeId, String productCategoryId,
			String productNameId, String modelId) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(vendorId.toUpperCase()).append("+");
		buffer.append(productTypeId.toUpperCase()).append("+");
		buffer.append(productCategoryId.toUpperCase()).append("+");
		buffer.append(productNameId.toUpperCase());

		if (!StringUtils.isEmpty(modelId)) {
			buffer.append("+").append(modelId.toUpperCase());
		}
		return buffer.toString();

	}

	public static String generateProductNativeId(Vendor vendor, Product product) {
		return generateProductNativeId(vendor.getId(), product.getProductType().getId(),
				product.getProductCategory().getId(), product.getProductName().getId(), product.getModelId());
	}

	/**
	 * Checks the product ownership of logged in user.
	 * 
	 * @param dbClient
	 * @param user
	 * @param product
	 * @throws {@code
	 *             ForbiddenException} if user is not the owner of the product.
	 */
	public static void checkProductOwnership(BNBNeedsUser user, Product product) {

		UserAccount userLogin = user.getAccount();

		if (user.isVendor()) {
			if (!product.getVendor().getId().equals(userLogin.getRelationshipId())) {
				throw ForbiddenException.unauthorizedAccess();
			}
		}
	}

	public static String deleteProduct(String productId, boolean forceDelete) {
		return deleteProduct(productId, false, forceDelete);
	}

	public static String deleteProduct(String productId, boolean notifyBuyersAfterDelete, boolean forceDelete) {
		String pipelineId = PipelineUtils.startPipelineService(
				new DeleteProductsJob.DeleteSingleProductJob(productId, notifyBuyersAfterDelete, forceDelete));
		logger.info("Task submitted to delete product: {}. Pipeline ID: {}. URL: {}", productId, pipelineId,
				PipelineUtils.getPipelineStatusUrl(pipelineId));
		return pipelineId;
	}

}
