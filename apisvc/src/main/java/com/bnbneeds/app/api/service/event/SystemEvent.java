package com.bnbneeds.app.api.service.event;

import org.springframework.context.ApplicationEvent;

/**
 * 
 * @author Amit Herlekar
 *
 */
public class SystemEvent extends ApplicationEvent {

	private static final long serialVersionUID = 5678709719556108773L;

	private RecipientType recipientType;
	private String recipientId;
	private String objectId;
	private String ojectName;
	private String link;
	private SystemEventType type;

	public SystemEvent(Object source) {
		super(source);
	}

	public SystemEventType getType() {
		return type;
	}

	public String getLink() {
		return link;
	}

	public void setType(SystemEventType type) {
		this.type = type;
	}

	
	public void setLink(String link) {
		this.link = link;
	}

	public String getObjectId() {
		return objectId;
	}

	public String getOjectName() {
		return ojectName;
	}
	
	public RecipientType getRecipientType() {
		return recipientType;
	}

	public String getRecipientId() {
		return recipientId;
	}

	public void setRecipientType(RecipientType recipientType) {
		this.recipientType = recipientType;
	}

	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public void setOjectName(String ojectName) {
		this.ojectName = ojectName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SystemEvent [");
		if (recipientType != null)
			builder.append("recipientType=").append(recipientType).append(", ");
		if (recipientId != null)
			builder.append("recipientId=").append(recipientId).append(", ");
		if (objectId != null)
			builder.append("objectId=").append(objectId).append(", ");
		if (ojectName != null)
			builder.append("ojectName=").append(ojectName).append(", ");
		if (link != null)
			builder.append("link=").append(link).append(", ");
		if (type != null)
			builder.append("type=").append(type);
		builder.append("]");
		return builder.toString();
	}

	

}
