package com.bnbneeds.app.api.service.response;

import java.util.List;

import com.bnbneeds.app.db.client.model.ApprovedDataObject;
import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.db.client.model.DescribedDataObject;
import com.bnbneeds.app.model.ApprovedDataObjectRestResponse;
import com.bnbneeds.app.model.DataObjectRestResponse;
import com.bnbneeds.app.model.DescribedDataObjectRestResponse;
import com.bnbneeds.app.util.StringUtils;

public abstract class DataObjectResponseMapper {

	public static void mapDataObject(DataObject from, DataObjectRestResponse to) {
		to.setId(from.getId());
		String name = from.getName();
		if (!StringUtils.isEmpty(name)) {
			to.setName(name);
		}
		to.setCreateTimestamp(from.getCreateTimestamp());
		to.setUpdateTimestamp(from.getUpdateTimestamp());
		to.setInactive(from.isInactive());
	}

	public static void mapApprovedDataObject(ApprovedDataObject from,
			ApprovedDataObjectRestResponse to) {

		mapDataObject(from, to);
		to.setEntityStatus(from.getEntityStatus().name());
		List<String> remarks = from.getRemarks();
		if (remarks != null && !remarks.isEmpty()) {
			to.setRemarks(remarks);
		}
	}

	public static void mapDescribedDataObject(DescribedDataObject from,
			DescribedDataObjectRestResponse to) {

		mapApprovedDataObject(from, to);
		String description = from.getDescription();
		if (!StringUtils.isEmpty(description)) {
			to.setDescription(description);
		}
	}

}
