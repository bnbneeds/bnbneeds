package com.bnbneeds.app.api.service.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpMethod;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import com.bnbneeds.app.util.StringUtils;

public class CSRFHeaderFilter extends OncePerRequestFilter {

	private static final String TOKEN_KEY = "X-CSRF-TOKEN";

	private String tokenKey;
	private String requestTokenUrl;
	private String requestTokenKey;
	
	public CSRFHeaderFilter() {
		this.tokenKey = TOKEN_KEY;
		this.requestTokenKey = TOKEN_KEY;
	}

	public void setRequestTokenUrl(String requestTokenUrl) {
		if ((requestTokenUrl != null) && !requestTokenUrl.startsWith("/")) {
			throw new IllegalArgumentException(
					"requestTokenUrl must begin with '/'");
		}
		this.requestTokenUrl = requestTokenUrl;
	}

	public void setTokenKey(String tokenKey) {
		if (!StringUtils.isEmpty(tokenKey)) {
			this.tokenKey = tokenKey;
		}
	}
	
	public void setRequestTokenKey(String requestTokenKey) {
		if (!StringUtils.isEmpty(requestTokenKey)) {
			this.requestTokenKey = requestTokenKey;
		}
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		if (HttpMethod.GET.name().equals(request.getMethod())
				&& requestTokenUrl.equals(request.getRequestURI())) {

			CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class
					.getName());
			if (csrf != null) {

				Cookie cookie = WebUtils.getCookie(request, tokenKey);
				String token = csrf.getToken();
				if (cookie == null || token != null
						&& !token.equals(cookie.getValue())) {
					cookie = new Cookie(requestTokenKey, token);
					cookie.setPath("/");
					response.addCookie(cookie);
				}

			}
		}
		filterChain.doFilter(request, response);
	}

}
