package com.bnbneeds.app.api.service.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.ProductType;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.CreateProductTypeBulkParam;
import com.bnbneeds.app.model.product.CreateProductTypeParam;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/product-types")
public class ProductTypeService extends ResourceMasterAdminService {

	private static final Logger logger = LoggerFactory
			.getLogger(ProductTypeService.class);

	@RequestMapping(method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN
			+ "') and hasDealership()")
	public ResponseEntity<CreateResourceResponse> createProductType(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody CreateProductTypeParam productTypeParam) {

		logger.info("API request received to add a new product type: {}",
				productTypeParam);

		ArgValidator.checkFieldNotEmpty(productTypeParam.getName(), "name");

		QueryFields queryFields = new QueryFields();
		queryFields.add("name", productTypeParam.getName());

		ProductType productType = dbClient.queryObjectByFields(
				ProductType.class, queryFields);

		if (productType != null) {
			logger.error("Product type with same name already exists: {}",
					productTypeParam.getName());
			throw BadRequestException.nameAlreadyExists("Product type",
					productTypeParam.getName());
		}

		productType = new ProductType();
		String uid = UUIDUtil.createId(ProductType.class);
		productType.setId(uid);
		productType.setName(productTypeParam.getName());
		String description = productTypeParam.getDescription();
		if (!StringUtils.isEmpty(description)) {
			productType.setDescription(description);
		}
		setStatusOfNewEntity(productType, user);

		logger.debug("New product type record creating in DB: {}", productType);

		dbClient.createObject(productType);

		logger.info("New product type persisted in DB: {}", uid);
		
		recordEvent(SystemEventType.PRODUCT_TYPE_CREATED,
				RecipientType.ADMIN, uid, productTypeParam.getName(), null,
				user.getUsername());

		return TaskResponseUtility.createResourceResponse(uid,
				Endpoint.PRODUCT_TYPE_INFO,
				"New product type created successfully");
	}

	@RequestMapping(value = "/bulk", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN
			+ "') and hasDealership()")
	public ResponseEntity<TaskResponse> createProductTypes(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody CreateProductTypeBulkParam bulkParam) {

		logger.info("API request received to add product types: {}", bulkParam);

		ArgValidator.checkFieldNotNull(bulkParam, "create_product_types");

		ArgValidator.checkFieldSize(bulkParam.getProductTypeList(),
				"product_types", MAX_ENTITIES_SIZE);

		DBOperationList dbOperationList = new DBOperationList();
		Set<String> namesInRequest = new HashSet<String>();

		for (Iterator<CreateProductTypeParam> iterator = bulkParam
				.getProductTypeList().iterator(); iterator.hasNext();) {
			CreateProductTypeParam requestParam = iterator.next();
			String name = requestParam.getName();

			ArgValidator.checkFieldNotEmpty(name, "name");
			if (namesInRequest.contains(name)) {
				logger.error(
						"Product type with same name already exists in the request: {}",
						name);
				throw BadRequestException.nameAlreadyExists("Product type",
						name);
			}
			namesInRequest.add(name);
			QueryFields queryFields = new QueryFields("name", name);

			ProductType productType = dbClient.queryObjectByFields(
					ProductType.class, queryFields);

			if (productType != null) {
				logger.error("Product type with same name already exists: {}",
						name);
				throw BadRequestException.nameAlreadyExists("Product type",
						name);
			}

			productType = new ProductType();
			String uid = UUIDUtil.createId(ProductType.class);
			productType.setId(uid);
			productType.setName(name);
			String description = requestParam.getDescription();
			if (!StringUtils.isEmpty(description)) {
				productType.setDescription(description);
			}
			setStatusOfNewEntity(productType, user);

			dbOperationList.addOperation(OperationType.INSERT, productType);
			
		}

		logger.debug("New product categories in a DB transaction: {}",
				dbOperationList);

		dbClient.transact(dbOperationList);
		
		recordEvent(SystemEventType.PRODUCT_TYPES_CREATED,
				RecipientType.ADMIN, null, null, null,
				user.getUsername());

		String logMessage = "Task initiated to create product types.";

		logger.info(logMessage);

		return TaskResponseUtility.createTaskSubmittedResponse(logMessage);
	}

	@Override
	public Class<ProductType> getDataModelClass() {
		return ProductType.class;
	}

}
