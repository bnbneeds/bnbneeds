package com.bnbneeds.app.api.service.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;

import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.exceptions.ResourceNotFoundException;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.model.ApprovedDataObject;
import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.util.StringUtils;

/**
 * Utility functions for validating arguments
 */
public class ArgValidator {

	/**
	 * Checks input URI and throws APIException.badRequests.invalidURI if validation
	 * fails
	 * 
	 * @param uri
	 *            the URN to check
	 */
	public static void checkUri(final String urn) {
		if (!UUIDUtil.isValid(urn)) {
			throw BadRequestException.invalidUID(urn);
		}
	}

	/**
	 * Validates that the uri supplied is not null, is of the urn:bnb: scheme and
	 * represents an object of specific type.
	 * 
	 * @param uri
	 *            the URN to check
	 * @param type
	 *            the DataObject class that the URI must represent
	 * @param fieldName
	 *            the name of the field where the uri originated
	 */
	public static void checkFieldUriType(final String uri, final Class<? extends DataObject> type,
			final String fieldName) {
		checkFieldNotNull(uri, fieldName);

		if (!UUIDUtil.isValid(uri)) {
			throw BadRequestException.invalidUID(uri);
		}

		if (!UUIDUtil.isType(uri, type)) {
			throw BadRequestException.invalidUID(uri, type);
		}
	}

	/*
	 * public static void checkUrl(final String url, final String fieldName) { try {
	 * new URL(url);//
	 * NOSONAR("We are creating an URL object to validate the url string") } catch
	 * (MalformedURLException e) { throw
	 * APIException.badRequests.invalidUrl(fieldName, url); } }
	 */

	/**
	 * Validates that the value supplied is not null.
	 * 
	 * @param value
	 *            the value to check
	 * @param fieldName
	 *            the name of the field where the value originated
	 */
	public static void checkFieldNotNull(final Object value, final String fieldName) {
		checkField(value != null, fieldName);
	}

	public static void checkFieldNotNull(final Object value, final String fieldName, String errorMessage) {
		checkField(value != null, fieldName, errorMessage);
	}

	/**
	 * Validates that the value supplied is not an empty string.
	 * 
	 * @param value
	 *            the value to check
	 * @param fieldName
	 *            the name of the field where the value originated
	 */
	public static void checkFieldNotEmpty(final String value, final String fieldName) {
		checkField(StringUtils.hasText(value), fieldName);
	}

	/**
	 * Validates that the value supplied is not null, and not an empty collection
	 * and does not exceed max size
	 * 
	 * @param value
	 *            the value to check
	 * @param fieldName
	 *            the name of the field where the value originated
	 * @param maxSize
	 *            max size allowed
	 */
	public static void checkFieldSize(final Collection<?> value, final String fieldName, int maxSize) {
		checkField(value != null && !value.isEmpty(), fieldName);
		if (value.size() > maxSize) {
			throw BadRequestException.parameterInvalid(fieldName, "Max allowed size is " + maxSize);
		}
	}

	/**
	 * Validates that the value supplied is not null, and not an empty collection
	 * 
	 * @param value
	 *            the value to check
	 * @param fieldName
	 *            the name of the field where the value originated
	 */
	public static void checkFieldNotEmpty(final Collection<?> value, final String fieldName) {
		checkField(value != null && !value.isEmpty(), fieldName);
	}

	/**
	 * Validates that the value supplied is not null, and not an empty map
	 * 
	 * @param value
	 *            the value to check
	 * @param fieldName
	 *            the name of the field where the value originated
	 */
	public static void checkFieldNotEmpty(final Map<?, ?> value, final String fieldName) {
		checkField(value != null && !value.isEmpty(), fieldName);
	}

	/**
	 * Fires BadRequestException if given condition is false for a field
	 * 
	 * @param condition
	 *            The condition to check. If false, it will throw a
	 *            ServiceCodeException
	 * @param fieldName
	 *            The field name to validate
	 */
	public static void checkField(final boolean condition, final String fieldName) {
		if (!condition) {
			throw BadRequestException.parameterMissing(fieldName);
		}
	}

	public static void checkField(final boolean condition, final String fieldName, String errorMessage) {
		if (!condition) {
			throw BadRequestException.parameterInvalid(fieldName, errorMessage);
		}
	}

	public static void checkFieldValue(final boolean condition, final String fieldName) {
		if (!condition) {
			throw BadRequestException.parameterInvalid(fieldName);
		}
	}

	/**
	 * Validates that the value supplied is not null, and matches one of the
	 * expected values
	 * 
	 * @param value
	 *            the value to check
	 * @param fieldName
	 *            the name of the field where the value originated
	 * @param expected
	 *            the set of Enum values to allow
	 */
	public static <E extends Enum<E>> void checkFieldForValueFromEnum(final E value, final String fieldName,
			final EnumSet<E> expected) {
		checkFieldNotNull(value, fieldName);
		checkFieldValueFromEnum(value.name(), fieldName, expected);
	}

	/**
	 * Validates that the value supplied matches one of the expected values' names
	 * 
	 * @param value
	 *            the value to check
	 * @param fieldName
	 *            the name of the field where the value originated
	 * @param expected
	 *            the set of Enum values to allow
	 */
	public static void checkFieldValueFromEnum(final String value, final String fieldName, final EnumSet<?> expected) {
		for (Enum<?> e : expected) {
			if (e.name().equals(value)) {
				return;
			}
		}
		throw BadRequestException.parameterInvalidExpectedValue(fieldName, expected.toString());
	}

	/**
	 * Validates that the value supplied matches one of the expected values' names
	 * 
	 * @param value
	 *            the value to check
	 * @param fieldName
	 *            the name of the field where the value originated
	 * @param enumType
	 *            the enum class the value is expected to be in
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void checkFieldValueFromEnum(final String value, final String fieldName,
			Class<? extends Enum> enumType) {
		checkFieldNotEmpty(value, fieldName);
		checkFieldValueFromEnum(value, fieldName, EnumSet.allOf(enumType));
	}

	/**
	 * Validates that the value supplied matches one of the expected values
	 * 
	 * @param value
	 *            the value to check
	 * @param fieldName
	 *            the name of the field where the value originated
	 * @param expected
	 *            the set of values to allow ignoring case
	 */
	public static void checkFieldValueFromArray(final Object value, final String fieldName, final Object... expected) {
		for (Object entry : expected) {
			if (entry.equals(value)) {
				return;
			}
		}
		throw BadRequestException.parameterInvalidExpectedValue(fieldName, Arrays.asList(expected).toString());
	}

	/**
	 * Validates that the value supplied matches one of the expected values ignoring
	 * case
	 * 
	 * @param value
	 *            the value to check
	 * @param fieldName
	 *            the name of the field where the value originated
	 * @param expected
	 *            the set of values to allow ignoring case
	 */
	public static void checkFieldValueFromArrayIgnoreCase(final String value, final String fieldName,
			final String... expected) {
		for (String entry : expected) {
			if (entry.equalsIgnoreCase(value)) {
				return;
			}
		}
		throw BadRequestException.parameterInvalidExpectedValue(fieldName, Arrays.asList(expected).toString());
	}

	/**
	 * Validates that a condition has passed, providing original and expected values
	 * for a clear error message
	 * 
	 * @param condition
	 *            the result of checking the value, if false then an exception will
	 *            be thrown
	 * @param fieldName
	 *            the name of the field where the value originated
	 * @param value
	 *            the value that was checked, used for the error message
	 *            presentation
	 * @param expected
	 *            the set of allowable values, used for the error message
	 *            presentation
	 */
	public static void checkFieldValueWithExpected(final boolean condition, final String fieldName, final Object value,
			final Object... expected) {
		if (!condition) {
			throw BadRequestException.parameterInvalidExpectedValue(fieldName, expected.toString());
		}
	}

	/**
	 * Validates that a condition has passed, providing original and expected values
	 * for a clear error message
	 * 
	 * @param condition
	 *            the result of checking the value, if false then an exception will
	 *            be thrown
	 * @param fieldName
	 *            the name of the field where the value originated
	 * @param value
	 *            the value that was checked, used for the error message
	 *            presentation
	 * @param expected
	 *            the set of allowable values, used for the error message
	 *            presentation
	 */
	public static void checkFieldValueWithExpected(final boolean condition, final String fieldName, final Object value,
			final Collection<Object> expected) {
		if (!condition) {
			throw BadRequestException.parameterInvalidExpectedValue(fieldName, expected.toString());
		}
	}

	/**
	 * Validates that a named field contains a valid IPv4 or IPv6 address
	 * 
	 * @param ip
	 *            a string representation of an IPv4 or IPv6 address
	 * @param fieldName
	 *            the name of the field where the value originated
	 */
	/*
	 * public static void checkFieldValidIP(final String ip, final String fieldName)
	 * { checkFieldNotEmpty(ip, fieldName); if (!isValidIPV4(ip) &&
	 * !isValidIPV6(ip)) { throw BadRequestException.parameterInvalid(fieldName); }
	 * }
	 */

	/**
	 * Validates that a named field contains a valid IPv4 address
	 * 
	 * @param ip
	 *            a string representation of an IPv4 address
	 * @param fieldName
	 *            the name of the field where the value originated
	 */
	/*
	 * public static void checkFieldValidIPV4(final String ip, final String
	 * fieldName) { checkFieldNotEmpty(ip, fieldName); if (!isValidIPV4(ip)) { throw
	 * APIException.badRequests.invalidParameterInvalidIPV4(fieldName, ip); } }
	 */

	/**
	 * Validates that a named field contains a valid IPv6 address
	 * 
	 * @param ip
	 *            a string representation of an IPv6 address
	 * @param fieldName
	 *            the name of the field where the value originated
	 */
	/*
	 * public static void checkFieldValidIPV6(final String ip, final String
	 * fieldName) { checkFieldNotEmpty(ip, fieldName); if (!isValidIPV6(ip)) { throw
	 * APIException.badRequests.invalidParameterInvalidIPV6(fieldName, ip); } }
	 */

	/**
	 * Validates that the supplied DataObject is not null. Null entities will result
	 * in a 404 Not Found exception being thrown if idEmbeddedInURL is true, or a
	 * 400 Bad Request otherwise.
	 * 
	 * @param object
	 *            the DataObject instance to verify
	 * @param id
	 *            the id of the null object, used for the error message presentation
	 * @param idEmbeddedInURL
	 *            true if and only if the id was supplied in the URL
	 */
	public static void checkEntityNotNull(final DataObject object, final String id, boolean idEmbeddedInURL) {
		if (object == null) {
			if (idEmbeddedInURL) {
				throw ResourceNotFoundException.idNotFound(id);
			} else {
				throw BadRequestException.invalidUID(id);
			}
		}
	}

	/**
	 * Validates that the supplied DataObject is not null AND is not inactive. Null
	 * or inactive entities will result in a 404 Not Found exception being thrown if
	 * idEmbeddedInURL is true, or a 400 Bad Request otherwise.
	 * 
	 * @param object
	 *            the DataObject instance to verify
	 * @param id
	 *            the id of the null object, used for the error message presentation
	 * @param idEmbeddedInURL
	 *            true if and only if the id was supplied in the URL
	 */
	public static void checkEntity(final DataObject object, final String id, final boolean idEmbeddedInURL) {
		checkEntityNotNull(object, id, idEmbeddedInURL);

		if (object.isInactive()) {
			if (idEmbeddedInURL) {
				throw ResourceNotFoundException.objectIsInactive(id);
			} else {
				throw BadRequestException.requestObjectIsInactive(id);
			}
		}
	}

	public static void checkEntityBlacklisted(final ApprovedDataObject object, final String id,
			final boolean idEmbeddedInURL) {
		checkEntityNotNull(object, id, idEmbeddedInURL);

		if (object.isBlacklisted()) {
			if (idEmbeddedInURL) {
				throw ForbiddenException.objectIsBlacklisted(id);
			} else {
				throw BadRequestException.requestObjectIsBlacklisted(id);
			}
		}
	}

	public static void checkEntityApproved(final ApprovedDataObject object, final String id,
			final boolean idEmbeddedInURL) {
		checkEntityNotNull(object, id, idEmbeddedInURL);

		if (object.isApproved()) {
			if (idEmbeddedInURL) {
				throw ForbiddenException.objectIsApproved(id);
			} else {
				throw BadRequestException.requestObjectIsApproved(id);
			}
		}
	}

	public static void checkEntityDisapproved(final ApprovedDataObject object, final String id,
			final boolean idEmbeddedInURL) {
		checkEntityNotNull(object, id, idEmbeddedInURL);

		if (object.isDispproved()) {
			if (idEmbeddedInURL) {
				throw ForbiddenException.objectIsDisapproved(id);
			} else {
				throw BadRequestException.requestObjectIsDisapproved(id);
			}
		}
	}

	public static void checkEntityDisapprovedOrBlacklisted(final ApprovedDataObject object, final String id,
			final boolean idEmbeddedInURL) {
		checkEntityDisapproved(object, id, idEmbeddedInURL);
		checkEntityBlacklisted(object, id, idEmbeddedInURL);
	}

	/**
	 * Validates that the supplied DataObject is not null AND (conditionally) is not
	 * inactive. Failing entities will result in a 404 Not Found exception being
	 * thrown if idEmbeddedInURL is true, or a 400 Bad Request otherwise.
	 * 
	 * @param object
	 *            the DataObject instance to verify
	 * @param id
	 *            the id of the null object, used for the error message presentation
	 * @param idEmbeddedInURL
	 *            true if and only if the id was supplied in the URL
	 * @param checkInactive
	 *            true if and only if an active DataObject is required
	 */
	/*
	 * public static void checkEntity(final DataObject object, final URI id, final
	 * boolean idEmbeddedInURL, final boolean checkInactive) {
	 * checkEntityNotNull(object, id, idEmbeddedInURL);
	 * 
	 * if (checkInactive && object.getInactive()) { if (idEmbeddedInURL) { throw
	 * APIException.notFound.entityInURLIsInactive(id); } else { throw
	 * APIException.badRequests.entityInRequestIsInactive(id); } } }
	 */

	/**
	 * Validates IPV4 address using regex for the given ipAddress
	 * 
	 * @param ipAddress
	 *            IP Address
	 * @return {@link Boolean} status flag
	 */
	/*
	 * private static Boolean isValidIPV4(final String ipAddress) { boolean status =
	 * false; if (StringUtils.isNotEmpty(ipAddress)) { status =
	 * InetAddressUtils.isIPv4Address(ipAddress); } return status; }
	 */

	/**
	 * Validates IPV6 address using regex for the given ipAddress
	 * 
	 * @param ipAddress
	 *            IP Address
	 * @return {@link Boolean} status flag
	 */
	/*
	 * private static Boolean isValidIPV6(final String ipAddress) { boolean status =
	 * false; if (StringUtils.isNotEmpty(ipAddress)) { status =
	 * InetAddressUtils.isIPv6Address(ipAddress); } return status; }
	 */

	/**
	 * Validates if a label contains only alphanumeric values
	 * 
	 * @param label
	 *            Label to validate
	 * @return {@link Boolean} status flag
	 */
	/*
	 * private static Boolean isAlphanumeric(final String label) { boolean status =
	 * false; if (StringUtils.isNotEmpty(label)) { Matcher matcher =
	 * patternAlphanumeric.matcher(label); status = matcher.matches(); } return
	 * status; }
	 */

	/**
	 * Validates that a named field is of minimum or greater value.
	 * 
	 * @param value
	 *            the suppled number to check
	 * @param minimum
	 *            the minimum acceptable value
	 * @param fieldName
	 *            the name of the field where the value originated
	 */

	public static void checkFieldMinimum(final long value, final long minimum, final String fieldName) {
		checkFieldMinimum(value, minimum, "", fieldName);
	}

	/**
	 * Validates that a named field is of minimum or greater value.
	 * 
	 * @param value
	 *            the suppled number to check
	 * @param minimum
	 *            the minimum acceptable value
	 * @param units
	 *            the units that the value represents, used for error message
	 *            presentation
	 * @param fieldName
	 *            the name of the field where the value originated
	 */

	public static void checkFieldMinimum(final long value, final long minimum, final String units,
			final String fieldName) {
		if (value < minimum) {
			throw BadRequestException.invalidParameterBelowMinimum(fieldName, value, minimum, units);
		}
	}

	/**
	 * Validates that a named field is of maximum or lesser value.
	 * 
	 * @param value
	 *            the suppled number to check
	 * @param maximum
	 *            the maximum acceptable value
	 * @param fieldName
	 *            the name of the field where the value originated
	 */

	public static void checkFieldMaximum(final long value, final long maximum, final String fieldName) {
		checkFieldMaximum(value, maximum, "", fieldName);
	}

	/**
	 * Validates that a named field is of maximum or lesser value.
	 * 
	 * @param value
	 *            the suppled number to check
	 * @param maximum
	 *            the maximum acceptable value
	 * @param units
	 *            the units that the value represents, used for error message
	 *            presentation
	 * @param fieldName
	 *            the name of the field where the value originated
	 */

	public static void checkFieldMaximum(final long value, final long maximum, final String units,
			final String fieldName) {
		if (value > maximum) {
			throw BadRequestException.invalidParameterAboveMaximum(fieldName, value, maximum, units);
		}
	}

	/**
	 * Validates that a named field is within the inclusive range specified.
	 * 
	 * @param value
	 *            the suppled number to check
	 * @param minimum
	 *            the minimum acceptable value
	 * @param maximum
	 *            the maximum acceptable value
	 * @param fieldName
	 *            the name of the field where the value originated
	 */

	public static void checkFieldRange(final long value, final long minimum, final long maximum,
			final String fieldName) {
		checkFieldRange(value, minimum, maximum, "", fieldName);
	}

	/**
	 * Validates that a named field is within the inclusive range specified.
	 * 
	 * @param value
	 *            the suppled number to check
	 * @param minimum
	 *            the minimum acceptable value
	 * @param maximum
	 *            the maximum acceptable value
	 * @param units
	 *            the units that the value represents, used for error message
	 *            presentation
	 * @param fieldName
	 *            the name of the field where the value originated
	 */

	public static void checkFieldRange(final long value, final long minimum, final long maximum, final String units,
			final String fieldName) {
		if (value < minimum || value > maximum) {
			throw BadRequestException.parameterNotWithinRange(fieldName, value, minimum, maximum, "");
		}
	}

}
