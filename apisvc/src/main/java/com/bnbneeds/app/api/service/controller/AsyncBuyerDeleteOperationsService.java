package com.bnbneeds.app.api.service.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.pipeline.buyer.DeleteBuyerJobROOT;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.AsyncTaskUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.BuyerAverageRating;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscription;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscriptionUsage;
import com.bnbneeds.app.db.client.model.BuyerReview;
import com.bnbneeds.app.db.client.model.Enquiry;
import com.bnbneeds.app.db.client.model.PaymentTransactionLinkedEntity;
import com.bnbneeds.app.db.client.model.ProductReview;
import com.bnbneeds.app.db.client.model.PurchaseItem;
import com.bnbneeds.app.db.client.model.Task;
import com.bnbneeds.app.db.client.model.Tender;
import com.bnbneeds.app.db.client.model.TenderBid;
import com.bnbneeds.app.db.client.model.TenderProduct;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.db.client.model.VendorReview;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;

/**
 * This class using GAE push queue for asynchronous task execution. Although it
 * is a queue, the tasks are not executed in FIFO order. So the status of each
 * task has to be tracked manually using persistence. This class is saved for
 * learning purposes only.
 * 
 * @see DeleteBuyerJobROOT
 * @author Amit Herlekar
 *
 */
@Deprecated
@RestController
@RequestMapping(method = RequestMethod.POST, path = AsyncBuyerDeleteOperationsService.BASE_REQUEST_PATH)
public class AsyncBuyerDeleteOperationsService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(AsyncBuyerDeleteOperationsService.class);

	protected static final String BASE_REQUEST_PATH = "/async-tasks/buyer";

	private static final String DELETE_BUYER_TASK_WORKER_URI = "/delete-buyer";
	private static final String DELETE_PURCHASE_ITEMS_TASK_WORKER_URI = "/delete-purchase-items";
	private static final String DELETE_REVIEWS_TASK_WORKER_URI = "/delete-reviews";
	private static final String DELETE_REVIEWS_IN_VENDOR_REVIEWS_TASK_WORKER_URI = "/delete-reviews-in-vendor-reviews";
	private static final String DELETE_REVIEWS_IN_PRODUCT_REVIEWS_TASK_WORKER_URI = "/delete-reviews-in-product-reviews";
	private static final String DELETE_BIDDING_SUBSCRIPTION_USAGES_TASK_WORKER_URI = "/delete-bidding-subscription-usages";
	private static final String DELETE_TENDERS_TASK_WORKER_URI = "/delete-tenders";
	private static final String DELETE_TENDER_TASK_WORKER_URI = "/delete-tender";
	private static final String DELETE_BIDDING_SUBSCRIPTIONS_TASK_WORKER_URI = "/delete-bidding-subscriptions";
	private static final String DELETE_PAYMENT_TRANSACTIONS_TASK_WORKER_URI = "/delete-payment-transactions";
	private static final String DELETE_PRODUCT_ENQUIRIES_TASK_WORKER_URI = "/delete-product-enquiries";

	private static final String QUEUE_NAME = "delete-dealer-task-queue";
	private static final String BUYER_ID_QUERY_PARAM = "buyerId";
	private static final String FORCE_DELETE_QUERY_PARAM = "forceDelete";
	private static final String TASK_ID_QUERY_PARAM = "taskId";

	private static void addTaskToQueue(String workerUri, Map<String, String> paramMap) {
		String absoluteURI = String.format("%1$s%2$s%3$s", Endpoint.CONTEXT_PATH, BASE_REQUEST_PATH, workerUri);
		AsyncTaskUtility.addTaskToQueue(QUEUE_NAME, absoluteURI, paramMap);
	}

	private Buyer queryBuyer(String buyerId) {
		ArgValidator.checkFieldUriType(buyerId, Buyer.class, BUYER_ID_QUERY_PARAM);
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		logger.info("Is buyer info of buyerID {} null?: [{}]", buyerId, buyer == null);
		ArgValidator.checkEntityNotNull(buyer, buyerId, false);
		return buyer;
	}

	private QueryFields buyerField(Buyer buyer) {
		return new QueryFields("buyer", buyer);
	}

	public static void enqueueDeletePurchaseItemsTask(String buyerId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_PURCHASE_ITEMS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_PURCHASE_ITEMS_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deletePurchaseItems(@RequestParam(BUYER_ID_QUERY_PARAM) String buyerId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);

		logger.info("Executing worker: delete purchase items...");

		Buyer buyer = queryBuyer(buyerId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);
		List<PurchaseItem> purchaseItems = dbClient.queryList(PurchaseItem.class, buyerField(buyer));
		if (!CollectionUtils.isEmpty(purchaseItems)) {
			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(buyerId, Buyer.class, PurchaseItem.class);
			}

			logger.info("Get purchase items of the buyer and delete them...");
			DBOperationList purchaseItemDeleteOperationList = new DBOperationList();
			for (Iterator<PurchaseItem> iterator = purchaseItems.iterator(); iterator.hasNext();) {
				PurchaseItem purchaseItem = iterator.next();
				logger.debug("Deleting purchase item: {}", purchaseItem);
				/*
				 * Must call delete because the entity type PurchaseItem is not picked by delete
				 * inactive entities cron job
				 */
				purchaseItemDeleteOperationList.addOperation(OperationType.DELETE, purchaseItem);
			}
			taskHelper.transact(purchaseItemDeleteOperationList, taskId);
		}
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Purchase items of deleted successfully.");
	}

	public static void enqueueDeleteReviewsTask(String buyerId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_REVIEWS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_REVIEWS_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteBuyerReviews(@RequestParam(BUYER_ID_QUERY_PARAM) String buyerId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);

		logger.info("Executing worker: delete buyer reviews...");

		Buyer buyer = queryBuyer(buyerId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);
		List<BuyerReview> buyerReviews = dbClient.queryList(BuyerReview.class, buyerField(buyer));
		if (!CollectionUtils.isEmpty(buyerReviews)) {

			DBOperationList reviewDeleteOperationList = new DBOperationList();

			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(buyerId, Buyer.class, BuyerReview.class);
			}

			logger.info("Get the reviews of the buyer and delete them...");
			for (Iterator<BuyerReview> iterator = buyerReviews.iterator(); iterator.hasNext();) {
				BuyerReview buyerReview = iterator.next();
				reviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, buyerReview);
			}

			BuyerAverageRating averageRating = dbClient.queryObjectByFields(BuyerAverageRating.class,
					buyerField(buyer));
			if (averageRating != null) {
				logger.debug("Delete the buyer average rating.");
				reviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, averageRating);
			}
			taskHelper.transact(reviewDeleteOperationList, taskId);
		}
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Buyer reviews deleted successfully.");
	}

	public static void enqueueDeleteReviewsInVendorReviewsTask(String buyerId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_REVIEWS_IN_VENDOR_REVIEWS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_REVIEWS_IN_VENDOR_REVIEWS_TASK_WORKER_URI, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteReviewsInVendorReviews(@RequestParam(BUYER_ID_QUERY_PARAM) String buyerId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);

		logger.info("Executing worker: delete reviews in vendor reviews...");

		Buyer buyer = queryBuyer(buyerId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);
		List<VendorReview> vendorReviews = dbClient.queryList(VendorReview.class, new QueryFields("reviewedBy", buyer));

		if (!CollectionUtils.isEmpty(vendorReviews)) {

			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(buyerId, Buyer.class, VendorReview.class);
			}
			DBOperationList reviewDeleteOperationList = new DBOperationList();

			logger.info("Get the reviews of the buyer and delete them...");
			for (Iterator<VendorReview> iterator = vendorReviews.iterator(); iterator.hasNext();) {
				VendorReview vendorReview = iterator.next();
				reviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, vendorReview);
			}
			taskHelper.transact(reviewDeleteOperationList, taskId);
		}
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Buyer reviews in vendor reviews deleted successfully.");
	}

	public static void enqueueDeleteReviewsInProductReviewsTask(String buyerId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_REVIEWS_IN_PRODUCT_REVIEWS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_REVIEWS_IN_PRODUCT_REVIEWS_TASK_WORKER_URI, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteReviewsInProductReviews(
			@RequestParam(BUYER_ID_QUERY_PARAM) String buyerId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);

		logger.info("Executing worker: delete reviews in product reviews...");

		Buyer buyer = queryBuyer(buyerId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);
		List<ProductReview> productReviews = dbClient.queryList(ProductReview.class,
				new QueryFields("reviewedBy", buyer));

		if (!CollectionUtils.isEmpty(productReviews)) {

			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(buyerId, Buyer.class, ProductReview.class);
			}
			DBOperationList reviewDeleteOperationList = new DBOperationList();

			logger.info("Get the product reviews of the buyer and delete them...");
			for (Iterator<ProductReview> iterator = productReviews.iterator(); iterator.hasNext();) {
				ProductReview productReview = iterator.next();
				reviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, productReview);
			}
			taskHelper.transact(reviewDeleteOperationList, taskId);
		}
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Buyer reviews in product reviews deleted successfully.");
	}

	public static void enqueueDeleteBuyerTask(String buyerId, boolean deleteUserAccount) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put("deleteUserAccount", String.valueOf(deleteUserAccount));
		addTaskToQueue(DELETE_BUYER_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_BUYER_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteBuyerTask(@RequestParam(BUYER_ID_QUERY_PARAM) String buyerId,
			@RequestParam("deleteUserAccount") String deleteUserAccount,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);

		logger.info("Executing worker: delete buyer entity...");

		Buyer buyer = queryBuyer(buyerId);
		Boolean deleteUserAccountFlag = new Boolean(deleteUserAccount);
		DBOperationList operationList = new DBOperationList();

		logger.info("Delete the buyer entity: {}", buyer);
		operationList.addOperation(OperationType.MARK_AS_DELETED, buyer);

		UserAccount userAccount = dbClient.queryObjectByFields(UserAccount.class,
				new QueryFields("relationshipId", buyerId));
		if (userAccount != null) {
			if (deleteUserAccountFlag) {
				logger.info("Finally, delete the user account: {}", userAccount);
				operationList.addOperation(OperationType.MARK_AS_DELETED, userAccount);
			} else {
				logger.info("Finally, updating the user account by setting relationship id to null");
				userAccount.setRelationshipId(null);
				operationList.addOperation(OperationType.UPDATE, userAccount);
			}
		}

		taskHelper.transact(operationList, taskId);
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Buyer deleted successfully");
	}

	public static void enqueueDeleteBiddingSubscriptionUsagesTask(String buyerId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_BIDDING_SUBSCRIPTION_USAGES_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_BIDDING_SUBSCRIPTION_USAGES_TASK_WORKER_URI, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteBiddingSubscriptionUsageTask(
			@RequestParam(BUYER_ID_QUERY_PARAM) String buyerId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);

		logger.info("Executing worker: delete buyer bidding subscription usages...");

		Buyer buyer = queryBuyer(buyerId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);

		List<BuyerBiddingSubscriptionUsage> biddingSubscriptionUsages = dbClient
				.queryList(BuyerBiddingSubscriptionUsage.class, buyerField(buyer));

		if (!CollectionUtils.isEmpty(biddingSubscriptionUsages)) {
			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(buyerId, Buyer.class,
						BuyerBiddingSubscriptionUsage.class);
			}

			logger.info("Get the bidding subscription usages of the buyer and delete them...");

			DBOperationList operationList = new DBOperationList();

			for (Iterator<BuyerBiddingSubscriptionUsage> iterator = biddingSubscriptionUsages.iterator(); iterator
					.hasNext();) {
				BuyerBiddingSubscriptionUsage biddingSubscriptionUsage = iterator.next();
				operationList.addOperation(OperationType.MARK_AS_DELETED, biddingSubscriptionUsage);
			}

			taskHelper.transact(operationList, taskId);
		}
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Buyer bidding subscription usages deleted successfully");
	}

	public static void enqueueDeleteTendersTask(String buyerId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_TENDERS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_TENDERS_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteTendersTask(@RequestParam(BUYER_ID_QUERY_PARAM) String buyerId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);

		logger.info("Executing worker: delete buyer tenders...");

		Buyer buyer = queryBuyer(buyerId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);

		List<Tender> tenders = dbClient.queryList(Tender.class, buyerField(buyer));

		if (!CollectionUtils.isEmpty(tenders)) {
			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(buyerId, Buyer.class, Tender.class);
			}

			logger.info("Get the tenders of the buyer and delete them...");
			Task task = null;
			for (Iterator<Tender> iterator = tenders.iterator(); iterator.hasNext();) {
				Tender tender = iterator.next();
				task = taskHelper.createTask(tender.getId(), "Delete tender with ID: [" + tender.getId() + "]", task);
				enqueueDeleteTenderTask(tender.getId(), forceDeleteFlag, task);
			}
		}

		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSubmittedResponse("Task submitted to delete tenders.");
	}

	private void enqueueDeleteTenderTask(String tenderId, Boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("tenderId", tenderId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_TENDER_TASK_WORKER_URI, paramMap);
	}

	public static void enqueueDeleteTenderTask(String tenderId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("tenderId", tenderId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_TENDER_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_TENDER_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteTenderTask(@RequestParam("tenderId") String tenderId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);

		logger.info("Executing worker: delete tender and all its dependents...");

		Boolean forceDeleteFlag = new Boolean(forceDelete);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, tenderId);
		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityNotNull(tender, tenderId, false);
		QueryFields tenderField = new QueryFields("tender", tender);
		DBOperationList bidProductDeleteOperationList = new DBOperationList();
		DBOperationList bidDeleteOperationList = new DBOperationList();

		List<TenderProduct> bidProducts = dbClient.queryList(TenderProduct.class, tenderField);
		if (!CollectionUtils.isEmpty(bidProducts)) {
			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(tenderId, Tender.class, TenderProduct.class);
			}

			logger.info("Get the bid products of the tender and delete them...");

			for (Iterator<TenderProduct> iterator = bidProducts.iterator(); iterator.hasNext();) {
				TenderProduct bidProduct = iterator.next();
				bidProductDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, bidProduct);
			}

		}

		List<TenderBid> bids = dbClient.queryList(TenderBid.class, tenderField);
		if (!CollectionUtils.isEmpty(bids)) {
			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(tenderId, Tender.class, TenderBid.class);
			}

			logger.info("Get the bids of the tender and delete them...");
			for (Iterator<TenderBid> iterator = bids.iterator(); iterator.hasNext();) {
				TenderBid bid = iterator.next();
				bidDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, bid);
			}
		}

		bidDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, tender);

		taskHelper.transact(bidProductDeleteOperationList, taskId);
		taskHelper.transact(bidDeleteOperationList, taskId);
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Tender deleted successfully");
	}

	public static void enqueueDeleteBiddingSubscriptionsTask(String buyerId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_BIDDING_SUBSCRIPTIONS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_BIDDING_SUBSCRIPTIONS_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteBiddingSubscriptionsTask(
			@RequestParam(BUYER_ID_QUERY_PARAM) String buyerId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);

		logger.info("Executing worker: delete buyer bidding subscriptions...");

		Buyer buyer = queryBuyer(buyerId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);

		List<BuyerBiddingSubscription> biddingSubscriptions = dbClient.queryList(BuyerBiddingSubscription.class,
				buyerField(buyer));

		if (!CollectionUtils.isEmpty(biddingSubscriptions)) {
			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(buyerId, Buyer.class,
						BuyerBiddingSubscription.class);
			}

			logger.info("Get the bidding subscriptions of the buyer and delete them...");

			DBOperationList operationList = new DBOperationList();

			for (Iterator<BuyerBiddingSubscription> iterator = biddingSubscriptions.iterator(); iterator.hasNext();) {
				BuyerBiddingSubscription biddingSubscription = iterator.next();
				operationList.addOperation(OperationType.MARK_AS_DELETED, biddingSubscription);
			}

			taskHelper.transact(operationList, taskId);
		}
		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Buyer bidding subscriptions deleted successfully");
	}

	public static void enqueueDeletePaymentTransactionsTask(String buyerId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_PAYMENT_TRANSACTIONS_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_PAYMENT_TRANSACTIONS_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deletePaymentTransactionsTask(
			@RequestParam(BUYER_ID_QUERY_PARAM) String buyerId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);

		logger.info("Executing worker: delete buyer payment transactions...");

		queryBuyer(buyerId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);

		List<PaymentTransactionLinkedEntity> paymentTransactions = dbClient
				.queryList(PaymentTransactionLinkedEntity.class, new QueryFields("dealerId", buyerId));

		if (!CollectionUtils.isEmpty(paymentTransactions)) {
			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(buyerId, Buyer.class,
						PaymentTransactionLinkedEntity.class);
			}

			logger.info("Get the payment transactions of the buyer and delete them...");
			DBOperationList operationList = new DBOperationList();

			for (Iterator<PaymentTransactionLinkedEntity> iterator = paymentTransactions.iterator(); iterator
					.hasNext();) {
				PaymentTransactionLinkedEntity paymentTransaction = iterator.next();
				operationList.addOperation(OperationType.MARK_AS_DELETED, paymentTransaction.getPaymentTransaction());
				operationList.addOperation(OperationType.MARK_AS_DELETED, paymentTransaction);
			}

			taskHelper.transact(operationList, taskId);
		}

		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Buyer payment transactions deleted successfully");
	}

	public static void enqueueDeleteProductEnquiriesTask(String buyerId, boolean forceDelete) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		addTaskToQueue(DELETE_PRODUCT_ENQUIRIES_TASK_WORKER_URI, paramMap);
	}

	@RequestMapping(path = DELETE_PRODUCT_ENQUIRIES_TASK_WORKER_URI, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteProductEnquiriesTask(@RequestParam(BUYER_ID_QUERY_PARAM) String buyerId,
			@RequestParam(FORCE_DELETE_QUERY_PARAM) String forceDelete,
			@RequestParam(name = TASK_ID_QUERY_PARAM, required = false) String taskId) {

		taskHelper.checkForWaitingTaskAndMarkAsInProgress(taskId);

		logger.info("Executing worker: delete product enquiries...");

		Buyer buyer = queryBuyer(buyerId);
		Boolean forceDeleteFlag = new Boolean(forceDelete);

		List<Enquiry> productEnquiries = dbClient.queryList(Enquiry.class, buyerField(buyer));

		if (!CollectionUtils.isEmpty(productEnquiries)) {
			if (!forceDeleteFlag) {
				taskHelper.markAsFailed(taskId);
				throw ForbiddenException.deleteFailedHasReferences(buyerId, Buyer.class, Enquiry.class);
			}

			logger.info("Get the product enquiries of the buyer and delete them...");
			DBOperationList operationList = new DBOperationList();

			for (Iterator<Enquiry> iterator = productEnquiries.iterator(); iterator.hasNext();) {
				Enquiry enquiry = iterator.next();
				operationList.addOperation(OperationType.MARK_AS_DELETED, enquiry);
			}

			taskHelper.transact(operationList, taskId);
		}

		taskHelper.markAsSuccess(taskId);
		return TaskResponseUtility.createTaskSuccessResponse("Buyer's product enquiries deleted successfully");
	}

	public static void enqueueDeletePurchaseItemsTask(String buyerId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_PURCHASE_ITEMS_TASK_WORKER_URI, paramMap);

	}

	public static void enqueueDeleteProductEnquiriesTask(String buyerId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_PRODUCT_ENQUIRIES_TASK_WORKER_URI, paramMap);

	}

	public static void enqueueDeleteReviewsTask(String buyerId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_REVIEWS_TASK_WORKER_URI, paramMap);

	}

	public static void enqueueDeleteReviewsInVendorReviewsTask(String buyerId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_REVIEWS_IN_VENDOR_REVIEWS_TASK_WORKER_URI, paramMap);

	}

	public static void enqueueDeleteReviewsInProductReviewsTask(String buyerId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_REVIEWS_IN_PRODUCT_REVIEWS_TASK_WORKER_URI, paramMap);

	}

	public static void enqueueDeleteBiddingSubscriptionUsagesTask(String buyerId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_BIDDING_SUBSCRIPTION_USAGES_TASK_WORKER_URI, paramMap);

	}

	public static void enqueueDeleteBiddingSubscriptionsTask(String buyerId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_BIDDING_SUBSCRIPTIONS_TASK_WORKER_URI, paramMap);

	}

	public static void enqueueDeleteTendersTask(String buyerId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_TENDERS_TASK_WORKER_URI, paramMap);

	}

	public static void enqueueDeletePaymentTransactionsTask(String buyerId, boolean forceDelete, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put(FORCE_DELETE_QUERY_PARAM, String.valueOf(forceDelete));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_PAYMENT_TRANSACTIONS_TASK_WORKER_URI, paramMap);

	}

	public static void enqueueDeleteBuyerTask(String buyerId, boolean deleteUserAccount, Task task) {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(BUYER_ID_QUERY_PARAM, buyerId);
		paramMap.put("deleteUserAccount", String.valueOf(deleteUserAccount));
		paramMap.put(TASK_ID_QUERY_PARAM, task.getId());
		addTaskToQueue(DELETE_BUYER_TASK_WORKER_URI, paramMap);

	}

}
