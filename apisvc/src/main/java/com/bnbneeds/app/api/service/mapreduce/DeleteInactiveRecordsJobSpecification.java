package com.bnbneeds.app.api.service.mapreduce;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.tools.mapreduce.MapJob;
import com.google.appengine.tools.mapreduce.MapSpecification;
import com.google.appengine.tools.mapreduce.inputs.DatastoreKeyInput;
import com.google.appengine.tools.mapreduce.outputs.NoOutput;

public class DeleteInactiveRecordsJobSpecification {

	private String entityType;

	public DeleteInactiveRecordsJobSpecification(String entityType) {
		super();
		this.entityType = entityType;
	}

	public static MapSpecification<Key, Void, Void> jobSpec(String entityType, int shardCount) {

		DeleteEntityMapper mapper = new DeleteEntityMapper();
		Filter filter = new Query.FilterPredicate("inactive", FilterOperator.EQUAL, true);
		Query query = new Query(entityType).setFilter(filter);

		MapSpecification<Key, Void, Void> spec = new MapSpecification.Builder<>(
				new DatastoreKeyInput(query, shardCount), mapper, new NoOutput<Void, Void>())
						.setJobName("Deleting inactive records of type: " + entityType).build();
		return spec;
	}

	public String start() {
		return MapJob.start(jobSpec(entityType, 50), MapReduceUtils.getMapSettings());
	}

}
