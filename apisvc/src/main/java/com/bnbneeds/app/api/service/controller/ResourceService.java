package com.bnbneeds.app.api.service.controller;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.helper.EmailHelper;
import com.bnbneeds.app.api.service.helper.TaskHelper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.LoginHelper;
import com.bnbneeds.app.api.service.security.PasswordEncoder;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.DbClientUtility;
import com.bnbneeds.app.api.service.util.NotificationUtility;
import com.bnbneeds.app.api.service.util.ResourceUtility;
import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.ResultSet;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.ApprovedDataObject;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.db.client.model.Notification;
import com.bnbneeds.app.util.StringUtils;

public abstract class ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(ResourceService.class);

	protected static final SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss z");
	protected static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MMM-yyyy");
	protected static final String REQ_PARAM_FETCH_SIZE = "fetchSize";
	protected static final String REQ_PARAM_FILTER = "filter";
	protected static final String REQ_PARAM_SEARCH_TEXT = "searchText";
	protected static final String REQ_PARAM_NEXT_OFFSET = "offset";
	protected static final String REQ_PARAM_FORCE_DELETE = "forceDelete";
	protected static final String DEFAULT_RESULT_FETCH_SIZE = "10";

	@Resource(name = "applicationSettings")
	protected Properties applicationSettings;

	@Autowired
	protected HttpServletRequest httpRequest;

	@Autowired
	protected PasswordEncoder passwordEncoder;

	@Autowired
	protected LoginHelper loginHelper;

	@Autowired
	protected TaskHelper taskHelper;

	@Autowired
	protected DbClient dbClient;

	/*
	 * @Autowired protected SystemEventPublisher eventPublisher;
	 */

	@Autowired
	protected EmailHelper emailHelper;

	protected void setEntityStatus(ApprovedDataObject dataObject, EntityStatus status, String remark) {
		dataObject.setEntityStatus(status);
		dataObject.addRemark(remark);
	}

	protected Map<String, String> getKeyValuePairFromFilter(final String filter, final String[] validFilterKeys) {
		return ResourceUtility.getKeyValuePairFromFilter(filter, validFilterKeys);
	}

	@Deprecated
	/**
	 * Google datastore does not support cursors with IN and != operators are used
	 * for queries.
	 */
	protected QueryFields applyEntityStatusFilter(BNBNeedsUser user, String filter) {
		QueryFields fields = new QueryFields();
		final String STATUS_FILTER = "status";
		final String[] QUERY_FILTER_KEYS = { STATUS_FILTER };

		if (StringUtils.hasText(filter)) {
			Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);
			String value = keyValueMap.get(STATUS_FILTER);
			if (value != null) {
				String[] statuses = value.toUpperCase().split(",");
				List<String> statusFilterList = new ArrayList<>();
				for (String status : statuses) {
					if (StringUtils.hasText(status)) {
						String trimmedValue = status.trim();
						ArgValidator.checkFieldValueFromEnum(trimmedValue, STATUS_FILTER, EntityStatus.class);
						if (!user.isAdmin()) {
							if (!EntityStatus.BLACKLISTED.name().equals(trimmedValue)) {
								statusFilterList.add(trimmedValue);
							}
						} else {
							statusFilterList.add(trimmedValue);
						}
					}
				}
				if (!statusFilterList.isEmpty()) {
					fields.add("entityStatus in", statusFilterList);
				}
			}
		}
		if (!fields.containsField("entityStatus in")) {
			if (!user.isAdmin()) {
				fields.add("entityStatus !=", EntityStatus.BLACKLISTED.name());
			}
		}
		return fields;
	}

	protected <T extends DataObject> ResultSet<T> getResultSet(Class<T> clazz, QueryFields fields, String offset,
			int fetchSize) {
		return DbClientUtility.getResultSet(dbClient, clazz, fields, offset, fetchSize);
	}

	protected <T extends DataObject> ResultSetIterator<T> getResultSetIterator(Class<T> clazz, QueryFields fields,
			String offset, int fetchSize) {
		return DbClientUtility.getResultSetIterator(dbClient, clazz, fields, offset, fetchSize);
	}

	/*
	 * protected void publishEvent(SystemEventType type, RecipientType
	 * recipientType, String recipientId, String objectId, String objectName, String
	 * objectLink) { eventPublisher.setEventAttributes(type, recipientType,
	 * recipientId, objectId, objectName, objectLink); eventPublisher.publish(); }
	 */

	/**
	 * Records the event for notifications
	 * 
	 * @param type
	 * @param recipientType
	 * @param recipientId
	 * @param objectId
	 * @param objectName
	 * @param objectLink
	 * @param createdBy
	 */
	protected synchronized void recordEvent(SystemEventType type, RecipientType recipientType, String recipientId,
			String objectId, String objectName, URI objectLink, String createdBy) {

		logger.info(
				"Recording event with attributes: type: {}, recipientType: {}, recipientId: {}, objectId: {}, objectName: {}, link: {}",
				type.name(), recipientType.name(), recipientId, objectId, objectName, objectLink);

		Notification notification = NotificationUtility.createNotification(type, recipientType, recipientId, objectId,
				objectName, objectLink, createdBy);

		DBOperationList dbOperationList = new DBOperationList();
		dbOperationList.addOperation(OperationType.INSERT, notification);
		dbClient.transact(dbOperationList);
	}

	protected synchronized void recordEvent(SystemEventType type, RecipientType recipientType, String objectId,
			String objectName, URI objectLink, String createdBy) {

		recordEvent(type, recipientType, "", objectId, objectName, objectLink, createdBy);
	}

	protected synchronized void recordEvent(SystemEventType type, RecipientType recipientType, Set<String> recipientIds,
			String objectId, String objectName, URI objectLink, String createdBy) {

		logger.info(
				"Recording event with attributes: type: {}, recipientType: {}, recipientIds: {}, objectId: {}, objectName: {}, link: {}",
				type.name(), recipientType.name(), recipientIds, objectId, objectName, objectLink);

		DBOperationList dbOperationList = new DBOperationList();
		for (Iterator<String> iterator = recipientIds.iterator(); iterator.hasNext();) {
			String recipientId = iterator.next();
			Notification notification = NotificationUtility.createNotification(type, recipientType, recipientId,
					objectId, objectName, objectLink, createdBy);
			dbOperationList.addOperation(OperationType.INSERT, notification);
		}

		dbClient.transact(dbOperationList);
	}
}
