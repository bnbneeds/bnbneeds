package com.bnbneeds.app.api.service.util;

import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.OrderBy;
import com.bnbneeds.app.db.client.ResultSet;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBClientException;
import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.db.client.model.Notification;

public class DbClientUtility {

	public static <T extends DataObject> ResultSet<T> getResultSet(
			DbClient dbClient, Class<T> clazz, QueryFields fields,
			String offset, int fetchSize) {
		ResultSet<T> resultSet = null;
		try {
			if (fields != null && !fields.isEmpty()) {
				resultSet = dbClient.queryResultSet(clazz, fields, offset,
						fetchSize);
			} else {
				resultSet = dbClient.queryResultSet(clazz, offset, fetchSize);
			}
		} catch (DBClientException e) {
			throw BadRequestException.create(e.getMessage());
		}
		return resultSet;
	}

	public static <T extends DataObject> ResultSetIterator<T> getResultSetIterator(
			DbClient dbClient, Class<T> clazz, QueryFields fields,
			String offset, int fetchSize) {
		ResultSetIterator<T> resultSet = null;
		try {
			if (fields != null && !fields.isEmpty()) {
				resultSet = dbClient.queryResultSetIterator(clazz, fields,
						offset, fetchSize);
			} else {
				resultSet = dbClient.queryResultSetIterator(clazz, offset,
						fetchSize);
			}
		} catch (DBClientException e) {
			throw BadRequestException.create(e.getMessage());
		}
		return resultSet;
	}

	public static <T extends DataObject> ResultSetIterator<T> getResultSetIteratorOrderByDesc(
			DbClient dbClient, Class<T> clazz, QueryFields fields,
			String offset, int fetchSize, String orderField) {
		ResultSetIterator<T> resultSet = null;
		try {
			if (fields != null && !fields.isEmpty()) {
				resultSet = dbClient.queryResultSetIteratorBySortOrder(clazz,
						fields, offset, fetchSize, OrderBy.DESC, orderField);
			} else {
				resultSet = dbClient.queryResultSetIteratorBySortOrder(clazz,
						offset, fetchSize, OrderBy.DESC, orderField);
			}
		} catch (DBClientException e) {
			throw BadRequestException.create(e.getMessage());
		}
		return resultSet;
	}

	public static ResultSetIterator<Notification> getNotificationResultSetIterator(
			DbClient dbClient, QueryFields fields, String offset, int fetchSize) {

		return getResultSetIteratorOrderByDesc(dbClient, Notification.class,
				fields, offset, fetchSize, "timestamp");

	}
}
