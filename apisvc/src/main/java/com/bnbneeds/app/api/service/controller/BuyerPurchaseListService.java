package com.bnbneeds.app.api.service.controller;

import static com.bnbneeds.app.api.service.response.DBObjectMapper.toNamedRelatedRestResponse;
import static com.bnbneeds.app.api.service.response.PurchaseItemResponseMapper.mapQuantity;

import java.net.URI;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.PurchaseItemResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.ProductName;
import com.bnbneeds.app.db.client.model.PurchaseItem;
import com.bnbneeds.app.db.client.model.Units;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.VendorListResponse;
import com.bnbneeds.app.model.dealer.order.AggregatedPurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.AggregatedPurchaseListResponse.AggregatedPuchaseItem;
import com.bnbneeds.app.model.dealer.order.AggregatedPurchaseListResponse.AggregatedPuchaseItem.DealerPurchaseItemInfo;
import com.bnbneeds.app.model.dealer.order.CreatePurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.CreatePurchaseListParam;
import com.bnbneeds.app.model.dealer.order.PurchaseItemInfoResponse;
import com.bnbneeds.app.model.dealer.order.PurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.PurchaseListResponse;
import com.bnbneeds.app.model.dealer.order.UpdatePurchaseItemParam;
import com.bnbneeds.app.model.dealer.order.UpdatePurchaseListParam;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.ProductNameListResponse;
import com.bnbneeds.app.util.DateUtils;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/buyers/{buyerId}/purchase-items")
public class BuyerPurchaseListService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(BuyerPurchaseListService.class);

	private static final String REQ_PARAM_REQUEST_DATE = "requestDate";
	private static final String REQ_PARAM_START_REQUEST_DATE = "startRequestDate";
	private static final String REQ_PARAM_END_REQUEST_DATE = "endRequestDate";

	private static enum State {
		CANCELLED, FULFILLED, ITEM_RETURNED, REQUIRED_QUANTITY_MODIFIED, VENDOR_CHANGED, ITEM_DESCRIPTION_CHANGED, GENERAL_UPDATE
	}

	@RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> createPurchaseList(@PathVariable("buyerId") String buyerId,
			@RequestBody CreatePurchaseListParam param) {

		logger.info("Request received to create purchase list: {}", param);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		List<CreatePurchaseItemParam> purchaseList = param.getPurchaseList();

		ArgValidator.checkFieldNotEmpty(purchaseList, "items");

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);
		Date date = new Date();
		String dateToday = DATE_FORMAT.format(date);
		DBOperationList dbOperationList = new DBOperationList();

		Set<String> reqList = new HashSet<>();
		boolean elementAdded = false;
		Set<String> vendorIds = new HashSet<>();
		for (Iterator<CreatePurchaseItemParam> iterator = purchaseList.iterator(); iterator.hasNext();) {
			CreatePurchaseItemParam purchaseItemParam = iterator.next();

			ArgValidator.checkFieldNotNull(purchaseItemParam, "item");
			String vendorId = purchaseItemParam.getVendorId();
			ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
			String productNameId = purchaseItemParam.getProductNameId();
			ArgValidator.checkFieldUriType(productNameId, ProductName.class, "product_name_id");
			Double requiredQuantity = purchaseItemParam.getRequiredQuantity();
			ArgValidator.checkFieldNotNull(requiredQuantity, "required_quantity");
			String unit = purchaseItemParam.getUnit();
			ArgValidator.checkFieldValueFromEnum(unit, "unit", Units.Quantity.class);
			String tags = purchaseItemParam.getTags();

			ProductName productName = dbClient.queryObject(ProductName.class, productNameId);
			ArgValidator.checkEntityDisapprovedOrBlacklisted(productName, productNameId, false);
			Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
			ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, false);
			String nativeId = null;
			if (StringUtils.hasText(tags)) {

				ArgValidator.checkField(StringUtils.isCSVStringValid(tags), "tags", "Not a valid CSV text.");

				tags = PurchaseItem.sanitizeTags(tags);
				nativeId = PurchaseItem.generateNativeId(dateToday, buyerId, vendorId, productNameId, tags);
			} else {
				nativeId = PurchaseItem.generateNativeId(dateToday, buyerId, vendorId, productNameId);
			}

			elementAdded = reqList.add(nativeId);
			if (!elementAdded) {
				logger.error("Duplicate purchase item: {}", nativeId);
				throw BadRequestException.create("Duplicate purchase item: [" + nativeId + "]");
			}
			// Check if nativeId exists in DB
			PurchaseItem purchaseItem = dbClient.queryObjectByFields(PurchaseItem.class,
					new QueryFields("nativeId", nativeId));
			if (purchaseItem != null) {
				// nativeId already exists
				logger.error("Duplicate purchase item: {}", nativeId);
				throw BadRequestException.create("Duplicate purchase item: [" + nativeId + "]");
			}
			purchaseItem = new PurchaseItem();
			String uid = UUIDUtil.createId(PurchaseItem.class);
			purchaseItem.setId(uid);
			String description = purchaseItemParam.getDescription();
			if (!StringUtils.isEmpty(description)) {
				purchaseItem.setDescription(description);
			}
			purchaseItem.setBuyer(buyer);
			purchaseItem.setVendor(vendor);
			purchaseItem.setProductName(productName);
			purchaseItem.setRequiredQuantity(requiredQuantity);
			purchaseItem.setUnit(unit);
			purchaseItem.setRequestTimestamp(date.getTime());
			if (StringUtils.hasText(tags)) {
				purchaseItem.setTags(tags);
			}
			purchaseItem.setNativeId(nativeId);
			purchaseItem.addOrderStatus(TIMESTAMP_FORMAT.format(date), "SUBMITTED", null, buyer.getName());
			dbOperationList.addOperation(OperationType.INSERT, purchaseItem);
			vendorIds.add(vendorId);
		}

		dbClient.transact(dbOperationList);

		recordEvent(SystemEventType.NEW_PURCHASE_ITEM_ORDERED, RecipientType.VENDOR, vendorIds, null, null, null,
				buyer.getName());

		return TaskResponseUtility.createTaskSubmittedResponse("Task initiated to create purchase list.");

	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and hasDealership() and isRelated(#buyerId)")
	public PurchaseListResponse getPurchaseList(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {
		logger.info("Request received to get purchase items by filter: {}, fetchSize: {}, offset: {}", filter,
				fetchSize, offset);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);

		final String[] QUERY_FILTER_KEYS = { "name", "status", "vendorId", "productNameId", REQ_PARAM_REQUEST_DATE,
				REQ_PARAM_START_REQUEST_DATE, REQ_PARAM_END_REQUEST_DATE };

		PurchaseListResponse response = new PurchaseListResponse();

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields("buyer", buyer);
		boolean requestDateGiven = false;
		for (String key : keyValueMap.keySet()) {

			String value = keyValueMap.get(key);

			switch (key) {
			case "name":
				ProductName productNameEntity = dbClient.queryObjectByFields(ProductName.class,
						new QueryFields("name", value));
				ArgValidator.checkEntityBlacklisted(productNameEntity, value, true);
				fields.add("productName", productNameEntity);
				break;
			case "productNameId":
				ArgValidator.checkFieldUriType(value, ProductName.class, key);
				ProductName productName = dbClient.queryObject(ProductName.class, value);
				ArgValidator.checkEntityBlacklisted(productName, value, true);
				fields.add("productName", productName);
				break;
			case "vendorId":
				ArgValidator.checkFieldUriType(value, Vendor.class, key);
				Vendor vendor = dbClient.queryObject(Vendor.class, value);
				ArgValidator.checkEntityBlacklisted(vendor, value, true);
				fields.add("vendor", vendor);
				break;
			case "status":
				ArgValidator.checkFieldValue(!EntityStatus.BLACKLISTED.name().equals(value.toUpperCase()), key);
				fields.add("entityStatus", value.toUpperCase());
				break;
			case REQ_PARAM_REQUEST_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("requestTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("requestTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE + " with this field.");
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				fields.add("requestTimestamp >=", startRequestDate.getTime());
				break;
			case REQ_PARAM_END_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE + " with this field.");
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils.getEndOfDayTimestamp(endRequestDate);
				fields.add("requestTimestamp <=", endTimestamp2);
				break;
			default:
				break;
			}

		}

		ResultSetIterator<PurchaseItem> resultSetIterator = null;
		do {

			resultSetIterator = getResultSetIterator(PurchaseItem.class, fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					PurchaseItem item = resultSetIterator.next();

					if (item == null || item.isBlacklisted()) {
						continue;
					}

					Vendor vendor = item.getVendor();
					if (vendor == null || vendor.isBlacklisted()) {
						continue;
					}

					PurchaseItemInfoResponse infoResponse = PurchaseItemResponseMapper.map(item, DATE_FORMAT);
					infoResponse.setLink(Endpoint.BUYER_PURCHASE_ITEM_INFO.get(buyerId, item.getId()));
					response.addPurchaseItemInfo(infoResponse);

				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}
		} while (response.size() < fetchSize);
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and hasDealership() and isRelated(#buyerId)")
	public PurchaseItemInfoResponse getPurchaseItem(@PathVariable("buyerId") String buyerId,
			@PathVariable("id") String itemId) {

		logger.info("Request received to retrive item details of {} beloging to buyer {}", itemId, buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(itemId, PurchaseItem.class, "item_id");

		PurchaseItem purchaseItem = dbClient.queryObject(PurchaseItem.class, itemId);

		ArgValidator.checkEntityBlacklisted(purchaseItem, itemId, true);
		ArgValidator.checkEntityBlacklisted(purchaseItem.getBuyer(), purchaseItem.getBuyer().getId(), true);

		if (!buyerId.equals(purchaseItem.getBuyer().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}
		PurchaseItemInfoResponse resp = PurchaseItemResponseMapper.map(purchaseItem, DATE_FORMAT);
		if (purchaseItem != null) {
			resp.setLink(Endpoint.BUYER_PURCHASE_ITEM_INFO.get(buyerId, itemId));
		}
		return resp;
	}

	@RequestMapping(method = RequestMethod.PUT, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and hasDealership() and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> updatePurchaseList(@PathVariable("buyerId") String buyerId,
			@RequestBody UpdatePurchaseListParam param) {

		logger.info("Request received to update purchase list of buyer {}", buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);

		List<UpdatePurchaseItemParam> purchaseList = param.getPurchaseList();
		ArgValidator.checkFieldNotEmpty(purchaseList, "items");

		DBOperationList operationList = new DBOperationList();
		boolean changed = false;
		boolean vendorChanged = false;
		boolean tagsChanged = false;
		Set<String> vendorIds = new HashSet<>();
		for (Iterator<UpdatePurchaseItemParam> iterator = purchaseList.iterator(); iterator.hasNext();) {

			UpdatePurchaseItemParam updatePurchaseItemParam = iterator.next();

			ArgValidator.checkFieldNotNull(updatePurchaseItemParam, "update_purchase_item");

			ArgValidator.checkFieldNotNull(updatePurchaseItemParam.getStatusUpdate(), "status_update");

			ArgValidator.checkFieldValueFromEnum(updatePurchaseItemParam.getStatusUpdate().getStatus(), "status",
					State.class);
			ArgValidator.checkFieldNotEmpty(updatePurchaseItemParam.getStatusUpdate().getComments(), "comments");

			String itemId = updatePurchaseItemParam.getPurchaseItemId();

			ArgValidator.checkFieldUriType(itemId, PurchaseItem.class, "item_id");

			PurchaseItem purchaseItem = dbClient.queryObject(PurchaseItem.class, itemId);

			ArgValidator.checkEntityDisapprovedOrBlacklisted(purchaseItem, itemId, true);
			ArgValidator.checkEntityDisapprovedOrBlacklisted(purchaseItem.getBuyer(), purchaseItem.getBuyer().getId(),
					true);

			if (!buyerId.equals(purchaseItem.getBuyer().getId())) {
				throw ForbiddenException.unauthorizedAccess();
			}

			ArgValidator.checkFieldValue(StringUtils.isEmpty(updatePurchaseItemParam.getDeliveredQuantity()),
					"delivered_quantity");

			String vendorId = updatePurchaseItemParam.getVendorId();
			Vendor vendor = null;
			if (StringUtils.hasText(vendorId)) {
				ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
				vendor = dbClient.queryObject(Vendor.class, vendorId);
				ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, false);
				purchaseItem.setVendor(vendor);
				vendorChanged = true;
				changed = true;
			}

			Double requiredQuantity = updatePurchaseItemParam.getRequiredQuantity();

			if (requiredQuantity != null) {
				purchaseItem.setRequiredQuantity(requiredQuantity);
				changed = true;
			}
			String unit = updatePurchaseItemParam.getUnit();
			if (StringUtils.hasText(unit)) {
				ArgValidator.checkFieldValueFromEnum(unit, "unit", Units.Quantity.class);
				purchaseItem.setUnit(unit);
				changed = true;
			}
			String description = updatePurchaseItemParam.getDescription();
			if (StringUtils.hasText(description)) {
				purchaseItem.setDescription(description);
				changed = true;
			}

			String tags = updatePurchaseItemParam.getTags();
			if (StringUtils.hasText(tags)) {
				ArgValidator.checkField(StringUtils.isCSVStringValid(tags), "tags", "Not a valid CSV text.");
				String newTags = PurchaseItem.sanitizeTags(tags);
				purchaseItem.setTags(newTags);
				tagsChanged = true;
				changed = true;

			}

			String productNameId = purchaseItem.getProductName().getName();
			String requestDate = StringUtils.getFormattedDate(purchaseItem.getRequestTimestamp(), DATE_FORMAT);

			if (vendorChanged) {
				String newNativeId = null;
				String existingTags = purchaseItem.getTags();
				if (StringUtils.hasText(existingTags)) {
					newNativeId = PurchaseItem.generateNativeId(requestDate, buyerId, vendorId, productNameId,
							existingTags);

				} else {
					newNativeId = PurchaseItem.generateNativeId(requestDate, buyerId, vendorId, productNameId);
				}
				purchaseItem.setNativeId(newNativeId);
			}

			if (tagsChanged) {
				String existingTags = purchaseItem.getTags();
				if (StringUtils.hasText(existingTags)) {
					String existingVendorId = purchaseItem.getVendor().getId();
					String newNativeId = PurchaseItem.generateNativeId(requestDate, buyerId, existingVendorId,
							productNameId, existingTags);
					purchaseItem.setNativeId(newNativeId);

				}
			}

			purchaseItem.addOrderStatus(TIMESTAMP_FORMAT.format(new Date()),
					updatePurchaseItemParam.getStatusUpdate().getStatus(),
					updatePurchaseItemParam.getStatusUpdate().getComments(), purchaseItem.getBuyer().getName());

			if (changed) {
				operationList.addOperation(OperationType.UPDATE, purchaseItem);
				vendorIds.add(purchaseItem.getVendor().getId());
				changed = false;
			}

		}
		logger.info("Updating purchase items of buyer vendor {}", buyerId);
		dbClient.transact(operationList);

		recordEvent(SystemEventType.PURCHASE_ITEMS_UPDATED, RecipientType.VENDOR, vendorIds, null, null, null,
				buyer.getName());

		return TaskResponseUtility.createTaskSubmittedResponse("Task submitted to update purchase list successfully.");

	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and hasDealership() and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> updatePurchaseItem(@PathVariable("buyerId") String buyerId,
			@PathVariable("id") String itemId, @RequestBody PurchaseItemParam param) {

		logger.info("Request received to update item {} belonging to buyer {}", itemId, buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(itemId, PurchaseItem.class, "item_id");

		PurchaseItem purchaseItem = dbClient.queryObject(PurchaseItem.class, itemId);

		ArgValidator.checkEntityDisapprovedOrBlacklisted(purchaseItem, itemId, true);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(purchaseItem.getBuyer(), purchaseItem.getBuyer().getId(),
				true);

		if (!buyerId.equals(purchaseItem.getBuyer().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		ArgValidator.checkFieldNotNull(param, "update_purchase_item");
		ArgValidator.checkFieldNotNull(param.getStatusUpdate(), "status_update");

		ArgValidator.checkFieldValueFromEnum(param.getStatusUpdate().getStatus(), "status", State.class);
		ArgValidator.checkFieldNotEmpty(param.getStatusUpdate().getComments(), "comments");

		ArgValidator.checkFieldValue(StringUtils.isEmpty(param.getDeliveredQuantity()), "delivered_quantity");

		boolean vendorChanged = false;
		boolean tagsChanged = false;

		String vendorId = param.getVendorId();
		Vendor vendor = null;
		if (StringUtils.hasText(vendorId)) {
			ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
			vendor = dbClient.queryObject(Vendor.class, vendorId);
			ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, false);
			purchaseItem.setVendor(vendor);
			vendorChanged = true;
		}

		Double requiredQuantity = param.getRequiredQuantity();

		if (requiredQuantity != null) {
			purchaseItem.setRequiredQuantity(requiredQuantity);
		}
		String unit = param.getUnit();
		if (StringUtils.hasText(unit)) {
			ArgValidator.checkFieldValueFromEnum(unit, "unit", Units.Quantity.class);
			purchaseItem.setUnit(unit);
		}
		String description = param.getDescription();
		if (StringUtils.hasText(description)) {
			purchaseItem.setDescription(description);
		}

		String tags = param.getTags();
		if (StringUtils.hasText(tags)) {
			ArgValidator.checkField(StringUtils.isCSVStringValid(tags), "tags", "Not a valid CSV text.");
			String newTags = PurchaseItem.sanitizeTags(tags);
			purchaseItem.setTags(newTags);
			tagsChanged = true;
		}

		String productNameId = purchaseItem.getProductName().getName();
		String requestDate = StringUtils.getFormattedDate(purchaseItem.getRequestTimestamp(), DATE_FORMAT);

		if (vendorChanged) {
			String newNativeId = null;
			String existingTags = purchaseItem.getTags();
			if (StringUtils.hasText(existingTags)) {
				newNativeId = PurchaseItem.generateNativeId(requestDate, buyerId, vendorId, productNameId,
						existingTags);

			} else {
				newNativeId = PurchaseItem.generateNativeId(requestDate, buyerId, vendorId, productNameId);
			}
			purchaseItem.setNativeId(newNativeId);
		}

		if (tagsChanged) {
			String existingTags = purchaseItem.getTags();
			if (StringUtils.hasText(existingTags)) {
				String existingVendorId = purchaseItem.getVendor().getId();
				String newNativeId = PurchaseItem.generateNativeId(requestDate, buyerId, existingVendorId,
						productNameId, existingTags);
				purchaseItem.setNativeId(newNativeId);

			}
		}

		purchaseItem.addOrderStatus(TIMESTAMP_FORMAT.format(new Date()), param.getStatusUpdate().getStatus(),
				param.getStatusUpdate().getComments(), purchaseItem.getBuyer().getName());

		dbClient.updateObject(purchaseItem);

		URI objectLink = null;
		if (vendorId != null) {
			objectLink = Endpoint.VENDOR_PURCHASE_ITEM_INFO.get(vendorId, itemId);
		}
		recordEvent(SystemEventType.PURCHASE_ITEM_STATUS_UPDATED, RecipientType.VENDOR,
				purchaseItem.getVendor().getId(), itemId, purchaseItem.getProductName().getName(), objectLink,
				purchaseItem.getBuyer().getName());

		return TaskResponseUtility.createTaskSuccessResponse("Purchase item updated successfully");
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and hasDealership() and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> deletePurchaseItem(@PathVariable("buyerId") String buyerId,
			@PathVariable("id") String itemId) {

		logger.info("Request received to delete item {} belonging to buyer {}", itemId, buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(itemId, PurchaseItem.class, "item_id");

		PurchaseItem purchaseItem = dbClient.queryObject(PurchaseItem.class, itemId);

		ArgValidator.checkEntityDisapprovedOrBlacklisted(purchaseItem, itemId, true);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(purchaseItem.getBuyer(), purchaseItem.getBuyer().getId(),
				true);

		if (!buyerId.equals(purchaseItem.getBuyer().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		dbClient.markAsDeleted(purchaseItem);

		recordEvent(SystemEventType.PURCHASE_ITEM_DELETED, RecipientType.VENDOR, purchaseItem.getVendor().getId(),
				itemId, purchaseItem.getProductName().getName(), null, purchaseItem.getBuyer().getName());

		return TaskResponseUtility.createTaskSuccessResponse("Purchase item deleted successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, path = "/associated-product-names", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and hasDealership() and isRelated(#buyerId)")
	public ProductNameListResponse getAssociatedProductNameList(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @RequestParam(name = REQ_PARAM_FILTER) String filter) {

		logger.info("Request received to get associated product names of purchase list requested by buyer: {}",
				buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);

		ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);

		final String[] QUERY_FILTER_KEYS = { REQ_PARAM_REQUEST_DATE, REQ_PARAM_START_REQUEST_DATE,
				REQ_PARAM_END_REQUEST_DATE };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		boolean validRequest = keyValueMap.containsKey(REQ_PARAM_REQUEST_DATE)
				|| (keyValueMap.containsKey(REQ_PARAM_START_REQUEST_DATE)
						&& keyValueMap.containsKey(REQ_PARAM_END_REQUEST_DATE));

		if (!validRequest) {
			throw BadRequestException.parameterMissing(REQ_PARAM_REQUEST_DATE);
		}

		QueryFields fields = new QueryFields("buyer", buyer);
		/*
		 * Cannot have more than one inequality operator in a query
		 * fields.add("entityStatus !=", EntityStatus.BLACKLISTED.name());
		 */
		boolean requestDateGiven = false;
		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case REQ_PARAM_REQUEST_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("requestTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("requestTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE + " with this field.");
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				fields.add("requestTimestamp >=", startRequestDate.getTime());
				break;
			case REQ_PARAM_END_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE + " with this field.");
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils.getEndOfDayTimestamp(endRequestDate);
				fields.add("requestTimestamp <=", endTimestamp2);
				break;
			default:
				break;
			}
		}

		List<PurchaseItem> purchaseItemList = dbClient.queryList(PurchaseItem.class, fields);

		ProductNameListResponse productNameListResponse = new ProductNameListResponse();
		Set<String> uriSet = new HashSet<>();
		for (Iterator<PurchaseItem> iterator = purchaseItemList.iterator(); iterator.hasNext();) {
			PurchaseItem purchaseItem = iterator.next();
			if (!purchaseItem.isBlacklisted()) {
				ProductName productName = purchaseItem.getProductName();
				if (productName != null && uriSet.add(productName.getId())) {
					productNameListResponse.addNamedResourceResponse(toNamedRelatedRestResponse(productName));
				}
			}
		}
		logger.info("Returning product name list of size: {}.", productNameListResponse.size());

		return productNameListResponse;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/associated-vendors", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and hasDealership() and isRelated(#buyerId)")
	public VendorListResponse getAssociatedVendorList(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @RequestParam(name = REQ_PARAM_FILTER) String filter) {

		logger.info("Request received to get associated vendors of purchase list requested by buyer: {}", buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);

		ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);

		final String[] QUERY_FILTER_KEYS = { REQ_PARAM_REQUEST_DATE, REQ_PARAM_START_REQUEST_DATE,
				REQ_PARAM_END_REQUEST_DATE };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		boolean validRequest = keyValueMap.containsKey(REQ_PARAM_REQUEST_DATE)
				|| (keyValueMap.containsKey(REQ_PARAM_START_REQUEST_DATE)
						&& keyValueMap.containsKey(REQ_PARAM_END_REQUEST_DATE));

		if (!validRequest) {
			throw BadRequestException.parameterMissing(REQ_PARAM_REQUEST_DATE);
		}

		boolean requestDateGiven = false;

		QueryFields fields = new QueryFields("buyer", buyer);

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case REQ_PARAM_REQUEST_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("requestTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("requestTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE + " with this field.");
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				fields.add("requestTimestamp >=", startRequestDate.getTime());
				break;
			case REQ_PARAM_END_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE + " with this field.");
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils.getEndOfDayTimestamp(endRequestDate);
				fields.add("requestTimestamp <=", endTimestamp2);
				break;
			default:
				break;
			}
		}

		List<PurchaseItem> purchaseItemList = dbClient.queryList(PurchaseItem.class, fields);

		VendorListResponse vendorListResponse = new VendorListResponse();

		Set<String> uriSet = new HashSet<>();
		for (Iterator<PurchaseItem> iterator = purchaseItemList.iterator(); iterator.hasNext();) {
			PurchaseItem purchaseItem = iterator.next();
			if (!purchaseItem.isBlacklisted()) {
				Vendor vendor = purchaseItem.getVendor();
				if (vendor != null && uriSet.add(vendor.getId())) {
					vendorListResponse.addNamedResourceResponse(toNamedRelatedRestResponse(vendor));
				}
			}
		}

		logger.info("Returning vendor list of size: {}.", vendorListResponse.size());

		return vendorListResponse;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/aggregated-list", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and hasDealership() and isRelated(#buyerId)")
	public AggregatedPurchaseListResponse getAggregatedPurchaseList(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @RequestParam(name = REQ_PARAM_FILTER) String filter) {

		logger.info("Request received to get aggregated purchase list requested by buyer: {}", buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);

		ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);

		final String[] QUERY_FILTER_KEYS = { REQ_PARAM_REQUEST_DATE, REQ_PARAM_START_REQUEST_DATE,
				REQ_PARAM_END_REQUEST_DATE, "vendorId", "productNameId" };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		boolean validRequest = keyValueMap.containsKey(REQ_PARAM_REQUEST_DATE)
				|| (keyValueMap.containsKey(REQ_PARAM_START_REQUEST_DATE)
						&& keyValueMap.containsKey(REQ_PARAM_END_REQUEST_DATE));

		if (!validRequest) {
			throw BadRequestException.parameterMissing(REQ_PARAM_REQUEST_DATE);
		}

		boolean requestDateGiven = false;

		QueryFields fields = new QueryFields("buyer", buyer);

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case REQ_PARAM_REQUEST_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("requestTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("requestTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE + " with this field.");
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp2 = startRequestDate.getTime();
				fields.add("requestTimestamp >=", startTimestamp2);
				break;
			case REQ_PARAM_END_REQUEST_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_REQUEST_DATE + " with this field.");
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils.getEndOfDayTimestamp(endRequestDate);

				fields.add("requestTimestamp <=", endTimestamp2);
				break;
			case "vendorId":
				ArgValidator.checkFieldUriType(value, Vendor.class, key);
				Vendor vendor = dbClient.queryObject(Vendor.class, value);
				ArgValidator.checkEntityBlacklisted(vendor, value, true);
				fields.add("vendor", vendor);
				break;
			case "productNameId":
				ArgValidator.checkFieldUriType(value, ProductName.class, key);
				ProductName productName = dbClient.queryObject(ProductName.class, value);
				ArgValidator.checkEntityBlacklisted(productName, value, true);
				fields.add("productName", productName);
				break;
			default:
				break;
			}
		}

		List<PurchaseItem> purchaseItemList = dbClient.queryList(PurchaseItem.class, fields);

		AggregatedPurchaseListResponse aggregatedListResponse = new AggregatedPurchaseListResponse();
		String requestDateParam = null;
		if (keyValueMap.containsKey(REQ_PARAM_REQUEST_DATE)) {
			requestDateParam = keyValueMap.get(REQ_PARAM_REQUEST_DATE);

		} else {
			requestDateParam = keyValueMap.get(REQ_PARAM_START_REQUEST_DATE) + " - "
					+ keyValueMap.get(REQ_PARAM_END_REQUEST_DATE);
		}
		aggregatedListResponse.setRequestDate(requestDateParam);

		for (Iterator<PurchaseItem> iterator = purchaseItemList.iterator(); iterator.hasNext();) {

			PurchaseItem purchaseItem = iterator.next();
			if (purchaseItem.isBlacklisted()) {
				continue;
			}

			Vendor vendor = purchaseItem.getVendor();
			ProductName productName = purchaseItem.getProductName();
			String requestDate = DATE_FORMAT.format(new Date(purchaseItem.getRequestTimestamp()));

			AggregatedPuchaseItem item = aggregatedListResponse.getItemByProductNameId(productName.getId());

			if (item == null) {
				item = new AggregatedPuchaseItem();
				item.setProductName(toNamedRelatedRestResponse(productName));
				item.setQuantity(mapQuantity(purchaseItem));
				aggregatedListResponse.add(item);
			} else {
				// Look for the item in aggregated list
				item.addRequiredQuantity(purchaseItem.getRequiredQuantity());
				item.addDeliveredQuantity(purchaseItem.getDeliveredQuantity());
			}

			if (vendor != null) {
				DealerPurchaseItemInfo dealerPurchaseItemInfo = new DealerPurchaseItemInfo(requestDate,
						new RelatedResourceRep(purchaseItem.getId(),
								new RestLinkRep("self",
										Endpoint.VENDOR_PURCHASE_ITEM_INFO.get(buyer.getId(), purchaseItem.getId()))),
						toNamedRelatedRestResponse(vendor), mapQuantity(purchaseItem));

				String tags = purchaseItem.getTags();

				if (StringUtils.hasText(tags)) {
					dealerPurchaseItemInfo.setTags(tags);
				}
				item.addVendorPurchaseItem(dealerPurchaseItemInfo);
			}

		}
		logger.info("Returning aggregated list of size: {}.", aggregatedListResponse.size());

		return aggregatedListResponse;
	}
}
