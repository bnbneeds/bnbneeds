package com.bnbneeds.app.api.service.controller;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.exceptions.APIException;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.exceptions.VerificationRequestFailedException;
import com.bnbneeds.app.api.service.response.UserAccountResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.HttpRequestResponseUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.messaging.email.MessageKeys;
import com.bnbneeds.app.messaging.email.Template;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.AccountType;
import com.bnbneeds.app.model.account.ChangePasswordParam;
import com.bnbneeds.app.model.account.CreateUserAccountParam;
import com.bnbneeds.app.model.account.UpdateUserAccountParam;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.account.VerifyAccountParam;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.util.DateUtils;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/user-accounts")
public class UserAccountService extends ResourceService {

	private static final Logger logger = LoggerFactory
			.getLogger(UserAccountService.class);

	private static final String SUPER_ADMIN_EMAIL_ADDRESS_PROPERTY_NAME = "superAdminEmailAddress";
	private static final long TIME_DIFFERENCE_FOR_RESET_PASSWORD_REQUEST_IN_MINUTES = 10;

	private static enum VerificationType {
		email, sms, email_and_sms
	}

	private static void checkTimeForValidResetPasswordRequest(String username,
			long lastPasswordResetTimestamp, long currentTimestamp) {

		if (lastPasswordResetTimestamp > 0) {
			long differenceInMinutes = DateUtils.differenceInMinutes(
					lastPasswordResetTimestamp, currentTimestamp);
			long minutesForRetry = TIME_DIFFERENCE_FOR_RESET_PASSWORD_REQUEST_IN_MINUTES
					- differenceInMinutes;
			if (minutesForRetry > 0) {
				String message = null;
				if (differenceInMinutes < 2) {
					message = String
							.format("Password for %1$s was reset a few seconds ago. Please try again %2$d minutes later.",
									username, minutesForRetry);
				} else {
					String minuteTextFormat = "minutes";
					if (minutesForRetry == 1) {
						minuteTextFormat = "minute";
					}
					message = String
							.format("Password for %1$s was reset %2$d minutes ago. Please try again %3$d %4$s later.",
									username, differenceInMinutes,
									minutesForRetry, minuteTextFormat);
				}
				logger.error(message);
				throw ForbiddenException.create(message);
			}
		}

	}

	@RequestMapping(method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CreateResourceResponse> createUserAccount(
			@RequestBody CreateUserAccountParam accountParam)
			throws APIException {

		logger.info("API request received to register a new user account: {}",
				accountParam);

		String username = accountParam.getUsername();
		String password = accountParam.getPassword();
		String accountName = accountParam.getAccountName();
		ArgValidator.checkFieldNotEmpty(accountName, "account_name");
		ArgValidator.checkFieldNotEmpty(username, "email_address");
		ArgValidator.checkFieldValue(StringUtils.isEmailAddressValid(username),
				"email_address");
		ArgValidator.checkFieldNotEmpty(password, "password");
		String mobileNumber = accountParam.getMobileNumber();
		ArgValidator.checkFieldNotEmpty(mobileNumber, "mobile_number");
		ArgValidator.checkFieldValue(
				StringUtils.isPhoneNumberValid(mobileNumber), "mobile_number");
		ArgValidator.checkFieldNotNull(accountParam.getType(), "account_type");
		ArgValidator.checkFieldValueFromArray(accountParam.getType(),
				"account_type", AccountType.VENDOR, AccountType.BUYER);

		UserAccount userLogin = loginHelper.getUserAccountByUsername(username);
		if (userLogin != null) {
			logger.error("Account with username: {} already exists.", username);
			throw BadRequestException
					.nameAlreadyExists("User account with email address");
		}

		if (!StringUtils.isPasswordValid(password)) {
			logger.error("Given password format is invalid");
			throw BadRequestException.invalidPasswordFormat();
		}

		userLogin = new UserAccount();
		String id = UUIDUtil.createId(UserAccount.class);
		userLogin.setId(id);
		userLogin.setName(username);
		userLogin.setAccountName(accountName);
		userLogin.setRole(accountParam.getType().name());
		userLogin.setMobileNumber(mobileNumber);
		String passKey = UUID.randomUUID().toString();
		userLogin.setPassKey(passKey);
		String encodedPassword = passwordEncoder.encrypt(passKey, password);
		userLogin.setEncryptedPassword(encodedPassword);
		String clientAddress = HttpRequestResponseUtility
				.getClientIPAddress(httpRequest);
		userLogin.setClientIPAddress(clientAddress);
		userLogin.setEmailAddressConfirmationCode(StringUtils
				.generateConfirmationCode());
		userLogin.setEmailAddressVerified(false);
		userLogin.setMobileNumberConfirmationCode(StringUtils
				.generateConfirmationCode());
		// TODO Send email and sms with confirmation codes
		userLogin.setMobileNumberVerified(false);
		userLogin.setDisabled(false);

		logger.debug("New user account record creating in DB: {}", userLogin);
		dbClient.createObject(userLogin);

		logger.info("New user account persisted in DB: {}", id);

		return TaskResponseUtility.createResourceResponse(id,
				Endpoint.USER_ACCOUNT_INFO,
				"New user account created successfully.");

	}

	@RequestMapping(method = RequestMethod.POST, value = "/login", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public UserAccountInfoResponse login(
			@AuthenticationPrincipal BNBNeedsUser user) throws APIException {

		UserAccount userLogin = user.getAccount();
		String clientAddress = HttpRequestResponseUtility
				.getClientIPAddress(httpRequest);

		userLogin.setClientIPAddress(clientAddress);
		loginHelper.logAuthenticationAttempt(userLogin);

		UserAccountInfoResponse response = UserAccountResponseMapper
				.map(userLogin);

		return response;
	}

	/**
	 * This method is called automatically after Spring security calls POST
	 * '/logout'. Logout URL is configured in security-config.xml.
	 * 
	 * @see <security:logout-url/>
	 * 
	 * @return taskResponse an instance of TaskResponse
	 * @throws APIException
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/session-expired", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> afterLogout() throws APIException {

		return TaskResponseUtility
				.createTaskSuccessResponse("User logged out successfully.");
	}

	@RequestMapping(value = "/token", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> getCSRFToken(
			@AuthenticationPrincipal BNBNeedsUser user) throws APIException {

		return TaskResponseUtility.createTaskSuccessResponse("User: "
				+ user.getUsername() + " login successful.");
	}

	/*
	 * @RequestMapping(method = RequestMethod.GET, produces = {
	 * MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	 * public UserAccountInfoResponse getUserList(
	 * 
	 * @RequestParam("username") String userName) throws APIException {
	 * 
	 * UserAccount login = loginHelper.getUserAccountByUsername(userName);
	 * 
	 * if (login == null) { throw
	 * BadRequestException.queryRecordNotFound("username", userName); }
	 * 
	 * UserAccountInfoResponse resp = UserAccountResponseMapper.map(login);
	 * 
	 * return resp; }
	 */

	@RequestMapping(method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public UserAccountInfoResponse getAccount(
			@AuthenticationPrincipal BNBNeedsUser user) throws APIException {

		UserAccount login = user.getAccount();
		UserAccountInfoResponse resp = UserAccountResponseMapper.map(login);
		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public UserAccountInfoResponse getAccount(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String userId) throws APIException {

		ArgValidator.checkFieldUriType(userId, UserAccount.class, "user_id");

		UserAccount login = user.getAccount();
		if (!login.getId().equals(userId)) {
			throw ForbiddenException.unauthorizedAccess();
		}
		UserAccountInfoResponse resp = UserAccountResponseMapper.map(login);
		return resp;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/passwords", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> changePassword(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody ChangePasswordParam param) {

		logger.info("Request received to change the password for username: {}",
				user.getUsername());

		ArgValidator.checkFieldNotNull(param, "change_password");
		String oldPassword = param.getCurrentPassword();
		String newPassword = param.getNewPassword();
		String confirmPassword = param.getConfirmPassword();
		ArgValidator.checkFieldNotEmpty(oldPassword, "current_password");
		ArgValidator.checkFieldNotEmpty(newPassword, "new_password");
		ArgValidator
				.checkFieldNotEmpty(confirmPassword, "confirm_new_password");

		ArgValidator.checkFieldValue(!oldPassword.equals(newPassword),
				"new_password");

		if (!StringUtils.isPasswordValid(newPassword)) {
			throw BadRequestException.invalidPasswordFormat();
		}

		UserAccount userLogin = user.getAccount();

		String confirmNewPassword = param.getConfirmPassword();
		ArgValidator.checkFieldValue(confirmNewPassword.equals(newPassword),
				"confirm_new_password");
		ArgValidator.checkFieldValue(
				passwordEncoder.decrypt(userLogin.getPassKey(),
						userLogin.getEncryptedPassword()).equals(oldPassword),
				"old_password");
		String newPassKey = UUID.randomUUID().toString();
		userLogin.setPassKey(newPassKey);
		String encryptedNewPassword = passwordEncoder.encrypt(newPassKey,
				param.getConfirmPassword());
		userLogin.setEncryptedPassword(encryptedNewPassword);
		if (userLogin.isAutoGeneratedPassword()) {
			userLogin.setAutoGeneratedPassword(false);
		}
		dbClient.updateObject(userLogin);

		Template message = emailHelper
				.createMessageTemplate(MessageKeys.USER_PASSWORD_CHANGED);
		message.resolveBodyValues(user.getAccount().getAccountName());
		emailHelper.send(message, user.getAccount().getName());

		return TaskResponseUtility
				.createTaskSuccessResponse("User password changed successfully.");
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{username}/passwords", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> resetPassword(
			@PathVariable("username") String username) {

		logger.info("Request received to reset the password for username: {}",
				username);

		UserAccount userLogin = loginHelper.getUserAccountByUsername(username);

		ArgValidator.checkEntityNotNull(userLogin, username, true);

		long lastPasswordResetTimestamp = userLogin
				.getLastPasswordRestTimestamp();
		long currentTimestamp = System.currentTimeMillis();

		checkTimeForValidResetPasswordRequest(username,
				lastPasswordResetTimestamp, currentTimestamp);

		String newPassKey = UUID.randomUUID().toString();
		userLogin.setPassKey(newPassKey);
		String newPassword = passwordEncoder.generateRandomPassword();
		String encryptedPassword = passwordEncoder.encrypt(newPassKey,
				newPassword);
		userLogin.setLastPasswordRestTimestamp(currentTimestamp);
		userLogin.setEncryptedPassword(encryptedPassword);
		userLogin.setAutoGeneratedPassword(true);
		Template message = emailHelper
				.createMessageTemplate(MessageKeys.USER_PASSWORD_RESET);
		message.resolveSubjectValues(userLogin.getAccountName());
		message.resolveBodyValues(userLogin.getAccountName(), newPassword);

		String[] superAdminEmailAddressArray = null;
		if (AdminAccountService.SUPER_ADMIN_USERNAME.equals(username)) {
			String superAdminEmailAddresses = applicationSettings
					.getProperty(SUPER_ADMIN_EMAIL_ADDRESS_PROPERTY_NAME);
			if (!StringUtils.isEmpty(superAdminEmailAddresses)) {
				// TODO Remove the console output statement
				/*
				 * logger.info("Super admin email-address: {}",
				 * superAdminEmailAddresses);
				 */
				superAdminEmailAddressArray = superAdminEmailAddresses
						.split(",");
			}
		}

		// TODO Remove the console output statement

		/*
		 * logger.info("User name: {}, new password: {}", userLogin.getName(),
		 * newPassword);
		 */

		dbClient.updateObject(userLogin);

		if (superAdminEmailAddressArray != null) {
			emailHelper.send(message, superAdminEmailAddressArray);
		} else {
			emailHelper.send(message, userLogin.getName());
		}

		return TaskResponseUtility
				.createTaskSuccessResponse("User password reset successfully.");
	}

	@RequestMapping(method = RequestMethod.PUT, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> updateAccount(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody UpdateUserAccountParam accountParam)
			throws APIException {

		logger.info("API request received to update user account: {}",
				accountParam);

		boolean changed = false;
		boolean sendConfirmationCodeToNewEmailAddress = false;
		boolean sendConfirmationCodeToNewMobileNumber = false;

		UserAccount userLogin = user.getAccount();

		String existingUsername = user.getUsername();
		String newUsername = accountParam.getUsername();
		String accountName = accountParam.getAccountName();
		String mobileNumber = accountParam.getMobileNumber();

		boolean superUser = AdminAccountService.SUPER_ADMIN_USERNAME
				.equals(existingUsername)
				&& (!StringUtils.isEmpty(newUsername) && !AdminAccountService.SUPER_ADMIN_USERNAME
						.equals(newUsername.trim()));

		ArgValidator.checkFieldValue(!superUser, "email_address");

		if (!StringUtils.isEmpty(newUsername)
				&& !existingUsername.equals(newUsername)) {
			ArgValidator.checkFieldValue(
					StringUtils.isEmailAddressValid(newUsername),
					"email_address");
			userLogin.setName(newUsername);
			userLogin.setEmailAddressVerified(false);
			sendConfirmationCodeToNewEmailAddress = true;
			changed = true;
		}

		if (!StringUtils.isEmpty(accountName)
				&& !accountName.equals(userLogin.getAccountName())) {
			userLogin.setAccountName(accountName);
			changed = true;
		}

		if (!StringUtils.isEmpty(mobileNumber)
				&& !mobileNumber.equals(userLogin.getMobileNumber())) {
			ArgValidator.checkFieldValue(
					StringUtils.isPhoneNumberValid(mobileNumber),
					"mobile_number");
			userLogin.setMobileNumber(mobileNumber);
			userLogin.setMobileNumberVerified(false);
			sendConfirmationCodeToNewMobileNumber = true;
			changed = true;
		}

		if (!changed) {
			throw BadRequestException
					.create("Nothing to update in the user account.");
		}

		if (sendConfirmationCodeToNewEmailAddress) {
			String emailAddressConfirmationCode = StringUtils
					.generateConfirmationCode();
			userLogin
					.setEmailAddressConfirmationCode(emailAddressConfirmationCode);

			// TODO send email notification with confirmation code
			logger.info("Email address confirmation code: {}",
					emailAddressConfirmationCode);
		}
		if (sendConfirmationCodeToNewMobileNumber) {
			String mobileNumberConfirmationCode = StringUtils
					.generateConfirmationCode();
			userLogin
					.setMobileNumberConfirmationCode(mobileNumberConfirmationCode);
			// TODO send SMS notification with confirmation code
			logger.info("Mobile number confirmation code: {}",
					mobileNumberConfirmationCode);
		}
		logger.debug("Saving updated user account record creating in DB: {}.",
				userLogin);
		dbClient.updateObject(userLogin);
		user.setAccount(userLogin);

		logger.info("Updated user account persisted in DB: {}", userLogin);

		return TaskResponseUtility
				.createTaskSuccessResponse("User account updated successfully.");
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{username}/verifications", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> verifyEmailAddressAndMobileNumber(
			@PathVariable("username") String username,
			@RequestBody VerifyAccountParam param) {
		logger.info("API request to verify account with username: {}", username);

		UserAccount userAccount = loginHelper
				.getUserAccountByUsername(username);
		ArgValidator.checkEntityNotNull(userAccount, username, true);

		if (userAccount.isEmailAddressVerified()
				&& userAccount.isMobileNumberVerified()) {
			throw BadRequestException
					.create("User email address and mobile number are already verified.");
		}

		ArgValidator.checkFieldNotNull(param, "verify_account_request");
		String emailVerificationCode = param.getEmailVerificationCode();
		String smsVerificationCode = param.getSmsVerificationCode();
		boolean inputGiven = false;
		boolean emailVerificationDone = false;
		boolean smsVerificationDone = false;
		String message = null;
		if (!StringUtils.isEmpty(emailVerificationCode)) {
			inputGiven = true;
			if (userAccount.isEmailAddressVerified()) {
				throw BadRequestException
						.create("User email address is already verified.");
			}
			if (emailVerificationCode.equals(userAccount
					.getEmailAddressConfirmationCode())) {
				userAccount.setEmailAddressVerified(true);
				userAccount.setEmailAddressConfirmationCode(null);
				emailVerificationDone = true;
				message = "Email address is verified.";
				// TODO Send email notification that email is verified
			} else {
				throw VerificationRequestFailedException
						.invalidVerificationCode(VerificationType.email.name());
			}
		}
		if (!StringUtils.isEmpty(smsVerificationCode)) {
			inputGiven = true;
			if (userAccount.isMobileNumberVerified()) {
				throw BadRequestException
						.create("User mobile number is already verified.");
			}
			if (smsVerificationCode.equals(userAccount
					.getMobileNumberConfirmationCode())) {
				userAccount.setMobileNumberVerified(true);
				userAccount.setMobileNumberConfirmationCode(null);
				smsVerificationDone = true;
				message = "Mobile number is verified.";
				// TODO Send email notification that email is verified
			} else {
				throw VerificationRequestFailedException
						.invalidVerificationCode(VerificationType.sms.name());
			}

		}

		if (!inputGiven) {
			throw VerificationRequestFailedException
					.create("Invalid Request. There is nothing to verify.");
		}

		if (smsVerificationDone && emailVerificationDone) {
			message = "Both email address and mobile number are verified.";
		}

		dbClient.updateObject(userAccount);
		logger.info("User account verified successfully.");
		return TaskResponseUtility.createTaskSuccessResponse(message);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/{username}/verifications", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> sendEmailAddressAndMobileNumberConfirmationCodes(
			@PathVariable("username") String username,
			@RequestParam("type") String type) {
		logger.info(
				"API request to resend confirmation codes for username: {}",
				username);

		UserAccount userAccount = loginHelper
				.getUserAccountByUsername(username);
		ArgValidator.checkEntityNotNull(userAccount, username, true);

		if (userAccount.isMobileNumberVerified()
				&& userAccount.isEmailAddressVerified()) {
			throw BadRequestException
					.create("User email address and mobile number are already verified.");
		}

		ArgValidator.checkFieldValueFromEnum(type, "type",
				VerificationType.class);
		String emailConfirmationcode = null;
		String mobileNumberConfirmationcode = null;
		VerificationType enumType = VerificationType.valueOf(type);
		String message = null;
		switch (enumType) {
		case email:
			if (!userAccount.isEmailAddressVerified()) {
				emailConfirmationcode = StringUtils.generateConfirmationCode();
				userAccount
						.setEmailAddressConfirmationCode(emailConfirmationcode);
				// TODO Send email having the confirmation code
				logger.info("New email confirmation code: {}",
						emailConfirmationcode);
				message = "New confirmation code is sent by email.";
			} else {
				throw BadRequestException
						.create("User email address is already verified.");
			}
			break;
		case sms:
			if (!userAccount.isMobileNumberVerified()) {
				mobileNumberConfirmationcode = StringUtils
						.generateConfirmationCode();
				userAccount
						.setMobileNumberConfirmationCode(mobileNumberConfirmationcode);
				// TODO Send SMS text message having the confirmation code
				logger.info("New mobile number confirmation code: {}",
						mobileNumberConfirmationcode);
				message = "New confirmation code is sent by SMS.";
			} else {
				throw BadRequestException
						.create("User mobile number is already verified.");
			}
			break;
		case email_and_sms:
			if (userAccount.isMobileNumberVerified()
					|| userAccount.isEmailAddressVerified()) {
				throw BadRequestException
						.create("User email address or mobile number is already verified.");
			}
			emailConfirmationcode = StringUtils.generateConfirmationCode();
			mobileNumberConfirmationcode = StringUtils
					.generateConfirmationCode();
			userAccount
					.setMobileNumberConfirmationCode(mobileNumberConfirmationcode);
			userAccount.setEmailAddressConfirmationCode(emailConfirmationcode);
			// TODO Send email having the confirmation code
			logger.info(
					"New email and mobile number confirmation codes: {}, {}",
					emailConfirmationcode, mobileNumberConfirmationcode);
			message = "New confirmation codes are sent by email and SMS.";
			break;
		}

		dbClient.updateObject(userAccount);
		logger.info("Confirmation code(s) updated successfully.");
		return TaskResponseUtility.createTaskSuccessResponse(message);
	}
}
