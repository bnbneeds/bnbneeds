package com.bnbneeds.app.api.service.pipeline.buyer;

import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.ProductReview;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeleteReviewsOfProductsJob extends Job2<Void, String, Boolean> {

	private static final Logger logger = LoggerFactory.getLogger(DeleteReviewsOfProductsJob.class);

	private static final long serialVersionUID = 5078130251116735960L;

	@Override
	public Value<Void> run(String buyerId, Boolean forceDelete) throws Exception {
		Buyer buyer = DeleteBuyerJobROOT.queryBuyer(buyerId);
		if (buyer == null) {
			return null;
		}

		List<ProductReview> productReviews = queryList(ProductReview.class, new QueryFields("reviewedBy", buyer));

		if (!CollectionUtils.isEmpty(productReviews)) {

			if (!forceDelete) {
				throw DeleteDealerJob.cannotDeleteHasReferences(buyerId, Buyer.class, ProductReview.class);
			}
			DBOperationList reviewDeleteOperationList = new DBOperationList();

			logger.info("Get the product reviews of the buyer [{}] and delete them...", buyerId);
			for (Iterator<ProductReview> iterator = productReviews.iterator(); iterator.hasNext();) {
				ProductReview productReview = iterator.next();
				reviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, productReview);
			}
			transact(reviewDeleteOperationList);
		}
		return null;
	}
}