package com.bnbneeds.app.api.service.controller;

import static com.bnbneeds.app.api.service.response.DBObjectMapper.toNamedRelatedRestResponse;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.ProductResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.Enquiry;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.dealer.BuyerListResponse;
import com.bnbneeds.app.model.dealer.EnquiryInfoListResponse;
import com.bnbneeds.app.model.dealer.EnquiryInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.util.DateUtils;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/vendors/{vendorId}/product-enquiries")
public class VendorEnquiryService extends ResourceService {

	private static final Logger logger = LoggerFactory
			.getLogger(VendorEnquiryService.class);

	private static final String REQ_PARAM_ENQUIRY_DATE = "enquiryDate";
	private static final String REQ_PARAM_START_ENQUIRY_DATE = "startEnquiryDate";
	private static final String REQ_PARAM_END_ENQUIRY_DATE = "endEnquiryDate";

	@RequestMapping(method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR
			+ "') and hasDealership() and isRelated(#vendorId)")
	public EnquiryInfoListResponse getEnquiryList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info(
				"Request received to get enquiries by filter: {}, fetchSize: {}, offset: {} for vendor {}",
				filter, fetchSize, offset, vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");

		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);

		final String[] QUERY_FILTER_KEYS = { "name", "status", "buyerId",
				REQ_PARAM_ENQUIRY_DATE, REQ_PARAM_START_ENQUIRY_DATE,
				REQ_PARAM_END_ENQUIRY_DATE };

		EnquiryInfoListResponse response = new EnquiryInfoListResponse();

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields();

		boolean requestDateGiven = false;

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);

			switch (key) {
			/*
			 * Google datastore does not support cursors for IN operator. So
			 * filter enquiries by name manually.
			 * 
			 * case "name": ProductName productNameEntity =
			 * dbClient.queryObjectByFields( ProductName.class, new
			 * QueryFields("name", keyValueMap.get(key))); if (productNameEntity
			 * != null) { productListFields.add("productName",
			 * productNameEntity); } List<Product> vendorProducts =
			 * dbClient.queryList( Product.class, productListFields);
			 * 
			 * if (vendorProducts != null && !vendorProducts.isEmpty()) {
			 * fields.add("product in", vendorProducts); } break;
			 */
			case "buyerId":
				ArgValidator.checkFieldUriType(value, Buyer.class, key);
				Buyer buyer = dbClient.queryObject(Buyer.class, value);
				ArgValidator.checkEntityBlacklisted(buyer, value, false);
				fields.add("buyer", buyer);
				break;
			case "status":
				ArgValidator.checkFieldValue(!EntityStatus.BLACKLISTED.name()
						.equals(value.toUpperCase()), key);
				fields.add("entityStatus", value.toUpperCase());
				break;
			case REQ_PARAM_ENQUIRY_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("enquiryTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("enquiryTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_ENQUIRY_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_ENQUIRY_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				fields.add("enquiryTimestamp >=", startRequestDate.getTime());
				break;
			case REQ_PARAM_END_ENQUIRY_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_ENQUIRY_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils
						.getEndOfDayTimestamp(endRequestDate);
				fields.add("enquiryTimestamp <=", endTimestamp2);
				break;
			default:
				break;
			}

		}

		ResultSetIterator<Enquiry> resultSetIterator = null;
		String productName = keyValueMap.get("name");
		do {
			resultSetIterator = getResultSetIterator(Enquiry.class, fields,
					offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					Enquiry item = resultSetIterator.next();

					if (item == null) {
						continue;
					} else if (item.isBlacklisted()
							|| item.getBuyer().isBlacklisted()
							|| item.getProduct().isBlacklisted()
							|| item.getProduct().getVendor().isBlacklisted()) {

						logger.info(
								"Skipping this enquiry [{}] as it (or its parent entities) is (are) BLACKLISTED.",
								item.getId());
						continue;

					} else if (productName != null
							&& !productName.equals(item.getProduct()
									.getProductName().getName())) {
						logger.info(
								"Skipping this enquiry [{}] as its product does not match with name: {}",
								item.getId(), productName);
						continue;
					} else if (!vendorId.equals(item.getProduct().getVendor()
							.getId())) {
						logger.info(
								"Skipping this enquiry [{}] as its product's vendor id does not match with vendor: {}",
								item.getId(), vendorId);
						continue;
					} else {
						EnquiryInfoResponse infoResponse = ProductResponseMapper
								.mapProductEnquiry(item, DATE_FORMAT);
						infoResponse
								.setLink(Endpoint.VENDOR_PRODUCT_ENQUIRY_INFO
										.get(vendorId, item.getId()));
						response.addEnquiryInfoResponse(infoResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null
						&& offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}
		} while (response.size() < fetchSize);

		logger.info(
				"Returning response of type enquiryListResponse of size {}.",
				response.size());
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR
			+ "') and hasDealership() and isRelated(#vendorId)")
	public EnquiryInfoResponse getEnquiry(
			@PathVariable("vendorId") String vendorId,
			@PathVariable("id") String enquiryId) {

		logger.info(
				"Request received to retrive enquiry details of {} for vendor {}",
				enquiryId, vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(enquiryId, Enquiry.class, "enquiry_id");

		Enquiry enquiry = dbClient.queryObject(Enquiry.class, enquiryId);

		ArgValidator.checkEntityBlacklisted(enquiry, enquiryId, true);
		ArgValidator.checkEntityBlacklisted(enquiry.getBuyer(), enquiry
				.getBuyer().getId(), false);
		Product product = enquiry.getProduct();
		ArgValidator.checkEntityBlacklisted(product, product.getId(), false);
		ArgValidator.checkEntityBlacklisted(product.getVendor(), product
				.getVendor().getId(), false);

		if (!vendorId.equals(enquiry.getProduct().getVendor().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}
		EnquiryInfoResponse resp = new EnquiryInfoResponse();
		if (enquiry != null) {
			ProductResponseMapper.mapProductEnquiry(enquiry, DATE_FORMAT, resp);
			resp.setLink(Endpoint.VENDOR_PRODUCT_ENQUIRY_INFO.get(vendorId,
					enquiryId));
		}
		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/associated-buyers", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR
			+ "') and hasDealership() and isRelated(#vendorId)")
	public BuyerListResponse getAssociatedBuyerList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId,
			@RequestParam(name = REQ_PARAM_FILTER) String filter) {

		logger.info(
				"Request received to get associated buyers of product enquiries to vendor: {}",
				vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);

		ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);

		final String[] QUERY_FILTER_KEYS = { REQ_PARAM_ENQUIRY_DATE,
				REQ_PARAM_START_ENQUIRY_DATE, REQ_PARAM_END_ENQUIRY_DATE };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields();

		boolean requestDateGiven = false;

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case REQ_PARAM_ENQUIRY_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("enquiryTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("enquiryTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_ENQUIRY_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_ENQUIRY_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				fields.add("enquiryTimestamp >=", startRequestDate.getTime());
				break;
			case REQ_PARAM_END_ENQUIRY_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_ENQUIRY_DATE
								+ " with this field.");
				ArgValidator.checkFieldValue(
						StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils
						.getEndOfDayTimestamp(endRequestDate);
				fields.add("enquiryTimestamp <=", endTimestamp2);
				break;
			default:
				break;
			}
		}

		List<Enquiry> enquiryList = dbClient.queryList(Enquiry.class, fields);

		BuyerListResponse buyerListResponse = new BuyerListResponse();
		Set<String> uriSet = new HashSet<String>();
		for (Iterator<Enquiry> iterator = enquiryList.iterator(); iterator
				.hasNext();) {
			Enquiry enquiry = iterator.next();

			if (enquiry.isBlacklisted()) {
				continue;
			}

			Product product = enquiry.getProduct();
			if (product.isBlacklisted()) {
				continue;
			}

			Vendor productVendor = product.getVendor();
			if (!vendorId.equals(productVendor.getId())) {
				continue;
			}

			Buyer buyer = enquiry.getBuyer();
			if (buyer != null && !buyer.isBlacklisted()) {
				if (uriSet.add(buyer.getId())) {
					buyerListResponse
							.addNamedResourceResponse(toNamedRelatedRestResponse(buyer));
				}
			}
		}
		logger.info("Returning buyer list of size: {}.",
				buyerListResponse.size());

		return buyerListResponse;
	}
}
