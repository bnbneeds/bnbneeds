package com.bnbneeds.app.api.service.controller;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.response.DataObjectResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.DbClientUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Notification;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.notification.NotificationInfoResponse;
import com.bnbneeds.app.model.notification.NotificationListParam;
import com.bnbneeds.app.model.notification.NotificationListResponse;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/notifications")
public class NotificationService extends ResourceService {

	private static final Logger logger = LoggerFactory
			.getLogger(NotificationService.class);

	@RequestMapping(method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public NotificationListResponse getNotifications(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		String dealerId = user.getRelationshipId();

		logger.info(
				"Request received to get notifications by filter: {}, fetchSize: {}, offset: {}",
				filter, fetchSize, offset);

		final String[] QUERY_FILTER_KEYS = { "startTimestamp", "endTimestamp" };

		NotificationListResponse response = new NotificationListResponse();

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields();

		/*
		 * String startTimestampkey = "startTimestamp"; if
		 * (keyValueMap.isEmpty() ||
		 * !keyValueMap.containsKey(startTimestampkey)) {
		 * keyValueMap.put(startTimestampkey,
		 * String.valueOf(System.currentTimeMillis())); }
		 */

		for (String key : keyValueMap.keySet()) {

			switch (key) {
			case "startTimestamp":
				fields.add("timestamp >=", Long.valueOf(keyValueMap.get(key)));
				break;
			case "endTimestamp":
				fields.add("timestamp <=", Long.valueOf(keyValueMap.get(key)));
				break;
			default:
				break;
			}

		}

		ResultSetIterator<Notification> resultSetIterator = null;
		do {
			resultSetIterator = DbClientUtility
					.getNotificationResultSetIterator(dbClient, fields, offset,
							fetchSize);
			if (resultSetIterator != null) {

				while (resultSetIterator.hasNext()) {
					Notification item = resultSetIterator.next();
					RecipientType recipientType = RecipientType.valueOf(item
							.getRecipientType());

					NotificationInfoResponse infoResponse = map(item, user);
					if (user.isVendor()) {
						switch (recipientType) {
						case VENDOR:
							if (item.getRecipientId().equals(dealerId)) {
								response.addInfoResponse(infoResponse);
							}
							break;
						case ALL_VENDORS:
						case VENDORS_AND_ADMIN:
						case EVERYONE:
							response.addInfoResponse(infoResponse);
							break;
						default:
							break;
						}
					}

					if (user.isBuyer()) {
						switch (recipientType) {
						case BUYER:
							if (item.getRecipientId().equals(dealerId)) {
								response.addInfoResponse(infoResponse);
							}
							break;
						case ALL_BUYERS:
						case BUYERS_AND_ADMIN:
						case EVERYONE:
							response.addInfoResponse(infoResponse);
							break;
						default:
							break;
						}
					}

					if (user.isAdmin()) {
						switch (recipientType) {
						case ADMIN:
						case VENDORS_AND_ADMIN:
						case BUYERS_AND_ADMIN:	
						case EVERYONE:
							response.addInfoResponse(infoResponse);
							break;
						default:
							break;
						}
					}

					if (user.isSuperAdmin()) {
						switch (recipientType) {
						case SUPER_ADMIN:
						case EVERYONE:
							response.addInfoResponse(infoResponse);
							break;
						default:
							break;
						}
					}
				}

				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null
						&& offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}
		} while (response.size() < fetchSize);

		logger.info("Returning notification list of size {}", response.size());

		return response;

	}

	@RequestMapping(method = RequestMethod.PUT, path = "/mark-as-read", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> markNotificationsAsRead(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody NotificationListParam param) {

		ArgValidator.checkFieldNotNull(param, "notification_list_param");
		List<String> notificationIdList = param.getNotificationIdList();
		ArgValidator
				.checkFieldNotEmpty(notificationIdList, "notification_list");
		DBOperationList dbOperationList = new DBOperationList();
		String userId = user.getAccount().getId();
		for (Iterator<String> iterator = notificationIdList.iterator(); iterator
				.hasNext();) {
			String notificationId = iterator.next();
			ArgValidator.checkFieldUriType(notificationId, Notification.class,
					"notification_id");
			Notification notification = dbClient.queryObject(
					Notification.class, notificationId);
			if (notification != null) {
				if (!notification.isRead(userId)) {
					notification.addReadBy(userId);
					dbOperationList.addOperation(OperationType.UPDATE,
							notification);
				}
			}

		}
		dbClient.transact(dbOperationList);
		logger.info("Task submitted to mark notifications as read.");
		return TaskResponseUtility
				.createTaskSubmittedResponse("Task submitted to mark notifications as read.");

	}

	@RequestMapping(method = RequestMethod.PUT, path = "/mark-as-unread", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> markNotificationsAsUnread(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody NotificationListParam param) {

		ArgValidator.checkFieldNotNull(param, "notification_list_param");
		List<String> notificationIdList = param.getNotificationIdList();
		ArgValidator
				.checkFieldNotEmpty(notificationIdList, "notification_list");
		DBOperationList dbOperationList = new DBOperationList();
		String userId = user.getAccount().getId();
		for (Iterator<String> iterator = notificationIdList.iterator(); iterator
				.hasNext();) {
			String notificationId = iterator.next();
			ArgValidator.checkFieldUriType(notificationId, Notification.class,
					"notification_id");
			Notification notification = dbClient.queryObject(
					Notification.class, notificationId);
			if (notification != null) {
				if (notification.isRead(userId)) {
					notification.removeReadBy(userId);
					dbOperationList.addOperation(OperationType.UPDATE,
							notification);
				}
			}

		}
		dbClient.transact(dbOperationList);
		logger.info("Task submitted to mark notifications as unread.");
		return TaskResponseUtility
				.createTaskSubmittedResponse("Task submitted to mark notifications as unread.");

	}

	private NotificationInfoResponse map(Notification item, BNBNeedsUser user) {

		NotificationInfoResponse infoResponse = new NotificationInfoResponse();
		String userId = user.getAccount().getId();
		if (item != null) {
			DataObjectResponseMapper.mapDataObject(item, infoResponse);
			infoResponse.setTimestamp(item.getTimestamp());
			infoResponse.setNotificationType(item.getType());
			infoResponse.setNotificationText(item.getNotificationText());
			infoResponse.setRecipientType(item.getRecipientType());
			String recipientId = item.getRecipientId();
			if (!StringUtils.isEmpty(recipientId)) {
				infoResponse.setRecipientId(recipientId);
			}
			String objectId = item.getEntityId();
			if (!StringUtils.isEmpty(objectId)) {
				infoResponse.setEntityId(objectId);
			}
			String objectName = item.getEntityName();
			if (!StringUtils.isEmpty(objectName)) {
				infoResponse.setEntityName(objectName);
			}
			String objectLink = item.getLink();
			if (!StringUtils.isEmpty(objectLink)) {
				infoResponse.setEntityLink(objectLink);
			}
			String triggeredBy = item.getTriggeredByDealerName();
			if (StringUtils.hasText(triggeredBy)) {
				infoResponse.setTriggeredBy(triggeredBy);
			}
			infoResponse.setRead(item.isRead(userId));
		}

		return infoResponse;

	}
}
