package com.bnbneeds.app.api.service.mapreduce;

import com.google.appengine.tools.mapreduce.MapSettings;

public class MapReduceUtils {

	private static final String QUEUE = "mapreduce-worker-queue";

	public static MapSettings getMapSettings() {
		MapSettings settings = new MapSettings.Builder().setWorkerQueueName(QUEUE).build();
		return settings;
	}

}
