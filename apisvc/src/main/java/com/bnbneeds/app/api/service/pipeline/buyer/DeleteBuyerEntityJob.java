package com.bnbneeds.app.api.service.pipeline.buyer;

import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryObjectByFields;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.util.NotificationUtility;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.Notification;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeleteBuyerEntityJob extends Job2<Void, String, Boolean> {

	private static final Logger logger = LoggerFactory.getLogger(DeleteBuyerEntityJob.class);

	private static final long serialVersionUID = 6996013296643909999L;

	@Override
	public Value<Void> run(String buyerId, Boolean deleteUserAccount) throws Exception {

		Buyer buyer = DeleteBuyerJobROOT.queryBuyer(buyerId);
		if (buyer == null) {
			return null;
		}
		DBOperationList operationList = new DBOperationList();
		logger.info("Delete the buyer entity: {}", buyerId);
		operationList.addOperation(OperationType.MARK_AS_DELETED, buyer);

		UserAccount userAccount = queryObjectByFields(UserAccount.class, new QueryFields("relationshipId", buyerId));
		if (userAccount != null) {
			if (deleteUserAccount) {
				logger.info("Finally, delete the user account: {}", userAccount);
				operationList.addOperation(OperationType.MARK_AS_DELETED, userAccount);
			} else {
				logger.info("Finally, updating the user account by setting relationship id to null");
				userAccount.setRelationshipId(null);
				operationList.addOperation(OperationType.UPDATE, userAccount);
			}
		}
		Notification notification = NotificationUtility.createNotification(SystemEventType.BUYER_DELETED,
				RecipientType.VENDORS_AND_ADMIN, null, buyerId, buyer.getName(), Endpoint.BUYER_INFO.get(buyerId),
				null);
		operationList.addOperation(OperationType.INSERT, notification);

		transact(operationList);
		return null;
	}

}