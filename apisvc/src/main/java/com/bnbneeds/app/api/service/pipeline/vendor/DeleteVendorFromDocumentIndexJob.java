package com.bnbneeds.app.api.service.pipeline.vendor;

import com.bnbneeds.app.api.service.documentsearch.Constants;
import com.bnbneeds.app.api.service.documentsearch.SearchAPIUtility;
import com.google.appengine.tools.pipeline.Job0;
import com.google.appengine.tools.pipeline.Value;

public class DeleteVendorFromDocumentIndexJob extends Job0<Void> {

	private static final long serialVersionUID = 7758526280568388045L;

	private String vendorId;

	public DeleteVendorFromDocumentIndexJob(String vendorId) {
		super();
		this.vendorId = vendorId;
	}

	@Override
	public String getJobDisplayName() {
		return "Remove document of vendor with ID: " + vendorId + " from index";
	}

	@Override
	public Value<Void> run() throws Exception {

		try {
			SearchAPIUtility.deleteDocument(Constants.VENDOR_INDEX, vendorId);
		} catch (Exception e) {
			// ignore
		}
		return null;
	}

}
