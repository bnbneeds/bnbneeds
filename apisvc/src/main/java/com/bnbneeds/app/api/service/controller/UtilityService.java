package com.bnbneeds.app.api.service.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.exceptions.APIException;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.db.client.DbClientUtils;
import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.model.EntityCountResponse;

@RestController
@RequestMapping(path = "/utilities")
public class UtilityService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(UtilityService.class);

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET, value = "/entity-count", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public <T extends DataObject> EntityCountResponse getEntity(@RequestParam(name = "entityType") String entityType)
			throws APIException {

		Class<T> clazz = null;

		try {
			clazz = (Class<T>) DbClientUtils.getClassByName(entityType);
		} catch (ClassNotFoundException e) {
			throw BadRequestException.parameterInvalid("entityType", e.getMessage());
		}

		int count = dbClient.countOf(clazz);
		return new EntityCountResponse(count);
	}

}
