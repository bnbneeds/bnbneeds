package com.bnbneeds.app.api.service.util;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpRequestResponseUtility {

	private static final Logger logger = LoggerFactory
			.getLogger(HttpRequestResponseUtility.class);

	private static final String REQUEST_CLIENT_IP_ADDRESS_HEADER = "X-Client-IP-Address";

	/**
	 * Returns the client IP address. First it checks the value in the request
	 * header: {@code X-Client-IP-Address}. If its null, then it return the
	 * value using {@code httpRequest.getRemoteAddr()}
	 * 
	 * @param httpRequest
	 *            - the request
	 * @return the IP address of the requested client
	 */
	public static String getClientIPAddress(HttpServletRequest httpRequest) {

		String clientAddress = httpRequest
				.getHeader(REQUEST_CLIENT_IP_ADDRESS_HEADER);

		if (clientAddress != null) {
			logger.info(
					"Requested client IP address specified in header {}: {}",
					REQUEST_CLIENT_IP_ADDRESS_HEADER, clientAddress);
		} else {
			clientAddress = httpRequest.getRemoteAddr();
			logger.info("Requested client IP adrress: {}", clientAddress);
		}

		return clientAddress;
	}

	public static String getParameter(HttpServletRequest httpRequest,
			String parameterName) {
		return httpRequest.getParameter(parameterName);
	}

}
