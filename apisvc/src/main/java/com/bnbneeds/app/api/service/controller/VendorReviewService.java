package com.bnbneeds.app.api.service.controller;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.APIException;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.ReviewResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.DbClientUtility;
import com.bnbneeds.app.api.service.util.ReviewUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.OrderBy;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.db.client.model.VendorAverageRating;
import com.bnbneeds.app.db.client.model.VendorReview;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.review.AverageRatingResponse;
import com.bnbneeds.app.model.review.ReviewParam;
import com.bnbneeds.app.model.review.VendorReviewInfoListResponse;
import com.bnbneeds.app.model.review.VendorReviewInfoResponse;

/**
 * 
 * @author Amit Herlekar
 *
 */

@RestController
@RequestMapping(value = "/vendors/{vendorId}/reviews")
public class VendorReviewService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(VendorReviewService.class);

	private void updateAverageRating(Vendor vendor) {
		VendorAverageRating averageRating = dbClient.queryObjectByFields(VendorAverageRating.class,
				new QueryFields("vendor", vendor));

		boolean created = false;
		if (averageRating == null) {
			averageRating = new VendorAverageRating();
			averageRating.setId(UUIDUtil.createId(VendorAverageRating.class));
			averageRating.setVendor(vendor);
			created = true;
		}
		QueryFields fields = new QueryFields("vendor", vendor);
		fields.add("entityStatus", EntityStatus.APPROVED.name());
		List<VendorReview> reviews = dbClient.queryListBySortOrder(VendorReview.class, fields,
				ReviewUtility.MAX_NUMBER_OF_REVIEWS_TO_CALCULATE_AVERAGE_RATING, OrderBy.DESC, "timestamp");
		ReviewUtility.calculateAverageRating(reviews, averageRating);
		DBOperationList opList = new DBOperationList();
		if (created) {
			opList.addOperation(OperationType.INSERT, averageRating);
		} else {
			opList.addOperation(OperationType.UPDATE, averageRating);
		}
		dbClient.transact(opList);
	}

	@RequestMapping(method = RequestMethod.POST, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and hasDealership()")
	public ResponseEntity<CreateResourceResponse> createBuyerReview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId, @RequestBody ReviewParam reviewParam) throws APIException {

		logger.info("Request received to post a review of vendor with id: {}", vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "buyer_id");
		ArgValidator.checkFieldNotNull(reviewParam, "review");

		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);

		Buyer buyer = dbClient.queryObject(Buyer.class, user.getRelationshipId());

		ArgValidator.checkEntityBlacklisted(buyer, buyer.getId(), true);

		QueryFields fields = new QueryFields("vendor", vendor);
		fields.add("reviewedBy", buyer);

		VendorReview vendorReview = dbClient.queryObjectByFields(VendorReview.class, fields);

		if (vendorReview != null) {
			throw BadRequestException.reviewAlreadyExists(vendorId);
		}

		VendorReview review = ReviewUtility.validateCreateReview(reviewParam, Vendor.class);
		String uid = UUIDUtil.createId(VendorReview.class);
		review.setId(uid);
		review.setVendor(vendor);
		review.setReviewedBy(buyer);
		review.setTimestamp(System.currentTimeMillis());

		dbClient.createObject(review);

		updateAverageRating(vendor);

		URI reviewLink = Endpoint.VENDOR_REVIEW_INFO.get(vendorId, uid);

		recordEvent(SystemEventType.VENDOR_REVIEW_POSTED, RecipientType.VENDOR, vendorId, uid, null, reviewLink,
				buyer.getName());

		logger.info("Vendor review created successfully.", review);
		return TaskResponseUtility.createResourceResponse(uid, reviewLink, "Vendor review posted successfully.");
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{reviewId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.BUYER + "','" + Role.ADMIN + "') and hasDealership()")
	public ResponseEntity<TaskResponse> updateBuyerReview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId, @PathVariable("reviewId") String reviewId,
			@RequestBody ReviewParam reviewParam) throws APIException {

		logger.info("Request received to update vendor review with id: {}", reviewId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(reviewId, VendorReview.class, "review_id");

		ArgValidator.checkFieldNotNull(reviewParam, "review");

		VendorReview vendorReview = dbClient.queryObject(VendorReview.class, reviewId);

		if (user.isBuyer()) {
			ArgValidator.checkEntityBlacklisted(vendorReview, reviewId, true);
			if (!vendorReview.getReviewedBy().getId().equals(user.getRelationshipId())) {
				throw ForbiddenException.unauthorizedAccess();
			} else {
				Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
				Buyer buyer = dbClient.queryObject(Buyer.class, user.getRelationshipId());
				ArgValidator.checkEntityBlacklisted(buyer, buyer.getId(), true);
				ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);
			}
		}

		if (!vendorReview.getVendor().getId().equals(vendorId)) {
			throw ForbiddenException.unauthorizedAccess();
		}

		ReviewUtility.validateUpdateReview(reviewParam, vendorReview);
		vendorReview.setTimestamp(System.currentTimeMillis());

		dbClient.updateObject(vendorReview);

		updateAverageRating(vendorReview.getVendor());

		logger.info("Vendor review updated successfully: {}.", vendorReview);
		return TaskResponseUtility.createTaskSuccessResponse("Vendor review updated successfully.");
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{reviewId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.BUYER + "', '" + Role.ADMIN + "') and hasDealership()")
	public ResponseEntity<TaskResponse> deleteVendorReview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId, @PathVariable("reviewId") String reviewId) throws APIException {

		logger.info("Request received to delete vendor review with id: {}", reviewId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(reviewId, VendorReview.class, "review_id");

		VendorReview vendorReview = dbClient.queryObject(VendorReview.class, reviewId);
		if (user.isAdmin()) {
			ArgValidator.checkEntityApproved(vendorReview, reviewId, true);
		} else {
			// Buyer
			ArgValidator.checkEntityBlacklisted(vendorReview, reviewId, true);
			if (!vendorReview.getReviewedBy().getId().equals(user.getRelationshipId())) {
				throw ForbiddenException.unauthorizedAccess();
			} else {
				Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
				ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);
				Buyer buyer = dbClient.queryObject(Buyer.class, user.getRelationshipId());
				ArgValidator.checkEntityBlacklisted(buyer, buyer.getId(), true);

			}

		}

		if (!vendorReview.getVendor().getId().equals(vendorId)) {
			throw ForbiddenException.unauthorizedAccess();
		}

		dbClient.markAsDeleted(vendorReview);

		updateAverageRating(vendorReview.getVendor());

		logger.info("Vendor review deleted successfully.");
		return TaskResponseUtility.createTaskSuccessResponse("Vendor review deleted successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "','" + Role.BUYER + "','" + Role.ADMIN + "') and  hasDealership()")
	public VendorReviewInfoListResponse getReviews(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {
		logger.info("Request received to get vendor reviews by fetchSize: {}, offset: {}", fetchSize, offset);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		if (user.isDealer()) {
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);
		}

		if (user.isVendor() && !vendor.getId().equals(user.getRelationshipId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		final String[] QUERY_FILTER_KEYS = { "reviewedBy" };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields("vendor", vendor);

		for (String key : keyValueMap.keySet()) {

			String value = keyValueMap.get(key);

			switch (key) {
			case "reviewedBy":
				ArgValidator.checkFieldUriType(value, Buyer.class, key);
				Buyer buyer = dbClient.queryObject(Buyer.class, value);
				if (!user.isAdmin()) {
					ArgValidator.checkEntityBlacklisted(buyer, value, true);
				}
				fields.add("reviewedBy", buyer);
				break;
			default:
				break;
			}
		}

		ResultSetIterator<VendorReview> resultSetIterator = null;

		VendorReviewInfoListResponse response = new VendorReviewInfoListResponse();
		do {

			resultSetIterator = DbClientUtility.getResultSetIteratorOrderByDesc(dbClient, VendorReview.class, fields,
					offset, fetchSize, "timestamp");

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					VendorReview item = resultSetIterator.next();

					if (item == null) {
						continue;
					}

					if (!user.isAdmin() && !item.isApproved()) {
						continue;
					}

					Buyer buyer = item.getReviewedBy();
					if (!user.isAdmin() && buyer.isBlacklisted()) {
						continue;
					}

					VendorReviewInfoResponse infoResponse = ReviewResponseMapper.map(item, DATE_FORMAT);
					response.addReviewInfoResponse(infoResponse);

				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}
		} while (response.size() < fetchSize);
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/average-rating", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "','" + Role.BUYER + "','" + Role.ADMIN + "') and  hasDealership()")
	public AverageRatingResponse getAverageRating(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId) {

		logger.info("Request received to get average rating of vendor with id: {}", vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);

		if (user.isVendor() && !vendor.getId().equals(user.getRelationshipId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		AverageRatingResponse response = new AverageRatingResponse();
		VendorAverageRating averageRating = dbClient.queryObjectByFields(VendorAverageRating.class,
				new QueryFields("vendor", vendor));
		if (averageRating != null) {
			ReviewResponseMapper.map(averageRating, response);
		}

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{reviewId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "','" + Role.BUYER + "','" + Role.ADMIN + "') and hasDealership()")
	public VendorReviewInfoListResponse getReview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId, @PathVariable("reviewId") String reviewId) {
		logger.info("Request received to get vendor review for reviewId: {}", reviewId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(reviewId, VendorReview.class, "review_id");

		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);

		if (user.isVendor() && !vendor.getId().equals(user.getRelationshipId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		VendorReview vendorReview = dbClient.queryObject(VendorReview.class, reviewId);
		ArgValidator.checkEntityNotNull(vendorReview, reviewId, true);
		if (user.isDealer()) {
			ArgValidator.checkEntityBlacklisted(vendorReview, vendorId, true);
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);
		}
		if (user.isBuyer() && !vendorReview.getReviewedBy().getId().equals(user.getRelationshipId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		VendorReviewInfoResponse infoResponse = ReviewResponseMapper.map(vendorReview, DATE_FORMAT);
		VendorReviewInfoListResponse listResponse = new VendorReviewInfoListResponse();
		listResponse.addReviewInfoResponse(infoResponse);

		return listResponse;
	}

}
