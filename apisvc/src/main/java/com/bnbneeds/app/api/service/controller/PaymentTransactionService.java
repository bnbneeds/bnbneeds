package com.bnbneeds.app.api.service.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import com.bnbneeds.app.api.service.response.PaymentTransactionResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.PaymentTransactionUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.Dealer;
import com.bnbneeds.app.db.client.model.PaymentTransaction;
import com.bnbneeds.app.db.client.model.PaymentTransactionLinkedEntity;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.PaymentTransactionInfoListResponse;
import com.bnbneeds.app.model.dealer.PaymentTransactionInfoResponse;
import com.bnbneeds.app.model.dealer.UpdatePaymentTransactionParam;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.util.StringUtils;

public abstract class PaymentTransactionService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(PaymentTransactionService.class);

	public abstract Class<? extends Dealer> getDealerType();

	public PaymentTransactionInfoListResponse getPaymentTransactionList(BNBNeedsUser user, String dealerId,
			String filter, int fetchSize, String offset) {

		logger.info("Request received to get list of payment transactions made by dealer ID: {}", dealerId);

		Dealer dealer = dbClient.queryObject(getDealerType(), dealerId);
		ArgValidator.checkEntityNotNull(dealer, dealerId, true);

		if (user.isDealer()) {
			ArgValidator.checkEntityBlacklisted(dealer, dealerId, true);
		}

		ArgValidator.checkFieldUriType(dealerId, getDealerType(), "dealer_id");

		PaymentTransactionInfoListResponse response = PaymentTransactionUtility.getPaymentTransactionList(dbClient,
				dealerId, filter, offset, fetchSize);

		logger.info("Exit retuning response list of size: {}", response.size());

		return response;
	}

	public PaymentTransactionInfoResponse getPaymentTransaction(BNBNeedsUser user, String dealerId,
			String paymentTransactionId) {

		logger.info(
				"Request received to get payment transaction for bidding subscription for dealer ID: {}, transaction ID: {}",
				dealerId, paymentTransactionId);

		ArgValidator.checkFieldUriType(dealerId, getDealerType(), "dealer_id");
		ArgValidator.checkFieldUriType(paymentTransactionId, PaymentTransaction.class, "transaction_id");

		Dealer dealer = dbClient.queryObject(getDealerType(), dealerId);
		ArgValidator.checkEntityNotNull(dealer, dealerId, true);

		if (user.isDealer()) {
			ArgValidator.checkEntityBlacklisted(dealer, dealerId, true);
		}

		PaymentTransaction transaction = dbClient.queryObject(PaymentTransaction.class, paymentTransactionId);
		ArgValidator.checkEntityNotNull(transaction, paymentTransactionId, true);

		QueryFields fields = new QueryFields("dealerId", dealerId);
		fields.add("paymentTransaction", transaction);

		PaymentTransactionLinkedEntity paymentTransactionLinkedEntity = dbClient
				.queryObjectByFields(PaymentTransactionLinkedEntity.class, fields);

		ArgValidator.checkEntityNotNull(paymentTransactionLinkedEntity, paymentTransactionId, true);

		Endpoint paymentTransactionEndpoint = PaymentTransactionUtility
				.getPaymentTransactionInfoEndpoint(getDealerType().getSimpleName());

		PaymentTransactionInfoResponse response = PaymentTransactionResponseMapper.map(transaction,
				paymentTransactionEndpoint.get(dealerId, paymentTransactionId));

		logger.info("Exit returning payment transaction info response {}.", response);

		return response;
	}

	public ResponseEntity<TaskResponse> updatePaymentTransaction(BNBNeedsUser user, String dealerId,
			String paymentTransactionId, UpdatePaymentTransactionParam param) {

		logger.info(
				"Request received to get payment transaction for bidding subscription for dealer ID: {}, transaction ID: {}",
				dealerId, paymentTransactionId);

		ArgValidator.checkFieldUriType(dealerId, getDealerType(), "dealer_id");
		ArgValidator.checkFieldUriType(paymentTransactionId, PaymentTransaction.class, "transaction_id");
		ArgValidator.checkFieldNotNull(param, "update_payment_transaction");

		Dealer dealer = dbClient.queryObject(getDealerType(), dealerId);
		ArgValidator.checkEntityNotNull(dealer, dealerId, true);

		if (user.isDealer()) {
			ArgValidator.checkEntityBlacklisted(dealer, dealerId, true);
		}

		PaymentTransaction transaction = dbClient.queryObject(PaymentTransaction.class, paymentTransactionId);
		ArgValidator.checkEntityNotNull(transaction, paymentTransactionId, true);

		QueryFields fields = new QueryFields("dealerId", dealerId);
		fields.add("paymentTransaction", transaction);

		PaymentTransactionLinkedEntity paymentTransactionLinkedEntity = dbClient
				.queryObjectByFields(PaymentTransactionLinkedEntity.class, fields);
		ArgValidator.checkEntityNotNull(paymentTransactionLinkedEntity, paymentTransactionId, true);

		boolean changed = verifyAndUpdateChanges(param, transaction);

		if (changed) {
			dbClient.updateObject(transaction);
		}

		return TaskResponseUtility.createTaskSuccessResponse("Payment transaction updated successfully");

	}

	private boolean verifyAndUpdateChanges(UpdatePaymentTransactionParam param, PaymentTransaction transaction) {
		boolean changed = false;
		String paymentTransactionId = param.getTransactionId();
		String transactionStatus = param.getTransactionStatus();
		Double amount = param.getAmount();
		String paymentGateway = param.getPaymentGateway();
		String paymentMethod = param.getPaymentMethod();
		Long timestamp = param.getTimestamp();
		String description = param.getTransactionDescription();

		if (StringUtils.hasText(paymentTransactionId) && !paymentTransactionId.equals(transaction.getTransactionId())) {
			transaction.setTransactionId(paymentTransactionId);
			changed = true;
		}

		if (StringUtils.hasText(transactionStatus) && !transactionStatus.equals(transaction.getTransactionStatus())) {
			transaction.setTransactionStatus(transactionStatus);
			changed = true;
		}

		if (StringUtils.hasText(description) && !description.equals(transaction.getTransactionDescription())) {
			transaction.setTransactionDescription(description);
			changed = true;
		}

		if (amount != null && amount != transaction.getAmount()) {
			transaction.setAmount(amount);
			changed = true;
		}

		if (StringUtils.hasText(paymentGateway) && !paymentGateway.equals(transaction.getPaymentGateway())) {
			transaction.setPaymentGateway(paymentGateway);
			changed = true;
		}

		if (StringUtils.hasText(paymentMethod) && !paymentMethod.equals(transaction.getPaymentMethod())) {
			transaction.setPaymentMethod(paymentMethod);
			changed = true;
		}

		if (timestamp != null && timestamp != transaction.getTimestamp()) {
			transaction.setTimestamp(timestamp);
		}

		return changed;
	}

}
