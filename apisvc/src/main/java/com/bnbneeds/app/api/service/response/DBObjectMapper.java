package com.bnbneeds.app.api.service.response;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.model.ApprovedDataObject;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.bnbneeds.app.model.RestLinkRep;
import com.bnbneeds.app.model.endpoints.Endpoint;

public class DBObjectMapper {

	private static final Map<String, Endpoint> ENDPOINT_MAP;

	static {
		ENDPOINT_MAP = new HashMap<String, Endpoint>();

		ENDPOINT_MAP.put("productcategory", Endpoint.PRODUCT_CATEGORY_INFO);
		ENDPOINT_MAP.put("producttype", Endpoint.PRODUCT_TYPE_INFO);
		ENDPOINT_MAP.put("productname", Endpoint.PRODUCT_NAME_INFO);
		ENDPOINT_MAP.put("businesstype", Endpoint.BUSINESS_TYPE_INFO);
		ENDPOINT_MAP.put("useraccount", Endpoint.USER_ACCOUNT_INFO);
		ENDPOINT_MAP.put("vendor", Endpoint.VENDOR_INFO);
		ENDPOINT_MAP.put("buyer", Endpoint.BUYER_INFO);
		ENDPOINT_MAP.put("product", Endpoint.PRODUCT_INFO);
		ENDPOINT_MAP.put("productoffer", Endpoint.PRODUCT_OFFER_INFO);
		ENDPOINT_MAP.put("abusiveentityreport", Endpoint.ABUSE_REPORT_INFO);
	}

	private static Endpoint getEndpoint(DataObject object) {

		String className = object.getClass().getSimpleName();
		Endpoint endpoint = ENDPOINT_MAP.get(className.toLowerCase());

		if (endpoint == null) {
			throw new IllegalArgumentException("Invalid entity type: "
					+ className + ". Cannot return endpoint for this type.");
		}
		return endpoint;
	}

	public static RelatedResourceRep toRelatedRestResponse(String id, URI href) {

		RelatedResourceRep resourceRep = new RelatedResourceRep();
		resourceRep.setId(id);
		RestLinkRep link = new RestLinkRep("self", href);
		resourceRep.setLink(link);
		return resourceRep;

	}

	public static RelatedResourceRep toRelatedRestResponse(DataObject object) {

		Endpoint endpoint = getEndpoint(object);
		URI href = endpoint.get(object.getId());
		RelatedResourceRep relatedResourceRep = toRelatedRestResponse(
				object.getId(), href);

		return relatedResourceRep;
	}

	public static RelatedResourceRep toRelatedRestResponse(Endpoint endpoint,
			DataObject object) {

		URI href = endpoint.get(object.getId());
		RelatedResourceRep relatedResourceRep = toRelatedRestResponse(
				object.getId(), href);

		return relatedResourceRep;
	}

	public static NamedRelatedResourceRep toNamedRelatedRestResponse(
			Endpoint endpoint, DataObject object) {

		RelatedResourceRep relatedRestlink = toRelatedRestResponse(endpoint,
				object);

		NamedRelatedResourceRep namedRelatedResourceRep = new NamedRelatedResourceRep(
				relatedRestlink, object.getName());

		return namedRelatedResourceRep;

	}

	public static NamedRelatedResourceRep toNamedRelatedRestResponse(
			DataObject object) {

		RelatedResourceRep relatedRestlink = toRelatedRestResponse(object);

		NamedRelatedResourceRep namedRelatedResourceRep = new NamedRelatedResourceRep(
				relatedRestlink, object.getName());

		return namedRelatedResourceRep;

	}

	public static List<NamedRelatedResourceRep> toNamedRelatedListResponse(
			List<? extends DataObject> list) {

		List<NamedRelatedResourceRep> responseList = new ArrayList<NamedRelatedResourceRep>();

		if (list != null && !list.isEmpty()) {

			for (Iterator<? extends DataObject> iterator = list.iterator(); iterator
					.hasNext();) {
				DataObject dbObject = iterator.next();
				responseList.add(toNamedRelatedRestResponse(dbObject));
			}
		}
		return responseList;
	}

	public static List<NamedRelatedResourceRep> toNamedRelatedListResponse(
			ResultSetIterator<? extends DataObject> iterator) {

		List<NamedRelatedResourceRep> responseList = new ArrayList<NamedRelatedResourceRep>();

		if (iterator != null) {

			while (iterator.hasNext()) {
				DataObject dbObject = iterator.next();
				responseList.add(toNamedRelatedRestResponse(dbObject));
			}
		}
		return responseList;
	}

	public static List<NamedRelatedResourceRep> toNamedRelatedListResponse(
			Endpoint endpoint, List<? extends DataObject> list) {

		List<NamedRelatedResourceRep> responseList = new ArrayList<NamedRelatedResourceRep>();

		if (list != null && !list.isEmpty()) {

			for (Iterator<? extends DataObject> iterator = list.iterator(); iterator
					.hasNext();) {
				DataObject dbObject = iterator.next();
				responseList
						.add(toNamedRelatedRestResponse(endpoint, dbObject));
			}
		}
		return responseList;
	}

	public static List<NamedRelatedResourceRep> toNamedRelatedListResponse(
			Endpoint endpoint,
			ResultSetIterator<? extends DataObject> resultSetIterator) {

		List<NamedRelatedResourceRep> responseList = new ArrayList<NamedRelatedResourceRep>();

		if (resultSetIterator != null) {

			while (resultSetIterator.hasNext()) {
				DataObject dbObject = resultSetIterator.next();
				responseList
						.add(toNamedRelatedRestResponse(endpoint, dbObject));
			}
		}
		return responseList;
	}

	public static List<NamedRelatedResourceRep> toNamedRelatedListResponse(
			Endpoint endpoint, List<? extends DataObject> list,
			EntityStatus[] havingStatus, EntityStatus[] withoutHavingStatus) {

		Set<EntityStatus> included = new HashSet<EntityStatus>();
		if (havingStatus != null) {
			included.addAll(Arrays.asList(havingStatus));
		}
		Set<EntityStatus> excluded = new HashSet<EntityStatus>();
		if (withoutHavingStatus != null) {
			excluded.addAll(Arrays.asList(withoutHavingStatus));
		}

		List<NamedRelatedResourceRep> responseList = new ArrayList<NamedRelatedResourceRep>();

		if (list != null && !list.isEmpty()) {

			for (Iterator<? extends DataObject> iterator = list.iterator(); iterator
					.hasNext();) {
				DataObject dbObject = iterator.next();
				if (ApprovedDataObject.class.isAssignableFrom(dbObject
						.getClass())) {
					ApprovedDataObject approvedDataObject = (ApprovedDataObject) dbObject;
					if (excluded.contains(approvedDataObject.getEntityStatus())) {
						continue;
					}
					if (included.isEmpty()
							|| included.contains(approvedDataObject
									.getEntityStatus())) {
						responseList.add(toNamedRelatedRestResponse(endpoint,
								dbObject));
					}
				} else {
					responseList.add(toNamedRelatedRestResponse(endpoint,
							dbObject));
				}
			}
		}
		return responseList;
	}

	public static List<NamedRelatedResourceRep> toNamedRelatedListResponse(
			Endpoint endpoint,
			ResultSetIterator<? extends DataObject> resultSetIterator,
			EntityStatus[] havingStatus, EntityStatus[] withoutHavingStatus) {

		Set<EntityStatus> included = new HashSet<EntityStatus>();
		if (havingStatus != null) {
			included.addAll(Arrays.asList(havingStatus));
		}
		Set<EntityStatus> excluded = new HashSet<EntityStatus>();
		if (withoutHavingStatus != null) {
			excluded.addAll(Arrays.asList(withoutHavingStatus));
		}

		List<NamedRelatedResourceRep> responseList = new ArrayList<NamedRelatedResourceRep>();

		if (resultSetIterator != null) {

			while (resultSetIterator.hasNext()) {
				DataObject dbObject = resultSetIterator.next();
				if (ApprovedDataObject.class.isAssignableFrom(dbObject
						.getClass())) {
					ApprovedDataObject approvedDataObject = (ApprovedDataObject) dbObject;
					if (excluded.contains(approvedDataObject.getEntityStatus())) {
						continue;
					}
					if (included.isEmpty()
							|| included.contains(approvedDataObject
									.getEntityStatus())) {
						responseList.add(toNamedRelatedRestResponse(endpoint,
								dbObject));
					}
				} else {
					responseList.add(toNamedRelatedRestResponse(endpoint,
							dbObject));
				}
			}
		}
		return responseList;
	}

	public static List<NamedRelatedResourceRep> toNamedRelatedListResponse(
			List<? extends DataObject> list, EntityStatus[] havingStatus,
			EntityStatus[] withoutHavingStatus) {

		Set<EntityStatus> included = new HashSet<EntityStatus>();
		if (havingStatus != null) {
			included.addAll(Arrays.asList(havingStatus));
		}
		Set<EntityStatus> excluded = new HashSet<EntityStatus>();
		if (withoutHavingStatus != null) {
			excluded.addAll(Arrays.asList(withoutHavingStatus));
		}

		List<NamedRelatedResourceRep> responseList = new ArrayList<NamedRelatedResourceRep>();

		if (list != null && !list.isEmpty()) {

			for (Iterator<? extends DataObject> iterator = list.iterator(); iterator
					.hasNext();) {
				DataObject dbObject = iterator.next();

				if (ApprovedDataObject.class.isAssignableFrom(dbObject
						.getClass())) {
					ApprovedDataObject approvedDataObject = (ApprovedDataObject) dbObject;
					if (excluded.contains(approvedDataObject.getEntityStatus())) {
						continue;
					}
					if (included.isEmpty()
							|| included.contains(approvedDataObject
									.getEntityStatus())) {
						responseList.add(toNamedRelatedRestResponse(dbObject));
					}
				} else {
					responseList.add(toNamedRelatedRestResponse(dbObject));
				}
			}
		}

		return responseList;
	}

	public static List<NamedRelatedResourceRep> toNamedRelatedListResponse(
			ResultSetIterator<? extends DataObject> resultSetIterator,
			EntityStatus[] havingStatus, EntityStatus[] withoutHavingStatus) {

		Set<EntityStatus> included = new HashSet<EntityStatus>();
		if (havingStatus != null) {
			included.addAll(Arrays.asList(havingStatus));
		}
		Set<EntityStatus> excluded = new HashSet<EntityStatus>();
		if (withoutHavingStatus != null) {
			excluded.addAll(Arrays.asList(withoutHavingStatus));
		}

		List<NamedRelatedResourceRep> responseList = new ArrayList<NamedRelatedResourceRep>();

		if (resultSetIterator != null) {

			while (resultSetIterator.hasNext()) {
				DataObject dbObject = resultSetIterator.next();

				if (ApprovedDataObject.class.isAssignableFrom(dbObject
						.getClass())) {
					ApprovedDataObject approvedDataObject = (ApprovedDataObject) dbObject;
					if (excluded.contains(approvedDataObject.getEntityStatus())) {
						continue;
					}
					if (included.isEmpty()
							|| included.contains(approvedDataObject
									.getEntityStatus())) {
						responseList.add(toNamedRelatedRestResponse(dbObject));
					}
				} else {
					responseList.add(toNamedRelatedRestResponse(dbObject));
				}
			}
		}

		return responseList;
	}
	// -------

	/*
	 * public static List<NamedRelatedResourceRep> toNamedRelatedListResponse(
	 * Endpoint endpoint, List<? extends DataObject> list, EntityStatus
	 * ignoreEntitiesWithStatus) {
	 * 
	 * List<NamedRelatedResourceRep> responseList = new
	 * ArrayList<NamedRelatedResourceRep>();
	 * 
	 * if (list != null && !list.isEmpty()) { for (Iterator<? extends
	 * DataObject> iterator = list.iterator(); iterator .hasNext();) {
	 * DataObject dbObject = iterator.next(); if
	 * (dbObject.getClass().isAssignableFrom( ApprovedDataObject.class)) {
	 * ApprovedDataObject approvedDataObject = (ApprovedDataObject) dbObject; if
	 * (ignoreEntitiesWithStatus != approvedDataObject .getEntityStatus()) {
	 * responseList.add(toNamedRelatedRestResponse(dbObject)); } } else {
	 * responseList.add(toNamedRelatedRestResponse(dbObject)); } } } return
	 * responseList; }
	 * 
	 * public static List<NamedRelatedResourceRep> toNamedRelatedListResponse(
	 * List<? extends DataObject> list, EntityStatus ignoreEntitiesWithStatus) {
	 * 
	 * List<NamedRelatedResourceRep> responseList = new
	 * ArrayList<NamedRelatedResourceRep>();
	 * 
	 * if (list != null && !list.isEmpty()) { for (Iterator<? extends
	 * DataObject> iterator = list.iterator(); iterator .hasNext();) {
	 * DataObject dbObject = iterator.next(); if
	 * (ApprovedDataObject.class.isAssignableFrom(dbObject .getClass())) {
	 * ApprovedDataObject approvedDataObject = (ApprovedDataObject) dbObject; if
	 * (approvedDataObject.getEntityStatus() != ignoreEntitiesWithStatus) {
	 * responseList.add(toNamedRelatedRestResponse(dbObject)); } } else {
	 * responseList.add(toNamedRelatedRestResponse(dbObject)); } } }
	 * 
	 * return responseList; }
	 */

}