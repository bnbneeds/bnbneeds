package com.bnbneeds.app.api.service.event;

public enum SystemEventType {

	NEW_VENDOR_REGISTERED("New vendor registered."),
	NEW_BUYER_REGISTERED("New buyer registered."),
	
	NEW_PRODUCT_ADDED("New product added."),
	PRODUCT_UPDATED("Product has been updated."),
	PRODUCT_DELETED("Product has been deleted."),
	
	NEW_PURCHASE_ITEM_ORDERED("New purchase item ordered."),
	PURCHASE_ITEMS_UPDATED("Purchase items updated."),
	PURCHASE_ITEM_STATUS_UPDATED("Updated status of purchase item."),
	PURCHASE_ITEM_DELETED("purchase item deleted."),
	
	NEW_PRODUCT_OFFER_POSTED("New product offer posted."),
	PRODUCT_OFFER_UPDATED("Product offer has been updated."),
	PRODUCT_OFFER_DELETED("Product offer has been deleted."),
	
	NEW_PRODUCT_ENQUIRY_POSTED("New product enquiry posted."),
	PRODUCT_ENQUIRY_UPDATED("Product enquiry has been updated."),
	PRODUCT_ENQUIRY_DELETED("Product enquiry has been deleted."),
	
	BUSINESS_TYPE_CREATED("New Business Type has been created."),
	BUSINESS_TYPES_CREATED("New Business Types have been created."),
	PRODUCT_TYPE_CREATED("New Product Type has been created."),
	PRODUCT_TYPES_CREATED("New Product Types have been created."),
	PRODUCT_CATEGORY_CREATED("New Product Category has been created."),
	PRODUCT_CATEGORIES_CREATED("New Product Categories have been created."),
	PRODUCT_NAME_CREATED("New Product Name has been created."),
	PRODUCT_NAMES_CREATED("New Product Names have been created."),
	
	ENTITY_APPROVED("This entity has been approved."),
	ENTITY_DISAPPROVED("This entity has been disapproved."),
	ENTITY_STATUS_UPDATED("The status of this entity has been changed."),
	ABUSE_REPORT_POSTED("Abuse report posted"),
	
	BUYER_REVIEW_POSTED("A new review has been posted about buyer."),
	VENDOR_REVIEW_POSTED("A new review has been posted about vendor."),
	PRODUCT_REVIEW_POSTED("A new review has been posted about product."),
	
	BIDDING_SUBSCRIPTION_PLAN_CREATED("A new bidding subscription plan has been created."),
	BIDDING_SUBSCRIPTION_PLAN_UPDATED("Bidding subscription plan has been updated."),
	
	TENDER_CREATED("New Tender has been created."),
	TENDER_OPEN("Tender is open for bidding."),
	FIRST_TENDER_BID_POSTED("First bid for the tender is posted."),
	TENDER_UPDATED("Tender has been updated by the Buyer."),
	BID_PRODUCTS_CREATED("New Bid Products have been added to Tender."),
	BID_PRODUCT_DELETED("bid product deleted."),
	TENDER_CLOSED("Tender has been closed."),
	
	BUYER_BIDDING_SUBSCRIPTION_EXPIRED("Buyer bidding subscription has expired."),
	VENDOR_BIDDING_SUBSCRIPTION_EXPIRED("Vendor bidding subscription has expired."),
	VENDOR_DELETED("Vendor has been deleted."),
	BUYER_DELETED("Buyer has been deleted."),
	;

	private String message;

	private SystemEventType(String message) {
		this.message = message;
	}

	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
