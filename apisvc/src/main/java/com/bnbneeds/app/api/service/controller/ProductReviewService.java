package com.bnbneeds.app.api.service.controller;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.APIException;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.ReviewResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.DbClientUtility;
import com.bnbneeds.app.api.service.util.ReviewUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.OrderBy;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.ProductAverageRating;
import com.bnbneeds.app.db.client.model.ProductReview;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.review.AverageRatingResponse;
import com.bnbneeds.app.model.review.ProductReviewInfoListResponse;
import com.bnbneeds.app.model.review.ProductReviewInfoResponse;
import com.bnbneeds.app.model.review.ReviewParam;

/**
 * 
 * @author Amit Herlekar
 *
 */

@RestController
@RequestMapping(value = "/products/{productId}/reviews")
public class ProductReviewService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(ProductReviewService.class);

	private void updateAverageRating(Product product) {
		ProductAverageRating averageRating = dbClient.queryObjectByFields(ProductAverageRating.class,
				new QueryFields("product", product));

		boolean created = false;
		if (averageRating == null) {
			averageRating = new ProductAverageRating();
			averageRating.setId(UUIDUtil.createId(ProductAverageRating.class));
			averageRating.setProduct(product);
			created = true;
		}
		QueryFields fields = new QueryFields("product", product);
		fields.add("entityStatus", EntityStatus.APPROVED.name());
		List<ProductReview> reviews = dbClient.queryListBySortOrder(ProductReview.class, fields,
				ReviewUtility.MAX_NUMBER_OF_REVIEWS_TO_CALCULATE_AVERAGE_RATING, OrderBy.DESC, "timestamp");
		ReviewUtility.calculateAverageRating(reviews, averageRating);
		DBOperationList opList = new DBOperationList();
		if (created) {
			opList.addOperation(OperationType.INSERT, averageRating);
		} else {
			opList.addOperation(OperationType.UPDATE, averageRating);
		}
		dbClient.transact(opList);
	}

	@RequestMapping(method = RequestMethod.POST, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and hasDealership()")
	public ResponseEntity<CreateResourceResponse> createBuyerReview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId, @RequestBody ReviewParam reviewParam) throws APIException {

		logger.info("Request received to post a review of product with id: {}", productId);

		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");
		ArgValidator.checkFieldNotNull(reviewParam, "review");

		Product product = dbClient.queryObject(Product.class, productId);
		ArgValidator.checkEntityBlacklisted(product, productId, true);

		Buyer buyer = dbClient.queryObject(Buyer.class, user.getRelationshipId());

		ArgValidator.checkEntityBlacklisted(buyer, buyer.getId(), true);

		QueryFields fields = new QueryFields("product", product);
		fields.add("reviewedBy", buyer);

		ProductReview productReview = dbClient.queryObjectByFields(ProductReview.class, fields);

		if (productReview != null) {
			throw BadRequestException.reviewAlreadyExists(productId);
		}

		ProductReview review = ReviewUtility.validateCreateReview(reviewParam, Product.class);
		String uid = UUIDUtil.createId(ProductReview.class);
		review.setId(uid);
		review.setProduct(product);
		review.setReviewedBy(buyer);
		review.setTimestamp(System.currentTimeMillis());

		dbClient.createObject(review);

		updateAverageRating(product);

		URI reviewLink = Endpoint.PRODUCT_REVIEW_INFO.get(productId, uid);

		Vendor productVendor = product.getVendor();
		String vendorId = productVendor.getId();
		String productName = product.getProductName().getName();

		recordEvent(SystemEventType.PRODUCT_REVIEW_POSTED, RecipientType.VENDOR, vendorId, uid, productName, reviewLink,
				buyer.getName());

		logger.info("Product review created successfully.", review);
		return TaskResponseUtility.createResourceResponse(uid, reviewLink, "Product review posted successfully.");
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{reviewId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.BUYER + "','" + Role.ADMIN + "') and hasDealership()")
	public ResponseEntity<TaskResponse> updateBuyerReview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId, @PathVariable("reviewId") String reviewId,
			@RequestBody ReviewParam reviewParam) throws APIException {

		logger.info("Request received to update product review with id: {}", reviewId);

		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");
		ArgValidator.checkFieldUriType(reviewId, ProductReview.class, "review_id");

		ArgValidator.checkFieldNotNull(reviewParam, "review");

		ProductReview productReview = dbClient.queryObject(ProductReview.class, reviewId);
		if (user.isBuyer()) {
			ArgValidator.checkEntityBlacklisted(productReview, reviewId, true);
			if (!productReview.getReviewedBy().getId().equals(user.getRelationshipId())) {
				throw ForbiddenException.unauthorizedAccess();
			} else {
				Buyer buyer = dbClient.queryObject(Buyer.class, user.getRelationshipId());
				ArgValidator.checkEntityBlacklisted(buyer, buyer.getId(), true);
				Product product = dbClient.queryObject(Product.class, productId);

				ArgValidator.checkEntityBlacklisted(product, productId, true);
			}
		}

		if (!productReview.getProduct().getId().equals(productId)) {
			throw ForbiddenException.unauthorizedAccess();
		}

		ReviewUtility.validateUpdateReview(reviewParam, productReview);
		productReview.setTimestamp(System.currentTimeMillis());

		dbClient.updateObject(productReview);

		updateAverageRating(productReview.getProduct());

		logger.info("Product review updated successfully.", productReview);
		return TaskResponseUtility.createTaskSuccessResponse("Product review updated successfully.");
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{reviewId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.BUYER + "', '" + Role.ADMIN + "') and hasDealership()")
	public ResponseEntity<TaskResponse> deleteProductReview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId, @PathVariable("reviewId") String reviewId)
			throws APIException {

		logger.info("Request received to delete product review with id: {}", reviewId);

		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");
		ArgValidator.checkFieldUriType(reviewId, ProductReview.class, "review_id");

		ProductReview productReview = dbClient.queryObject(ProductReview.class, reviewId);
		if (user.isAdmin()) {
			ArgValidator.checkEntityApproved(productReview, reviewId, true);
		} else {
			// Buyer
			ArgValidator.checkEntityBlacklisted(productReview, reviewId, true);
			if (!productReview.getReviewedBy().getId().equals(user.getRelationshipId())) {
				throw ForbiddenException.unauthorizedAccess();
			} else {
				Product product = dbClient.queryObject(Product.class, productId);
				ArgValidator.checkEntityBlacklisted(product, productId, true);

				ArgValidator.checkEntityBlacklisted(product, productId, true);
				Vendor productVendor = product.getVendor();
				ArgValidator.checkEntityBlacklisted(productVendor, productVendor.getId(), true);
				Buyer buyer = dbClient.queryObject(Buyer.class, user.getRelationshipId());
				ArgValidator.checkEntityBlacklisted(buyer, buyer.getId(), true);
			}
		}

		if (!productReview.getProduct().getId().equals(productId)) {
			throw ForbiddenException.unauthorizedAccess();
		}

		dbClient.markAsDeleted(productReview);

		updateAverageRating(productReview.getProduct());

		logger.info("Product review deleted successfully.");
		return TaskResponseUtility.createTaskSuccessResponse("Product review deleted successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "','" + Role.BUYER + "','" + Role.ADMIN + "') and  hasDealership()")
	public ProductReviewInfoListResponse getReviews(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {
		logger.info("Request received to get product reviews by fetchSize: {}, offset: {}", fetchSize, offset);

		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");
		Product product = dbClient.queryObject(Product.class, productId);
		Vendor productVendor = product.getVendor();
		if (user.isDealer()) {
			ArgValidator.checkEntityBlacklisted(product, productId, true);
			ArgValidator.checkEntityBlacklisted(productVendor, productVendor.getId(), false);
		}

		if (user.isVendor() && !productVendor.getId().equals(user.getRelationshipId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		final String[] QUERY_FILTER_KEYS = { "reviewedBy" };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields("product", product);

		for (String key : keyValueMap.keySet()) {

			String value = keyValueMap.get(key);

			switch (key) {
			case "reviewedBy":
				ArgValidator.checkFieldUriType(value, Buyer.class, key);
				Buyer buyer = dbClient.queryObject(Buyer.class, value);
				if (user.isDealer()) {
					ArgValidator.checkEntityBlacklisted(buyer, value, true);
				}
				fields.add("reviewedBy", buyer);
				break;
			default:
				break;
			}
		}

		ResultSetIterator<ProductReview> resultSetIterator = null;

		ProductReviewInfoListResponse response = new ProductReviewInfoListResponse();
		do {

			resultSetIterator = DbClientUtility.getResultSetIteratorOrderByDesc(dbClient, ProductReview.class, fields,
					offset, fetchSize, "timestamp");

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					ProductReview item = resultSetIterator.next();

					if (item == null) {
						continue;
					}

					Buyer buyer = item.getReviewedBy();

					if (user.isDealer()) {
						if (!item.isApproved()) {
							continue;
						}

						if (buyer.isBlacklisted()) {
							continue;
						}
					}

					ProductReviewInfoResponse infoResponse = ReviewResponseMapper.map(item, DATE_FORMAT);
					response.addReviewInfoResponse(infoResponse);

				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}
		} while (response.size() < fetchSize);
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{reviewId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "','" + Role.BUYER + "','" + Role.ADMIN + "') and  hasDealership()")
	public ProductReviewInfoListResponse getReview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId, @PathVariable("reviewId") String reviewId) {

		logger.info("Request received to get vendor review for reviewId: {}", reviewId);

		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");
		ArgValidator.checkFieldUriType(reviewId, ProductReview.class, "review_id");

		ProductReview productReview = dbClient.queryObject(ProductReview.class, reviewId);
		ArgValidator.checkEntityNotNull(productReview, reviewId, true);

		Product product = dbClient.queryObject(Product.class, productId);

		Vendor productVendor = product.getVendor();

		if (user.isDealer()) {
			ArgValidator.checkEntityBlacklisted(product, productId, true);
			ArgValidator.checkEntityBlacklisted(productReview, reviewId, true);
			ArgValidator.checkEntityBlacklisted(productVendor, productVendor.getId(), true);
		}

		if (user.isVendor() && !productVendor.getId().equals(user.getRelationshipId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		ProductReviewInfoListResponse response = new ProductReviewInfoListResponse();
		ProductReviewInfoResponse infoResponse = ReviewResponseMapper.map(productReview, DATE_FORMAT);
		response.addReviewInfoResponse(infoResponse);

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/average-rating", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "','" + Role.BUYER + "') and  hasDealership() or (hasAnyRole('"
			+ Role.ADMIN + "'))")
	public AverageRatingResponse getAverageRating(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("productId") String productId) {

		logger.info("Request received to get average rating of product with id: {}", productId);

		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");
		Product product = dbClient.queryObject(Product.class, productId);
		ArgValidator.checkEntityBlacklisted(product, productId, true);

		Vendor productVendor = product.getVendor();
		if (user.isVendor() && !productVendor.getId().equals(user.getRelationshipId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		AverageRatingResponse response = new AverageRatingResponse();
		ProductAverageRating averageRating = dbClient.queryObjectByFields(ProductAverageRating.class,
				new QueryFields("product", product));
		if (averageRating != null) {
			ReviewResponseMapper.map(averageRating, response);
		}

		return response;
	}
}
