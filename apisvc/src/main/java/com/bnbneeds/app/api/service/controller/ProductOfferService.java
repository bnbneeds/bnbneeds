package com.bnbneeds.app.api.service.controller;

import static com.bnbneeds.app.api.service.response.DBObjectMapper.toNamedRelatedRestResponse;

import java.net.URI;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.response.ProductResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.ProductUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.ProductOffer;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.CreateProductOfferParam;
import com.bnbneeds.app.model.product.ProductOfferInfoResponse;
import com.bnbneeds.app.model.product.ProductOfferListResponse;
import com.bnbneeds.app.model.product.UpdateProductOfferParam;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/product-offers")
public class ProductOfferService extends ResourceService {

	private static final Logger logger = LoggerFactory
			.getLogger(ProductOfferService.class);

	@RequestMapping(method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasRole('" + Role.VENDOR + "') and hasDealership()")
	public ResponseEntity<CreateResourceResponse> createOffer(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody CreateProductOfferParam param) {

		logger.info(
				"Request received to create product offer with payload: {}",
				param);

		ArgValidator.checkFieldNotNull(param, "create_product_offer");
		ArgValidator.checkFieldUriType(param.getProductId(), Product.class,
				"product_id");
		ArgValidator.checkFieldNotEmpty(param.getOfferTitle(), "offer_title");

		String startDate = param.getStartDate();
		ArgValidator.checkFieldNotEmpty(startDate, "start_date");
		String endDate = param.getEndDate();
		ArgValidator.checkFieldNotEmpty(endDate, "end_date");

		Product product = dbClient.queryObject(Product.class,
				param.getProductId());

		ArgValidator.checkEntityDisapprovedOrBlacklisted(product,
				param.getProductId(), false);
		ProductUtility.checkProductOwnership(user, product);

		if (!StringUtils.isDateFormatValid(startDate, DATE_FORMAT.toPattern())) {
			throw BadRequestException.parameterInvalidExpectedValue(
					"start_date",
					"Value must be of the format: " + DATE_FORMAT.toPattern());
		}

		if (!StringUtils.isDateFormatValid(endDate, DATE_FORMAT.toPattern())) {
			throw BadRequestException.parameterInvalidExpectedValue("end_date",
					"Value must be of the format: " + DATE_FORMAT.toPattern());
		} else {
			Date today = new Date();
			Date endDateInput = null;
			try {
				endDateInput = DATE_FORMAT.parse(endDate);
			} catch (ParseException e) {
				// Won't reach this block. Date is validated in if block.
			}
			if (endDateInput != null) {
				if (!endDateInput.after(today)) {
					throw BadRequestException.parameterInvalidExpectedValue(
							"end_date", "End date must be after today");
				}
			}
		}

		QueryFields fields = new QueryFields("product", product);
		ProductOffer offer = dbClient.queryObjectByFields(ProductOffer.class,
				fields);

		if (offer != null) {
			logger.error("An offer of the product: {} already exists.",
					param.getProductId());
			throw BadRequestException.create("An offer with product["
					+ param.getProductId() + "] already exists.");
		}

		offer = new ProductOffer();
		String uid = UUIDUtil.createId(ProductOffer.class);
		offer.setId(uid);
		offer.setProduct(product);
		offer.setName(param.getOfferTitle());
		offer.setStartDate(param.getStartDate());
		offer.setEndDate(param.getEndDate());
		offer.setDescription(param.getDescription());
		offer.setEntityStatus(EntityStatus.DISAPPROVED);

		dbClient.createObject(offer);
		logger.info("Created record of product offer in DB successfully");

		URI uri = Endpoint.PRODUCT_OFFER_INFO.get(uid);

		recordEvent(SystemEventType.NEW_PRODUCT_OFFER_POSTED,
				RecipientType.ALL_BUYERS, uid, product.getProductName()
						.getName(), uri, null);

		return TaskResponseUtility.createResourceResponse(uid, uri,
				"Product offer created successfully");
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{offerId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasRole('" + Role.VENDOR + "')")
	public ResponseEntity<TaskResponse> updateOffer(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("offerId") String offerId,
			@RequestBody UpdateProductOfferParam param) {

		logger.info("Request received to update offer: {} with payload {}",
				offerId, param);

		ArgValidator.checkFieldUriType(offerId, ProductOffer.class, "offer_id");
		ArgValidator.checkFieldNotNull(param, "update_product_offer");

		ProductOffer productOffer = dbClient.queryObject(ProductOffer.class,
				offerId);
		ArgValidator.checkEntityBlacklisted(productOffer, offerId, true);

		String productId = productOffer.getProduct().getId();
		Product product = dbClient.queryObject(Product.class, productId);

		ArgValidator.checkEntityDisapprovedOrBlacklisted(product, productId,
				false);

		ProductUtility.checkProductOwnership(user, product);

		boolean changed = false;
		String offerTitle = param.getOfferTitle();
		String offerDescription = param.getDescription();
		String startDate = param.getStartDate();
		String endDate = param.getEndDate();

		if (!StringUtils.isEmpty(offerTitle)
				|| !StringUtils.isEmpty(offerDescription)
				|| !StringUtils.isEmpty(startDate)
				|| !StringUtils.isEmpty(endDate)) {
			changed = true;
		}
		if (!StringUtils.isEmpty(startDate)) {
			if (!StringUtils.isDateFormatValid(startDate,
					DATE_FORMAT.toPattern())) {
				throw BadRequestException.parameterInvalidExpectedValue(
						"start_date", "Value must be of the format: "
								+ DATE_FORMAT.toPattern());
			}
		}
		if (!StringUtils.isEmpty(endDate)) {
			if (!StringUtils
					.isDateFormatValid(endDate, DATE_FORMAT.toPattern())) {
				throw BadRequestException.parameterInvalidExpectedValue(
						"end_date", "Value must be of the format: "
								+ DATE_FORMAT.toPattern());
			} else {
				Date today = new Date();
				Date endDateInput = null;
				try {
					endDateInput = DATE_FORMAT.parse(endDate);
				} catch (ParseException e) {
					// Won't reach this block. Date is validated in if block.
				}
				if (endDateInput != null) {
					if (!endDateInput.after(today)) {
						throw BadRequestException
								.parameterInvalidExpectedValue("end_date",
										"End date must be after today");
					}
				}
			}
		}

		if (!changed) {
			logger.error("Product offer: {} - There is nothing to update.",
					offerId);
			throw BadRequestException.create("There is nothing to update");
		}

		productOffer.setName(param.getOfferTitle());
		productOffer.setDescription(param.getDescription());
		productOffer.setStartDate(param.getStartDate());
		productOffer.setEndDate(param.getEndDate());

		dbClient.updateObject(productOffer);
		logger.info("Updated record of product offer in DB successfully");

		String uid = productOffer.getId();

		recordEvent(SystemEventType.PRODUCT_OFFER_UPDATED,
				RecipientType.ALL_BUYERS, uid, productOffer.getProduct()
						.getProductName().getName(),
				Endpoint.PRODUCT_OFFER_INFO.get(uid), null);

		return TaskResponseUtility
				.createTaskSuccessResponse("Product offer updated successfully.");

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{offerId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasRole('" + Role.VENDOR + "')")
	public ResponseEntity<TaskResponse> deleteOffer(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("offerId") String offerId) {

		logger.info("Request received to delete offer: {}", offerId);

		ArgValidator.checkFieldUriType(offerId, ProductOffer.class, "offer_id");
		ProductOffer productOffer = dbClient.queryObject(ProductOffer.class,
				offerId);
		ArgValidator.checkEntityBlacklisted(productOffer, offerId, true);

		String productId = productOffer.getProduct().getId();
		Product product = dbClient.queryObject(Product.class, productId);

		ArgValidator.checkEntityDisapprovedOrBlacklisted(product, productId,
				false);
		ProductUtility.checkProductOwnership(user, product);

		dbClient.markAsDeleted(productOffer);
		logger.info("Deleted record of product offer from DB successfully.");

		recordEvent(SystemEventType.PRODUCT_OFFER_DELETED,
				RecipientType.ALL_BUYERS, productOffer.getId(), productOffer
						.getProduct().getProductName().getName(), null, null);

		return TaskResponseUtility
				.createTaskSuccessResponse("Product offer deleted successfully.");

	}

	@RequestMapping(method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER
			+ "')")
	public ProductOfferListResponse getOfferList(
			@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info(
				"Request received to list offers with filter: {}, offset: {} and fetchSize: {}",
				filter, offset, fetchSize);

		final String[] QUERY_FILTER_KEYS = { "productId", "status",
				"startDate", "endDate" };

		ProductOfferListResponse response = new ProductOfferListResponse();

		QueryFields fields = new QueryFields();

		/*
		 * if (isVendor(user)) { Vendor vendor =
		 * dbClient.queryObject(Vendor.class, user.getRelationshipId()); if
		 * (vendor != null) { List<Product> vendorProductList =
		 * dbClient.queryList( Product.class, new QueryFields("vendor",
		 * vendor)); if (vendorProductList != null &&
		 * !vendorProductList.isEmpty()) { fields.add("product in",
		 * vendorProductList); } } }
		 */

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);

			switch (key) {

			case "productId":
				ArgValidator.checkFieldUriType(value, Product.class,
						"productId");
				Product product = dbClient.queryObject(Product.class, value);
				if (product != null) {
					fields.add("product", product);
				}
				break;
			case "status":
				ArgValidator.checkFieldValue(!EntityStatus.BLACKLISTED.name()
						.equals(value.toUpperCase()), key);
				fields.add("entityStatus", value.toUpperCase());
				break;
			case "startDate":
				fields.add("startDate", value);
				break;
			case "endDate":
				fields.add("endDate", value);
				break;
			default:
				break;
			}
		}

		ResultSetIterator<ProductOffer> resultSetIterator = null;
		do {
			resultSetIterator = getResultSetIterator(ProductOffer.class,
					fields, offset, fetchSize);
			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					ProductOffer productOffer = resultSetIterator.next();
					if (productOffer.getEntityStatus() == EntityStatus.BLACKLISTED
							|| productOffer.getProduct().getEntityStatus() == EntityStatus.BLACKLISTED
							|| EntityStatus.BLACKLISTED == productOffer
									.getProduct().getVendor().getEntityStatus()) {
						logger.info(
								"Skipping Product Offer: {} as its product or its vendor is BLACKLISTED.",
								productOffer);
						continue;
					} else if (user.isVendor()) {
						String vendorId = user.getRelationshipId();

						if (!vendorId.equals(productOffer.getProduct()
								.getVendor().getId())) {
							logger.info(
									"User is VENDOR. So filtering out this product offer [{}] as it does not belong to the vendor.",
									productOffer.getId());
							continue;
						}
						response.addNamedResourceResponse(toNamedRelatedRestResponse(productOffer));
					} else if (user.isBuyer() && !productOffer.isApproved()) {
						continue;
					} else {
						response.addNamedResourceResponse(toNamedRelatedRestResponse(productOffer));
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null
						&& offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}
		} while (response.size() < fetchSize);

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{offerId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER
			+ "')")
	public ProductOfferInfoResponse getOffer(
			@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("offerId") String offerId) {

		logger.info("Request received to get details of product offer: {}",
				offerId);

		ArgValidator.checkFieldUriType(offerId, ProductOffer.class, "offer_id");
		ProductOffer productOffer = dbClient.queryObject(ProductOffer.class,
				offerId);
		ArgValidator.checkEntityBlacklisted(productOffer, offerId, true);
		String productId = productOffer.getProduct().getId();
		Product product = dbClient.queryObject(Product.class, productId);
		ArgValidator.checkEntityBlacklisted(product, productId, false);
		ArgValidator.checkEntityBlacklisted(product.getVendor(), product
				.getVendor().getId(), false);

		if (user.isBuyer()) {
			ArgValidator.checkEntityDisapproved(productOffer, offerId, true);
		}

		ProductUtility.checkProductOwnership(user, product);

		ProductOfferInfoResponse response = new ProductOfferInfoResponse();
		ProductResponseMapper.mapProductOffer(productOffer, response);

		logger.info("Returning product offer as response: {}", response);

		return response;
	}

}
