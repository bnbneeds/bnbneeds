package com.bnbneeds.app.api.service.controller;

import static com.bnbneeds.app.api.service.response.DBObjectMapper.toNamedRelatedListResponse;
import static com.bnbneeds.app.api.service.response.DBObjectMapper.toNamedRelatedRestResponse;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.APIException;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.response.DealerResponseMapper;
import com.bnbneeds.app.api.service.response.ProductResponseMapper;
import com.bnbneeds.app.api.service.response.PurchaseItemResponseMapper;
import com.bnbneeds.app.api.service.response.ReviewResponseMapper;
import com.bnbneeds.app.api.service.response.SubscriptionResponseMapper;
import com.bnbneeds.app.api.service.response.UserAccountResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.DealerUtility;
import com.bnbneeds.app.api.service.util.EntityAdminUtility;
import com.bnbneeds.app.api.service.util.ProductUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.DbClientUtils;
import com.bnbneeds.app.db.client.ResultSet;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.ApprovedDataObject;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscription;
import com.bnbneeds.app.db.client.model.BuyerReview;
import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.db.client.model.Enquiry;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.ProductOffer;
import com.bnbneeds.app.db.client.model.ProductReview;
import com.bnbneeds.app.db.client.model.PurchaseItem;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscription;
import com.bnbneeds.app.db.client.model.VendorReview;
import com.bnbneeds.app.model.DataObjectRestResponse;
import com.bnbneeds.app.model.EntityApprovalParam;
import com.bnbneeds.app.model.EntityListResponse;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.dealer.BuyerInfoResponse;
import com.bnbneeds.app.model.dealer.BuyerListResponse;
import com.bnbneeds.app.model.dealer.EnquiryInfoListResponse;
import com.bnbneeds.app.model.dealer.EnquiryInfoResponse;
import com.bnbneeds.app.model.dealer.VendorInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.ProductInfoResponse;
import com.bnbneeds.app.model.product.ProductOfferInfoResponse;
import com.bnbneeds.app.model.review.BuyerReviewInfoResponse;
import com.bnbneeds.app.model.review.NullReviewInfoResponse;
import com.bnbneeds.app.model.review.ProductReviewInfoResponse;
import com.bnbneeds.app.model.review.ReviewInfoResponse;
import com.bnbneeds.app.model.review.VendorReviewInfoResponse;
import com.bnbneeds.app.util.DateUtils;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/entities")
@PreAuthorize("hasRole('" + Role.ADMIN + "')")
public class EntityAdminService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(EntityAdminService.class);

	private static final String REQ_PARAM_ENQUIRY_DATE = "enquiryDate";
	private static final String REQ_PARAM_START_ENQUIRY_DATE = "startEnquiryDate";
	private static final String REQ_PARAM_END_ENQUIRY_DATE = "endEnquiryDate";

	private EntityListResponse getListResponse(Class<? extends DataObject> entityType, QueryFields fields,
			int fetchSize, String nextOffset) {

		ResultSet<? extends DataObject> resultSet = null;

		if (!fields.isEmpty()) {
			resultSet = dbClient.queryResultSet(entityType, fields, nextOffset, fetchSize);
		} else {
			resultSet = dbClient.queryResultSet(entityType, nextOffset, fetchSize);
		}
		EntityListResponse listResponse = new EntityListResponse();
		listResponse.setNextOffset(resultSet.getOffset());
		String type = entityType.getSimpleName();
		listResponse.setEntityType(type);
		switch (type.toLowerCase()) {
		case "vendor":
			break;
		case "buyer":
			break;
		case "productoffer":
			break;

		/*
		 * case "businesstype": listResponse = new BusinessTypeListResponse(); break;
		 * case "producttype": listResponse = new ProductTypeListResponse(); break; case
		 * "productcategory": listResponse = new ProductCategoryListResponse(); break;
		 * case "productname": listResponse = new ProductNameListResponse(); break;
		 */

		/*
		 * case "useraccount": listResponse = new UserAccountListResponse(); break;
		 */
		case "product":
			for (Iterator<? extends DataObject> iterator = resultSet.getList().iterator(); iterator.hasNext();) {
				Product product = (Product) iterator.next();
				product.setName(product.getProductName().getName());
			}
			break;
		case "enquiry":
			for (Iterator<? extends DataObject> iterator = resultSet.getList().iterator(); iterator.hasNext();) {
				Enquiry enquiry = (Enquiry) iterator.next();
				enquiry.setName(enquiry.getProduct().getProductName().getName());
			}
			break;
		case "purchaseitem":
			for (Iterator<? extends DataObject> iterator = resultSet.getList().iterator(); iterator.hasNext();) {
				PurchaseItem purchaseItem = (PurchaseItem) iterator.next();
				purchaseItem.setName(purchaseItem.getProductName().getName());
			}
			break;
		default:
			throw new IllegalArgumentException("Invalid entity type: [" + type + "]");
		}

		List<NamedRelatedResourceRep> responseList = null;

		responseList = toNamedRelatedListResponse(Endpoint.ADMIN_ENTITY_BY_ID, resultSet.getList());

		listResponse.setNamedRelatedResourceList(responseList);
		return listResponse;
	}

	private DataObjectRestResponse getDataObjectRestResponse(String id) {
		ArgValidator.checkUri(id);
		String className = UUIDUtil.getTypeName(id);
		DataObject dataObject = dbClient.queryObject(className, id);
		ArgValidator.checkEntityNotNull(dataObject, id, true);
		DataObjectRestResponse response = null;
		switch (className.toLowerCase()) {
		case "vendor":
			Vendor vendor = (Vendor) dataObject;
			response = new VendorInfoResponse();
			DealerResponseMapper.mapVendor(vendor, (VendorInfoResponse) response);
			break;
		case "buyer":
			Buyer buyer = (Buyer) dataObject;
			response = new BuyerInfoResponse();
			DealerResponseMapper.mapBuyer(buyer, (BuyerInfoResponse) response);
			break;
		/*
		 * case "businesstype": DescribedDataObject businessType = (DescribedDataObject)
		 * dataObject; response = new BusinessTypeInfoResponse();
		 * DataObjectResponseMapper.mapDescribedDataObject(businessType,
		 * (DescribedDataObjectRestResponse) response); break; case "producttype":
		 * DescribedDataObject productType = (DescribedDataObject) dataObject; response
		 * = new ProductTypeInfoResponse();
		 * DataObjectResponseMapper.mapDescribedDataObject(productType,
		 * (DescribedDataObjectRestResponse) response); break; case "productcategory":
		 * DescribedDataObject productCategory = (DescribedDataObject) dataObject;
		 * response = new ProductCategoryInfoResponse();
		 * DataObjectResponseMapper.mapDescribedDataObject(productCategory,
		 * (DescribedDataObjectRestResponse) response); break; case "productname":
		 * DescribedDataObject productName = (DescribedDataObject) dataObject; response
		 * = new ProductNameInfoResponse();
		 * DataObjectResponseMapper.mapDescribedDataObject(productName,
		 * (DescribedDataObjectRestResponse) response); break;
		 */
		case "productoffer":
			ProductOffer productOffer = (ProductOffer) dataObject;
			response = new ProductOfferInfoResponse();
			ProductResponseMapper.mapProductOffer(productOffer, (ProductOfferInfoResponse) response);
			break;
		case "product":
			Product product = (Product) dataObject;
			response = new ProductInfoResponse();
			ProductResponseMapper.mapProduct(product, (ProductInfoResponse) response);
			break;
		case "enquiry":
			Enquiry productEnquiry = (Enquiry) dataObject;
			response = new EnquiryInfoResponse();
			ProductResponseMapper.mapProductEnquiry(productEnquiry, DATE_FORMAT, (EnquiryInfoResponse) response);
			break;
		case "purchaseitem":
			PurchaseItem purchaseItem = (PurchaseItem) dataObject;
			response = PurchaseItemResponseMapper.map(purchaseItem, DATE_FORMAT);
			break;
		case "buyerbiddingsubscription":
			BuyerBiddingSubscription biddingSubscription = (BuyerBiddingSubscription) dataObject;
			response = SubscriptionResponseMapper.map(biddingSubscription, TIMESTAMP_FORMAT);
			break;
		case "vendorbiddingsubscription":
			VendorBiddingSubscription subscription = (VendorBiddingSubscription) dataObject;
			response = SubscriptionResponseMapper.map(subscription, TIMESTAMP_FORMAT);
			break;
		default:
			throw new IllegalArgumentException("Invalid entity type: [" + className + "]");
		}
		return response;
	}

	private String getEntityName(ApprovedDataObject dataObject) {

		String entityType = UUIDUtil.getTypeName(dataObject.getId());

		String entityName = dataObject.getName();

		switch (entityType.toLowerCase()) {
		case "product":
			entityName = ((Product) dataObject).getProductName().getName();
			break;
		default:
			break;
		}
		return entityName;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{entityId}/approvals", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> updateEntityStatus(@PathVariable("entityId") String entityId,
			@RequestBody EntityApprovalParam approvalParam, @AuthenticationPrincipal BNBNeedsUser user) {

		logger.info("API request received to set approval status of entity with id: {}: {}", entityId, approvalParam);

		ArgValidator.checkUri(entityId);
		ArgValidator.checkFieldNotNull(approvalParam, "entity_approval");
		String newStatus = approvalParam.getStatus().name();
		ArgValidator.checkFieldValueFromEnum(newStatus, "status", ApprovedDataObject.EntityStatus.class);

		String entityType = UUIDUtil.getTypeName(entityId);
		DataObject dataObject = dbClient.queryObject(entityType, entityId);

		ArgValidator.checkEntityNotNull(dataObject, entityId, true);

		if (!ApprovedDataObject.class.isAssignableFrom(dataObject.getClass())) {
			throw BadRequestException.parameterInvalid("entity_id", "This entity is not valid for approval.");
		}
		ApprovedDataObject approvedDataObject = (ApprovedDataObject) dataObject;
		EntityStatus newEntityStatus = EntityStatus.valueOf(newStatus);
		if (approvedDataObject.getEntityStatus() == newEntityStatus) {
			throw BadRequestException.create("Entity status is already " + newStatus);
		}

		// Validate conditions specific for entity type, if any
		EntityAdminUtility.validateEntityApprovalForEntities(approvedDataObject, newEntityStatus, dbClient);

		approvedDataObject.setEntityStatus(newEntityStatus);
		approvedDataObject.addRemark(user.getUsername(), user.getRole(), approvalParam.getRemarks());
		dbClient.updateObject(approvedDataObject);

		logger.debug("Entity updated in DB: {}", approvedDataObject);
		RecipientType recipientType = null;
		SystemEventType systemEventType = null;
		switch (newEntityStatus) {
		case APPROVED:
			systemEventType = SystemEventType.ENTITY_APPROVED;
			switch (approvedDataObject.getClass().getSimpleName().toLowerCase()) {
			case "vendor":
				recipientType = RecipientType.BUYERS_AND_ADMIN;
				break;
			case "buyer":
				recipientType = RecipientType.VENDORS_AND_ADMIN;
				break;
			case "vendorbiddingsubscriptionplan":
			case "buyerbiddingsubscriptionplan":
				recipientType = RecipientType.ADMIN;
				break;
			default:
				recipientType = RecipientType.EVERYONE;
				break;
			}
			break;
		case DISAPPROVED:
			systemEventType = SystemEventType.ENTITY_DISAPPROVED;
			recipientType = RecipientType.ADMIN;
			break;
		default:
			systemEventType = SystemEventType.ENTITY_STATUS_UPDATED;
			recipientType = RecipientType.ADMIN;
			break;
		}

		String entityName = getEntityName(approvedDataObject);
		recordEvent(systemEventType, recipientType, approvedDataObject.getId(), entityName, null, Role.ADMIN);

		return TaskResponseUtility.createTaskSuccessResponse("Entity status updated successfully.");

	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public EntityListResponse getEntityList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_FILTER, required = true) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get vendors by filter: {}, fetchSize: {}, offset: {}", filter, fetchSize,
				offset);

		final String[] QUERY_FILTER_KEYS = { "type", "status" };
		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields();

		String value = null;
		String type = null;
		for (String key : keyValueMap.keySet()) {
			value = keyValueMap.get(key);
			switch (key) {
			case "type":
				type = value;
				break;
			case "status":
				String status = value.toUpperCase();
				ArgValidator.checkFieldValueFromEnum(status, "status", EntityStatus.class);
				fields.add("entityStatus", status);
				break;
			default:
				break;
			}
		}

		ArgValidator.checkFieldNotEmpty(type, "type");

		EntityListResponse resp = null;
		try {
			Class<? extends DataObject> entityType = DbClientUtils.getClassByName(type);
			resp = getListResponse(entityType, fields, fetchSize, offset);
		} catch (ClassNotFoundException | IllegalArgumentException e) {
			throw BadRequestException.parameterInvalid("type", "Invalid entity type: [" + type + "]");
		}
		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public DataObjectRestResponse getEntity(@PathVariable("id") String id) throws APIException {

		logger.info("Request received to retrieve the entity details. id: {}", id);

		DataObjectRestResponse resp = null;
		try {
			resp = getDataObjectRestResponse(id);
		} catch (IllegalArgumentException e) {
			throw BadRequestException.parameterInvalid("id", "The Id is does not correspond to any resource type.");
		}
		return resp;
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteEntity(@PathVariable("id") String id,
			@RequestParam(name = REQ_PARAM_FORCE_DELETE, defaultValue = "false") String forceDelete)
			throws APIException {

		logger.info("Request received to delete entity with id: {}", id);
		ArgValidator.checkUri(id);
		String className = UUIDUtil.getTypeName(id);
		DataObject dataObject = dbClient.queryObject(className, id);
		ArgValidator.checkEntityNotNull(dataObject, id, true);

		Boolean mustDelete = Boolean.valueOf(forceDelete);

		switch (className.toLowerCase()) {
		case "vendor":
			Vendor vendor = (Vendor) dataObject;
			DealerUtility.deleteVendor(vendor.getId(), false, mustDelete);
			break;
		case "buyer":
			Buyer buyer = (Buyer) dataObject;
			DealerUtility.deleteBuyer(buyer.getId(), false, mustDelete);
			break;
		case "product":
			Product product = (Product) dataObject;
			ProductUtility.deleteProduct(product.getId(), mustDelete);
			break;
		case "productoffer":
		case "enquiry":
		case "purchaseitem":
			dbClient.markAsDeleted(dataObject);
			break;
		default:
			break;
		}

		return TaskResponseUtility.createTaskSubmittedResponse("Task initiated to delete entity.");
	}

	@RequestMapping(method = RequestMethod.GET, value = "/reviews/{reviewId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ReviewInfoResponse getReview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("reviewId") String reviewId) {
		logger.info("Request received to get review for reviewId: {}", reviewId);

		String reviewType = StringUtils.getTypeName(reviewId);

		switch (reviewType.toLowerCase()) {
		case "vendorreview":
			VendorReview vendorReview = dbClient.queryObject(VendorReview.class, reviewId);
			VendorReviewInfoResponse vendorReviewInfoResponse = ReviewResponseMapper.map(vendorReview, DATE_FORMAT);
			return vendorReviewInfoResponse;
		case "buyerreview":
			BuyerReview buyerReview = dbClient.queryObject(BuyerReview.class, reviewId);
			BuyerReviewInfoResponse buyerReviewInfoResponse = ReviewResponseMapper.map(buyerReview, DATE_FORMAT);
			return buyerReviewInfoResponse;
		case "productreview":
			ProductReview productReview = dbClient.queryObject(ProductReview.class, reviewId);
			ProductReviewInfoResponse productReviewInfoResponse = ReviewResponseMapper.map(productReview, DATE_FORMAT);
			return productReviewInfoResponse;
		default:
			return new NullReviewInfoResponse();

		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/product-enquiries", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public EnquiryInfoListResponse getEnquiryList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get enquiries by filter: {}, fetchSize: {}, offset: {}", filter, fetchSize,
				offset);

		final String[] QUERY_FILTER_KEYS = { "name", "status", "buyerId", REQ_PARAM_ENQUIRY_DATE,
				REQ_PARAM_START_ENQUIRY_DATE, REQ_PARAM_END_ENQUIRY_DATE };

		EnquiryInfoListResponse response = new EnquiryInfoListResponse();

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields();

		boolean requestDateGiven = false;

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);

			switch (key) {
			case "buyerId":
				ArgValidator.checkFieldUriType(value, Buyer.class, key);
				Buyer buyer = dbClient.queryObject(Buyer.class, value);
				fields.add("buyer", buyer);
				break;
			case "status":
				ArgValidator.checkFieldValue(!EntityStatus.BLACKLISTED.name().equals(value.toUpperCase()), key);
				fields.add("entityStatus", value.toUpperCase());
				break;
			case REQ_PARAM_ENQUIRY_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("enquiryTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("enquiryTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_ENQUIRY_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_ENQUIRY_DATE + " with this field.");
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				fields.add("enquiryTimestamp >=", startRequestDate.getTime());
				break;
			case REQ_PARAM_END_ENQUIRY_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_ENQUIRY_DATE + " with this field.");
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils.getEndOfDayTimestamp(endRequestDate);
				fields.add("enquiryTimestamp <=", endTimestamp2);
				break;
			default:
				break;
			}

		}

		ResultSetIterator<Enquiry> resultSetIterator = null;
		String productName = keyValueMap.get("name");
		do {
			resultSetIterator = getResultSetIterator(Enquiry.class, fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					Enquiry item = resultSetIterator.next();

					if (item == null) {
						continue;
					} else if (productName != null
							&& !productName.equals(item.getProduct().getProductName().getName())) {
						logger.info("Skipping this enquiry [{}] as its product does not match with name: {}",
								item.getId(), productName);
						continue;
					} else {
						EnquiryInfoResponse infoResponse = ProductResponseMapper.mapProductEnquiry(item, DATE_FORMAT);
						infoResponse.setLink(Endpoint.ADMIN_PRODUCT_ENQUIRY_INFO.get(item.getId()));
						response.addEnquiryInfoResponse(infoResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}
		} while (response.size() < fetchSize);

		logger.info("Returning response of type enquiryListResponse of size {}.", response.size());
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/product-enquiries/{id}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public EnquiryInfoResponse getEnquiry(@PathVariable("id") String enquiryId) {

		logger.info("Request received to retrive enquiry details of {} ", enquiryId);

		ArgValidator.checkFieldUriType(enquiryId, Enquiry.class, "enquiry_id");

		Enquiry enquiry = dbClient.queryObject(Enquiry.class, enquiryId);

		EnquiryInfoResponse resp = new EnquiryInfoResponse();
		if (enquiry != null) {
			ProductResponseMapper.mapProductEnquiry(enquiry, DATE_FORMAT, resp);
			resp.setLink(Endpoint.ADMIN_PRODUCT_ENQUIRY_INFO.get(enquiryId));
		}
		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/dealers/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public UserAccountInfoResponse getUserAccountByRelationshipId(@PathVariable("id") String relationshipId) {

		logger.info("Request received to retrive user account details of user with relationship Id {} ",
				relationshipId);

		ArgValidator.checkFieldNotNull(relationshipId, "relationship_id");
		UserAccount userAccount = loginHelper.getUserAccountByRelationshipId(relationshipId);
		ArgValidator.checkEntityNotNull(userAccount, relationshipId, true);
		UserAccountInfoResponse resp = UserAccountResponseMapper.map(userAccount);

		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/product-enquiries/associated-buyers", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public BuyerListResponse getAssociatedBuyerList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_FILTER) String filter) {

		logger.info("Request received to get associated buyers of product enquiries");

		final String[] QUERY_FILTER_KEYS = { REQ_PARAM_ENQUIRY_DATE, REQ_PARAM_START_ENQUIRY_DATE,
				REQ_PARAM_END_ENQUIRY_DATE };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields();

		boolean requestDateGiven = false;

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case REQ_PARAM_ENQUIRY_DATE:
				requestDateGiven = true;
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date requestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long startTimestamp = requestDate.getTime();
				fields.add("enquiryTimestamp >=", startTimestamp);
				Long endTimestamp = DateUtils.getEndOfDayTimestamp(requestDate);
				fields.add("enquiryTimestamp <=", endTimestamp);
				break;
			case REQ_PARAM_START_ENQUIRY_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_ENQUIRY_DATE + " with this field.");
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date startRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				fields.add("enquiryTimestamp >=", startRequestDate.getTime());
				break;
			case REQ_PARAM_END_ENQUIRY_DATE:
				ArgValidator.checkField(requestDateGiven == false, key,
						"Cannot have " + REQ_PARAM_ENQUIRY_DATE + " with this field.");
				ArgValidator.checkFieldValue(StringUtils.isDateFormatValid(value, DATE_FORMAT), key);
				Date endRequestDate = DateUtils.getDate(DATE_FORMAT, value);
				Long endTimestamp2 = DateUtils.getEndOfDayTimestamp(endRequestDate);
				fields.add("enquiryTimestamp <=", endTimestamp2);
				break;
			default:
				break;
			}
		}

		List<Enquiry> enquiryList = dbClient.queryList(Enquiry.class, fields);

		BuyerListResponse buyerListResponse = new BuyerListResponse();
		Set<String> uriSet = new HashSet<>();
		for (Iterator<Enquiry> iterator = enquiryList.iterator(); iterator.hasNext();) {
			Enquiry enquiry = iterator.next();
			Buyer buyer = enquiry.getBuyer();
			if (buyer != null) {
				if (uriSet.add(buyer.getId())) {
					buyerListResponse.addNamedResourceResponse(toNamedRelatedRestResponse(buyer));
				}
			}
		}
		logger.info("Returning buyer list of size: {}.", buyerListResponse.size());

		return buyerListResponse;
	}

}