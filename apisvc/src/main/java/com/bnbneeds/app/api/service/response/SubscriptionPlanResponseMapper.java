package com.bnbneeds.app.api.service.response;

import com.bnbneeds.app.db.client.model.BiddingSubscriptionPlan;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscriptionPlan;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscriptionPlan;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionPlanInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.SubscriptionPlanInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionPlanInfoResponse;

public abstract class SubscriptionPlanResponseMapper extends DataObjectResponseMapper {

	public static SubscriptionPlanInfoResponse mapSubscriptionPlan(BiddingSubscriptionPlan from,
			SubscriptionPlanInfoResponse to) {

		mapDescribedDataObject(from, to);

		to.setTitle(from.getTitle());
		to.setDurationType(from.getDurationType());
		to.setDurationValue(from.getDurationValue());
		to.setPremiumAmount(from.getPremiumAmount());
		to.setType(from.getType());
		to.setDefaultPlan(from.isDefaultPlan());
		to.setAvailablityStatus(from.getAvailabilityStatus());
		return to;
	}

	public static BuyerBiddingSubscriptionPlanInfoResponse mapSubscriptionPlan(BuyerBiddingSubscriptionPlan from) {

		BuyerBiddingSubscriptionPlanInfoResponse to = new BuyerBiddingSubscriptionPlanInfoResponse();
		mapSubscriptionPlan(from, to);
		to.setNumberOfTendersAllowedToCreate(from.getNumberOfTendersAllowedToCreate());
		return to;
	}

	public static VendorBiddingSubscriptionPlanInfoResponse mapSubscriptionPlan(VendorBiddingSubscriptionPlan from) {

		VendorBiddingSubscriptionPlanInfoResponse to = new VendorBiddingSubscriptionPlanInfoResponse();
		mapSubscriptionPlan(from, to);
		to.setNumberOfTendersAllowedToAccess(from.getNumberOfTendersAllowedToAccess());
		return to;
	}

}
