package com.bnbneeds.app.api.service.response;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import com.bnbneeds.app.db.client.model.BlobObject;
import com.bnbneeds.app.db.client.model.Enquiry;
import com.bnbneeds.app.db.client.model.Enquiry.LikelyToBuyIn;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.ProductCategory;
import com.bnbneeds.app.db.client.model.ProductName;
import com.bnbneeds.app.db.client.model.ProductOffer;
import com.bnbneeds.app.db.client.model.ProductType;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.NamedRelatedResourceRep;
import com.bnbneeds.app.model.RelatedResourceRep;
import com.bnbneeds.app.model.dealer.EnquiryInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.ProductImageListResponse;
import com.bnbneeds.app.model.product.ProductInfoResponse;
import com.bnbneeds.app.model.product.ProductOfferInfoResponse;

public abstract class ProductResponseMapper extends DataObjectResponseMapper {

	public static void mapProduct(Product from, ProductInfoResponse to) {
		mapDescribedDataObject(from, to);
		to.setCostPrice(from.getCostPrice());
		to.setModelId(from.getModelId());
		Vendor vendor = from.getVendor();
		to.setVendorByIdAndName(vendor.getId(), vendor.getName());
		ProductName prodName = from.getProductName();
		to.setProductName(prodName.getId(), prodName.getName());
		ProductCategory category = from.getProductCategory();
		to.setProductCategoryById(category.getId(), category.getName());
		ProductType type = from.getProductType();
		to.setProductTypeById(type.getId(), type.getName());
		to.setAttributes(from.getAttributes());
		to.setServiceLocations(from.getServiceLocations());
		to.setRemarks(from.getRemarks());
		to.setVideoURL(from.getVideoURL());
		to.setTutorialVideoURL(from.getTutorialVideoURL());
		Set<BlobObject> imageFiles = from.getImages();
		if (imageFiles != null && !imageFiles.isEmpty()) {
			ProductImageListResponse imageList = new ProductImageListResponse();
			imageList.setProductId(from.getId());
			for (Iterator<BlobObject> iterator = imageFiles.iterator(); iterator
					.hasNext();) {
				BlobObject blobObject = iterator.next();
				imageList.addImage(blobObject.getBlobKey(),
						blobObject.getMediaType(), blobObject.getSize());

			}
			to.setImageList(imageList);
		}
	}

	public static ProductInfoResponse mapProduct(Product from) {
		ProductInfoResponse to = new ProductInfoResponse();
		mapProduct(from, to);
		return to;
	}

	public static void mapProductOffer(ProductOffer from,
			ProductOfferInfoResponse to) {
		mapDescribedDataObject(from, to);
		to.setProduct(from.getProduct().getId());
		to.setStartDate(from.getStartDate());
		to.setEndDate(from.getEndDate());
	}

	public static ProductOfferInfoResponse mapProductOffer(ProductOffer from) {
		ProductOfferInfoResponse to = new ProductOfferInfoResponse();
		mapProductOffer(from, to);
		return to;
	}

	public static void mapProductEnquiry(Enquiry from,
			SimpleDateFormat dateFormat, EnquiryInfoResponse to) {
		mapDescribedDataObject(from, to);
		to.setBuyer(DBObjectMapper.toNamedRelatedRestResponse(from.getBuyer()));
		Vendor vendor = from.getProduct().getVendor();
		to.setVendor(DBObjectMapper.toNamedRelatedRestResponse(vendor));
		RelatedResourceRep productRelatedResourceRep = DBObjectMapper
				.toRelatedRestResponse(from.getProduct());
		String productName = from.getProduct().getProductName().getName();
		to.setProduct(new NamedRelatedResourceRep(productRelatedResourceRep,
				productName));
		Date enquiryDate = new Date(from.getEnquiryTimestamp());
		to.setRequestDate(dateFormat.format(enquiryDate));
		to.setRequiredQuantity(from.getRequiredQuantity());
		LikelyToBuyIn likelyToBuyIn = from.getLikelyToBuyIn();
		to.setLikelyToBuyIn(likelyToBuyIn.getValue(),
				likelyToBuyIn.getTimeUnit());
		to.setLink(Endpoint.BUYER_PRODUCT_ENQUIRY_INFO.get(from.getBuyer()
				.getId(), from.getId()));
	}

	public static EnquiryInfoResponse mapProductEnquiry(Enquiry from,
			SimpleDateFormat dateFormat) {
		EnquiryInfoResponse to = new EnquiryInfoResponse();
		mapProductEnquiry(from, dateFormat, to);
		return to;
	}
}
