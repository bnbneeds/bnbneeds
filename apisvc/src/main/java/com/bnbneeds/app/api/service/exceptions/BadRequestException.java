package com.bnbneeds.app.api.service.exceptions;

public class BadRequestException extends APIException {

	private static final long serialVersionUID = 2417486850678036862L;

	private static final String PARAMETER_INVALID1 = "Parameter provided is invalid: %s.";
	private static final String PARAMETER_INVALID2 = "Parameter provided is invalid: %1$s. %2$s.";
	private static final String PARAMETER_MISSING = "%s is missing or empty.";
	private static final String QUERYPARAM_IS_NULL = "Query parameter: %s is null.";
	private static final String QUERYPARAM_NOT_FOUND = "Query parameter: %s with value: %s not found.";
	private static final String NAME_ALREADY_EXISTS_1 = "%s with name %s already exists.";
	private static final String NAME_ALREADY_EXISTS_2 = "%s already exists.";
	private static final String INVALID_UID = "%s is invalid.";
	private static final String INVALID_UID_WRONG_TYPE = "%s is not of type %s.";
	private static final String PARAMETER_INVALID_EXPECTED = PARAMETER_INVALID1 + ". Expected value: %s.";
	private static final String REQUEST_OBJECT_INACTIVE = "Requested object with id: %s is inactive.";
	private static final String REQUEST_OBJECT_BLACKLISTED = "Requested object with id: %s is blacklisted.";
	private static final String REQUEST_OBJECT_DISAPPROVED = "Requested object with id: %s has been disapproved.";
	private static final String REQUEST_OBJECT_APPROVED = "Requested object with id: %s is approved.";
	private static final String PARAMETER_NOT_WITHIN_RANGE = "Invalid parameter %1$s was %2$s %5$s but should be between %3$s %5$s and %4$s %5$s.";
	private static final String INVALID_PARAMETER_BELOW_MINIMUM = "Invalid parameter %1$s was %2$s %4$s but minimum is %3$s %4$s.";
	private static final String INVALID_PARAMETER_ABOVE_MAXIMUM = "Invalid parameter %1$s was %2$s %4$s but maximum is %3$s %4$s.";

	private static final String MAX_IMAGES_UPLOADED = "Cannot upload more than %s image(s).";

	private static final String REVIEW_ALREADY_EXISTS = "Review already exists for the entity with id: %s.";
	private static final String CANNOT_INITIATE_ACTIVATION_FOR_SUBSCRIPTION_WITH_REASON = "Cannot initiate activation for this subscription. Reason: %s.";
	private static final String SUBSCRIPTION_CANNOT_BE_ACTIVATED_SUBSCRIPTION_STATE_NOT_VALID = "The subscription cannot be activated because its state is: %s.";
	private static final String SUBSCRIPTION_STATUS_CANNOT_BE_UPDATED_SUBSCRIPTION_STATE_NOT_VALID = "The subscription status cannot be updated because its state is: %s.";
	private static final String SUBSCRIPTION_CANNOT_BE_ACTIVATED_NO_SUCCESSFUL_PAYMENT_TRANSACTION_FOUND = "No successful payment transaction found for the subscription. So the subscription cannot be activated.";
	private static final String REQUEST_INVALID_NOTHING_TO_UPDATE = "Bad Request: There was nothing to update.";
	private static final String QUOTA_EXCEEDED = "Quota exceeded for the %s with id: %s.";
	private static final String SUBSCRIPTION_PLAN_STATUS_CANNOT_BE_CHANGED = "The status of the subscription plan cannot be changed. There is a subscription %1$s with status: %2$s using this plan.";

	public BadRequestException() {
		super();
	}

	public BadRequestException(String message) {
		super(message);
	}

	public static BadRequestException create(String message) {
		return new BadRequestException(message);
	}

	public static BadRequestException nameAlreadyExists(String paramName, String paramValue) {
		return new BadRequestException(getMessage(NAME_ALREADY_EXISTS_1, paramName, paramValue));
	}

	public static BadRequestException nameAlreadyExists(String paramName) {
		return new BadRequestException(getMessage(NAME_ALREADY_EXISTS_2, paramName));
	}

	public static BadRequestException invalidPasswordFormat() {
		return new BadRequestException(
				getMessage(PARAMETER_INVALID2, "password", "Must be an alphanumeric word of atleast 6 characters."));
	}

	public static BadRequestException queryStringIsNull(String queryParamName) {
		return new BadRequestException(getMessage(QUERYPARAM_IS_NULL, queryParamName));
	}

	public static BadRequestException queryRecordNotFound(String queryParamName, String queryParamValue) {
		return new BadRequestException(getMessage(QUERYPARAM_NOT_FOUND, queryParamName, queryParamValue));
	}

	public static BadRequestException invalidUID(String uid) {
		return new BadRequestException(getMessage(INVALID_UID, uid));
	}

	@SuppressWarnings("rawtypes")
	public static BadRequestException invalidUID(String uid, Class clazz) {
		return new BadRequestException(getMessage(INVALID_UID_WRONG_TYPE, uid, clazz.getSimpleName()));
	}

	public static BadRequestException parameterMissing(String fieldName) {
		return new BadRequestException(getMessage(PARAMETER_MISSING, fieldName));
	}

	public static BadRequestException parameterInvalid(String fieldName) {
		return new BadRequestException(getMessage(PARAMETER_INVALID1, fieldName));
	}

	public static BadRequestException parameterInvalid(String fieldName, String message) {
		return new BadRequestException(getMessage(PARAMETER_INVALID2, fieldName, message));
	}

	public static BadRequestException parameterInvalidExpectedValue(String fieldName, String expectedValues) {
		return new BadRequestException(getMessage(PARAMETER_INVALID_EXPECTED, fieldName, expectedValues));
	}

	public static BadRequestException requestObjectIsInactive(String id) {
		return new BadRequestException(getMessage(REQUEST_OBJECT_INACTIVE, id));
	}

	public static BadRequestException requestObjectIsBlacklisted(String id) {
		return new BadRequestException(getMessage(REQUEST_OBJECT_BLACKLISTED, id));
	}

	public static BadRequestException requestObjectIsDisapproved(String id) {
		return new BadRequestException(getMessage(REQUEST_OBJECT_DISAPPROVED, id));
	}

	public static BadRequestException requestObjectIsApproved(String id) {
		return new BadRequestException(getMessage(REQUEST_OBJECT_APPROVED, id));
	}

	public static BadRequestException imageUploadsExceeded(String maxNumber) {
		return new BadRequestException(getMessage(MAX_IMAGES_UPLOADED, maxNumber));
	}

	public static BadRequestException reviewAlreadyExists(String id) {
		return new BadRequestException(getMessage(REVIEW_ALREADY_EXISTS, id));
	}

	public static BadRequestException parameterNotWithinRange(final String parameter, Number value, final Number min,
			final Number max, String units) {
		return new BadRequestException(getMessage(PARAMETER_NOT_WITHIN_RANGE, parameter.toString(), value.toString(),
				min.toString(), max.toString(), units));
	}

	public static BadRequestException invalidParameterBelowMinimum(String parameter, Number value, Number minimum,
			String unit) {

		return new BadRequestException(getMessage(INVALID_PARAMETER_BELOW_MINIMUM, parameter.toString(),
				value.toString(), minimum.toString(), unit));

	}

	public static BadRequestException invalidParameterAboveMaximum(String parameter, Number value, Number maximum,
			String unit) {

		return new BadRequestException(getMessage(INVALID_PARAMETER_ABOVE_MAXIMUM, parameter.toString(),
				value.toString(), maximum.toString(), unit));

	}

	public static BadRequestException subscriptionCannotBeActivatedAsNoSuccessfulPaymentTransactionFound() {
		return create(SUBSCRIPTION_CANNOT_BE_ACTIVATED_NO_SUCCESSFUL_PAYMENT_TRANSACTION_FOUND);
	}

	public static BadRequestException cannotInitiateActivationForSubscriptionWithReason(String reason) {
		return create(getMessage(CANNOT_INITIATE_ACTIVATION_FOR_SUBSCRIPTION_WITH_REASON, reason));
	}

	public static BadRequestException subscriptionCannotBeActivatedAsStateIsInvalid(String invalidState) {
		return create(getMessage(SUBSCRIPTION_CANNOT_BE_ACTIVATED_SUBSCRIPTION_STATE_NOT_VALID, invalidState));
	}

	public static BadRequestException subscriptionStatusCannotBeUpdatedAsStateIsInvalid(String invalidState) {
		return create(getMessage(SUBSCRIPTION_STATUS_CANNOT_BE_UPDATED_SUBSCRIPTION_STATE_NOT_VALID, invalidState));
	}

	public static BadRequestException nothingToUpdate() {
		return create(REQUEST_INVALID_NOTHING_TO_UPDATE);
	}

	public static BadRequestException quotaExceeded(String entity, String id) {
		return new BadRequestException(getMessage(QUOTA_EXCEEDED, entity, id));
	}

	public static BadRequestException subscriptionPlanStatusCannotBeUpdated(String id, String subscriptionStatus) {
		return create(getMessage(SUBSCRIPTION_PLAN_STATUS_CANNOT_BE_CHANGED, id, subscriptionStatus));
	}

}
