package com.bnbneeds.app.api.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.ApprovedDataObject;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscription;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscriptionPlan;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription.SubscriptionState;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscription;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscriptionPlan;

public class EntityAdminUtility {

	private static final Logger logger = LoggerFactory.getLogger(EntityAdminUtility.class);

	/**
	 * Validates different conditions specific for entity types
	 * 
	 * @param approvedDataObject
	 *            the entity whose status needs to be updated
	 * @param newEntityStatus
	 *            the new status of the entity
	 * @throws BadRequestException
	 *             if entity is type SubscriptionPlan and the subscriptions state is
	 *             ACTIVATION_IN_PROGRESS
	 */
	public static void validateEntityApprovalForEntities(ApprovedDataObject approvedDataObject,
			EntityStatus newEntityStatus, DbClient dbClient) {
		String entityType = approvedDataObject.getClass().getSimpleName();

		switch (entityType) {
		case "BuyerBiddingSubscriptionPlan":
			BuyerBiddingSubscriptionPlan plan = (BuyerBiddingSubscriptionPlan) approvedDataObject;
			// check if there are any ACTIVATION_IN_PROGRESS subscription with this plan.
			QueryFields fields = new QueryFields("subscriptionPlan", plan).add("subscriptionState",
					SubscriptionState.ACTIVATION_IN_PROGRESS.name());

			BuyerBiddingSubscription buyerSubscription = dbClient.queryObjectByFields(BuyerBiddingSubscription.class,
					fields);
			if (buyerSubscription != null) {
				throw BadRequestException.subscriptionPlanStatusCannotBeUpdated(buyerSubscription.getId(),
						buyerSubscription.getSubscriptionState().name());
			}
			break;
		case "VendorBiddingSubscriptionPlan":
			VendorBiddingSubscriptionPlan vendorBiddingSubscriptionPlan = (VendorBiddingSubscriptionPlan) approvedDataObject;
			// check if there are any ACTIVATION_IN_PROGRESS subscription with this plan.
			QueryFields vedorQueryFields = new QueryFields("subscriptionPlan", vendorBiddingSubscriptionPlan)
					.add("subscriptionState", SubscriptionState.ACTIVATION_IN_PROGRESS.name());

			VendorBiddingSubscription vendorSubscription = dbClient.queryObjectByFields(VendorBiddingSubscription.class,
					vedorQueryFields);
			if (vendorSubscription != null) {
				throw BadRequestException.subscriptionPlanStatusCannotBeUpdated(vendorSubscription.getId(),
						vendorSubscription.getSubscriptionState().name());
			}
			break;
		default:
			break;
		}

	}

}
