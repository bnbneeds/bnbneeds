package com.bnbneeds.app.api.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.ApprovedDataObject;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.BiddingSubscriptionPlan;
import com.bnbneeds.app.model.dealer.subscription.BiddingSubscriptionPlanParam;

public class BiddingSubscriptionPlanUtility {

	private static final Logger logger = LoggerFactory.getLogger(BiddingSubscriptionPlanUtility.class);

	public static void validateBiddingSubscriptionPlanRequest(BiddingSubscriptionPlanParam param) {

		ArgValidator.checkFieldNotNull(param, "bidding_subscription_plan");
		ArgValidator.checkFieldNotEmpty(param.getTitle(), "title");
		ArgValidator.checkFieldNotEmpty(param.getName(), "name");
		ArgValidator.checkFieldNotEmpty(param.getDescription(), "description");
		ArgValidator.checkFieldNotEmpty(param.getPremiumAmount().toString(), "premium_amount");
		ArgValidator.checkFieldNotEmpty(param.getType(), "type");
	}

	public static void setStatusOfNewEntity(ApprovedDataObject dataObject, BNBNeedsUser user) {
		StringBuffer remark = new StringBuffer(dataObject.getClass().getSimpleName());
		remark.append(" entity created by ").append(user.getUsername());
		dataObject.setEntityStatus(EntityStatus.DISAPPROVED);
		remark.append(", Role: ").append(user.getRole()).append(".");
		dataObject.addRemark(remark.toString());
	}

	public static void setSubscriptionPlanEntityFromRequestForDurationPlanType(BiddingSubscriptionPlanParam param,
			BiddingSubscriptionPlan subscriptionPlanEntity) {
		ArgValidator.checkFieldNotEmpty(param.getDurationType(), "duration_type");
		ArgValidator.checkFieldNotEmpty(param.getDurationType(), "duration_value");
		subscriptionPlanEntity.setType(BiddingSubscriptionPlan.PlanType.DURATION.name());
		subscriptionPlanEntity.setDurationType(param.getDurationType());
		subscriptionPlanEntity.setDurationValue(param.getDurationValue());
	}

	public static void setDefaultStatus(BiddingSubscriptionPlan subscriptionPlan, boolean defaultStatus,
			DbClient dbClient) {

		if (defaultStatus == subscriptionPlan.isDefaultPlan()) {
			return;
		}

		DBOperationList dbOperationList = new DBOperationList();
		if (defaultStatus) {
			// Find the existing default plan and set it as not default
			QueryFields defaultPlanQueryFields = new QueryFields("defaultPlan", true);
			BiddingSubscriptionPlan currentDefaultPlan = dbClient.queryObjectByFields(subscriptionPlan.getClass(),
					defaultPlanQueryFields);
			if (currentDefaultPlan != null) {
				currentDefaultPlan.setDefaultPlan(false);
				dbOperationList.addOperation(OperationType.UPDATE, currentDefaultPlan);
			}
		}
		subscriptionPlan.setDefaultPlan(defaultStatus);
		dbOperationList.addOperation(OperationType.UPDATE, subscriptionPlan);

		dbClient.transact(dbOperationList);
	}
}
