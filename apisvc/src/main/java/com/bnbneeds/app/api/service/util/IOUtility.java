package com.bnbneeds.app.api.service.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.IOUtils;

import com.bnbneeds.app.messaging.email.Attachment;

public class IOUtility {

	public static InputStream clone(InputStream is) throws IOException {
		ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
		IOUtils.copyLarge(is, byteOut);
		InputStream clone = new ByteArrayInputStream(byteOut.toByteArray());
		return clone;
	}

	public static Attachment getAttachment(FileItemStream file)
			throws IOException {
		Attachment attachment = null;
		if (file != null) {
			attachment = new Attachment();
			attachment.setFileName(file.getName());
			attachment.setInputStream(IOUtility.clone(file.openStream()));
			attachment.setMediaType(file.getContentType());
		}
		return attachment;
	}

	public static String getString(FileItemStream item) throws IOException {
		InputStream is = item.openStream();
		return Streams.asString(is);
	}

	public static boolean isImage(FileItemStream item) {
		if (item != null && item.getContentType() != null) {
			return item.getContentType().startsWith("image/");
		}
		return false;
	}

}
