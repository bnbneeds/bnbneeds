package com.bnbneeds.app.api.service.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Task;
import com.bnbneeds.app.db.client.model.Task.Status;
import com.bnbneeds.app.util.StringUtils;

/**
 * The instance of this class must be used in HTTP request scope only because it
 * is using DbClient.
 * 
 * @author Amit Herlekar
 *
 */
public class TaskHelper {

	private static final Logger logger = LoggerFactory.getLogger(TaskHelper.class);

	private DbClient dbClient;

	public void setDbClient(DbClient dbClient) {
		this.dbClient = dbClient;
	}

	public Task createTask(String entityId, String taskName, Task waitFor) {
		Task task = new Task();
		task.setId(UUIDUtil.createId(Task.class));
		task.setName(taskName);
		if (waitFor != null) {
			task.setWaitFor(waitFor);
		}
		task.setEntityId(entityId);
		task.setStatus(Status.NEW);
		logger.info("Creating task: {}", task);
		dbClient.createObject(task);
		return task;
	}

	public Task createTask(String entityId, String taskName) {
		Task task = new Task();
		task.setId(UUIDUtil.createId(Task.class));
		task.setName(taskName);
		task.setEntityId(entityId);
		task.setStatus(Status.NEW);
		dbClient.createObject(task);
		return task;
	}

	public void markAsSuccess(Task task) {
		task.setStatus(Status.SUCCESS);
		dbClient.updateObject(task);
	}

	public void markAsSuccess(String taskId) {
		Task task = getTask(taskId);
		if (task != null) {
			markAsSuccess(task);
		}
	}

	public void markAsFailed(Task task) {
		task.setStatus(Status.FAILED);
		dbClient.updateObject(task);
	}

	public void markAsFailed(String taskId) {
		Task task = getTask(taskId);
		if (task != null) {
			markAsFailed(task);
		}
	}

	public void markAsInProgress(Task task) {
		task.setStatus(Status.IN_PROGRESS);
		dbClient.updateObject(task);
	}

	public void markAsInProgress(String taskId) {
		Task task = getTask(taskId);
		if (task != null) {
			markAsInProgress(task);
		}
	}

	public void delete(Task task) {
		dbClient.markAsDeleted(task);
	}

	public void delete(String taskId) {
		Task task = getTask(taskId);
		if (task != null) {
			delete(task);
		}
	}

	public Task getTask(String taskId) {
		Task task = null;
		if (StringUtils.hasText(taskId)) {
			ArgValidator.checkFieldUriType(taskId, Task.class, "taskId");
			task = dbClient.queryObject(Task.class, taskId);
		}
		return task;
	}

	public void checkForWaitingTask(String taskId) {
		Task task = getTask(taskId);
		if (task != null) {
			checkForWaitingTask(task);
		}
	}

	public void checkForWaitingTaskAndMarkAsInProgress(String taskId) {
		Task task = getTask(taskId);
		if (task != null) {
			checkForWaitingTask(task);
			markAsInProgress(task);
		}
	}

	public void transact(DBOperationList oplist, String taskId) {
		try {
			dbClient.transact(oplist);
		} catch (Exception e) {
			markAsFailed(taskId);
			throw e;
		}
	}

	public void checkForWaitingTask(Task task) {
		Task waitingFor = task.getWaitFor();
		if (waitingFor != null) {
			if (waitingFor.hasStatus(Status.SUCCESS)) {
				return;
			} else if (waitingFor.hasStatus(Status.NEW)) {
				logger.info("Cannot execute task [{}] because it is depending on task [{}] to start execution...",
						task.getId(), waitingFor.getId());
				throw BadRequestException.create("Cannot execute task: [" + task.getName()
						+ "] because it is depending on the task [" + waitingFor.getName() + "] to start execution...");
			} else if (waitingFor.hasStatus(Status.IN_PROGRESS)) {
				logger.info("Cannot execute task [{}] because the dependent task [{}] is in progress..", task.getId(),
						waitingFor.getId());
				throw BadRequestException.create("Cannot execute task: [" + task.getName()
						+ "] because the dependent task [" + waitingFor.getName() + "] is in progress...");
			} else if (waitingFor.hasStatus(Status.FAILED)) {
				logger.info("Cannot execute task [{}] because the dependent task [{}] has failed!", task.getId(),
						waitingFor.getId());
				throw BadRequestException.create("Cannot execute task: [" + task.getName()
						+ "] because the dependent task [" + waitingFor.getName() + "] has failed.");
			}
		}
	}
}
