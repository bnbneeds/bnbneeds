package com.bnbneeds.app.api.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.api.service.controller.AsyncBuyerDeleteOperationsService;
import com.bnbneeds.app.api.service.controller.AsyncVendorDeleteOperationsService;
import com.bnbneeds.app.api.service.documentsearch.SearchAPIUtility;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.helper.TaskHelper;
import com.bnbneeds.app.api.service.pipeline.PipelineUtils;
import com.bnbneeds.app.api.service.pipeline.buyer.DeleteBuyerJobROOT;
import com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.BusinessType;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.Dealer;
import com.bnbneeds.app.db.client.model.Task;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.dealer.CreateBuyerParam;
import com.bnbneeds.app.model.dealer.CreateVendorParam;
import com.bnbneeds.app.model.dealer.DealerParam;
import com.bnbneeds.app.model.dealer.UpdateBuyerParam;
import com.bnbneeds.app.model.dealer.UpdateVendorParam;
import com.bnbneeds.app.util.StringUtils;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Document.Builder;

public class DealerUtility {
	private static final Logger logger = LoggerFactory.getLogger(DealerUtility.class);

	public static void validateCreateDealerParam(DealerParam param, DbClient dbClient) {

		String dealerName = param.getOrganizationName();
		ArgValidator.checkFieldNotEmpty(dealerName, "organization_name");

		QueryFields field = new QueryFields("name", dealerName);
		if (param instanceof CreateVendorParam) {
			CreateVendorParam createVendorParam = (CreateVendorParam) param;
			ArgValidator.checkFieldNotNull(createVendorParam, "create_vendor");
			Vendor vendor = dbClient.queryObjectByFields(Vendor.class, field);
			if (vendor != null) {
				throw BadRequestException.nameAlreadyExists(Vendor.class.getSimpleName(), dealerName);
			}
			/*
			 * ArgValidator.checkFieldNotEmpty(createVendorParam.getVatNumber(),
			 * "vat_number");
			 */
			ArgValidator.checkFieldNotEmpty(createVendorParam.getCities(), "cities");
		}

		if (param instanceof CreateBuyerParam) {
			CreateBuyerParam createBuyerParam = (CreateBuyerParam) param;
			ArgValidator.checkFieldNotNull(createBuyerParam, "create_buyer");
			Buyer buyer = dbClient.queryObjectByFields(Buyer.class, field);
			if (buyer != null) {
				throw BadRequestException.nameAlreadyExists(Buyer.class.getSimpleName(), dealerName);
			}
			String businessTypeId = createBuyerParam.getBusinessTypeId();
			ArgValidator.checkFieldUriType(businessTypeId, BusinessType.class, "business_type_id");
			BusinessType businessType = dbClient.queryObject(BusinessType.class, businessTypeId);
			ArgValidator.checkEntityDisapprovedOrBlacklisted(businessType, createBuyerParam.getBusinessTypeId(), false);

		}

		ArgValidator.checkFieldNotEmpty(param.getContactName(), "contact_name");
		ArgValidator.checkFieldNotEmpty(param.getAddress(), "address");
		ArgValidator.checkFieldNotEmpty(param.getEmailAddress(), "email_address");
		ArgValidator.checkFieldValue(StringUtils.isEmailAddressValid(param.getEmailAddress()), "email_address");
		ArgValidator.checkFieldNotEmpty(param.getMobileNumber(), "mobile_number");

		ArgValidator.checkFieldValue(StringUtils.isPhoneNumberValid(param.getMobileNumber()), "mobile_number");

		ArgValidator.checkFieldNotEmpty(param.getPan(), "pan");

	}

	public static Dealer validateUpdateDealerParam(String id, DealerParam param, BNBNeedsUser user, DbClient dbClient) {

		Vendor vendor = null;
		Buyer buyer = null;
		Dealer dealer = null;

		if (param instanceof UpdateVendorParam) {
			ArgValidator.checkFieldUriType(id, Vendor.class, "vendor_id");
			vendor = dbClient.queryObject(Vendor.class, id);
			ArgValidator.checkEntityNotNull(vendor, id, true);
			if (user.isVendor()) {
				ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, id, true);
			}

			UpdateVendorParam updateVendorParam = (UpdateVendorParam) param;
			String vatNumber = updateVendorParam.getVatNumber();
			vendor.setVatNumber(vatNumber);

			String servicetaxNumber = updateVendorParam.getServiceTaxNumber();
			vendor.setServiceTaxNumber(servicetaxNumber);

			String cities = updateVendorParam.getCities();
			if (!StringUtils.isEmpty(cities)) {
				vendor.setCities(cities);
			}
			dealer = vendor;
		}

		if (param instanceof UpdateBuyerParam) {
			ArgValidator.checkFieldUriType(id, Buyer.class, "buyer_id");
			buyer = dbClient.queryObject(Buyer.class, id);
			ArgValidator.checkEntityNotNull(buyer, id, true);
			if (user.isBuyer()) {
				ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, id, true);
			}
			UpdateBuyerParam updateBuyerParam = (UpdateBuyerParam) param;

			String businessTypeId = updateBuyerParam.getBusinessTypeId();
			if (!StringUtils.isEmpty(businessTypeId)) {
				ArgValidator.checkFieldUriType(businessTypeId, BusinessType.class, "business_type_id");
				BusinessType businessType = dbClient.queryObject(BusinessType.class, businessTypeId);
				ArgValidator.checkEntityDisapprovedOrBlacklisted(businessType, businessTypeId, false);
				buyer.setBusinessType(businessType);
			}
			dealer = buyer;

		}

		String orgName = param.getOrganizationName();
		if (!StringUtils.isEmpty(orgName)) {
			if (!orgName.equals(dealer.getName())) {
				QueryFields dealerNameField = new QueryFields("name", orgName);
				Dealer tempDealer = dbClient.queryObjectByFields(dealer.getClass(), dealerNameField);
				if (tempDealer != null) {
					throw BadRequestException.nameAlreadyExists(dealer.getClass().getSimpleName(), orgName);
				}
				dealer.setName(orgName);
			}
		}

		String description = param.getDescription();
		dealer.setDescription(description);

		String contactName = param.getContactName();
		if (!StringUtils.isEmpty(contactName)) {
			dealer.setContactName(contactName);
		}

		String address = param.getAddress();
		if (!StringUtils.isEmpty(param.getAddress())) {
			dealer.setAddress(address);
		}

		String emailAddress = param.getEmailAddress();
		if (!StringUtils.isEmpty(emailAddress)) {
			ArgValidator.checkFieldValue(StringUtils.isEmailAddressValid(emailAddress), "email_address");
			dealer.setEmailAddress(emailAddress);
		}

		String mobileNumber = param.getMobileNumber();
		if (!StringUtils.isEmpty(mobileNumber)) {
			ArgValidator.checkFieldValue(StringUtils.isPhoneNumberValid(mobileNumber), "mobile_number");
			dealer.setMobileNumber(mobileNumber);
		}

		String pan = param.getPan();
		if (!StringUtils.isEmpty(pan)) {
			dealer.setPan(pan);
		}

		String website = param.getWebsite();
		dealer.setWebsite(website);

		return (vendor == null ? buyer : vendor);
	}

	/**
	 * This uses GAE pipeline job to delete the buyer and its children
	 * 
	 * @param buyerId
	 * @param deleteUserAccount
	 * @param forceDelete
	 * @return the aysnc job ID
	 */
	public static String deleteBuyer(String buyerId, boolean deleteUserAccount, boolean forceDelete) {
		String pipelineId = PipelineUtils
				.startPipelineService(new DeleteBuyerJobROOT(buyerId, deleteUserAccount, forceDelete));
		logger.info("Task submitted to delete buyer: {}. Pipeline ID: {}. URL: {}", buyerId, pipelineId,
				PipelineUtils.getPipelineStatusUrl(pipelineId));
		return pipelineId;
	}

	/**
	 * This uses GAE pipeline job to delete the vendor and its children
	 * 
	 * @param buyerId
	 * @param deleteUserAccount
	 * @param forceDelete
	 * @return the aysnc job ID
	 */
	public static String deleteVendor(String vendorId, boolean deleteUserAccount, boolean forceDelete) {

		String pipelineId = PipelineUtils
				.startPipelineService(new DeleteVendorJobROOT(vendorId, deleteUserAccount, forceDelete));
		logger.info("Task submitted to delete vendor: {}. Pipeline ID: {}. URL: {}", vendorId, pipelineId,
				PipelineUtils.getPipelineStatusUrl(pipelineId));
		return pipelineId;

	}

	/**
	 * This uses GAE Push Queues to delete the buyer entity and its children. It is
	 * kept here only for learning purposes only.
	 * 
	 * @param taskHelper
	 * @param vendorId
	 * @param deleteUserAccount
	 * @param forceDelete
	 */
	@Deprecated
	public static void deleteBuyer(TaskHelper taskHelper, String buyerId, boolean deleteUserAccount,
			boolean forceDelete) {
		/*
		 * NOTE: The order of these calls are VERY IMPORTANT
		 */

		Task task = taskHelper.createTask(buyerId, "Delete Purchase Items created by Buyer with ID: [" + buyerId + "]");
		AsyncBuyerDeleteOperationsService.enqueueDeletePurchaseItemsTask(buyerId, forceDelete, task);
		task = taskHelper.createTask(buyerId, "Delete Product Enquiries created by Buyer with ID: [" + buyerId + "]",
				task);
		AsyncBuyerDeleteOperationsService.enqueueDeleteProductEnquiriesTask(buyerId, forceDelete, task);
		task = taskHelper.createTask(buyerId, "Delete Reviews of Buyer with ID: [" + buyerId + "]", task);
		AsyncBuyerDeleteOperationsService.enqueueDeleteReviewsTask(buyerId, forceDelete, task);
		task = taskHelper.createTask(buyerId, "Delete Vendor Reviews by Buyer with ID: [" + buyerId + "]", task);
		AsyncBuyerDeleteOperationsService.enqueueDeleteReviewsInVendorReviewsTask(buyerId, forceDelete, task);
		task = taskHelper.createTask(buyerId, "Delete Product Reviews by Buyer with ID: [" + buyerId + "]", task);
		AsyncBuyerDeleteOperationsService.enqueueDeleteReviewsInProductReviewsTask(buyerId, forceDelete, task);
		task = taskHelper.createTask(buyerId, "Delete Bidding Subscription Usages of Buyer with ID: [" + buyerId + "]",
				task);
		AsyncBuyerDeleteOperationsService.enqueueDeleteBiddingSubscriptionUsagesTask(buyerId, forceDelete, task);
		task = taskHelper.createTask(buyerId, "Delete Bidding Subscriptions of Buyer with ID: [" + buyerId + "]", task);
		AsyncBuyerDeleteOperationsService.enqueueDeleteBiddingSubscriptionsTask(buyerId, forceDelete, task);
		task = taskHelper.createTask(buyerId, "Delete Tenders of Buyer with ID: [" + buyerId + "]", task);
		AsyncBuyerDeleteOperationsService.enqueueDeleteTendersTask(buyerId, forceDelete, task);
		task = taskHelper.createTask(buyerId, "Delete Payment Transactions of Buyer with ID: [" + buyerId + "]", task);
		AsyncBuyerDeleteOperationsService.enqueueDeletePaymentTransactionsTask(buyerId, forceDelete, task);
		task = taskHelper.createTask(buyerId, "Delete Buyer entity with ID: [" + buyerId + "]", task);
		AsyncBuyerDeleteOperationsService.enqueueDeleteBuyerTask(buyerId, deleteUserAccount, task);
	}

	/**
	 * This uses GAE Push Queues to delete the vendor entity and its children. It is
	 * kept here only for learning purposes only.
	 * 
	 * @param taskHelper
	 * @param vendorId
	 * @param deleteUserAccount
	 * @param forceDelete
	 */
	@Deprecated
	public static void deleteVendor(TaskHelper taskHelper, String vendorId, boolean deleteUserAccount,
			boolean forceDelete) {
		/*
		 * NOTE: The order of these calls are VERY IMPORTANT
		 */
		Task task = taskHelper.createTask(vendorId,
				"Nullify Vendors in Purchase Items linked Vendor with ID: [" + vendorId + "]");
		AsyncVendorDeleteOperationsService.enqueueNullifyVendorInPurchaseItemsTask(vendorId, forceDelete, task);
		task = taskHelper.createTask(vendorId, "Delete Products created by Vendor with ID: [" + vendorId + "]", task);
		AsyncVendorDeleteOperationsService.enqueueDeleteProductsTask(vendorId, forceDelete, task);
		task = taskHelper.createTask(vendorId, "Delete reviews Vendor with ID: [" + vendorId + "]", task);
		AsyncVendorDeleteOperationsService.enqueueDeleteReviewsTask(vendorId, forceDelete, task);
		task = taskHelper.createTask(vendorId, "Delete buyer reviews posted by Vendor with ID: [" + vendorId + "]",
				task);
		AsyncVendorDeleteOperationsService.enqueueDeleteReviewsInBuyerReviewsTask(vendorId, forceDelete, task);
		task = taskHelper.createTask(vendorId,
				"Delete bidding subscription usages of Vendor with ID: [" + vendorId + "]", task);
		AsyncVendorDeleteOperationsService.enqueueDeleteBiddingSubscriptionUsagesTask(vendorId, forceDelete, task);
		task = taskHelper.createTask(vendorId, "Delete bidding subscriptions of Vendor with ID: [" + vendorId + "]",
				task);
		AsyncVendorDeleteOperationsService.enqueueDeleteBiddingSubscriptionsTask(vendorId, forceDelete, task);
		task = taskHelper.createTask(vendorId, "Delete tender bids by Vendor with ID: [" + vendorId + "]", task);
		AsyncVendorDeleteOperationsService.enqueueDeleteTenderBidsTask(vendorId, forceDelete, task);
		task = taskHelper.createTask(vendorId, "Delete payment transactions of Vendor with ID: [" + vendorId + "]",
				task);
		AsyncVendorDeleteOperationsService.enqueueDeletePaymentTransactionsTask(vendorId, forceDelete, task);
		task = taskHelper.createTask(vendorId, "Delete Vendor entity with ID: [" + vendorId + "]", task);
		AsyncVendorDeleteOperationsService.enqueueDeleteVendorTask(vendorId, deleteUserAccount, task);

	}

	public static Document createSearchDocumentForBuyer(Buyer buyer) {

		String buyerName = buyer.getName();
		String contactName = buyer.getContactName();
		String address = buyer.getAddress();
		String businessType = buyer.getBusinessType().getName();

		Builder buyerDocBuilder = Document.newBuilder();
		buyerDocBuilder.setId(buyer.getId());
		buyerDocBuilder.addField(SearchAPIUtility.createTextField("organizationName", buyerName))
				.addField(SearchAPIUtility.createTextField("address", address))
				.addField(SearchAPIUtility.createTextField("businessType", buyer.getBusinessType().getName()))
				.addField(SearchAPIUtility.createTextField("contactName", contactName))
				.addField(SearchAPIUtility.createTextField("emailAddress", buyer.getMobileNumber()))
				.addField(SearchAPIUtility.createTextField("mobileNumber", buyer.getEmailAddress()))
				.addField(SearchAPIUtility.createTextField("searchTokens1",
						StringUtils.getWordTokensAsString(buyerName, 3)))
				.addField(SearchAPIUtility.createTextField("searchTokens2",
						StringUtils.getWordTokensAsString(businessType, 3)))
				.addField(SearchAPIUtility.createTextField("searchTokens3",
						StringUtils.getWordTokensAsString(address, 3)))
				.addField(SearchAPIUtility.createTextField("searchTokens4",
						StringUtils.getWordTokensAsString(contactName, 3)));

		return buyerDocBuilder.build();

	}

	public static Document createSearchDocumentForVendor(Vendor vendor) {

		String vendorName = vendor.getName();
		String contactName = vendor.getContactName();
		String address = vendor.getAddress();
		String cities = vendor.getCities();

		Builder vendorDocBuilder = Document.newBuilder();
		vendorDocBuilder.setId(vendor.getId());
		vendorDocBuilder.addField(SearchAPIUtility.createTextField("organizationName", vendorName))
				.addField(SearchAPIUtility.createTextField("address", address))
				.addField(SearchAPIUtility.createTextField("cities", cities))
				.addField(SearchAPIUtility.createTextField("contactName", vendor.getContactName()))
				.addField(SearchAPIUtility.createTextField("emailAddress", vendor.getMobileNumber()))
				.addField(SearchAPIUtility.createTextField("mobileNumber", vendor.getEmailAddress()))
				.addField(SearchAPIUtility.createTextField("searchTokens1",
						StringUtils.getWordTokensAsString(vendorName, 3)))
				.addField(SearchAPIUtility.createTextField("searchTokens2",
						StringUtils.getWordTokensAsString(address, 3)))
				.addField(
						SearchAPIUtility.createTextField("searchTokens3", StringUtils.getWordTokensAsString(cities, 3)))
				.addField(SearchAPIUtility.createTextField("searchTokens4",
						StringUtils.getWordTokensAsString(contactName, 3)));
		return vendorDocBuilder.build();

	}
}
