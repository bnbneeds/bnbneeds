package com.bnbneeds.app.api.service.pipeline.buyer;

import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.VendorReview;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeleteReviewsOfVendorsJob extends Job2<Void, String, Boolean> {

	private static final Logger logger = LoggerFactory.getLogger(DeleteReviewsOfVendorsJob.class);

	private static final long serialVersionUID = -5955033856821270875L;

	@Override
	public Value<Void> run(String buyerId, Boolean forceDelete) throws Exception {
		Buyer buyer = DeleteBuyerJobROOT.queryBuyer(buyerId);
		if (buyer == null) {
			return null;
		}

		List<VendorReview> vendorReviews = queryList(VendorReview.class, new QueryFields("reviewedBy", buyer));

		if (!CollectionUtils.isEmpty(vendorReviews)) {

			if (!forceDelete) {
				throw DeleteDealerJob.cannotDeleteHasReferences(buyerId, Buyer.class, VendorReview.class);
			}
			DBOperationList reviewDeleteOperationList = new DBOperationList();

			logger.info("Get the vendor reviews of the buyer [{}] and delete them...", buyerId);
			for (Iterator<VendorReview> iterator = vendorReviews.iterator(); iterator.hasNext();) {
				VendorReview vendorReview = iterator.next();
				reviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, vendorReview);
			}
		}
		return null;
	}
}
