package com.bnbneeds.app.api.service.controller;

import static com.bnbneeds.app.api.service.response.DBObjectMapper.toNamedRelatedListResponse;

import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.exceptions.APIException;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.UserAccountResponseMapper;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.CreateUserAccountParam;
import com.bnbneeds.app.model.account.UpdateAccountStatusParam;
import com.bnbneeds.app.model.account.UserAccountInfoListResponse;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.model.account.UserAccountListResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/admin-accounts")
public class AdminAccountService extends ResourceService {

	private static final Logger logger = LoggerFactory
			.getLogger(AdminAccountService.class);

	private static final String SUPER_ADMIN_ACCOUNT_NAME = "BNBNeeds";
	private static final String SUPER_ADMIN_DEFAULT_PASSWORD = "omnipotent";
	private static final String ADMIN_DEFAULT_PASSWORD = "ChangeMe";
	public static final String SUPER_ADMIN_USERNAME = "root";

	@PostConstruct
	public void createRootUserIfNotPresent() {

		logger.info("Inside createRootUserIfNotPresent()");

		try {
			dbClient.init();
			UserAccount login = loginHelper
					.getUserAccountByUsername(SUPER_ADMIN_USERNAME);
			if (login == null) {
				login = new UserAccount();
				login.setId(UUIDUtil.createId(UserAccount.class));
				login.setAccountName(SUPER_ADMIN_ACCOUNT_NAME);
				login.setName(SUPER_ADMIN_USERNAME);
				login.setRole(Role.SUPER_ADMIN);
				login.setDisabled(false);
				String passkey = UUID.randomUUID().toString();
				String encryptedPassword = passwordEncoder.encrypt(passkey,
						SUPER_ADMIN_DEFAULT_PASSWORD);
				login.setPassKey(passkey);
				login.setEncryptedPassword(encryptedPassword);
				dbClient.createObject(login);
				logger.info("Created a new user for " + Role.SUPER_ADMIN + ": "
						+ SUPER_ADMIN_USERNAME);
			}
		} finally {
			dbClient.destroy();
		}
	}

	@RequestMapping(method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.SUPER_ADMIN + "')")
	public ResponseEntity<CreateResourceResponse> createAdminUserAccount(
			@RequestBody CreateUserAccountParam accountParam)
			throws APIException {

		logger.info("API request received to register a new user account: {}",
				accountParam);

		String username = accountParam.getUsername();
		String password = accountParam.getPassword();
		String accountName = accountParam.getAccountName();
		ArgValidator.checkFieldNotEmpty(accountName, "account_name");
		ArgValidator.checkFieldNotEmpty(username, "email_address");
		ArgValidator.checkFieldValue(StringUtils.isEmailAddressValid(username),
				"email_address");
		// ArgValidator.checkFieldNotEmpty(password, "password");
		String mobileNumber = accountParam.getMobileNumber();
		ArgValidator.checkFieldNotEmpty(mobileNumber, "mobile_number");
		ArgValidator.checkFieldValue(
				StringUtils.isPhoneNumberValid(mobileNumber), "mobile_number");

		UserAccount userLogin = loginHelper.getUserAccountByUsername(username);
		if (userLogin != null) {
			logger.error("Account with username: {} already exists.", username);
			throw BadRequestException
					.nameAlreadyExists("User account with email address");
		}

		if (StringUtils.isEmpty(password)) {
			password = ADMIN_DEFAULT_PASSWORD;
		}

		userLogin = new UserAccount();
		String id = UUIDUtil.createId(UserAccount.class);
		userLogin.setId(id);
		userLogin.setName(username);
		userLogin.setAccountName(accountName);
		userLogin.setRole(Role.ADMIN);
		userLogin.setMobileNumber(mobileNumber);
		String passKey = UUID.randomUUID().toString();
		userLogin.setPassKey(passKey);
		String encodedPassword = passwordEncoder.encrypt(passKey, password);
		userLogin.setEncryptedPassword(encodedPassword);
		userLogin.setClientIPAddress(httpRequest.getRemoteAddr());
		userLogin.setEmailAddressVerified(false);
		userLogin.setMobileNumberVerified(false);
		userLogin.setDisabled(false);

		logger.debug("New user account record creating in DB: {}", userLogin);
		dbClient.createObject(userLogin);

		logger.info("New user account persisted in DB: {}", id);

		return TaskResponseUtility.createResourceResponse(id,
				Endpoint.USER_ACCOUNT_INFO,
				"New user account created successfully");

	}

	@RequestMapping(method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.SUPER_ADMIN + "')")
	public UserAccountListResponse getAdminList(
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset)
			throws APIException {

		logger.info(
				"Request received to get user accounts by filter: {}, fetchSize: {}, offset: {}",
				filter, fetchSize, offset);

		final String[] QUERY_FILTER_KEYS = { "name" };
		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);
		QueryFields fields = new QueryFields("role", Role.ADMIN);
		for (String key : keyValueMap.keySet()) {
			switch (key) {
			case "name":
				fields.add(key, keyValueMap.get(key));
				break;
			default:
				break;
			}
		}

		UserAccountListResponse resp = new UserAccountListResponse();
		/*
		 * ResultSet<UserAccount> resultSet = getResultSet(UserAccount.class,
		 * fields, offset, fetchSize);
		 */

		ResultSetIterator<UserAccount> resultSetIterator = getResultSetIterator(
				UserAccount.class, fields, offset, fetchSize);

		resp.setNamedRelatedResourceList(toNamedRelatedListResponse(
				Endpoint.ADMIN_USER_ACCOUNT_INFO, resultSetIterator));
		resp.setNextOffset(resultSetIterator.getOffset());

		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/bulk", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.SUPER_ADMIN + "')")
	public UserAccountInfoListResponse getAdminInfoList(
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset)
			throws APIException {

		logger.info(
				"Request received to get user accounts by filter: {}, fetchSize: {}, offset: {}",
				filter, fetchSize, offset);

		final String[] QUERY_FILTER_KEYS = { "name" };
		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter,
				QUERY_FILTER_KEYS);
		QueryFields fields = new QueryFields("role", Role.ADMIN);
		for (String key : keyValueMap.keySet()) {
			switch (key) {
			case "name":
				fields.add(key, keyValueMap.get(key));
				break;
			default:
				break;
			}
		}

		UserAccountInfoListResponse resp = new UserAccountInfoListResponse();

		ResultSetIterator<UserAccount> resultSetIterator = getResultSetIterator(
				UserAccount.class, fields, offset, fetchSize);

		if (resultSetIterator != null) {
			while (resultSetIterator.hasNext()) {
				UserAccount dataObject = resultSetIterator.next();
				UserAccountInfoResponse objectResponse = new UserAccountInfoResponse();
				objectResponse = UserAccountResponseMapper.map(dataObject);
				resp.add(objectResponse);
			}
		}
		resp.setNextOffset(resultSetIterator.getOffset());

		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.SUPER_ADMIN + "')")
	public UserAccountInfoResponse getAdmin(@PathVariable("id") String id)
			throws APIException {

		logger.info("Request received to get user account info : {}", id);
		ArgValidator.checkFieldUriType(id, UserAccount.class, "id");

		UserAccount account = dbClient.queryObject(UserAccount.class, id);

		ArgValidator.checkEntityNotNull(account, id, true);

		if (!Role.ADMIN.equals(account.getRole())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		UserAccountInfoResponse resp = UserAccountResponseMapper.map(account);

		return resp;

	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}/userInfo", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.ADMIN + "')")
	public UserAccountInfoResponse getUserInfo(@PathVariable("id") String id)
			throws APIException {

		logger.info("Request received to get user account info : {}", id);
		ArgValidator.checkFieldUriType(id, UserAccount.class, "id");

		UserAccount account = dbClient.queryObject(UserAccount.class, id);

		ArgValidator.checkEntityNotNull(account, id, true);

		// if (Role.ADMIN.equals(account.getRole())) {
		// throw ForbiddenException.unauthorizedAccess();
		// }

		UserAccountInfoResponse resp = UserAccountResponseMapper.map(account);

		return resp;

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.SUPER_ADMIN + "')")
	public ResponseEntity<TaskResponse> deleteAdmin(
			@PathVariable("id") String id) throws APIException {

		logger.info("Request received to delete user account: {}", id);
		ArgValidator.checkFieldUriType(id, UserAccount.class, "id");

		UserAccount account = dbClient.queryObject(UserAccount.class, id);

		ArgValidator.checkEntityNotNull(account, id, true);

		if (!Role.ADMIN.equals(account.getRole())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		dbClient.markAsDeleted(account);

		logger.info("User account deleted successfully: {}", id);

		return TaskResponseUtility
				.createTaskSuccessResponse("User account deleted successfully");

	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/approvals", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.SUPER_ADMIN + "')")
	public ResponseEntity<TaskResponse> updateAccountStatus(
			@PathVariable("id") String id,
			@RequestBody UpdateAccountStatusParam accountStatusParam)
			throws APIException {

		logger.info("API request received to update user account status: {}",
				accountStatusParam);

		ArgValidator.checkFieldUriType(id, UserAccount.class, "id");
		UserAccount account = dbClient.queryObject(UserAccount.class, id);
		ArgValidator.checkEntityNotNull(account, id, true);

		boolean changed = false;

		if (accountStatusParam.isDisabled()) {
			account.setDisabled(true);
			changed = true;
		} else if (accountStatusParam.isEnabled()) {
			account.setDisabled(false);
			changed = true;
		}

		if (!changed) {
			throw BadRequestException
					.create("Nothing to update in the user account.");
		}

		logger.debug("Saving updated user account Status record in DB: {}.",
				account);
		dbClient.updateObject(account);

		logger.info("Updated user account status persisted in DB: {}", account);

		return TaskResponseUtility
				.createTaskSuccessResponse("User account status updated successfully.");
	}

}
