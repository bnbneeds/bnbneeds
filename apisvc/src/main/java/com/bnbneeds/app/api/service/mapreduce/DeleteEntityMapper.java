package com.bnbneeds.app.api.service.mapreduce;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.tools.mapreduce.DatastoreMutationPool;
import com.google.appengine.tools.mapreduce.MapOnlyMapper;

public class DeleteEntityMapper extends MapOnlyMapper<Key, Void> {

	private static final long serialVersionUID = 657105403454678131L;

	private transient DatastoreMutationPool batcher;

	public DeleteEntityMapper() {

	}

	@Override
	public void beginSlice() {
		batcher = DatastoreMutationPool.create();
	}

	@Override
	public void endSlice() {
		batcher.flush();
	}

	@Override
	public void map(Key key) {
		batcher.delete(key);
	}
}
