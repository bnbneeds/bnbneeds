package com.bnbneeds.app.api.service.security;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.util.Assert;

import com.bnbneeds.app.model.OperationStatus;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.util.StringUtils;

public class BasicAuthenticationEntryGate extends
		LoginAndAccessRequestResponseHelper implements
		AuthenticationEntryPoint, InitializingBean {
	// ~ Instance fields
	// ================================================================================================

	private static final String SOURCE_HEADER_NAME = "Request-Source";
	private String realmName;
	private List<String> acceptableSources;

	// ~ Methods
	// ========================================================================================================

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.hasText(realmName, "realmName must be specified");
	}

	@Override
	public void commence(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException authException)
			throws IOException, ServletException {

		String headerValue = request.getHeader(SOURCE_HEADER_NAME);
		if (!hasHeaderValue(headerValue, acceptableSources)) {
			response.addHeader("WWW-Authenticate", "Basic realm=\"" + realmName
					+ "\"");
		}
		/*
		 * response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
		 * authException.getMessage());
		 */
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

		TaskResponse taskResponse = new TaskResponse(
				HttpServletResponse.SC_UNAUTHORIZED, OperationStatus.ERROR,
				authException.getMessage(),
				AuthenticationException.class.getSimpleName());

		String acceptedMediaType = getAcceptedMediaType(request);
		setResponse(response, acceptedMediaType, taskResponse);
	}

	private boolean hasHeaderValue(String headerValue,
			List<String> acceptableSources) {

		if (!StringUtils.isEmpty(headerValue)) {
			if (acceptableSources != null && !acceptableSources.isEmpty()) {
				if (acceptableSources.contains(headerValue)) {
					return true;
				}
			}
		}

		return false;
	}

	public String getRealmName() {
		return realmName;
	}

	public void setRealmName(String realmName) {
		this.realmName = realmName;
	}

	public void setAcceptableSources(List<String> acceptableSources) {
		this.acceptableSources = acceptableSources;
	}

}
