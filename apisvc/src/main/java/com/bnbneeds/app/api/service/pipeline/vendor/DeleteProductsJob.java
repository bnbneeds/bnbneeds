package com.bnbneeds.app.api.service.pipeline.vendor;

import static com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob.DELETE_DEALER_JOB_SETTING;
import static com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT.queryVendor;
import static com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT.vendorField;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryObject;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryObjectByFields;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.api.service.util.BlobStoreUtility;
import com.bnbneeds.app.api.service.util.NotificationUtility;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Enquiry;
import com.bnbneeds.app.db.client.model.Notification;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.ProductAverageRating;
import com.bnbneeds.app.db.client.model.ProductOffer;
import com.bnbneeds.app.db.client.model.ProductReview;
import com.bnbneeds.app.db.client.model.Vendor;
import com.google.appengine.tools.pipeline.FutureValue;
import com.google.appengine.tools.pipeline.Job0;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeleteProductsJob extends Job2<Void, String, Boolean> {

	private static final long serialVersionUID = -5444739413519675217L;
	private static final Logger logger = LoggerFactory.getLogger(DeleteProductsJob.class);

	@Override
	public Value<Void> run(String vendorId, Boolean forceDelete) throws Exception {
		Vendor vendor = queryVendor(vendorId);
		if (vendor == null) {
			return null;
		}
		Boolean forceDeleteFlag = new Boolean(forceDelete);
		List<Product> vendorProducts = queryList(Product.class, vendorField(vendor));

		if (!CollectionUtils.isEmpty(vendorProducts)) {

			if (!forceDeleteFlag) {
				throw DeleteDealerJob.cannotDeleteHasReferences(vendorId, Vendor.class, Product.class);
			}
			logger.info("Get the products of the vendor [{}] and delete them...", vendorId);
			FutureValue<Void> deleteProductJob = null;
			for (Iterator<Product> productIterator = vendorProducts.iterator(); productIterator.hasNext();) {
				Product product = productIterator.next();
				String productId = product.getId();
				if (deleteProductJob != null) {
					deleteProductJob = futureCall(new DeleteSingleProductJob(productId, forceDelete),
							waitFor(deleteProductJob), DELETE_DEALER_JOB_SETTING);
				} else {
					deleteProductJob = futureCall(new DeleteSingleProductJob(productId, forceDelete),
							DELETE_DEALER_JOB_SETTING);
				}

			}
		}
		return null;
	}

	public static class DeleteSingleProductJob extends Job0<Void> {

		private static final long serialVersionUID = 1291497271319376649L;

		private String productId;
		private Boolean notifyBuyersAfterDelete;
		private Boolean forceDelete;

		public DeleteSingleProductJob(String productId, Boolean forceDelete) {
			super();
			this.productId = productId;
			this.forceDelete = forceDelete;
			this.notifyBuyersAfterDelete = false;
		}

		public DeleteSingleProductJob(String productId, Boolean notifyBuyersAfterDelete, Boolean forceDelete) {
			super();
			this.productId = productId;
			this.notifyBuyersAfterDelete = notifyBuyersAfterDelete;
			this.forceDelete = forceDelete;
		}

		@Override
		public String getJobDisplayName() {
			return "Delete Product with ID: " + productId;
		}

		@Override
		public Value<Void> run() throws Exception {

			Product product = queryObject(Product.class, productId);

			DBOperationList productOfferDeleteOperationList = new DBOperationList();
			DBOperationList productEnquiryDeleteOperationList = new DBOperationList();
			DBOperationList productReviewDeleteOperationList = new DBOperationList();
			DBOperationList productDeleteOperationList = new DBOperationList();
			List<String> imageKeys = new ArrayList<>();

			QueryFields productField = new QueryFields("product", product);
			List<ProductOffer> productOffers = queryList(ProductOffer.class, productField);
			if (!CollectionUtils.isEmpty(productOffers)) {

				if (!forceDelete) {
					throw DeleteDealerJob.cannotDeleteHasReferences(productId, Product.class, ProductOffer.class);
				}

				logger.info("Get the offer for the products and delete them...");
				for (Iterator<ProductOffer> iterator = productOffers.iterator(); iterator.hasNext();) {
					ProductOffer productOffer = iterator.next();
					productOfferDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, productOffer);
				}
			}

			List<Enquiry> productEnquiries = queryList(Enquiry.class, productField);
			if (!CollectionUtils.isEmpty(productEnquiries)) {

				if (!forceDelete) {
					throw DeleteDealerJob.cannotDeleteHasReferences(productId, Product.class, Enquiry.class);
				}

				logger.info("Get the enquiries of the products and delete them...");
				for (Iterator<Enquiry> iterator = productEnquiries.iterator(); iterator.hasNext();) {
					Enquiry productEnquiry = iterator.next();
					productEnquiryDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, productEnquiry);
				}
			}

			// Delete product reviews
			List<ProductReview> productReviews = queryList(ProductReview.class, productField);
			if (!CollectionUtils.isEmpty(productReviews)) {

				if (!forceDelete) {
					throw DeleteDealerJob.cannotDeleteHasReferences(productId, Product.class, ProductReview.class);
				}

				logger.info("Get the review of the products and delete them...");
				for (Iterator<ProductReview> iterator = productReviews.iterator(); iterator.hasNext();) {
					ProductReview productReview = iterator.next();
					productReviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, productReview);
				}

				logger.info("Get the products and delete them...");
				ProductAverageRating productAverageRating = queryObjectByFields(ProductAverageRating.class,
						productField);
				if (productAverageRating != null) {
					productDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, productAverageRating);
				}
			}

			Set<String> images = product.getImageKeys();
			if (images != null && !images.isEmpty()) {
				imageKeys.addAll(images);
			}
			productDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, product);

			transact(productOfferDeleteOperationList);
			transact(productEnquiryDeleteOperationList);
			transact(productReviewDeleteOperationList);
			transact(productDeleteOperationList);
			if (imageKeys != null && !imageKeys.isEmpty()) {
				logger.debug("Deleting product images...");
				BlobStoreUtility.deleteBlobs(imageKeys);
			}

			if (notifyBuyersAfterDelete) {
				Notification notification = NotificationUtility.createNotification(SystemEventType.PRODUCT_DELETED,
						RecipientType.ALL_BUYERS, null, productId, product.getProductName().getName(), null,
						product.getVendor().getName());
				DBOperationList opList = new DBOperationList();
				opList.addOperation(OperationType.INSERT, notification);
				transact(opList);
			}
			return null;
		}
	}
}
