package com.bnbneeds.app.api.service.pipeline.buyer;

import static com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob.DELETE_DEALER_JOB_SETTING;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryObject;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.Tender;
import com.bnbneeds.app.db.client.model.TenderBid;
import com.bnbneeds.app.db.client.model.TenderProduct;
import com.google.appengine.tools.pipeline.FutureValue;
import com.google.appengine.tools.pipeline.ImmediateValue;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeleteTendersJob extends Job2<Void, String, Boolean> {

	private static final Logger logger = LoggerFactory.getLogger(DeleteTendersJob.class);

	private static final long serialVersionUID = 1932410515446158503L;

	@Override
	public Value<Void> run(String buyerId, Boolean forceDelete) throws Exception {
		Buyer buyer = DeleteBuyerJobROOT.queryBuyer(buyerId);
		if (buyer == null) {
			return null;
		}
		List<Tender> tenders = queryList(Tender.class, DeleteBuyerJobROOT.buyerField(buyer));

		if (!CollectionUtils.isEmpty(tenders)) {
			if (!forceDelete) {
				throw DeleteDealerJob.cannotDeleteHasReferences(buyerId, Buyer.class, Tender.class);
			}

			Value<Boolean> forceDeleteInput = new ImmediateValue<>(forceDelete);

			logger.info("Get the tenders of the buyer [{}] and delete them...", buyerId);
			FutureValue<Void> deleteTenderJob = null;
			for (Iterator<Tender> iterator = tenders.iterator(); iterator.hasNext();) {
				Tender tender = iterator.next();
				String tenderId = tender.getId();
				if (deleteTenderJob != null) {
					deleteTenderJob = futureCall(new DeleteSingleTenderJob(), new ImmediateValue<>(tenderId),
							forceDeleteInput, waitFor(deleteTenderJob), DELETE_DEALER_JOB_SETTING);
				} else {
					deleteTenderJob = futureCall(new DeleteSingleTenderJob(), new ImmediateValue<>(tenderId),
							forceDeleteInput, DELETE_DEALER_JOB_SETTING);
				}
			}
		}
		return null;
	}

	public static class DeleteSingleTenderJob extends Job2<Void, String, Boolean> {

		private static final long serialVersionUID = 6289321063046586517L;

		@Override
		public Value<Void> run(String tenderId, Boolean forceDelete) throws Exception {
			Tender tender = queryObject(Tender.class, tenderId);
			if (tender == null) {
				return null;
			}
			QueryFields tenderField = new QueryFields("tender", tender);
			DBOperationList bidProductDeleteOperationList = new DBOperationList();
			DBOperationList bidDeleteOperationList = new DBOperationList();

			List<TenderProduct> bidProducts = queryList(TenderProduct.class, tenderField);
			if (!CollectionUtils.isEmpty(bidProducts)) {
				if (!forceDelete) {
					throw DeleteDealerJob.cannotDeleteHasReferences(tenderId, Tender.class, TenderProduct.class);
				}

				logger.info("Get the bid products of the tender [{}] and delete them...", tenderId);

				for (Iterator<TenderProduct> iterator = bidProducts.iterator(); iterator.hasNext();) {
					TenderProduct bidProduct = iterator.next();
					bidProductDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, bidProduct);
				}

			}

			List<TenderBid> bids = queryList(TenderBid.class, tenderField);
			if (!CollectionUtils.isEmpty(bids)) {
				if (!forceDelete) {
					throw DeleteDealerJob.cannotDeleteHasReferences(tenderId, Tender.class, TenderBid.class);
				}

				logger.info("Get the bids of the tender [{}] and delete them...", tenderId);
				for (Iterator<TenderBid> iterator = bids.iterator(); iterator.hasNext();) {
					TenderBid bid = iterator.next();
					bidDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, bid);
				}
			}

			bidDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, tender);

			transact(bidProductDeleteOperationList);
			transact(bidDeleteOperationList);
			return null;
		}
	}
}
