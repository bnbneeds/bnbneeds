package com.bnbneeds.app.api.service.pipeline.dealer;

import com.bnbneeds.app.api.service.pipeline.Constants;
import com.bnbneeds.app.api.service.pipeline.JobFailureException;
import com.bnbneeds.app.db.client.model.DataObject;
import com.google.appengine.tools.pipeline.Job0;
import com.google.appengine.tools.pipeline.JobSetting;

public abstract class DeleteDealerJob extends Job0<Void> {

	private static final long serialVersionUID = -2646919006379091860L;

	public static final JobSetting DELETE_DEALER_JOB_SETTING = new JobSetting.OnQueue(Constants.DELETE_DELAER_QUEUE);

	public static JobFailureException cannotDeleteHasReferences(String id, Class<? extends DataObject> clazz,
			Class<? extends DataObject> referencedClass) {
		String message = String.format("Cannot delete %1$s with id: %2$s. It is referenced by records of %3$s.", id,
				clazz.getSimpleName(), referencedClass.getSimpleName());
		throw new JobFailureException(message);
	}

}
