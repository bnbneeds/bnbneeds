package com.bnbneeds.app.api.service.event;

public enum RecipientType {
	
	EVERYONE, ALL_VENDORS,VENDORS_AND_ADMIN, ALL_BUYERS,BUYERS_AND_ADMIN, VENDOR, BUYER, ADMIN, SUPER_ADMIN;

}
