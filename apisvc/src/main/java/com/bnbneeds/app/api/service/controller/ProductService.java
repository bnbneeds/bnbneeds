package com.bnbneeds.app.api.service.controller;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.exceptions.ResourceNotFoundException;
import com.bnbneeds.app.api.service.response.ProductResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.BlobStoreUtility;
import com.bnbneeds.app.api.service.util.ProductUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.BlobObject;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.ProductCategory;
import com.bnbneeds.app.db.client.model.ProductName;
import com.bnbneeds.app.db.client.model.ProductType;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.product.ProductImageListResponse;
import com.bnbneeds.app.model.product.ProductInfoListResponse;
import com.bnbneeds.app.model.product.ProductInfoResponse;

@RestController
@RequestMapping(value = "/products")
public class ProductService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(ProductService.class);

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "', '" + Role.ADMIN + "')  and hasDealership()")
	public ProductInfoResponse getProduct(@PathVariable("id") String id, @AuthenticationPrincipal BNBNeedsUser user) {

		logger.info("Request received to get product info of id: {}", id);

		ArgValidator.checkFieldUriType(id, Product.class, "product_id");

		Product product = dbClient.queryObject(Product.class, id);

		ArgValidator.checkEntityBlacklisted(product, id, true);
		ProductUtility.checkProductOwnership(user, product);

		ProductInfoResponse response = ProductResponseMapper.mapProduct(product);

		return response;

	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "', '" + Role.ADMIN + "')  and hasDealership()")
	public ProductInfoListResponse searchProducts(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to search products by filter: {}, fetchSize: {}, offset: {}", filter, fetchSize,
				offset);

		final String[] QUERY_FILTER_KEYS = { "name", "type", "productNameId", "productTypeId", "productCategoryId",
				"vendorId", "category", "minCostPrice", "maxCostPrice" };

		ProductInfoListResponse response = new ProductInfoListResponse();

		QueryFields fields = new QueryFields();

		if (user.isVendor()) {
			UserAccount userLogin = user.getAccount();
			Vendor vendor = dbClient.queryObject(Vendor.class, userLogin.getRelationshipId());

			ArgValidator.checkEntityBlacklisted(vendor, vendor.getId(), false);

			if (vendor != null) {
				fields.add("vendor", vendor);
			}
		}

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		for (String key : keyValueMap.keySet()) {

			String value = keyValueMap.get(key);

			switch (key) {
			case "vendorId":
				if (user.isBuyer()) {
					ArgValidator.checkFieldUriType(value, Vendor.class, key);
					Vendor vendor = dbClient.queryObject(Vendor.class, value);
					ArgValidator.checkEntityBlacklisted(vendor, value, false);
					fields.add("vendor", vendor);
				}
				break;
			case "productNameId":
				ArgValidator.checkFieldUriType(value, ProductName.class, key);
				ProductName productNameByIdEntity = dbClient.queryObject(ProductName.class, value);
				if (productNameByIdEntity != null) {
					fields.add("productName", productNameByIdEntity);
				}
				break;
			case "name":
				ProductName productNameEntity = dbClient.queryObjectByFields(ProductName.class,
						new QueryFields("name", value));
				if (productNameEntity != null) {
					fields.add("productName", productNameEntity);
				}
				break;
			/*
			 * case "status":
			 * ArgValidator.checkFieldValue(!EntityStatus.BLACKLISTED.name().equals(value.
			 * toUpperCase()), key); fields.add("entityStatus", value.toUpperCase()); break;
			 */
			case "type":
				ProductType productTypeEntity = dbClient.queryObjectByFields(ProductType.class,
						new QueryFields("name", value));
				if (productTypeEntity != null) {
					fields.add("productType", productTypeEntity);
				}
				break;
			case "productTypeId":
				ArgValidator.checkFieldUriType(value, ProductType.class, key);
				ProductType productTypeByIdEntity = dbClient.queryObject(ProductType.class, value);
				if (productTypeByIdEntity != null) {
					fields.add("productType", productTypeByIdEntity);
				}
				break;
			case "category":
				ProductCategory productCategoryEntity = dbClient.queryObjectByFields(ProductCategory.class,
						new QueryFields("name", value));
				if (productCategoryEntity != null) {
					fields.add("productCategory", productCategoryEntity);
				}
				break;
			case "productCategoryId":
				ArgValidator.checkFieldUriType(value, ProductCategory.class, key);
				ProductCategory productCategoryByIdEntity = dbClient.queryObject(ProductCategory.class, value);
				if (productCategoryByIdEntity != null) {
					fields.add("productCategory", productCategoryByIdEntity);
				}
				break;
			case "minCostPrice":
				fields.add("costPrice >=", Double.valueOf(value));
				break;
			case "maxCostPrice":
				fields.add("costPrice <=", Double.valueOf(value));
				break;
			default:
				break;
			}
		}

		ResultSetIterator<Product> resultSetIterator = null;

		do {

			resultSetIterator = getResultSetIterator(Product.class, fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					Product product = resultSetIterator.next();
					if (product != null) {
						if (user.isDealer()) {
							if (product.isBlacklisted()) {
								continue;
							}
							if (product.getVendor().isBlacklisted()) {
								continue;
							}
						}
						ProductInfoResponse infoResponse = ProductResponseMapper.mapProduct(product);
						response.addProductInfoResponse(infoResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());

				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (response.size() < fetchSize);

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}/images", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "', '" + Role.ADMIN + "')  and hasDealership()")
	public ProductImageListResponse getProductImageLinks(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String productId) {

		logger.info("Request received to get image(s) of product id: {}", productId);

		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");

		Product product = dbClient.queryObject(Product.class, productId);

		ArgValidator.checkEntityBlacklisted(product, productId, true);
		ProductUtility.checkProductOwnership(user, product);

		ProductImageListResponse resp = new ProductImageListResponse();
		resp.setProductId(product.getId());
		if (product.getImages() != null) {
			Set<BlobObject> images = product.getImages();
			for (BlobObject image : images) {
				resp.addImage(image.getBlobKey(), image.getMediaType(), image.getSize());
			}
		}
		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}/images/{imageId}", produces = {
			MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "', '" + Role.ADMIN + "')  and hasDealership()")
	public void getProductImage(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String productId,
			@PathVariable("imageId") String imageKey, HttpServletResponse response) throws IOException {

		logger.info("Request received to get image of product id: {}", productId);

		ArgValidator.checkFieldUriType(productId, Product.class, "product_id");
		ArgValidator.checkFieldNotEmpty(imageKey, "image_id");

		Product product = dbClient.queryObject(Product.class, productId);

		ArgValidator.checkEntityBlacklisted(product, productId, true);

		ProductUtility.checkProductOwnership(user, product);

		if (!product.containsImageKey(imageKey)) {
			throw ResourceNotFoundException.idNotFound(imageKey);
		}

		BlobStoreUtility.serveBlob(imageKey, response);
	}

}
