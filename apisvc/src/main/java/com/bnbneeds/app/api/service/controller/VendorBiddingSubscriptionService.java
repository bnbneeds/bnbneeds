package com.bnbneeds.app.api.service.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.SubscriptionResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.BiddingSubscriptionUtility;
import com.bnbneeds.app.api.service.util.PaymentTransactionUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription.SubscriptionState;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscription;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscriptionPlan;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.CreatePaymentTransactionParam;
import com.bnbneeds.app.model.dealer.subscription.CreateSubscriptionParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionStatusParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionStatusParam.SubscriptionStatus;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionInfoListResponse;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;

@RestController
@RequestMapping(path = "/vendors/{vendorId}/bidding-subscriptions")
public class VendorBiddingSubscriptionService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(VendorBiddingSubscriptionService.class);

	/**
	 * 
	 * Throws {@code ForbiddenException} if the given vendor ID is not the same as
	 * the subscription's buyer ID
	 * 
	 * @param subscription
	 *            the buyer bidding subscription
	 * @param buyerId
	 *            the vendor ID
	 */
	private static void forbidSubscriptionAccessToOtherVendor(VendorBiddingSubscription subscription, String vendorId) {
		if (!subscription.getVendor().getId().equals(vendorId)) {
			throw ForbiddenException.unauthorizedAccess();
		}
	}

	@RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR + "') and isRelated(#vendorId)")
	public ResponseEntity<CreateResourceResponse> createSubscription(@PathVariable("vendorId") String vendorId,
			@RequestBody CreateSubscriptionParam param) {

		logger.info("Request received to create bidding subscription for vendor ID: {}. Request: {}", vendorId, param);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldNotNull(param, "create_subscription");
		String subscriptionPlanId = param.getSubscriptionPlanId();
		ArgValidator.checkFieldUriType(subscriptionPlanId, VendorBiddingSubscriptionPlan.class, "subscription_plan_id");

		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, true);
		/*
		 * Check if already a subscription exists which is either ACTIVE or
		 * ACTIVATION_PENDING. If yes, then throw forbidden exception.
		 */
		final String SUBSCRIPTION_STATE_FIELD = "subscriptionState";
		QueryFields vendorField = new QueryFields("vendor", vendor);
		QueryFields fields = new QueryFields(SUBSCRIPTION_STATE_FIELD, SubscriptionState.ACTIVE.name());
		fields.add(vendorField);
		VendorBiddingSubscription subscription = dbClient.queryObjectByFields(VendorBiddingSubscription.class, fields);

		if (subscription != null) {
			logger.error("Subscription already exists for vendorId: {}. Subscription: {} - {}", vendorId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerSubscriptionAlreadyExists(subscription.getSubscriptionState().name());
		}

		subscription = dbClient.queryObjectByFields(VendorBiddingSubscription.class,
				new QueryFields(SUBSCRIPTION_STATE_FIELD, SubscriptionState.ACTIVATION_IN_PROGRESS.name())
						.add(vendorField));

		if (subscription != null) {
			logger.error("Subscription already exists for vendorId: {}. Subscription: {} - {}", vendorId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerSubscriptionAlreadyExists(subscription.getSubscriptionState().name());
		}

		subscription = dbClient.queryObjectByFields(VendorBiddingSubscription.class,
				new QueryFields(SUBSCRIPTION_STATE_FIELD, SubscriptionState.ACTIVATION_PENDING.name())
						.add(vendorField));

		if (subscription != null) {
			logger.error("Subscription already exists for vendorId: {}. Subscription: {} - {}", vendorId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerSubscriptionAlreadyExists(subscription.getSubscriptionState().name());
		}

		/*
		 * Check if the requested plan is disapproved. If yes, then throw exception.
		 */
		VendorBiddingSubscriptionPlan subscriptionPlan = dbClient.queryObject(VendorBiddingSubscriptionPlan.class,
				subscriptionPlanId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(subscriptionPlan, subscriptionPlanId, false);
		if (!subscriptionPlan.isAvailable()) {
			throw BadRequestException.parameterInvalid("subscription_plan_id",
					"The subscription plan is not available for subscription: [" + subscriptionPlanId + "]");
		}

		if (subscriptionPlan.isDefaultPlan()) {
			throw BadRequestException.parameterInvalid("subscription_plan_id",
					"Cannot create a subscription with a default plan: [" + subscriptionPlanId + "]");
		}

		/*
		 * Everything is OK. Create the subscription.
		 */
		String subscriptionId = UUIDUtil.createId(VendorBiddingSubscription.class);
		subscription = new VendorBiddingSubscription();
		subscription.setId(subscriptionId);
		subscription.setVendor(vendor);
		subscription.setSubscriptionPlan(subscriptionPlan);
		BiddingSubscriptionUtility.initialize(subscription);

		dbClient.createObject(subscription);
		logger.info("Vendor bidding subscription created successfully.");

		return TaskResponseUtility.createResourceResponse(subscriptionId,
				Endpoint.VENDOR_BIDDING_SUBSCRIPTION_INFO.get(vendorId, subscriptionId),
				"Bidding subscription created successfully.");
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/{subscriptionId}", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR + "') and isRelated(#vendorId)")
	public ResponseEntity<TaskResponse> updateSubscription(@PathVariable("vendorId") String vendorId,
			@PathVariable("subscriptionId") String subscriptionId, @RequestBody UpdateSubscriptionParam param) {

		logger.info("Request received to update bidding subscription {} for vendor ID: {}. Request: {}", subscriptionId,
				vendorId, param);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(subscriptionId, VendorBiddingSubscription.class, "subscription_id");
		ArgValidator.checkFieldNotNull(param, "update_subscription_param");
		String subscriptionPlanId = param.getSubscriptionPlanId();
		ArgValidator.checkFieldUriType(subscriptionPlanId, VendorBiddingSubscription.class, "subscription_plan_id");

		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, true);

		VendorBiddingSubscription subscription = dbClient.queryObject(VendorBiddingSubscription.class, subscriptionId);
		ArgValidator.checkEntityNotNull(subscription, subscriptionId, true);

		forbidSubscriptionAccessToOtherVendor(subscription, vendorId);

		BiddingSubscriptionUtility.validateUpdateBiddingSubscriptionRequest(vendorId, subscription);
		/*
		 * Check if the requested plan is disapproved. If yes, then throw exception.
		 */
		VendorBiddingSubscriptionPlan subscriptionPlan = dbClient.queryObject(VendorBiddingSubscriptionPlan.class,
				subscriptionPlanId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(subscriptionPlan, subscriptionPlanId, false);
		if (!subscriptionPlan.isAvailable()) {
			throw BadRequestException.parameterInvalid("subscription_plan_id",
					"The subscription plan is not available for subscription: [" + subscriptionPlanId + "]");
		}

		if (subscriptionPlan.isDefaultPlan()) {
			throw BadRequestException.parameterInvalid("subscription_plan_id",
					"Cannot update a subscription with a default plan: [" + subscriptionPlanId + "]");
		}

		/*
		 * Everything is OK. Update the subscription.
		 */
		subscription.setSubscriptionPlan(subscriptionPlan);
		BiddingSubscriptionUtility.initialize(subscription);

		dbClient.updateObject(subscription);
		logger.info("Vendor bidding subscription updated successfully.");

		return TaskResponseUtility.createTaskSuccessResponse("Bidding subscription updated successfully.");
	}

	@RequestMapping(method = RequestMethod.DELETE, path = "/{subscriptionId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR + "') and isRelated(#vendorId)")
	public ResponseEntity<TaskResponse> deleteSubscription(@PathVariable("vendorId") String vendorId,
			@PathVariable("subscriptionId") String subscriptionId) {

		logger.info("Request received to update bidding subscription {} for vendor ID: {}", subscriptionId, vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(subscriptionId, VendorBiddingSubscription.class, "subscription_id");

		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, true);

		VendorBiddingSubscription subscription = dbClient.queryObject(VendorBiddingSubscription.class, subscriptionId);
		ArgValidator.checkEntityNotNull(subscription, subscriptionId, true);

		forbidSubscriptionAccessToOtherVendor(subscription, vendorId);

		BiddingSubscriptionUtility.validateDeleteBiddingSubscriptionRequest(vendorId, subscription);

		/*
		 * Everything is OK. Update the subscription.
		 */
		dbClient.markAsDeleted(subscription);
		logger.info("Vendor bidding subscription deleted successfully.");

		return TaskResponseUtility.createTaskSuccessResponse("Bidding subscription deleted successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{subscriptionId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "') and isRelated(#vendorId)")
	public VendorBiddingSubscriptionInfoResponse getSubscription(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId, @PathVariable("subscriptionId") String subscriptionId) {

		logger.info("Request received to get bidding subscription {} for vendor ID: {}.", subscriptionId, vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(subscriptionId, VendorBiddingSubscription.class, "subscription_id");

		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityNotNull(vendor, vendorId, true);

		if (user.isVendor()) {
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);
		}

		VendorBiddingSubscription subscription = dbClient.queryObject(VendorBiddingSubscription.class, subscriptionId);
		ArgValidator.checkEntityNotNull(subscription, subscriptionId, true);

		forbidSubscriptionAccessToOtherVendor(subscription, vendorId);

		logger.info("Exit returning subscription details: {}", subscription);

		return SubscriptionResponseMapper.map(subscription, TIMESTAMP_FORMAT);
	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "') and isRelated(#vendorId)")
	public VendorBiddingSubscriptionInfoListResponse getSubscriptions(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get bidding subscriptions for vendor ID: {}.", vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityNotNull(vendor, vendorId, true);

		if (user.isVendor()) {
			ArgValidator.checkEntityBlacklisted(vendor, vendorId, true);
		}
		final String[] QUERY_FILTER_KEYS = { "state", "subscriptionPlanId" };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields("vendor", vendor);

		for (String key : keyValueMap.keySet()) {

			String value = keyValueMap.get(key);

			switch (key) {
			case "state":
				ArgValidator.checkFieldValueFromEnum(value, key, SubscriptionState.class);
				fields.add("subscriptionState", value);
				break;
			case "subscriptionPlanId":
				ArgValidator.checkFieldUriType(value, VendorBiddingSubscriptionPlan.class, key);
				VendorBiddingSubscriptionPlan plan = dbClient.queryObject(VendorBiddingSubscriptionPlan.class, value);
				fields.add("subscriptionPlan", plan);
				break;
			default:
				break;
			}
		}

		VendorBiddingSubscriptionInfoListResponse response = new VendorBiddingSubscriptionInfoListResponse();
		ResultSetIterator<VendorBiddingSubscription> resultSetIterator = null;

		do {
			resultSetIterator = getResultSetIterator(VendorBiddingSubscription.class, fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					VendorBiddingSubscription subscription = resultSetIterator.next();
					if (subscription != null) {
						VendorBiddingSubscriptionInfoResponse infoResponse = SubscriptionResponseMapper
								.map(subscription, TIMESTAMP_FORMAT);
						response.add(infoResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());

				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (response.size() < fetchSize);

		logger.info("Exit retuning response with a list of size: {}", response.size());

		return response;

	}

	@RequestMapping(method = RequestMethod.POST, path = "/{subscriptionId}/payment-transactions", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR + "') and isRelated(#vendorId)")
	public ResponseEntity<CreateResourceResponse> createSubscriptionPaymentTransaction(
			@PathVariable("vendorId") String vendorId, @PathVariable("subscriptionId") String subscriptionId,
			@RequestBody CreatePaymentTransactionParam param) {

		logger.info(
				"Request received to create payment transaction for bidding subscription for vendor ID: {}, subscription ID {}. Request: {}",
				vendorId, subscriptionId, param);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(subscriptionId, VendorBiddingSubscription.class, "subscription_id");

		PaymentTransactionUtility.validatePaymentTransactionParam(param);

		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, true);

		VendorBiddingSubscription subscription = dbClient.queryObject(VendorBiddingSubscription.class, subscriptionId);
		ArgValidator.checkEntityNotNull(subscription, subscriptionId, true);

		forbidSubscriptionAccessToOtherVendor(subscription, vendorId);

		SubscriptionState activationInProgress = SubscriptionState.ACTIVATION_IN_PROGRESS;
		if (!subscription.hasSubscriptionState(activationInProgress)) {
			String currentState = subscription.getSubscriptionState().name();
			logger.error("Subscription state is {}. So, aborting payment transaction.", currentState);
			throw ForbiddenException.dealerSubscriptionExistsInState(currentState,
					"Payment transaction can only be performed on a subscription with state: ["
							+ activationInProgress.name() + "]");
		}

		/*
		 * Everything is OK. Create the payment transaction.
		 */
		String transactionEntityId = PaymentTransactionUtility.savePaymentTransaction(dbClient, param, Vendor.class,
				vendorId, VendorBiddingSubscription.class, subscriptionId);
		logger.info("Payment transaction for vendor bidding subscription created successfully.");

		return TaskResponseUtility.createResourceResponse(subscriptionId,
				Endpoint.VENDOR_PAYMENT_TRANSACTION_INFO.get(vendorId, transactionEntityId),
				"Bidding subscription payment transaction created successfully.");
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/{subscriptionId}/status", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "') and isRelated(#vendorId)")
	public ResponseEntity<TaskResponse> updateSubscriptionStatus(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("vendorId") String vendorId, @PathVariable("subscriptionId") String subscriptionId,
			@RequestBody UpdateSubscriptionStatusParam param) {

		logger.info("Request received to update bidding subscription status {} for vendor ID: {}. Request: {}",
				subscriptionId, vendorId, param);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");
		ArgValidator.checkFieldUriType(subscriptionId, VendorBiddingSubscription.class, "subscription_id");
		ArgValidator.checkFieldNotNull(param, "update_subscription_status");
		ArgValidator.checkFieldValueFromEnum(param.getStatus(), "status",
				UpdateSubscriptionStatusParam.SubscriptionStatus.class);

		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityNotNull(vendor, vendorId, true);

		if (user.isVendor()) {
			ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, true);
		}

		VendorBiddingSubscription subscription = dbClient.queryObject(VendorBiddingSubscription.class, subscriptionId);
		ArgValidator.checkEntityNotNull(subscription, subscriptionId, true);

		forbidSubscriptionAccessToOtherVendor(subscription, vendorId);

		SubscriptionStatus status = SubscriptionStatus.valueOf(param.getStatus());

		boolean statusChanged = BiddingSubscriptionUtility.updateSubscriptionState(dbClient, subscription, status);

		if (!statusChanged) {
			throw BadRequestException.nothingToUpdate();
		}
		dbClient.updateObject(subscription);
		logger.info("Vendor bidding subscription status updated successfully.");
		return TaskResponseUtility.createTaskSuccessResponse("Bidding subscription status updated successfully.");
	}

	@RequestMapping(method = RequestMethod.POST, path = "/create-subscription-with-default-plan", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR + "') and isRelated(#vendorId)")
	public ResponseEntity<CreateResourceResponse> createSubscriptionWithDefaultPlan(
			@PathVariable("vendorId") String vendorId) {

		logger.info("Request received to create bidding subscription with default plan for vendor ID: {}", vendorId);

		ArgValidator.checkFieldUriType(vendorId, Vendor.class, "vendor_id");

		Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, vendorId, true);
		/*
		 * Check if there is/was at-least one subscription associated with the dealer.
		 * If yes, then throw forbidden exception.
		 */
		QueryFields vendorField = new QueryFields("vendor", vendor);
		VendorBiddingSubscription subscription = dbClient.queryObjectByFields(VendorBiddingSubscription.class,
				vendorField);

		if (subscription != null) {
			logger.error("Subscription already exists for vendorId: {}. Subscription: {} - {}", vendorId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerAlreadyHasSubsctiption(vendorId);
		}

		/*
		 * Everything is OK. Create the subscription with default plan.
		 */
		subscription = BiddingSubscriptionUtility.createVendorBiddingSubscriptionWithDefaultPlan(dbClient, vendor);
		logger.info("Vendor bidding subscription with default plan created successfully.");

		return TaskResponseUtility.createResourceResponse(subscription.getId(),
				Endpoint.BUYER_BIDDING_SUBSCRIPTION_INFO.get(vendorId, subscription.getId()),
				"Bidding subscription created successfully.");

	}

}
