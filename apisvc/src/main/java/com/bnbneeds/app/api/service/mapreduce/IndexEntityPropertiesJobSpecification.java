package com.bnbneeds.app.api.service.mapreduce;

import java.util.List;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.tools.mapreduce.MapJob;
import com.google.appengine.tools.mapreduce.MapSpecification;
import com.google.appengine.tools.mapreduce.inputs.DatastoreInput;
import com.google.appengine.tools.mapreduce.outputs.NoOutput;

public class IndexEntityPropertiesJobSpecification {

	private String entityType;
	private List<String> properties;

	public IndexEntityPropertiesJobSpecification(String entityType, List<String> properties) {
		super();
		this.entityType = entityType;
		this.properties = properties;
	}

	public static MapSpecification<Entity, Void, Void> jobSpec(
			String entityType, List<String> properties, int shardCount) {
		IndexEntityPropertiesMapper mapper = new IndexEntityPropertiesMapper(
				properties);
		MapSpecification<Entity, Void, Void> spec = new MapSpecification.Builder<>(
				new DatastoreInput(entityType, shardCount), mapper,
				new NoOutput<Void, Void>()).setJobName(
				"Indexing entities of type: " + entityType).build();
		return spec;
	}

	public String start() {
		return MapJob.start(jobSpec(entityType, properties, 50),
				MapReduceUtils.getMapSettings());
	}

}
