package com.bnbneeds.app.api.service.controller;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.APIException;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.ReviewResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.DbClientUtility;
import com.bnbneeds.app.api.service.util.ReviewUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.OrderBy;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.BuyerAverageRating;
import com.bnbneeds.app.db.client.model.BuyerReview;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.review.AverageRatingResponse;
import com.bnbneeds.app.model.review.BuyerReviewInfoListResponse;
import com.bnbneeds.app.model.review.BuyerReviewInfoResponse;
import com.bnbneeds.app.model.review.ReviewParam;

/**
 * 
 * @author Amit Herlekar
 *
 */

@RestController
@RequestMapping(value = "/buyers/{buyerId}/reviews")
public class BuyerReviewService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(BuyerReviewService.class);

	private void updateAverageRating(Buyer buyer) {
		BuyerAverageRating averageRating = dbClient.queryObjectByFields(BuyerAverageRating.class,
				new QueryFields("buyer", buyer));

		boolean created = false;
		if (averageRating == null) {
			averageRating = new BuyerAverageRating();
			averageRating.setId(UUIDUtil.createId(BuyerAverageRating.class));
			averageRating.setBuyer(buyer);
			created = true;
		}
		QueryFields fields = new QueryFields("buyer", buyer);
		fields.add("entityStatus", EntityStatus.APPROVED.name());
		List<BuyerReview> reviews = dbClient.queryListBySortOrder(BuyerReview.class, fields,
				ReviewUtility.MAX_NUMBER_OF_REVIEWS_TO_CALCULATE_AVERAGE_RATING, OrderBy.DESC, "timestamp");
		ReviewUtility.calculateAverageRating(reviews, averageRating);
		DBOperationList opList = new DBOperationList();
		if (created) {
			opList.addOperation(OperationType.INSERT, averageRating);
		} else {
			opList.addOperation(OperationType.UPDATE, averageRating);
		}
		dbClient.transact(opList);
	}

	@RequestMapping(method = RequestMethod.POST, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR + "') and hasDealership()")
	public ResponseEntity<CreateResourceResponse> createBuyerReview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @RequestBody ReviewParam reviewParam) throws APIException {

		logger.info("Request received to post a review of buyer with id: {}", buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldNotNull(reviewParam, "review");

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);

		Vendor vendor = dbClient.queryObject(Vendor.class, user.getRelationshipId());

		ArgValidator.checkEntityBlacklisted(vendor, vendor.getId(), true);

		QueryFields fields = new QueryFields("buyer", buyer);
		fields.add("reviewedBy", vendor);

		BuyerReview buyerReview = dbClient.queryObjectByFields(BuyerReview.class, fields);

		if (buyerReview != null) {
			throw BadRequestException.reviewAlreadyExists(buyerId);
		}

		BuyerReview review = ReviewUtility.validateCreateReview(reviewParam, Buyer.class);
		String uid = UUIDUtil.createId(BuyerReview.class);
		review.setId(uid);
		review.setTimestamp(System.currentTimeMillis());
		review.setBuyer(buyer);
		review.setReviewedBy(vendor);

		dbClient.createObject(review);

		updateAverageRating(buyer);

		URI reviewLink = Endpoint.BUYER_REVIEW_INFO.get(buyerId, uid);

		recordEvent(SystemEventType.BUYER_REVIEW_POSTED, RecipientType.BUYER, buyerId, uid, null, reviewLink,
				vendor.getName());

		logger.info("Buyer review created successfully.", review);
		return TaskResponseUtility.createResourceResponse(uid, Endpoint.BUYER_REVIEW_INFO.get(buyerId, uid),
				"Buyer review posted successfully.");
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{reviewId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "','" + Role.ADMIN + "') and hasDealership()")
	public ResponseEntity<TaskResponse> updateBuyerReview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @PathVariable("reviewId") String reviewId,
			@RequestBody ReviewParam reviewParam) throws APIException {

		logger.info("Request received to update buyer review with id: {}", reviewId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(reviewId, BuyerReview.class, "review_id");

		ArgValidator.checkFieldNotNull(reviewParam, "review");

		BuyerReview buyerReview = dbClient.queryObject(BuyerReview.class, reviewId);
		if (user.isVendor()) {
			ArgValidator.checkEntityBlacklisted(buyerReview, reviewId, true);

			if (!buyerReview.getReviewedBy().getId().equals(user.getRelationshipId())) {
				throw ForbiddenException.unauthorizedAccess();
			} else {
				Vendor vendor = dbClient.queryObject(Vendor.class, user.getRelationshipId());
				ArgValidator.checkEntityBlacklisted(vendor, vendor.getId(), true);
				Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
				ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);
			}
		}

		if (!buyerReview.getBuyer().getId().equals(buyerId)) {
			throw ForbiddenException.unauthorizedAccess();
		}

		ReviewUtility.validateUpdateReview(reviewParam, buyerReview);

		buyerReview.setTimestamp(System.currentTimeMillis());

		dbClient.updateObject(buyerReview);
		updateAverageRating(buyerReview.getBuyer());

		logger.info("Buyer review updated successfully.", buyerReview);
		return TaskResponseUtility.createTaskSuccessResponse("Buyer review updated successfully.");
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{reviewId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "') and hasDealership()")
	public ResponseEntity<TaskResponse> deleteBuyerReview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @PathVariable("reviewId") String reviewId) throws APIException {

		logger.info("Request received to delete buyer review with id: {}", reviewId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(reviewId, BuyerReview.class, "review_id");

		if (!user.isAdmin()) {

		}

		BuyerReview buyerReview = dbClient.queryObject(BuyerReview.class, reviewId);
		if (user.isAdmin()) {
			ArgValidator.checkEntityApproved(buyerReview, reviewId, true);
		} else {
			// Vendor
			ArgValidator.checkEntityBlacklisted(buyerReview, reviewId, true);
			if (!buyerReview.getReviewedBy().getId().equals(user.getRelationshipId())) {
				throw ForbiddenException.unauthorizedAccess();
			} else {
				Vendor vendor = dbClient.queryObject(Vendor.class, user.getRelationshipId());
				ArgValidator.checkEntityBlacklisted(vendor, vendor.getId(), true);
				Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
				ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);
			}
		}

		if (!buyerReview.getBuyer().getId().equals(buyerId)) {
			throw ForbiddenException.unauthorizedAccess();
		}

		dbClient.markAsDeleted(buyerReview);

		updateAverageRating(buyerReview.getBuyer());

		logger.info("Buyer review deleted successfully.", buyerReview);
		return TaskResponseUtility.createTaskSuccessResponse("Buyer review deleted successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "','" + Role.BUYER + "','" + Role.ADMIN + "') and  hasDealership()")
	public BuyerReviewInfoListResponse getReviews(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {
		logger.info("Request received to get buyer reviews by fetchSize: {}, offset: {}", fetchSize, offset);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);

		if (user.isBuyer() && !buyer.getId().equals(user.getRelationshipId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		final String[] QUERY_FILTER_KEYS = { "reviewedBy" };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields("buyer", buyer);

		for (String key : keyValueMap.keySet()) {

			String value = keyValueMap.get(key);

			switch (key) {
			case "reviewedBy":
				ArgValidator.checkFieldUriType(value, Vendor.class, key);
				Vendor vendor = dbClient.queryObject(Vendor.class, value);
				ArgValidator.checkEntityBlacklisted(vendor, value, true);
				fields.add("reviewedBy", vendor);
				break;
			default:
				break;
			}
		}

		BuyerReviewInfoListResponse response = new BuyerReviewInfoListResponse();

		ResultSetIterator<BuyerReview> resultSetIterator = null;
		do {

			resultSetIterator = DbClientUtility.getResultSetIteratorOrderByDesc(dbClient, BuyerReview.class, fields,
					offset, fetchSize, "timestamp");

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					BuyerReview item = resultSetIterator.next();

					if (item == null) {
						continue;
					}

					if (!user.isAdmin() && !item.isApproved()) {
						continue;
					}

					Vendor vendor = item.getReviewedBy();
					if (!user.isAdmin() && vendor.isBlacklisted()) {
						continue;
					}

					BuyerReviewInfoResponse infoResponse = ReviewResponseMapper.map(item, DATE_FORMAT);
					response.addReviewInfoResponse(infoResponse);

				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}
		} while (response.size() < fetchSize);
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{reviewId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("(hasAnyRole('" + Role.VENDOR + "','" + Role.BUYER + "','" + Role.ADMIN
			+ "') and  hasDealership()) or (hasAnyRole('" + Role.ADMIN + "'))")
	public BuyerReviewInfoListResponse getReview(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @PathVariable("reviewId") String reviewId) {
		logger.info("Request received to get buyer review for reviewId: {}", reviewId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);

		if (user.isBuyer() && !buyer.getId().equals(user.getRelationshipId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		BuyerReviewInfoListResponse response = new BuyerReviewInfoListResponse();

		BuyerReview buyerReview = dbClient.queryObject(BuyerReview.class, reviewId);
		ArgValidator.checkEntityNotNull(buyerReview, reviewId, true);
		if (user.isDealer()) {
			ArgValidator.checkEntityBlacklisted(buyerReview, reviewId, true);
			ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);
		}
		if (user.isVendor() && !buyerReview.getReviewedBy().getId().equals(user.getRelationshipId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		BuyerReviewInfoResponse infoResponse = ReviewResponseMapper.map(buyerReview, DATE_FORMAT);
		response.addReviewInfoResponse(infoResponse);

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/average-rating", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "','" + Role.BUYER + "') and  hasDealership() or (hasAnyRole('"
			+ Role.ADMIN + "'))")
	public AverageRatingResponse getAverageRating(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId) {

		logger.info("Request received to get average rating of buyer with id: {}", buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);

		if (user.isBuyer() && !buyer.getId().equals(user.getRelationshipId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		AverageRatingResponse response = new AverageRatingResponse();
		BuyerAverageRating averageRating = dbClient.queryObjectByFields(BuyerAverageRating.class,
				new QueryFields("buyer", buyer));
		if (averageRating != null) {
			ReviewResponseMapper.map(averageRating, response);
		}

		return response;

	}

}
