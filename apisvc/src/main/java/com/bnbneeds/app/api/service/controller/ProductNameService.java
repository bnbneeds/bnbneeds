package com.bnbneeds.app.api.service.controller;

import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ResourceNotFoundException;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.BlobStoreUtility;
import com.bnbneeds.app.api.service.util.FileUploadConstants;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.ProductName;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.UploadFormResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.product.CreateProductNameBulkParam;
import com.bnbneeds.app.model.product.CreateProductNameParam;
import com.bnbneeds.app.util.StringUtils;
import com.google.appengine.api.blobstore.BlobInfo;

@RestController
@RequestMapping(value = "/product-names")
public class ProductNameService extends ResourceMasterAdminService {

	private static final Logger logger = LoggerFactory
			.getLogger(ProductNameService.class);

	@RequestMapping(method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN
			+ "') and hasDealership()")
	public ResponseEntity<CreateResourceResponse> createProductName(
			@RequestBody CreateProductNameParam nameParam,
			@AuthenticationPrincipal BNBNeedsUser user) {

		logger.info("API request received to add a new product name: {}",
				nameParam);

		ArgValidator.checkFieldNotEmpty(nameParam.getName(), "name");

		QueryFields queryFields = new QueryFields();
		queryFields.add("name", nameParam.getName());

		ProductName productNameEntity = dbClient.queryObjectByFields(
				ProductName.class, queryFields);

		if (productNameEntity != null) {
			logger.error("Product name with the given name already exists: {}",
					nameParam.getName());
			throw BadRequestException.nameAlreadyExists("Product name",
					nameParam.getName());
		}

		productNameEntity = new ProductName();
		String uid = UUIDUtil.createId(ProductName.class);
		productNameEntity.setId(uid);
		productNameEntity.setName(nameParam.getName());
		String description = nameParam.getDescription();
		if (!StringUtils.isEmpty(description)) {
			productNameEntity.setDescription(description);
		}

		setStatusOfNewEntity(productNameEntity, user);

		logger.debug("New product name record creating in DB: {}",
				productNameEntity);

		dbClient.createObject(productNameEntity);

		logger.info("New product name persisted in DB: {}", uid);

		recordEvent(SystemEventType.PRODUCT_NAME_CREATED, RecipientType.ADMIN,
				uid, nameParam.getName(), null, user.getUsername());

		return TaskResponseUtility.createResourceResponse(uid,
				Endpoint.PRODUCT_NAME_INFO,
				"New product name created successfully");
	}

	@RequestMapping(value = "/bulk", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN
			+ "') and hasDealership()")
	public ResponseEntity<TaskResponse> createProductNames(
			@RequestBody CreateProductNameBulkParam bulkParam,
			@AuthenticationPrincipal BNBNeedsUser user) {

		logger.info("API request received to add product names: {}", bulkParam);

		ArgValidator.checkFieldNotNull(bulkParam, "create_product_names");
		ArgValidator.checkFieldSize(bulkParam.getProductNameList(),
				"product_names", MAX_ENTITIES_SIZE);

		DBOperationList dbOperationList = new DBOperationList();

		Set<String> namesInRequest = new HashSet<String>();
		for (Iterator<CreateProductNameParam> iterator = bulkParam
				.getProductNameList().iterator(); iterator.hasNext();) {
			CreateProductNameParam requestParam = iterator.next();

			String name = requestParam.getName();
			ArgValidator.checkFieldNotEmpty(name, "name");

			if (namesInRequest.contains(name)) {
				logger.error(
						"Product name with same name already exists in the request: {}",
						name);
				throw BadRequestException.nameAlreadyExists("Product name",
						name);
			}
			namesInRequest.add(name);

			QueryFields queryFields = new QueryFields("name", name);

			ProductName productNameEntity = dbClient.queryObjectByFields(
					ProductName.class, queryFields);

			if (productNameEntity != null) {
				logger.error("Product name with same name already exists: {}",
						name);
				throw BadRequestException.nameAlreadyExists("Product name",
						name);
			}

			productNameEntity = new ProductName();
			String uid = UUIDUtil.createId(ProductName.class);
			productNameEntity.setId(uid);
			productNameEntity.setName(name);
			String description = requestParam.getDescription();
			if (!StringUtils.isEmpty(description)) {
				productNameEntity.setDescription(description);
			}

			setStatusOfNewEntity(productNameEntity, user);

			dbOperationList.addOperation(OperationType.INSERT,
					productNameEntity);
		}

		logger.debug("New product names in a DB transaction: {}",
				dbOperationList);

		dbClient.transact(dbOperationList);

		recordEvent(SystemEventType.PRODUCT_NAMES_CREATED, RecipientType.ADMIN,
				null, null, null, user.getUsername());

		String logMessage = "Task initiated to create product names.";

		logger.info(logMessage);

		return TaskResponseUtility.createTaskSubmittedResponse(logMessage);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/images", consumes = {
			MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.IMAGE_PNG_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<CreateResourceResponse> uploadProductNameImage(
			@PathVariable("id") String id) {

		logger.info(
				"Request received to upload image of product name with id: {}",
				id);

		ArgValidator
				.checkFieldUriType(id, ProductName.class, "product_name_id");

		ProductName productName = dbClient.queryObject(ProductName.class, id);

		ArgValidator.checkEntityBlacklisted(productName, id, true);

		String imageKey = productName.getImageKey();
		if (!StringUtils.isEmpty(imageKey)) {
			throw BadRequestException.imageUploadsExceeded("one");
		}

		BlobInfo fileInfo = BlobStoreUtility.getUploadedBlobKey(httpRequest,
				FileUploadConstants.IMAGE_FILE_FORM_ELEMENT_NAME);

		if (fileInfo == null) {
			throw BadRequestException
					.create("No image file selected for upload.");
		}
		String blobKeyString = fileInfo.getBlobKey().getKeyString();
		productName.setImageKey(blobKeyString);

		dbClient.updateObject(productName);

		return TaskResponseUtility.createResourceResponse(blobKeyString,
				Endpoint.PRODUCT_NAME_IMAGE.get(id, blobKeyString),
				"New image for the product name uploaded successfully.");
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}/images/{imageId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasRole('" + Role.ADMIN + "')")
	public ResponseEntity<TaskResponse> deleteProductNameImage(
			@PathVariable("id") String id,
			@PathVariable("imageId") String imageKey) {

		logger.info(
				"Request received to delete image with id: {}, of product name id: {}",
				imageKey, id);

		ArgValidator
				.checkFieldUriType(id, ProductName.class, "product_name_id");
		ArgValidator.checkFieldNotEmpty(imageKey, "image_key");

		ProductName productName = dbClient.queryObject(ProductName.class, id);

		ArgValidator.checkEntityNotNull(productName, id, true);

		String productNameImageKey = productName.getImageKey();

		if (StringUtils.isEmpty(productNameImageKey)
				|| !productNameImageKey.equals(imageKey)) {
			throw ResourceNotFoundException.idNotFound(imageKey);
		}
		BlobStoreUtility.deleteBlob(imageKey);
		productName.setImageKey(null);
		dbClient.updateObject(productName);

		return TaskResponseUtility
				.createTaskSuccessResponse("Product name image with id: ["
						+ imageKey + "] was deleted successfully.");

	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}/upload-image-form")
	@PreAuthorize(value = "hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN
			+ "') and hasDealership()")
	public UploadFormResponse getProductNameImageUploadElement(
			@PathVariable("id") String productNameId) {

		final URI UPLOAD_IMAGE_ACTION_URL = Endpoint.PRODUCT_NAME_IMAGES
				.get(productNameId);

		logger.info(
				"Request received to get upload image form element - product name id: {}",
				productNameId);

		ArgValidator.checkFieldUriType(productNameId, ProductName.class,
				"productNameId");

		ProductName productName = dbClient.queryObject(ProductName.class,
				productNameId);

		ArgValidator.checkEntityBlacklisted(productName, productNameId, true);

		String imageKey = productName.getImageKey();
		if (!StringUtils.isEmpty(imageKey)) {

			logger.error("Cannot upload more than 1 image for the product name.");
			throw BadRequestException.imageUploadsExceeded("one");
		}

		UploadFormResponse uploadForm = new UploadFormResponse();
		uploadForm
				.setAcceptedFileTypes(FileUploadConstants.IMAGE_FILE_ACCEPTED_TYPES);
		uploadForm
				.setInputFileElementName(FileUploadConstants.IMAGE_FILE_FORM_ELEMENT_NAME);
		URI uploadURL = URI.create(BlobStoreUtility
				.createUploadURL(UPLOAD_IMAGE_ACTION_URL));
		uploadForm.setActionURL(uploadURL.getPath());

		return uploadForm;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}/images/{imageId}")
	@PreAuthorize(value = "hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER
			+ "', '" + Role.ADMIN + "') and hasDealership()")
	public void getProductNameImage(@PathVariable("id") String productNameId,
			@PathVariable("imageId") String imageKey,
			HttpServletResponse response) throws IOException {

		logger.info("Request received to get image of product name id: {}",
				productNameId);

		ArgValidator.checkFieldUriType(productNameId, ProductName.class,
				"product_name_id");
		ArgValidator.checkFieldNotEmpty(imageKey, "image_id");

		ProductName productName = dbClient.queryObject(ProductName.class,
				productNameId);

		ArgValidator.checkEntityBlacklisted(productName, productNameId, true);

		String productNameImageKey = productName.getImageKey();
		if (productNameImageKey == null
				|| !productNameImageKey.equals(imageKey)) {
			throw ResourceNotFoundException.idNotFound(imageKey);
		}

		BlobStoreUtility.serveBlob(imageKey, response);
	}

	@Override
	public Class<ProductName> getDataModelClass() {
		return ProductName.class;
	}
}