package com.bnbneeds.app.api.service.pipeline.vendor;

import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryObject;

import com.bnbneeds.app.api.service.documentsearch.Constants;
import com.bnbneeds.app.api.service.documentsearch.SearchAPIUtility;
import com.bnbneeds.app.api.service.util.DealerUtility;
import com.bnbneeds.app.db.client.model.Vendor;
import com.google.appengine.api.search.Document;
import com.google.appengine.tools.pipeline.Job0;
import com.google.appengine.tools.pipeline.Value;

public class PutVendorIntoDocumentIndexJob extends Job0<Void> {

	private static final long serialVersionUID = 7758526280568388045L;

	private String vendorId;

	public PutVendorIntoDocumentIndexJob(String vendorId) {
		super();
		this.vendorId = vendorId;
	}

	@Override
	public String getJobDisplayName() {
		return "Index document for vendor with ID: " + vendorId;
	}

	@Override
	public Value<Void> run() throws Exception {
		Vendor vendor = queryObject(Vendor.class, vendorId);
		if (vendor == null) {
			return null;
		}

		Document vendorDoc = DealerUtility.createSearchDocumentForVendor(vendor);
		SearchAPIUtility.indexDocument(Constants.VENDOR_INDEX, vendorDoc, true);

		return null;
	}

}
