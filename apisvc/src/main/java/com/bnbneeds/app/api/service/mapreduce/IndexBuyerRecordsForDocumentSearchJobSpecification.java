package com.bnbneeds.app.api.service.mapreduce;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.tools.mapreduce.MapJob;
import com.google.appengine.tools.mapreduce.MapSpecification;
import com.google.appengine.tools.mapreduce.inputs.DatastoreInput;
import com.google.appengine.tools.mapreduce.outputs.NoOutput;

public class IndexBuyerRecordsForDocumentSearchJobSpecification {

	private static final String KIND = "Buyer";

	public IndexBuyerRecordsForDocumentSearchJobSpecification() {
		super();
	}

	public static MapSpecification<Entity, Void, Void> jobSpec(int shardCount) {

		IndexBuyerPropertiesForDoumentSearchMapper mapper = new IndexBuyerPropertiesForDoumentSearchMapper();
		Filter filter = new Query.FilterPredicate("inactive", FilterOperator.EQUAL, false);
		Query query = new Query(KIND).setFilter(filter).setKeysOnly();

		MapSpecification<Entity, Void, Void> spec = new MapSpecification.Builder<>(
				new DatastoreInput(query, shardCount), mapper, new NoOutput<Void, Void>())
						.setJobName("Index buyer records for document search").build();
		return spec;
	}

	public String start() {
		return MapJob.start(jobSpec(50), MapReduceUtils.getMapSettings());
	}

}
