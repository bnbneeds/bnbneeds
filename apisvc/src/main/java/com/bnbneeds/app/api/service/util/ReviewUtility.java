package com.bnbneeds.app.api.service.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.bnbneeds.app.db.client.model.AverageRating;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.BuyerReview;
import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.ProductReview;
import com.bnbneeds.app.db.client.model.Review;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.db.client.model.VendorReview;
import com.bnbneeds.app.model.review.AverageRatingResponse;
import com.bnbneeds.app.model.review.ReviewParam;

public class ReviewUtility {

	public static final int MAX_NUMBER_OF_REVIEWS_TO_CALCULATE_AVERAGE_RATING = 100;

	private static void validateRating(ReviewParam param) {
		Integer rating = param.getRating();
		ArgValidator.checkFieldNotNull(rating, "rating");
		ArgValidator.checkFieldRange(rating, 1, 5, "rating");
	}

	private static void copyReviewParam(ReviewParam from, Review to) {
		to.setRating(from.getRating());
		to.setHeadline(from.getHeadline());
		to.setDescription(from.getDescription());
	}

	/**
	 * Validates the request and returns a new review object after coping review
	 * properties.
	 * 
	 * @param param
	 * @param reviewForclazz
	 * @return the review data object for persistence
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Review> T validateCreateReview(ReviewParam param,
			Class<? extends DataObject> reviewForclazz) {

		validateRating(param);

		T review = null;
		String className = reviewForclazz.getSimpleName();
		if (Buyer.class.getSimpleName().equals(className)) {
			review = (T) new BuyerReview();
		}
		if (Vendor.class.getSimpleName().equals(className)) {
			review = (T) new VendorReview();
		}

		if (Product.class.getSimpleName().equals(className)) {
			review = (T) new ProductReview();
		}

		copyReviewParam(param, review);

		return review;

	}

	/**
	 * Validates request and copies it to review data object
	 * 
	 * @param from
	 * @param to
	 */
	public static void validateUpdateReview(ReviewParam from, Review to) {
		validateRating(from);
		copyReviewParam(from, to);
	}

	public static AverageRatingResponse calculateAverageRating(List<? extends Review> reviews) {
		AverageRatingResponse response = new AverageRatingResponse();

		if (reviews == null || reviews.isEmpty()) {
			return response;
		}

		double averageRating = 0.0;
		double totalNumberOfRatings = 0;
		Map<Integer, Integer> ratingMap = new LinkedHashMap<>();

		for (Iterator<? extends Review> iterator = reviews.iterator(); iterator.hasNext();) {
			Review review = iterator.next();
			totalNumberOfRatings++;
			Integer eachRating = review.getRating();
			if (ratingMap.containsKey(eachRating)) {
				Integer numberOfRatings = ratingMap.get(eachRating);
				numberOfRatings++;
				ratingMap.put(eachRating, numberOfRatings);
			} else {
				ratingMap.put(eachRating, 1);
			}

		}
		Integer ratingFactor = 0;
		Double ratingPercentage = 0.0;
		for (Integer rating : ratingMap.keySet()) {
			int numberOfRatings = ratingMap.get(rating);
			ratingFactor += rating * numberOfRatings;
			ratingPercentage = (numberOfRatings * 100.0) / totalNumberOfRatings;

			switch (rating) {
			case 5:
				response.setFiveStarPercent(ratingPercentage);
				break;
			case 4:
				response.setFourStarPercent(ratingPercentage);
				break;
			case 3:
				response.setThreeStarPercent(ratingPercentage);
				break;
			case 2:
				response.setTwoStarPercent(ratingPercentage);
				break;
			case 1:
				response.setOneStarPercent(ratingPercentage);
				break;
			default:
				break;
			}
		}
		averageRating = (ratingFactor / totalNumberOfRatings);
		response.setAverageRating(averageRating);
		return response;
	}

	public static void calculateAverageRating(List<? extends Review> reviews, AverageRating averageRatingEntity) {

		averageRatingEntity.reset();

		if (reviews == null || reviews.isEmpty()) {
			return;
		}

		double averageRating = 0.0;
		double totalNumberOfRatings = 0;
		Map<Integer, Integer> ratingMap = new LinkedHashMap<>();

		for (Iterator<? extends Review> iterator = reviews.iterator(); iterator.hasNext();) {
			Review review = iterator.next();
			if (review.isBlacklisted()) {
				continue;
			}
			totalNumberOfRatings++;
			Integer eachRating = review.getRating();
			if (ratingMap.containsKey(eachRating)) {
				Integer numberOfRatings = ratingMap.get(eachRating);
				numberOfRatings++;
				ratingMap.put(eachRating, numberOfRatings);
			} else {
				ratingMap.put(eachRating, 1);
			}

		}
		Integer ratingFactor = 0;
		Double ratingPercentage = 0.0;
		for (Integer rating : ratingMap.keySet()) {
			int numberOfRatings = ratingMap.get(rating);
			ratingFactor += rating * numberOfRatings;
			ratingPercentage = (numberOfRatings * 100.0) / totalNumberOfRatings;

			switch (rating) {
			case 5:
				averageRatingEntity.setFiveStarRatingPercentage(ratingPercentage);
				break;
			case 4:
				averageRatingEntity.setFourStarRatingPercentage(ratingPercentage);
				break;
			case 3:
				averageRatingEntity.setThreeStarRatingPercentage(ratingPercentage);
				break;
			case 2:
				averageRatingEntity.setTwoStarRatingPercentage(ratingPercentage);
				break;
			case 1:
				averageRatingEntity.setOneStarRatingPercentage(ratingPercentage);
				break;
			default:
				break;
			}
		}
		averageRating = (ratingFactor / totalNumberOfRatings);
		averageRatingEntity.setAverageRating(averageRating);
	}

}
