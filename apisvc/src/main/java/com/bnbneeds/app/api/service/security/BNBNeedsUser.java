package com.bnbneeds.app.api.service.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.bnbneeds.app.db.client.model.UserAccount;

public class BNBNeedsUser extends User {

	private static final long serialVersionUID = -1482889925852544457L;
	private UserAccount account;

	public BNBNeedsUser(String username, String password,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
	}

	public BNBNeedsUser(String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired,
				credentialsNonExpired, accountNonLocked, authorities);
	}

	public UserAccount getAccount() {
		return account;
	}

	public void setAccount(UserAccount account) {
		this.account = account;
	}

	public String getRole() {
		return account.getRole();
	}

	public String getRelationshipId() {
		return account.getRelationshipId();
	}

	public boolean isAdmin() {
		return (Role.ADMIN.equals(getRole()) || Role.SUPER_ADMIN
				.equals(getRole()));
	}

	public boolean isSuperAdmin() {
		return Role.SUPER_ADMIN.equals(getRole());
	}

	public boolean isVendor() {
		return Role.VENDOR.equals(getRole());
	}

	public boolean isBuyer() {
		return Role.BUYER.equals(getRole());
	}

	public boolean isDealer() {
		return (isVendor() || isBuyer());
	}

}
