package com.bnbneeds.app.api.service.security;

import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;

/**
 * Implementing this class since the Spring implementation uses ResourceBundle
 * for exception messages. Itis forbidden by GAE
 * 
 * @author Amit Herlekar
 *
 */
public class UserAccountDetailsStatusChecker implements UserDetailsChecker {

	@Override
	public void check(UserDetails user) {
		if (!user.isAccountNonLocked()) {
			throw new LockedException("User account is locked");
		}

		if (!user.isEnabled()) {
			throw new DisabledException("User is disabled");
		}

		if (!user.isAccountNonExpired()) {
			throw new AccountExpiredException("User account has expired");
		}

		if (!user.isCredentialsNonExpired()) {
			throw new CredentialsExpiredException(
					"User credentials have expired");
		}
	}

}
