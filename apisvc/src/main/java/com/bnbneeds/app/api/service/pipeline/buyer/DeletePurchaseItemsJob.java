package com.bnbneeds.app.api.service.pipeline.buyer;

import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.PurchaseItem;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeletePurchaseItemsJob extends Job2<Void, String, Boolean> {

	private static final Logger logger = LoggerFactory.getLogger(DeletePurchaseItemsJob.class);

	private static final long serialVersionUID = 6564082081712030373L;

	@Override
	public Value<Void> run(String buyerId, Boolean forceDelete) throws Exception {
		Buyer buyer = DeleteBuyerJobROOT.queryBuyer(buyerId);
		if (buyer == null) {
			return null;
		}
		List<PurchaseItem> purchaseItems = queryList(PurchaseItem.class, DeleteBuyerJobROOT.buyerField(buyer));
		if (!CollectionUtils.isEmpty(purchaseItems)) {
			if (!forceDelete) {
				throw DeleteDealerJob.cannotDeleteHasReferences(buyerId, Buyer.class, PurchaseItem.class);
			}

			logger.info("Get purchase items of the buyer [{}] and delete them...", buyerId);
			DBOperationList purchaseItemDeleteOperationList = new DBOperationList();
			for (Iterator<PurchaseItem> iterator = purchaseItems.iterator(); iterator.hasNext();) {
				PurchaseItem purchaseItem = iterator.next();
				logger.debug("Deleting purchase item: {}", purchaseItem);
				/*
				 * Must call delete because the entity type PurchaseItem is not picked by delete
				 * inactive entities cron job
				 */
				purchaseItemDeleteOperationList.addOperation(OperationType.DELETE, purchaseItem);
			}
			transact(purchaseItemDeleteOperationList);
		}
		return null;
	}

}
