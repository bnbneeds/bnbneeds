package com.bnbneeds.app.api.service.util;

import java.net.URI;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.model.Notification;

public class NotificationUtility {

	public static Notification createNotification(SystemEventType type, RecipientType recipientType, String recipientId,
			String objectId, String objectName, URI objectLink, String createdBy) {

		Notification notification = new Notification();
		notification.setTimestamp(System.currentTimeMillis());
		notification.setId(UUIDUtil.createId(Notification.class));
		notification.setType(type.name());
		notification.setNotificationText(type.getMessage());
		notification.setRecipientType(recipientType.name());
		notification.setRecipientId(recipientId);
		notification.setEntityId(objectId);
		if (objectName != null) {
			notification.setEntityName(objectName);
		}
		if (objectLink != null) {
			notification.setLink(objectLink.toString());
		}
		if (createdBy != null) {
			notification.setTriggeredByDealerName(createdBy);
		}
		return notification;
	}
}
