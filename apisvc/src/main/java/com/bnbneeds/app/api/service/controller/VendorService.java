package com.bnbneeds.app.api.service.controller;

import static com.bnbneeds.app.api.service.response.DBObjectMapper.toNamedRelatedListResponse;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.documentsearch.Constants;
import com.bnbneeds.app.api.service.documentsearch.SearchAPIUtility;
import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.APIException;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.pipeline.PipelineUtils;
import com.bnbneeds.app.api.service.pipeline.vendor.PutVendorIntoDocumentIndexJob;
import com.bnbneeds.app.api.service.pipeline.vendor.ReindexVendorDocumentJob;
import com.bnbneeds.app.api.service.response.DealerResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.DealerUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.messaging.email.MessageKeys;
import com.bnbneeds.app.messaging.email.Template;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.CreateVendorParam;
import com.bnbneeds.app.model.dealer.UpdateVendorParam;
import com.bnbneeds.app.model.dealer.VendorInfoListResponse;
import com.bnbneeds.app.model.dealer.VendorInfoResponse;
import com.bnbneeds.app.model.dealer.VendorListResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.google.appengine.api.search.Cursor;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;

@RestController
@RequestMapping(value = "/vendors")
public class VendorService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(VendorService.class);

	private ResultSetIterator<Vendor> getResultSetIterator(String filter, String offset, int fetchSize) {
		final String[] QUERY_FILTER_KEYS = { "name", "status" };
		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);
		QueryFields fields = new QueryFields();

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);

			switch (key) {
			case "name":
				fields.add(key, value);
				break;
			case "status":
				ArgValidator.checkFieldValue(!EntityStatus.BLACKLISTED.name().equals(value.toUpperCase()), key);
				fields.add("entityStatus", value.toUpperCase());
				break;
			default:
				break;
			}
		}

		ResultSetIterator<Vendor> resultSetIterator = getResultSetIterator(Vendor.class, fields, offset, fetchSize);

		return resultSetIterator;
	}

	@RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.VENDOR + "') and canMakeRelationship()")
	public ResponseEntity<CreateResourceResponse> createVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestBody CreateVendorParam vendorParam) {

		logger.info("API request received to register a new vendor: {}", vendorParam);

		DealerUtility.validateCreateDealerParam(vendorParam, dbClient);

		Vendor vendor = new Vendor();
		String uid = UUIDUtil.createId(Vendor.class);
		vendor.setId(uid);
		vendor.setName(vendorParam.getOrganizationName());
		vendor.setDescription(vendorParam.getDescription());
		vendor.setAddress(vendorParam.getAddress());
		vendor.setCities(vendorParam.getCities());
		vendor.setContactName(vendorParam.getContactName());
		vendor.setMobileNumber(vendorParam.getMobileNumber());
		vendor.setEmailAddress(vendorParam.getEmailAddress());
		vendor.setPan(vendorParam.getPan());
		vendor.setVatNumber(vendorParam.getVatNumber());
		vendor.setServiceTaxNumber(vendorParam.getServiceTaxNumber());
		vendor.setWebsite(vendorParam.getWebsite());

		logger.debug("New vendor record creating in DB: {}", vendor);

		DBOperationList dbOperationList = new DBOperationList();
		dbOperationList.addOperation(OperationType.INSERT, vendor);

		UserAccount userLogin = user.getAccount();
		userLogin.setRelationshipId(uid);
		dbOperationList.addOperation(OperationType.UPDATE, userLogin);
		dbClient.transact(dbOperationList);
		user.setAccount(userLogin);
		logger.debug("Updated relationshipId in user login: {}", uid);
		logger.info("New vendor persisted in DB: {}", uid);

		URI uri = Endpoint.VENDOR_INFO.get(uid);

		recordEvent(SystemEventType.NEW_VENDOR_REGISTERED, RecipientType.BUYERS_AND_ADMIN, uid,
				vendorParam.getOrganizationName(), uri, user.getUsername());

		PipelineUtils.startPipelineService(new PutVendorIntoDocumentIndexJob(uid));

		Template message = emailHelper.createMessageTemplate(MessageKeys.DEALER_REGISTERED);
		message.resolveBodyValues(user.getAccount().getAccountName());
		emailHelper.send(message, user.getAccount().getName());

		return TaskResponseUtility.createResourceResponse(uid, uri, "New vendor created successfully");

	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "') and hasDealership()")
	public VendorListResponse getVendorList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get vendors by filter: {}, fetchSize: {}, offset: {}", filter, fetchSize,
				offset);

		VendorListResponse resp = new VendorListResponse();
		List<Vendor> vendorList = null;

		if (user.isVendor()) {
			String relationshipId = user.getRelationshipId();
			Vendor vendor = dbClient.queryObject(Vendor.class, relationshipId);

			if (vendor != null) {
				if (!vendor.isBlacklisted()) {
					vendorList = new ArrayList<>();
					vendorList.add(vendor);
					resp.setNamedRelatedResourceList(toNamedRelatedListResponse(vendorList));
				}
			}
		} else {
			ResultSetIterator<Vendor> resultSetIterator = null;
			do {
				resultSetIterator = getResultSetIterator(filter, offset, fetchSize);
				if (resultSetIterator != null) {
					resp.addNamedRelatedResourceList(toNamedRelatedListResponse(resultSetIterator, null,
							new EntityStatus[] { EntityStatus.BLACKLISTED }));
					resp.setNextOffset(resultSetIterator.getOffset());

					if (offset != null && offset.equals(resultSetIterator.getOffset())) {
						break;
					}
					offset = resultSetIterator.getOffset();
				} else {
					break;
				}

			} while (resp.size() < fetchSize);
		}

		logger.debug("Exit returning vendors of size: {}",
				resp.getVendorList() == null ? 0 : resp.getVendorList().size());

		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/bulk", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "','" + Role.ADMIN + "')  and hasDealership()")
	public VendorInfoListResponse getVendorInfoList(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get vendors by filter: {}, fetchSize: {}, offset: {}", filter, fetchSize,
				offset);

		VendorInfoListResponse resp = new VendorInfoListResponse();

		if (user.isVendor()) {
			String relationshipId = user.getRelationshipId();
			Vendor vendor = dbClient.queryObject(Vendor.class, relationshipId);

			if (vendor != null) {
				if (EntityStatus.BLACKLISTED != vendor.getEntityStatus()) {
					VendorInfoResponse to = new VendorInfoResponse();
					DealerResponseMapper.mapVendor(vendor, to);
					resp.add(to);
				}
			}
		} else {
			ResultSetIterator<Vendor> resultSetIterator = null;
			do {
				resultSetIterator = getResultSetIterator(filter, offset, fetchSize);
				if (resultSetIterator != null) {
					while (resultSetIterator.hasNext()) {
						Vendor vendor = resultSetIterator.next();
						if (vendor != null && (!vendor.isBlacklisted() || user.isAdmin())) {
							VendorInfoResponse to = new VendorInfoResponse();
							DealerResponseMapper.mapVendor(vendor, to);
							resp.add(to);
						}
					}
					resp.setNextOffset(resultSetIterator.getOffset());
					if (offset != null && offset.equals(resultSetIterator.getOffset())) {
						break;
					}
					offset = resultSetIterator.getOffset();
				} else {
					break;
				}
			} while (resp.size() < fetchSize);
		}

		logger.debug("Exit returning vendors of size: {}",
				resp.getVendorList() == null ? 0 : resp.getVendorList().size());

		return resp;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.VENDOR + "', '" + Role.BUYER + "','" + Role.ADMIN + "') and hasDealership()")
	public VendorInfoResponse getVendor(@AuthenticationPrincipal BNBNeedsUser user, @PathVariable("id") String id)
			throws APIException {

		logger.info("Request received to retrieve the vendor details: vendor id: {}", id);

		ArgValidator.checkFieldUriType(id, Vendor.class, "vendor_id");

		if (user.isVendor() && !id.equals(user.getRelationshipId())) {
			logger.error(
					"Unauthorized access: Requested vendorId is not related to user logged in. Throwing forbidden exception.");
			throw ForbiddenException.unauthorizedAccess();
		}

		Vendor vendor = dbClient.queryObject(Vendor.class, id);
		ArgValidator.checkEntityNotNull(vendor, id, true);
		if (!user.isAdmin()) {
			ArgValidator.checkEntityBlacklisted(vendor, id, true);
		}

		VendorInfoResponse resp = new VendorInfoResponse();
		DealerResponseMapper.mapVendor(vendor, resp);

		return resp;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/deactivate", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasAnyRole('" + Role.VENDOR + "', '" + Role.ADMIN + "') and isRelated(#id)")
	public ResponseEntity<CreateResourceResponse> deactivateVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String id,
			@RequestParam(name = REQ_PARAM_FORCE_DELETE, defaultValue = "false") String forceDelete)
			throws APIException {

		logger.info("Request received to delete vendor. Vendor id: {}", id);

		ArgValidator.checkFieldUriType(id, Vendor.class, "vendor_id");
		Vendor vendor = dbClient.queryObject(Vendor.class, id);
		if (user.isAdmin()) {
			ArgValidator.checkEntityApproved(vendor, id, true);
		} else {
			ArgValidator.checkEntityDisapprovedOrBlacklisted(vendor, id, true);
		}

		String jobId = DealerUtility.deleteVendor(id, false, Boolean.valueOf(forceDelete));

		return PipelineUtils.createPipelineJobInfoResponse(jobId, "Task initiated to delete vendor.");
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasAnyRole('" + Role.VENDOR + "','" + Role.ADMIN + "') and isRelated(#id) ")
	public ResponseEntity<TaskResponse> updateVendor(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("id") String id, @RequestBody UpdateVendorParam updateParam) throws APIException {

		logger.info("Request received to update vendor. Vendor id: {}", id);

		ArgValidator.checkFieldNotNull(updateParam, "update_vendor");

		Vendor vendor = (Vendor) DealerUtility.validateUpdateDealerParam(id, updateParam, user, dbClient);
		if (vendor == null) {
			throw BadRequestException.create("There is nothing to update in vendor information.");
		}

		dbClient.updateObject(vendor);

		PipelineUtils.startPipelineService(new ReindexVendorDocumentJob(id));

		return TaskResponseUtility.createTaskSuccessResponse("Vendor information updated successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, value = "/search", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize(value = "hasAnyRole('" + Role.BUYER + "','" + Role.ADMIN + "') and hasDealership()")
	public VendorInfoListResponse searchVendors(@AuthenticationPrincipal BNBNeedsUser user,
			@RequestParam(name = REQ_PARAM_SEARCH_TEXT) String searchText,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) throws APIException {

		logger.info("Request received to search vendors with search text: {}", searchText);

		if (user.isBuyer()) {
			String buyerId = user.getRelationshipId();
			Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
			ArgValidator.checkEntityBlacklisted(buyer, buyerId, false);
		}

		VendorInfoListResponse resp = new VendorInfoListResponse();
		Results<ScoredDocument> results = SearchAPIUtility.searchDocuments(Constants.VENDOR_INDEX, searchText,
				fetchSize, offset, true);

		for (ScoredDocument doc : results) {
			String vendorId = doc.getId();
			Vendor vendor = dbClient.queryObject(Vendor.class, vendorId);
			if (user.isBuyer() && vendor.isBlacklisted()) {
				continue;
			}
			VendorInfoResponse vendorInfo = DealerResponseMapper.mapVendor(vendor);
			resp.add(vendorInfo);
		}

		Cursor cursor = results.getCursor();
		if (cursor != null) {
			resp.setNextOffset(cursor.toWebSafeString());
		}

		return resp;
	}

}
