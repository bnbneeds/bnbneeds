package com.bnbneeds.app.api.service.pipeline.vendor;

import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.PaymentTransactionLinkedEntity;
import com.bnbneeds.app.db.client.model.Vendor;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeletePaymentTransactionsJob extends Job2<Void, String, Boolean> {

	private static final long serialVersionUID = 4580221874518413925L;
	private static final Logger logger = LoggerFactory.getLogger(DeletePaymentTransactionsJob.class);

	@Override
	public Value<Void> run(String vendorId, Boolean forceDelete) throws Exception {

		List<PaymentTransactionLinkedEntity> paymentTransactions = queryList(PaymentTransactionLinkedEntity.class,
				new QueryFields("dealerId", vendorId));

		if (!CollectionUtils.isEmpty(paymentTransactions)) {
			if (!forceDelete) {
				throw DeleteDealerJob.cannotDeleteHasReferences(vendorId, Vendor.class,
						PaymentTransactionLinkedEntity.class);
			}

			logger.info("Delete the payment transactions of the vendor [{}]", vendorId);
			DBOperationList operationList = new DBOperationList();

			for (Iterator<PaymentTransactionLinkedEntity> iterator = paymentTransactions.iterator(); iterator
					.hasNext();) {
				PaymentTransactionLinkedEntity paymentTransaction = iterator.next();
				operationList.addOperation(OperationType.MARK_AS_DELETED, paymentTransaction.getPaymentTransaction());
				operationList.addOperation(OperationType.MARK_AS_DELETED, paymentTransaction);
			}

			transact(operationList);
		}
		return null;
	}
}
