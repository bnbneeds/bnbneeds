package com.bnbneeds.app.api.service.configuration;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.db.client.DbClient;
import com.google.appengine.api.ThreadManager;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.Entity;

/**
 * This class processes the classpath for classes with the @Entity or @Subclass
 * annotations from Objectify and registers them with the ObjectifyFactory, it
 * is multi-threaded and works very fast!
 */
public class ObjectifyLoaderContextListener implements ServletContextListener {
	
	private static final Logger logger = LoggerFactory
			.getLogger(ObjectifyLoaderContextListener.class);
	
	//private static final String DB_MODEL_BASE_PACKAGE = "com.bnbneeds.app.db.client.model";

	private final Set<Class<?>> entities;
	

	public ObjectifyLoaderContextListener() {
		this.entities = new HashSet<>();
	}
	

	@Override
	public void contextInitialized(final ServletContextEvent sce) {
		
		final ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setUrls(ClasspathHelper.forPackage(DbClient.DB_MODEL_PACKAGE));
		
		final ExecutorService es = Executors.newCachedThreadPool(ThreadManager
				.currentRequestThreadFactory());
		cb.setExecutorService(es);
		final Reflections r = new Reflections(cb);
		this.entities.addAll(r.getTypesAnnotatedWith(Entity.class));
		es.shutdown();
		final ObjectifyFactory of = ObjectifyService.factory();
		for (final Class<?> cls : this.entities) {
			of.register(cls);
			logger.info("Registered {} with Objectify", cls.getName());
		}
	}

	@Override
	public void contextDestroyed(final ServletContextEvent sce) {
		/* this is intentionally empty */
	}

}
