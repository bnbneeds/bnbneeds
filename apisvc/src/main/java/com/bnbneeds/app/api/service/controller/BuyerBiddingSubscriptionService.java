package com.bnbneeds.app.api.service.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.SubscriptionResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.BiddingSubscriptionUtility;
import com.bnbneeds.app.api.service.util.PaymentTransactionUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscription;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscriptionPlan;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription.SubscriptionState;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.CreatePaymentTransactionParam;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionInfoListResponse;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.CreateSubscriptionParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionStatusParam;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionStatusParam.SubscriptionStatus;
import com.bnbneeds.app.model.endpoints.Endpoint;

@RestController
@RequestMapping(path = "/buyers/{buyerId}/bidding-subscriptions")
public class BuyerBiddingSubscriptionService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(BuyerBiddingSubscriptionService.class);

	/**
	 * 
	 * Throws {@code ForbiddenException} if the given buyer ID is not the same as
	 * the subscription's buyer ID
	 * 
	 * @param subscription
	 *            the buyer bidding subscription
	 * @param buyerId
	 *            the buyer ID
	 */
	private static void forbidSubscriptionAccessToOtherBuyer(BuyerBiddingSubscription subscription, String buyerId) {
		if (!subscription.getBuyer().getId().equals(buyerId)) {
			throw ForbiddenException.unauthorizedAccess();
		}
	}

	@RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and isRelated(#buyerId)")
	public ResponseEntity<CreateResourceResponse> createSubscription(@PathVariable("buyerId") String buyerId,
			@RequestBody CreateSubscriptionParam param) {

		logger.info("Request received to create bidding subscription for buyer ID: {}. Request: {}", buyerId, param);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldNotNull(param, "create_subscription");
		String subscriptionPlanId = param.getSubscriptionPlanId();
		ArgValidator.checkFieldUriType(subscriptionPlanId, BuyerBiddingSubscriptionPlan.class, "subscription_plan_id");

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);
		/*
		 * Check if already a subscription exists which is either ACTIVE or
		 * ACTIVATION_PENDING. If yes, then throw forbidden exception.
		 */
		QueryFields buyerField = new QueryFields("buyer", buyer);
		final String SUBSCRIPTION_STATE_FIELD = "subscriptionState";
		QueryFields fields = new QueryFields(SUBSCRIPTION_STATE_FIELD,
				DealerBiddingSubscription.SubscriptionState.ACTIVE.name());
		fields.add(buyerField);
		BuyerBiddingSubscription subscription = dbClient.queryObjectByFields(BuyerBiddingSubscription.class, fields);

		if (subscription != null) {
			logger.error("Subscription already exists for buyerId: {}. Subscription: {} - {}", buyerId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerSubscriptionAlreadyExists(subscription.getSubscriptionState().name());
		}

		subscription = dbClient.queryObjectByFields(BuyerBiddingSubscription.class,
				new QueryFields(SUBSCRIPTION_STATE_FIELD, SubscriptionState.ACTIVATION_IN_PROGRESS.name())
						.add(buyerField));

		if (subscription != null) {
			logger.error("Subscription already exists for buyerId: {}. Subscription: {} - {}", buyerId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerSubscriptionAlreadyExists(subscription.getSubscriptionState().name());
		}

		subscription = dbClient.queryObjectByFields(BuyerBiddingSubscription.class,
				new QueryFields(SUBSCRIPTION_STATE_FIELD, SubscriptionState.ACTIVATION_PENDING.name()).add(buyerField));

		if (subscription != null) {
			logger.error("Subscription already exists for buyerId: {}. Subscription: {} - {}", buyerId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerSubscriptionAlreadyExists(subscription.getSubscriptionState().name());
		}

		/*
		 * Check if the requested plan is disapproved. If yes, then throw exception.
		 */
		BuyerBiddingSubscriptionPlan subscriptionPlan = dbClient.queryObject(BuyerBiddingSubscriptionPlan.class,
				subscriptionPlanId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(subscriptionPlan, subscriptionPlanId, false);
		if (!subscriptionPlan.isAvailable()) {
			throw BadRequestException.parameterInvalid("subscription_plan_id",
					"The subscription plan is not available for subscription: [" + subscriptionPlanId + "]");
		}

		if (subscriptionPlan.isDefaultPlan()) {
			throw BadRequestException.parameterInvalid("subscription_plan_id",
					"Cannot create a subscription with a default plan: [" + subscriptionPlanId + "]");
		}

		/*
		 * Everything is OK. Create the subscription.
		 */
		String subscriptionId = UUIDUtil.createId(BuyerBiddingSubscription.class);
		subscription = new BuyerBiddingSubscription();
		subscription.setId(subscriptionId);
		subscription.setBuyer(buyer);
		subscription.setSubscriptionPlan(subscriptionPlan);
		BiddingSubscriptionUtility.initialize(subscription);

		dbClient.createObject(subscription);
		logger.info("Buyer bidding subscription created successfully.");

		return TaskResponseUtility.createResourceResponse(subscriptionId,
				Endpoint.BUYER_BIDDING_SUBSCRIPTION_INFO.get(buyerId, subscriptionId),
				"Bidding subscription created successfully.");
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/{subscriptionId}", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> updateSubscription(@PathVariable("buyerId") String buyerId,
			@PathVariable("subscriptionId") String subscriptionId, @RequestBody UpdateSubscriptionParam param) {

		logger.info("Request received to update bidding subscription {} for buyer ID: {}. Request: {}", subscriptionId,
				buyerId, param);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(subscriptionId, BuyerBiddingSubscription.class, "subscription_id");
		ArgValidator.checkFieldNotNull(param, "update_subscription");
		String subscriptionPlanId = param.getSubscriptionPlanId();
		ArgValidator.checkFieldUriType(subscriptionPlanId, BuyerBiddingSubscriptionPlan.class, "subscription_plan_id");

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);

		BuyerBiddingSubscription subscription = dbClient.queryObject(BuyerBiddingSubscription.class, subscriptionId);
		ArgValidator.checkEntityNotNull(subscription, subscriptionId, true);

		forbidSubscriptionAccessToOtherBuyer(subscription, buyerId);

		BiddingSubscriptionUtility.validateUpdateBiddingSubscriptionRequest(buyerId, subscription);

		/*
		 * Check if the requested plan is disapproved. If yes, then throw exception.
		 */
		BuyerBiddingSubscriptionPlan subscriptionPlan = dbClient.queryObject(BuyerBiddingSubscriptionPlan.class,
				subscriptionPlanId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(subscriptionPlan, subscriptionPlanId, false);
		if (!subscriptionPlan.isAvailable()) {
			throw BadRequestException.parameterInvalid("subscription_plan_id",
					"The subscription plan is not available for subscription: [" + subscriptionPlanId + "]");
		}

		if (subscriptionPlan.isDefaultPlan()) {
			throw BadRequestException.parameterInvalid("subscription_plan_id",
					"Cannot update a subscription with a default plan: [" + subscriptionPlanId + "]");
		}

		/*
		 * Everything is OK. Update the subscription.
		 */
		subscription.setSubscriptionPlan(subscriptionPlan);
		BiddingSubscriptionUtility.initialize(subscription);

		dbClient.updateObject(subscription);
		logger.info("Buyer bidding subscription updated successfully.");

		return TaskResponseUtility.createTaskSuccessResponse("Bidding subscription updated successfully.");
	}

	@RequestMapping(method = RequestMethod.DELETE, path = "/{subscriptionId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> deleteSubscription(@PathVariable("buyerId") String buyerId,
			@PathVariable("subscriptionId") String subscriptionId) {

		logger.info("Request received to delete bidding subscription {} for buyer ID: {}. Request: {}", subscriptionId,
				buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(subscriptionId, BuyerBiddingSubscription.class, "subscription_id");

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);

		BuyerBiddingSubscription subscription = dbClient.queryObject(BuyerBiddingSubscription.class, subscriptionId);
		ArgValidator.checkEntityNotNull(subscription, subscriptionId, true);

		forbidSubscriptionAccessToOtherBuyer(subscription, buyerId);

		BiddingSubscriptionUtility.validateDeleteBiddingSubscriptionRequest(buyerId, subscription);

		/*
		 * Everything is OK. Delete the subscription.
		 */

		dbClient.markAsDeleted(subscription);

		logger.info("Buyer bidding subscription deleted successfully.");

		return TaskResponseUtility.createTaskSuccessResponse("Bidding subscription deleted successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{subscriptionId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.BUYER + "', '" + Role.ADMIN + "') and isRelated(#buyerId)")
	public BuyerBiddingSubscriptionInfoResponse getSubscription(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @PathVariable("subscriptionId") String subscriptionId) {

		logger.info("Request received to get bidding subscription {} for buyer ID: {}.", subscriptionId, buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(subscriptionId, BuyerBiddingSubscription.class, "subscription_id");

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityNotNull(buyer, buyerId, true);

		if (user.isBuyer()) {
			ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);
		}

		BuyerBiddingSubscription subscription = dbClient.queryObject(BuyerBiddingSubscription.class, subscriptionId);
		ArgValidator.checkEntityNotNull(subscription, subscriptionId, true);

		forbidSubscriptionAccessToOtherBuyer(subscription, buyerId);

		logger.info("Exit returning subscription deatails: {}", subscription);

		return SubscriptionResponseMapper.map(subscription, TIMESTAMP_FORMAT);
	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.BUYER + "', '" + Role.ADMIN + "') and isRelated(#buyerId)")
	public BuyerBiddingSubscriptionInfoListResponse getSubscriptions(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get bidding subscriptions for buyer ID: {}.", buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityNotNull(buyer, buyerId, true);

		if (user.isBuyer()) {
			ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);
		}
		final String[] QUERY_FILTER_KEYS = { "state", "subscriptionPlanId" };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields("buyer", buyer);

		for (String key : keyValueMap.keySet()) {

			String value = keyValueMap.get(key);

			switch (key) {
			case "state":
				ArgValidator.checkFieldValueFromEnum(value, key, SubscriptionState.class);
				fields.add("subscriptionState", value);
				break;
			case "subscriptionPlanId":
				ArgValidator.checkFieldUriType(value, BuyerBiddingSubscriptionPlan.class, key);
				BuyerBiddingSubscriptionPlan plan = dbClient.queryObject(BuyerBiddingSubscriptionPlan.class, value);
				fields.add("subscriptionPlan", plan);
				break;
			default:
				break;
			}
		}

		BuyerBiddingSubscriptionInfoListResponse response = new BuyerBiddingSubscriptionInfoListResponse();
		ResultSetIterator<BuyerBiddingSubscription> resultSetIterator = null;

		do {

			resultSetIterator = getResultSetIterator(BuyerBiddingSubscription.class, fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					BuyerBiddingSubscription subscription = resultSetIterator.next();
					if (subscription != null) {
						BuyerBiddingSubscriptionInfoResponse infoResponse = SubscriptionResponseMapper.map(subscription,
								TIMESTAMP_FORMAT);
						response.add(infoResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (response.size() < fetchSize);

		logger.info("Exit retuning response with a list of size: {}", response.size());

		return response;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{subscriptionId}/payment-transactions", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and isRelated(#buyerId)")
	public ResponseEntity<CreateResourceResponse> createSubscriptionPaymentTransaction(
			@PathVariable("buyerId") String buyerId, @PathVariable("subscriptionId") String subscriptionId,
			@RequestBody CreatePaymentTransactionParam param) {

		logger.info(
				"Request received to create payment transaction for bidding subscription for buyer ID: {}, subscription ID {}. Request: {}",
				buyerId, subscriptionId, param);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(subscriptionId, BuyerBiddingSubscription.class, "subscription_id");

		PaymentTransactionUtility.validatePaymentTransactionParam(param);

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);

		BuyerBiddingSubscription subscription = dbClient.queryObject(BuyerBiddingSubscription.class, subscriptionId);
		ArgValidator.checkEntityNotNull(subscription, subscriptionId, true);

		forbidSubscriptionAccessToOtherBuyer(subscription, buyerId);

		SubscriptionState activationInProgress = SubscriptionState.ACTIVATION_IN_PROGRESS;
		if (!subscription.hasSubscriptionState(activationInProgress)) {
			String currentState = subscription.getSubscriptionState().name();
			logger.error("Subscription state is {}. So, aborting payment transaction.", currentState);
			throw ForbiddenException.dealerSubscriptionExistsInState(currentState,
					"Payment transaction can only be performed on a subscription with state: ["
							+ activationInProgress.name() + "]");
		}

		/*
		 * Everything is OK. Create the payment transaction.
		 */
		String transactionEntityId = PaymentTransactionUtility.savePaymentTransaction(dbClient, param, Buyer.class,
				buyerId, BuyerBiddingSubscription.class, subscriptionId);
		logger.info("Payment transaction for buyer bidding subscription created successfully.");

		return TaskResponseUtility.createResourceResponse(subscriptionId,
				Endpoint.BUYER_PAYMENT_TRANSACTION_INFO.get(buyerId, transactionEntityId),
				"Bidding subscription payment transaction created successfully.");
	}

	/*
	 * @RequestMapping(method = RequestMethod.GET, path =
	 * "/{subscriptionId}/payment-transactions/{paymentTransactionId}", produces = {
	 * MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	 * 
	 * @PreAuthorize("hasAnyRole('" + Role.BUYER + "','" + Role.ADMIN +
	 * "') and isRelated(#buyerId)") public PaymentTransactionInfoResponse
	 * getSubscriptionPaymentTransaction(@AuthenticationPrincipal BNBNeedsUser user,
	 * 
	 * @PathVariable("buyerId") String buyerId, @PathVariable("subscriptionId")
	 * String subscriptionId,
	 * 
	 * @PathVariable("paymentTransactionId") String paymentTransactionId) {
	 * 
	 * logger.info(
	 * "Request received to get payment transaction for bidding subscription for buyer ID: {}, subscription ID {}, transaction ID: {}"
	 * , buyerId, subscriptionId, paymentTransactionId);
	 * 
	 * ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
	 * ArgValidator.checkFieldUriType(subscriptionId,
	 * BuyerBiddingSubscription.class, "subscription_id");
	 * ArgValidator.checkFieldUriType(subscriptionId, PaymentTransaction.class,
	 * "transaction_id");
	 * 
	 * Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
	 * ArgValidator.checkEntityNotNull(buyer, buyerId, true); if (user.isBuyer()) {
	 * ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true); }
	 * 
	 * BuyerBiddingSubscription subscription =
	 * dbClient.queryObject(BuyerBiddingSubscription.class, subscriptionId);
	 * ArgValidator.checkEntityNotNull(subscription, subscriptionId, true);
	 * 
	 * if (!subscription.getBuyer().getId().equals(buyerId)) { throw
	 * ForbiddenException.unauthorizedAccess(); }
	 * 
	 * PaymentTransaction transaction =
	 * dbClient.queryObject(PaymentTransaction.class, paymentTransactionId);
	 * ArgValidator.checkEntityNotNull(transaction, paymentTransactionId, true);
	 * 
	 * QueryFields fields = new QueryFields("entityId", subscription.getId());
	 * fields.add("paymentTransaction", transaction);
	 * 
	 * PaymentTransactionLinkedEntity paymentTransactionLinkedEntity = dbClient
	 * .queryObjectByFields(PaymentTransactionLinkedEntity.class, fields);
	 * ArgValidator.checkEntityNotNull(paymentTransactionLinkedEntity,
	 * paymentTransactionId, true);
	 * 
	 * logger.info("Exit returning payment transaction info response.");
	 * 
	 * return PaymentTransactionResponseMapper.map(transaction,
	 * Endpoint.BUYER_BIDDING_SUBSCRIPTION_PAYMENT_TRANSACTION_INFO.get(buyerId,
	 * subscriptionId, paymentTransactionId)); }
	 * 
	 * @RequestMapping(method = RequestMethod.GET, path =
	 * "/{subscriptionId}/payment-transactions", produces = {
	 * MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	 * 
	 * @PreAuthorize("hasAnyRole('" + Role.BUYER + "','" + Role.ADMIN +
	 * "') and isRelated(#buyerId)") public PaymentTransactionInfoListResponse
	 * getSubscriptionPaymentTransactions(
	 * 
	 * @AuthenticationPrincipal BNBNeedsUser user, @PathVariable("buyerId") String
	 * buyerId,
	 * 
	 * @PathVariable("subscriptionId") String subscriptionId) {
	 * 
	 * logger.info(
	 * "Request received to get payment transactions for bidding subscription for buyer ID: {}, subscription ID {}"
	 * , buyerId, subscriptionId);
	 * 
	 * ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
	 * ArgValidator.checkFieldUriType(subscriptionId,
	 * BuyerBiddingSubscription.class, "subscription_id");
	 * 
	 * Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
	 * ArgValidator.checkEntityNotNull(buyer, buyerId, true); if (user.isBuyer()) {
	 * ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true); }
	 * 
	 * BuyerBiddingSubscription subscription =
	 * dbClient.queryObject(BuyerBiddingSubscription.class, subscriptionId);
	 * ArgValidator.checkEntityNotNull(subscription, subscriptionId, true);
	 * 
	 * if (!subscription.getBuyer().getId().equals(buyerId)) { throw
	 * ForbiddenException.unauthorizedAccess(); }
	 * 
	 * QueryFields fields = new QueryFields("entityId", subscription.getId());
	 * 
	 * List<PaymentTransactionLinkedEntity> paymentTransactionLinkedEntityList =
	 * dbClient .queryList(PaymentTransactionLinkedEntity.class, fields);
	 * 
	 * PaymentTransactionInfoListResponse response = new
	 * PaymentTransactionInfoListResponse();
	 * 
	 * if (!CollectionUtils.isEmpty(paymentTransactionLinkedEntityList)) { for
	 * (Iterator<PaymentTransactionLinkedEntity> iterator =
	 * paymentTransactionLinkedEntityList .iterator(); iterator.hasNext();) {
	 * PaymentTransactionLinkedEntity paymentTransactionLinkedEntity =
	 * iterator.next(); PaymentTransaction transaction =
	 * paymentTransactionLinkedEntity.getPaymentTransaction();
	 * PaymentTransactionInfoResponse info =
	 * PaymentTransactionResponseMapper.map(transaction,
	 * Endpoint.BUYER_BIDDING_SUBSCRIPTION_PAYMENT_TRANSACTIONS.get(buyerId,
	 * subscriptionId)); response.add(info); } }
	 * 
	 * logger.info("Exit returning payment transaction list response of size {}.",
	 * response.size());
	 * 
	 * return response;
	 * 
	 * }
	 */

	@RequestMapping(method = RequestMethod.PUT, path = "/{subscriptionId}/status", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.BUYER + "', '" + Role.ADMIN + "') and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> updateSubscriptionStatus(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @PathVariable("subscriptionId") String subscriptionId,
			@RequestBody UpdateSubscriptionStatusParam param) {

		logger.info("Request received to update bidding subscription status {} for buyer ID: {}. Request: {}",
				subscriptionId, buyerId, param);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(subscriptionId, BuyerBiddingSubscription.class, "subscription_id");
		ArgValidator.checkFieldNotNull(param, "update_subscription_status_param");
		ArgValidator.checkFieldValueFromEnum(param.getStatus(), "status",
				UpdateSubscriptionStatusParam.SubscriptionStatus.class);

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityNotNull(buyer, buyerId, true);

		if (user.isBuyer()) {
			ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);
		}

		BuyerBiddingSubscription subscription = dbClient.queryObject(BuyerBiddingSubscription.class, subscriptionId);
		ArgValidator.checkEntityNotNull(subscription, subscriptionId, true);

		forbidSubscriptionAccessToOtherBuyer(subscription, buyerId);

		SubscriptionStatus status = SubscriptionStatus.valueOf(param.getStatus());

		boolean statusChanged = BiddingSubscriptionUtility.updateSubscriptionState(dbClient, subscription, status);

		if (!statusChanged) {
			throw BadRequestException.nothingToUpdate();
		}
		dbClient.updateObject(subscription);
		logger.info("Buyer bidding subscription status updated successfully.");
		return TaskResponseUtility.createTaskSuccessResponse("Bidding subscription status updated successfully.");
	}

	@RequestMapping(method = RequestMethod.POST, path = "/create-subscription-with-default-plan", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and isRelated(#buyerId)")
	public ResponseEntity<CreateResourceResponse> createSubscriptionWithDefaultPlan(
			@PathVariable("buyerId") String buyerId) {

		logger.info("Request received to create bidding subscription with default plan for buyer ID: {}.", buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);
		/*
		 * Check if there is/was at-least one subscription associated with the dealer.
		 * If yes, then throw forbidden exception.
		 */
		QueryFields buyerField = new QueryFields("buyer", buyer);
		BuyerBiddingSubscription subscription = dbClient.queryObjectByFields(BuyerBiddingSubscription.class,
				buyerField);

		if (subscription != null) {
			logger.error("Subscription already exists for buyerId: {}. Subscription: {} - {}", buyerId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerAlreadyHasSubsctiption(buyerId);
		}

		/*
		 * Everything is OK. Create the subscription with default plan.
		 */
		subscription = BiddingSubscriptionUtility.createBuyerBiddingSubscriptionWithDefaultPlan(dbClient, buyer);
		logger.info("Buyer bidding subscription with default plan created successfully.");

		return TaskResponseUtility.createResourceResponse(subscription.getId(),
				Endpoint.BUYER_BIDDING_SUBSCRIPTION_INFO.get(buyerId, subscription.getId()),
				"Bidding subscription created successfully.");

	}

}
