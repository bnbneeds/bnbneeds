package com.bnbneeds.app.api.service.pipeline.vendor;

import static com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT.queryVendor;
import static com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT.vendorField;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.TenderBid;
import com.bnbneeds.app.db.client.model.Vendor;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeleteTenderBidsJob extends Job2<Void, String, Boolean> {

	private static final long serialVersionUID = 2454115556985393442L;

	private static final Logger logger = LoggerFactory.getLogger(DeleteTenderBidsJob.class);

	@Override
	public Value<Void> run(String vendorId, Boolean forceDelete) throws Exception {
		Vendor vendor = queryVendor(vendorId);
		if (vendor == null) {
			return null;
		}

		List<TenderBid> tenderBids = queryList(TenderBid.class, vendorField(vendor));

		if (!CollectionUtils.isEmpty(tenderBids)) {
			if (!forceDelete) {
				throw DeleteDealerJob.cannotDeleteHasReferences(vendorId, Vendor.class, TenderBid.class);
			}

			logger.info("Delete tender bids of the vendor [{}]", vendorId);

			DBOperationList operationList = new DBOperationList();

			for (Iterator<TenderBid> iterator = tenderBids.iterator(); iterator.hasNext();) {
				TenderBid tenderBid = iterator.next();
				operationList.addOperation(OperationType.MARK_AS_DELETED, tenderBid);
			}

			transact(operationList);
		}
		return null;
	}
}
