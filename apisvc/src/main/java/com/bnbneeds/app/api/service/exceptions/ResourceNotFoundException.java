package com.bnbneeds.app.api.service.exceptions;

import com.bnbneeds.app.db.client.model.DataObject;

public class ResourceNotFoundException extends APIException {

	private static final long serialVersionUID = 2417486850678036862L;

	private static final String ID_NOT_FOUND_OF_TYPE = "%s not found with id: %s";
	private static final String ID_NOT_FOUND = "Record not found with id: %s";
	private static final String OBJECT_WITH_ID_IS_INACTIVE = "Object with id: %s is inactive.";

	public ResourceNotFoundException() {
		super();
	}

	public ResourceNotFoundException(String message) {
		super(message);
	}

	public static ResourceNotFoundException idNotFound(String id,
			Class<? extends DataObject> clazz) {
		return new ResourceNotFoundException(getMessage(ID_NOT_FOUND_OF_TYPE,
				clazz.getSimpleName(), id));
	}

	public static ResourceNotFoundException idNotFound(String id) {
		return new ResourceNotFoundException(getMessage(ID_NOT_FOUND, id));
	}

	public static ResourceNotFoundException objectIsInactive(String id) {
		return new ResourceNotFoundException(getMessage(
				OBJECT_WITH_ID_IS_INACTIVE, id));
	}

}
