package com.bnbneeds.app.api.service.security;

public interface Role {
	
	String VENDOR = "VENDOR";
	String BUYER = "BUYER";
	String ADMIN = "ADMIN";
	String SUPER_ADMIN = "SUPER_ADMIN";

}
