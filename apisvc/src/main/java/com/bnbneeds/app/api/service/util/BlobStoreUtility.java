package com.bnbneeds.app.api.service.util;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.appengine.api.ThreadManager;
import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

public class BlobStoreUtility {

	private static final Logger logger = LoggerFactory
			.getLogger(BlobStoreUtility.class);

	public static BlobInfo getUploadedBlobKey(
			HttpServletRequest httpServletRequest, String formElementName) {

		BlobstoreService blobstoreService = BlobstoreServiceFactory
				.getBlobstoreService();

		Map<String, List<BlobInfo>> blobs = blobstoreService
				.getBlobInfos(httpServletRequest);

		List<BlobInfo> blobKeys = blobs.get(formElementName);

		if (blobKeys == null) {
			return null;
		}
		BlobInfo blobKey = blobKeys.get(0);
		return blobKey;
	}

	public static void deleteBlobs(Collection<String> blobKeyStrings) {

		BlobKeysDeleteOperation deleteOperation = new BlobStoreUtility.BlobKeysDeleteOperation(
				blobKeyStrings);
		Thread deleteOperationBackgroundThread = ThreadManager
				.createThreadForCurrentRequest(deleteOperation);
		deleteOperationBackgroundThread.start();
	}

	public static void deleteBlob(String blobKeyString) {
		List<String> list = new ArrayList<String>();
		list.add(blobKeyString);
		deleteBlobs(list);

	}

	public static String createUploadURL(String callbackActionURL) {
		BlobstoreService blobstoreService = BlobstoreServiceFactory
				.getBlobstoreService();
		return blobstoreService.createUploadUrl(callbackActionURL);
	}

	public static String createUploadURL(URI callbackActionURL) {
		return createUploadURL(callbackActionURL.toString());
	}

	public static void serveBlob(String blobKeyString,
			HttpServletResponse response) throws IOException {
		BlobstoreService blobstoreService = BlobstoreServiceFactory
				.getBlobstoreService();
		BlobKey blobKey = new BlobKey(blobKeyString);
		blobstoreService.serve(blobKey, response);
	}

	private static class BlobKeysDeleteOperation implements Runnable {

		private Collection<String> blobKeyStrings;
		private BlobstoreService blobstoreService;

		public BlobKeysDeleteOperation(Collection<String> blobKeyStrings) {
			super();
			this.blobstoreService = BlobstoreServiceFactory
					.getBlobstoreService();
			this.blobKeyStrings = new ArrayList<String>();
			if (blobKeyStrings != null && !blobKeyStrings.isEmpty()) {
				this.blobKeyStrings.addAll(blobKeyStrings);
			}
		}

		@Override
		public void run() {
			if (!blobKeyStrings.isEmpty()) {
				for (Iterator<String> iterator = blobKeyStrings.iterator(); iterator
						.hasNext();) {
					String blobKeyString = iterator.next();
					BlobKey blobKey = new BlobKey(blobKeyString);
					try {
						blobstoreService.delete(blobKey);
					} finally {
						logger.debug("Removing item from iterator...");
						iterator.remove();
					}
				}
			}

		}

	}

}
