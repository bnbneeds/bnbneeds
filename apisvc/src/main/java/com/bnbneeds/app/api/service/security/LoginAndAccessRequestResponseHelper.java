package com.bnbneeds.app.api.service.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import org.springframework.http.MediaType;

import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.util.JAXB2XmlJsonUtils;

public abstract class LoginAndAccessRequestResponseHelper {

	protected String getAcceptedMediaType(HttpServletRequest request) {

		String acceptedMediaType = null;
		if (request.getRequestURI().endsWith(".json")) {
			acceptedMediaType = MediaType.APPLICATION_JSON_VALUE;
		} else if (request.getRequestURI().endsWith(".xml")) {
			acceptedMediaType = MediaType.APPLICATION_XML_VALUE;
		} else {
			// Check the Accept header
			acceptedMediaType = request.getHeader("Accept");
		}

		return acceptedMediaType;

	}

	protected void setResponse(HttpServletResponse response,
			String acceptedMediaType, TaskResponse taskResponse)
			throws IOException {

		response.setContentType(acceptedMediaType);

		PrintWriter out = response.getWriter();

		try {

			if (MediaType.APPLICATION_XML_VALUE.equals(acceptedMediaType)) {

				try {
					out.write(JAXB2XmlJsonUtils.pojo2Xml(taskResponse));
				} catch (JAXBException e) {
					throw new IOException(e);
				}
			} else if (MediaType.APPLICATION_JSON_VALUE
					.equals(acceptedMediaType)) {
				out.write(JAXB2XmlJsonUtils.pojo2Json(taskResponse));
			} else {
				response.setContentType(MediaType.APPLICATION_JSON_VALUE);
				out.write(JAXB2XmlJsonUtils.pojo2Json(taskResponse));
			}
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

}
