package com.bnbneeds.app.api.service.response;

import java.net.URI;

import com.bnbneeds.app.db.client.model.PaymentTransaction;
import com.bnbneeds.app.model.dealer.PaymentTransactionInfoResponse;

public class PaymentTransactionResponseMapper extends DataObjectResponseMapper {

	public static void map(PaymentTransaction from, PaymentTransactionInfoResponse to) {
		mapDataObject(from, to);
		to.setAmount(from.getAmount());
		to.setPaymentGateway(from.getPaymentGateway());
		to.setPaymentMethod(from.getPaymentMethod());
		to.setTransactionId(from.getTransactionId());
		to.setTransactionStatus(from.getTransactionStatus());

		// TODO Set timestamp as per response returned from payment gateway.
		to.setTimestamp(from.getTimestamp());
		to.setTransactionDescription(from.getTransactionDescription());
	}

	public static void map(PaymentTransaction from, PaymentTransactionInfoResponse to, URI selfLink) {
		map(from, to);
		to.setLink(selfLink);
	}

	public static PaymentTransactionInfoResponse map(PaymentTransaction from, URI selfLink) {
		PaymentTransactionInfoResponse to = new PaymentTransactionInfoResponse();
		map(from, to, selfLink);
		return to;
	}

}
