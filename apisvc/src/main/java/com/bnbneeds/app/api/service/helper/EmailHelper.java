package com.bnbneeds.app.api.service.helper;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.bnbneeds.app.job.Job;
import com.bnbneeds.app.job.email.EmailWorker;
import com.bnbneeds.app.job.email.EmailWorkerThread;
import com.bnbneeds.app.messaging.email.Attachment;
import com.bnbneeds.app.messaging.email.EmailClient;
import com.bnbneeds.app.messaging.email.EmailMessage;
import com.bnbneeds.app.messaging.email.MessageKeys;
import com.bnbneeds.app.messaging.email.Template;

public class EmailHelper {

	private static final Logger logger = LoggerFactory
			.getLogger(EmailHelper.class);

	private Map<String, EmailClient> emailClients;
	private String emailClientToUse;
	private Properties messages;

	public void setEmailClients(Map<String, EmailClient> emailClients) {
		this.emailClients = emailClients;
	}

	public void setEmailClientToUse(String emailClientToUse) {
		this.emailClientToUse = emailClientToUse;
	}

	public void setMessages(Properties messages) {
		this.messages = messages;
	}

	private EmailClient getClient(String clientName) {
		return emailClients.get(clientName);
	}

	/**
	 * Uses the message key to form the mail subject and body. The supplied mail
	 * body formatter will be used.
	 * 
	 * @param messageKey
	 * @return a new instance of {@code EmailTemplate}
	 * @see MessageKeys
	 */

	public Template createMessageTemplate(MessageKeys messageKey) {
		Template template = new Template(messages, messageKey);
		return template;
	}

	/**
	 * Uses the subject and body to form the mail subject and body.
	 * 
	 * @param subject
	 *            mail subject
	 * @param message
	 *            mail body
	 * @return a new instance of {@code EmailTemplate}
	 *         {@link EmailHelper#createMessageTemplate(MessageKeys)}
	 */
	public Template createMessageTemplate(String subject, String message) {
		Template template = new Template(subject, message);
		return template;
	}

	public void send(Template emailMessage, String... recipients) {
		Assert.notEmpty(recipients, "Recipients cannot be empty.");
		List<String> recipientList = Arrays.asList(recipients);
		send(emailMessage, recipientList);

	}

	public void send(Template emailMessage, List<Attachment> attachments,
			String... recipients) {
		Assert.notEmpty(recipients, "Recipients cannot be empty.");

		List<String> recipientList = Arrays.asList(recipients);
		send(emailMessage, attachments, recipientList);

	}

	public void send(Template emailMessage, List<String> recipients) {
		Assert.notEmpty(recipients, "Recipients cannot be empty.");
		send(emailMessage.getSubject(), emailMessage.getBody(), recipients);
	}

	public void send(Template emailMessage, List<Attachment> attachments,
			List<String> recipients) {
		Assert.notEmpty(recipients, "Recipients cannot be empty.");
		send(emailMessage.getSubject(), emailMessage.getBody(), attachments,
				recipients);
	}

	public void send(MessageKeys messageKey, String[] subjectValues,
			String[] bodyValues, String[] recipients) {
		send(messageKey, subjectValues, bodyValues, recipients, null);
	}

	public void send(MessageKeys messageKey, String[] subjectValues,
			String[] bodyValues, String[] recipients,
			List<Attachment> attachments) {
		Assert.notNull(messageKey, "Message key cannot be null.");
		Template template = createMessageTemplate(messageKey);
		if (subjectValues != null && subjectValues.length > 0) {
			template.resolveSubjectValues(subjectValues);
		}
		if (bodyValues != null && bodyValues.length > 0) {
			template.resolveBodyValues(bodyValues);
		}
		send(template, attachments, recipients);
	}

	public void send(MessageKeys messageKey, String[] recipients) {
		send(messageKey, null, null, recipients);
	}

	public void send(MessageKeys messageKey, String[] bodyValues,
			String[] recipients) {
		send(messageKey, null, bodyValues, recipients);
	}

	public void send(String subject, String message, List<String> recipients) {
		send(subject, message, null, recipients);
	}

	public void send(String subject, String message,
			List<Attachment> attachments, List<String> recipients) {

		EmailMessage email = new EmailMessage(subject, message, recipients);
		email.setAttachments(attachments);
		send(email);
	}

	public void send(EmailMessage email) {

		logger.info("Email client to use: {}", emailClientToUse);

		logger.info("Email: {}", email);

		EmailClient emailClient = getClient(emailClientToUse);

		EmailWorker runner = new EmailWorkerThread(email);
		runner.setEmailClient(emailClient);
		Job job = new Job();
		try {
			job.execute(runner);
		} finally {
			job.stop();
		}
	}
}
