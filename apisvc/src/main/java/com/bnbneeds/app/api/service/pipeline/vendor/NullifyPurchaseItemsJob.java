package com.bnbneeds.app.api.service.pipeline.vendor;

import static com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT.queryVendor;
import static com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT.vendorField;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.PurchaseItem;
import com.bnbneeds.app.db.client.model.Vendor;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class NullifyPurchaseItemsJob extends Job2<Void, String, Boolean> {

	private static final Logger logger = LoggerFactory.getLogger(NullifyPurchaseItemsJob.class);

	private static final long serialVersionUID = 8731128830476154629L;

	@Override
	public Value<Void> run(String vendorId, Boolean forceDelete) throws Exception {
		Vendor vendor = queryVendor(vendorId);
		if (vendor == null) {
			return null;
		}
		Boolean forceDeleteFlag = new Boolean(forceDelete);
		List<PurchaseItem> purchaseItems = queryList(PurchaseItem.class, vendorField(vendor));
		if (!CollectionUtils.isEmpty(purchaseItems)) {
			if (!forceDeleteFlag) {
				throw DeleteDealerJob.cannotDeleteHasReferences(vendorId, Vendor.class, PurchaseItem.class);
			}

			logger.info("Get purchase items of the assigned to vendor: [{}] and nullify them...", vendorId);
			DBOperationList purchaseItemDeleteOperationList = new DBOperationList();
			for (Iterator<PurchaseItem> iterator = purchaseItems.iterator(); iterator.hasNext();) {
				PurchaseItem purchaseItem = iterator.next();
				purchaseItem.setVendorToNull();
				logger.debug("Nullifying vendor field in purchase item: {}", purchaseItem);
				purchaseItemDeleteOperationList.addOperation(OperationType.UPDATE, purchaseItem);
			}
			transact(purchaseItemDeleteOperationList);
		}
		return null;
	}

}
