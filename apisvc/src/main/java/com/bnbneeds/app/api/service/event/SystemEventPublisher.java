package com.bnbneeds.app.api.service.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.util.Assert;

public class SystemEventPublisher implements ApplicationEventPublisherAware {
	
	private static final Logger logger = LoggerFactory.getLogger(SystemEventPublisher.class);

	private ApplicationEventPublisher applicationEventPublisher;
	private SystemEvent event;

	@Override
	public void setApplicationEventPublisher(
			ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;

	}

	/**
	 * Sets the event attributes to publish
	 * 
	 * @param type
	 * @param recipientType
	 * @param recipientId
	 * @param objectId
	 * @param objectName
	 * @param objectLink
	 */
	public void setEventAttributes(SystemEventType type,
			RecipientType recipientType, String recipientId, String objectId,
			String objectName, String objectLink) {
		
		event = new SystemEvent(this);

		event.setType(type);
		event.setRecipientId(recipientId);
		event.setRecipientType(recipientType);
		if (RecipientType.VENDOR.equals(recipientType)
				|| RecipientType.BUYER.equals(recipientType)) {
			Assert.hasText(recipientId, "recipientId cannot be null");
		}
		event.setObjectId(objectId);
		event.setOjectName(objectName);
		event.setLink(objectLink);
		
		logger.info("Setting event attributes: {}", event);
	}

	public void publish() {
		logger.info("Publishing event...");
		Assert.notNull(event, "Event object cannot be null");
		applicationEventPublisher.publishEvent(event);
		event = null;

	}

}
