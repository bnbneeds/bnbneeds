package com.bnbneeds.app.api.service.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.util.NotificationUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.DbClientUtils;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscription;
import com.bnbneeds.app.db.client.model.Dealer;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription.EmailNotificationStatus;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription.SubscriptionState;
import com.bnbneeds.app.db.client.model.Notification;
import com.bnbneeds.app.db.client.model.PurchaseItem;
import com.bnbneeds.app.db.client.model.Tender;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscription;
import com.bnbneeds.app.messaging.email.MessageKeys;
import com.bnbneeds.app.messaging.email.Template;
import com.bnbneeds.app.model.OperationStatus;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.util.DateUtils;

/**
 * See
 * <a href="https://cloud.google.com/appengine/docs/standard/java/config/cron"
 * >Scheduling Tasks With Cron for Java</a>
 * 
 * @author Amit Herlekar
 *
 */

@RestController
@RequestMapping(method = RequestMethod.GET, path = "/cron-jobs/application", produces = {
		MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
public class ApplicationCronJobService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationCronJobService.class);

	@RequestMapping(path = "/delete-purchase-items")
	public ResponseEntity<TaskResponse> deletePurchaseItems(
			@RequestParam(name = "olderThanInDays", defaultValue = "30") int olderThanInDays,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = "50") int fetchSize) {

		List<PurchaseItem> purchaseList = dbClient.queryList(PurchaseItem.class, fetchSize);
		int deletedCount = 0;
		Date dateToday = new Date();
		DBOperationList operationList = new DBOperationList();
		for (Iterator<PurchaseItem> iterator = purchaseList.iterator(); iterator.hasNext();) {
			PurchaseItem purchaseItem = iterator.next();

			Date requestDate = DateUtils.getEndOfDay(new Date(purchaseItem.getRequestTimestamp()));

			if (DateUtils.differenceInDays(requestDate, dateToday) > olderThanInDays) {
				logger.info("Purchase Item: {} with request date: {} will be marked as deleted.",
						purchaseItem.getProductName().getName(), requestDate);
				operationList.addOperation(OperationType.MARK_AS_DELETED, purchaseItem);
				deletedCount++;
			}
		}
		if (!operationList.isEmpty()) {
			dbClient.transact(operationList);
		}

		String message = String.format("Deleted %1$d purchase items older than %2$d days.", deletedCount,
				olderThanInDays);
		logger.info(message);

		return TaskResponseUtility.createTaskSuccessResponse(message);
	}

	/**
	 * Set the properties for subscription expiry.
	 * 
	 * 
	 * @param subscription
	 *            [IN] the dealer bidding subscription
	 * @param dealer
	 *            [IN] the buyer or vendor object
	 * @param operationList
	 *            [OUT] the the operation list
	 * @param dealerAccountEmailAddressList
	 *            [OUT] the user account email addresses of the dealers for which
	 *            email should be sent on expiry. This set is cumulative on each
	 *            invocation. Make sure it is not null.
	 * @param recipientIds
	 *            [OUT] the list of dealer Ids for which notification should be
	 *            recorded for the subscription expiry. This list is cumulative on
	 *            each invocation. Make sure it is not null.
	 */
	private void setPropertiesForDealerBiddingSubscriptionExpiry(DealerBiddingSubscription subscription, Dealer dealer,
			DBOperationList operationList, List<String> dealerAccountEmailAddressList, Set<String> recipientIds) {

		boolean markAsExpired = false;
		switch (subscription.getSubscriptionPlanType()) {
		case DURATION:
			if (System.currentTimeMillis() >= subscription.getExpiryTimestamp()) {
				markAsExpired = true;
			}
			break;
		case QUOTA:
			if (subscription.isQuotaUsageFullOrExceeded()) {
				markAsExpired = true;
			}
			break;
		default:
			break;
		}

		if (markAsExpired) {
			subscription.setSubscriptionState(SubscriptionState.EXPIRED);
			operationList.addOperation(OperationType.UPDATE, subscription);
			UserAccount account = loginHelper.getUserAccountOfDealer(dealer);
			dealerAccountEmailAddressList.add(account.getName());
			recipientIds.add(dealer.getId());

		}
	}

	/**
	 * Set the properties for subscription due for expiry. Marks the subscription
	 * notification state to {@code REMINDER_SENT_BEFORE_EXPIRY_FOR_RENEWAL}
	 * 
	 * 
	 * @param subscription
	 *            [IN] the dealer bidding subscription
	 * @param dealer
	 *            [IN] the buyer or vendor object
	 * @param reminderToBeSentBeforExpiryInDays
	 *            the value in days for the reminder to be sent before the expiry
	 *            date occurs
	 * @param reminderForUsedQuotaPercentage
	 *            [IN] the used percentage quota for which reminder should be sent.
	 * @param operationList
	 *            [OUT] the the operation list
	 * @param dealerAccountEmailAddressList
	 *            [OUT] the user account email addresses of the dealers for which
	 *            email should be sent on expiry. This set is cumulative on each
	 *            invocation. Make sure it is not null.
	 */
	private void setPropertiesForDealerBiddingSubscriptionDueForExpiry(DealerBiddingSubscription subscription,
			Dealer dealer, int reminderToBeSentBeforExpiryInDays, int reminderForUsedQuotaPercentage,
			DBOperationList operationList, List<String> dealerAccountEmailAddressList) {

		boolean sendEmailNotification = false;
		switch (subscription.getSubscriptionPlanType()) {
		case DURATION:
			Date expiryDate = new Date(subscription.getExpiryTimestamp());
			int days = DateUtils.differenceInDays(expiryDate, new Date());
			if (days >= reminderToBeSentBeforExpiryInDays) {
				sendEmailNotification = true;
			}
			break;
		case QUOTA:
			double usedQuotaPercentage = subscription.getUsedQuotaPercentage();
			if (!Double.isNaN(usedQuotaPercentage) && usedQuotaPercentage >= reminderForUsedQuotaPercentage) {
				sendEmailNotification = true;
			}
			break;
		default:
			break;
		}

		if (sendEmailNotification) {
			subscription.setEmailNotificationStatus(EmailNotificationStatus.REMINDER_SENT_BEFORE_EXPIRY_FOR_RENEWAL);
			operationList.addOperation(OperationType.UPDATE, subscription);
			UserAccount account = loginHelper.getUserAccountOfDealer(dealer);
			dealerAccountEmailAddressList.add(account.getName());
		}
	}

	@RequestMapping(path = "/close-buyer-bidding-subscriptions")
	public ResponseEntity<TaskResponse> closeBuyerBiddingSubscriptions(
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = "100") int fetchSize) {

		List<BuyerBiddingSubscription> subscriptions = dbClient.queryList(BuyerBiddingSubscription.class,
				new QueryFields("subscriptionState", SubscriptionState.ACTIVE.name()), fetchSize);

		if (!CollectionUtils.isEmpty(subscriptions)) {
			DBOperationList opList = new DBOperationList();
			DBOperationList notificationOpList = new DBOperationList();
			List<String> emailAddressList = new ArrayList<>();
			Set<String> recipientIds = new HashSet<>();
			for (Iterator<BuyerBiddingSubscription> iterator = subscriptions.iterator(); iterator.hasNext();) {
				BuyerBiddingSubscription biddingSubscription = iterator.next();
				Buyer buyer = biddingSubscription.getBuyer();
				setPropertiesForDealerBiddingSubscriptionExpiry(biddingSubscription, buyer, opList, emailAddressList,
						recipientIds);
				if (biddingSubscription.hasSubscriptionState(SubscriptionState.EXPIRED)) {
					Notification notification = NotificationUtility.createNotification(
							SystemEventType.BUYER_BIDDING_SUBSCRIPTION_EXPIRED, RecipientType.BUYER, buyer.getId(),
							biddingSubscription.getId(), null,
							Endpoint.BUYER_BIDDING_SUBSCRIPTION_INFO.get(buyer.getId(), biddingSubscription.getId()),
							"SYSTEM");
					notificationOpList.addOperation(OperationType.INSERT, notification);
				}
			}
			if (!opList.isEmpty()) {
				Template emailMessage = emailHelper
						.createMessageTemplate(MessageKeys.BUYER_BIDDING_SUBSCRIPTION_EXPIRED);
				emailHelper.send(emailMessage, emailAddressList);
				dbClient.transact(opList);
				dbClient.transact(notificationOpList);
			}
		}
		return TaskResponseUtility.createTaskSubmittedResponse("Task submitted to close buyer bidding subsciptions");
	}

	@RequestMapping(path = "/close-vendor-bidding-subscriptions")
	public ResponseEntity<TaskResponse> closeVendorBiddingSubscriptions(
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = "100") int fetchSize) {

		List<VendorBiddingSubscription> subscriptions = dbClient.queryList(VendorBiddingSubscription.class,
				new QueryFields("subscriptionState", SubscriptionState.ACTIVE.name()), fetchSize);

		if (!CollectionUtils.isEmpty(subscriptions)) {
			DBOperationList opList = new DBOperationList();
			DBOperationList notificationOpList = new DBOperationList();
			List<String> emailAddressList = new ArrayList<>();
			Set<String> recipientIds = new HashSet<>();

			for (Iterator<VendorBiddingSubscription> iterator = subscriptions.iterator(); iterator.hasNext();) {
				VendorBiddingSubscription biddingSubscription = iterator.next();
				Vendor vendor = biddingSubscription.getVendor();
				setPropertiesForDealerBiddingSubscriptionExpiry(biddingSubscription, vendor, opList, emailAddressList,
						recipientIds);
				if (biddingSubscription.hasSubscriptionState(SubscriptionState.EXPIRED)) {
					Notification notification = NotificationUtility.createNotification(
							SystemEventType.VENDOR_BIDDING_SUBSCRIPTION_EXPIRED, RecipientType.VENDOR, vendor.getId(),
							biddingSubscription.getId(), null,
							Endpoint.VENDOR_BIDDING_SUBSCRIPTION_INFO.get(vendor.getId(), biddingSubscription.getId()),
							"SYSTEM");
					notificationOpList.addOperation(OperationType.INSERT, notification);
				}
			}
			if (!opList.isEmpty()) {

				Template emailMessage = emailHelper
						.createMessageTemplate(MessageKeys.VENDOR_BIDDING_SUBSCRIPTION_EXPIRED);
				emailHelper.send(emailMessage, emailAddressList);
				dbClient.transact(opList);
				dbClient.transact(notificationOpList);
			}
		}
		return TaskResponseUtility.createTaskSubmittedResponse("Task submitted to close vendor bidding subsciptions.");
	}

	/**
	 * Sends email notification to dealers about their bidding subscriptions nearing
	 * expiry.
	 * 
	 * @param fetchSize
	 *            the number of records to be fetched in a db query
	 * @return the {@code TaskResponse}
	 */
	@RequestMapping(path = "/send-reminder-about-vendor-bidding-subscription-expiry")
	public ResponseEntity<TaskResponse> sendReminderAboutVendorBiddingSubscriptionsDueForExpiry(
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = "100") int fetchSize,
			@RequestParam(name = "reminderToBeSentBeforExpiryInDays", defaultValue = "15") int reminderToBeSentBeforExpiryInDays,
			@RequestParam(name = "reminderForUsedQuotaPercentage", defaultValue = "80") int reminderForUsedQuotaPercentage) {

		QueryFields fields = new QueryFields("subscriptionState", SubscriptionState.ACTIVE.name());
		fields.add("emailNotificationStatus", EmailNotificationStatus.EXPIRY_REMINDER_NOT_SENT.name());

		List<VendorBiddingSubscription> subscriptions = dbClient.queryList(VendorBiddingSubscription.class, fields,
				fetchSize);

		if (!CollectionUtils.isEmpty(subscriptions)) {
			DBOperationList opList = new DBOperationList();
			List<String> emailAddressList = new ArrayList<>();

			for (Iterator<VendorBiddingSubscription> iterator = subscriptions.iterator(); iterator.hasNext();) {
				VendorBiddingSubscription biddingSubscription = iterator.next();
				Vendor vendor = biddingSubscription.getVendor();
				setPropertiesForDealerBiddingSubscriptionDueForExpiry(biddingSubscription, vendor,
						reminderToBeSentBeforExpiryInDays, reminderForUsedQuotaPercentage, opList, emailAddressList);
			}
			if (!opList.isEmpty()) {

				Template emailMessage = emailHelper
						.createMessageTemplate(MessageKeys.VENDOR_BIDDING_SUBSCRIPTION_DUE_FOR_EXPIRY);
				emailHelper.send(emailMessage, emailAddressList);
				dbClient.transact(opList);
			}
		}
		return TaskResponseUtility.createTaskSubmittedResponse(
				"Task submitted to send reminder about expiry of vendor bidding subsciptions.");
	}

	@RequestMapping(path = "/send-reminder-about-buyer-bidding-subscription-expiry")
	public ResponseEntity<TaskResponse> sendReminderAboutBuyerBiddingSubscriptionsDueForExpiry(
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = "100") int fetchSize,
			@RequestParam(name = "reminderToBeSentBeforExpiryInDays", defaultValue = "15") int reminderToBeSentBeforExpiryInDays,
			@RequestParam(name = "reminderForUsedQuotaPercentage", defaultValue = "80") int reminderForUsedQuotaPercentage) {

		QueryFields fields = new QueryFields("subscriptionState", SubscriptionState.ACTIVE.name());
		fields.add("emailNotificationStatus", EmailNotificationStatus.EXPIRY_REMINDER_NOT_SENT.name());

		List<BuyerBiddingSubscription> subscriptions = dbClient.queryList(BuyerBiddingSubscription.class, fields,
				fetchSize);

		if (!CollectionUtils.isEmpty(subscriptions)) {
			DBOperationList opList = new DBOperationList();
			List<String> emailAddressList = new ArrayList<>();

			for (Iterator<BuyerBiddingSubscription> iterator = subscriptions.iterator(); iterator.hasNext();) {
				BuyerBiddingSubscription biddingSubscription = iterator.next();
				Buyer buyer = biddingSubscription.getBuyer();
				setPropertiesForDealerBiddingSubscriptionDueForExpiry(biddingSubscription, buyer,
						reminderToBeSentBeforExpiryInDays, reminderForUsedQuotaPercentage, opList, emailAddressList);
			}
			if (!opList.isEmpty()) {

				Template emailMessage = emailHelper
						.createMessageTemplate(MessageKeys.BUYER_BIDDING_SUBSCRIPTION_DUE_FOR_EXPIRY);
				emailHelper.send(emailMessage, emailAddressList);
				dbClient.transact(opList);
			}
		}
		return TaskResponseUtility.createTaskSubmittedResponse(
				"Task submitted to send reminder about expiry of buyer bidding subsciptions.");
	}

	@RequestMapping(path = "/close-tenders")
	public ResponseEntity<TaskResponse> closeTendersOnExpiry(
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = "100") int fetchSize) {

		List<Tender> tenders = dbClient.queryList(Tender.class, new QueryFields("status", Tender.Status.OPEN.name()),
				fetchSize);

		if (!CollectionUtils.isEmpty(tenders)) {
			DBOperationList opList = new DBOperationList();
			DBOperationList notificationOpList = new DBOperationList();
			for (Iterator<Tender> iterator = tenders.iterator(); iterator.hasNext();) {
				Tender tender = iterator.next();
				Buyer buyer = tender.getBuyer();
				// if tender has crossed expiry, then close the tender.
				if (System.currentTimeMillis() >= tender.getCloseTimestamp()) {
					tender.setStatus(Tender.Status.CLOSED);
					opList.addOperation(OperationType.UPDATE, tender);
					Notification notification = NotificationUtility.createNotification(SystemEventType.TENDER_CLOSED,
							RecipientType.BUYER, buyer.getId(), tender.getId(), tender.getName(),
							Endpoint.BUYER_TENDER_INFO.get(buyer.getId(), tender.getId()), "SYSTEM");
					notificationOpList.addOperation(OperationType.INSERT, notification);
				}
			}
			dbClient.transact(opList);
			dbClient.transact(notificationOpList);
		}
		return TaskResponseUtility.createTaskSubmittedResponse("Task submitted to close tenders");
	}

	@RequestMapping(path = "/reset-bidding-subscriptions")
	public ResponseEntity<TaskResponse> resetBiddingSubscriptionsActivations(
			@RequestParam(name = "subscriptionEntityType") String subscriptionEntityType,
			@RequestParam(name = "olderThanInDays", defaultValue = "1") int olderThanInDays,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = "50") int fetchSize) {

		final List<String> validEntityTypes = Arrays.asList(new String[] {
				VendorBiddingSubscription.class.getSimpleName(), BuyerBiddingSubscription.class.getSimpleName() });

		if (!validEntityTypes.contains(subscriptionEntityType)) {
			TaskResponse task = new TaskResponse(HttpServletResponse.SC_BAD_REQUEST, OperationStatus.ERROR,
					"Invalid subscriptionType", "ClassNotFoundException");
			return TaskResponseUtility.createTaskResponse(task);
		}

		Class<DealerBiddingSubscription> clazz = null;
		try {
			clazz = DbClientUtils.getClassByName(subscriptionEntityType, DealerBiddingSubscription.class);
		} catch (ClassNotFoundException e) {
			TaskResponse task = new TaskResponse(HttpServletResponse.SC_BAD_REQUEST, OperationStatus.ERROR,
					"Invalid subscriptionType", e.getClass().getSimpleName());
			return TaskResponseUtility.createTaskResponse(task);
		}

		List<DealerBiddingSubscription> subscriptions = dbClient.queryList(clazz,
				new QueryFields("subscriptionState", SubscriptionState.ACTIVATION_IN_PROGRESS.name()), fetchSize);

		if (!CollectionUtils.isEmpty(subscriptions)) {
			DBOperationList opList = new DBOperationList();
			Date currentDate = new Date();
			for (Iterator<DealerBiddingSubscription> iterator = subscriptions.iterator(); iterator.hasNext();) {
				DealerBiddingSubscription biddingSubscription = iterator.next();
				Date lastUpdateddate = DateUtils.getDate(DbClient.TIMESTAMP_PATTERN,
						biddingSubscription.getUpdateTimestamp());
				int differenceInDays = DateUtils.differenceInDays(lastUpdateddate, currentDate);
				if (differenceInDays > olderThanInDays) {
					biddingSubscription.setSubscriptionState(SubscriptionState.ACTIVATION_PENDING);
					opList.addOperation(OperationType.UPDATE, biddingSubscription);
				}
			}
			dbClient.transact(opList);
		}
		return TaskResponseUtility.createTaskSubmittedResponse("Task submitted to reset vendor bidding subsciptions.");
	}
}
