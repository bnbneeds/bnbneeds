package com.bnbneeds.app.api.service.security;

import java.util.Iterator;

import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.Dealer;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.util.StringUtils;

public class UserIdentitySecurityExpressionRoot extends SecurityExpressionRoot
		implements MethodSecurityExpressionOperations {

	private Object filterObject;
	private Object returnObject;
	private Object target;
	private LoginHelper loginHelper;
	private DbClient dbClient;

	private BNBNeedsUser getBNBNeedsUser() {
		return (BNBNeedsUser) getPrincipal();
	}

	/*
	 * private List<String> getRoles() { BNBNeedsUser user = getBNBNeedsUser();
	 * List<String> roles = new ArrayList<String>();
	 * Collection<GrantedAuthority> authorities = user.getAuthorities(); for
	 * (Iterator<GrantedAuthority> iterator = authorities.iterator(); iterator
	 * .hasNext();) { GrantedAuthority grantedAuthority = iterator.next();
	 * 
	 * roles.add(grantedAuthority.getAuthority());
	 * 
	 * }
	 * 
	 * return roles; }
	 */

	public UserIdentitySecurityExpressionRoot(Authentication authentication) {
		super(authentication);
	}

	public void setLoginHelper(LoginHelper loginHelper) {
		this.loginHelper = loginHelper;
	}

	public void setDbClient(DbClient dbClient) {
		this.dbClient = dbClient;
	}

	public boolean isRelated(String objectId) {

		BNBNeedsUser user = getBNBNeedsUser();

		if (user != null && user.isDealer()) {
			if (StringUtils.isEmpty(user.getRelationshipId())) {
				throw ForbiddenException.userNotRegisteredAsDealer(user
						.getRole().toString());
			}
			if (!StringUtils.isEmpty(user.getRelationshipId())
					&& !user.getRelationshipId().equals(objectId)) {
				throw ForbiddenException.unauthorizedAccess();
			}
		}
		return true;
	}

	public boolean hasOtherRoleIfNotRelated(String objectId) {

		boolean hasIdentity = false;

		BNBNeedsUser user = getBNBNeedsUser();

		if (user != null && !StringUtils.isEmpty(user.getRelationshipId())) {
			if (user.getRelationshipId().equals(objectId)) {
				hasIdentity = true;
			} else {
				/*
				 * UserAccount userAccount = loginHelper
				 * .getUserAccountByUsername(login.getUsername());
				 */
				String role = user.getRole();

				for (Iterator<GrantedAuthority> iterator = user
						.getAuthorities().iterator(); iterator.hasNext();) {
					GrantedAuthority grantedAuthority = iterator.next();
					if (!role.equals(grantedAuthority.getAuthority())) {
						hasIdentity = true;
						break;
					}
				}
			}
		}
		return hasIdentity;
	}

	public boolean hasDealership() {

		BNBNeedsUser user = getBNBNeedsUser();

		if (user.isDealer()) {
			if (StringUtils.isEmpty(user.getRelationshipId())) {
				throw ForbiddenException.userNotRegisteredAsDealer(user
						.getRole());
			}
		}

		return true;
	}

	public boolean checkDealerBlacklisted() {
		BNBNeedsUser user = getBNBNeedsUser();
		String relationshipId = user.getRelationshipId();
		if (StringUtils.isEmpty(relationshipId)) {
			throw ForbiddenException.userNotRegisteredAsDealer(user.getRole());
		} else {
			Dealer dealer = null;
			if (user.isBuyer()) {
				dealer = dbClient.queryObject(Buyer.class, relationshipId);
			} else if (user.isVendor()) {
				dealer = dbClient.queryObject(Vendor.class, relationshipId);
			}

			if (dealer != null) {
				if (EntityStatus.BLACKLISTED == dealer.getEntityStatus())
					throw new ForbiddenException("Dealer:" + dealer.getName()
							+ " is blacklisted.");
			}
		}
		return true;
	}

	public boolean canMakeRelationship() {

		BNBNeedsUser login = getBNBNeedsUser();

		if (login != null && !StringUtils.isEmpty(login.getRelationshipId())) {
			throw ForbiddenException.userAlreadyRegisteredAsDealer(login
					.getRole());
		}

		return true;

	}

	@Override
	public void setFilterObject(Object filterObject) {
		this.filterObject = filterObject;

	}

	@Override
	public Object getFilterObject() {
		return filterObject;
	}

	@Override
	public void setReturnObject(Object returnObject) {
		this.returnObject = returnObject;

	}

	@Override
	public Object getReturnObject() {
		return returnObject;
	}

	@Override
	public Object getThis() {
		return target;
	}

	public void setThis(Object target) {
		this.target = target;
	}

}
