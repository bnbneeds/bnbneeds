package com.bnbneeds.app.api.service.response;

import java.text.SimpleDateFormat;

import com.bnbneeds.app.db.client.model.AverageRating;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.BuyerReview;
import com.bnbneeds.app.db.client.model.Product;
import com.bnbneeds.app.db.client.model.ProductReview;
import com.bnbneeds.app.db.client.model.Review;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.db.client.model.VendorReview;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.model.review.AverageRatingResponse;
import com.bnbneeds.app.model.review.BuyerReviewInfoResponse;
import com.bnbneeds.app.model.review.ProductReviewInfoResponse;
import com.bnbneeds.app.model.review.ReviewInfoResponse;
import com.bnbneeds.app.model.review.VendorReviewInfoResponse;
import com.bnbneeds.app.util.StringUtils;

public class ReviewResponseMapper extends DataObjectResponseMapper {

	private static void mapReview(Review from, ReviewInfoResponse to, SimpleDateFormat dateFormat) {
		mapDescribedDataObject(from, to);
		String timestamp = StringUtils.getFormattedDate(from.getTimestamp(), dateFormat);
		to.setTimestamp(timestamp);
		to.setRating(from.getRating());
		String headline = from.getHeadline();
		if (StringUtils.hasText(headline)) {
			to.setHeadline(headline);
		}
	}

	public static void map(BuyerReview from, BuyerReviewInfoResponse to, SimpleDateFormat dateFormat) {
		mapReview(from, to, dateFormat);
		Buyer buyer = from.getBuyer();
		to.setBuyer(buyer.getId(), buyer.getName());
		Vendor vendor = from.getReviewedBy();
		to.setReviewedBy(vendor.getId(), vendor.getName());
		to.setLink(Endpoint.BUYER_REVIEW_INFO.get(buyer.getId(), from.getId()));
	}

	public static void map(VendorReview from, VendorReviewInfoResponse to, SimpleDateFormat dateFormat) {
		mapReview(from, to, dateFormat);
		Vendor vendor = from.getVendor();
		to.setVendor(vendor.getId(), vendor.getName());
		Buyer buyer = from.getReviewedBy();
		to.setReviewedBy(buyer.getId(), buyer.getName());
		to.setLink(Endpoint.VENDOR_REVIEW_INFO.get(vendor.getId(), from.getId()));
	}

	public static void map(ProductReview from, ProductReviewInfoResponse to, SimpleDateFormat dateFormat) {
		mapReview(from, to, dateFormat);
		Product product = from.getProduct();
		to.setProduct(product.getId(), product.getProductName().getName());
		Buyer buyer = from.getReviewedBy();
		to.setReviewedBy(buyer.getId(), buyer.getName());
		to.setLink(Endpoint.PRODUCT_REVIEW_INFO.get(product.getId(), from.getId()));
	}

	public static ProductReviewInfoResponse map(ProductReview from, SimpleDateFormat dateFormat) {
		ProductReviewInfoResponse to = new ProductReviewInfoResponse();
		mapReview(from, to, dateFormat);
		map(from, to, dateFormat);
		return to;
	}

	public static BuyerReviewInfoResponse map(BuyerReview from, SimpleDateFormat dateFormat) {
		BuyerReviewInfoResponse to = new BuyerReviewInfoResponse();
		mapReview(from, to, dateFormat);
		map(from, to, dateFormat);

		return to;
	}

	public static VendorReviewInfoResponse map(VendorReview from, SimpleDateFormat dateFormat) {
		VendorReviewInfoResponse to = new VendorReviewInfoResponse();
		mapReview(from, to, dateFormat);
		map(from, to, dateFormat);
		return to;
	}

	public static void map(AverageRating from, AverageRatingResponse to) {
		mapDataObject(from, to);
		to.setAverageRating(from.getAverageRating());
		to.setFiveStarPercent(from.getFiveStarRatingPercentage());
		to.setFourStarPercent(from.getFourStarRatingPercentage());
		to.setThreeStarPercent(from.getThreeStarRatingPercentage());
		to.setTwoStarPercent(from.getTwoStarRatingPercentage());
		to.setOneStarPercent(from.getOneStarRatingPercentage());
	}

	public static AverageRatingResponse map(AverageRating from) {
		AverageRatingResponse to = new AverageRatingResponse();
		map(from, to);
		return to;
	}
}
