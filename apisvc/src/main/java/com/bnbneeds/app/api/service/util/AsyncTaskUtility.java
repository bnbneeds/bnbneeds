package com.bnbneeds.app.api.service.util;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;

public class AsyncTaskUtility {
	private static final Logger logger = LoggerFactory.getLogger(AsyncTaskUtility.class);

	public static void addTaskToQueue(String queueName, String workerURI, Map<String, String> paramMap) {
		logger.info("Adding task to the queue: [{}]. Worker URI: [{}], params: {}", queueName, workerURI, paramMap);
		Queue queue = QueueFactory.getQueue(queueName);
		TaskOptions options = TaskOptions.Builder.withUrl(workerURI);
		if (paramMap != null && !paramMap.isEmpty()) {
			for (String param : paramMap.keySet()) {
				String value = paramMap.get(param);
				options.param(param, value);
			}
		}
		queue.add(options);
	}

}
