package com.bnbneeds.app.api.service.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.exceptions.APIException;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.response.UserAccountResponseMapper;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.DealerUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.UserAccount;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.account.AccountType;
import com.bnbneeds.app.model.account.UpdateAccountStatusParam;
import com.bnbneeds.app.model.account.UpdateUserAccountParam;
import com.bnbneeds.app.model.account.UserAccountInfoListResponse;
import com.bnbneeds.app.model.account.UserAccountInfoResponse;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/dealers/user-accounts")
@PreAuthorize("hasRole('" + Role.ADMIN + "')")
public class DealerUserAccountService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(DealerUserAccountService.class);

	private static void checkDealerUserAccountRole(UserAccount account) {

		if (!account.isDealer()) {
			throw BadRequestException.create("Only Dealer user accounts can be deleted.");
		}
	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public UserAccountInfoListResponse getDealerUserAccountInfoList(
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) throws APIException {

		final String[] QUERY_FILTER_KEYS = { "type", "username" };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields();

		for (String key : keyValueMap.keySet()) {
			String value = keyValueMap.get(key);
			switch (key) {
			case "type":
				ArgValidator.checkFieldValueFromArray(AccountType.valueOf(value), key, AccountType.VENDOR,
						AccountType.BUYER);
				fields.add("role", value.toUpperCase());
				break;
			case "username":
				fields.add("name", value.toLowerCase());
				break;
			default:
				break;
			}
		}

		ResultSetIterator<UserAccount> resultSetIterator = null;
		UserAccountInfoListResponse response = new UserAccountInfoListResponse();
		do {
			resultSetIterator = getResultSetIterator(UserAccount.class, fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					UserAccount userAccount = resultSetIterator.next();
					if (userAccount != null) {
						if (userAccount.isAdmin()) {
							continue;
						}
						UserAccountInfoResponse infoResponse = UserAccountResponseMapper.map(userAccount);
						response.add(infoResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());

				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (response.size() < fetchSize);
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public UserAccountInfoResponse getAccount(@PathVariable("id") String id) throws APIException {

		logger.info("Request received to get user account info : {}", id);
		ArgValidator.checkFieldUriType(id, UserAccount.class, "id");

		UserAccount account = dbClient.queryObject(UserAccount.class, id);

		ArgValidator.checkEntityNotNull(account, id, true);

		if (account.isAdmin()) {
			throw ForbiddenException.unauthorizedAccess();
		}

		UserAccountInfoResponse resp = UserAccountResponseMapper.map(account);

		return resp;

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> deleteDealerUserAccount(@PathVariable("id") String id) throws APIException {

		logger.info("Request received to delete user account: {}", id);
		ArgValidator.checkFieldUriType(id, UserAccount.class, "id");

		UserAccount account = dbClient.queryObject(UserAccount.class, id);

		ArgValidator.checkEntityNotNull(account, id, true);

		checkDealerUserAccountRole(account);
		String relationshipId = account.getRelationshipId();

		if (StringUtils.hasText(relationshipId) && account.isBuyer()) {
			Buyer buyer = dbClient.queryObject(Buyer.class, relationshipId);
			if (buyer != null) {
				DealerUtility.deleteBuyer(relationshipId, true, true);
			}
		}

		if (StringUtils.hasText(relationshipId) && account.isVendor()) {
			Vendor vendor = dbClient.queryObject(Vendor.class, relationshipId);
			if (vendor != null) {
				DealerUtility.deleteVendor(relationshipId, true, true);
			}
		}

		logger.info("User account deleted successfully: {}", id);

		return TaskResponseUtility.createTaskSubmittedResponse("Task submitted to delete dealer user account.");
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/{id}/approvals", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> updateAccountStatus(@PathVariable("id") String id,
			@RequestBody UpdateAccountStatusParam accountStatusParam) throws APIException {

		logger.info("API request received to update user account status: {}", accountStatusParam);

		ArgValidator.checkFieldUriType(id, UserAccount.class, "id");
		UserAccount account = dbClient.queryObject(UserAccount.class, id);
		ArgValidator.checkEntityNotNull(account, id, true);

		checkDealerUserAccountRole(account);

		boolean changed = false;

		if (accountStatusParam.isDisabled()) {
			account.setDisabled(true);
			changed = true;
		} else if (accountStatusParam.isEnabled()) {
			account.setDisabled(false);
			changed = true;
		}

		if (!changed) {
			throw BadRequestException.create("Nothing to update in the user account.");
		}

		logger.debug("Saving updated user account Status record in DB: {}.", account);
		dbClient.updateObject(account);

		logger.info("Updated user account status persisted in DB: {}", account);

		return TaskResponseUtility.createTaskSuccessResponse("User account status updated successfully.");
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TaskResponse> updateAccount(@PathVariable("id") String id,
			@RequestBody UpdateUserAccountParam accountParam) throws APIException {

		logger.info("API request received to update user account: {} for account ID: {}", accountParam, id);

		ArgValidator.checkFieldUriType(id, UserAccount.class, "id");
		UserAccount account = dbClient.queryObject(UserAccount.class, id);
		ArgValidator.checkEntityNotNull(account, id, true);

		checkDealerUserAccountRole(account);

		boolean changed = false;
		boolean sendConfirmationCodeToNewEmailAddress = false;
		boolean sendConfirmationCodeToNewMobileNumber = false;

		String existingUsername = account.getName();
		String newUsername = accountParam.getUsername();
		String accountName = accountParam.getAccountName();
		String mobileNumber = accountParam.getMobileNumber();
		AccountType type = accountParam.getType();

		if (!StringUtils.isEmpty(newUsername) && !existingUsername.equals(newUsername)) {
			ArgValidator.checkFieldValue(StringUtils.isEmailAddressValid(newUsername), "email_address");
			account.setName(newUsername);
			account.setEmailAddressVerified(false);
			sendConfirmationCodeToNewEmailAddress = true;
			changed = true;
		}

		if (!StringUtils.isEmpty(accountName) && !accountName.equals(account.getAccountName())) {
			account.setAccountName(accountName);
			changed = true;
		}

		if (!StringUtils.isEmpty(mobileNumber) && !mobileNumber.equals(account.getMobileNumber())) {
			ArgValidator.checkFieldValue(StringUtils.isPhoneNumberValid(mobileNumber), "mobile_number");
			account.setMobileNumber(mobileNumber);
			account.setMobileNumberVerified(false);
			sendConfirmationCodeToNewMobileNumber = true;
			changed = true;
		}

		if (type != null) {
			ArgValidator.checkFieldValueFromArray(type, "account_type", AccountType.VENDOR, AccountType.BUYER);
			ArgValidator.checkField(!StringUtils.hasText(account.getRelationshipId()), "account_type",
					"Cannot update account type. User already has dealer organization registered.");

			account.setRole(type);
			changed = true;
		}

		if (!changed) {
			throw BadRequestException.create("Nothing to update in the user account.");
		}

		if (sendConfirmationCodeToNewEmailAddress) {
			String emailAddressConfirmationCode = StringUtils.generateConfirmationCode();
			account.setEmailAddressConfirmationCode(emailAddressConfirmationCode);

			// TODO send email notification with confirmation code
			logger.info("Email address confirmation code: {}", emailAddressConfirmationCode);
		}
		if (sendConfirmationCodeToNewMobileNumber) {
			String mobileNumberConfirmationCode = StringUtils.generateConfirmationCode();
			account.setMobileNumberConfirmationCode(mobileNumberConfirmationCode);
			// TODO send SMS notification with confirmation code
			logger.info("Mobile number confirmation code: {}", mobileNumberConfirmationCode);
		}
		logger.debug("Saving updated user account record creating in DB: {}.", account);
		dbClient.updateObject(account);

		logger.info("Updated user account persisted in DB: {}", account);

		return TaskResponseUtility.createTaskSuccessResponse("User account updated successfully.");
	}
}
