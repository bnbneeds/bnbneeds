package com.bnbneeds.app.api.service.pipeline.buyer;

import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryObjectByFields;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.BuyerAverageRating;
import com.bnbneeds.app.db.client.model.BuyerReview;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeleteReviewsJob extends Job2<Void, String, Boolean> {

	private static final Logger logger = LoggerFactory.getLogger(DeleteReviewsJob.class);

	private static final long serialVersionUID = -5450015286774528141L;

	@Override
	public Value<Void> run(String buyerId, Boolean forceDelete) throws Exception {
		Buyer buyer = DeleteBuyerJobROOT.queryBuyer(buyerId);
		if (buyer == null) {
			return null;
		}
		List<BuyerReview> buyerReviews = queryList(BuyerReview.class, DeleteBuyerJobROOT.buyerField(buyer));
		if (!CollectionUtils.isEmpty(buyerReviews)) {

			if (!forceDelete) {
				throw DeleteDealerJob.cannotDeleteHasReferences(buyerId, Buyer.class, BuyerReview.class);
			}
			DBOperationList reviewDeleteOperationList = new DBOperationList();
			logger.info("Get the reviews of the buyer [{}] and delete them...", buyerId);
			for (Iterator<BuyerReview> iterator = buyerReviews.iterator(); iterator.hasNext();) {
				BuyerReview buyerReview = iterator.next();
				reviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, buyerReview);
			}

			BuyerAverageRating averageRating = queryObjectByFields(BuyerAverageRating.class,
					DeleteBuyerJobROOT.buyerField(buyer));
			if (averageRating != null) {
				logger.debug("Delete the buyer average rating: {}", buyerId);
				reviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, averageRating);
			}
			transact(reviewDeleteOperationList);
		}
		return null;
	}
}
