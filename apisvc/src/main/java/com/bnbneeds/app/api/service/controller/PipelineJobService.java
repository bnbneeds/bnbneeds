package com.bnbneeds.app.api.service.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.pipeline.PipelineUtils;
import com.bnbneeds.app.model.PipelineJobInfoResponse;
import com.google.appengine.tools.pipeline.NoSuchObjectException;
import com.google.appengine.tools.pipeline.PipelineService;
import com.google.appengine.tools.pipeline.impl.model.JobRecord;

@RestController
@RequestMapping(path = "/pipeline-jobs")
public class PipelineJobService extends ResourceService {

	@RequestMapping(method = RequestMethod.GET, value = "/{jobId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	public PipelineJobInfoResponse getJobInfo(@PathVariable("jobId") String jobId) throws NoSuchObjectException {
		PipelineService service = PipelineUtils.getPipelineService();
		JobRecord jobInfo = (JobRecord) service.getJobInfo(jobId);
		return PipelineUtils.mapJobInfo(jobInfo);
	}

}
