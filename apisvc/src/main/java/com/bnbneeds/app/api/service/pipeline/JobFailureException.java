package com.bnbneeds.app.api.service.pipeline;

public class JobFailureException extends RuntimeException {

	private static final long serialVersionUID = -4056975787464401648L;

	public JobFailureException() {
		super();
	}

	public JobFailureException(String message, Throwable cause) {
		super(message, cause);
	}

	public JobFailureException(String message) {
		super(message);
	}

	public JobFailureException(Throwable cause) {
		super(cause);
	}

}
