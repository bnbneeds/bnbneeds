package com.bnbneeds.app.api.service.pipeline.buyer;

import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryObject;

import com.bnbneeds.app.api.service.documentsearch.Constants;
import com.bnbneeds.app.api.service.documentsearch.SearchAPIUtility;
import com.bnbneeds.app.api.service.util.DealerUtility;
import com.bnbneeds.app.db.client.model.Buyer;
import com.google.appengine.api.search.Document;
import com.google.appengine.tools.pipeline.Job0;
import com.google.appengine.tools.pipeline.Value;

public class ReindexBuyerDocumentJob extends Job0<Void> {

	private static final long serialVersionUID = 7758526280568388045L;

	private String buyerId;

	public ReindexBuyerDocumentJob(String buyerId) {
		super();
		this.buyerId = buyerId;
	}

	@Override
	public String getJobDisplayName() {
		return "Reindex document for buyer with ID: " + buyerId;
	}

	@Override
	public Value<Void> run() throws Exception {

		Buyer buyer = queryObject(Buyer.class, buyerId);
		if (buyer == null) {
			return null;
		}

		SearchAPIUtility.deleteDocument(Constants.BUYER_INDEX, buyerId);
		Document buyerDoc = DealerUtility.createSearchDocumentForBuyer(buyer);
		SearchAPIUtility.indexDocument(Constants.BUYER_INDEX, buyerDoc, true);

		return null;
	}

}
