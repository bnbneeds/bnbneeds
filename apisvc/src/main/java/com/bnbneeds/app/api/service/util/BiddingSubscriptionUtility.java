package com.bnbneeds.app.api.service.util;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.BiddingSubscriptionPlan;
import com.bnbneeds.app.db.client.model.BiddingSubscriptionPlan.PlanType;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscription;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscriptionPlan;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription.EmailNotificationStatus;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription.SubscriptionState;
import com.bnbneeds.app.db.client.model.PaymentTransaction;
import com.bnbneeds.app.db.client.model.PaymentTransactionLinkedEntity;
import com.bnbneeds.app.db.client.model.SubscriptionPlan.DurationType;
import com.bnbneeds.app.db.client.model.Tender;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscription;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscriptionPlan;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscriptionUsage;
import com.bnbneeds.app.model.dealer.subscription.UpdateSubscriptionStatusParam.SubscriptionStatus;
import com.bnbneeds.app.util.DateUtils;

public class BiddingSubscriptionUtility {

	private static final Logger logger = LoggerFactory.getLogger(BiddingSubscriptionUtility.class);

	/**
	 * Validates the current state of the subscription. If state is ACTIVE,
	 * ACTIVATION_IN_PROGRESS or EXPIRED then exception is thrown
	 * 
	 * @param dealerId
	 *            the buyer or vendor ID
	 * @param subscription
	 *            the subscription
	 * @throws ForbiddenException
	 *             if the subscriptions state is ACTIVE, ACTIVATION_IN_PROGRESS or
	 *             EXPIRED
	 */
	public static void validateUpdateBiddingSubscriptionRequest(String dealerId,
			DealerBiddingSubscription subscription) {

		SubscriptionState stateEnum = subscription.getSubscriptionState();
		String state = stateEnum.name();
		switch (stateEnum) {
		case ACTIVE:
			logger.error("Subscription already exists for dealerId: {}. Subscription: {} - {}", dealerId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerSubscriptionExistsInState(state,
					"Cannot update the subscription which is active.");
		case ACTIVATION_IN_PROGRESS:
			logger.error("Subscription already exists for dealerId: {}. Subscription: {} - {}", dealerId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerSubscriptionExistsInState(state,
					"Cannot update the subscription as activation is in progress.");
		case EXPIRED:
			logger.error("Subscription already exists for dealerId: {}. Subscription: {} - {}", dealerId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerSubscriptionExistsInState(state,
					"Cannot update the subscription which is expired. Create a new subscription.");
		default:
			break;
		}

	}

	/**
	 * Validates the current state of the subscription. If state is ACTIVE,
	 * ACTIVATION_IN_PROGRESS or EXPIRED then exception is thrown
	 * 
	 * @param dealerId
	 *            the buyer or vendor ID
	 * @param subscription
	 *            the subscription
	 * @throws ForbiddenException
	 *             if the subscriptions state is ACTIVE, ACTIVATION_IN_PROGRESS or
	 *             EXPIRED
	 */
	public static void validateDeleteBiddingSubscriptionRequest(String dealerId,
			DealerBiddingSubscription subscription) {

		SubscriptionState stateEnum = subscription.getSubscriptionState();
		String state = stateEnum.name();
		switch (stateEnum) {
		case ACTIVE:
			logger.error("Subscription already exists for dealerId: {}. Subscription: {} - {}", dealerId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerSubscriptionExistsInState(state,
					"Cannot delete the subscription which is active.");
		case ACTIVATION_IN_PROGRESS:
			logger.error("Subscription already exists for dealerId: {}. Subscription: {} - {}", dealerId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerSubscriptionExistsInState(state,
					"Cannot delete the subscription as activation is in progress.");
		case EXPIRED:
			logger.error("Subscription already exists for dealerId: {}. Subscription: {} - {}", dealerId,
					subscription.getId(), subscription.getSubscriptionState().name());
			throw ForbiddenException.dealerSubscriptionExistsInState(state,
					"Cannot delete the subscription which is expired.");
		default:
			break;
		}

	}

	// Method is public as it will be called after dealer registration and there
	// wont be any
	// payment associated with the subscription. That is the only place from which
	// this method can be
	// called.
	private static boolean activateBiddingSubscription(DbClient dbClient, DealerBiddingSubscription subscription) {

		boolean statusChanged = false;
		subscription.setSubscriptionState(SubscriptionState.ACTIVE);

		switch (subscription.getSubscriptionPlanType()) {
		case DURATION:
			BiddingSubscriptionPlan plan = subscription.getSubscriptionPlan();
			DurationType durationType = plan.getDurationTypeEnum();
			int durationValue = plan.getDurationValue();
			Date expirydate = null;
			Date currentDate = new Date();
			subscription.setActivationTimestamp(currentDate.getTime());
			switch (durationType) {
			case days:
				expirydate = DateUtils.addDays(currentDate, durationValue);
				break;
			case months:
				expirydate = DateUtils.addMonths(currentDate, durationValue);
				break;
			case years:
				expirydate = DateUtils.addYears(currentDate, durationValue);
				break;
			default:
				break;
			}
			expirydate = DateUtils.getEndOfDay(expirydate);
			subscription.setExpiryTimestamp(expirydate.getTime());
			statusChanged = true;
			break;
		case QUOTA:
			subscription.resetQuotaUsage();
			statusChanged = true;
			break;
		default:
			break;
		}
		return statusChanged;
	}

	public static boolean checkPaymentAndActivateBiddingSubscription(DbClient dbClient,
			DealerBiddingSubscription subscription) {

		boolean statusChanged = false;
		// Check the payment transaction status.
		QueryFields fields = new QueryFields("entityId", subscription.getId());
		List<PaymentTransactionLinkedEntity> transactionLinkedEntityList = dbClient
				.queryList(PaymentTransactionLinkedEntity.class, fields);
		if (CollectionUtils.isEmpty(transactionLinkedEntityList)) {
			throw BadRequestException.subscriptionCannotBeActivatedAsNoSuccessfulPaymentTransactionFound();
		}
		boolean successfulTransactionFound = false;
		for (Iterator<PaymentTransactionLinkedEntity> iterator = transactionLinkedEntityList.iterator(); iterator
				.hasNext();) {

			// Look for a successful transaction.
			PaymentTransactionLinkedEntity paymentTransactionLinkedEntity = iterator.next();
			PaymentTransaction transaction = paymentTransactionLinkedEntity.getPaymentTransaction();

			if (transaction.isTransactionSuccess()) {
				successfulTransactionFound = true;
				break;
			}
		}
		if (successfulTransactionFound) {
			statusChanged = activateBiddingSubscription(dbClient, subscription);
		} else {
			throw BadRequestException.subscriptionCannotBeActivatedAsNoSuccessfulPaymentTransactionFound();
		}
		return statusChanged;
	}

	public static void initialize(DealerBiddingSubscription subscription) {
		subscription.setSubscriptionState(SubscriptionState.ACTIVATION_PENDING);
		subscription.setEmailNotificationStatus(EmailNotificationStatus.EXPIRY_REMINDER_NOT_SENT);
		subscription.setCreatedTimestamp(System.currentTimeMillis());
		invalidateBiddingSubscription(subscription);
	}

	public static void invalidateBiddingSubscription(DealerBiddingSubscription subscription) {
		subscription.invalidateActivationTimestamp();
		subscription.invalidateExpiryTimestamp();
		subscription.invalidateQuotaUsage();
	}

	public static void resetSubscription(DealerBiddingSubscription subscription) {
		subscription.setSubscriptionState(SubscriptionState.ACTIVATION_PENDING);
	}

	public static boolean updateSubscriptionState(DbClient dbClient, DealerBiddingSubscription subscription,
			SubscriptionStatus status) {

		boolean statusChanged = false;
		SubscriptionState currentState = subscription.getSubscriptionState();
		switch (subscription.getSubscriptionState()) {
		case ACTIVE:
		case EXPIRED:
			throw BadRequestException.subscriptionStatusCannotBeUpdatedAsStateIsInvalid(currentState.name());
		default:
			break;
		}

		switch (status) {
		case ACTIVATE:
			if (!subscription.hasSubscriptionState(SubscriptionState.ACTIVATION_IN_PROGRESS)) {
				throw BadRequestException.subscriptionCannotBeActivatedAsStateIsInvalid(currentState.name());
			}
			statusChanged = checkPaymentAndActivateBiddingSubscription(dbClient, subscription);
			break;
		case INITIATE_ACTIVATION:
			if (!subscription.hasSubscriptionState(SubscriptionState.ACTIVATION_PENDING)) {
				throw BadRequestException.subscriptionCannotBeActivatedAsStateIsInvalid(currentState.name());
			}
			BiddingSubscriptionPlan plan = subscription.getSubscriptionPlan();
			if (plan.isDispproved() || !plan.isAvailable()) {
				// This is the case where the subscription is pending for activation for a long
				// time but the associated plan is no longer in APPROVED state or AVAILABLE.
				throw BadRequestException
						.cannotInitiateActivationForSubscriptionWithReason("The subscription plan: " + plan.getName()
								+ ", is no longer available. Please update the subscription by choosing another plan");
			}
			subscription.setSubscriptionState(SubscriptionState.ACTIVATION_IN_PROGRESS);
			statusChanged = true;
			break;
		case ACTIVATION_FAILED:
			if (!subscription.hasSubscriptionState(SubscriptionState.ACTIVATION_IN_PROGRESS)) {
				throw BadRequestException.subscriptionStatusCannotBeUpdatedAsStateIsInvalid(currentState.name());
			}
			subscription.setSubscriptionState(SubscriptionState.ACTIVATION_PENDING);
			statusChanged = true;
			break;
		default:
			break;
		}
		return statusChanged;
	}

	/**
	 * Checks if the tender is accessed by the vender or not.
	 * 
	 * @param dbClient
	 *            the DB Client
	 * @param vendor
	 *            the vendor
	 * @param tender
	 *            the tender
	 * @return true if tender is already accessed by the vendor; false otherwise.
	 */
	public static boolean isTenderAccessed(DbClient dbClient, Vendor vendor, Tender tender) {
		boolean accessed = false;

		QueryFields fields = new QueryFields("vendor", vendor);
		fields.add("tender", tender);
		VendorBiddingSubscriptionUsage usage = dbClient.queryObjectByFields(VendorBiddingSubscriptionUsage.class,
				fields);
		accessed = (usage != null);
		return accessed;
	}

	/**
	 * Post processing after accessing a tender. Increment QuotaUsage in the
	 * subscription if the planType is Quota and add the tender to subscription
	 * usage.
	 * 
	 * @param dbClient
	 *            the DB Client
	 * @param vendor
	 *            the vendor
	 * @param subscription
	 *            the vendor subscription
	 * @param tender
	 *            the tender accessed
	 */
	public static void updateSubscriptionUsage(DbClient dbClient, Vendor vendor, VendorBiddingSubscription subscription,
			Tender tender) {

		DBOperationList opList = new DBOperationList();
		if (subscription.hasPlanOfType(PlanType.QUOTA)) {
			subscription.incrementQuotaUsage(1);
			opList.addOperation(OperationType.UPDATE, subscription);
		}

		VendorBiddingSubscriptionUsage usage = new VendorBiddingSubscriptionUsage();
		String uid = UUIDUtil.createId(VendorBiddingSubscriptionUsage.class);
		usage.setId(uid);
		usage.setTender(tender);
		usage.setVendor(vendor);
		usage.setSubscription(subscription);
		opList.addOperation(OperationType.INSERT, usage);

		dbClient.transact(opList);
	}

	public static BuyerBiddingSubscription createBuyerBiddingSubscriptionWithDefaultPlan(DbClient dbClient,
			Buyer buyer) {
		BuyerBiddingSubscription subscription = new BuyerBiddingSubscription();
		QueryFields fields = new QueryFields("defaultPlan", true);
		BuyerBiddingSubscriptionPlan subscriptionPlan = dbClient.queryObjectByFields(BuyerBiddingSubscriptionPlan.class,
				fields);
		if (subscriptionPlan == null || !subscriptionPlan.isApproved() || !subscriptionPlan.isAvailable()) {
			throw BadRequestException.create("There is no default plan in buyer bidding subscription plans.");
		}

		String subscriptionId = UUIDUtil.createId(BuyerBiddingSubscription.class);
		subscription = new BuyerBiddingSubscription();
		subscription.setId(subscriptionId);
		subscription.setBuyer(buyer);
		subscription.setSubscriptionPlan(subscriptionPlan);
		BiddingSubscriptionUtility.initialize(subscription);
		BiddingSubscriptionUtility.activateBiddingSubscription(dbClient, subscription);
		dbClient.createObject(subscription);

		return subscription;
	}

	public static VendorBiddingSubscription createVendorBiddingSubscriptionWithDefaultPlan(DbClient dbClient,
			Vendor vendor) {
		VendorBiddingSubscription subscription = new VendorBiddingSubscription();
		QueryFields fields = new QueryFields("defaultPlan", true);
		VendorBiddingSubscriptionPlan subscriptionPlan = dbClient
				.queryObjectByFields(VendorBiddingSubscriptionPlan.class, fields);
		if (subscriptionPlan == null || !subscriptionPlan.isApproved() || !subscriptionPlan.isAvailable()) {
			throw BadRequestException.create("There is no default plan in vendor bidding subscription plans.");
		}
		String subscriptionId = UUIDUtil.createId(VendorBiddingSubscription.class);
		subscription = new VendorBiddingSubscription();
		subscription.setId(subscriptionId);
		subscription.setVendor(vendor);
		subscription.setSubscriptionPlan(subscriptionPlan);
		BiddingSubscriptionUtility.initialize(subscription);
		BiddingSubscriptionUtility.activateBiddingSubscription(dbClient, subscription);
		dbClient.createObject(subscription);

		return subscription;
	}
}
