package com.bnbneeds.app.api.service.pipeline;

import java.io.IOException;
import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;

import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.PipelineJobInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.google.appengine.tools.pipeline.Job0;
import com.google.appengine.tools.pipeline.JobInfo;
import com.google.appengine.tools.pipeline.NoSuchObjectException;
import com.google.appengine.tools.pipeline.PipelineService;
import com.google.appengine.tools.pipeline.PipelineServiceFactory;
import com.google.appengine.tools.pipeline.impl.model.JobRecord;

public class PipelineUtils {

	public static PipelineService getPipelineService() {
		return PipelineServiceFactory.newPipelineService();
	}

	public static String startPipelineService(Job0<?> job) {
		PipelineService service = getPipelineService();
		return service.startNewPipeline(job);
	}

	public static JobInfo getJobInfo(String pipelineId) {
		PipelineService service = getPipelineService();
		JobInfo info = null;
		try {
			info = service.getJobInfo(pipelineId);
		} catch (NoSuchObjectException e) {
		}
		return info;
	}

	public static String getPipelineStatusUrl(String pipelineId) {
		return "/_ah/pipeline/status.html?root=" + pipelineId;
	}

	public static void redirectToPipelineStatus(HttpServletResponse resp, String pipelineId) throws IOException {
		String destinationUrl = getPipelineStatusUrl(pipelineId);
		resp.sendRedirect(destinationUrl);
	}

	public static void mapJobInfo(JobRecord from, PipelineJobInfoResponse to) {
		to.setId(from.getKey().getName());
		to.setStatus(from.getJobState().name());
	}

	public static PipelineJobInfoResponse mapJobInfo(JobRecord from) {
		PipelineJobInfoResponse to = new PipelineJobInfoResponse();
		mapJobInfo(from, to);
		return to;

	}

	public static ResponseEntity<CreateResourceResponse> createPipelineResponse(String pipelineId) {
		String pipelineURL = getPipelineStatusUrl(pipelineId);
		URI href = URI.create(pipelineURL);
		return TaskResponseUtility.createResourceResponse(pipelineId, href, "Pipeline created for the submitted job.");
	}

	public static ResponseEntity<CreateResourceResponse> createPipelineJobInfoResponse(String jobId, String message) {
		return TaskResponseUtility.createResourceResponse(jobId, Endpoint.PIPELINE_JOB_INFO.get(jobId), message);
	}

}
