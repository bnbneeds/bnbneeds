package com.bnbneeds.app.api.service.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnbneeds.app.api.service.event.RecipientType;
import com.bnbneeds.app.api.service.event.SystemEventType;
import com.bnbneeds.app.api.service.exceptions.BadRequestException;
import com.bnbneeds.app.api.service.exceptions.ForbiddenException;
import com.bnbneeds.app.api.service.request.TenderRequestMapper;
import com.bnbneeds.app.api.service.response.ReviewResponseMapper;
import com.bnbneeds.app.api.service.response.TenderResponseMapper;
import com.bnbneeds.app.api.service.security.BNBNeedsUser;
import com.bnbneeds.app.api.service.security.Role;
import com.bnbneeds.app.api.service.util.ArgValidator;
import com.bnbneeds.app.api.service.util.NotificationUtility;
import com.bnbneeds.app.api.service.util.TaskResponseUtility;
import com.bnbneeds.app.api.service.util.TenderUtility;
import com.bnbneeds.app.db.client.OrderBy;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.UUIDUtil;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.BiddingSubscriptionPlan.PlanType;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscription;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscriptionUsage;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription.SubscriptionState;
import com.bnbneeds.app.db.client.model.Notification;
import com.bnbneeds.app.db.client.model.ProductName;
import com.bnbneeds.app.db.client.model.Tender;
import com.bnbneeds.app.db.client.model.Tender.Status;
import com.bnbneeds.app.db.client.model.TenderBid;
import com.bnbneeds.app.db.client.model.TenderProduct;
import com.bnbneeds.app.db.client.model.VendorAverageRating;
import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.dealer.tender.AddTenderProductsParam;
import com.bnbneeds.app.model.dealer.tender.CreateTenderParam;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderBidInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderProductInfoListResponse;
import com.bnbneeds.app.model.dealer.tender.TenderProductInfoResponse;
import com.bnbneeds.app.model.dealer.tender.TenderProductParam;
import com.bnbneeds.app.model.dealer.tender.UpdateTenderParam;
import com.bnbneeds.app.model.dealer.tender.UpdateTenderProductParam;
import com.bnbneeds.app.model.dealer.tender.UpdateTenderStatusParam;
import com.bnbneeds.app.model.dealer.tender.UpdateTenderStatusParam.TenderStatus;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.util.StringUtils;

@RestController
@RequestMapping(value = "/buyers/{buyerId}/tenders")
public class TenderService extends ResourceService {

	private static final Logger logger = LoggerFactory.getLogger(TenderService.class);

	private static final Map<TenderStatus, Status> TENDER_STATUS_MAP = new HashMap<>();

	static {
		TENDER_STATUS_MAP.put(TenderStatus.OPEN, Status.OPEN);
		TENDER_STATUS_MAP.put(TenderStatus.CANCEL, Status.CANCELLED);
		TENDER_STATUS_MAP.put(TenderStatus.EDIT, Status.EDIT_IN_PROGRESS);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and isRelated(#buyerId)")
	public ResponseEntity<CreateResourceResponse> createTender(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @RequestBody CreateTenderParam param) {

		logger.info("API request received to create a new tender: {}", param);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyerId");
		ArgValidator.checkFieldNotNull(param, "create_tender");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);

		TenderUtility.validateCreateTenderRequest(param, DATE_FORMAT);

		QueryFields queryFields = new QueryFields();
		queryFields.add("name", param.getName());
		queryFields.add("buyer", buyer);
		Tender tenderEntity = dbClient.queryObjectByFields(Tender.class, queryFields);

		if (tenderEntity != null) {
			logger.error("Tender with the given name already exists: {}", param.getName());
			throw BadRequestException.nameAlreadyExists("Tender", param.getName());
		}

		// Check subscription validity.
		BuyerBiddingSubscription subscription = verifySubscriptionValidity(buyer);

		tenderEntity = TenderRequestMapper.createAndMapCreateTenderRequest(param, DATE_FORMAT);
		tenderEntity.setBuyer(buyer);
		logger.debug("New tender record creating in DB: {}", tenderEntity);

		dbClient.createObject(tenderEntity);

		updateSubscriptionUsage(subscription, tenderEntity);

		String uid = tenderEntity.getId();

		logger.info("New tender persisted in DB with ID: {}", uid);

		return TaskResponseUtility.createResourceResponse(uid, Endpoint.BUYER_TENDER_INFO.get(buyerId, uid),
				"New tender created successfully");
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/{tenderId}", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> updateTender(@PathVariable("buyerId") String buyerId,
			@PathVariable("tenderId") String tenderId, @RequestBody UpdateTenderParam param) {

		logger.info("Request received to update Tender {} for buyer ID: {}. Request: {}", tenderId, buyerId, param);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tender_id");
		ArgValidator.checkFieldNotNull(param, "update_tender");

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);

		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityNotNull(tender, tenderId, true);

		if (!tender.getBuyer().getId().equals(buyerId)) {
			throw ForbiddenException.unauthorizedAccess();
		}

		TenderUtility.validateAndMapUpdateTenderRequest(param, tender, DATE_FORMAT);

		/*
		 * Everything is OK. Update the tender.
		 */

		dbClient.updateObject(tender);
		logger.info("Tender updated successfully.");

		return TaskResponseUtility.createTaskSuccessResponse("Tender updated successfully.");
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/{tenderId}/status", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.BUYER + "', '" + Role.ADMIN + "') and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> updateTenderStatus(@PathVariable("buyerId") String buyerId,
			@PathVariable("tenderId") String tenderId, @RequestBody UpdateTenderStatusParam param) {

		logger.info("Request received to update Tender status {} for buyer ID: {}. Request: {}", tenderId, buyerId,
				param);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tender_id");
		ArgValidator.checkFieldNotNull(param, "update_tender_status");
		TenderStatus requestStatus = param.getStatus();
		ArgValidator.checkFieldValueFromEnum(requestStatus.toString(), "status", TenderStatus.class);

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);

		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityNotNull(tender, tenderId, true);

		if (!tender.getBuyer().getId().equals(buyerId)) {
			throw ForbiddenException.unauthorizedAccess();
		}

		Status currentStatus = TENDER_STATUS_MAP.get(requestStatus);
		if (tender.hasTenderStatus(currentStatus)) {
			throw BadRequestException.parameterInvalid("status",
					"The tender status is already " + currentStatus.name());
		}

		TenderUtility.validateUpdateTenderStatusRequest(dbClient, buyerId, tender, param);

		/*
		 * Everything is OK. Update the tender status.
		 */
		updateTenderTimestamps(requestStatus, tender);

		if (TenderStatus.CANCEL == param.getStatus()) {
			tender.addRemark("Reason for cancellation: " + param.getReasonForCancellation());
		}

		tender.setStatus(TENDER_STATUS_MAP.get(requestStatus));

		dbClient.updateObject(tender);

		if (tender.hasTenderStatus(Status.OPEN)) {
			recordEvent(SystemEventType.TENDER_OPEN, RecipientType.ALL_VENDORS, tender.getId(), tender.getName(),
					Endpoint.TENDER_PREVIEW.get(tender.getId()), null);
		}

		logger.info("Tender status updated successfully.");

		return TaskResponseUtility.createTaskSuccessResponse("Tender status updated successfully.");
	}

	private void updateTenderTimestamps(TenderStatus requestStatus, Tender tender) {

		switch (requestStatus) {
		case OPEN:
			Date currentDate = new Date();
			Date closeDate = new Date(tender.getCloseTimestamp());

			if (!TenderUtility.hasCorrectDiffernceInDaysForTenderCloseDate(currentDate, closeDate)) {
				throw ForbiddenException
						.create(TenderUtility.getErrorMessageForIncorrectTenderCloseDate(currentDate, DATE_FORMAT)
								+ " Please update the tender to change the close date accordingly.");
			}
			tender.setOpenTimestamp(currentDate.getTime());
			tender.setCloseTimestamp(tender.getCloseTimestamp());
			break;
		case EDIT:
			tender.setOpenTimestamp(0l);
			break;
		default:
			break;

		}
	}

	@RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.BUYER + "', '" + Role.ADMIN + "') and isRelated(#buyerId)")
	public TenderInfoListResponse getTenders(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get Tenders for buyer ID: {}.", buyerId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityNotNull(buyer, buyerId, true);

		if (user.isBuyer()) {
			ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);
		}
		final String[] QUERY_FILTER_KEYS = { "status" };

		Map<String, String> keyValueMap = getKeyValuePairFromFilter(filter, QUERY_FILTER_KEYS);

		QueryFields fields = new QueryFields("buyer", buyer);

		for (String key : keyValueMap.keySet()) {

			String value = keyValueMap.get(key);

			switch (key) {
			case "status":
				ArgValidator.checkFieldValueFromEnum(value, key, Tender.Status.class);
				fields.add("status", value);
				break;
			default:
				break;
			}
		}

		TenderInfoListResponse response = new TenderInfoListResponse();
		ResultSetIterator<Tender> resultSetIterator = null;

		do {

			resultSetIterator = getResultSetIterator(Tender.class, fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					Tender tender = resultSetIterator.next();
					if (tender != null) {
						TenderInfoResponse infoResponse = TenderResponseMapper.map(tender,
								Endpoint.BUYER_TENDER_INFO.get(buyerId, tender.getId()));
						response.add(infoResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (response.size() < fetchSize);

		logger.info("Returning response with a list of size: {}", response.size());

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.BUYER + "', '" + Role.ADMIN + "') and isRelated(#buyerId)")
	public TenderInfoResponse getTender(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @PathVariable("tenderId") String tenderId) {

		logger.info("Request received to get Tender ID: {}.", tenderId);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tender_id");
		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityNotNull(buyer, buyerId, true);

		if (user.isBuyer()) {
			ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);
		}

		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityNotNull(tender, tenderId, true);

		if (user.isBuyer()) {
			ArgValidator.checkEntityBlacklisted(tender, tenderId, true);
		}

		if (!buyerId.equals(tender.getBuyer().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		TenderInfoResponse response = TenderResponseMapper.map(tender,
				Endpoint.BUYER_TENDER_INFO.get(buyerId, tenderId));

		logger.info("Returning details for tender: {}", response);

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/products", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasAnyRole('" + Role.BUYER + "', '" + Role.ADMIN + "') and isRelated(#buyerId)")
	public TenderProductInfoListResponse getTenderProducts(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @PathVariable("tenderId") String tenderId,
			@RequestParam(name = REQ_PARAM_FILTER, required = false) String filter,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get Bid products for Tender ID: {}.", tenderId);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tender_id");
		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityNotNull(buyer, buyerId, true);

		if (user.isBuyer()) {
			ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);
		}

		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityNotNull(tender, tenderId, true);
		if (user.isBuyer()) {
			ArgValidator.checkEntityBlacklisted(tender, tenderId, true);
		}

		if (!buyerId.equals(tender.getBuyer().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		// Get BidProducts for tender
		TenderProductInfoListResponse bidProductList = new TenderProductInfoListResponse();
		ResultSetIterator<TenderProduct> resultSetIterator = null;
		do {
			QueryFields fields = new QueryFields("tender", tender);
			resultSetIterator = getResultSetIterator(TenderProduct.class, fields, offset, fetchSize);

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					TenderProduct bidProduct = resultSetIterator.next();
					if (bidProduct != null) {
						TenderProductInfoResponse infoResponse = TenderResponseMapper.map(bidProduct,
								Endpoint.BUYER_TENDER_PRODUCT_INFO.get(buyerId, tenderId, bidProduct.getId()));
						bidProductList.add(infoResponse);
					}
				}
				bidProductList.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (bidProductList.size() < fetchSize);

		logger.info("Returning Bid Products for tender: {}", tender);

		return bidProductList;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/{tenderId}/products", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> addTenderProducts(@PathVariable("buyerId") String buyerId,
			@PathVariable("tenderId") String tenderId, @RequestBody AddTenderProductsParam bulkParam,
			@AuthenticationPrincipal BNBNeedsUser user) {

		logger.info("API request received to add a new subscription plan: {}", bulkParam);
		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyerId");
		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tenderId");
		ArgValidator.checkFieldNotNull(bulkParam, "add_bid_products");

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);

		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(tender, tenderId, true);

		if (!buyerId.equals(tender.getBuyer().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		TenderUtility.checkIfTenderIsAllowedToManageBidProducts(buyerId, tender);

		DBOperationList dbOperationList = new DBOperationList();

		for (Iterator<TenderProductParam> iterator = bulkParam.getBidProductList().iterator(); iterator.hasNext();) {
			TenderProductParam param = iterator.next();

			// Check if the Tender Status allows adding Bid Products ie the status is
			// "CREATED" OR "EDIT_IN_PROGRESS"
			TenderUtility.validateAddBidProductRequest(param, tender, buyerId);

			ProductName productName = dbClient.queryObject(ProductName.class, param.getProductNameId());
			ArgValidator.checkEntityDisapprovedOrBlacklisted(productName, param.getProductNameId(), false);

			QueryFields queryFields = new QueryFields();
			queryFields.add("product", productName).add("tender", tender);
			TenderProduct bidProductEntity = dbClient.queryObjectByFields(TenderProduct.class, queryFields);

			if (bidProductEntity != null) {
				logger.error("Tender Product with the given product name already exists: {}", param.getProductNameId());
				throw BadRequestException.nameAlreadyExists("Tender product", productName.getName());
			}

			bidProductEntity = TenderRequestMapper.createAndMapNewBidProduct(param);
			bidProductEntity.setTender(tender);
			bidProductEntity.setProduct(productName);

			dbOperationList.addOperation(OperationType.INSERT, bidProductEntity);
		}

		logger.debug("New bid products in a DB transaction: {}", dbOperationList);

		dbClient.transact(dbOperationList);

		String logMessage = "Task initiated to create bid products.";

		return TaskResponseUtility.createTaskSubmittedResponse(logMessage);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/{tenderId}/products/{bidProductId}", consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
					MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> updateTenderProduct(@PathVariable("buyerId") String buyerId,
			@PathVariable("tenderId") String tenderId, @PathVariable("bidProductId") String bidProductId,
			@RequestBody UpdateTenderProductParam param) {

		logger.info("Request received to update product {} for tender ID: {}. Request: {}", bidProductId, tenderId,
				param);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tender_id");
		ArgValidator.checkFieldUriType(bidProductId, TenderProduct.class, "tender_product_id");
		ArgValidator.checkFieldNotNull(param, "update_tender_product");

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(buyer, buyerId, true);

		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityNotNull(tender, tenderId, true);

		if (!tender.getBuyer().getId().equals(buyerId)) {
			throw ForbiddenException.unauthorizedAccess();
		}

		TenderProduct bidProduct = dbClient.queryObject(TenderProduct.class, bidProductId);
		ArgValidator.checkEntityNotNull(bidProduct, bidProductId, true);
		if (!bidProduct.getTender().getId().equals(tenderId)) {
			throw ForbiddenException.unauthorizedAccess();
		}

		TenderUtility.checkIfTenderIsAllowedToManageBidProducts(buyerId, tender);

		/*
		 * Everything is OK. Update the Bid Product.
		 */
		if (!StringUtils.isEmpty(param.getProductNameId())
				&& !bidProduct.getProduct().getId().equals(param.getProductNameId())) {
			ProductName productName = dbClient.queryObject(ProductName.class, param.getProductNameId());
			ArgValidator.checkEntityDisapprovedOrBlacklisted(productName, param.getProductNameId(), false);
			bidProduct.setProduct(productName);
		}

		if (StringUtils.hasText(param.getQualitySpecification())) {
			bidProduct.setQualitySpecification(param.getQualitySpecification());
		}
		if (StringUtils.hasText(param.getSpecialRequirements())) {
			bidProduct.setSpecialRequirements(param.getSpecialRequirements());
		}
		if (StringUtils.hasText(param.getUnit())) {
			bidProduct.setUnit(param.getUnit());
		}
		if (param.getQuantity() != null) {
			bidProduct.setQuantity(param.getQuantity());
		}

		dbClient.updateObject(bidProduct);
		logger.info("Bid Product updated successfully.");

		return TaskResponseUtility.createTaskSuccessResponse("Bid Product updated successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/bids", produces = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and isRelated(#buyerId)")
	public TenderBidInfoListResponse getTenderBids(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @PathVariable("tenderId") String tenderId,
			@RequestParam(name = REQ_PARAM_FETCH_SIZE, defaultValue = DEFAULT_RESULT_FETCH_SIZE) int fetchSize,
			@RequestParam(name = REQ_PARAM_NEXT_OFFSET, required = false) String offset) {

		logger.info("Request received to get bids of tender: {}", tenderId);

		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tenderId");
		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyerId");

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);
		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityBlacklisted(tender, tender.getId(), true);

		if (!tender.getBuyer().getId().equals(buyerId)) {
			throw ForbiddenException.unauthorizedAccess();
		}

		QueryFields fields = new QueryFields("tender", tender);
		TenderBidInfoListResponse response = new TenderBidInfoListResponse();
		ResultSetIterator<TenderBid> resultSetIterator = null;
		do {

			resultSetIterator = dbClient.queryResultSetIteratorBySortOrder(TenderBid.class, fields, offset, fetchSize,
					OrderBy.ASC, "value");

			if (resultSetIterator != null) {
				while (resultSetIterator.hasNext()) {
					TenderBid bid = resultSetIterator.next();
					if (bid != null) {

						if (bid.getVendor().isBlacklisted()) {
							continue;
						}

						TenderBidInfoResponse infoResponse = TenderResponseMapper.map(bid);
						VendorAverageRating vendorAverageRating = dbClient.queryObjectByFields(
								VendorAverageRating.class, new QueryFields("vendor", bid.getVendor()));
						if (vendorAverageRating != null) {
							infoResponse.setVendorAverageRating(ReviewResponseMapper.map(vendorAverageRating));
						}
						response.add(infoResponse);
					}
				}
				response.setNextOffset(resultSetIterator.getOffset());
				if (offset != null && offset.equals(resultSetIterator.getOffset())) {
					break;
				}
				offset = resultSetIterator.getOffset();
			} else {
				break;
			}

		} while (response.size() < fetchSize);
		logger.info("Exit returning response list of size: {}", response.size());
		return response;
	}

	/**
	 * Check validity in BuyerBiddingSubscription. The conditions checked when
	 * subscription is active for both plan types DURATION and QUOTA. Since this
	 * method is used only in
	 * {@link #createTender(BNBNeedsUser, String, CreateTenderParam)} it will close
	 * (expire the subscription if either quota is full or expiry time has crossed.
	 * This will enable the buyer to subscribe to new plan immediately rather than
	 * waiting for the cron job to execute and mark the subscription to EXPIRED.
	 * 
	 * @param buyer
	 * @return the buyer bidding subscription if the it is valid
	 */
	private BuyerBiddingSubscription verifySubscriptionValidity(Buyer buyer) {
		QueryFields subscriptionQueryfields = new QueryFields("buyer", buyer);
		subscriptionQueryfields.add("subscriptionState", SubscriptionState.ACTIVE.name());
		BuyerBiddingSubscription subscription = dbClient.queryObjectByFields(BuyerBiddingSubscription.class,
				subscriptionQueryfields);
		if (subscription == null) {
			throw ForbiddenException.dealerSubscriptionDoesNotExist("Buyer", buyer.getName());
		}
		boolean subscriptionQuotaFull = false;
		boolean subscriptionExpiryDateCrossed = false;
		PlanType planType = subscription.getSubscriptionPlanType();
		switch (planType) {
		case QUOTA:
			if (subscription.isQuotaUsageFullOrExceeded()) {
				logger.info("Subscription quota is full for subscription: {}", subscription.getId());
				subscriptionQuotaFull = true;
			}
			break;
		case DURATION:
			long expiryTimestamp = subscription.getExpiryTimestamp();
			long currentTimestamp = System.currentTimeMillis();
			if (currentTimestamp >= expiryTimestamp) {
				logger.info("Subscription expiry timestamp has crossed for subscription: {}", subscription.getId());
				subscriptionExpiryDateCrossed = true;
			}
		default:
			break;
		}

		if (subscriptionQuotaFull || subscriptionExpiryDateCrossed) {
			subscription.setSubscriptionState(SubscriptionState.EXPIRED);
			DBOperationList opList = new DBOperationList();
			opList.addOperation(OperationType.UPDATE, subscription);
			logger.info("Setting the bidding subscription to EXPIRED: {}", subscription.getId());
			Notification notification = NotificationUtility.createNotification(
					SystemEventType.BUYER_BIDDING_SUBSCRIPTION_EXPIRED, RecipientType.BUYER, buyer.getId(),
					subscription.getId(), null,
					Endpoint.BUYER_BIDDING_SUBSCRIPTION_INFO.get(buyer.getId(), subscription.getId()), "SYSTEM");
			opList.addOperation(OperationType.INSERT, notification);
			dbClient.transact(opList);

			if (subscriptionQuotaFull) {
				throw ForbiddenException.dealerSubscriptionQuotaExceeded("Buyer", buyer.getName());
			}

			if (subscriptionExpiryDateCrossed) {
				long expiryTimestamp = subscription.getExpiryTimestamp();
				String expiryTimestampText = StringUtils.getFormattedDate(expiryTimestamp, TIMESTAMP_FORMAT);
				throw ForbiddenException.dealerSubscriptionDueToBeMarkedExpired(expiryTimestampText);
			}
		}

		return subscription;
	}

	/**
	 * Post processing after creating a tender. Increment QuotaUsage in the
	 * subscription if the planType is Quota and add the tender to subscription
	 * usage.
	 * 
	 * @param subscription
	 *            the buyer subscription
	 * @param tender
	 *            the tender accessed
	 */
	private void updateSubscriptionUsage(BuyerBiddingSubscription subscription, Tender tender) {
		DBOperationList opList = new DBOperationList();

		if (subscription.hasPlanOfType(PlanType.QUOTA)) {
			subscription.incrementQuotaUsage(1);
			opList.addOperation(OperationType.UPDATE, subscription);
		}

		BuyerBiddingSubscriptionUsage usage = new BuyerBiddingSubscriptionUsage();
		String uid = UUIDUtil.createId(BuyerBiddingSubscriptionUsage.class);
		usage.setId(uid);
		usage.setTender(tender);
		usage.setBuyer(subscription.getBuyer());
		usage.setSubscription(subscription);
		opList.addOperation(OperationType.INSERT, usage);

		dbClient.transact(opList);
	}

	/*
	 * @RequestMapping(method = RequestMethod.DELETE, value = "/{tenderId}",
	 * produces = { MediaType.APPLICATION_XML_VALUE,
	 * MediaType.APPLICATION_JSON_VALUE })
	 * 
	 * @PreAuthorize("hasRole('" + Role.BUYER +
	 * "') and hasDealership() and isRelated(#buyerId)") public
	 * ResponseEntity<TaskResponse> deleteTender(@PathVariable("buyerId") String
	 * buyerId,
	 * 
	 * @PathVariable("tenderId") String tenderId) {
	 * 
	 * logger.info("Request received to delete tender {}", tenderId);
	 * 
	 * ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
	 * ArgValidator.checkFieldUriType(tenderId, Tender.class, "tender_id");
	 * 
	 * Tender tender = dbClient.queryObject(Tender.class, tenderId);
	 * 
	 * ArgValidator.checkEntityDisapprovedOrBlacklisted(tender, tenderId, true);
	 * ArgValidator.checkEntityDisapprovedOrBlacklisted(tender.getBuyer(), buyerId,
	 * true); if (!buyerId.equals(tender.getBuyer().getId())) { throw
	 * ForbiddenException.unauthorizedAccess(); }
	 * 
	 * if (tender.hasTenderStatus(Status.OPEN)) { throw
	 * ForbiddenException.create("Cannot delete a tender which is OPEN for bidding."
	 * ); }
	 * 
	 * DBOperationList opList = new DBOperationList();
	 * 
	 * List<BidProduct> bidProducts = dbClient.queryList(BidProduct.class, new
	 * QueryFields("tender", tender)); if (!CollectionUtils.isEmpty(bidProducts)) {
	 * for (Iterator<BidProduct> iterator = bidProducts.iterator();
	 * iterator.hasNext();) { BidProduct bidProduct = iterator.next();
	 * opList.addOperation(OperationType.MARK_AS_DELETED, bidProduct); } }
	 * 
	 * List<TenderBid> bids = dbClient.queryList(TenderBid.class, new
	 * QueryFields("tender", tender)); if (!CollectionUtils.isEmpty(bids)) { for
	 * (Iterator<TenderBid> iterator = bids.iterator(); iterator.hasNext();) {
	 * TenderBid bid = iterator.next();
	 * opList.addOperation(OperationType.MARK_AS_DELETED, bid); } }
	 * opList.addOperation(OperationType.MARK_AS_DELETED, tender);
	 * 
	 * if (!opList.isEmpty()) { logger.info("Task initiated to delete tender: {}",
	 * tenderId); dbClient.transact(opList); }
	 * 
	 * return
	 * TaskResponseUtility.createTaskSuccessResponse("Tender deleted successfully."
	 * ); }
	 */

	@RequestMapping(method = RequestMethod.DELETE, value = "/{tenderId}/products/{bidProductId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and hasDealership() and isRelated(#buyerId)")
	public ResponseEntity<TaskResponse> deleteBidProduct(@PathVariable("buyerId") String buyerId,
			@PathVariable("tenderId") String tenderId, @PathVariable("bidProductId") String bidProductId) {

		logger.info("Request received to delete product {} belonging to Tender {}", bidProductId, tenderId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tender_id");
		ArgValidator.checkFieldUriType(bidProductId, TenderProduct.class, "tender_product_id");

		Tender tender = dbClient.queryObject(Tender.class, tenderId);

		ArgValidator.checkEntityDisapprovedOrBlacklisted(tender, tenderId, true);
		ArgValidator.checkEntityDisapprovedOrBlacklisted(tender.getBuyer(), buyerId, true);
		if (!buyerId.equals(tender.getBuyer().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		TenderProduct bidProduct = dbClient.queryObject(TenderProduct.class, bidProductId);
		ArgValidator.checkEntityNotNull(bidProduct, bidProductId, true);
		if (!tenderId.equals(bidProduct.getTender().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		TenderUtility.checkIfTenderIsAllowedToManageBidProducts(buyerId, tender);

		dbClient.markAsDeleted(bidProduct);

		return TaskResponseUtility.createTaskSuccessResponse("Bid Product deleted successfully.");
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{tenderId}/products/{bidProductId}", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and hasDealership() and isRelated(#buyerId)")
	public TenderProductInfoResponse getTenderProduct(@PathVariable("buyerId") String buyerId,
			@PathVariable("tenderId") String tenderId, @PathVariable("bidProductId") String bidProductId) {

		logger.info("Request received to get product {} belonging to Tender {}", bidProductId, tenderId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyer_id");
		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tender_id");
		ArgValidator.checkFieldUriType(bidProductId, TenderProduct.class, "tender_product_id");

		Tender tender = dbClient.queryObject(Tender.class, tenderId);

		ArgValidator.checkEntityBlacklisted(tender, tenderId, true);
		ArgValidator.checkEntityBlacklisted(tender.getBuyer(), buyerId, true);
		if (!buyerId.equals(tender.getBuyer().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		TenderProduct bidProduct = dbClient.queryObject(TenderProduct.class, bidProductId);
		ArgValidator.checkEntityNotNull(bidProduct, bidProductId, true);
		if (!tenderId.equals(bidProduct.getTender().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		TenderProductInfoResponse response = TenderResponseMapper.map(bidProduct,
				Endpoint.BUYER_TENDER_PRODUCT_INFO.get(buyerId, tenderId, bidProductId));

		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{tenderId}/bids/lowest-bid", produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@PreAuthorize("hasRole('" + Role.BUYER + "') and isRelated(#buyerId)")
	public TenderBidInfoResponse getLowestBid(@AuthenticationPrincipal BNBNeedsUser user,
			@PathVariable("buyerId") String buyerId, @PathVariable("tenderId") String tenderId) {

		logger.info("Request received to get my bid of tender: {}", tenderId);

		ArgValidator.checkFieldUriType(buyerId, Buyer.class, "buyerId");
		ArgValidator.checkFieldUriType(tenderId, Tender.class, "tenderId");

		Buyer buyer = dbClient.queryObject(Buyer.class, buyerId);
		ArgValidator.checkEntityBlacklisted(buyer, buyerId, true);
		Tender tender = dbClient.queryObject(Tender.class, tenderId);
		ArgValidator.checkEntityBlacklisted(tender, tenderId, true);

		if (!buyerId.equals(tender.getBuyer().getId())) {
			throw ForbiddenException.unauthorizedAccess();
		}

		QueryFields fields = new QueryFields("tender", tender);
		TenderBidInfoResponse response = new TenderBidInfoResponse();
		TenderBid bid = null;
		List<TenderBid> bidList = dbClient.queryListBySortOrder(TenderBid.class, fields, 1, OrderBy.ASC, "value");
		if (!bidList.isEmpty()) {
			bid = bidList.get(0);
			TenderResponseMapper.map(bid, response);
			VendorAverageRating vendorRating = dbClient.queryObjectByFields(VendorAverageRating.class,
					new QueryFields("vendor", bid.getVendor()));
			if (vendorRating != null) {
				response.setVendorAverageRating(ReviewResponseMapper.map(vendorRating));
			}
		}

		logger.info("Exit returning response : {}", response);
		return response;
	}

}
