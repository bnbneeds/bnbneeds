package com.bnbneeds.app.api.service.response;

import java.net.URI;
import java.text.SimpleDateFormat;

import com.bnbneeds.app.api.service.util.PaymentTransactionUtility;
import com.bnbneeds.app.db.client.model.BiddingSubscriptionPlan;
import com.bnbneeds.app.db.client.model.BiddingSubscriptionPlan.PlanType;
import com.bnbneeds.app.db.client.model.Buyer;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscription;
import com.bnbneeds.app.db.client.model.BuyerBiddingSubscriptionPlan;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription;
import com.bnbneeds.app.db.client.model.DealerBiddingSubscription.EmailNotificationStatus;
import com.bnbneeds.app.db.client.model.Vendor;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscription;
import com.bnbneeds.app.db.client.model.VendorBiddingSubscriptionPlan;
import com.bnbneeds.app.model.dealer.subscription.BuyerBiddingSubscriptionInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.DealerBiddingSubscriptionInfoResponse;
import com.bnbneeds.app.model.dealer.subscription.VendorBiddingSubscriptionInfoResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;
import com.bnbneeds.app.util.StringUtils;

public class SubscriptionResponseMapper extends DataObjectResponseMapper {

	private static void mapSubscription(DealerBiddingSubscription from, DealerBiddingSubscriptionInfoResponse to,
			SimpleDateFormat dateFormat) {
		mapDataObject(from, to);
		String createdTimestampText = StringUtils.getFormattedDate(from.getCreatedTimestamp(), dateFormat);
		to.setSubscriptionState(from.getSubscriptionState().name());
		PlanType planType = from.getSubscriptionPlanType();
		to.setSubscriptionType(planType.name());
		to.setCreatedTimestamp(createdTimestampText);

		switch (planType) {
		case DURATION:
			long expiryTimeStamp = from.getExpiryTimestamp();
			if (expiryTimeStamp > 0) {
				String expiryTimestampText = StringUtils.getFormattedDate(expiryTimeStamp, dateFormat);
				to.setExpiryTimestamp(expiryTimestampText);
			}
			long activationTimestamp = from.getActivationTimestamp();
			if (activationTimestamp > 0) {
				String activationTimestampText = StringUtils.getFormattedDate(activationTimestamp, dateFormat);
				to.setActivationTimestamp(activationTimestampText);
			}
			break;
		case QUOTA:
			BiddingSubscriptionPlan plan = from.getSubscriptionPlan();
			to.setQuota(plan.getQuota(), from.getUsedQuota(), from.getRemainingQuota());
			break;
		default:
			break;
		}

		EmailNotificationStatus emailNotificationStatus = from.getEmailNotificationStatus();
		if (emailNotificationStatus != null) {
			to.setEmailNotificationStatus(emailNotificationStatus.name());
		}
	}

	public static void map(BuyerBiddingSubscription from, BuyerBiddingSubscriptionInfoResponse to,
			SimpleDateFormat dateFormat) {
		mapSubscription(from, to, dateFormat);
		Buyer buyer = from.getBuyer();
		to.setBuyer(buyer.getId(), buyer.getName());
		URI link = Endpoint.BUYER_BIDDING_SUBSCRIPTION_INFO.get(buyer.getId(), from.getId());
		to.setLink(link);
		BuyerBiddingSubscriptionPlan plan = from.getSubscriptionPlan();
		to.setSubscriptionPlan(plan.getId(), plan.getName());
		to.setNumberOfTendersCreated(from.getNumberOfTendersCreated());
		to.setPaymentTransactionsLink(PaymentTransactionUtility
				.getPaymentTransactionListURI(Buyer.class.getSimpleName(), buyer.getId(), from.getId()));

	}

	public static BuyerBiddingSubscriptionInfoResponse map(BuyerBiddingSubscription from, SimpleDateFormat dateFormat) {
		BuyerBiddingSubscriptionInfoResponse to = new BuyerBiddingSubscriptionInfoResponse();
		map(from, to, dateFormat);
		return to;
	}

	public static void map(VendorBiddingSubscription from, VendorBiddingSubscriptionInfoResponse to,
			SimpleDateFormat dateFormat) {
		mapSubscription(from, to, dateFormat);
		Vendor vendor = from.getVendor();
		to.setVendor(vendor.getId(), vendor.getName());
		URI link = Endpoint.VENDOR_BIDDING_SUBSCRIPTION_INFO.get(vendor.getId(), from.getId());
		to.setLink(link);
		VendorBiddingSubscriptionPlan plan = from.getSubscriptionPlan();
		to.setSubscriptionPlan(plan.getId(), plan.getName());
		to.setNumberOfTendersAccessed(from.getNumberOfTendersAccessed());
		to.setPaymentTransactionsLink(PaymentTransactionUtility
				.getPaymentTransactionListURI(Vendor.class.getSimpleName(), vendor.getId(), from.getId()));

	}

	public static VendorBiddingSubscriptionInfoResponse map(VendorBiddingSubscription from,
			SimpleDateFormat dateFormat) {
		VendorBiddingSubscriptionInfoResponse to = new VendorBiddingSubscriptionInfoResponse();
		map(from, to, dateFormat);
		return to;
	}
}
