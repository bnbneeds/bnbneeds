package com.bnbneeds.app.api.service.pipeline.vendor;

import static com.bnbneeds.app.api.service.pipeline.vendor.DeleteVendorJobROOT.queryVendor;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryList;
import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.transact;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.BuyerReview;
import com.bnbneeds.app.db.client.model.Vendor;
import com.google.appengine.tools.pipeline.Job2;
import com.google.appengine.tools.pipeline.Value;

public class DeleteReviewsOfBuyersJob extends Job2<Void, String, Boolean> {

	private static final long serialVersionUID = 4433108282037367856L;
	private static final Logger logger = LoggerFactory.getLogger(DeleteReviewsOfBuyersJob.class);

	@Override
	public Value<Void> run(String vendorId, Boolean forceDelete) throws Exception {
		Vendor vendor = queryVendor(vendorId);
		if (vendor == null) {
			return null;
		}

		List<BuyerReview> buyerReviews = queryList(BuyerReview.class, new QueryFields("reviewedBy", vendor));
		if (!CollectionUtils.isEmpty(buyerReviews)) {
			if (!forceDelete) {
				throw DeleteDealerJob.cannotDeleteHasReferences(vendorId, Vendor.class, BuyerReview.class);
			}
			DBOperationList buyerReviewDeleteOperationList = new DBOperationList();

			logger.info("Deelte the buyer reviews by the vendor [{}] and delete them...", vendorId);
			for (Iterator<BuyerReview> iterator = buyerReviews.iterator(); iterator.hasNext();) {
				BuyerReview buyerReview = iterator.next();
				buyerReviewDeleteOperationList.addOperation(OperationType.MARK_AS_DELETED, buyerReview);
			}
			transact(buyerReviewDeleteOperationList);
		}
		return null;
	}

}
