package com.bnbneeds.app.api.service.util;

import static com.bnbneeds.app.api.service.response.DBObjectMapper.toRelatedRestResponse;

import java.net.URI;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.bnbneeds.app.model.CreateResourceResponse;
import com.bnbneeds.app.model.OperationStatus;
import com.bnbneeds.app.model.TaskResponse;
import com.bnbneeds.app.model.endpoints.Endpoint;

public class TaskResponseUtility {

	public static ResponseEntity<CreateResourceResponse> createResourceResponse(
			String resourceId, Endpoint targetEndPoint, String message) {

		CreateResourceResponse task = new CreateResourceResponse(
				HttpStatus.CREATED.value(), OperationStatus.SUCCESS, message);
		URI href = targetEndPoint.get(resourceId);
		task.setResourceRep(toRelatedRestResponse(resourceId, href));

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(href);

		return new ResponseEntity<CreateResourceResponse>(task, headers,
				HttpStatus.CREATED);

	}

	public static ResponseEntity<CreateResourceResponse> createResourceResponse(
			String resourceId, URI href, String message) {

		CreateResourceResponse task = new CreateResourceResponse(
				HttpStatus.CREATED.value(), OperationStatus.SUCCESS, message);

		task.setResourceRep(toRelatedRestResponse(resourceId, href));

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(href);

		return new ResponseEntity<CreateResourceResponse>(task, headers,
				HttpStatus.CREATED);

	}

	public static ResponseEntity<TaskResponse> createTaskResponse(
			TaskResponse task) {
		return new ResponseEntity<TaskResponse>(task, HttpStatus.valueOf(task
				.getHttpCode()));
	}

	public static ResponseEntity<TaskResponse> createTaskSuccessResponse(
			String message) {
		TaskResponse task = new TaskResponse(HttpStatus.OK.value(),
				OperationStatus.SUCCESS, message, null);
		return createTaskResponse(task);
	}

	public static ResponseEntity<TaskResponse> createTaskSubmittedResponse(
			String message) {
		TaskResponse task = new TaskResponse(HttpStatus.ACCEPTED.value(),
				OperationStatus.SUCCESS, message, null);
		return createTaskResponse(task);
	}

}
