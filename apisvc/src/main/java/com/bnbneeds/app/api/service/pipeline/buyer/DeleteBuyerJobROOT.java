package com.bnbneeds.app.api.service.pipeline.buyer;

import static com.bnbneeds.app.db.client.impl.ObjectifyHandle.queryObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.api.service.pipeline.dealer.DeleteDealerJob;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.Buyer;
import com.google.appengine.tools.pipeline.FutureValue;
import com.google.appengine.tools.pipeline.ImmediateValue;
import com.google.appengine.tools.pipeline.Value;

/**
 * THis class deletes the buyer entity along with all its references. It employs
 * GAE pipelines to execute tasks sequentially.
 * 
 * See <a href=
 * "https://github.com/GoogleCloudPlatform/appengine-pipelines/wiki/Java"
 * >AppEngine Pipelines using Java</a>
 * 
 * @author Amit Herlekar
 *
 */
public class DeleteBuyerJobROOT extends DeleteDealerJob {

	private static final Logger logger = LoggerFactory.getLogger(DeleteBuyerJobROOT.class);

	private static final long serialVersionUID = 4939125748442384823L;

	private String buyerId;
	private Boolean deleteUserAccount;
	private Boolean forcedelete;

	public DeleteBuyerJobROOT(String buyerId, Boolean deleteUserAccount, Boolean forcedelete) {
		super();
		this.buyerId = buyerId;
		this.deleteUserAccount = deleteUserAccount;
		this.forcedelete = forcedelete;
	}

	static QueryFields buyerField(Buyer buyer) {
		return new QueryFields("buyer", buyer);
	}

	static Buyer queryBuyer(String buyerId) {
		return queryObject(Buyer.class, buyerId);
	}

	@Override
	public String getJobDisplayName() {
		return "Delete Buyer entity and all its child references. Buyer ID: " + buyerId;
	}

	@Override
	public Value<Void> run() throws Exception {

		logger.info("Request receieved to delete the buyer record and all its children: [{}]", buyerId);

		/*
		 * The order of these calls are very important
		 */

		Value<String> buyerIdInput = new ImmediateValue<>(buyerId);
		Value<Boolean> forceDeleteInput = new ImmediateValue<>(forcedelete);

		FutureValue<Void> job = futureCall(new DeletePurchaseItemsJob(), buyerIdInput, forceDeleteInput,
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteProductEnquiriesJob(), buyerIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteReviewsJob(), buyerIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteReviewsOfProductsJob(), buyerIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteReviewsOfVendorsJob(), buyerIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteBiddingSubscriptionUsageJob(), buyerIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteBiddingSubscriptionsJob(), buyerIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteTendersJob(), buyerIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeletePaymentTransactionsJob(), buyerIdInput, forceDeleteInput, waitFor(job),
				DELETE_DEALER_JOB_SETTING);
		job = futureCall(new DeleteBuyerEntityJob(), buyerIdInput, new ImmediateValue<>(deleteUserAccount),
				waitFor(job), DELETE_DEALER_JOB_SETTING);
		futureCall(new DeleteBuyerFromDocumentIndexJob(buyerId), waitFor(job), DELETE_DEALER_JOB_SETTING);
		return null;
	}

}
