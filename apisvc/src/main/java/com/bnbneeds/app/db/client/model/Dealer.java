package com.bnbneeds.app.db.client.model;

public class Dealer extends DescribedDataObject {

	private String address;
	private String contactName;
	private String mobileNumber;
	private String emailAddress;
	private String website;
	private String pan;
	
	

	public Dealer() {
		super();
	}

	protected Dealer(Dealer dealer) {
		setId(dealer.getId());
		setName(dealer.getName());
		setDescription(dealer.getDescription());
		setEntityStatus(dealer.getEntityStatus());
		setAddress(dealer.getAddress());
		setContactName(dealer.getContactName());
		setEmailAddress(dealer.getEmailAddress());
		setMobileNumber(dealer.getMobileNumber());
		setWebsite(dealer.getWebsite());
		setPan(dealer.getPan());
	}

	public String getAddress() {
		return address;
	}

	public String getContactName() {
		return contactName;
	}

	public String getPan() {
		return pan;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}


	public void setPan(String pan) {
		this.pan = pan;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Dealer [");
		builder.append(super.toString());
		if (address != null)
			builder.append("address=").append(address).append(", ");
		if (contactName != null)
			builder.append("contactName=").append(contactName).append(", ");
		if (mobileNumber != null)
			builder.append("mobileNumber=").append(mobileNumber).append(", ");
		if (emailAddress != null)
			builder.append("emailAddress=").append(emailAddress).append(", ");
		if (website != null)
			builder.append("website=").append(website).append(", ");
		if (pan != null)
			builder.append("pan=").append(pan);
		builder.append("]");
		return builder.toString();
	}

}
