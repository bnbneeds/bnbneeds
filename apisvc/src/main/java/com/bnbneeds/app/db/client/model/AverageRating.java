package com.bnbneeds.app.db.client.model;

public class AverageRating extends DataObject {

	private double averageRating;
	private double fiveStarRatingPercentage;
	private double fourStarRatingPercentage;
	private double threeStarRatingPercentage;
	private double twoStarRatingPercentage;
	private double oneStarRatingPercentage;

	public double getAverageRating() {
		return averageRating;
	}

	public double getFiveStarRatingPercentage() {
		return fiveStarRatingPercentage;
	}

	public double getFourStarRatingPercentage() {
		return fourStarRatingPercentage;
	}

	public double getThreeStarRatingPercentage() {
		return threeStarRatingPercentage;
	}

	public double getTwoStarRatingPercentage() {
		return twoStarRatingPercentage;
	}

	public double getOneStarRatingPercentage() {
		return oneStarRatingPercentage;
	}

	public void setAverageRating(double averageRating) {
		this.averageRating = averageRating;
	}

	public void setFiveStarRatingPercentage(double fiveStarRatingPercentage) {
		this.fiveStarRatingPercentage = fiveStarRatingPercentage;
	}

	public void setFourStarRatingPercentage(double fourStarRatingPercentage) {
		this.fourStarRatingPercentage = fourStarRatingPercentage;
	}

	public void setThreeStarRatingPercentage(double threeStarRatingPercentage) {
		this.threeStarRatingPercentage = threeStarRatingPercentage;
	}

	public void setTwoStarRatingPercentage(double twoStarRatingPercentage) {
		this.twoStarRatingPercentage = twoStarRatingPercentage;
	}

	public void setOneStarRatingPercentage(double oneStarRatingPercentage) {
		this.oneStarRatingPercentage = oneStarRatingPercentage;
	}

	public void reset() {
		setAverageRating(0.0);
		setFiveStarRatingPercentage(0.0);
		setFourStarRatingPercentage(0.0);
		setThreeStarRatingPercentage(0.0);
		setTwoStarRatingPercentage(0.0);
		setOneStarRatingPercentage(0.0);
	}

}
