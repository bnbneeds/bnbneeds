package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class BuyerAverageRating extends AverageRating {

	@Load
	@Index
	private Ref<Buyer> buyer;

	public Buyer getBuyer() {
		return buyer.get();
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = Ref.create(buyer);
	}

}
