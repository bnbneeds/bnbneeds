package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class BuyerBiddingSubscriptionUsage extends DealerBiddingSubscriptionUsage {

	@Index
	@Load
	private Ref<Buyer> buyer;

	@Index
	@Load
	private Ref<BuyerBiddingSubscription> subscription;

	public Buyer getBuyer() {
		return buyer.get();
	}

	public BuyerBiddingSubscription getSubscription() {
		return subscription.get();
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = Ref.create(buyer);
	}

	public void setSubscription(BuyerBiddingSubscription subscription) {
		this.subscription = Ref.create(subscription);
	}

	@Override
	public DealerBiddingSubscription getBiddingSubscription() {
		return getSubscription();
	}

	@Override
	public Dealer getDealer() {
		return getBuyer();
	}

}
