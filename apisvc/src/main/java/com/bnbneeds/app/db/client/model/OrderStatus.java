package com.bnbneeds.app.db.client.model;

public class OrderStatus {

	private String timestamp;
	private String state;
	private String comments;
	private String updatedBy;

	public String getTimestamp() {
		return timestamp;
	}

	public String getState() {
		return state;
	}

	public String getComments() {
		return comments;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
}
