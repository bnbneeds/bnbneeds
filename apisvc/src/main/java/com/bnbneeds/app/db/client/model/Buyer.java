package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class Buyer extends Dealer {

	public Buyer() {
		super();
	}

	public Buyer(Dealer dealer) {
		super(dealer);
	}

	@Load
	@Index
	private Ref<BusinessType> businessType;

	public BusinessType getBusinessType() {
		return businessType.get();
	}

	public void setBusinessType(BusinessType businessType) {
		this.businessType = Ref.create(businessType);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Buyer [");
		builder.append(super.toString());
		if (businessType != null)
			builder.append("businessType=").append(businessType);
		builder.append("]");
		return builder.toString();
	}

}
