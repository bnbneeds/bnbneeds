package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;

@Entity
@Cache
public class Vendor extends Dealer {

	private String vatNumber;
	private String serviceTaxNumber;
	private String cities;

	public Vendor() {
		super();
	}

	public Vendor(Dealer dealer) {
		super(dealer);
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public String getServiceTaxNumber() {
		return serviceTaxNumber;
	}

	public String getCities() {
		return cities;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public void setServiceTaxNumber(String serviceTaxNumber) {
		this.serviceTaxNumber = serviceTaxNumber;
	}

	public void setCities(String cities) {
		this.cities = cities;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Vendor [");
		builder.append(super.toString());
		if (vatNumber != null)
			builder.append("vatNumber=").append(vatNumber).append(", ");
		if (serviceTaxNumber != null)
			builder.append("serviceTaxNumber=").append(serviceTaxNumber)
					.append(", ");
		if (cities != null)
			builder.append("cities=").append(cities);
		builder.append("]");
		return builder.toString();
	}

}
