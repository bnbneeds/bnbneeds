package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfNotEmpty;
import com.googlecode.objectify.condition.IfNotNull;

public class DataObject {

	@Id
	private String id;
	@Index({ IfNotNull.class, IfNotEmpty.class })
	private String name;

	@Index
	private boolean inactive;
	private String createTimestamp;
	private String updateTimestamp;

	public DataObject() {
		super();
		inactive = false;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public String getCreateTimestamp() {
		return createTimestamp;
	}

	public String getUpdateTimestamp() {
		return updateTimestamp;
	}

	public void setCreateTimestamp(String createTimestamp) {
		this.createTimestamp = createTimestamp;
	}

	public void setUpdateTimestamp(String updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataObject [");
		if (id != null)
			builder.append("id=").append(id).append(", ");
		if (name != null)
			builder.append("name=").append(name).append(", ");
		builder.append("inactive=").append(inactive).append(", ");
		if (createTimestamp != null)
			builder.append("createTimestamp=").append(createTimestamp)
					.append(", ");
		if (updateTimestamp != null)
			builder.append("updateTimestamp=").append(updateTimestamp);
		builder.append("]");
		return builder.toString();
	}

}
