package com.bnbneeds.app.db.client.impl;

import com.bnbneeds.app.db.client.model.DataObject;

public class DBOperation {

	public enum OperationType {
		GET, INSERT, UPDATE, DELETE, MARK_AS_DELETED;
	}

	private OperationType operationType;
	private DataObject dataObject;

	public DBOperation(OperationType operationType, DataObject dataObject) {
		this.operationType = operationType;
		this.dataObject = dataObject;
	}

	public OperationType getOperationType() {
		return operationType;
	}

	public DataObject getDataObject() {
		return dataObject;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	public void setDataObject(DataObject dataObject) {
		this.dataObject = dataObject;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Operation [");
		if (operationType != null) {
			builder.append("operationType=");
			builder.append(operationType);
			builder.append(", ");
		}
		if (dataObject != null) {
			builder.append("dataObject=");
			builder.append(dataObject);
		}
		builder.append("]");
		return builder.toString();
	}

}
