package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class BuyerReview extends Review {

	@Load
	@Index
	private Ref<Buyer> buyer;

	@Index
	@Load
	private Ref<Vendor> reviewedBy;

	public Buyer getBuyer() {
		return buyer.get();
	}

	public Vendor getReviewedBy() {
		return reviewedBy.get();
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = Ref.create(buyer);
	}

	public void setReviewedBy(Vendor reviewedBy) {
		this.reviewedBy = Ref.create(reviewedBy);
	}

}
