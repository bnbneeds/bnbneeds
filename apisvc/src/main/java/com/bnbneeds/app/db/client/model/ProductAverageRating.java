package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class ProductAverageRating extends AverageRating {

	@Load
	@Index
	private Ref<Product> product;

	public Product getProduct() {
		return product.get();
	}

	public void setProduct(Product product) {
		this.product = Ref.create(product);
	}

}
