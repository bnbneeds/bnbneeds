package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Entity
@Cache
public class PaymentTransaction extends DataObject {

	private long timestamp;
	@Index
	private String transactionId;
	@Index
	private String transactionStatus;
	private String transactionDescription;
	private String paymentGateway;
	private String paymentMethod;
	private double amount;

	public static enum TransactionStatus {
		SUCCESS, FAILURE
	}

	public boolean isTransactionSuccess() {
		return TransactionStatus.SUCCESS.name().equalsIgnoreCase(this.transactionStatus);
	}

	public long getTimestamp() {
		return timestamp;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public String getTransactionDescription() {
		return transactionDescription;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public double getAmount() {
		return amount;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PaymentTransaction [");
		builder.append(super.toString());
		builder.append(" timestamp=").append(timestamp).append(", ");
		if (transactionId != null)
			builder.append("transactionId=").append(transactionId).append(", ");
		if (transactionStatus != null)
			builder.append("transactionStatus=").append(transactionStatus).append(", ");
		if (transactionDescription != null)
			builder.append("transactionDescription=").append(transactionDescription).append(", ");
		if (paymentGateway != null)
			builder.append("paymentGateway=").append(paymentGateway).append(", ");
		if (paymentMethod != null)
			builder.append("paymentMethod=").append(paymentMethod).append(", ");
		builder.append("amount=").append(amount).append("]");
		return builder.toString();
	}

}
