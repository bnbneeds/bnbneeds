package com.bnbneeds.app.db.client.model;

import java.util.ArrayList;
import java.util.List;

import com.bnbneeds.app.util.StringUtils;
import com.googlecode.objectify.annotation.Index;

public class ApprovedDataObject extends DataObject {

	public static enum EntityStatus {
		APPROVED, DISAPPROVED, BLACKLISTED;
	}

	@Index
	private String entityStatus;
	private List<String> remarks;

	public ApprovedDataObject() {
		super();
		entityStatus = EntityStatus.APPROVED.name();
	}

	public EntityStatus getEntityStatus() {
		return EntityStatus.valueOf(entityStatus);
	}

	public void setEntityStatus(EntityStatus entityStatus) {
		this.entityStatus = entityStatus.name();
	}

	public boolean hasStatus(EntityStatus entityStatus) {
		return entityStatus == getEntityStatus();
	}

	public boolean isBlacklisted() {
		return hasStatus(EntityStatus.BLACKLISTED);
	}

	public boolean isApproved() {
		return hasStatus(EntityStatus.APPROVED);
	}

	public boolean isDispproved() {
		return hasStatus(EntityStatus.DISAPPROVED);
	}

	public List<String> getRemarks() {
		return remarks;
	}

	public void addRemark(String remark) {
		if (!StringUtils.isEmpty(remark)) {
			if (remarks == null) {
				remarks = new ArrayList<String>();
			}
			remarks.add(remark);
		}
	}

	public void addRemark(String username, String remark) {

		if (remarks == null) {
			remarks = new ArrayList<String>();
		}

		if (!StringUtils.isEmpty(remark)) {
			remarks.add("User: " + username + " - " + remark);
		} else {
			remarks.add("Entity updated by User: " + username);
		}

	}

	public void addRemark(String username, String role, String remark) {

		if (remarks == null) {
			remarks = new ArrayList<String>();
		}

		if (!StringUtils.isEmpty(remark)) {
			remarks.add("User: " + username + " (Role: " + role + ") - "
					+ remark);
		} else {
			remarks.add("Entity status updated to: " + entityStatus
					+ " by User: " + username + " (Role: " + role + ")");
		}

	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApprovedDataObject [");
		builder.append(super.toString());
		if (entityStatus != null)
			builder.append("entityStatus=").append(entityStatus).append(", ");
		if (remarks != null)
			builder.append("remarks=").append(remarks);
		builder.append("]");
		return builder.toString();
	}

}
