package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class Enquiry extends DescribedDataObject {

	@Index
	@Load
	private Ref<Product> product;

	@Index
	@Load
	private Ref<Buyer> buyer;

	@Index
	private long enquiryTimestamp;
	private String requiredQuantity;
	private LikelyToBuyIn likelyToBuyIn;

	@Index
	private String nativeId;

	public Product getProduct() {
		return product.get();
	}

	public String getRequiredQuantity() {
		return requiredQuantity;
	}

	public Buyer getBuyer() {
		return buyer.get();
	}

	public LikelyToBuyIn getLikelyToBuyIn() {
		return likelyToBuyIn;
	}

	public long getEnquiryTimestamp() {
		return enquiryTimestamp;
	}

	public String getNativeId() {
		return nativeId;
	}

	public void setNativeId(String nativeId) {
		this.nativeId = nativeId;
	}

	public void setEnquiryTimestamp(long enquiryTimestamp) {
		this.enquiryTimestamp = enquiryTimestamp;
	}

	public void setLikelyToBuyIn(LikelyToBuyIn likelyToBuyIn) {
		this.likelyToBuyIn = likelyToBuyIn;
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = Ref.create(buyer);
	}

	public void setProduct(Product product) {
		this.product = Ref.create(product);
	}

	public void setRequiredQuantity(String requiredQuantity) {
		this.requiredQuantity = requiredQuantity;
	}

	public static String generateNativeId(String requestDate, String buyerId,
			String productId) {
		return String.format("%1$s+%2$s+%3$s", requestDate, buyerId, productId);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Enquiry [");
		if (product != null)
			builder.append("product=").append(product).append(", ");
		if (buyer != null)
			builder.append("buyer=").append(buyer).append(", ");
		builder.append("enquiryTimestamp=").append(enquiryTimestamp)
				.append(", ");
		if (requiredQuantity != null)
			builder.append("requiredQuantity=").append(requiredQuantity)
					.append(", ");
		if (likelyToBuyIn != null)
			builder.append("likelyToBuyIn=").append(likelyToBuyIn);
		builder.append("]");
		return builder.toString();
	}

	public static class LikelyToBuyIn {

		public static enum TimeUnit {
			days, weeks, months
		}

		private Integer value;
		private String timeUnit;

		public LikelyToBuyIn() {
			super();
		}

		public LikelyToBuyIn(Integer value, String timeUnit) {
			super();
			this.value = value;
			this.timeUnit = timeUnit;
		}

		public Integer getValue() {
			return value;
		}

		public String getTimeUnit() {
			return timeUnit;
		}

		public void setValue(Integer value) {
			this.value = value;
		}

		public void setTimeUnit(String timeUnit) {
			this.timeUnit = timeUnit;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("LikelyToBuyIn [");
			if (value != null)
				builder.append("value=").append(value).append(", ");
			if (timeUnit != null)
				builder.append("timeUnit=").append(timeUnit);
			builder.append("]");
			return builder.toString();
		}

	}
}
