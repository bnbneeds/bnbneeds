package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class VendorReview extends Review {

	@Index
	@Load
	private Ref<Vendor> vendor;

	@Index
	@Load
	private Ref<Buyer> reviewedBy;

	public Vendor getVendor() {
		return vendor.get();
	}

	public Buyer getReviewedBy() {
		return reviewedBy.get();
	}

	public void setVendor(Vendor vendor) {
		this.vendor = Ref.create(vendor);
	}

	public void setReviewedBy(Buyer reviewedBy) {
		this.reviewedBy = Ref.create(reviewedBy);
	}

}
