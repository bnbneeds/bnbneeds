package com.bnbneeds.app.db.client.impl;

import java.util.ArrayList;
import java.util.List;

import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.model.DataObject;

public class DBOperationList {
	private List<DBOperation> dbOpList;

	public DBOperationList() {
		dbOpList = new ArrayList<DBOperation>();
	}

	public DBOperationList addOperation(OperationType operationType,
			DataObject dataObject) {
		dbOpList.add(new DBOperation(operationType, dataObject));
		return this;
	}

	public List<DBOperation> get() {
		return dbOpList;
	}

	public boolean isEmpty() {
		return (get() == null || get().isEmpty());
	}

}
