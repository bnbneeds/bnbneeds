package com.bnbneeds.app.db.client.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.db.client.constraint.ParentKeyConstraint;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.DataObject;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.cmd.Query;

/**
 * 
 * @author Amit Herlekar
 *
 */
class QueryBuilder {

	private static final Logger logger = LoggerFactory
			.getLogger(QueryBuilder.class);

	public static <T extends DataObject> Query<T> create(Objectify ofy,
			Class<T> returnType, QueryFields queryFields) {

		logger.debug("Building query for return type: {} using fields: {}",
				returnType.getSimpleName(), queryFields);

		if (queryFields == null || queryFields.isEmpty()) {
			logger.error("Query field(s) is null or empty");
			throw new DBClientException("Query field(s) is null or empty");
		}

		Query<T> q = ofy.load().type(returnType);

		for (String field : queryFields.getFields()) {
			q = q.filter(field, queryFields.get(field));
		}

		return q;
	}

	public static <T extends DataObject> Query<T> create(Objectify ofy,
			Class<T> returnType, ParentKeyConstraint parentConstraint) {

		logger.debug(
				"Building query for return type: {} having parent constraint: {}",
				returnType.getSimpleName(), parentConstraint);

		if (parentConstraint == null) {
			logger.error("Parent constraint is null");
			throw new DBClientException("Parent constraint is null");
		}

		Query<T> q = ofy.load().type(returnType)
				.ancestor(parentConstraint.getKey());

		return q;
	}

	public static <T extends DataObject> Query<T> create(Query<T> q,
			QueryFields queryFields) {

		logger.debug("Building query for query class: {} using fields: {}", q
				.getClass().getSimpleName(), queryFields);

		if (queryFields == null || queryFields.isEmpty()) {
			logger.error("Query field(s) is null or empty");
			throw new DBClientException("Query field(s) is null or empty");
		}

		for (String field : queryFields.getFields()) {
			q = q.filter(field, queryFields.get(field));
		}

		return q;
	}

}
