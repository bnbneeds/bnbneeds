package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class Tender extends DescribedDataObject {

	public static enum Status {
		CREATED, OPEN, CANCELLED, CLOSED, EDIT_IN_PROGRESS
	}

	public static enum DeliveryFrequency {
		ONETIME, DAILY, WEEKLY, FORTNIGHTLY, MONTHLY, QUARTERLY
	}

	public static enum PaymentMode {
		CASH, CHEQUE, ONLINE, CREDIT
	}

	public static enum CreditTerms {
		IMMEDIATE_PAYMENT, DAYS_15, DAYS_30, DAYS_45, DAYS_60, DAYS_90, DAYS_120, MORE_THAN_120_DAYS
	}

	public static enum BidValueType {
		UNIT_PRICE, TOTAL_PRICE
	}

	@Index
	@Load
	private Ref<Buyer> buyer;
	private long closeTimestamp;
	private long openTimestamp;
	@Index
	private String status;
	private double expectedPrice;
	private String deliveryFrequency;
	private String paymentMode;
	private String creditTerms;
	private String deliveryTerms;
	private String bidValueType;
	private String deliveryLocation;

	public String getBidValueType() {
		return bidValueType;
	}

	public void setBidValueType(String bidValueType) {
		this.bidValueType = bidValueType;
	}

	public void setBidValueType(BidValueType bidValueType) {
		this.bidValueType = bidValueType.name();
	}

	public Buyer getBuyer() {
		return buyer.get();
	}

	public void setBuyer(Buyer bidOwner) {
		this.buyer = Ref.create(bidOwner);
	}

	public long getCloseTimestamp() {
		return closeTimestamp;
	}

	public void setCloseTimestamp(long closeTimestamp) {
		this.closeTimestamp = closeTimestamp;
	}

	public String getStatus() {
		return status;
	}

	public Status getStatusEnum() {
		return Status.valueOf(status);
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setStatus(Status status) {
		setStatus(status.name());
	}

	public double getExpectedPrice() {
		return expectedPrice;
	}

	public void setExpectedPrice(double expectedPrice) {
		this.expectedPrice = expectedPrice;
	}

	public String getDeliveryFrequency() {
		return deliveryFrequency;
	}

	public void setDeliveryFrequency(String deliveryFrequency) {
		this.deliveryFrequency = deliveryFrequency;
	}

	public void setDeliveryFrequency(DeliveryFrequency deliveryFrequency) {
		setDeliveryFrequency(deliveryFrequency.name());
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public void setPaymentMode(PaymentMode paymentMode) {
		setPaymentMode(paymentMode.name());
	}

	public String getCreditTerms() {
		return creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public void setCreditTerms(CreditTerms creditTerms) {
		setCreditTerms(creditTerms.name());
	}

	public String getDeliveryTerms() {
		return deliveryTerms;
	}

	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}

	public boolean hasTenderStatus(Status status) {
		return status == Status.valueOf(getStatus());
	}

	public boolean hasTenderStatus(String status) {
		return getStatus().equals(status);
	}

	public String getDeliveryLocation() {
		return deliveryLocation;
	}

	public void setDeliveryLocation(String deliveryLocation) {
		this.deliveryLocation = deliveryLocation;
	}

	public long getOpenTimestamp() {
		return openTimestamp;
	}

	public void setOpenTimestamp(long openTimestamp) {
		this.openTimestamp = openTimestamp;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Tender [");
		builder.append("toString()=").append(super.toString());
		if (buyer != null)
			builder.append("buyer=").append(buyer).append(", ");
		builder.append("closeTimestamp=").append(closeTimestamp).append(", ");
		if (status != null)
			builder.append("status=").append(status).append(", ");
		builder.append("expectedPrice=").append(expectedPrice).append(", ");
		if (deliveryFrequency != null)
			builder.append("deliveryFrequency=").append(deliveryFrequency).append(", ");
		if (paymentMode != null)
			builder.append("paymentMode=").append(paymentMode).append(", ");
		if (creditTerms != null)
			builder.append("creditTerms=").append(creditTerms).append(", ");
		if (deliveryTerms != null)
			builder.append("deliveryTerms=").append(deliveryTerms).append(", ");
		if (bidValueType != null)
			builder.append("bidValueType=").append(bidValueType).append(", ");
		if (deliveryLocation != null)
			builder.append("deliveryLocation=").append(deliveryLocation).append(", ");

		builder.append("]");
		return builder.toString();
	}

}
