package com.bnbneeds.app.db.client.model;

import com.bnbneeds.app.db.client.model.BiddingSubscriptionPlan.PlanType;
import com.googlecode.objectify.annotation.Index;

public abstract class DealerBiddingSubscription extends DataObject {

	public static enum SubscriptionState {
		ACTIVE, ACTIVATION_PENDING, ACTIVATION_IN_PROGRESS, EXPIRED
	}

	public static enum EmailNotificationStatus {
		EXPIRY_REMINDER_NOT_SENT, REMINDER_SENT_BEFORE_EXPIRY_FOR_RENEWAL
	}

	private long createdTimestamp;
	private long expiryTimestamp;
	private long activationTimestamp;
	@Index
	private String subscriptionState;
	@Index
	private String emailNotificationStatus;

	public abstract BiddingSubscriptionPlan getSubscriptionPlan();

	public abstract void setQuotaUsage(int quota);

	public abstract int getUsedQuota();

	public void incrementQuotaUsage(int incrementValue) {
		int usage = getUsedQuota();
		setQuotaUsage(usage + incrementValue);
	}

	public void resetQuotaUsage() {
		setQuotaUsage(0);
	}

	public void invalidateQuotaUsage() {
		setQuotaUsage(-1);
	}

	public boolean isQuotaUsageFullOrExceeded() {
		if (getUsedQuota() >= 0) {
			BiddingSubscriptionPlan plan = getSubscriptionPlan();
			return getUsedQuota() >= plan.getQuota();
		}
		return false;
	}

	public double getUsedQuotaPercentage() {

		BiddingSubscriptionPlan plan = getSubscriptionPlan();
		if (getUsedQuota() < 0 || plan == null) {
			return Double.NaN;
		}

		double percent = (getUsedQuota() / plan.getQuota()) * 100;
		return percent;
	}

	public int getRemainingQuota() {
		BiddingSubscriptionPlan plan = getSubscriptionPlan();
		if (getUsedQuota() < 0 || plan == null) {
			return -1;
		}
		return (plan.getQuota() - getUsedQuota());
	}

	public PlanType getSubscriptionPlanType() {
		return getSubscriptionPlan() == null ? null : getSubscriptionPlan().getPlanType();
	}

	public boolean hasPlanOfType(PlanType planType) {
		return getSubscriptionPlanType() == null ? false : (getSubscriptionPlanType() == planType);
	}

	public EmailNotificationStatus getEmailNotificationStatus() {
		return emailNotificationStatus == null ? null : EmailNotificationStatus.valueOf(emailNotificationStatus);
	}

	public void setEmailNotificationStatus(EmailNotificationStatus emailNotificationStatus) {
		this.emailNotificationStatus = emailNotificationStatus.name();
	}

	public boolean hasSubscriptionState(SubscriptionState state) {
		return state == getSubscriptionState();
	}

	public long getCreatedTimestamp() {
		return createdTimestamp;
	}

	public SubscriptionState getSubscriptionState() {
		return SubscriptionState.valueOf(subscriptionState);
	}

	public void setSubscriptionState(SubscriptionState subscriptionState) {
		this.subscriptionState = subscriptionState.name();
	}

	public void setCreatedTimestamp(long createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public long getExpiryTimestamp() {
		return expiryTimestamp;
	}

	public void setExpiryTimestamp(long expiryTimestamp) {
		this.expiryTimestamp = expiryTimestamp;
	}

	public long getActivationTimestamp() {
		return activationTimestamp;
	}

	public void setActivationTimestamp(long activationTimestamp) {
		this.activationTimestamp = activationTimestamp;
	}

	public void invalidateActivationTimestamp() {
		setActivationTimestamp(-1);
	}

	public void invalidateExpiryTimestamp() {
		setExpiryTimestamp(-1);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DealerBiddingSubscription [createdTimestamp=").append(createdTimestamp)
				.append(", expiryTimestamp=").append(expiryTimestamp).append(", activationTimestamp=")
				.append(activationTimestamp).append(", ");
		if (subscriptionState != null)
			builder.append("subscriptionState=").append(subscriptionState).append(", ");
		if (emailNotificationStatus != null)
			builder.append("emailNotificationStatus=").append(emailNotificationStatus);
		builder.append("]");
		return builder.toString();
	}

}
