package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class VendorBiddingSubscriptionUsage extends DealerBiddingSubscriptionUsage {

	@Index
	@Load
	private Ref<Vendor> vendor;

	@Index
	@Load
	private Ref<VendorBiddingSubscription> subscription;

	public Vendor getVendor() {
		return vendor.get();
	}

	public VendorBiddingSubscription getSubscription() {
		return subscription.get();
	}

	public void setVendor(Vendor vendor) {
		this.vendor = Ref.create(vendor);
	}

	public void setSubscription(VendorBiddingSubscription subscription) {
		this.subscription = Ref.create(subscription);
	}

	@Override
	public DealerBiddingSubscription getBiddingSubscription() {
		return getSubscription();
	}

	@Override
	public Dealer getDealer() {
		return getVendor();
	}

}
