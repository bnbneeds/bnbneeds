package com.bnbneeds.app.db.client.model;


public class DescribedDataObject extends ApprovedDataObject {
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RelatedDataObject [");
		builder.append(super.toString());
		if (description != null)
			builder.append("description=").append(description);
		builder.append("]");
		return builder.toString();
	}

}
