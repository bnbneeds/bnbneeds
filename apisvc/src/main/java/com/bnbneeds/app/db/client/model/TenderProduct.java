package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class TenderProduct extends DataObject {

	@Index
	@Load
	private Ref<Tender> tender;
	@Index
	@Load
	private Ref<ProductName> product;
	private double quantity;
	private String unit;
	private String qualitySpecification;
	private String specialRequirements;

	public Tender getTender() {
		return tender.get();
	}

	public void setTender(Tender tender) {
		this.tender = Ref.create(tender);
	}

	public ProductName getProduct() {
		return product.get();
	}

	public void setProduct(ProductName product) {
		this.product = Ref.create(product);
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setUnit(Units.Quantity unit) {
		setUnit(unit.name());
	}

	public String getQualitySpecification() {
		return qualitySpecification;
	}

	public void setQualitySpecification(String qualitySpecification) {
		this.qualitySpecification = qualitySpecification;
	}

	public String getSpecialRequirements() {
		return specialRequirements;
	}

	public void setSpecialRequirements(String specialRequirements) {
		this.specialRequirements = specialRequirements;
	}

}
