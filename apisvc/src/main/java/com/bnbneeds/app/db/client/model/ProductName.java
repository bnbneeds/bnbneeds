package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;

@Entity
@Cache
public class ProductName extends DescribedDataObject {

	private String imageKey;

	public String getImageKey() {
		return imageKey;
	}

	public void setImageKey(String imageKey) {
		this.imageKey = imageKey;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductName [");
		builder.append(super.toString());
		if (imageKey != null)
			builder.append("imageKey=").append(imageKey);
		builder.append("]");
		return builder.toString();
	}

}
