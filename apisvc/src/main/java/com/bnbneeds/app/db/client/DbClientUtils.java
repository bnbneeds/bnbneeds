package com.bnbneeds.app.db.client;

import com.bnbneeds.app.db.client.model.DataObject;

public class DbClientUtils {

	@SuppressWarnings("unchecked")
	public static Class<? extends DataObject> getClassByName(String className) throws ClassNotFoundException {

		Class<? extends DataObject> entityType = null;

		try {
			entityType = (Class<? extends DataObject>) Class.forName(DbClient.DB_MODEL_PACKAGE + "." + className);
		} catch (ClassNotFoundException e) {
			throw new ClassNotFoundException("Invalid entity type: [" + className + "]");
		}
		return entityType;
	}

	@SuppressWarnings("unchecked")
	public static <T extends DataObject> Class<T> getClassByName(String className, Class<T> classToClast)
			throws ClassNotFoundException {

		Class<T> entityType = null;

		try {
			entityType = (Class<T>) Class.forName(DbClient.DB_MODEL_PACKAGE + "." + className);
		} catch (ClassNotFoundException e) {
			throw new ClassNotFoundException("Invalid entity type: [" + className + "]");
		}
		return entityType;
	}

}
