package com.bnbneeds.app.db.client.constraint;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author Amit Herlekar
 *
 */
public class QueryFields {

	private Map<String, Object> fields;

	public QueryFields() {
		fields = new LinkedHashMap<>();
	}

	public QueryFields(String field, Object value) {
		fields = new LinkedHashMap<>();
		fields.put(field, value);
	}

	public QueryFields add(String field, Object value) {
		if (fields == null) {
			fields = new LinkedHashMap<>();
		}
		fields.put(field, value);
		return this;
	}

	private String getKey(QueryFields field) {
		return new String(field.fields.keySet().iterator().next());
	}

	private Object getObjectValue(QueryFields field) {
		return field.fields.get(getKey(field));
	}

	public QueryFields add(QueryFields field) {
		if (field.size() != 1) {
			throw new IllegalArgumentException("Number of fields must be one.");
		}
		return add(getKey(field), getObjectValue(field));
	}

	public Map<String, Object> get() {
		return fields;
	}

	public Set<String> getFields() {
		return fields.keySet();
	}

	public Object get(String field) {
		return fields.get(field);
	}

	public boolean isEmpty() {
		return (get().isEmpty());
	}

	public int size() {
		return (get().size());
	}

	public boolean containsField(String field) {
		return fields.containsKey(field);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("QueryFields [");
		if (fields != null)
			builder.append("fields=").append(fields);
		builder.append("]");
		return builder.toString();
	}

}
