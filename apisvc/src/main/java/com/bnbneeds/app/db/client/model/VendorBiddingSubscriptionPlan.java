package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;

@Entity
@Cache
public class VendorBiddingSubscriptionPlan extends BiddingSubscriptionPlan {

	private int numberOfTendersAllowedToAccess;

	public int getNumberOfTendersAllowedToAccess() {
		return numberOfTendersAllowedToAccess;
	}

	public void setNumberOfTendersAllowedToAccess(int numberOfTendersAllowedToAccess) {
		this.numberOfTendersAllowedToAccess = numberOfTendersAllowedToAccess;
	}

	@Override
	public int getQuota() {
		return getNumberOfTendersAllowedToAccess();
	}

	@Override
	public void setQuota(int quota) {
		setNumberOfTendersAllowedToAccess(quota);
	}

}
