package com.bnbneeds.app.db.client.model;

public abstract class BiddingSubscriptionPlan extends SubscriptionPlan {

	public static enum PlanType {
		DURATION, QUOTA
	}

	private String type;

	public String getType() {
		return type;
	}

	public abstract int getQuota();

	public abstract void setQuota(int quota);

	public PlanType getPlanType() {
		return PlanType.valueOf(type);
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isPlanType(PlanType planType) {
		return planType == getPlanType();
	}

}
