package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.annotation.Index;

public abstract class SubscriptionPlan extends DescribedDataObject {

	public static enum DurationType {
		months, years, days
	}

	public static enum AvailabilityStatus {
		AVAILABLE, NOT_AVAILABLE
	}

	private String title;
	private String durationType;
	private int durationValue;
	private double premiumAmount;
	private String availabilityStatus;
	@Index
	private boolean defaultPlan;

	public SubscriptionPlan() {
		super();
		setAvailabilityStatus(AvailabilityStatus.AVAILABLE);
	}

	public boolean isDefaultPlan() {
		return defaultPlan;
	}

	public void setDefaultPlan(boolean defaultPlan) {
		this.defaultPlan = defaultPlan;
	}

	public String getDurationType() {
		return durationType;
	}

	public DurationType getDurationTypeEnum() {
		return durationType == null ? null : DurationType.valueOf(durationType);
	}

	public int getDurationValue() {
		return durationValue;
	}

	public String getTitle() {
		return title;
	}

	public double getPremiumAmount() {
		return premiumAmount;
	}

	public String getAvailabilityStatus() {
		return availabilityStatus;
	}

	public AvailabilityStatus getAvailabilityStatusEnum() {
		return AvailabilityStatus.valueOf(availabilityStatus);
	}

	public boolean isAvailable() {
		return getAvailabilityStatusEnum() == AvailabilityStatus.AVAILABLE;
	}

	public void setAvailabilityStatus(String availabilityStatus) {
		this.availabilityStatus = availabilityStatus;
	}

	public void setAvailabilityStatus(AvailabilityStatus availabilityStatus) {
		setAvailabilityStatus(availabilityStatus.name());
	}

	public void setPremiumAmount(double premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDurationType(String durtationType) {
		this.durationType = durtationType;
	}

	public void setDurationValue(int durationValue) {
		this.durationValue = durationValue;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SubscriptionPlan [");
		if (title != null)
			builder.append("title=").append(title).append(", ");
		if (durationType != null)
			builder.append("durationType=").append(durationType).append(", ");
		builder.append("durationValue=").append(durationValue).append(", ");
		builder.append("premiumAmount=").append(premiumAmount);
		builder.append("]");
		return builder.toString();
	}

}
