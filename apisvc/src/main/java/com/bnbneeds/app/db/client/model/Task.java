package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class Task extends DataObject {

	public static enum Status {
		NEW, IN_PROGRESS, FAILED, SUCCESS
	}

	private String entityId;
	@Load
	private Ref<Task> waitFor;

	private String status;

	public String getEntityId() {
		return entityId;
	}

	public Task getWaitFor() {
		return (waitFor == null) ? null : waitFor.get();
	}

	public String getStatus() {
		return status;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public void setWaitFor(Task waitFor) {
		this.waitFor = Ref.create(waitFor);
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Status getStatusEnum() {
		return Status.valueOf(status);
	}

	public boolean hasStatus(Status status) {
		return getStatusEnum() == status;
	}

	public void setStatus(Status status) {
		setStatus(status.name());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Task [");
		builder.append(super.toString());
		if (entityId != null)
			builder.append("entityId=").append(entityId).append(", ");
		if (waitFor != null)
			builder.append("waitFor=").append(waitFor).append(", ");
		if (status != null)
			builder.append("status=").append(status);
		builder.append("]");
		return builder.toString();
	}

}
