package com.bnbneeds.app.db.client.constraint;

import com.googlecode.objectify.Key;

public class ParentKeyConstraint {

	private Key<?> parentKey;

	private ParentKeyConstraint() {

	}

	public Key<?> getKey() {
		return parentKey;
	}

	public static ParentKeyConstraint create(Class<?> clazz, String parentId) {
		ParentKeyConstraint constraint = new ParentKeyConstraint();
		constraint.parentKey = Key.create(clazz, parentId);
		return constraint;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ParentKeyConstraint [");
		if (parentKey != null)
			builder.append("parentKey=").append(parentKey.toWebSafeString());
		builder.append("]");
		return builder.toString();
	}

}
