package com.bnbneeds.app.db.client;

import com.bnbneeds.app.db.client.model.DataObject;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;

public class ResultSetIterator<T extends DataObject> {

	private QueryResultIterator<T> iterator;

	public ResultSetIterator(QueryResultIterator<T> iterator) {
		super();
		this.iterator = iterator;
	}

	/**
	 * Gets a cursor offset that points to the Entity immediately after the last
	 * Entity that was retrieved by iterator.next(). Therefore, call this method
	 * after iterating through all the entities in the iterator to get the
	 * offset for the next batch of entities.
	 * 
	 * @return next offset
	 */
	public String getOffset() {
		String offset = null;
		Cursor c = iterator.getCursor();
		if (c != null) {
			offset = c.toWebSafeString();
		}
		return offset;
	}

	public boolean hasNext() {
		return iterator.hasNext();
	}

	public T next() {
		return iterator.next();
	}
}
