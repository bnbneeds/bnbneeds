package com.bnbneeds.app.db.client;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.util.StringUtils;

public class UUIDUtil {

	private static final String ID_FORMAT = "urn:bnb:%s:%s";
	private static final Pattern TYPE_PATTERN = Pattern.compile("urn\\:bnb\\:([^\\:]+)");

	private static String newId(Class<? extends DataObject> clazz, String id) {
		return String.format(ID_FORMAT, clazz.getSimpleName(), id);
	}

	public static String createId(Class<? extends DataObject> clazz) {
		return newId(clazz, UUID.randomUUID().toString());
	}
	
	public static boolean isValid(String urn) {
		
		if(StringUtils.isEmpty(urn)) {
			return false;
		}

        /*
         * (?i) - ignores case of letters in following parentheses
         * urn:bnb: - matches exact setting, this is consistent for all BNBNeeds uris
         * [A-Z]+: - This will match the class with any number of letters followed by a colon
         * [A-F0-9]{8} - used for matching UUID, This segment is 8 hex characters.
         * The full UUID pattern is all Hex characters separated by '-' in specific quantities
         * (:[A-Z0-9]+)? - (optional) any amount of letters or numbers preceded by a colon
         * 
         * Only legal characters (letters(any case), numbers, '-', ':')
         */

        return urn.matches("(?i)(urn:bnb:" +
                "[A-Z]+:" +
                "[A-F0-9]{8}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{12}" +
                "(:[A-Z0-9]+)?)");
    }
	
	@SuppressWarnings("rawtypes")
	public static boolean isType(String uri, Class clazz) {
        String simpleName = clazz.getSimpleName();
        return uri.startsWith("urn:bnb:" + simpleName);
    }
	
	public static String getTypeName(String id) {
        Matcher m = TYPE_PATTERN.matcher(id);
        if (m.find()) {
            return m.group(1);
        }
        return null;
    }
	
}
