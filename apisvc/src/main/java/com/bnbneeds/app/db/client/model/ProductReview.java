package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class ProductReview extends Review {

	@Index
	@Load
	private Ref<Product> product;
	@Index
	@Load
	private Ref<Buyer> reviewedBy;

	public Product getProduct() {
		return product.get();
	}

	public Buyer getReviewedBy() {
		return reviewedBy.get();
	}

	public void setProduct(Product product) {
		this.product = Ref.create(product);
	}

	public void setReviewedBy(Buyer reviewedBy) {
		this.reviewedBy = Ref.create(reviewedBy);
	}

}
