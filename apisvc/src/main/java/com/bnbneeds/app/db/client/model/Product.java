package com.bnbneeds.app.db.client.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;
import com.googlecode.objectify.condition.IfNotZero;

@Entity
@Cache
public class Product extends DescribedDataObject {

	@Index
	@Load
	private Ref<ProductName> productName;
	@Index
	@Load
	private Ref<Vendor> vendor;
	@Index
	@Load
	private Ref<ProductCategory> productCategory;
	@Index
	@Load
	private Ref<ProductType> productType;

	@Index
	private String nativeId;
	@Index({ IfNotZero.class })
	private double costPrice;
	private String modelId;
	private String serviceLocations;
	private Map<String, String> attributes;
	private Set<BlobObject> images;
	private String videoURL;
	private String tutorialVideoURL;

	public double getCostPrice() {
		return costPrice;
	}

	public String getModelId() {
		return modelId;
	}

	public String getServiceLocations() {
		return serviceLocations;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setCostPrice(double costPrice) {
		this.costPrice = costPrice;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public void setServiceLocations(String serviceLocations) {
		this.serviceLocations = serviceLocations;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	public void addAttribute(String key, String value) {
		if (key == null || value == null) {
			throw new IllegalArgumentException("Key or value is null.");
		}
		if (attributes == null) {
			attributes = new HashMap<String, String>();
		}
		attributes.put(key, value);
	}

	public ProductCategory getProductCategory() {
		return productCategory.get();
	}

	public ProductType getProductType() {
		return productType.get();
	}

	public String getNativeId() {
		return nativeId;
	}

	public void setNativeId(String nativeId) {
		this.nativeId = nativeId;
	}

	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = Ref.create(productCategory);
	}

	public void setProductType(ProductType productType) {
		this.productType = Ref.create(productType);
	}

	public Vendor getVendor() {
		return vendor.get();
	}

	public void setVendor(Vendor vendor) {
		this.vendor = Ref.create(vendor);
	}

	public ProductName getProductName() {
		return productName.get();
	}

	public void setProductName(ProductName productName) {
		this.productName = Ref.create(productName);
	}

	public Key<Product> getKeyByParentVendor() {

		if (getId() == null) {
			throw new IllegalArgumentException("Product id is not set.");
		}
		if (vendor == null) {
			throw new IllegalArgumentException("Parent vendor is not set.");
		}

		return Key.create(this.vendor.key(), Product.class, getId());

	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [");
		builder.append(super.toString());
		if (productCategory != null) {
			builder.append("productCategory=");
			builder.append(productCategory);
			builder.append(", ");
		}
		if (productType != null) {
			builder.append("productType=");
			builder.append(productType);
			builder.append(", ");
		}
		builder.append("costPrice=");
		builder.append(costPrice);
		builder.append(", ");
		if (modelId != null) {
			builder.append("modelId=");
			builder.append(modelId);
			builder.append(", ");
		}
		if (serviceLocations != null) {
			builder.append("serviceLocations=");
			builder.append(serviceLocations);
			builder.append(", ");
		}
		if (attributes != null) {
			builder.append("attributes=");
			builder.append(attributes);
		}
		builder.append("]");
		return builder.toString();
	}

	public Set<BlobObject> getImages() {
		return images;
	}

	public void setImages(Set<BlobObject> images) {
		this.images = images;
	}

	public void addImage(BlobObject image) {
		if (images == null) {
			images = new LinkedHashSet<BlobObject>();
		}
		images.add(image);
	}

	public void addImage(String mediaType, String imageKey, long size) {
		BlobObject image = new BlobObject(mediaType, imageKey, size);
		addImage(image);
	}

	public void removeImage(String imageKey) {
		if (images != null && !images.isEmpty()) {
			for (Iterator<BlobObject> iterator = images.iterator(); iterator
					.hasNext();) {
				BlobObject blobObject = iterator.next();
				if (blobObject.getBlobKey().equals(imageKey)) {
					iterator.remove();
				}

			}
		}
	}

	public Set<String> getImageKeys() {
		Set<String> imageKeys = new HashSet<String>();

		if (images != null && !images.isEmpty()) {
			for (Iterator<BlobObject> iterator = images.iterator(); iterator
					.hasNext();) {
				BlobObject blobObject = iterator.next();
				imageKeys.add(blobObject.getBlobKey());
			}
		}
		return imageKeys;
	}

	public boolean containsImageKey(String imageKey) {
		if (images != null && !images.isEmpty()) {
			for (Iterator<BlobObject> iterator = images.iterator(); iterator
					.hasNext();) {
				BlobObject blobObject = iterator.next();
				if (blobObject.getBlobKey().equals(imageKey)) {
					return true;
				}
			}
		}

		return false;
	}

	public String getVideoURL() {
		return videoURL;
	}

	public String getTutorialVideoURL() {
		return tutorialVideoURL;
	}

	public void setVideoURL(String videoURL) {
		this.videoURL = videoURL;
	}

	public void setTutorialVideoURL(String tutorialVideoURL) {
		this.tutorialVideoURL = tutorialVideoURL;
	}

}
