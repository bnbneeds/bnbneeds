package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.annotation.Index;

public class Review extends DescribedDataObject {

	@Index
	private Long timestamp;
	@Index
	private Integer rating;
	private String headline;

	public Integer getRating() {
		return rating;
	}

	public String getHeadline() {
		return headline;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Review [");
		builder.append(super.toString());
		if (rating != null)
			builder.append("rating=").append(rating).append(", ");
		if (headline != null)
			builder.append("headline=").append(headline);
		builder.append("]");
		return builder.toString();
	}
}
