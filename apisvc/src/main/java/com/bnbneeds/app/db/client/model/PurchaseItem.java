package com.bnbneeds.app.db.client.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

import com.bnbneeds.app.util.StringUtils;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class PurchaseItem extends DescribedDataObject {

	@Index
	@Load
	private Ref<Buyer> buyer;

	@Index
	@Load
	private Ref<Vendor> vendor;

	@Index
	@Load
	private Ref<ProductName> productName;

	@Index
	private long requestTimestamp;
	@Index
	private String nativeId;
	private List<OrderStatus> statusUpdates;
	private double requiredQuantity;
	private double deliveredQuantity;
	private String unit;
	private String tags;

	public ProductName getProductName() {
		return productName.get();
	}

	public double getRequiredQuantity() {
		return requiredQuantity;
	}

	public double getDeliveredQuantity() {
		return deliveredQuantity;
	}

	public List<OrderStatus> getStatusUpdates() {
		return statusUpdates;
	}

	public Buyer getBuyer() {
		return buyer.get();
	}

	public Vendor getVendor() {
		return vendor.get();
	}

	public String getNativeId() {
		return nativeId;
	}

	public Units.Quantity getUnit() {
		return Units.Quantity.valueOf(unit);
	}

	public long getRequestTimestamp() {
		return requestTimestamp;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public void setRequestTimestamp(long requestTimestamp) {
		this.requestTimestamp = requestTimestamp;
	}

	public void setUnit(Units.Quantity unit) {
		this.unit = unit.name();
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setNativeId(String nativeId) {
		this.nativeId = nativeId;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = Ref.create(vendor);
	}

	public void setVendorToNull() {
		this.vendor = null;
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = Ref.create(buyer);
	}

	public void setStatusUpdates(List<OrderStatus> statusUpdates) {
		this.statusUpdates = statusUpdates;
	}

	public void setProductName(ProductName productName) {
		this.productName = Ref.create(productName);
	}

	public void setRequiredQuantity(double requiredQuantity) {
		this.requiredQuantity = requiredQuantity;
	}

	public void setDeliveredQuantity(double deliveredQuantity) {
		this.deliveredQuantity = deliveredQuantity;
	}

	public void addOrderStatus(OrderStatus orderStatus) {
		if (statusUpdates == null) {
			statusUpdates = new ArrayList<>();
		}
		statusUpdates.add(orderStatus);
	}

	public void addOrderStatus(String timestamp, String state, String comments, String updatedBy) {

		OrderStatus orderStatus = new OrderStatus();
		orderStatus.setTimestamp(timestamp);
		orderStatus.setState(state);
		orderStatus.setComments(comments);
		orderStatus.setUpdatedBy(updatedBy);
		addOrderStatus(orderStatus);
	}

	public static String generateNativeId(String requestDate, String buyerId, String vendorId, String productNameId) {
		return String.format("%1$s+%2$s+%3$s+%4$s", requestDate, buyerId, vendorId, productNameId);
	}

	public static String generateNativeId(String requestDate, String buyerId, String vendorId, String productNameId,
			String tags) {
		return String.format("%1$s+%2$s+%3$s+%4$s+%5$s", requestDate, buyerId, vendorId, productNameId, tags);
	}

	public static String sanitizeTags(String tags) {

		Assert.hasText(tags, "Tags cannot be null or empty.");
		String sanitizedTags = StringUtils.trimAllWhitespace(tags);
		sanitizedTags = sanitizedTags.toLowerCase();
		return sanitizedTags;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PurchaseItem [");
		builder.append(super.toString());
		builder.append("requestTimestamp=").append(requestTimestamp).append(", ");
		if (buyer != null)
			builder.append("buyer=").append(buyer).append(", ");
		if (vendor != null)
			builder.append("vendor=").append(vendor).append(", ");
		if (productName != null)
			builder.append("productName=").append(productName).append(", ");

		if (nativeId != null)
			builder.append("nativeId=").append(nativeId).append(", ");
		if (statusUpdates != null)
			builder.append("statusUpdates=").append(statusUpdates).append(", ");
		builder.append("requiredQuantity=").append(requiredQuantity).append(", ");
		builder.append("deliveredQuantity=").append(deliveredQuantity).append(", ");
		if (unit != null)
			builder.append("unit=").append(unit);
		builder.append("]");
		return builder.toString();
	}

}
