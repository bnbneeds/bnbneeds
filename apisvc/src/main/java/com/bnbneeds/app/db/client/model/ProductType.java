package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;

@Entity
@Cache
public class ProductType extends DescribedDataObject {

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductType [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}

}
