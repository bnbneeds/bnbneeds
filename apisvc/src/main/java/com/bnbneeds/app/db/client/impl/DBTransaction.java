package com.bnbneeds.app.db.client.impl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.db.client.impl.DBOperation.OperationType;
import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.util.StringUtils;
import com.googlecode.objectify.VoidWork;

class DBTransaction {

	private static final Logger logger = LoggerFactory.getLogger(DBTransaction.class);

	private static final int MAX_TRANSACTION_ENTITIES = 25;

	private List<DBOperation> operationList;

	public DBTransaction() {
		operationList = new ArrayList<>();
	}

	public DBTransaction(DBOperationList dbOperationList) {

		if (dbOperationList == null || dbOperationList.get() == null || dbOperationList.get().isEmpty()) {
			logger.error("Operation list is empty. Call begin() to initialize.");
			throw new DBTransactionException("Operation list is null. Call begin() to initialize.");
		}
		operationList = new ArrayList<>();
		operationList.addAll(dbOperationList.get());
	}

	public DBTransaction addOperation(DBOperation op) {
		if (op == null) {
			logger.error("DBOperation is null.");
			throw new DBTransactionException("DBOperation is null.");
		}
		operationList.add(op);
		return this;
	}

	public void execute(final String timestamp) {

		if (operationList != null && !operationList.isEmpty()) {

			int size = operationList.size();

			if (size > MAX_TRANSACTION_ENTITIES) {
				int fromIndex = 0;
				int toIndex = MAX_TRANSACTION_ENTITIES;
				List<DBOperation> list = null;
				while (toIndex < size) {
					list = operationList.subList(fromIndex, toIndex);
					transact(list, timestamp);
					fromIndex = toIndex;
					toIndex += MAX_TRANSACTION_ENTITIES;
				}
				if (fromIndex < size) {
					list = operationList.subList(fromIndex, size);
					transact(list, timestamp);
				}
			} else {
				transact(operationList, timestamp);
			}

		}
	}

	private static void transact(final List<DBOperation> dbOperationList, final String timestamp) {

		if (dbOperationList != null && !dbOperationList.isEmpty()) {

			ofy().transactNew(new VoidWork() {
				@Override
				public void vrun() {
					logger.debug("Executing transaction...");

					for (Iterator<DBOperation> iterator = dbOperationList.iterator(); iterator.hasNext();) {

						DBOperation operation = iterator.next();
						DataObject dataObject = operation.getDataObject();
						if (dataObject == null) {
							logger.error("dataObject is null");
							break;
						}
						OperationType opType = operation.getOperationType();
						logger.debug("{}", operation);
						switch (opType) {
						case INSERT:
							dataObject.setCreateTimestamp(timestamp);
							ofy().defer().save().entity(dataObject);
							break;
						case UPDATE:
							if (StringUtils.isEmpty(dataObject.getId())) {
								logger.error("DataObject Id is null or empty");
							} else {
								dataObject.setUpdateTimestamp(timestamp);
								ofy().defer().save().entity(dataObject);
							}
							break;
						case MARK_AS_DELETED:
							if (StringUtils.isEmpty(dataObject.getId())) {
								logger.error("DataObject Id is null or empty");
							} else {
								if (!dataObject.isInactive()) {
									dataObject.setInactive(true);
									dataObject.setUpdateTimestamp(timestamp);
									ofy().defer().save().entity(dataObject);
								}
							}
							break;
						case DELETE:
							if (StringUtils.isEmpty(dataObject.getId())) {
								logger.error("DataObject Id is null or empty");
							} else {
								ofy().defer().delete().entity(dataObject);
							}
							break;
						default:
							break;
						}
					}
				}

			});
		}
	}
}
