package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class VendorBiddingSubscription extends DealerBiddingSubscription {

	@Load
	@Index
	private Ref<Vendor> vendor;

	@Load
	@Index
	private Ref<VendorBiddingSubscriptionPlan> subscriptionPlan;

	private int numberOfTendersAccessed;

	@Override
	public VendorBiddingSubscriptionPlan getSubscriptionPlan() {
		return subscriptionPlan.get();
	}

	public void setSubscriptionPlan(VendorBiddingSubscriptionPlan subscriptionPlan) {
		this.subscriptionPlan = Ref.create(subscriptionPlan);
	}

	public Vendor getVendor() {
		return vendor.get();
	}

	public void setVendor(Vendor vendor) {
		this.vendor = Ref.create(vendor);
	}

	public int getNumberOfTendersAccessed() {
		return numberOfTendersAccessed;
	}

	public void setNumberOfTendersAccessed(int numberOfTendersAccessed) {
		this.numberOfTendersAccessed = numberOfTendersAccessed;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VendorBiddingSubscription [");
		builder.append(super.toString());
		if (vendor != null)
			builder.append("vendor=").append(vendor).append(", ");
		if (subscriptionPlan != null)
			builder.append("biddingSubscriptionPlan=").append(subscriptionPlan).append(", ");
		builder.append("numberOfTendersAccessed=").append(numberOfTendersAccessed).append("]");
		return builder.toString();
	}

	@Override
	public void setQuotaUsage(int usage) {
		setNumberOfTendersAccessed(usage);
	}

	@Override
	public int getUsedQuota() {
		return getNumberOfTendersAccessed();
	}
	
}
