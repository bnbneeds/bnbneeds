package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class ProductOffer extends DescribedDataObject {

	@Index
	@Load
	private Ref<Product> product;
	@Index
	private String startDate;
	@Index
	private String endDate;

	public Product getProduct() {
		return product.get();
	}

	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setProduct(Product product) {
		this.product = Ref.create(product);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductOffer [");
		builder.append(super.toString());
		if (product != null)
			builder.append("product=").append(product).append(", ");
		if (startDate != null)
			builder.append("startDate=").append(startDate).append(", ");
		if (endDate != null)
			builder.append("endDate=").append(endDate);
		builder.append("]");
		return builder.toString();
	}

}
