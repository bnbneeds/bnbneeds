package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class BuyerBiddingSubscription extends DealerBiddingSubscription {

	@Index
	@Load
	private Ref<Buyer> buyer;

	@Index
	@Load
	private Ref<BuyerBiddingSubscriptionPlan> subscriptionPlan;

	private int numberOfTendersCreated;

	@Override
	public BuyerBiddingSubscriptionPlan getSubscriptionPlan() {
		return subscriptionPlan.get();
	}

	public void setSubscriptionPlan(BuyerBiddingSubscriptionPlan subscriptionPlan) {
		this.subscriptionPlan = Ref.create(subscriptionPlan);
	}

	public Buyer getBuyer() {
		return buyer.get();
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = Ref.create(buyer);
	}

	public int getNumberOfTendersCreated() {
		return numberOfTendersCreated;
	}

	public void setNumberOfTendersCreated(int numberOfTendersCreated) {
		this.numberOfTendersCreated = numberOfTendersCreated;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BuyerBiddingSubscription [");
		builder.append(super.toString());
		if (buyer != null)
			builder.append("buyer=").append(buyer).append(", ");
		if (subscriptionPlan != null)
			builder.append("biddingSubscriptionPlan=").append(subscriptionPlan).append(", ");
		builder.append("numberOfTendersCreated=").append(numberOfTendersCreated).append(", ");
		builder.append("]");
		return builder.toString();
	}

	@Override
	public void setQuotaUsage(int usage) {
		setNumberOfTendersCreated(usage);

	}

	@Override
	public int getUsedQuota() {
		return getNumberOfTendersCreated();
	}
	
}
