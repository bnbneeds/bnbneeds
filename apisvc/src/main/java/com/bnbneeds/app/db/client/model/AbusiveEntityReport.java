package com.bnbneeds.app.db.client.model;

import com.bnbneeds.app.db.client.model.ApprovedDataObject.EntityStatus;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfNotEmpty;
import com.googlecode.objectify.condition.IfNotNull;

@Entity
public class AbusiveEntityReport extends DescribedDataObject {

	@Index({ IfNotNull.class, IfNotEmpty.class })
	private String entityId;
	@Index
	private Long timestamp;
	private String title;
	private String reportedUserAccountId;
	private String assignedUserId;
	private String reportStatus;
	
	public static enum ReportStatus {
		OPEN, ASSIGNED, CLOSED ;
	}
	
	public String getAssignedUserId() {
		return assignedUserId;
	}

	public void setAssignedUserId(String assignedUserId) {
		this.assignedUserId = assignedUserId;
	}

	public ReportStatus getReportStatus() {
		return ReportStatus.valueOf(reportStatus);
	}

	public void setReportStatus(ReportStatus reportStatus) {
		this.reportStatus = reportStatus.name();
	}
	
	public boolean isClosed() {
		return hasStatus(ReportStatus.CLOSED);
	}
	

	public boolean hasStatus(ReportStatus reportStatus) {
		return reportStatus == getReportStatus();
	}

	public String getEntityId() {
		return entityId;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public String getTitle() {
		return title;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getReportedUserAccountId() {
		return reportedUserAccountId;
	}

	public void setReportedUserAccountId(String reportedUserAccountId) {
		this.reportedUserAccountId = reportedUserAccountId;
	}

}
