package com.bnbneeds.app.db.client.impl;

import static com.googlecode.objectify.ObjectifyService.begin;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.db.client.DbClient;
import com.bnbneeds.app.db.client.DbClientUtils;
import com.bnbneeds.app.db.client.OrderBy;
import com.bnbneeds.app.db.client.ResultSet;
import com.bnbneeds.app.db.client.ResultSetIterator;
import com.bnbneeds.app.db.client.constraint.ParentKeyConstraint;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.model.DataObject;
import com.bnbneeds.app.util.StringUtils;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.QueryResultIterator;
import com.googlecode.objectify.cmd.Query;
import com.googlecode.objectify.util.Closeable;

/**
 * This is same as {@link DbClientImpl} except all its methods are
 * {@code public static}. This class is required for execution of async tasks in
 * GAE queues because DbClientImpl cannot (must not) be injected (autowired) and
 * serialized.
 * 
 * @author Amit Herlekar
 *
 */
public class ObjectifyHandle {

	private static final Logger logger = LoggerFactory.getLogger(ObjectifyHandle.class);

	private static final SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat(DbClient.TIMESTAMP_PATTERN);

	private static volatile Closeable closeable;

	public static void init() {
		closeable = begin();
	}

	public void destroy() {
		if (closeable != null) {
			closeable.close();
		}

	}

	private static void setQueryForActiveEntity(QueryFields fields) {
		fields.add("inactive", false);
	}

	private static void setQueryForInactiveEntity(QueryFields fields) {
		fields.add("inactive", true);
	}

	public static <T extends DataObject> void createObject(T object) {
		object.setCreateTimestamp(TIMESTAMP_FORMAT.format(new Date()));
		logger.info("Creating entity record in DB: {}", object);
		ofy().save().entity(object).now();
	}

	public static <T extends DataObject> void updateObject(T object) throws DBClientException {
		logger.info("Updating entity record in DB: {}", object);
		if (StringUtils.isEmpty(object.getId())) {
			throw new DBClientException("DataObject id cannot be null or empty");
		}
		object.setUpdateTimestamp(TIMESTAMP_FORMAT.format(new Date()));
		ofy().save().entity(object).now();
	}

	public static <T extends DataObject> void deleteObject(T object) {
		logger.info("Deleting entity record from DB: {}", object);
		if (StringUtils.isEmpty(object.getId())) {
			throw new DBClientException("DataObject id cannot be null or empty");
		}
		ofy().delete().entity(object).now();
	}

	public static <T extends DataObject> void deleteObjects(List<T> objects) {
		logger.info("Deleting entities from DB: {}", objects);
		ofy().delete().entities(objects.toArray()).now();
	}

	public static <T extends DataObject> T queryObject(Class<T> clazz, String id) {
		logger.info("Querying entity {} with fields: {}", clazz.getSimpleName(), id);
		T object = ofy().load().type(clazz).id(id).now();
		if (object != null && object.isInactive()) {
			return null;
		}
		return object;

	}

	@SuppressWarnings("unchecked")
	public static <T extends DataObject> T queryObject(String clazzName, String id) {
		logger.info("Querying entity {} with id: {}", clazzName, id);
		Class<T> clazz = null;

		try {
			clazz = (Class<T>) DbClientUtils.getClassByName(clazzName);
		} catch (ClassNotFoundException e) {
			clazz = null;
		}

		if (clazz == null) {
			return null;
		}

		return queryObject(clazz, id);

	}

	public static <T extends DataObject> T queryObjectByFields(Class<T> clazz, QueryFields fields) {

		setQueryForActiveEntity(fields);

		logger.info("Querying entity for class: {} using fields: {}", clazz.getSimpleName(), fields);

		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);

		T object = q.first().now();

		if (object != null && object.isInactive()) {
			return null;
		}
		return object;
	}

	public static <T extends DataObject> List<T> queryList(Class<T> clazz, ParentKeyConstraint parentConstraint) {

		logger.info("Querying entity list for class: {} using fields: {}", clazz.getSimpleName(), parentConstraint);

		Query<T> q = QueryBuilder.create(ofy(), clazz, parentConstraint);

		List<T> sourceList = q.list();

		filterInactiveEntities(sourceList);

		return sourceList;
	}

	public static <T extends DataObject> List<T> queryList(Class<T> clazz) {

		logger.info("Querying list for entity: {}", clazz.getSimpleName());

		QueryFields fields = new QueryFields();
		setQueryForActiveEntity(fields);

		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);
		List<T> list = q.list();

		return list;
	}

	public static <T extends DataObject> List<T> queryList(Class<T> clazz, QueryFields fields) {
		setQueryForActiveEntity(fields);
		logger.info("Querying list for class: {} using fields: {}", clazz.getSimpleName(), fields);
		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);
		List<T> list = q.list();

		return list;
	}

	public static <T extends DataObject> T queryObjectByFields(Class<T> clazz, ParentKeyConstraint parentConstraint,
			QueryFields fields) {

		setQueryForActiveEntity(fields);

		logger.info("Querying entity: {} using fields: {}", clazz.getSimpleName(), parentConstraint);

		Query<T> q = QueryBuilder.create(ofy(), clazz, parentConstraint);
		q = QueryBuilder.create(q, fields);
		T object = q.first().now();
		return object;
	}

	public static void transact(DBOperationList operationList) {
		if (!operationList.isEmpty()) {
			DBTransaction transaction = new DBTransaction(operationList);
			transaction.execute(TIMESTAMP_FORMAT.format(new Date()));
		}
	}

	public static <T extends DataObject> List<T> queryList(Class<T> clazz, ParentKeyConstraint constraint,
			int fetchSize) {

		logger.info("Querying list of size {} for entity: {} using fields: {}", fetchSize, clazz.getSimpleName(),
				constraint);

		Query<T> q = QueryBuilder.create(ofy(), clazz, constraint);

		return q.limit(fetchSize).list();
	}

	public static <T extends DataObject> List<T> queryList(Class<T> clazz, int fetchSize) {

		QueryFields fields = new QueryFields();
		setQueryForActiveEntity(fields);

		logger.info("Querying list of size {} for entity: {}", fetchSize, clazz.getSimpleName());

		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);
		List<T> list = q.limit(fetchSize).list();
		return list;
	}

	public static <T extends DataObject> int countOf(Class<T> clazz, QueryFields fields) {
		setQueryForActiveEntity(fields);
		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);
		return q.count();
	}

	public static <T extends DataObject> int countOf(Class<T> clazz) {
		QueryFields fields = new QueryFields();
		setQueryForActiveEntity(fields);
		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);
		return q.count();
	}

	public static <T extends DataObject> List<T> queryList(Class<T> clazz, QueryFields fields, int fetchSize) {

		setQueryForActiveEntity(fields);

		logger.info("Querying list of size {} for entity {} using fields: {}", fetchSize, clazz.getSimpleName(),
				fields);

		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);
		return q.limit(fetchSize).list();
	}

	public static <T extends DataObject> List<T> queryListBySortOrder(Class<T> clazz, OrderBy orderBy,
			String orderByField) {

		QueryFields fields = new QueryFields();
		setQueryForActiveEntity(fields);

		logger.info("Querying list for entity {} by sort order {} of: {}", clazz.getSimpleName(), orderBy.name(),
				orderByField);

		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);

		q = addOrderByToQuery(q, orderBy, orderByField);

		return q.list();
	}

	public static <T extends DataObject> List<T> queryListBySortOrder(Class<T> clazz, QueryFields fields,
			OrderBy orderBy, String orderByField) {

		setQueryForActiveEntity(fields);

		logger.info("Querying list for entity {} using fields: {} by sort order {} of: {}", clazz.getSimpleName(),
				fields, orderBy.name(), orderByField);

		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);

		q = addOrderByToQuery(q, orderBy, orderByField);
		return q.list();

	}

	public static <T extends DataObject> List<T> queryListBySortOrder(Class<T> clazz, QueryFields fields, int fetchSize,
			OrderBy orderBy, String orderByField) {

		setQueryForActiveEntity(fields);

		logger.info("Querying list or size {} for entity {} using fields: {} by sort order {} of: {}", fetchSize,
				clazz.getSimpleName(), fields, orderBy.name(), orderByField);

		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);

		q = addOrderByToQuery(q, orderBy, orderByField);

		return q.limit(fetchSize).list();

	}

	public static <T extends DataObject> ResultSet<T> queryResultSet(Class<T> clazz, String offsetString,
			int fetchSize) {

		QueryFields fields = new QueryFields();
		setQueryForActiveEntity(fields);

		logger.info("Querying result set of size {} for entity {} with offset: {}", fetchSize, clazz.getSimpleName(),
				offsetString);

		Query<T> query = QueryBuilder.create(ofy(), clazz, fields);
		query = query.limit(fetchSize);

		return getResultSet(query, offsetString);

	}

	public static <T extends DataObject> ResultSet<T> queryResultSet(Class<T> clazz, QueryFields fields,
			String offsetString, int fetchSize) {
		logger.info("Querying result set of size {} for entity {} using fields {} with offset: {}", fetchSize,
				clazz.getSimpleName(), fields, offsetString);

		Query<T> query = QueryBuilder.create(ofy(), clazz, fields);
		query = query.limit(fetchSize);

		return getResultSet(query, offsetString);

	}

	public static <T extends DataObject> ResultSetIterator<T> queryResultSetIterator(Class<T> clazz,
			String offsetString, int fetchSize) {

		QueryFields fields = new QueryFields();
		setQueryForActiveEntity(fields);

		logger.info("Querying result set iterator of size {} for entity {} with offset: {}", fetchSize,
				clazz.getSimpleName(), offsetString);

		Query<T> query = QueryBuilder.create(ofy(), clazz, fields);
		query = query.limit(fetchSize);

		if (StringUtils.hasText(offsetString)) {
			query = query.startAt(Cursor.fromWebSafeString(offsetString));
		}

		return new ResultSetIterator<>(query.iterator());

	}

	public static <T extends DataObject> ResultSetIterator<T> queryResultSetIterator(Class<T> clazz, QueryFields fields,
			String offsetString, int fetchSize) {
		setQueryForActiveEntity(fields);

		logger.info("Querying result set iterator of size {} for entity {} using fields {} with offset: {}", fetchSize,
				clazz.getSimpleName(), fields, offsetString);

		Query<T> query = QueryBuilder.create(ofy(), clazz, fields);
		query = query.limit(fetchSize);

		if (StringUtils.hasText(offsetString)) {
			query = query.startAt(Cursor.fromWebSafeString(offsetString));
		}

		return new ResultSetIterator<>(query.iterator());

	}

	public static <T extends DataObject> ResultSetIterator<T> queryResultSetIteratorBySortOrder(Class<T> clazz,
			String offsetString, int fetchSize, OrderBy orderBy, String orderByField) {

		QueryFields fields = new QueryFields();
		setQueryForActiveEntity(fields);

		logger.info("Querying result set iterator of size {} for entity {} with offset: {}", fetchSize,
				clazz.getSimpleName(), offsetString);

		Query<T> query = QueryBuilder.create(ofy(), clazz, fields);

		query = addOrderByToQuery(query, orderBy, orderByField);

		query = query.limit(fetchSize);

		if (StringUtils.hasText(offsetString)) {
			query = query.startAt(Cursor.fromWebSafeString(offsetString));
		}

		return new ResultSetIterator<>(query.iterator());

	}

	public static <T extends DataObject> ResultSetIterator<T> queryResultSetIteratorBySortOrder(Class<T> clazz,
			QueryFields fields, String offsetString, int fetchSize, OrderBy orderBy, String orderByField) {

		setQueryForActiveEntity(fields);

		logger.info("Querying result set iterator of size {} for entity {} using fields {} with offset: {}", fetchSize,
				clazz.getSimpleName(), fields, offsetString);

		Query<T> query = QueryBuilder.create(ofy(), clazz, fields);

		query = addOrderByToQuery(query, orderBy, orderByField);

		query = query.limit(fetchSize);

		if (StringUtils.hasText(offsetString)) {
			query = query.startAt(Cursor.fromWebSafeString(offsetString));
		}

		return new ResultSetIterator<>(query.iterator());

	}

	private static <T extends DataObject> ResultSet<T> getResultSet(Query<T> query, String cursorString) {

		logger.debug("Get result set with query {} and offset: {}", query, cursorString);

		if (StringUtils.hasText(cursorString)) {
			query = query.startAt(Cursor.fromWebSafeString(cursorString));
		}

		QueryResultIterator<T> iterator = query.iterator();
		List<T> returnList = new ArrayList<>();
		try {
			while (iterator.hasNext()) {
				T obj = iterator.next();
				returnList.add(obj);
			}
		} catch (IllegalArgumentException e) {
			logger.error("IllegalArgumentException occured", e);
			throw new DBClientException(e.getMessage());
		}

		String nextCursorString = null;
		if (!returnList.isEmpty()) {
			Cursor cursor = iterator.getCursor();
			if (cursor != null) {
				nextCursorString = cursor.toWebSafeString();
			}
		}
		return new ResultSet<>(nextCursorString, returnList);

	}

	private static <T extends DataObject> Query<T> addOrderByToQuery(Query<T> query, OrderBy orderBy,
			String orderByField) {

		switch (orderBy) {
		case ASC:
			return query.order(orderByField);
		case DESC:
			return query.order("-" + orderByField);
		default:
			return null;
		}
	}

	private static <T extends DataObject> void filterInactiveEntities(List<T> sourceList) {
		if (sourceList != null && !sourceList.isEmpty()) {
			for (Iterator<T> iterator = sourceList.iterator(); iterator.hasNext();) {
				T item = iterator.next();
				if (item.isInactive()) {
					iterator.remove();
				}
			}
		}
	}

	public static <T extends DataObject> void markAsDeleted(T object) {
		object.setInactive(true);
		updateObject(object);
	}

	public static <T extends DataObject> int countOfDeletedEntities(Class<T> clazz, QueryFields fields) {
		setQueryForInactiveEntity(fields);
		logger.info("Querying list for class: {} using fields: {}", clazz.getSimpleName(), fields);
		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);
		return q.count();
	}

	public static <T extends DataObject> int countOfDeletedEntities(Class<T> clazz) {
		QueryFields fields = new QueryFields();
		setQueryForInactiveEntity(fields);
		logger.info("Querying list for class: {} using fields: {}", clazz.getSimpleName(), fields);
		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);
		return q.count();
	}

	public static <T extends DataObject> List<T> queryListMarkedAsDeleted(Class<T> clazz, QueryFields fields) {
		setQueryForInactiveEntity(fields);
		logger.info("Querying list for class: {} using fields: {}", clazz.getSimpleName(), fields);
		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);
		List<T> list = q.list();

		return list;
	}

	public static <T extends DataObject> List<T> queryListMarkedAsDeleted(Class<T> clazz, int fetchSize) {
		QueryFields fields = new QueryFields();
		setQueryForInactiveEntity(fields);
		logger.info("Querying list for class: {} using fields: {}", clazz.getSimpleName(), fields);
		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);
		List<T> list = q.limit(fetchSize).list();
		return list;
	}

	public static <T extends DataObject> List<T> queryListMarkedAsDeleted(Class<T> clazz, QueryFields fields,
			int fetchSize) {
		setQueryForInactiveEntity(fields);
		logger.info("Querying list for class: {} using fields: {}", clazz.getSimpleName(), fields);
		Query<T> q = QueryBuilder.create(ofy(), clazz, fields);
		List<T> list = q.limit(fetchSize).list();
		return list;
	}

	public static <T extends DataObject> ResultSet<T> queryResultSetMarkedAsDeleted(Class<T> clazz, String offsetString,
			int fetchSize) {

		QueryFields fields = new QueryFields();
		setQueryForInactiveEntity(fields);

		logger.info("Querying result set of size {} for entity {} with offset: {}", fetchSize, clazz.getSimpleName(),
				offsetString);

		Query<T> query = QueryBuilder.create(ofy(), clazz, fields);
		query = query.limit(fetchSize);

		return getResultSet(query, offsetString);

	}

	public static <T extends DataObject> ResultSet<T> queryResultSetMarkedAsDeleted(Class<T> clazz, QueryFields fields,
			String offsetString, int fetchSize) {

		setQueryForInactiveEntity(fields);

		logger.info("Querying result set of size {} for entity {} with offset: {}", fetchSize, clazz.getSimpleName(),
				offsetString);

		Query<T> query = QueryBuilder.create(ofy(), clazz, fields);
		query = query.limit(fetchSize);

		return getResultSet(query, offsetString);
	}

	public static <T extends DataObject> ResultSetIterator<T> queryResultSetMarkedAsDeletedIterator(Class<T> clazz,
			String offsetString, int fetchSize) {

		QueryFields fields = new QueryFields();
		setQueryForInactiveEntity(fields);

		logger.info("Querying result set iterator of size {} for entity {} with offset: {}", fetchSize,
				clazz.getSimpleName(), offsetString);

		Query<T> query = QueryBuilder.create(ofy(), clazz, fields);
		query = query.limit(fetchSize);

		if (StringUtils.hasText(offsetString)) {
			query = query.startAt(Cursor.fromWebSafeString(offsetString));
		}

		return new ResultSetIterator<>(query.iterator());

	}

	public static <T extends DataObject> ResultSetIterator<T> queryResultSetMarkedAsDeletedIterator(Class<T> clazz,
			QueryFields fields, String offsetString, int fetchSize) {

		setQueryForInactiveEntity(fields);
		logger.info("Querying result set iterator of size {} for entity {} with offset: {}", fetchSize,
				clazz.getSimpleName(), offsetString);

		Query<T> query = QueryBuilder.create(ofy(), clazz, fields);
		query = query.limit(fetchSize);

		if (StringUtils.hasText(offsetString)) {
			query = query.startAt(Cursor.fromWebSafeString(offsetString));
		}

		return new ResultSetIterator<>(query.iterator());

	}
}
