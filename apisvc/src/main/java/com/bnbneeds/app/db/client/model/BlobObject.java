package com.bnbneeds.app.db.client.model;

public class BlobObject {

	private String mediaType;
	private String blobKey;
	private long size;

	public BlobObject() {
		super();
	}

	public BlobObject(String mediaType, String blobKey, long size) {
		super();
		this.mediaType = mediaType;
		this.blobKey = blobKey;
		this.size = size;
	}

	public String getMediaType() {
		return mediaType;
	}

	public String getBlobKey() {
		return blobKey;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public void setBlobKey(String blobKey) {
		this.blobKey = blobKey;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BlobObject [");
		if (mediaType != null)
			builder.append("mediaType=").append(mediaType).append(", ");
		if (blobKey != null)
			builder.append("blobKey=").append(blobKey).append(", ");
		builder.append("size=").append(size).append(" bytes ]");
		return builder.toString();
	}

}
