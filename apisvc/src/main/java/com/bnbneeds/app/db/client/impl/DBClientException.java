package com.bnbneeds.app.db.client.impl;

public class DBClientException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DBClientException() {
		super();
	}


	public DBClientException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public DBClientException(String arg0) {
		super(arg0);
	}

	public DBClientException(Throwable arg0) {
		super(arg0);
	}
	
	

}
