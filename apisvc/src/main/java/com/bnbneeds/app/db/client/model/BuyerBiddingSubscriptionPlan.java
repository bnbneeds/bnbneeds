package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;

@Entity
@Cache
public class BuyerBiddingSubscriptionPlan extends BiddingSubscriptionPlan {

	private int numberOfTendersAllowedToCreate;


	public int getNumberOfTendersAllowedToCreate() {
		return numberOfTendersAllowedToCreate;
	}

	public void setNumberOfTendersAllowedToCreate(int numberOfTendersAllowedToCreate) {
		this.numberOfTendersAllowedToCreate = numberOfTendersAllowedToCreate;
	}

	@Override
	public int getQuota() {
		return getNumberOfTendersAllowedToCreate();
	}

	@Override
	public void setQuota(int quota) {
		setNumberOfTendersAllowedToCreate(quota);
	}

}
