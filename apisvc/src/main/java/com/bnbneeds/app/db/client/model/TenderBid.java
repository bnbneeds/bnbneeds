package com.bnbneeds.app.db.client.model;

import com.bnbneeds.app.util.StringUtils;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

@Entity
@Cache
public class TenderBid extends DataObject {

	@Index
	@Load
	private Ref<Vendor> vendor;
	@Index
	@Load
	private Ref<Tender> tender;
	@Index
	@Load
	private double value;
	private String comments;

	public Vendor getVendor() {
		return vendor.get();
	}

	public void setVendor(Vendor vendor) {
		this.vendor = Ref.create(vendor);
	}

	public Tender getTender() {
		return tender.get();
	}

	public void setTender(Tender tender) {
		this.tender = Ref.create(tender);
	}

	public double getValue() {
		return value;
	}

	public String getComments() {
		return comments;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public boolean isBidByVendor(String vendorId){
		return (vendor.get() == null || StringUtils.isEmpty(vendorId)) ? false : vendor.get().getId().equals(vendorId);
	}

}
