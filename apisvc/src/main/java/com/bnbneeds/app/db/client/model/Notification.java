package com.bnbneeds.app.db.client.model;

import java.util.HashSet;
import java.util.Set;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Entity
@Cache
public class Notification extends DataObject {

	@Index
	private Long timestamp;
	private String type;
	private String notificationText;
	private String recipientType;
	private String recipientId;
	private String entityId;
	private String entityName;
	private String link;
	private String triggeredByDealerName;
	private Set<String> readByList;

	public Long getTimestamp() {
		return timestamp;
	}

	public String getNotificationText() {
		return notificationText;
	}

	public String getType() {
		return type;
	}

	public String getTriggeredByDealerName() {
		return triggeredByDealerName;
	}

	public void setTriggeredByDealerName(String triggeredByDealerName) {
		this.triggeredByDealerName = triggeredByDealerName;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public void setNotificationText(String notificationText) {
		this.notificationText = notificationText;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getEntityId() {
		return entityId;
	}

	public String getEntityName() {
		return entityName;
	}

	public void addReadBy(String userId) {
		if (readByList == null) {
			readByList = new HashSet<String>();
		}
		readByList.add(userId);
	}

	public void removeReadBy(String userId) {
		if (readByList != null && !readByList.isEmpty()) {
			readByList.remove(userId);
		}
	}

	public boolean isRead(String userId) {
		if (readByList == null || readByList.isEmpty()) {
			return false;
		}
		return readByList.contains(userId);
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getRecipientType() {
		return recipientType;
	}

	public String getRecipientId() {
		return recipientId;
	}

	public void setRecipientType(String recipientType) {
		this.recipientType = recipientType;
	}

	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Notification [");
		if (timestamp != null)
			builder.append("timestamp=").append(timestamp).append(", ");
		if (type != null)
			builder.append("type=").append(type).append(", ");
		if (notificationText != null)
			builder.append("notificationText=").append(notificationText)
					.append(", ");
		if (recipientType != null)
			builder.append("recipientType=").append(recipientType).append(", ");
		if (recipientId != null)
			builder.append("recipientId=").append(recipientId).append(", ");
		if (entityId != null)
			builder.append("entityId=").append(entityId).append(", ");
		if (entityName != null)
			builder.append("entityName=").append(entityName).append(", ");
		if (link != null)
			builder.append("link=").append(link).append(", ");
		if (triggeredByDealerName != null)
			builder.append("triggeredByDealerName=").append(
					triggeredByDealerName);
		builder.append("]");
		return builder.toString();
	}

}
