package com.bnbneeds.app.db.client.model;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

public abstract class DealerBiddingSubscriptionUsage extends DataObject {

	@Index
	@Load
	private Ref<Tender> tender;

	public Tender getTender() {
		return tender.get();
	}

	public void setTender(Tender tender) {
		this.tender = Ref.create(tender);
	}

	public abstract DealerBiddingSubscription getBiddingSubscription();

	public abstract Dealer getDealer();

}
