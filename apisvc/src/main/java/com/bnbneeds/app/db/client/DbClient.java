package com.bnbneeds.app.db.client;

import java.util.List;

import com.bnbneeds.app.db.client.constraint.ParentKeyConstraint;
import com.bnbneeds.app.db.client.constraint.QueryFields;
import com.bnbneeds.app.db.client.impl.DBClientException;
import com.bnbneeds.app.db.client.impl.DBOperationList;
import com.bnbneeds.app.db.client.model.DataObject;

public interface DbClient {

	String DB_MODEL_PACKAGE = "com.bnbneeds.app.db.client.model";
	String TIMESTAMP_PATTERN = "dd-MMM-yyyy HH:mm:ss z";

	/**
	 * Initialize shared resources if they are used outside the scope of servlet
	 * filter. This will be useful especially for methods annotated with
	 * 
	 * @PostConstruct. Leave the implementation blank if not required.
	 */
	void init();

	/**
	 * This method can be useful if you have implemented the init() method.
	 */
	void destroy();

	void transact(DBOperationList operationList);

	<T extends DataObject> void createObject(T object);

	<T extends DataObject> void updateObject(T object) throws DBClientException;

	<T extends DataObject> void deleteObject(T object);

	<T extends DataObject> void deleteObjects(List<T> objects);

	/**
	 * 
	 * @param clazz
	 *            of type <code>DataObject</code>
	 * @param id
	 *            the id for the query
	 * @return an instance of <code>DataObject</code> if the record is found,
	 *         <code>null</code> otherwise.
	 */
	<T extends DataObject> T queryObject(Class<T> clazz, String id);

	<T extends DataObject> T queryObject(String clazzName, String id);

	<T extends DataObject> T queryObjectByFields(Class<T> clazz, QueryFields fields);

	<T extends DataObject> T queryObjectByFields(Class<T> clazz, ParentKeyConstraint parentConstraint,
			QueryFields fields);

	<T extends DataObject> List<T> queryList(Class<T> clazz, ParentKeyConstraint constraint);

	<T extends DataObject> List<T> queryList(Class<T> clazz);

	<T extends DataObject> List<T> queryList(Class<T> clazz, QueryFields fields);

	// Query list by limit

	<T extends DataObject> List<T> queryList(Class<T> clazz, ParentKeyConstraint constraint, int fetchSize);

	<T extends DataObject> List<T> queryList(Class<T> clazz, int fetchSize);

	<T extends DataObject> List<T> queryList(Class<T> clazz, QueryFields fields, int fetchSize);

	<T extends DataObject> ResultSet<T> queryResultSet(Class<T> clazz, String offsetString, int fetchSize);

	<T extends DataObject> ResultSet<T> queryResultSet(Class<T> clazz, QueryFields fields, String offsetString,
			int fetchSize);

	<T extends DataObject> ResultSetIterator<T> queryResultSetIterator(Class<T> clazz, String offsetString,
			int fetchSize);

	<T extends DataObject> ResultSetIterator<T> queryResultSetIterator(Class<T> clazz, QueryFields fields,
			String offsetString, int fetchSize);

	<T extends DataObject> List<T> queryListBySortOrder(Class<T> clazz, OrderBy orderBy, String orderByField);

	<T extends DataObject> List<T> queryListBySortOrder(Class<T> clazz, QueryFields fields, OrderBy orderBy,
			String orderByField);

	<T extends DataObject> List<T> queryListBySortOrder(Class<T> clazz, QueryFields fields, int fetchSize,
			OrderBy orderBy, String orderByField);

	<T extends DataObject> ResultSetIterator<T> queryResultSetIteratorBySortOrder(Class<T> clazz, String offsetString,
			int fetchSize, OrderBy orderBy, String orderByField);

	<T extends DataObject> ResultSetIterator<T> queryResultSetIteratorBySortOrder(Class<T> clazz, QueryFields fields,
			String offsetString, int fetchSize, OrderBy orderBy, String orderByField);

	<T extends DataObject> void markAsDeleted(T object);

	<T extends DataObject> List<T> queryListMarkedAsDeleted(Class<T> clazz, QueryFields fields);

	<T extends DataObject> List<T> queryListMarkedAsDeleted(Class<T> clazz, int fetchSize);

	<T extends DataObject> List<T> queryListMarkedAsDeleted(Class<T> clazz, QueryFields fields, int fetchSize);

	<T extends DataObject> ResultSet<T> queryResultSetMarkedAsDeleted(Class<T> clazz, String offsetString,
			int fetchSize);

	<T extends DataObject> ResultSet<T> queryResultSetMarkedAsDeleted(Class<T> clazz, QueryFields fields,
			String offsetString, int fetchSize);

	<T extends DataObject> ResultSetIterator<T> queryResultSetMarkedAsDeletedIterator(Class<T> clazz,
			String offsetString, int fetchSize);

	<T extends DataObject> ResultSetIterator<T> queryResultSetMarkedAsDeletedIterator(Class<T> clazz,
			QueryFields fields, String offsetString, int fetchSize);

	<T extends DataObject> int countOf(Class<T> clazz, QueryFields fields);

	<T extends DataObject> int countOf(Class<T> clazz);

	<T extends DataObject> int countOfDeletedEntities(Class<T> clazz, QueryFields fields);

	<T extends DataObject> int countOfDeletedEntities(Class<T> clazz);

}
