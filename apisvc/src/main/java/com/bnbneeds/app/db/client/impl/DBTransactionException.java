package com.bnbneeds.app.db.client.impl;

public class DBTransactionException extends DBClientException {


	private static final long serialVersionUID = 8247990471882275098L;

	public DBTransactionException() {
		super();
	}


	public DBTransactionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public DBTransactionException(String arg0) {
		super(arg0);
	}

	public DBTransactionException(Throwable arg0) {
		super(arg0);
	}
	
	

}
