package com.bnbneeds.app.db.client;

import java.util.List;

import com.bnbneeds.app.db.client.model.DataObject;

public class ResultSet<T extends DataObject> {

	private String offset;
	private List<T> list;

	
	public ResultSet(String offsetString, List<T> list) {
		super();
		this.offset = offsetString;
		this.list = list;
	}

	public String getOffset() {
		return offset;
	}

	public List<T> getList() {
		return list;
	}
}
