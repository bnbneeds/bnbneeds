package com.bnbneeds.app.messaging.email.impl.mailgun;

import javax.mail.Session;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.messaging.email.AbstractEmailClientImpl;
import com.bnbneeds.app.messaging.email.EmailMessage;
import com.bnbneeds.app.messaging.email.MessagingException;

public class MailGunEmailClient extends AbstractEmailClientImpl {

	private static final String BASE_URL = "https://api.mailgun.net/v3";

	private static final Logger logger = LoggerFactory
			.getLogger(MailGunEmailClient.class);

	private String domain;
	private Client client;

	private static Client initClient(String apiKey) {
		Client client = ClientBuilder.newClient();
		client.register(HttpAuthenticationFeature.basic("api", apiKey));
		client.register(MultiPartFeature.class);
		client.register(JacksonFeature.class);
		return client;
	}

	public MailGunEmailClient(String apiKey, String domain) {
		super();
		this.domain = domain;
		this.client = initClient(apiKey);
	}

	@Override
	public Session getMailSession() {
		return null;
	}

	private void invokeMailgunRestApiToSendMail(FormDataMultiPart formData) {

		WebTarget mgRoot = client.target(BASE_URL);

		Response response = mgRoot
				.path("/{domain}/messages")
				.resolveTemplate("domain", domain)
				.request(MediaType.APPLICATION_FORM_URLENCODED)
				.buildPost(
						Entity.entity(formData, MediaType.MULTIPART_FORM_DATA))
				.invoke(Response.class);
		logger.info("Mailgun response: {}: {}", response.getStatus(), response
				.getStatusInfo().getReasonPhrase());
	}

	@Override
	public void send(EmailMessage email) throws MessagingException {

		try {
			FormDataMultiPart formData = (FormDataMultiPart) formatter
					.format(email);
			String fromField = String.format("%s <%s>", sender.getSenderName(),
					sender.getSenderAddress());
			formData.field("from", fromField);
			invokeMailgunRestApiToSendMail(formData);
		} catch (Exception e) {
			logger.error("Exception occurred", e);
			throw new MessagingException(e);
		}
	}
}
