package com.bnbneeds.app.messaging.email;

import java.io.InputStream;

public class Attachment {

	private String mediaType;
	private String fileName;
	private InputStream inputStream;

	public String getMediaType() {
		return mediaType;
	}

	public String getFileName() {
		return fileName;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

}
