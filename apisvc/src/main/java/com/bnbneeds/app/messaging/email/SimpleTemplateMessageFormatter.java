package com.bnbneeds.app.messaging.email;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.mail.BodyPart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.core.MediaType;

/**
 * This class uses the specified template file as a layout for e-mail body. Make
 * sure to replace these place holders: {PLACE_HOLDER_SUBJECT} and
 * {PLACE_HOLDER_BODY}
 * 
 * @author Amit Herlekar
 *
 */
public class SimpleTemplateMessageFormatter extends MessageFormatter {

	protected String templateFileLocation;

	public void setTemplateFileLocation(String templateFileLocation) {
		this.templateFileLocation = templateFileLocation;
	}

	@Override
	public MimeMultipart format(EmailMessage email) throws MessagingException {
		try {
			String htmlMessage = EmailUtils
					.readEmailTemplateAndResolveMessageValues(resourceLoader,
							templateFileLocation, email);

			MimeMultipart multipart = new MimeMultipart();

			// Set message body
			BodyPart messageBodyPart = new MimeBodyPart();

			messageBodyPart.setContent(htmlMessage, MediaType.TEXT_HTML);

			multipart.addBodyPart(messageBodyPart);
			// Set image logo
			messageBodyPart = getMimeBodyPartForPNGFile("bnbneeds",
					getBNBneedsLogoFile());
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = getMimeBodyPartForPNGFile("website",
					getWebsiteIconFile());
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = getMimeBodyPartForPNGFile("email",
					getEmailIconFile());
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = getMimeBodyPartForPNGFile("facebook",
					getFacebookLogoFile());
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = getMimeBodyPartForPNGFile("twitter",
					getTwitterLogoFile());
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = getMimeBodyPartForPNGFile("google-plus",
					getGooglePlusLogoFile());
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = getMimeBodyPartForPNGFile("google-play",
					getGooglePlayLogoFile());
			multipart.addBodyPart(messageBodyPart);

			List<Attachment> attachments = email.getAttachments();
			if (attachments != null && !attachments.isEmpty()) {
				for (Iterator<Attachment> iterator = attachments.iterator(); iterator
						.hasNext();) {
					Attachment attachment = iterator.next();
					multipart.addBodyPart(EmailUtils
							.getMimeBodyPartForFile(attachment));
				}
			}

			Attachment inlineImage = email.getInlineImage();
			if (inlineImage != null) {
				multipart.addBodyPart(EmailUtils.getMimeBodyPartForFile(
						"inline-image", inlineImage));
			}

			return multipart;
		} catch (MessagingException | IOException
				| javax.mail.MessagingException e) {
			throw new MessagingException(e);
		}
	}

}
