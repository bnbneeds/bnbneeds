package com.bnbneeds.app.messaging.email.impl.appengine;

import java.util.Properties;

import javax.mail.Session;

import com.bnbneeds.app.messaging.email.AbstractEmailClientImpl;

public class AppEngineEmailClient extends AbstractEmailClientImpl {

	@Override
	public Session getMailSession() {
		Properties properties = new Properties();
		Session session = Session.getDefaultInstance(properties, null);
		return session;
	}

}
