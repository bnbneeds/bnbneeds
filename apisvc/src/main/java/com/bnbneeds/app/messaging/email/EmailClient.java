package com.bnbneeds.app.messaging.email;

import javax.mail.Session;

public interface EmailClient {

	public void setMessageFormatter(MessageFormatter formatter);

	public Session getMailSession();

	public void send(EmailMessage emailMessage) throws MessagingException;

}
