package com.bnbneeds.app.messaging.email.impl.mailgun;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;

import com.bnbneeds.app.messaging.email.Attachment;
import com.bnbneeds.app.messaging.email.EmailMessage;
import com.bnbneeds.app.messaging.email.EmailUtils;
import com.bnbneeds.app.messaging.email.MessageFormatter;
import com.bnbneeds.app.util.StringUtils;
import com.google.gson.Gson;

public class MailgunSimpleTemplateMessageFormatter extends MessageFormatter {

	protected String templateFileLocation;

	public void setTemplateFileLocation(String templateFileLocation) {
		this.templateFileLocation = templateFileLocation;
	}

	@Override
	public FormDataMultiPart format(EmailMessage email) throws IOException {

		String htmlMessage = EmailUtils
				.readEmailTemplateAndResolveMessageValues(resourceLoader,
						templateFileLocation, email);

		FormDataMultiPart form = new FormDataMultiPart();
		form.field("html", htmlMessage);
		form.field("subject", email.getSubject());
		form.bodyPart(MailGunUtils
				.getInlineFileDataBodyPart(getBNBneedsLogoFile()));
		form.bodyPart(MailGunUtils
				.getInlineFileDataBodyPart(getWebsiteIconFile()));
		form.bodyPart(MailGunUtils
				.getInlineFileDataBodyPart(getEmailIconFile()));
		form.bodyPart(MailGunUtils
				.getInlineFileDataBodyPart(getFacebookLogoFile()));
		form.bodyPart(MailGunUtils
				.getInlineFileDataBodyPart(getTwitterLogoFile()));
		form.bodyPart(MailGunUtils
				.getInlineFileDataBodyPart(getGooglePlusLogoFile()));
		form.bodyPart(MailGunUtils
				.getInlineFileDataBodyPart(getGooglePlayLogoFile()));

		List<String> toList = email.getTo();
		if (toList != null) {
			if (toList.size() == 1) {
				form.field("to", toList.get(0));
			} else {
				Gson gson = new Gson();
				Map<String, RecipientIDHolder> variableMap = new HashMap<String, RecipientIDHolder>();

				for (int i = 0; i < toList.size(); i++) {
					String recipient = toList.get(i);
					form.field("to", recipient);
					variableMap.put(recipient, new RecipientIDHolder(i + 1));
				}
				form.field("recipient-variables", gson.toJson(variableMap));
			}
		}
		List<String> ccList = email.getCc();
		if (ccList != null) {
			String ccListCSV = StringUtils
					.collectionToCommaDelimitedString(ccList);
			form.field("cc", ccListCSV);
		}

		List<String> bccList = email.getBcc();
		if (bccList != null) {
			String bccListCSV = StringUtils
					.collectionToCommaDelimitedString(bccList);
			form.field("bcc", bccListCSV);
		}

		List<Attachment> attachments = email.getAttachments();
		if (attachments != null) {
			for (Iterator<Attachment> iterator = attachments.iterator(); iterator
					.hasNext();) {
				Attachment attachment = iterator.next();
				form.bodyPart(MailGunUtils
						.getFileDataBodyPartAsAttachment(attachment));
			}
		}

		Attachment inlineImage = email.getInlineImage();
		if (inlineImage != null) {
			form.bodyPart(MailGunUtils
					.getFileDataBodyPartAsInlineAttachment(inlineImage));
		}

		return form;
	}

	private static class RecipientIDHolder {

		@SuppressWarnings("unused")
		private int id;

		public RecipientIDHolder(int id) {
			super();
			this.id = id;
		}
	}

}
