package com.bnbneeds.app.messaging.email;

public class MailSenderVO {

	private String senderName;
	private String senderAddress;
	private String replyToAddress;

	public String getSenderName() {
		return senderName;
	}

	public String getSenderAddress() {
		return senderAddress;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	public String getReplyToAddress() {
		return replyToAddress;
	}

	public void setReplyToAddress(String replyToAddress) {
		this.replyToAddress = replyToAddress;
	}

}
