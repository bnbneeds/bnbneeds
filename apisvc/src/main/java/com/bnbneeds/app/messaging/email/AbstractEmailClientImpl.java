package com.bnbneeds.app.messaging.email;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractEmailClientImpl implements EmailClient {

	private static final Logger logger = LoggerFactory
			.getLogger(AbstractEmailClientImpl.class);

	protected MessageFormatter formatter;

	protected MailSenderVO sender;

	public void setSender(MailSenderVO sender) {
		this.sender = sender;
	}

	@Override
	public void setMessageFormatter(MessageFormatter formatter) {
		this.formatter = formatter;
	}

	@Override
	public void send(EmailMessage email) throws MessagingException {

		try {
			Message mailMessage = getMailMessage(email);
			mailMessage.setSubject(email.getSubject());
			MimeMultipart multipartMessage = (MimeMultipart) formatter
					.format(email);
			mailMessage.setContent(multipartMessage);
			Transport.send(mailMessage);
		} catch (javax.mail.MessagingException e) {
			logger.error("MessagingException occurred", e);
			throw new MessagingException(e);
		} catch (UnsupportedEncodingException e) {
			logger.error("UnsupportedEncodingException occurred", e);
			throw new MessagingException(e);
		} catch (IOException e) {
			logger.error("IOException occurred", e);
			throw new MessagingException(e);
		}
	}

	private Message getMailMessage(EmailMessage email)
			throws javax.mail.MessagingException, IOException {

		Session session = getMailSession();
		Message mailMessage = new MimeMessage(session);
		Address from = new InternetAddress(sender.getSenderAddress(),
				sender.getSenderName());
		Address replyTo = new InternetAddress(sender.getReplyToAddress(),
				sender.getSenderName());
		mailMessage.setReplyTo(new Address[] { replyTo });
		mailMessage.setFrom(from);
		mailMessage.setRecipients(Message.RecipientType.TO,
				EmailUtils.getRecipients(email.getTo()));
		List<String> cc = email.getCc();
		if (cc != null && !cc.isEmpty()) {
			mailMessage.setRecipients(Message.RecipientType.CC,
					EmailUtils.getRecipients(cc));
		}

		List<String> bcc = email.getBcc();

		if (bcc != null && !bcc.isEmpty()) {
			mailMessage.setRecipients(Message.RecipientType.BCC,
					EmailUtils.getRecipients(bcc));
		}

		return mailMessage;
	}
}
