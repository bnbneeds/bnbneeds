package com.bnbneeds.app.messaging.email;

import java.io.File;
import java.io.IOException;

import javax.mail.internet.MimeBodyPart;

import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;

/**
 * Use this class to format email messages. format() will be called implicitly
 * before sending the e-mail.
 * 
 * @author Amit Herlekar
 *
 */
public abstract class MessageFormatter implements ResourceLoaderAware {

	protected static final String MEDIA_TYPE_IMAGE_PNG = "image/png";
	protected static final String BNBNEEDS_LOGO_FILE = "classpath:static/images/logos/bnbneeds.png";
	protected static final String GOOGLE_PLAY_LOGO_FILE = "classpath:static/images/logos/google-play-badge.png";
	protected static final String APPLE_STORE_LOGO_FILE = "classpath:static/images/logos/app-store.png";

	// Basic Icons
	protected static final String WEBSITE_ICON_FILE = "classpath:static/images/icons/globe.png";
	protected static final String EMAIL_ICON_FILE = "classpath:static/images/icons/mail.png";

	// Social Media Logos
	protected static final String FACEBOOK_LOGO_FILE = "classpath:static/images/SocialMediaIcons/PNG/CircleGrey/Facebook.png";
	protected static final String TWITTER_LOGO_FILE = "classpath:static/images/SocialMediaIcons/PNG/CircleGrey/Twitter.png";
	protected static final String GPLUS_LOGO_FILE = "classpath:static/images/SocialMediaIcons/PNG/CircleGrey/Google+.png";
	protected ResourceLoader resourceLoader;

	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

	protected File getResourceAsFile(String filePath) throws IOException {
		return resourceLoader.getResource(filePath).getFile();
	}

	protected File getWebsiteIconFile() throws IOException {
		return getResourceAsFile(WEBSITE_ICON_FILE);
	}

	protected File getEmailIconFile() throws IOException {
		return getResourceAsFile(EMAIL_ICON_FILE);
	}

	protected File getBNBneedsLogoFile() throws IOException {
		return getResourceAsFile(BNBNEEDS_LOGO_FILE);
	}

	protected File getFacebookLogoFile() throws IOException {
		return getResourceAsFile(FACEBOOK_LOGO_FILE);
	}

	protected File getTwitterLogoFile() throws IOException {
		return getResourceAsFile(TWITTER_LOGO_FILE);
	}

	protected File getGooglePlusLogoFile() throws IOException {
		return getResourceAsFile(GPLUS_LOGO_FILE);
	}

	protected File getGooglePlayLogoFile() throws IOException {
		return getResourceAsFile(GOOGLE_PLAY_LOGO_FILE);
	}

	protected File getAppleStoreLogoFile() throws IOException {
		return getResourceAsFile(APPLE_STORE_LOGO_FILE);
	}

	protected MimeBodyPart getMimeBodyPartForPNGFile(String content, File file)
			throws MessagingException {
		try {
			return EmailUtils.getMimeBodyPartForFile(content,
					MEDIA_TYPE_IMAGE_PNG, file);
		} catch (javax.mail.MessagingException e) {
			throw new MessagingException(e);
		}
	}

	public abstract Object format(EmailMessage email) throws IOException,
			MessagingException;

}
