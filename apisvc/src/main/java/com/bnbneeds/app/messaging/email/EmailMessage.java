package com.bnbneeds.app.messaging.email;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

public class EmailMessage {

	private String subject;
	private String message;
	private List<String> to;
	private List<String> cc;
	private List<String> bcc;
	private List<Attachment> attachments;
	private Attachment inlineImage;

	/**
	 * @param subject
	 *            the mail subject
	 * @param message
	 *            the mail message
	 * @param the
	 *            recipient list
	 */
	public EmailMessage(String subject, String message, List<String> to) {
		super();
		Assert.hasText(subject, "Subject cannot be null or empty.");
		Assert.hasText(message, "Message cannot be null or empty.");
		Assert.notEmpty(to, "To cannot be null or empty.");
		this.subject = subject;
		this.message = message;
		this.to = to;
	}

	public void addTo(String address) {
		if (to == null) {
			to = new ArrayList<String>();
		}
		to.add(address);
	}

	public void addCC(String address) {
		if (cc == null) {
			cc = new ArrayList<String>();
		}
		cc.add(address);
	}

	public void addBCC(String address) {
		if (bcc == null) {
			bcc = new ArrayList<String>();
		}
		bcc.add(address);
	}

	public void addAttachment(Attachment attachment) {
		if (attachments == null) {
			attachments = new ArrayList<Attachment>();
		}
		attachments.add(attachment);
	}

	public String getSubject() {
		return subject;
	}

	public String getMessage() {
		return message;
	}

	public List<String> getTo() {
		return to;
	}

	public List<String> getCc() {
		return cc;
	}

	public List<String> getBcc() {
		return bcc;
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setTo(List<String> to) {
		this.to = to;
	}

	public void setCc(List<String> cc) {
		this.cc = cc;
	}

	public void setBcc(List<String> bcc) {
		this.bcc = bcc;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public Attachment getInlineImage() {
		return inlineImage;
	}

	public void setInlineImage(Attachment inlineImage) {
		this.inlineImage = inlineImage;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmailMessage [");
		if (subject != null)
			builder.append("subject=").append(subject).append(", ");
		if (message != null)
			builder.append("message=").append(message).append(", ");
		if (to != null)
			builder.append("to=").append(to).append(", ");
		if (cc != null)
			builder.append("cc=").append(cc).append(", ");
		if (bcc != null)
			builder.append("bcc=").append(bcc).append(", ");
		if (attachments != null)
			builder.append("attachments=").append(attachments);
		builder.append("]");
		return builder.toString();
	}

}
