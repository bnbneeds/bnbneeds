package com.bnbneeds.app.messaging.email;

import java.text.MessageFormat;
import java.util.Properties;

import org.springframework.util.Assert;

public class Template {

	private String subject;
	private String body;

	public Template(Properties prop, MessageKeys messageKey) {
		String subjectKeyString = messageKey.getKey() + ".subject";
		this.subject = prop.getProperty(subjectKeyString);
		Assert.notNull(this.subject, "No key or value was found for the key: "
				+ subjectKeyString);
		String messageKeyString = messageKey.getKey() + ".message";
		this.body = prop.getProperty(messageKeyString);
		Assert.notNull(this.body, "No key or value was found for the key: "
				+ messageKeyString);

	}

	public Template(String subject, String message) {
		this.subject = subject;
		this.body = message;
	}

	public String getSubject() {
		return subject;
	}

	public void resolveSubjectValues(String... values) {
		this.subject = MessageFormat.format(subject, values);
	}

	public void resolveBodyValues(String... values) {
		this.body = MessageFormat.format(body, values);
	}

	public String getBody() {
		return body;
	}

}