package com.bnbneeds.app.messaging.email;

public class MessagingException extends Exception {

	private static final long serialVersionUID = -4585758253466877646L;

	public MessagingException() {
		super();
	}

	public MessagingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public MessagingException(String arg0) {
		super(arg0);
	}

	public MessagingException(Throwable arg0) {
		super(arg0);
	}

}
