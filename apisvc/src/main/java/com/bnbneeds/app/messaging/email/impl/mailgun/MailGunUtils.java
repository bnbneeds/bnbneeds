package com.bnbneeds.app.messaging.email.impl.mailgun;

import java.io.File;
import java.io.InputStream;

import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;

import com.bnbneeds.app.messaging.email.Attachment;

public class MailGunUtils {

	/**
	 * Returns the inline FileDataBodyPart for Mailgun file attachment. The file
	 * media type is set as application/octet-stream}
	 * 
	 * @param file
	 *            the file to be attached with {@code inline} as the name of the
	 *            body part.
	 * @return the instance of FileDataBodyPart
	 * @see <a
	 *      href="https://documentation.mailgun.com/en/latest/user_manual.html#sending-via-api">Sending
	 *      mails via Mailgun API</a>
	 */
	public static FileDataBodyPart getInlineFileDataBodyPart(File file) {
		return new FileDataBodyPart("inline", file,
				MediaType.APPLICATION_OCTET_STREAM_TYPE);
	}

	/**
	 * 
	 * @param file
	 *            the file to be attached with {@code attachment} as the name of
	 *            the body part.
	 * @param mediaType
	 *            the media type {@link MediaType}
	 * @return the instance of FileDataBodyPart
	 * @see <a
	 *      href="https://documentation.mailgun.com/en/latest/user_manual.html#sending-via-api">Sending
	 *      mails via Mailgun API</a>
	 */
	public static FileDataBodyPart getFileDataBodyPartAsAttachment(File file,
			MediaType mediaType) {
		return new FileDataBodyPart("attachment", file, mediaType);
	}

	/**
	 * 
	 * @param is
	 *            the file input stream of the file to be attached with
	 *            {@code attachment} as the name of the body part.
	 * @param fileName
	 *            the name of the file
	 * @param mediaType
	 *            the media type {@link MediaType}
	 * @return the instance of StreamDataBodyPart
	 * @see <a
	 *      href="https://documentation.mailgun.com/en/latest/user_manual.html#sending-via-api">Sending
	 *      mails via Mailgun API</a>
	 */
	public static StreamDataBodyPart getFileDataBodyPartAsAttachment(
			InputStream is, String fileName, MediaType mediaType) {
		return new StreamDataBodyPart("attachment", is, fileName, mediaType);
	}

	public static StreamDataBodyPart getFileDataBodyPartAsAttachment(
			InputStream is, String fileName, String mediaType) {
		return new StreamDataBodyPart("attachment", is, fileName,
				MediaType.valueOf(mediaType));
	}

	public static StreamDataBodyPart getFileDataBodyPartAsInlineAttachment(
			InputStream is, String fileName) {
		return new StreamDataBodyPart("inline", is, fileName,
				MediaType.APPLICATION_OCTET_STREAM_TYPE);
	}

	/**
	 * @see #getFileDataBodyPartAsAttachment(InputStream, String, MediaType)
	 * @param attachment
	 *            the attachment
	 * @return the StreamDataBodyPart
	 */
	public static StreamDataBodyPart getFileDataBodyPartAsAttachment(
			Attachment attachment) {
		return getFileDataBodyPartAsAttachment(attachment.getInputStream(),
				attachment.getFileName(),
				MediaType.valueOf(attachment.getMediaType()));
	}

	/**
	 * @see #getFileDataBodyPartAsInlineAttachment(InputStream, String, String)
	 * @param attachment
	 *            the attachment
	 * @return the StreamDataBodyPart
	 */
	public static StreamDataBodyPart getFileDataBodyPartAsInlineAttachment(
			Attachment attachment) {
		return getFileDataBodyPartAsInlineAttachment(
				attachment.getInputStream(), attachment.getFileName());
	}

}
