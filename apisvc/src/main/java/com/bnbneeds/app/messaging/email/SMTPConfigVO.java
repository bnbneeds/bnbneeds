package com.bnbneeds.app.messaging.email;

public class SMTPConfigVO {

	private String accountUsername;
	private String accountPassword;
	private String serverName;
	private int serverPort;

	public String getAccountUsername() {
		return accountUsername;
	}

	public String getAccountPassword() {
		return accountPassword;
	}

	public String getServerName() {
		return serverName;
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setAccountUsername(String accountUsername) {
		this.accountUsername = accountUsername;
	}

	public void setAccountPassword(String accountPassword) {
		this.accountPassword = accountPassword;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

}
