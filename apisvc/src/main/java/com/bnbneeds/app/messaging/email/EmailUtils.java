package com.bnbneeds.app.messaging.email;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.util.ByteArrayDataSource;

import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;

public class EmailUtils {

	public static InternetAddress[] getRecipients(List<String> recipients)
			throws AddressException {

		String addressListString = StringUtils
				.collectionToCommaDelimitedString(recipients);
		return InternetAddress.parse(addressListString);
	}

	public static MimeBodyPart getMimeBodyPartForFile(String content,
			String mediaType, File file) throws MessagingException {

		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setFileName(file.getName());
		messageBodyPart.setContent(content, mediaType);
		DataSource fds = new FileDataSource(file);
		messageBodyPart.setContentID("<" + file.getName() + ">");
		messageBodyPart.setDataHandler(new DataHandler(fds));
		return messageBodyPart;
	}

	public static MimeBodyPart getMimeBodyPartForFile(InputStream is,
			String fileName, String mediaType) throws MessagingException,
			IOException {
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setFileName(fileName);
		ByteArrayDataSource bds = new ByteArrayDataSource(is, mediaType);
		messageBodyPart.setDataHandler(new DataHandler(bds));
		return messageBodyPart;
	}

	public static MimeBodyPart getMimeBodyPartForFile(InputStream is,
			String contentName, String fileName, String mediaType)
			throws MessagingException, IOException {
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setFileName(fileName);
		messageBodyPart.setContent(contentName, mediaType);
		messageBodyPart.setContentID("<" + fileName + ">");
		ByteArrayDataSource bds = new ByteArrayDataSource(is, mediaType);
		messageBodyPart.setDataHandler(new DataHandler(bds));
		return messageBodyPart;
	}

	public static MimeBodyPart getMimeBodyPartForFile(Attachment attachment)
			throws MessagingException, IOException {
		return getMimeBodyPartForFile(attachment.getInputStream(),
				attachment.getFileName(), attachment.getMediaType());
	}

	public static MimeBodyPart getMimeBodyPartForFile(String contentName,
			Attachment attachment) throws MessagingException, IOException {
		return getMimeBodyPartForFile(attachment.getInputStream(), contentName,
				attachment.getFileName(), attachment.getMediaType());
	}

	/**
	 * Reads the email template from the resource loader and resolves message
	 * values. Note: Email template file must have these place holders:
	 * {PLACE_HOLDER_SUBJECT} and {PLACE_HOLDER_BODY}
	 * 
	 * @param resourceLoader
	 * @param templateFilePath
	 * @param subject
	 *            the mail subject
	 * @param message
	 *            the mail body
	 * @return the mail text with resolved subject and body vaules
	 * @throws IOException
	 */
	public static String readEmailTemplateAndResolveMessageValues(
			ResourceLoader resourceLoader, String templateFilePath,
			EmailMessage email) throws IOException {

		String htmlMessage = null;
		InputStream emailTemplateIn = resourceLoader.getResource(
				templateFilePath).getInputStream();
		if (emailTemplateIn == null) {
			throw new FileNotFoundException(
					"Cannot find template file in classpath: "
							+ templateFilePath);
		}
		InputStreamReader reader = new InputStreamReader(emailTemplateIn);
		htmlMessage = FileCopyUtils.copyToString(reader);
		htmlMessage = htmlMessage.replaceAll("\\{PLACE_HOLDER_SUBJECT\\}",
				email.getSubject());
		htmlMessage = htmlMessage.replaceAll("\\{PLACE_HOLDER_BODY\\}",
				email.getMessage());

		Attachment inlineImage = email.getInlineImage();
		if (inlineImage != null) {
			htmlMessage = htmlMessage
					.replaceAll(
							"\\{INLINE_IMAGE\\}",
							String.format(
									"<p class='align-center'><img src='cid:%s' alt='inlineImage' data-default='placeholder' width='400px'/></p>",
									inlineImage.getFileName()));

		}

		return htmlMessage;
	}
}
