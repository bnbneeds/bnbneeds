package com.bnbneeds.app.messaging.email;

public enum MessageKeys {

	DEALER_REGISTERED("dealer.registered"),
	USER_PASSWORD_CHANGED("user.password.changed"),
	USER_PASSWORD_RESET("user.password.reset"),
	PRODUCT_ENQUIRY_POSTED("product.enquiry.posted"),
	VENDOR_BIDDING_SUBSCRIPTION_EXPIRED("vendor.bidding.subscription.expired"),
	BUYER_BIDDING_SUBSCRIPTION_EXPIRED("buyer.bidding.subscription.expired"),
	BUYER_BIDDING_SUBSCRIPTION_DUE_FOR_EXPIRY("buyer.bidding.subscription.about-to-expire"),
	VENDOR_BIDDING_SUBSCRIPTION_DUE_FOR_EXPIRY("vendor.bidding.subscription.about-to-expire");
	
	private String key;

	private MessageKeys(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

}
