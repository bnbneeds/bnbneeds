package com.bnbneeds.app.messaging.email;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

public class SMTPClient extends AbstractEmailClientImpl {

	private static final String SMTP_SERVER_HOST = "smtp.host";
	private static final String SMTP_SERVER_PORT = "smtp.port";

	protected SMTPConfigVO smtpConfigVO;

	public void setSmtpConfigVO(SMTPConfigVO smtpConfigVO) {
		this.smtpConfigVO = smtpConfigVO;
	}

	@Override
	public Session getMailSession() {
		Properties prop = new Properties();
		prop.put(SMTP_SERVER_HOST, smtpConfigVO.getServerName());
		prop.put(SMTP_SERVER_PORT, smtpConfigVO.getServerPort());

		Session session = Session.getInstance(prop, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(smtpConfigVO
						.getAccountUsername(), smtpConfigVO
						.getAccountPassword());
			}
		});
		return session;
	}
}
