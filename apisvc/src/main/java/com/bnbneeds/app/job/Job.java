package com.bnbneeds.app.job;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.appengine.api.ThreadManager;

public class Job {

	private static final Logger logger = LoggerFactory.getLogger(Job.class);
	protected ExecutorService execService;

	public Job() {
		execService = Executors.newCachedThreadPool(ThreadManager
				.currentRequestThreadFactory());
	}

	public void execute(Runnable runner) {
		logger.info("Executing runner: {}", runner.getClass().getSimpleName());
		execService.execute(runner);
	}

	public void stop() {
		try {
			execService.shutdown();
			if (execService.awaitTermination(20, TimeUnit.SECONDS)) {
				logger.info("Job executed successfully.");
			} else {
				logger.info("Forcing job to shut down...");
				execService.shutdownNow();
			}
		} catch (InterruptedException e) {
			logger.error("InterruptedException occured during job shutdown", e);
		}

	}

}
