package com.bnbneeds.app.job.email;

import com.bnbneeds.app.messaging.email.EmailClient;


public interface EmailWorker extends Runnable {

	public void setEmailClient(EmailClient emailClient);

}
