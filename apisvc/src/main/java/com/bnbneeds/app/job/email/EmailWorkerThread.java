package com.bnbneeds.app.job.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bnbneeds.app.messaging.email.EmailClient;
import com.bnbneeds.app.messaging.email.EmailMessage;
import com.bnbneeds.app.messaging.email.MessagingException;

public class EmailWorkerThread implements EmailWorker {

	private static final Logger logger = LoggerFactory
			.getLogger(EmailWorkerThread.class);

	private EmailClient emailClient;
	private EmailMessage email;

	public EmailWorkerThread(EmailMessage email) {
		super();
		this.email = email;

	}

	public void setEmailClient(EmailClient emailClient) {
		this.emailClient = emailClient;
	}

	@Override
	public void run() {
		try {
			logger.info("Sending mail using {}", emailClient.getClass()
					.getSimpleName());
			logger.debug("Sending mail  {}", email);
			emailClient.send(email);
		} catch (MessagingException e) {
			logger.error("Exception occured while sending email: {}", e);
		}

	}
}
