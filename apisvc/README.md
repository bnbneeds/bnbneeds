BNBNeeds API service
=============================

[REST API specification](https://bitbucket.org/bnbneeds/bnbneeds/wiki/BNB%20Needs%20REST%20API%20Specification)

To buid projects:

```
#!java

root@AMITHERLEKAR  ~/home/bnbneeds (master)
$ ./gradle clean build

```

To boot up apisvc & portalsvc on appengine (local devserver at port 8080):

```
#!java

root@AMITHERLEKAR  ~/home/bnbneeds (master)
$ ./gradle appengineRun

```